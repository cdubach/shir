package cGen.eqsat

import cGen.{AlgoOp, BinaryOp, Constant, IntType}
import eqsat.{AnalysesBuilder, EGraph, ExprToText, HierarchicalStopwatch, Pattern}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class RelaxPatternTest {
  @Test
  def canRelaxNestedBinary(): Unit = {
    val t = IntType(32)
    val regularBinary = BinaryOp(Constant(0, t), Constant(42, t), AlgoOp.Add)
    val nestedBinary = BinaryOp(Constant(0, t), BinaryOp(Constant(10, t), Constant(20, t), AlgoOp.Mul), AlgoOp.Add)
    val graph = EGraph(analyses = new AnalysesBuilder().toAnalyses)
    val root = graph.add(regularBinary)
    graph.rebuild()

    val relaxedPattern = Pattern(nestedBinary).relax(graph)(root, HierarchicalStopwatch.disabled).get
    assert(ExprToText(relaxedPattern) == "(const 0: i32) + (expr 0: unknown)")
  }
}
