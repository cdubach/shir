package cGen.eqsat

import cGen.idioms.ExtractionBasedSemanticRules
import cGen.primitive.Idx
import cGen.{AlgoOp, BinaryOp, Build, Constant, EagerType, ExternFunctionCall, IntType, IntTypeVar}
import core.{Expr, ExprVar, FunctionCall, TypeChecker}
import eqsat.{AnalysesBuilder, DeBruijnLambda, DeBruijnParamUse, EClassT, EGraph, ENodeT, ExtractionAnalysis, HierarchicalStopwatch, ParameterUseAnalysis, Pattern, Rewrite}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

/**
  * Tests for the rewrite system that replace simulated madd with the builtin madd. These tests stress
  * ExtractionBasedSemanticRules in particular.
  */
@Category(Array(classOf[EssentialTests]))
class RewriteMAddTest {
  def areEquivalent[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])(eClass: EClass, pattern: Expr): Boolean =
    Pattern(pattern).searchEClass(graph)(eClass, HierarchicalStopwatch.disabled).nonEmpty

  def simulatedMAdd(a: Expr, b: Expr, c: Expr): Expr = {
    TypeChecker.check(TypeChecker.check(BinaryOp(a, TypeChecker.check(BinaryOp(b, c, AlgoOp.Mul)), AlgoOp.Add)))
  }

  def builtinMAdd(a: Expr, b: Expr, c: Expr): Expr = {
    TypeChecker.check(ExternFunctionCall("madd", Seq(a, b, c), Seq(IntTypeVar(), IntTypeVar(), IntTypeVar()), a.t))
  }

  def simulatedAdd(a: Expr, b: Expr): Expr = {
    TypeChecker.check(BinaryOp(a, b, AlgoOp.Add))
  }

  def builtinAdd(a: Expr, b: Expr): Expr = {
    TypeChecker.check(ExternFunctionCall("add", Seq(a, b), Seq(IntTypeVar(), IntTypeVar()), a.t))
  }

  def simulatedMul(a: Expr, b: Expr): Expr = {
    TypeChecker.check(BinaryOp(a, b, AlgoOp.Mul))
  }

  def builtinMul(a: Expr, b: Expr): Expr = {
    TypeChecker.check(ExternFunctionCall("mul", Seq(a, b), Seq(IntTypeVar(), IntTypeVar()), a.t))
  }

  val findMAddRule: Rewrite = {
    val t = IntType(32)
    val a = ExprVar(t)
    val b = ExprVar(t)
    val c = ExprVar(t)
    Rewrite(
      "FindMAdd",
      simulatedMAdd(a, b, c),
      builtinMAdd(a, b, c))
  }

  val findAddRule: Rewrite = {
    val t = IntType(32)
    val a = ExprVar(t)
    val b = ExprVar(t)
    Rewrite(
      "FindAdd",
      simulatedAdd(a, b),
      builtinAdd(a, b))
  }

  val findMulRule: Rewrite = {
    val t = IntType(32)
    val a = ExprVar(t)
    val b = ExprVar(t)
    Rewrite(
      "FindMul",
      simulatedMul(a, b),
      builtinMul(a, b))
  }

  val extractionAnalysis = ExtractionAnalysis(
    "madd-extractor",
    (node: Expr, argCosts: Seq[BigInt]) => {
      (node, argCosts) match {
        case (ExternFunctionCall("add", _, _, _), _) => 1 + argCosts.sum
        case (ExternFunctionCall("madd", _, _, _), _) => 1 + argCosts.sum
        case (ExternFunctionCall("mul", _, _, _), _) => 1 + argCosts.sum
        case _ => 2 + argCosts.sum
      }
    },
    _ => true)

  val rules: Seq[Rewrite] = Seq(findMAddRule, findAddRule, findMulRule) ++
    ExtractionBasedSemanticRules(extractionAnalysis.extractor).rules

  val rulesAndReverses: Seq[Rewrite] =
    Seq(findMAddRule, findAddRule, findMulRule, findMAddRule.reverse, findAddRule.reverse, findMulRule.reverse) ++
      ExtractionBasedSemanticRules(extractionAnalysis.extractor).rules

  @Test
  def canFindMAdd(): Unit = {
    val i32 = IntType(32)
    val a = Constant(1, i32)
    val b = Constant(42, i32)
    val c = Constant(20, i32)

    val graph = EGraph(analyses = new AnalysesBuilder().add(ParameterUseAnalysis).add(extractionAnalysis).toAnalyses)
    val root = graph.add(simulatedMAdd(a, b, c))
    graph.rebuild()

    graph.saturate(rules, 1)

    assert(areEquivalent(graph)(root, builtinMAdd(a, b, c)))
    assert(areEquivalent(graph)(root, builtinAdd(a, simulatedMul(b, c))))
    assert(areEquivalent(graph)(root, builtinAdd(a, builtinMul(b, c))))
  }

  @Test
  def canFindMAddWithInlining(): Unit = {
    val i32 = IntType(32)
    val a = Constant(1, i32)
    val b = Constant(42, i32)
    val c = Constant(20, i32)

    val graph = EGraph(analyses = new AnalysesBuilder().add(ParameterUseAnalysis).add(extractionAnalysis).toAnalyses)
    val root = graph.add(simulatedAdd(a, FunctionCall(DeBruijnLambda(i32, simulatedMul(b, DeBruijnParamUse(0, i32))), c)))
    graph.rebuild()

    graph.saturate(rules, 2)

    assert(areEquivalent(graph)(root, builtinMAdd(a, b, c)))
    assert(areEquivalent(graph)(root, builtinAdd(a, simulatedMul(b, c))))
    assert(areEquivalent(graph)(root, builtinAdd(a, builtinMul(b, c))))
  }

  @Test
  def cannotFindMAddWithDelayedInlining(): Unit = {
    val i32 = IntType(32)
    val a = Constant(1, i32)
    val b = Constant(42, i32)
    val c = Constant(20, i32)

    val graph = EGraph(analyses = new AnalysesBuilder().add(ParameterUseAnalysis).add(extractionAnalysis).toAnalyses)
    val root = graph.add(simulatedAdd(a, Idx(Build(DeBruijnLambda(i32, simulatedMul(b, DeBruijnParamUse(0, i32))), 100), c, EagerType())))
    graph.rebuild()

    graph.saturate(rules, 3)

    assert(areEquivalent(graph)(root, builtinAdd(a, builtinMul(b, c))))
    assert(!areEquivalent(graph)(root, builtinMAdd(a, b, c)))
    assert(!areEquivalent(graph)(root, builtinAdd(a, simulatedMul(b, c))))
  }

  @Test
  def canFindDelayedMAddWithBidirectionalRules(): Unit = {
    val i32 = IntType(32)
    val a = Constant(1, i32)
    val b = Constant(42, i32)
    val c = Constant(20, i32)

    val graph = EGraph(analyses = new AnalysesBuilder().add(ParameterUseAnalysis).add(extractionAnalysis).toAnalyses)
    val root = graph.add(simulatedAdd(a, Idx(Build(DeBruijnLambda(i32, simulatedMul(b, DeBruijnParamUse(0, i32))), 100), c, EagerType())))
    graph.rebuild()

    graph.saturate(rulesAndReverses, 4)

    assert(areEquivalent(graph)(root, builtinAdd(a, builtinMul(b, c))))
    assert(areEquivalent(graph)(root, builtinMAdd(a, b, c)))
    assert(areEquivalent(graph)(root, builtinAdd(a, simulatedMul(b, c))))
  }
}
