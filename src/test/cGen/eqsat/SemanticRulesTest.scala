package cGen.eqsat

import cGen.AlgoOp.AlgoOp
import cGen.{AlgoOp, ArrayType, BinaryOp, Build, Constant, DoubleType, EagerType, Ifold, IntType}
import cGen.idioms.{ExtractionBasedSemanticRules, TimeComplexityExtractor}
import cGen.primitive.Idx
import core.{ArithTypeT, Expr, FunctionCall, TypeChecker}
import eqsat.{AnalysesBuilder, DeBruijnLambda, DeBruijnParamUse, EGraph, ExtractionAnalysis, Extractor, HierarchicalStopwatch, ParameterUseAnalysis, Pattern, Rewrite}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

/**
 * Tests for the rewrite system that check the system's ability to expand linear algebra operations such as matrix-vector
 * and matrix-matrix products. While not tested here, the expanded products can then be recognized as special
 * domain-specific functions.
 */
@Category(Array(classOf[EssentialTests]))
class SemanticRulesTest {
  def defaultExtractor: Extractor = ConditionalExtractor(Extractor.DefaultSmallest, DeBruijnLambda.tryCheckParamTypes)

  def rules: Seq[Rewrite] = ExtractionBasedSemanticRules(defaultExtractor).rules

  @Test
  def introduceMulOne(): Unit = {
    // This test checks that we can rewrite `p_4[p_2][p_1]` as `1 * p_4[p_2][p_1]`.

    val graph = EGraph(analyses = new AnalysesBuilder().add(TimeComplexityExtractor.analysis).add(ParameterUseAnalysis).toAnalyses)

    val i32 = IntType(32)
    val t = DoubleType()
    val vectorType = ArrayType(t, 1024)
    val matrixType = ArrayType(vectorType, 1024)
    val expr = TypeChecker.check(
      Idx(
        Idx(
          DeBruijnParamUse(4, matrixType),
          DeBruijnParamUse(2, i32),
          EagerType()),
        DeBruijnParamUse(1, i32),
        EagerType()))

    val root = graph.add(expr)
    graph.rebuild()
    graph.saturate(rules, 1)

    assert(Pattern(TypeChecker.check(BinaryOp(Constant(1.0, t), expr, AlgoOp.Mul)))
      .searchEClass(graph)(root, HierarchicalStopwatch.disabled)
      .nonEmpty)
  }

  @Test
  def zeroToVectorAndMatrix(): Unit = {
    // This test checks that we can rewrite `0` as `(build 1024 (\i32. 0)) p_0`.

    val graph = EGraph(
        analyses = new AnalysesBuilder().add(TimeComplexityExtractor.analysis).add(ParameterUseAnalysis).toAnalyses)

    val t = DoubleType()
    val i32 = IntType(32)
    val root = graph.add(Constant(0.0, t))
    //    val constVec = graph.add(TypeChecker.check(constArray(Constant(0, t), 1024)))
    graph.add(TypeChecker.check(Build(DeBruijnLambda(i32, DeBruijnParamUse(0, i32)), 1024)))
    graph.add(TypeChecker.check(DeBruijnParamUse(1, i32)))
    graph.rebuild()
    graph.defaultSaturationEngine.withMatchesAppliedIndefinitely.saturate(rules, 4)

    val expected = Idx(constArray(Constant(0, t), 1024), DeBruijnParamUse(0, i32), EagerType())

    assert(Pattern(expected)
      .searchEClass(graph)(root, HierarchicalStopwatch.disabled)
      .nonEmpty)

    val expected2 = Idx(constArray(Constant(0, t), 1024), DeBruijnParamUse(1, i32), EagerType())

    assert(Pattern(expected2)
      .searchEClass(graph)(root, HierarchicalStopwatch.disabled)
      .nonEmpty)

    val constVec = Pattern(TypeChecker.check(constArray(Constant(0, t), 1024))).search(graph).head.where

    assert(
      Pattern(
        FunctionCall(
          DeBruijnLambda(i32, constArray(Constant(0, t), 1024)),
          DeBruijnParamUse(1, i32)))
        .searchEClass(graph)(constVec, HierarchicalStopwatch.disabled)
        .nonEmpty)

    assert(
      Pattern(
        Idx(
          constArray(constArray(Constant(0, t), 1024), 1024),
          DeBruijnParamUse(1, i32),
          EagerType()))
        .searchEClass(graph)(constVec, HierarchicalStopwatch.disabled)
        .nonEmpty)

    val expectedMat = Idx(
      Idx(
        constArray(constArray(Constant(0, t), 1024), 1024),
        DeBruijnParamUse(1, i32),
        EagerType()),
      DeBruijnParamUse(0, i32),
      EagerType())

    assert(Pattern(expectedMat)
      .searchEClass(graph)(root, HierarchicalStopwatch.disabled)
      .nonEmpty)
  }

  @Test
  def introduceAddZeroVector(): Unit = {
    // This test checks that we can rewrite `p_1` as `p_1 + (build 1024 (\i32. 0)) p_0`.

    val graph = EGraph(
        analyses = new AnalysesBuilder().add(TimeComplexityExtractor.analysis).add(ParameterUseAnalysis).toAnalyses)

    val t = DoubleType()
    val i32 = IntType(32)
    val root = graph.add(DeBruijnParamUse(1, t))
    graph.add(TypeChecker.check(Build(DeBruijnLambda(i32, DeBruijnParamUse(0, i32)), 1024)))
    graph.rebuild()
    graph.saturate(rules, 3)

    val expected = TypeChecker.check(
      BinaryOp(
        DeBruijnParamUse(1, t),
        TypeChecker.check(Idx(constArray(Constant(0, t), 1024), DeBruijnParamUse(0, i32), EagerType())),
        AlgoOp.Add))

    assert(Pattern(expected)
      .searchEClass(graph)(root, HierarchicalStopwatch.disabled)
      .nonEmpty)
  }

  @Test
  def expandMatVecMul(): Unit = {
    val t = DoubleType
    val i32 = IntType(32)
    val n = 1024
    val m = n
    val alpha = Constant(1.0, t())
    val beta = Constant(1.0, t())
    val a = DeBruijnParamUse(4, ArrayType(ArrayType(t(), n), n))
    val b = DeBruijnParamUse(3, ArrayType(t(), n))
    val c = constArray(Constant(0.0, t()), n)

    def binOp(lhs: Expr, rhs: Expr, op: AlgoOp): Expr =
      TypeChecker.check(BinaryOp(TypeChecker.check(lhs), TypeChecker.check(rhs), op))

    val start = Build(
      DeBruijnLambda(i32,
        Ifold(
          DeBruijnLambda(i32,
            DeBruijnLambda(t(),
              binOp(
                binOp(
                  Idx(Idx(a, DeBruijnParamUse(2, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType()),
                  Idx(b, DeBruijnParamUse(1, i32), EagerType()),
                  AlgoOp.Mul),
                DeBruijnParamUse(0, t()), AlgoOp.Add))),
          Constant(0.0, t()),
          m)),
      n)

    val withAlpha = Build(
      DeBruijnLambda(i32,
        Ifold(
          DeBruijnLambda(i32,
            DeBruijnLambda(t(),
              binOp(
                binOp(
                  binOp(
                    alpha,
                    Idx(Idx(a, DeBruijnParamUse(2, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType()),
                    AlgoOp.Mul),
                  Idx(b, DeBruijnParamUse(1, i32), EagerType()),
                  AlgoOp.Mul),
                DeBruijnParamUse(0, t()), AlgoOp.Add))),
          Constant(0.0, t()),
          m)),
      n)

    val withZeroVec = Build(
      DeBruijnLambda(i32,
        binOp(
          Ifold(
            DeBruijnLambda(i32,
              DeBruijnLambda(t(),
                binOp(
                  binOp(
                    binOp(
                      alpha,
                      Idx(Idx(a, DeBruijnParamUse(2, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType()),
                      AlgoOp.Mul),
                    Idx(b, DeBruijnParamUse(1, i32), EagerType()),
                    AlgoOp.Mul),
                  DeBruijnParamUse(0, t()), AlgoOp.Add))),
            Constant(0.0, t()),
            m),
          Idx(c, DeBruijnParamUse(0, i32), EagerType()),
          AlgoOp.Add)),
      n)

    val destination = Build(
      DeBruijnLambda(i32,
        binOp(
          Ifold(
            DeBruijnLambda(i32,
              DeBruijnLambda(t(),
                binOp(
                  binOp(
                    binOp(
                      alpha,
                      Idx(Idx(a, DeBruijnParamUse(2, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType()),
                      AlgoOp.Mul),
                    Idx(b, DeBruijnParamUse(1, i32), EagerType()),
                    AlgoOp.Mul),
                  DeBruijnParamUse(0, t()), AlgoOp.Add))),
            Constant(0.0, t()),
            m),
          binOp(
            beta,
            Idx(c, DeBruijnParamUse(0, i32), EagerType()),
            AlgoOp.Mul),
          AlgoOp.Add)),
      n)

    val graph = EGraph(
        analyses = new AnalysesBuilder()
          .add(ExtractionAnalysis.smallestExpressionExtractor)
          .add(TimeComplexityExtractor.analysis)
          .toAnalyses)

    val root = graph.add(TypeChecker.check(start))
    graph.add(DeBruijnParamUse(0, i32))
    graph.rebuild()
    graph.saturate(rules, 2)

    assert(Pattern(TypeChecker.check(withAlpha)).searchEClass(graph)(root, HierarchicalStopwatch.disabled).nonEmpty)
    assert(Pattern(TypeChecker.check(withZeroVec)).searchEClass(graph)(root, HierarchicalStopwatch.disabled).nonEmpty)
    assert(Pattern(TypeChecker.check(destination)).searchEClass(graph)(root, HierarchicalStopwatch.disabled).nonEmpty)
  }

  @Test
  def expandMatMatMul(): Unit = {
    val t = DoubleType
    val i32 = IntType(32)
    val n = 1024
    val alpha = Constant(1.0, t())
    val beta = Constant(1.0, t())
    val a = DeBruijnParamUse(4, ArrayType(ArrayType(t(), n), n))
    val b = DeBruijnParamUse(3, ArrayType(ArrayType(t(), n), n))
    val c = constArray(constArray(Constant(0.0, t()), n), n)

    def binOp(lhs: Expr, rhs: Expr, op: AlgoOp): Expr =
      TypeChecker.check(BinaryOp(TypeChecker.check(lhs), TypeChecker.check(rhs), op))

    val start = Build(
      DeBruijnLambda(i32,
        Build(
          DeBruijnLambda(i32,
            Ifold(
              DeBruijnLambda(i32,
                DeBruijnLambda(t(),
                  binOp(
                    binOp(
                      Idx(Idx(a, DeBruijnParamUse(3, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType()),
                      Idx(Idx(b, DeBruijnParamUse(1, i32), EagerType()), DeBruijnParamUse(2, i32), EagerType()),
                      AlgoOp.Mul),
                    DeBruijnParamUse(0, t()), AlgoOp.Add))),
              Constant(0.0, t()),
              n)),
          n)),
      n)

    val withAlpha = Build(
      DeBruijnLambda(i32,
        Build(
          DeBruijnLambda(i32,
            Ifold(
              DeBruijnLambda(i32,
                DeBruijnLambda(t(),
                  binOp(
                    binOp(
                      alpha,
                      binOp(
                        Idx(Idx(a, DeBruijnParamUse(3, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType()),
                        Idx(Idx(b, DeBruijnParamUse(1, i32), EagerType()), DeBruijnParamUse(2, i32), EagerType()),
                        AlgoOp.Mul),
                      AlgoOp.Mul),
                    DeBruijnParamUse(0, t()),
                    AlgoOp.Add))),
              Constant(0.0, t()),
              n)),
          n)),
      n)

    val withZeroMat = Build(
      DeBruijnLambda(i32,
        Build(
          DeBruijnLambda(i32,
            binOp(
              Ifold(
                DeBruijnLambda(i32,
                  DeBruijnLambda(t(),
                    binOp(
                      binOp(
                        alpha,
                        binOp(
                          Idx(Idx(a, DeBruijnParamUse(3, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType()),
                          Idx(Idx(b, DeBruijnParamUse(1, i32), EagerType()), DeBruijnParamUse(2, i32), EagerType()),
                          AlgoOp.Mul),
                        AlgoOp.Mul),
                      DeBruijnParamUse(0, t()),
                      AlgoOp.Add))),
                Constant(0.0, t()),
                n),
              Idx(Idx(c, DeBruijnParamUse(1, i32), EagerType()), DeBruijnParamUse(0, i32), EagerType()),
              AlgoOp.Add)),
          n)),
      n)

    val destination = Build(
      DeBruijnLambda(i32,
        Build(
          DeBruijnLambda(i32,
            binOp(
              Ifold(
                DeBruijnLambda(i32,
                  DeBruijnLambda(t(),
                    binOp(
                      binOp(
                        alpha,
                        binOp(
                          Idx(Idx(a, DeBruijnParamUse(3, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType()),
                          Idx(Idx(b, DeBruijnParamUse(1, i32), EagerType()), DeBruijnParamUse(2, i32), EagerType()),
                          AlgoOp.Mul),
                        AlgoOp.Mul),
                      DeBruijnParamUse(0, t()),
                      AlgoOp.Add))),
                Constant(0.0, t()),
                n),
              binOp(
                beta,
                Idx(Idx(c, DeBruijnParamUse(1, i32), EagerType()), DeBruijnParamUse(0, i32), EagerType()),
                AlgoOp.Mul),
              AlgoOp.Add)),
          n)),
      n)

    val graph = EGraph(
        analyses = new AnalysesBuilder()
          .add(ExtractionAnalysis.smallestExpressionExtractor)
          .add(TimeComplexityExtractor.analysis)
          .add(ParameterUseAnalysis)
          .toAnalyses)

    val root = graph.add(TypeChecker.check(start))
    graph.add(DeBruijnParamUse(0, i32))
    graph.rebuild()
    graph.saturate(rules, 4)

    assert(Pattern(TypeChecker.check(withAlpha)).searchEClass(graph)(root, HierarchicalStopwatch.disabled).nonEmpty)
    assert(Pattern(TypeChecker.check(withZeroMat)).searchEClass(graph)(root, HierarchicalStopwatch.disabled).nonEmpty)
    assert(Pattern(TypeChecker.check(destination)).searchEClass(graph)(root, HierarchicalStopwatch.disabled).nonEmpty)
  }

  private def constArray(value: Expr, length: ArithTypeT): Expr = {
    val i32 = IntType(32)
    Build(DeBruijnLambda(i32, value), length)
  }
}
