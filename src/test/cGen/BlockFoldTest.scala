package cGen

import core._
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class BlockFoldTest {
  @Test
  def createBlockFold(): Unit = {
    val acc = ParamDef(IntType(32))
    val i = ParamDef(IntType(32))
    val j = ParamDef(IntType(32))
    val init = CLambda(i, ParamUse(i))
    val body = CLambda(Seq(i, j, acc).reverse, BinaryOp(ParamUse(acc), ParamUse(j), AlgoOp.Add))
    BlockFold(init, body, Seq(32), 32)
  }

  @Test
  def unwrapBlockFold(): Unit = {
    val acc = ParamDef(IntType(32))
    val i = ParamDef(IntType(32))
    val j = ParamDef(IntType(32))
    val init = CLambda(i, ParamUse(i))
    val body = CLambda(Seq(i, j, acc).reverse, BinaryOp(ParamUse(acc), ParamUse(j), AlgoOp.Add))
    val expr = BlockFold(init, body, Seq(32), 32)
    expr match {
      case BlockFold(initCopy, bodyCopy, dimsCopy, nCopy, _) =>
        assert(initCopy == init)
        assert(bodyCopy == body)
        assert(dimsCopy == expr.dims)
        assert(nCopy == expr.n)

      case _ =>
        assert(false)
    }
  }

  @Test
  def blockFoldSupportsArgsBuild(): Unit = {
    val acc = ParamDef(IntType(32))
    val i = ParamDef(IntType(32))
    val j = ParamDef(IntType(32))
    val init = CLambda(i, ParamUse(i))
    val body = CLambda(Seq(i, j, acc).reverse, BinaryOp(ParamUse(acc), ParamUse(j), AlgoOp.Add))
    val expr = BlockFold(init, body, Seq(32), 32)
    expr.argsBuild(expr.args) match {
      case BlockFold(initCopy, bodyCopy, dimsCopy, nCopy, _) =>
        assert(initCopy == init)
        assert(bodyCopy == body)
        assert(dimsCopy == expr.dims)
        assert(nCopy == expr.n)

      case _ =>
        assert(false)
    }
  }

  @Test
  def blockFoldTypeChecks(): Unit = {
    val acc = ParamDef(IntType(32))
    val i = ParamDef(IntType(32))
    val j = ParamDef(IntType(32))
    val init = CLambda(i, ParamUse(i))
    val body = CLambda(Seq(i, j, acc).reverse, BinaryOp(ParamUse(acc), ParamUse(j), AlgoOp.Add))
    val expr = BlockFold(init, body, Seq(32), 32)
    assert(TypeChecker.check(expr).t == ArrayType(IntType(32), 32))
  }

  @Test
  def twoDimBlockFoldTypeChecks(): Unit = {
    val acc = ParamDef(IntType(32))
    val i = ParamDef(IntType(32))
    val j = ParamDef(IntType(32))
    val k = ParamDef(IntType(32))
    val init = CLambda(Seq(i, k).reverse, BinaryOp(ParamUse(k), ParamUse(j), AlgoOp.Add))
    val body = CLambda(Seq(i, k, j, acc).reverse, BinaryOp(ParamUse(acc), ParamUse(j), AlgoOp.Add))
    val expr = BlockFold(init, body, Seq(32, 64), 32)
    assert(TypeChecker.check(expr).t == ArrayType(ArrayType(IntType(32), 64), 32))
  }
}
