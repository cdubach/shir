package cGen

import cGen.Lib._
import cGen.Util.FileWriter
import cGen.pass.TuningPass.{getUpperBoundRewriteNum, perfModelTuningPassWithDPS, randomTuningPassWithDPS}
import cGen.primitive._
import core._
import core.util.IRDotGraph
import org.junit.Test

import scala.collection.mutable
import scala.language.postfixOps

class DPSPassTest {

  @Test
  def scalarAddTest(): Unit = {
    var tree: Expr = {
      implicit val t: () => DoubleType = () => DoubleType()
      val a = ParamDef(t())
      val b = ParamDef(t())
      val c = ParamDef(t())
      CLambda(Seq(a, b, c), add(ParamUse(a), add(ParamUse(b), ParamUse(c))))
    }
    tree = DPSPass.apply(tree)
    println(CCodeGen(tree).ccode())
  }

  @Test
  def buildFuse1Test(): Unit = {
    var tree: Expr = Lib.vectorAdd3Opt(100)
    tree = DPSPass(tree)
    println(CCodeGen(tree).ccode())
  }

  @Test
  def add2Test(): Unit = {
    var tree: Expr = {
      val len = 1 << 24
      implicit val t: () => DoubleType = () => DoubleType()
      val a = ParamDef(ArrayType(t(), len))
      val b = ParamDef(ArrayType(t(), len))
      val map = Materialize(Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))), Some(SrcViewType())))
      CLambda(Seq(a, b), map)
    }
    tree = DPSPass.apply(tree)
    IRDotGraph(tree).show()
    val ccode = CCodeGen(tree)
    ccode.genTest("add2")
    println(CCodeGen(tree).ccode())
  }

  @Test
  def add3Test(): Unit = {
    var tree: Expr = Lib.vectorAdd3(1 << 24)
    tree = DPSPass.apply(tree)
    val ccode = CCodeGen(tree, "add3").genTest()
    println(ccode)
  }

  @Test
  def add3TuneTest(): Unit = {
    var tree: Expr = {
      val len = 1 << 24
      implicit val t: () => DoubleType = () => DoubleType()
      val a = ParamDef(ArrayType(t(), len))
      val b = ParamDef(ArrayType(t(), len))
      val c = ParamDef(ArrayType(t(), len))
      val map1 = Mtrl(Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))), Some(SrcViewType())))
      val map2 = Must(Map(Lib.addTup, Zip(Tuple(ParamUse(c), map1)), Some(SrcViewType())))
      CLambda(Seq(a, b, c), map2)
    }
    var trees = AutoTune(tree)
    trees = trees.map(DPSPass(_))
    //    CCodeGen.genCodes("add3", trees)
  }

  @Test
  def add3Tune2Test(): Unit = {
    val tree: Expr = {
      val len = 1 << 24
      implicit val t: () => DoubleType = () => DoubleType()
      val a = ParamDef(ArrayType(t(), len))
      val b = ParamDef(ArrayType(t(), len))
      val c = ParamDef(ArrayType(t(), len))
      val map1 = Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))))
      val map2 = Map(Lib.addTup, Zip(Tuple(ParamUse(c), map1)))
      CLambda(Seq(a, b, c), map2)
    }
    val trees = perfModelTuningPassWithDPS(tree)
    println(trees.length)
    CCode.genCodes(trees.map(CCodeGen(_, "add3")))
  }

  @Test
  def add3RandomTuneTest(): Unit = {
    val tree: Expr = {
      val len = 1 << 24
      implicit val t: () => DoubleType = () => DoubleType()
      val a = ParamDef(ArrayType(t(), len))
      val b = ParamDef(ArrayType(t(), len))
      val c = ParamDef(ArrayType(t(), len))
      val map1 = Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))))
      val map2 = Map(Lib.addTup, Zip(Tuple(ParamUse(c), map1)))
      CLambda(Seq(a, b, c), map2)
    }
    println(getUpperBoundRewriteNum(tree))
    val trees = randomTuningPassWithDPS(tree, 1000, getUpperBoundRewriteNum(tree))
    Isomorphic.deduplicate(trees)
    CCode.genCodes(trees.map(CCodeGen.tryCCodeGen(_, "add3")).filter(_.isDefined).map(_.get).seq)
  }

  @Test
  def add3WorseCaseTest(): Unit = {
    val len = (1 << 24)
    var tree: Expr = {
      implicit val t: () => DoubleType = () => DoubleType()
      val a = ParamDef(ArrayType(t(), len))
      val b = ParamDef(ArrayType(t(), len))
      val c = ParamDef(ArrayType(t(), len))
      val prog = new Program()
      val tuple0 = prog.add(Materialize(Tuple(ParamUse(a), ParamUse(b))))
      val zip0 = prog.add(Materialize(Zip(tuple0)))
      val map0 = prog.add(Map(Lib.addTup, zip0, Some(SrcViewType())))
      val tuple1 = prog.add(Materialize(Tuple(ParamUse(c), map0)))
      val zip1 = prog.add(Materialize(Zip(tuple1)))
      val map1 = prog.build(Map(Lib.addTup, zip1))
      CLambda(Seq(a, b, c), map1)
    }
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree);
    ccode.genTest("add3WorseCase")
    println(CCodeGen(tree).ccode())
  }

  @Test
  def buildFuse3Test(): Unit = {
    val len = 100
    implicit val t: () => IntType = () => IntType(32)
    val a = ParamDef(ArrayType(IntType(32), len))
    val b = ParamDef(ArrayType(IntType(32), len))
    val c = ParamDef(ArrayType(IntType(32), len))
    val map1 = Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))), EagerType())
    val map2 = Map(Lib.addTup, Zip(Tuple(ParamUse(c), map1)), EagerType())
    var tree: Expr = CLambda(Seq(a, b, c), map2)
    tree = DPSPass.apply(tree)
    //    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
    ccode.genTest("add3")
  }

  @Test
  def crossTest(): Unit = {
    val a = ParamDef(ArrayType(DoubleType(), 3))
    val b = ParamDef(ArrayType(DoubleType(), 3))
    val f = CLambda(a, CLambda(b, Cross(ParamUse(a), ParamUse(b))))
    val tree = DPSPass(f)
    val ccode = CCodeGen(tree, "cross").genCode()
    println(ccode)
  }

  val liftStyleCross = {
    val a = ParamDef(ArrayType(DoubleType(), 3))
    val b = ParamDef(ArrayType(DoubleType(), 3))

    val pf0 = {
      val i = ParamDef(IntType(32))
      implicit val t = () => IntType(32)
      CLambda(i, mod(add(ParamUse(i), one), three))
    }

    val pf1 = {
      val i = ParamDef(IntType(32))
      implicit val t = () => IntType(32)
      CLambda(i, mod(add(ParamUse(i), two), three))
    }

    implicit val t = () => DoubleType()
    CLambda(a, CLambda(b,
      VectorSub(
        VectorMul(Permute(pf0, ParamUse(a)), Permute(pf1, ParamUse(b)), SrcViewType()),
        VectorMul(Permute(pf1, ParamUse(a)), Permute(pf0, ParamUse(b)), SrcViewType())
        , EagerType())
    ))
  }

  @Test
  def liftStyleCrossTest(): Unit = {
    var tree: Expr = liftStyleCross
    tree = DPSPass.apply(tree)
    CCodeGen(tree, "cross").genCode()
  }

  @Test
  def liftStyleCrossPerfTuneTest(): Unit = {
    var tree: Expr = liftStyleCross
    val trees = perfModelTuningPassWithDPS(tree)
    CCode.genCodes(trees.map(CCodeGen(_, "cross")))
  }

  @Test
  def liftStyleCrossRandomTuneTest(): Unit = {
    var tree: Expr = liftStyleCross
    println(getUpperBoundRewriteNum(tree))
    val trees = randomTuningPassWithDPS(tree, 1000, getUpperBoundRewriteNum(tree))
    Isomorphic.deduplicate(trees)
    CCode.genCodes(trees.map(CCodeGen.tryCCodeGen(_, "cross")).filter(_.isDefined).map(_.get).seq)
  }

  @Test
  def liftStyleCrossTuneTest(): Unit = {
    val a = ParamDef(ArrayType(DoubleType(), 3))
    val b = ParamDef(ArrayType(DoubleType(), 3))

    val pf0 = {
      val i = ParamDef(IntType(32))
      implicit val t = () => IntType(32)
      CLambda(i, mod(add(ParamUse(i), one), three))
    }

    val pf1 = {
      val i = ParamDef(IntType(32))
      implicit val t = () => IntType(32)
      CLambda(i, mod(add(ParamUse(i), two), three))
    }

    implicit val t = () => DoubleType()
    val f = CLambda(a, CLambda(b,
      Must(VectorSub(
        Mtrl(VectorMul(Permute(pf0, ParamUse(a)), Permute(pf1, ParamUse(b)), SrcViewType())),
        Mtrl(VectorMul(Permute(pf1, ParamUse(a)), Permute(pf0, ParamUse(b)), SrcViewType()))
        , EagerType()))
    ))
    val trees = AutoTune(f).map(DPSPass(_))
    //    CCodeGen.genCodes("cross", trees)
    print(trees.length)
  }

  @Test
  def liftStyleCrossWorseCaseTest(): Unit = {
    val a = ParamDef(ArrayType(DoubleType(), 3))
    val b = ParamDef(ArrayType(DoubleType(), 3))

    val pf0 = {
      val i = ParamDef(IntType(32))
      implicit val t = () => IntType(32)
      CLambda(i, mod(add(ParamUse(i), one), three))
    }

    val pf1 = {
      val i = ParamDef(IntType(32))
      implicit val t = () => IntType(32)
      CLambda(i, mod(add(ParamUse(i), two), three))
    }

    implicit val t = () => DoubleType()
    val f = CLambda(a, CLambda(b, {
      val prog = new Program()
      val mul0 = prog << VectorMulWorseCase(Permute(pf0, ParamUse(a)), Permute(pf1, ParamUse(b)))
      val mul1 = prog << VectorMulWorseCase(Permute(pf1, ParamUse(a)), Permute(pf0, ParamUse(b)))
      prog.build(VectorSubWorseCase(mul0, mul1))
    }
    ))
    val tree = DPSPass(f)
    val ccode = CCodeGen(tree, "cross_worse_case")
    ccode.genTest()
    println(ccode.ccode())
  }

  @Test
  def toArrayTest(): Unit = {
    val a = ParamDef(ArrayType(IntType(32), 3))
    val aInt = Constant(1, IntType(32))
    val f = CLambda(Seq(a), ConcatFactory(RepeatOnce(aInt), RepeatOnce(aInt)))

    var tree: Expr = f
    tree = TypeChecker.check(tree)
    tree = DPSPass(tree)
    println(CCodeGen(tree).ccode())
  }

  @Test
  def vectorSumTest(): Unit = {
    implicit val t: () => Type = () => DoubleType()
    val v = ParamDef(ArrayType(DoubleType(), 12))
    var tree: Expr = CLambda(v, Reduce(add2, BinaryOp(zero, zero, AlgoOp.Add), ParamUse(v)))
    tree = DPSPass.apply(tree)
    println(CCodeGen(tree))
    //    println(CCodeGen(tree).ccode())
  }

  @Test
  def sqNormTest(): Unit = {
    implicit val t: () => CGenDataTypeT = () => DoubleType()
    val v = ParamDef(ArrayType(DoubleType(), 4))
    var tree: Expr = CLambda(v, SqNorm(ParamUse(v)))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("sqnorm")
    println(ccode)
  }

  @Test
  def slideTest(): Unit = {
    val x = ParamDef(ArrayType(DoubleType(), 12))
    val arr = Slide(ParamUse(x), 3)
    var tree: Expr = CLambda(x, Materialize(arr))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree).ccode()
    println(ccode)
  }

  @Test
  def radialDistortTest(): Unit = {
    val radParams = ParamDef(ArrayType(DoubleType(), 3))
    val proj = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(radParams, CLambda(proj, RadialDistort(ParamUse(radParams), ParamUse(proj))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, "radial_distort").ccode()
    FileWriter("src/test/cGen/C/radial-distort.h", ccode)
    println(ccode)
  }

  @Test
  def rodriguesRotatePointTest(): Unit = {
    val rot = ParamDef(ArrayType(DoubleType(), 3))
    val x = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(rot, CLambda(x, RodriguesRotatePoint(ParamUse(rot), ParamUse(x))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, "rodrigues_rotate_point").ccode()
    println(ccode)
    FileWriter("src/test/cGen/C/rodrigues-rotate-point.h", ccode)
  }

  @Test
  def projectTest(): Unit = {
    val cam = ParamDef(ArrayType(DoubleType(), 11))
    val x = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(cam, CLambda(x, Project(ParamUse(cam), ParamUse(x))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, "project")
    ccode.genTest()
  }

  @Test
  def projectWorseCaseTest(): Unit = {
    val cam = ParamDef(ArrayType(DoubleType(), 11))
    val x = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(cam, CLambda(x, ProjectWorseCase(ParamUse(cam), ParamUse(x))))
    tree = DPSPass(tree)
    val cgen = CCodeGen(tree)
    cgen.genTest("project_worse_case")
  }

  @Test
  def projectAndSqNormTest(): Unit = {
    implicit val t: () => DoubleType = () => DoubleType()
    val cam = ParamDef(ArrayType(DoubleType(), 11))
    val x = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(cam, CLambda(x, ProjectAndSqNorm(ParamUse(cam), ParamUse(x))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, "project_and_sqnorm").ccode()
    FileWriter("src/test/cGen/C/project-and-sqnorm.h", ccode)
  }

  val ba: Expr = {
    val a = ParamDef(ArrayType(DoubleType(), 3))
    val b = ParamDef(ArrayType(DoubleType(), 3))
    val cam = ParamDef(ArrayType(DoubleType(), 11))
    CLambda(a, CLambda(b, CLambda(cam, BundleAdjustment(ParamUse(a), ParamUse(b), ParamUse(cam)))))
  }

  @Test
  def baTest(): Unit = {
    var tree = ba
    tree = DPSPass(tree)
    CCodeGen(tree, "ba").genCode()
  }

  @Test
  def baTuneTest(): Unit = {
    val trees = perfModelTuningPassWithDPS(ba)
    CCode.genCodes(trees.map(CCodeGen(_, "ba")))
  }

  @Test
  def baRandomTest(): Unit = {
    println(getUpperBoundRewriteNum(ba))
    val partrees = randomTuningPassWithDPS(ba, 100, getUpperBoundRewriteNum(ba))
    var trees = Isomorphic.deduplicate(partrees)
    CCode.genCodes(trees.map(CCodeGen.tryCCodeGen(_, "ba_random")).filter(_.isDefined).map(_.get).seq)
    //    CCode.genCodes(trees.map(CCodeGen(_, "ba")))
  }


  @Test
  def ifTest(): Unit = {
    val a = ParamDef(DoubleType())
    val b = ParamDef(DoubleType())
    var tree: Expr = CLambda(a, CLambda(b, If(gt(ParamUse(a), ParamUse(b)), Id(ParamUse(a)), Id(ParamUse(b)))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, "_if").ccode()
    FileWriter("src/test/cGen/C/if.h", ccode)
    println(ccode)
  }

  @Test
  def vectorMaxTest(): Unit = {
    val v = ParamDef(ArrayType(DoubleType(), 5))
    var tree: Expr = CLambda(v, VectorMax(ParamUse(v))(() => DoubleType()))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, "vector_max").ccode()
    FileWriter("src/test/cGen/C/vector-max.h", ccode)
    println(ccode)
  }

  @Test
  def logsumexpTest(): Unit = {
    val v = ParamDef(ArrayType(DoubleType(), 5))
    var tree: Expr = CLambda(v, LogSumExp(ParamUse(v))(() => DoubleType()))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("logsumexp")
    println(ccode)
  }

  @Test
  def dzipTest(): Unit = {
    val v = ParamDef(ArrayType(DoubleType(), 5))
    var tree: Expr = CLambda(v, Zip(Tuple(Materialize(ParamUse(v)), Materialize(ParamUse(v)), DestViewType()), DestViewType()))
    tree = DPSPass(tree)
    println(CCodeGen(tree))
  }

  @Test
  def matrixMulTest(): Unit = {
    implicit val t = () => DoubleType()
    val m1 = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    val m2 = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    var tree: Expr = CLambda(m1, CLambda(m2, MatrixMul(ParamUse(m1), ParamUse(m2))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("matrix_mul")
    println(ccode)
  }


  @Test
  def qtimesvTest(): Unit = {
    val d = 3;
    val td = ((d) * ((d) + (1))) / (2)
    val q = ParamDef(ArrayType(DoubleType(), d))
    val l = ParamDef(ArrayType(DoubleType(), td))
    val v = ParamDef(ArrayType(DoubleType(), d))
    var tree: Expr = CLambda(q, CLambda(l, CLambda(v, Qtimesv(ParamUse(q), ParamUse(l), ParamUse(v)))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("qtimesv")
    println(ccode)
  }

  @Test
  def GMMQitmesvIncludeTest(): Unit = {
    implicit val t = () => DoubleType()
    val n = 1 << 18;
    val d = 3;
    val K = 5;
    val td = ((d) * ((d) + (1))) / (2)
    val x = ParamDef(ArrayType(ArrayType(t(), d), n))
    val alphas = ParamDef(ArrayType(t(), K))
    val means = ParamDef(ArrayType(ArrayType(t(), d), K))
    val qs = ParamDef(ArrayType(ArrayType(t(), d), K))
    val ls = ParamDef(ArrayType(ArrayType(t(), td), K))
    val wishartGamma = ParamDef(t())
    val wishartM = ParamDef(t())
    var tree: Expr = CLambda(x, CLambda(alphas, CLambda(means, CLambda(qs, CLambda(ls, CLambda(wishartGamma, CLambda(wishartM,
      GMMAllView2(n, d, K, ParamUse(x), ParamUse(alphas), ParamUse(means), ParamUse(qs), ParamUse(ls), ParamUse(wishartGamma), ParamUse(wishartM)))))))))
    tree = DPSPass.apply(tree)
    val ccode = CCodeGen(tree, "gmm", moreIncludes = "#include \"qtimesv.h\"\n").genTest()
    //    val ccode = CCodeGen(tree, moreInclude = "#include \"qtimesv.h\"\n")
    //    ccode.genTest("gmm")
    println(ccode)
    //       printf("%d %d\n", var129, var136);
  }

  val gmm: Expr = {
    implicit val t = () => DoubleType()
    val n = 1 << 18;
    val d = 3;
    val K = 5;
    val td = ((d) * ((d) + (1))) / (2)
    val x = ParamDef(ArrayType(ArrayType(t(), d), n))
    val alphas = ParamDef(ArrayType(t(), K))
    val means = ParamDef(ArrayType(ArrayType(t(), d), K))
    val qs = ParamDef(ArrayType(ArrayType(t(), d), K))
    val ls = ParamDef(ArrayType(ArrayType(t(), td), K))
    val wishartGamma = ParamDef(t())
    val wishartM = ParamDef(t())
    CLambda(x, CLambda(alphas, CLambda(means, CLambda(qs, CLambda(ls, CLambda(wishartGamma, CLambda(wishartM,
      GMM(n, d, K, ParamUse(x), ParamUse(alphas), ParamUse(means), ParamUse(qs), ParamUse(ls), ParamUse(wishartGamma), ParamUse(wishartM)))))))))
  }

  @Test
  def GMMTest(): Unit = {
    val tree = DPSPass.apply(gmm)
    val ccode = CCodeGen(tree, "gmm", moreIncludes = "#include \"qtimesv.h\"\n").genTest()
    println(ccode)
  }

  @Test
  def GMMTune2Test(): Unit = {
    val trees = perfModelTuningPassWithDPS(gmm, 1)
    println(trees.length)
    CCode.genCodes(trees.map(CCodeGen(_, "gmm", moreIncludes = "#include \"qtimesv.h\"\n")))
  }

  @Test
  def GMMRandomTuneTest(): Unit = {
    println(getUpperBoundRewriteNum(gmm))
    val partrees = randomTuningPassWithDPS(gmm, 1000, getUpperBoundRewriteNum(gmm))
    var trees = Isomorphic.deduplicate(partrees)
    CCode.genCodes(trees.map(CCodeGen.tryCCodeGen(_, "gmm", moreIncludes = "#include \"qtimesv.h\"\n")).filter(_.isDefined).map(_.get).seq)
  }

  @Test
  def GMMAllViewTest(): Unit = {
    implicit val t = () => DoubleType()
    val n = 100;
    val d = 3;
    val K = 5;
    val td = ((d) * ((d) + (1))) / (2)
    val x = ParamDef(ArrayType(ArrayType(t(), d), n))
    val alphas = ParamDef(ArrayType(t(), K))
    val means = ParamDef(ArrayType(ArrayType(t(), d), K))
    val qs = ParamDef(ArrayType(ArrayType(t(), d), K))
    val ls = ParamDef(ArrayType(ArrayType(t(), td), K))
    val wishartGamma = ParamDef(t())
    val wishartM = ParamDef(t())
    var tree: Expr = CLambda(x, CLambda(alphas, CLambda(means, CLambda(qs, CLambda(ls, CLambda(wishartGamma, CLambda(wishartM,
      GMMAllView(ParamUse(x), ParamUse(alphas), ParamUse(means), ParamUse(qs), ParamUse(ls), ParamUse(wishartGamma), ParamUse(wishartM)))))))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, moreIncludes = "#include \"qtimesv.h\"\n")
    ccode.genTest("gmm_all_view")
    println(ccode)
  }

  @Test
  def GMMWorseCaseTest(): Unit = {
    implicit val t = () => DoubleType()
    val n = 1 << 18;
    val d = 3;
    val K = 5;
    val td = ((d) * ((d) + (1))) / (2)
    val x = ParamDef(ArrayType(ArrayType(t(), d), n))
    val alphas = ParamDef(ArrayType(t(), K))
    val means = ParamDef(ArrayType(ArrayType(t(), d), K))
    val qs = ParamDef(ArrayType(ArrayType(t(), d), K))
    val ls = ParamDef(ArrayType(ArrayType(t(), td), K))
    val wishartGamma = ParamDef(t())
    val wishartM = ParamDef(t())
    var tree: Expr = CLambda(x, CLambda(alphas, CLambda(means, CLambda(qs, CLambda(ls, CLambda(wishartGamma, CLambda(wishartM,
      GMMWorseCase(ParamUse(x), ParamUse(alphas), ParamUse(means), ParamUse(qs), ParamUse(ls), ParamUse(wishartGamma), ParamUse(wishartM)))))))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, moreIncludes = "#include \"qtimesv.h\"\n")
    ccode.genTest("gmm_worse_case")
    println(ccode)
  }

  @Test
  def crazyConcatPushTest(): Unit = {
    implicit val t = () => DoubleType()
    val r0 = ConcatFactory(ConcatFactory(RepeatOnce(one), RepeatOnce(zero)), RepeatOnce(zero))
    val r1 = ConcatFactory(ConcatFactory(RepeatOnce(one), RepeatOnce(zero)), RepeatOnce(zero))
    val r2 = ConcatFactory(ConcatFactory(RepeatOnce(one), RepeatOnce(zero)), RepeatOnce(zero))
    val a = ParamDef(t())
    var tree: Expr = CLambda(a, ConcatFactory(ConcatFactory(RepeatOnce(r0), RepeatOnce(r1)), RepeatOnce(r2)))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
  }

  @Test
  def angleAxisToRotationMatrixTest(): Unit = {
    val angleAxis = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(angleAxis, AngleAxisToRotationMatrix(ParamUse(angleAxis)))
    println(getUpperBoundRewriteNum(tree))
    val partrees = randomTuningPassWithDPS(tree, 100, getUpperBoundRewriteNum(tree))
    var trees = Isomorphic.deduplicate(partrees)
    CCode.genCodes(trees.map(CCodeGen.tryCCodeGen(_, "ht_random")).filter(_.isDefined).map(_.get).seq)
  }

  @Test
  def angleAxisToRotationMatrixWorseCaseTest(): Unit = {
    val angleAxis = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(angleAxis, AngleAxisToRotationMatrixWorseCase(ParamUse(angleAxis)))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("angle_axis_to_rotation_matrix_worse_case")
    println(ccode)
  }

  @Test
  def angleAxisToRotationMatrixRandamTest(): Unit = {
    val angleAxis = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(angleAxis, AngleAxisToRotationMatrix(ParamUse(angleAxis)))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, "angle_axis_to_rotation_matrix")
    ccode.genTest("angle_axis_to_rotation_matrix")
    println(ccode)
  }


  @Test
  def eulerAnglesToRotationMatrixTest(): Unit = {
    val eulerAngles = ParamDef(ArrayType(DoubleType(), 3))
    var tree: Expr = CLambda(eulerAngles, EulerAnglesToRotationMatrix(ParamUse(eulerAngles)))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("angle_axis_rotation_matrix")
    println(ccode)
  }

  @Test
  def makeRelativeTest(): Unit = {
    val poseParam = ParamDef(ArrayType(DoubleType(), 3))
    val baseRelative = ParamDef(ArrayType(ArrayType(DoubleType(), 4), 4))
    var tree: Expr = CLambda(poseParam, CLambda(baseRelative, MakeRelative(ParamUse(poseParam), ParamUse(baseRelative))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("make_relative")
    println(ccode)
  }

  @Test
  def applyGlobalTransformTest(): Unit = {
    val poseParams = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    val positions = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    var tree: Expr = CLambda(poseParams, CLambda(positions, ApplyGlobalTransform(ParamUse(poseParams), ParamUse(positions))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    ccode.genTest("apply_global_transform")
    println(ccode)
  }

  @Test
  def getPosedRelativesTest(): Unit = {
    val tree = DPSPass(GetPosedRelativesFunction(4))
    val ccode = CCodeGen(tree)
    ccode.genTest("get_posed_relatives")
    println(ccode)
  }

  @Test
  def matrix3DUpdate(): Unit = {
    val m = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    val nm = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    var tree: Expr = CLambda(m, CLambda(nm, Matrix3DUpdate(ParamUse(m), one(() => IntType(32)), two(() => IntType(32)), ParamUse(nm))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
  }

  @Test
  def relativeToAbsoluteTest(): Unit = {
    val relatives = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
    val parents = ParamDef(ArrayType(DoubleType(), 4))
    var tree: Expr = CLambda(relatives, CLambda(parents, RelativeToAbsolute(ParamUse(relatives), ParamUse(parents))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
  }

  @Test
  def getSkinnedVertexPositionsTest(): Unit = {
    val isMirror = ParamDef(ArrayType(DoubleType(), 3))
    val nBones = 4
    val poseParams = ParamDef(ArrayType(ArrayType(DoubleType(), 4), 6))
    val baseRelatives = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
    val inverseBaseRelatives = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
    val basePositions = ParamDef(ArrayType(ArrayType(DoubleType(), nBones), 3))
    val basePositionsHomg = ParamDef(ArrayType(ArrayType(DoubleType(), 3), nBones))
    val parents = ParamDef(ArrayType(DoubleType(), 4))
    val weights = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
    var tree: Expr = CLambda(isMirror, CLambda(poseParams, CLambda(baseRelatives, CLambda(parents, CLambda(inverseBaseRelatives, CLambda(basePositions, CLambda(basePositionsHomg, CLambda(weights,
      GetSkinnedVertexPositions(ParamUse(isMirror), nBones, ParamUse(poseParams), ParamUse(baseRelatives), ParamUse(parents), ParamUse(inverseBaseRelatives), ParamUse(basePositions), ParamUse(basePositionsHomg), ParamUse(weights))))))))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, moreIncludes = "#include \"get_posed_relatives.h\"\n")
    ccode.genTest("get_skinned_vertex_position")
    println(ccode)
  }

  @Test
  def arrayAccessLiftTest(): Unit = {
    val a = ParamDef(ArrayType(DoubleType(), 4))
    var tree: Expr = CLambda(a, Split(Map({
      val x = ParamDef(DoubleType())
      CLambda(x, Id(ParamUse(x)))
    }, ParamUse(a)), 2))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
  }

  @Test
  def blurForwardTest(): Unit = {
    val rows = 1 << 13
    val cols = 1 << 13
    val tree = DPSPass(blurForward(rows, cols))
    val ccode = CCodeGen(tree)
    ccode.genTest("blur_forward")
    println(ccode)
  }

  @Test
  def blurBackwardTest(): Unit = {
    val rows = 1 << 13
    val cols = 1 << 13
    val tree = DPSPass(blurBackward(rows, cols))
    val ccode = CCodeGen(tree)
    ccode.genTest("blur_backward")
    println(ccode)
  }

  @Test
  def blurTest(): Unit = {
    val rows = 1 << 13
    val cols = 1 << 13
    var tree = DPSPass(blurBackward(rows, cols))
    var ccode = CCodeGen(tree)
    ccode.genTest("blur_backward")

    tree = DPSPass(blurForward(rows, cols))
    ccode = CCodeGen(tree)
    ccode.genTest("blur_forward")
  }

  @Test
  def fftTest(): Unit = {
    val N = 1024;
    val tree = DPSPass(fft(N))
    var ccode = CCodeGen(tree, "#include \"reverse_bits.h\"\n")
    ccode.genTest("fft")
  }

  @Test
  def fftShortVersionTest(): Unit = {
    val N = 1 << 10;
    val tree = DPSPass(fftShortVersion(N))
    var ccode = CCodeGen(tree, "#include \"reverse_bits.h\"\n")
    ccode.genTest("fft_short_version")
  }

  @Test
  def fftShortVersionWorseCaseTest(): Unit = {
    val N = 1 << 10;
    val tree = DPSPass(fftShortVersionWorseCase(N))
    var ccode = CCodeGen(tree, "#include \"reverse_bits.h\"\n")
    ccode.genTest("fft_short_version_worse_case")
  }

  @Test
  def BufferedSlideTest(): Unit = {
    val program = {
      implicit val t = () => DoubleType()
      val in = ParamDef(ArrayType(DoubleType(), 1 << 26))
      val a = ParamDef(ArrayType(DoubleType(), 3))
      val m = Map(CLambda(a, div(VectorSum(ParamUse(a)), num(3))), Slide(ParamUse(in), 3))
      CLambda(in, m)
    }
    val tree = DPSPass(program)
    val ccode = CCodeGen(tree)
    ccode.genTest("buffered_slide")
    println(ccode)
  }

  @Test
  def Buffered2DSlideTest(): Unit = {
    val program = {
      implicit val t = () => DoubleType()
      val len = 1 << 14
      val size = 3
      val A = ParamDef(ArrayType(ArrayType(t(), len), len))
      val slide2 = Map({
        val p = ParamDef(ArrayType(ArrayType(ArrayType(t(), size), len - 2), size))
        CLambda(p, transpose(ParamUse(p)))
      },
        Slide(
          Map({
            val window = ParamDef(ArrayType(t(), len))
            CLambda(window, Slide(ParamUse(window), size))
          }, ParamUse(A)),
          size)
      )
      CLambda(A, {
        val c =
          Map({
            val p1 = ParamDef(ArrayType(ArrayType(ArrayType(t(), size), size), len - 2))
            CLambda(p1, Map({
              val p2 = ParamDef(ArrayType(ArrayType(t(), size), size))
              CLambda(p2, div(MatrixSum(ParamUse(p2)), nine))
            }, ParamUse(p1)))
          }, slide2)
        val a = tt(c)
        print(a)
        c
      })
    }

    val tree = DPSPass(program)
    val ccode = CCodeGen(tree)
    ccode.genTest("buffered_slide_2d")
    println(ccode)
  }

  @Test
  def CommonSubexprsElimitation(): Unit = {
    def CSEPass(expr: Expr) = {
      val CSETable = mutable.Set[Expr]()
      expr.visit({
        case e: CGenExpr if !e.isInstanceOf[CFCExpr] => // at this point, there should not be any non-let function call
          if (CSETable.contains(e)) println(s"HIHI $e")
          CSETable.add(e)
        case _ =>
      })
      println(CSETable)
    }

    val A = ParamDef(ArrayType(DoubleType(), 8))
    val in = ParamDef(DoubleType())
    implicit val t = () => DoubleType()
    val program = CLambda(A, {
      val common = Map(CLambda(in, sqrt(ParamUse(in))), ParamUse(A), Some(EagerType()))
      VectorAdd(common, common)
    })
    CSEPass(program)
    val tree = DPSPass(program)
    val ccode = CCodeGen(tree)
    //    println(ccode)
  }

  // This test is for the transformation for mapD
  @Test
  def LazyMap(): Unit = {
    def occurCountPass(expr: Expr, target: Expr): Int = {
      var counter = 0
      expr.visit({
        case e if e == target => counter += 1
        case _ =>
      })
      return counter
    }

    def pass2(expr: Expr, target: Expr, to: mutable.Queue[Expr]): Expr = {
      expr.visitAndRebuild({
        case e if e == target => to.dequeue()
        case other => other
      }).asInstanceOf[Expr]
    }

    def pass(expr: Expr): Expr = {
      expr.visitAndRebuild({
        case map@Map(CLambda(p, b, _), in, _, _) if occurCountPass(b, ParamUse(p)) == 2 =>
          // in is not a srcview since Map should be eager or dest
          val pt = tt(p)
          val newP = ParamDef(TupleType(pt.asInstanceOf[CGenDataTypeT], pt.asInstanceOf[CGenDataTypeT]))
          val tmp = ParamDef(tt(in))
          CGenLet(tmp, in) apply
            Map(CLambda(newP, pass2(b, ParamUse(p), mutable.Queue(SelectS(ParamUse(newP), 0), SelectS(ParamUse(newP), 1)))),
              Zip(Tuple(ParamUse(tmp), ParamUse(tmp))))
        case other => other
      }).asInstanceOf[Expr]
    }

    val A = ParamDef(ArrayType(DoubleType(), 8))
    implicit val t = () => DoubleType()
    val in = ParamDef(DoubleType())
    var tree: Expr = CLambda(A, {
      val src = Map(CLambda(in, sqrt(ParamUse(in))), ParamUse(A), Some(EagerType()))
      Map(CLambda(in, Tuple(Id(ParamUse(in)), Id(ParamUse(in)))), src, Some(EagerType()))
    })

    tree = pass(tree)
    IRDotGraph(tree).show()
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    println(ccode)
  }

  @Test
  def autoTunerTest() = {
    // example for vector add3
    val len = 8
    implicit val t: () => DoubleType = () => DoubleType()
    val a = ParamDef(ArrayType(t(), len))
    val b = ParamDef(ArrayType(t(), len))
    val c = ParamDef(ArrayType(t(), len))
    val map1 = Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))), Some(SrcViewType()))
    val map2 = Materialize(Map(Lib.addTup, Zip(Tuple(ParamUse(c), map1)), Some(SrcViewType())))
    var tree: Expr = CLambda(Seq(a, b, c), map2)

    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    IRDotGraph(tree).show()
  }

  @Test
  def ifSrcTest(): Unit = {
    val len = 8
    implicit val t: () => DoubleType = () => DoubleType()
    val in = ParamDef(ArrayType(t(), 5))
    val idx = ParamDef(IntType(32))
    var tree: Expr = CLambda(in,
      Materialize(Build(CLambda(idx, If(le(ParamUse(idx), zero(() => IntType(32))), five, IdxS(ParamUse(in), ParamUse(idx)), SrcViewType())),
        len, SrcViewType())))
    tree = DPSPass(tree)
    print(CCodeGen(tree))
  }

  @Test
  def fstDestTest(): Unit = {
    val a = ParamDef(IntType(32))
    val b = ParamDef(IntType(32))
    var tree: Expr = CLambda(a, CLambda(b, Fst(Tuple(Id(ParamUse(a)), Id(ParamUse(b)), DestViewType()), DestViewType())))
    tree = DPSPass(tree)
    println(CCodeGen(tree))
  }

  @Test
  def idxDestTest(): Unit = {
    val a = ParamDef(ArrayType(IntType(32), 8))
    var tree: Expr = CLambda(a, Idx(Materialize(ParamUse(a)), Constant(4, IntType(32)), DestViewType()))
    tree = DPSPass(tree)
    println(CCodeGen(tree))
  }

  @Test
  def concatIdxDestTest(): Unit = {
    val a = ParamDef(ArrayType(IntType(32), 8))
    var tree: Expr = CLambda(a,
      Concat(Tuple(
        Concat(Tuple(
          RepeatOnce(Idx(Materialize(ParamUse(a)), Constant(0, IntType(32)), DestViewType())),
          Materialize(ParamUse(a)), DestViewType()), DestViewType()),
        RepeatOnce(Idx(Materialize(ParamUse(a)), Constant(7, IntType(32)), DestViewType())), DestViewType()), DestViewType())
    )

    val trees = perfModelTuningPassWithDPS(tree, 5)
    val codes = trees.map(CCodeGen(_, "concatIdxDest"))
    codes.foreach(println(_))
  }

  @Test
  def ifExprSrcTest(): Unit = {
    val a = ParamDef(IntType(32))
    val b = ParamDef(IntType(32))
    var tree: Expr = CLambda(a, CLambda(b, Materialize(If(le(ParamUse(a), ParamUse(b)), ParamUse(a), ParamUse(b), SrcViewType()))))
    tree = DPSPass(tree)
    println(CCodeGen(tree))
  }

  @Test
  def rollingSlide1DTest(): Unit = {
    implicit val t = () => DoubleType()
    val len = 1 << 24
    val in = ParamDef(ArrayType(t(), len))
    val w = ParamDef(ArrayType(t(), 3))
    var tree: Expr = CLambda(in, Map(conv1, RollingSlide(ParamUse(in), 3, SrcViewType()), EagerType()))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree, "rolling1d").genCode()
    print(ccode)
  }

  @Test
  def unrepeatTest(): Unit = {
    //    val a = ParamDef(IntType(32))
    val a = ParamDef(ArrayType(ArrayType(IntType(32), 8), 8))
    var tree: Expr = CLambda(a, Repeat(Materialize(ParamUse(a)), 8, DestViewType()))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    print(ccode)
  }

  @Test
  def unrepeatUnidxTest(): Unit = {
    val a = ParamDef(ArrayType(ArrayType(IntType(32), 8), 8))
    var tree: Expr = CLambda(a, Repeat(Idx(Materialize(ParamUse(a)), Constant(5, IntType(32)), DestViewType()), 1, DestViewType()))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    print(ccode)
  }

  @Test
  def transposeUnrepeatUnidxTest(): Unit = {
    val a = ParamDef(ArrayType(ArrayType(IntType(32), 8), 8))
    var tree: Expr = CLambda(a,
      Transpose(Repeat(Idx(Materialize(ParamUse(a)), Constant(0, IntType(32)), DestViewType()), 1, DestViewType()), DestViewType())
    )
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    print(ccode)
  }

  def padD(mat: Expr) = {
    val concat0 = Concat(
      Tuple(Repeat(Idx(mat, Constant(5, IntType(32)), DestViewType()), 1, DestViewType()),
        mat, DestViewType()),
      DestViewType())
    Concat(Tuple(concat0, Repeat(Idx(mat, Constant(6, IntType(32)), DestViewType()), 1, DestViewType()), DestViewType()), DestViewType())
  }

  @Test
  def padDTest(): Unit = {
    val a = ParamDef(ArrayType(ArrayType(IntType(32), 8), 8))
    var tree: Expr = CLambda(a, padD(Materialize(ParamUse(a))))
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    print(ccode)
  }

  @Test
  def pad2DTest(): Unit = {
    val a = ParamDef(ArrayType(ArrayType(IntType(32), 8), 8))
    var tree: Expr = CLambda(a, {
      val p = ParamDef(ArrayType(IntType(32), 8))
      padD(MapE(CLambda(p, padD(Materialize(ParamUse(p)))), ParamUse(a)))
    })
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    print(ccode)
  }

  @Test
  def jacobi2DwithPadDTest(): Unit = {
    implicit val t = () => DoubleType()
    val a = ParamDef(ArrayType(ArrayType(t(), 8), 8))
    var tree: Expr = CLambda(a, {
      val slide = slideS2D(ParamUse(a), 3)
      val p = ParamDef(tt(slide).asInstanceOf[ArrayTypeT].et)
      val mape = padD(MapE(CLambda(p, padD(MapE(conv2, ParamUse(p)))), slide))
      mape
    })
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    print(ccode)

  }

  @Test
  def padSTest(): Unit = {
    implicit val t = () => DoubleType()
    val a = ParamDef(ArrayType(ArrayType(t(), 8), 8))
    var tree: Expr = CLambda(a, {
      val p = ParamDef(ArrayType(t(), 8))
      val pad2d = padS(MapS(CLambda(p, padS(ParamUse(p))), ParamUse(a)))
      val slided = slideS2D(pad2d, 3)
      Materialize(slided)
      //      map2DE(conv2, slided)
    })
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    print(ccode)
  }

  @Test
  def padS2Test(): Unit = {
    implicit val t = () => DoubleType()
    val a = ParamDef(ArrayType(ArrayType(t(), 8), 8))
    var tree: Expr = CLambda(a, {
      val p = ParamDef(ArrayType(t(), 8))
      val pad2d = Transpose(padS(Transpose(padS(ParamUse(a)), SrcViewType())), SrcViewType())
      val slided = slideS2D(pad2d, 3)
      Materialize(slided)
      //      map2DE(conv2, slided)
    })
    tree = DPSPass(tree)
    val ccode = CCodeGen(tree)
    print(ccode)
  }
}
