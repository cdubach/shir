package tags

/**
 * A test category for essential tests that execute quickly. These tests are designed to run during every CI build.
 * To mark a test as an essential test, annotate it or its containing test class with `@Category(Array(classOf[EssentialTests]))`.
 * To run only the essential tests, invoke sbt as follows: `sbt 'testOnly -- --include-categories=tags.EssentialTests'`.
 */
trait EssentialTests

/**
 * A test category for additional tests that execute slowly. These tests are not designed to be run during every CI build.
 * To mark a test as an extra test, annotate it or its containing test class with `@Category(Array(classOf[ExtraTests]))`.
 * To run only the extra tests, invoke sbt as follows: `sbt 'testOnly -- --include-categories=tags.ExtraTests'`.
 */
trait ExtraTests
