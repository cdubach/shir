package algo.vgg8bits

import algo.layerTemplates.ConvDataGen.multiConvGen
import algo.nn.templates.{ConvEngineFunTemplate, ConvLayerTemplate, DotProdFunTemplate, DoubleBufferingFunTemplate, FCLayerTemplate, OuterWrapperFunTemplate}
import backend.hdl.HDLProject
import backend.hdl.arch.ArchCompiler
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.MemFunctionsCompiler
import backend.hdl.arch.rewrite.{FixTimingRules, InputBufferingRules, MapAndFunctionRules, ParallelizeBufferRules, ParallelizeDotProductRules, TransposeCounterRules, WorkAroundTimingRules}
import backend.hdl.arch.rewrite.sharedFunc.{CacheFunRules, FuncBufferingRules, FuncTimingRules, SynchronizeBitWidthRules}
import backend.hdl.arch.sharedFunc.{GlobalFuncCompiler, MergeFuncCompiler}
import core.{Counter, Let, ParamDef, TypeChecker}
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted, Rules}
import core.util.IRDotGraph
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

class VggConv8NoBiasTest {
  @Test
  def testConv(): Unit = {
    Counter.resetAll()
    Rules.disabled = Seq(
      "exchangeABMapWithWeight",
      "doubleBufferOutputMapStmToVec",
      "convertTranspose2DIntoPermuteVec",
      "limitedConvertTransposeNDIntoPermute",
      "convertTranspose2DIntoPermuteVec",
      "exchangeABMapWithSize",
      "bufferRepeatParam",
      "expandPseudoTransposeND",
    )

    val tileSize = 8
    val bufSize = 50
    val ich = 64
    val och = 64
    val size = 32

    val dpFun = DotProdFunTemplate.createDotProdFun(64, 8)
    val dpFunParam = ParamDef(dpFun.t)
    val convFun = ConvEngineFunTemplate.createConvFunEngineV1(dpFunParam, 64, 9, 4, 64, 8)
    val convFunParam = ParamDef(convFun.t)

    val inputBuffer = DoubleBufferingFunTemplate.createSlidingBuffer(tileSize, tileSize)
    val inputBufferParam = ParamDef(inputBuffer.t)

    val weightBuffer = DoubleBufferingFunTemplate.createRepeatingBuffer(tileSize, tileSize)
    val weightBufferParam = ParamDef(weightBuffer.t)

    val convWrapperFun = OuterWrapperFunTemplate.createConvWrapper(convFunParam, inputBufferParam, weightBufferParam, tileSize, tileSize)
    val convWrapperFunParam = ParamDef(convWrapperFun.t)

    val test = Let(
      dpFunParam,
      Let(
        convFunParam,
        Let(
          inputBufferParam,
          Let(
            weightBufferParam,
            Let(
              convWrapperFunParam,
              {
                val conv8_3 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, size, size, ich, och, tileSize, tileSize, pool = true, id = 0)
                conv8_3
              },
              convWrapperFun
            ),
            weightBuffer
          ),
          inputBuffer
        ),
        convFun
      ),
      dpFun
    )

    HDLProject(this.getClass.getSimpleName + "t" + tileSize + "s" + size + "x" +  ich + "x" + och, test, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeBufferRules.get())),

      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), SynchronizeBitWidthRules.get(Some(17)))),
      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), FuncBufferingRules.get())),
      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.get(Some(64)))),
      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(ParallelizeDotProductRules.parallelizeSum) ++ ParallelizeDotProductRules.createMultipleDotProd)),

      (MergeFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), CacheFunRules.get())),
      (MergeFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), FuncTimingRules.get())),

      (MemFunctionsCompiler.phaseBefore, RewriteStep(RewriteAll(), InputBufferingRules.fissionReadOOB)),
      (MemFunctionsCompiler.phaseBefore, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(64)))),
      (MemFunctionsCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(InputBufferingRules.limitParallelReadRequestsLevel3(bufSize)))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), InputBufferingRules.readDoubleBuffering ++ Seq(InputBufferingRules.doubleBufferReadLimited))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), TransposeCounterRules.get())),

      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(MapAndFunctionRules.moveOutCommonAlterMap, MapAndFunctionRules.moveOutCommonAlterMap2))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), MapAndFunctionRules.moveDownMemAllocFunCalls)),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get() ++ FixTimingRules.otherFixes)),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.regAdd))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.reg4Inputs))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.regRepeatSplit))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.regJoins))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(WorkAroundTimingRules.regMapResize))),
    )).debugWithHostRam(multiConvGen(size, size, ich, Seq(
      (3, och, true, 8, 8),
    ), false, false))
  }
}