package algo.vgg8bits

import algo.layerTemplates.ConvDataGen.multiConvGen
import algo.nn.templates.{ConvEngineFunTemplate, ConvLayerTemplate, DotProdFunTemplate, DoubleBufferingFunTemplate, FCLayerTemplate, OuterWrapperFunTemplate}
import backend.hdl.HDLProject
import backend.hdl.arch.ArchCompiler
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.MemFunctionsCompiler
import backend.hdl.arch.rewrite.{FixTimingRules, InputBufferingRules, MapAndFunctionRules, ParallelizeBufferRules, ParallelizeDotProductRules, TransposeCounterRules, WorkAroundTimingRules}
import backend.hdl.arch.rewrite.sharedFunc.{CacheFunRules, FuncBufferingRules, FuncTimingRules, SynchronizeBitWidthRules}
import backend.hdl.arch.sharedFunc.{GlobalFuncCompiler, MergeFuncCompiler}
import core.{Counter, Let, ParamDef}
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted, Rules}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class VggFullNoBiasTest {
  @Test
  def testConv(): Unit = {
    Counter.resetAll()
    Rules.disabled = Seq(
      "exchangeABMapWithWeight",
      "doubleBufferOutputMapStmToVec",
      "convertTranspose2DIntoPermuteVec",
      "limitedConvertTransposeNDIntoPermute",
      "convertTranspose2DIntoPermuteVec",
      "exchangeABMapWithSize",
      "bufferRepeatParam",
      "expandPseudoTransposeND",
    )

    val tileSize = 8
    val bufSize = if(tileSize == 8) 50 else 36

    val dpFun = DotProdFunTemplate.createDotProdFun(64, 8)
    val dpFunParam = ParamDef(dpFun.t)
    val convFun = ConvEngineFunTemplate.createConvFunEngineV1(dpFunParam, 64, 9, 4, 64, 8)
    val convFunParam = ParamDef(convFun.t)

    val inputBuffer = DoubleBufferingFunTemplate.createSlidingBuffer(tileSize, tileSize)
    val inputBufferParam = ParamDef(inputBuffer.t)

    val weightBuffer = DoubleBufferingFunTemplate.createRepeatingBuffer(tileSize, tileSize)
    val weightBufferParam = ParamDef(weightBuffer.t)

    val convWrapperFun = OuterWrapperFunTemplate.createConvWrapper(convFunParam, inputBufferParam, weightBufferParam, tileSize, tileSize)
    val convWrapperFunParam = ParamDef(convWrapperFun.t)

    val test = Let(
      dpFunParam,
      Let(
        convFunParam,
        Let(
          inputBufferParam,
          Let(
            weightBufferParam,
            Let(
              convWrapperFunParam,
              {
                val conv0 = ConvLayerTemplate.createConvWrapperSimple(dpFunParam, 32, 32, 3, 64, 3, 1, 8, 0)
                val conv1 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 32, 32, 64, 64, tileSize, tileSize, id = 1, optInput = Some(conv0))
                val conv2 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 16, 16, 64, 128, tileSize, tileSize, pool = false, id = 2, optInput = Some(conv1))
                val conv3 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 16, 16, 128, 128, tileSize, tileSize, pool = true, id = 3, optInput = Some(conv2))
                val conv4 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 8, 8, 128, 256, tileSize, tileSize, pool = false, id = 4, optInput = Some(conv3))
                val conv5_1 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 8, 8, 256, 256, tileSize, tileSize, pool = false, id = 5, optInput = Some(conv4))
                val conv5_2 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 8, 8, 256, 256, tileSize, tileSize, pool = true, id = 6, optInput = Some(conv5_1))
                val conv6 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 4, 4, 256, 512, tileSize, tileSize, pool = false, id = 7, optInput = Some(conv5_2))
                val conv7_1 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 4, 4, 512, 512, tileSize, tileSize, pool = false, id = 8, optInput = Some(conv6))
                val conv7_2 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 4, 4, 512, 512, tileSize, tileSize, pool = true, id = 9, optInput = Some(conv7_1))
                val conv8_1 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 2, 2, 512, 512, tileSize, tileSize, pool = false, id = 10, optInput = Some(conv7_2))
                val conv8_2 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 2, 2, 512, 512, tileSize, tileSize, pool = false, id = 11, optInput = Some(conv8_1))
                val conv8_3 = ConvLayerTemplate.createConvWrapper(convWrapperFunParam, 2, 2, 512, 512, tileSize, tileSize, pool = true, id = 12, optInput = Some(conv8_2))
                val fc1 = FCLayerTemplate.createMVWrapper(dpFunParam, 512, 512, 8, id = 13, optInput = Some(conv8_3))
                fc1
              },
              convWrapperFun
            ),
            weightBuffer
          ),
          inputBuffer
        ),
        convFun
      ),
      dpFun
    )

    HDLProject(this.getClass.getSimpleName, test, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeBufferRules.get())),
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("weight0", 1)))),
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("weight13", 1)))),

      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), SynchronizeBitWidthRules.get(Some(17)))),
      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), FuncBufferingRules.get())),
      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.get(Some(64)))),
      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(ParallelizeDotProductRules.parallelizeSum) ++ ParallelizeDotProductRules.createMultipleDotProd)),

      (MergeFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), CacheFunRules.get())),
      (MergeFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), FuncTimingRules.get())),

      (MemFunctionsCompiler.phaseBefore, RewriteStep(RewriteAll(), InputBufferingRules.fissionReadOOB)),
      (MemFunctionsCompiler.phaseBefore, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(64)))),
      (MemFunctionsCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(InputBufferingRules.limitParallelReadRequestsLevel3(bufSize)))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), InputBufferingRules.readDoubleBuffering ++ Seq(InputBufferingRules.doubleBufferReadLimited))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), TransposeCounterRules.get())),

      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(MapAndFunctionRules.moveOutCommonAlterMap, MapAndFunctionRules.moveOutCommonAlterMap2))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), MapAndFunctionRules.moveDownMemAllocFunCalls)),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get() ++ FixTimingRules.otherFixes)),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.regAdd))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.reg4Inputs))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.regRepeatSplit))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.regJoins))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(WorkAroundTimingRules.regMapResize))),
    )).writeAllFiles(multiConvGen(32, 32, 3, Seq(
      (3, 64, false, 8, 8),
      (3, 64, true, 8, 8),
      (3, 128, false, 8, 8),
      (3, 128, true, 8, 8),
      (3, 256, false, 8, 8),
      (3, 256, false, 8, 8),
      (3, 256, true, 8, 8),
      (3, 512, false, 8, 8),
      (3, 512, false, 8, 8),
      (3, 512, true, 8, 8),
      (3, 512, false, 8, 8),
      (3, 512, false, 8, 8),
      (3, 512, true, 8, 8),
      (-1, 512, false, 8, 8),
    ), false, true))
  }
}