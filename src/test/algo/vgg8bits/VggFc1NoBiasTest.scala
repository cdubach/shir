package algo.vgg8bits

import algo.layerTemplates.ConvDataGen.multiConvGen
import algo.nn.templates.{ConvEngineFunTemplate, ConvLayerTemplate, DotProdFunTemplate, DoubleBufferingFunTemplate, FCLayerTemplate, OuterWrapperFunTemplate}
import backend.hdl.HDLProject
import backend.hdl.arch.ArchCompiler
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.MemFunctionsCompiler
import backend.hdl.arch.rewrite.{FixTimingRules, InputBufferingRules, MapAndFunctionRules, ParallelizeBufferRules, ParallelizeDotProductRules, TransposeCounterRules, WorkAroundTimingRules}
import backend.hdl.arch.rewrite.sharedFunc.{CacheFunRules, FuncBufferingRules, FuncTimingRules, SynchronizeBitWidthRules}
import backend.hdl.arch.sharedFunc.{GlobalFuncCompiler, MergeFuncCompiler}
import core.{Counter, Let, ParamDef}
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted, Rules}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

class VggFc1NoBiasTest {
  @Test
  def testConv(): Unit = {
    Counter.resetAll()
    Rules.disabled = Seq(
      "exchangeABMapWithWeight",
      "doubleBufferOutputMapStmToVec",
      "convertTranspose2DIntoPermuteVec",
      "limitedConvertTransposeNDIntoPermute",
      "convertTranspose2DIntoPermuteVec",
      "exchangeABMapWithSize",
      "bufferRepeatParam",
      "expandPseudoTransposeND",
    )

    val dpFun = DotProdFunTemplate.createDotProdFun(64, 8)
    val dpFunParam = ParamDef(dpFun.t)
    val convFun = ConvEngineFunTemplate.createConvFunEngineV1(dpFunParam, 64, 9, 4, 64, 8)
    val convFunParam = ParamDef(convFun.t)

    val inputBuffer = DoubleBufferingFunTemplate.createSlidingBuffer()
    val inputBufferParam = ParamDef(inputBuffer.t)

    val weightBuffer = DoubleBufferingFunTemplate.createRepeatingBuffer()
    val weightBufferParam = ParamDef(weightBuffer.t)

    val convWrapperFun = OuterWrapperFunTemplate.createConvWrapper(convFunParam, inputBufferParam, weightBufferParam)
    val convWrapperFunParam = ParamDef(convWrapperFun.t)

    val test = Let(
      dpFunParam,
      {
        val fc1 = FCLayerTemplate.createMVWrapper(dpFunParam, 512, 512, 8, id = 0)
        fc1
      },
      dpFun
    )

    HDLProject(this.getClass.getSimpleName, test, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeBufferRules.get())),
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("weight0", 1)))),

      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), SynchronizeBitWidthRules.get(Some(17)))),
      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), FuncBufferingRules.get())),
      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.get(Some(64)))),
      (GlobalFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(ParallelizeDotProductRules.parallelizeSum) ++ ParallelizeDotProductRules.createMultipleDotProd)),

      (MergeFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), CacheFunRules.get())),
      (MergeFuncCompiler.phaseAfter, RewriteStep(RewriteAll(), FuncTimingRules.get())),

      (MemFunctionsCompiler.phaseBefore, RewriteStep(RewriteAll(), InputBufferingRules.fissionReadOOB)),
      (MemFunctionsCompiler.phaseBefore, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(64)))),
      //(MemFunctionsCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(InputBufferingRules.limitParallelReadRequestsLevel3(50)))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), InputBufferingRules.readDoubleBuffering ++ Seq(InputBufferingRules.doubleBufferReadLimited))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), TransposeCounterRules.get())),

      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(MapAndFunctionRules.moveOutCommonAlterMap, MapAndFunctionRules.moveOutCommonAlterMap2))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), MapAndFunctionRules.moveDownMemAllocFunCalls)),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get() ++ FixTimingRules.otherFixes)),
      /*(DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.regAdd))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.reg4Inputs))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.regRepeatSplit))),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(WorkAroundTimingRules.regJoins))),*/
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(WorkAroundTimingRules.regMapResize))),
    )).writeAllFiles(multiConvGen(1, 1, 512, Seq(
      (-1, 512, false, 8, 8),
    ), false, true))
  }
}