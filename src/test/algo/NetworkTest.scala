package algo

import algo.nn.{ConvolutionLayer, InputLayer, Network, NetworkLayerExpr, PoolingLayer, RoundingLayer, SaveLayer}
import core.{Expr, TypeChecker}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

/**
 * Tests for the `Network` helper object.
 */
@Category(Array(classOf[EssentialTests]))
class NetworkTest {
  /**
   * Checks that calling `Network.countLayers` counts as we might expect.
   */
  @Test
  def countingWorks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val conv0 = ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)
    val round0 = RoundingLayer(conv0, 8, 8)
    val conv1 = ConvolutionLayer(round0, 64, 64, 3, 1, 8, 1)
    val round1 = RoundingLayer(conv1, 8, 8)
    val conv1Pool = PoolingLayer(round1, 2)
    val conv2 = ConvolutionLayer(conv1Pool, 64, 128, 3, 1, 8, 2)
    val round2 = RoundingLayer(conv2, 8, 8)
    val res = SaveLayer(round2)

    assert(Network.countLayers(input) == 1)
    assert(Network.countLayers(res) == 12)
  }

  /**
   * Checks that calling `Network.lower` leaves no neural network layers in an expression.
   */
  @Test
  def lowerLeavesNoTrace(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val conv0 = ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)
    val round0 = RoundingLayer(conv0, 8, 8)
    val conv1 = ConvolutionLayer(round0, 64, 64, 3, 1, 8, 1)
    val round1 = RoundingLayer(conv1, 8, 8)
    val conv1Pool = PoolingLayer(round1, 2)
    val conv2 = ConvolutionLayer(conv1Pool, 64, 128, 3, 1, 8, 2)
    val round2 = RoundingLayer(conv2, 8, 8)
    val res = SaveLayer(round2)

    assert(Network.countLayers(res) > 0)

    val lowered = Network.lower(res)
    assert(Network.countLayers(lowered) == 0)
    assert(TypeChecker.check(lowered).t == TypeChecker.check(res).t)
  }
}
