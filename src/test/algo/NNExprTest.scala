package algo

import algo.nn._
import core.{ArithType, ParamDef, ParamUse, TypeChecker}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class NNExprTest {
  @Test
  def inputLayerTypeChecks(): Unit = {
    val input = InputLayer(128, 128, 128, 128, 0)
    assert(TypeChecker.check(input).t == SeqType(SeqType(SeqType(SignedIntType(128), 128), 128), 128))
  }

  @Test
  def saveLayerTypeChecks(): Unit = {
    val p = ParamDef(TypeChecker.check(InputLayer(128, 128, 128, 128, 0)).t)
    val input = SaveLayer(ParamUse(p))
    assert(TypeChecker.check(input).t == SeqType(SeqType(SignedIntType(128), 128), 128 * 128))
  }

  @Test
  def convolutionLayerTypeChecks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0)
    assert(TypeChecker.check(conv).t == SeqType(SeqType(SeqType(SignedIntType(21), 64), 32), 32))
  }

  @Test
  def convolutionLayerUnpacks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val firstConv = ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0)
    val secondConv = firstConv match {
      case ConvolutionLayer(PaddingLayer(input, padT, padB, padL, padR, _), weight,  _) =>
        input match {
          case ParamUse(param) => assert(p.id == param.id)
        }
        ConvolutionLayer(PaddingLayer(input, padT, padB, padL, padR), weight)
    }
    (firstConv, secondConv) match {
      case (ConvolutionLayer(PaddingLayer(input1, padT1, padB1, padL1, padR1, _), weight1, _),
      ConvolutionLayer(PaddingLayer(input2, padT2, padB2, padL2, padR2, _), weight2, _)) =>
        assert(input1 == input2)
        assert(weight1 == weight2)
        assert(padT1 == padT2)
        assert(padB1 == padB2)
        assert(padL1 == padL2)
        assert(padR1 == padR2)
    }
  }

  @Test
  def tiledConvHWLayerTypeChecks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0).tileHW(18, 18)
    assert(TypeChecker.check(conv).t == SeqType(SeqType(SeqType(SignedIntType(21), 64), 32), 32))
  }

  @Test
  def tiledHWLayerUnpacks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val firstConv = TypeChecker.check(ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0))
    firstConv match {
      case ConvolutionLayer(input1, weight1,  _) =>
        val tiled = firstConv.asInstanceOf[ConvolutionLayerExpr].tileHW(18, 18)
        tiled match {
          case TiledConvHWLayer(input2, weight2, _, _) =>
            assert(input1 == input2)
            assert(weight1 == weight2)
        }
    }
  }

  @Test
  def tiledHWLayerExpands(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0)
    val tiledConv = conv.tileHW(18, 18)
    assert(TypeChecker.check(conv).t == TypeChecker.check(tiledConv).t)

    val expandedConv = tiledConv.asInstanceOf[NetworkLayerExpr].expand()
    assert(TypeChecker.check(conv).t == TypeChecker.check(expandedConv).t)
  }

  @Test
  def tiledConvOCHLayerTypeChecks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0).tileOCH(32)
    assert(TypeChecker.check(conv).t == SeqType(SeqType(SeqType(SignedIntType(21), 64), 32), 32))
  }

  @Test
  def tiledOCHLayerUnpacks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val firstConv = TypeChecker.check(ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0))
    firstConv match {
      case ConvolutionLayer(input1, weight1,  _) =>
        val tiled = firstConv.asInstanceOf[ConvolutionLayerExpr].tileOCH(32)
        tiled match {
          case TiledConvOCHLayer(input2, weight2, _, _) =>
            assert(input1 == input2)
            assert(weight1 == weight2)
        }
    }
  }

  @Test
  def tiledOCHLayerExpands(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0)
    val tiledConv = conv.tileOCH(32)
    assert(TypeChecker.check(conv).t == TypeChecker.check(tiledConv).t)

    val expandedConv = tiledConv.asInstanceOf[NetworkLayerExpr].expand()
    assert(TypeChecker.check(conv).t == TypeChecker.check(expandedConv).t)
  }

  @Test
  def tiledConvICHLayerTypeChecks(): Unit = {
    val input = InputLayer(64, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = ConvolutionLayer(ParamUse(p), 64, 64, 3, 1, 8, 0).tileICH(32)
    assert(TypeChecker.check(conv).t == SeqType(SeqType(SeqType(SignedIntType(26), 64), 32), 32))
  }

  @Test
  def tiledICHLayerUnpacks(): Unit = {
    val input = InputLayer(64, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val firstConv = TypeChecker.check(ConvolutionLayer(ParamUse(p), 64, 64, 3, 1, 8, 0))
    firstConv match {
      case ConvolutionLayer(input1, weight1,  _) =>
        val tiled = firstConv.asInstanceOf[ConvolutionLayerExpr].tileICH(32)
        tiled match {
          case TiledConvICHLayer(input2, weight2, _, _) =>
            assert(input1 == input2)
            assert(weight1 == weight2)
        }
    }
  }

  @Test
  def tiledICHLayerExpands(): Unit = {
    val input = InputLayer(64, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = ConvolutionLayer(ParamUse(p), 64, 64, 3, 1, 8, 0)
    val tiledConv = conv.tileICH(32)
    assert(TypeChecker.check(conv).t == TypeChecker.check(tiledConv).t)

    val expandedConv = tiledConv.asInstanceOf[NetworkLayerExpr].expand()
    assert(TypeChecker.check(conv).t == TypeChecker.check(expandedConv).t)
  }

  @Test
  def paddedConvHWLayerTypeChecks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0).padHW(2, 2)
    assert(TypeChecker.check(conv).t == SeqType(SeqType(SeqType(SignedIntType(21), 64), 32), 32))
  }

  @Test
  def paddedHWLayerUnpacks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val firstConv = TypeChecker.check(ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0))
    firstConv match {
      case ConvolutionLayer(input1, weight1,  _) =>
        val padded = firstConv.asInstanceOf[ConvolutionLayerExpr].padHW(2, 2)
        padded match {
          case PaddedConvHWLayer(input2, weight2, _, _) =>
            assert(input1 == input2)
            assert(weight1 == weight2)
        }
    }
  }

  @Test
  def paddedHWLayerExpands(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0)
    val paddedConv = conv.padHW(2, 2)
    assert(TypeChecker.check(conv).t == TypeChecker.check(paddedConv).t)

    val expandedConv = paddedConv.asInstanceOf[NetworkLayerExpr].expand()
    assert(TypeChecker.check(conv).t == TypeChecker.check(expandedConv).t)
  }

  @Test
  def roundingLayerTypeChecks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val conv0 = ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)

    val p = ParamDef(TypeChecker.check(conv0).t)
    val round0 = RoundingLayer(ParamUse(p), 8, 8)

    val t = SeqType(SeqType(SeqType(SignedIntType(8), 64), 32), 32)
    assert(TypeChecker.check(round0).t == t)
  }

  @Test
  def poolingLayerTypeChecks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val conv0 = ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)
    val round0 = RoundingLayer(conv0, 8, 8)
    val conv1 = ConvolutionLayer(round0, 64, 64, 3, 1, 8, 1)
    val round1 = RoundingLayer(conv1, 8, 8)

    val p = ParamDef(TypeChecker.check(round1).t)
    val conv1Pool = PoolingLayer(ParamUse(p), 2)

    val t = SeqType(SeqType(SeqType(SignedIntType(8), 64), 16), 16)
    assert(TypeChecker.check(conv1Pool).t == t)
  }

  @Test
  def reluLayerTypeChecks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)

    val p = ParamDef(TypeChecker.check(input).t)
    val relu = ReLULayer(ParamUse(p))

    val t = SeqType(SeqType(SeqType(SignedIntType(8), 3), 32), 32)
    assert(TypeChecker.check(relu).t == t)
  }

  @Test
  def resizeIntegerLayerTypeChecks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)

    val p = ParamDef(TypeChecker.check(input).t)
    val resize = ResizeIntegerLayer(ParamUse(p), 7)

    val t = SeqType(SeqType(SeqType(SignedIntType(7), 3), 32), 32)
    assert(TypeChecker.check(resize).t == t)
  }

  @Test
  def dotProductLayerTypeChecks(): Unit = {
    val left = ParamDef(SeqType(SignedIntType(8), 256))
    val right = ParamDef(SeqType(SignedIntType(8), 256))

    val dot = DotProductLayer(ParamUse(left), ParamUse(right))

    val t = SignedIntType(24)
    assert(TypeChecker.check(dot).t == t)
  }

  @Test
  def dotProductLayerUnpacks(): Unit = {
    val left = ParamDef(SeqType(SignedIntType(8), 256))
    val right = ParamDef(SeqType(SignedIntType(8), 256))

    val dot = DotProductLayer(ParamUse(left), ParamUse(right))
    dot match {
      case DotProductLayer(ParamUse(left2), ParamUse(right2), _) =>
        assert(left.id == left2.id)
        assert(right.id == right2.id)
    }
  }

  @Test
  def matVecLayerTypeChecks(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = MatVecLayer(ParamUse(matrix), ParamUse(vector))

    val t = SeqType(SignedIntType(24), 8)
    assert(TypeChecker.check(matVec).t == t)
  }

  @Test
  def matVecLayerUnpacks(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = MatVecLayer(ParamUse(matrix), ParamUse(vector))

    matVec match {
      case MatVecLayer(ParamUse(matrix2), ParamUse(vector2), _) =>
        assert(matrix.id == matrix2.id)
        assert(vector.id == vector2.id)
    }
  }

  @Test
  def paddedMatVecLayerTypeChecks(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = MatVecLayer(ParamUse(matrix), ParamUse(vector)).pad(2)

    val t = SeqType(SignedIntType(24), 8)
    assert(TypeChecker.check(matVec).t == t)
  }

  @Test
  def paddedMatVecLayerUnpacks(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = MatVecLayer(ParamUse(matrix), ParamUse(vector)).pad(2)
    matVec match {
      case PaddedMatVecLayer(ParamUse(matrix2), ParamUse(vector2), size, _, _) =>
        assert(matrix.id == matrix2.id)
        assert(vector.id == vector2.id)
        assert(size.ae.evalInt == 2)
    }
  }

  @Test
  def paddedMatVecExpands(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = MatVecLayer(ParamUse(matrix), ParamUse(vector)).pad(2)
    val expandedMatVec = matVec.expand()
    assert(TypeChecker.check(matVec).t == TypeChecker.check(expandedMatVec).t)
  }

  @Test
  def tiledMatVecLayerTypeChecks(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = MatVecLayer(ParamUse(matrix), ParamUse(vector)).tile(16)

    val t = SeqType(SignedIntType(24), 8)
    assert(TypeChecker.check(matVec).t == t)
  }

  @Test
  def tiledMatVecLayerUnpacks(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = MatVecLayer(ParamUse(matrix), ParamUse(vector)).tile(16)
    matVec match {
      case TiledMatVecLayer(ParamUse(matrix2), ParamUse(vector2), tileSize, _, _) =>
        assert(matrix.id == matrix2.id)
        assert(vector.id == vector2.id)
        assert(tileSize.ae.evalInt == 16)
    }
  }

  @Test
  def tiledMatVecLayerExpands(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = MatVecLayer(ParamUse(matrix), ParamUse(vector)).tile(16)
    val expandedMatVec = matVec.expand()
    assert(TypeChecker.check(matVec).t == TypeChecker.check(expandedMatVec).t)
  }

  @Test
  def parallelDotProductsLayerTypeChecks(): Unit = {
    val left = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 256))
    val right = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 256))

    val dot = ParallelDotProductsLayer(ParamUse(left), ParamUse(right), 256)

    val t = SeqType(SignedIntType(24), 256)
    assert(TypeChecker.check(dot).t == t)
  }

  @Test
  def parallelDotProductsLayerUnpacks(): Unit = {
    val left = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 256))
    val right = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 256))
    val simultaneousProducts = ArithType(256)

    val dot = ParallelDotProductsLayer(ParamUse(left), ParamUse(right), simultaneousProducts)
    dot match {
      case ParallelDotProductsLayer(ParamUse(left2), ParamUse(right2), simultaneousProducts2, _) =>
        assert(left.id == left2.id)
        assert(right.id == right2.id)
        assert(simultaneousProducts == simultaneousProducts2)
    }
  }

  @Test
  def parallelDotProductsLayerExpands(): Unit = {
    val left = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 256))
    val right = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 256))

    val dot = ParallelDotProductsLayer(ParamUse(left), ParamUse(right), 256)
    val expandedDot = dot.expand()
    assert(TypeChecker.check(dot).t == TypeChecker.check(expandedDot).t)

    val dot2 = ParallelDotProductsLayer(ParamUse(left), ParamUse(right), 1)
    val expandedDot2 = dot2.expand()
    assert(TypeChecker.check(dot2).t == TypeChecker.check(expandedDot2).t)

    val dot3 = ParallelDotProductsLayer(ParamUse(left), ParamUse(right), 2)
    val expandedDot3 = dot3.expand()
    assert(TypeChecker.check(dot3).t == TypeChecker.check(expandedDot3).t)
  }

  @Test
  def parallelDotProductsLayerPartiallyExpands(): Unit = {
    val left = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 256))
    val right = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 256))

    val dot = ParallelDotProductsLayer(ParamUse(left), ParamUse(right), 256)
    val expandedDot = dot.partiallyExpanded
    assert(TypeChecker.check(dot).t == TypeChecker.check(expandedDot).t)

    val dot2 = ParallelDotProductsLayer(ParamUse(left), ParamUse(right), 1)
    val expandedDot2 = dot2.partiallyExpanded
    assert(TypeChecker.check(dot2).t == TypeChecker.check(expandedDot2).t)

    val dot3 = ParallelDotProductsLayer(ParamUse(left), ParamUse(right), 2)
    val expandedDot3 = dot3.partiallyExpanded
    assert(TypeChecker.check(dot3).t == TypeChecker.check(expandedDot3).t)
  }

  @Test
  def parallelDotProductsLayerSequentializes(): Unit = {
    val left = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 256))
    val right = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 256))

    val dot = ParallelDotProductsLayer(ParamUse(left), ParamUse(right), 256)
    val expandedDot = dot.expand()
    assert(TypeChecker.check(dot).t == TypeChecker.check(expandedDot).t)
  }

  @Test
  def tiledDotProductLayerTypeChecks(): Unit = {
    val left = ParamDef(SeqType(SignedIntType(8), 256))
    val right = ParamDef(SeqType(SignedIntType(8), 256))

    val dot = DotProductLayer(ParamUse(left), ParamUse(right)).tile(16, 4)

    val t = SignedIntType(24)
    assert(TypeChecker.check(dot).t == t)
  }

  @Test
  def tiledDotProductLayerUnpacks(): Unit = {
    val left = ParamDef(SeqType(SignedIntType(8), 256))
    val right = ParamDef(SeqType(SignedIntType(8), 256))

    val dot = DotProductLayer(ParamUse(left), ParamUse(right)).tile(16, 4)
    dot match {
      case TiledDotProductLayer(ParamUse(left2), ParamUse(right2), tileSize, _, _) =>
        assert(left.id == left2.id)
        assert(right.id == right2.id)
        assert(tileSize.ae.evalInt == 16)
    }
  }

  @Test
  def tiledDotProductLayerExpands(): Unit = {
    val left = ParamDef(SeqType(SignedIntType(8), 256))
    val right = ParamDef(SeqType(SignedIntType(8), 256))

    val dot = DotProductLayer(ParamUse(left), ParamUse(right)).tile(16, 4)
    val expandedDot = dot.expand()
    assert(TypeChecker.check(dot).t == TypeChecker.check(expandedDot).t)
  }

  @Test
  def networkTypeChecks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val conv0 = ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)
    val round0 = RoundingLayer(conv0, 8, 8)
    val conv1 = ConvolutionLayer(round0, 64, 64, 3, 1, 8, 1)
    val round1 = RoundingLayer(conv1, 8, 8)
    val conv1Pool = PoolingLayer(round1, 2)
    val conv2 = ConvolutionLayer(conv1Pool, 64, 128, 3, 1, 8, 2)
    val round2 = RoundingLayer(conv2, 8, 8)
    val res = SaveLayer(round2)

    val t = SeqType(SeqType(SignedIntType(8), 128), 256)
    assert(TypeChecker.check(res).t == t)
  }
}
