package algo

import algo.nn.MatVecLayer
import core.{ParamDef, ParamUse, TypeChecker}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class TilingTest {
  @Test
  def matVecTilingTypeChecks(): Unit = {
    val t = SignedIntType(8)
    val matrix = ParamDef(SeqType(SeqType(t, 32), 100))
    val vector = ParamDef(SeqType(t, 32))
    val expectedResultType = SeqType(SignedIntType(21), 100)

    val untiled = MatVecLayer(ParamUse(matrix), ParamUse(vector))
    assert(TypeChecker.check(untiled).t == expectedResultType)

    val tiled = untiled.tile(16)
    assert(TypeChecker.check(tiled).t == expectedResultType)
  }
}
