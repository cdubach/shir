package algo.layerTemplates

import algo.util.Matrix
import backend.hdl.arch.mem.MemoryImage

object ConvDataGen {

  def convLayerGen(
    imageRows: Int,
    imageCols: Int,
    weightSize: Int,
    inChannel: Int,
    outChannel: Int,
    pool: Boolean,
    bitOffset: Int = 8,
    bitWidth: Int = 8,
    id: Int = 0,
    bias: Boolean = true,
    previousLayer: Option[Predef.Map[String, Seq[Seq[Int]]]] = None
  ): Map[String, Seq[Seq[Int]]] = {
    var inputName: String = "image"
    val imageOrig = previousLayer match {
      case Some(layerData) if layerData.contains(MemoryImage.RESULT_VAR_NAME) =>
        inputName = "Inserted_"
        Matrix(layerData.get(MemoryImage.RESULT_VAR_NAME).get, "i" + id).deReshape(imageCols, false)
      case _ => Matrix.fromRandom(imageRows, imageCols * inChannel, 9, Some("i" + id))
    }
    val weightOrig = Matrix.fromRandom(outChannel, weightSize * weightSize * inChannel, 9, Some("w" + id))
    val imagePad = imageOrig.pad(1, 1, inChannel, inChannel, false)
    val output = imagePad.conv(weightOrig, inChannel, false, false)
    if (bias) {
      val biasOrig = Matrix.fromRandom(1, outChannel, 9, Some("b" + id))
      val outputBias = output.addBias(biasOrig, false)
      val outputRound = outputBias.round(bitOffset).clip(bitWidth).max(0)
      val outputPool = outputRound.pool(2, 2, outChannel)
      val realOut = if (pool) outputPool.reshape(imageCols / 2, false).data else outputRound.reshape(imageCols, false).data
      Predef.Map(
        inputName + id -> imageOrig.reshape(imageCols, false).data,
        "weight" + id -> weightOrig.data,
        "bias" + id -> biasOrig.data,
        MemoryImage.RESULT_VAR_NAME -> realOut
      )
    } else {
      val outputRound = output.round(bitOffset).clip(bitWidth).max(0)
      val outputPool = outputRound.pool(2, 2, outChannel, false)
      val realOut = if (pool) outputPool.reshape(imageCols / 2, false).data else outputRound.reshape(imageCols, false).data
      Predef.Map(
        inputName + id-> imageOrig.reshape(imageCols, false).data,
        "weight" + id-> weightOrig.data,
        MemoryImage.RESULT_VAR_NAME -> realOut
      )
    }
  }

  def convLeadingLayerGen(
    imageRows: Int,
    imageCols: Int,
    weightSize: Int,
    inChannel: Int,
    outChannel: Int,
    bitOffset: Int = 8,
    bitWidth: Int = 8,
    id: Int = 0,
    bias: Boolean = true
  ): Map[String, Seq[Seq[Int]]] = {
    val imageOrig = Matrix.fromRandom(imageRows, imageCols * inChannel, 9, Some("i" + id))
    val weightOrig = Matrix.fromRandom(outChannel, weightSize * weightSize * inChannel, 9, Some("w" + id))
    val imagePad = imageOrig.pad(1, 1, inChannel, inChannel, false)
    val output = imagePad.conv(weightOrig, inChannel, false, false)
    if (bias) {
      val biasOrig = Matrix.fromRandom(1, outChannel, 9, Some("b" + id))
      val outputReshape = output.reshape(imageCols, false)
      val outputBias = outputReshape.addBias(biasOrig, false)
      val outputRound = outputBias.round(bitOffset).clip(bitWidth).max(0)
      Predef.Map(
        "image" + id -> imageOrig.data,
        "weight" + id -> weightOrig.data,
        "bias" + id -> biasOrig.data,
        MemoryImage.RESULT_VAR_NAME -> outputRound.data
      )
    } else {
      val outputReshape = output.reshape(imageCols, false)
      val outputRound = outputReshape.round(bitOffset).clip(bitWidth).max(0)
      Predef.Map(
        "image" + id -> imageOrig.data,
        "weight" + id -> weightOrig.data,
        MemoryImage.RESULT_VAR_NAME -> outputRound.data
      )
    }
  }

  def FCDataGen(
    rows: Int,
    cols: Int,
    bitOffset: Int = 8,
    bitWidth: Int = 8,
    id: Int = 0,
    bias: Boolean = true,
    previousLayer: Option[Predef.Map[String, Seq[Seq[Int]]]] = None
  ): Map[String, Seq[Seq[Int]]] = {
    var inputName: String = "vec"
    val matA = Matrix.fromRandom(rows, cols, 9, Some("m" + id))
    val matB = previousLayer match {
      case Some(layerData) if layerData.contains(MemoryImage.RESULT_VAR_NAME) =>
        inputName = "Inserted_"
        Matrix(layerData.get(MemoryImage.RESULT_VAR_NAME).get, "v" + id).reshape(1, false)
      case _ => Matrix.fromRandom(1, cols, 9, Some("v" + id))
    }
    if(bias) {
      val biasV = Matrix.fromRandom(1, rows, 9, Some("b" + id))
      val matC = matA.mul(matB).addBias(biasV, false).round(bitOffset).clip(bitWidth).max(0)
      Predef.Map(
        "mat" + id -> matA.data,
        inputName + id -> matB.data,
        "bias" + id -> biasV.data,
        MemoryImage.RESULT_VAR_NAME -> Seq(matC.data.flatten)
      )
    } else {
      val matC = matA.mul(matB).round(bitOffset).clip(bitWidth).max(0)
      Predef.Map(
        "mat" + id -> matA.data,
        inputName + id -> matB.data,
        MemoryImage.RESULT_VAR_NAME -> Seq(matC.data.flatten)
      )
    }
  }


  /**
   * Automatic layer-wise data generation.
   *
   * @param layerSetups Tuples of 5 elements with weight size, output channel, pooling, bit offset, and bit width.
   *                    Each tuple indicates one single convolutional layer.
   * @param bias Use Bias or not.
   * @param useLeadingLayer Use a different data generator for the first layer.
   * @return A map of layer-wise data.
   */

  def multiConvGen(
    imageRows: Int,
    imageCols: Int,
    inChannel: Int,
    layerSetups: Seq[(Int, Int, Boolean, Int, Int)], // weightSize(-1 for FC layer), och, pool, bitOffset, bitWidth
    bias: Boolean = true,
    useLeadingLayer: Boolean = false,
  ): Map[String, Seq[Seq[Int]]] = {
    var rows: Int = imageRows
    var cols: Int = imageCols
    var ich: Int = inChannel
    var data: Map[String, Seq[Seq[Int]]] = Map()
    var id: Int = 0
    layerSetups.foreach(setup =>
    {
      if(setup._1 == -1) {
        data = data ++ FCDataGen(rows * cols * ich, setup._2, setup._4, setup._5, id, bias, if(data.isEmpty) None else Some(data))
        rows = 1
        cols = 1
      } else if(id == 0 && useLeadingLayer)
        data = data ++ convLeadingLayerGen(rows, cols, setup._1, ich, setup._2, setup._4, setup._5, id, bias)
      else
        data = data ++ convLayerGen(rows, cols, setup._1, ich, setup._2, setup._3, setup._4, setup._5, id, bias, if(data.isEmpty) None else Some(data))
      if(setup._3){
        rows = rows / 2
        cols = cols / 2
      }
      ich = setup._2
      id = id + 1
    }
    )
    data
  }
}