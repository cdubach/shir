package algo

import _root_.eqsat.DeBruijnTransform
import algo.nn._
import core.{ParamDef, ParamUse, TypeChecker}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class HierarchicalNNExprTest {
  @Test
  def matVecConvolutionLayerTypeChecks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0))
    assert(TypeChecker.check(conv).t == SeqType(SeqType(SeqType(SignedIntType(21), 64), 32), 32))
  }

  @Test
  def matVecConvolutionLayerUnpacks(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val firstConv = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0))
    val secondConv = firstConv match {
      case MatVecConvolutionLayer(PaddingLayer(input, padT, padB, padL, padR, _), weight, func, _) =>
        input match {
          case ParamUse(param) => assert(p.id == param.id)
        }
        MatVecConvolutionLayer(PaddingLayer(input, padT, padB, padL, padR), weight, func)
    }
    (firstConv, secondConv) match {
      case (MatVecConvolutionLayer(PaddingLayer(input1, padT1, padB1, padL1, padR1, _), weight1, _, _),
      MatVecConvolutionLayer(PaddingLayer(input2, padT2, padB2, padL2, padR2, _), weight2, _, _)) =>
        assert(input1 == input2)
        assert(weight1 == weight2)
        assert(padT1 == padT2)
        assert(padB1 == padB2)
        assert(padL1 == padL2)
        assert(padR1 == padR2)
    }
  }

  @Test
  def matVecConvolutionLayerExpands(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0))
    assert(TypeChecker.check(conv.expand()).t == SeqType(SeqType(SeqType(SignedIntType(21), 64), 32), 32))
  }

  @Test
  def matVecConvolutionLayerIsSynthesized(): Unit = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = HierarchicalLayer.fromRec(ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0))
    assert(conv.contains(_.isInstanceOf[MatVecConvolutionLayerExpr]))
    assert(conv.contains(_.isInstanceOf[DotProductMatVecLayerExpr]))
    val checked = TypeChecker.check(conv)
    val t = SeqType(SeqType(SeqType(SignedIntType(21), 64), 32), 32)
    assert(checked.t == t)
    assert(TypeChecker.check(DeBruijnTransform.forward(checked)).t == t)
  }

  @Test
  def dotProductMatVecLayerTypeChecks(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = DotProductMatVecLayer.from(MatVecLayer(ParamUse(matrix), ParamUse(vector)))

    val t = SeqType(SignedIntType(24), 8)
    assert(TypeChecker.check(matVec).t == t)
  }

  @Test
  def dotProductMatVecLayerUnpacks(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = DotProductMatVecLayer.from(MatVecLayer(ParamUse(matrix), ParamUse(vector)))

    matVec match {
      case DotProductMatVecLayer(ParamUse(matrix2), ParamUse(vector2), _, _) =>
        assert(matrix.id == matrix2.id)
        assert(vector.id == vector2.id)
    }
  }

  @Test
  def dotProductMatVecLayerExpands(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = DotProductMatVecLayer.from(MatVecLayer(ParamUse(matrix), ParamUse(vector)))

    val expanded = matVec.expand()
    val t = SeqType(SignedIntType(24), 8)
    assert(TypeChecker.check(expanded).t == t)
  }

  @Test
  def dotProductMatVecLayerIsSynthesized(): Unit = {
    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))

    val matVec = HierarchicalLayer.fromRec(MatVecLayer(ParamUse(matrix), ParamUse(vector)))

    assert(matVec.contains(_.isInstanceOf[DotProductMatVecLayerExpr]))
    val t = SeqType(SignedIntType(24), 8)
    val checked = TypeChecker.check(matVec)
    assert(checked.t == t)
    assert(TypeChecker.check(DeBruijnTransform.forward(checked)).t == t)
  }
}
