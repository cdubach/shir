package core

import cGen.{AlgoOp, BinaryOp, Constant, DoubleType, IntType}
import core.util.Prune
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

/**
 * Tests for the Prune utility.
 */
@Category(Array(classOf[EssentialTests]))
class PruneTest {
  @Test
  def findTypeError(): Unit = {
    val t1 = IntType(32)
    val t2 = DoubleType()
    val problematic = BinaryOp(Constant(1.0, t1), Constant(2.0, t2), AlgoOp.Add)
    val expr = BinaryOp(BinaryOp(Constant(1.0, t1), Constant(2.0, t1), AlgoOp.Mul), problematic, AlgoOp.Add)
    val issue = Prune.findTypeError(expr)

    assert(issue.get == problematic)
  }

  @Test
  def findUnsolvedTypeVar(): Unit = {
    val t = UnknownType
    val expr = BinaryOp(BinaryOp(Constant(1.0, t), Constant(2.0, t), AlgoOp.Mul), Constant(1.0, t), AlgoOp.Add)
    val issue = Prune.findUnsolvedTypeVar(expr)

    assert(issue.get != expr)
  }
}
