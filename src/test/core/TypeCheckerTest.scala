package core

import org.junit.{Ignore, Test}
import cGen.{ArrayType, ArrayTypeVar, ExternFunctionCall, IntType}
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class TypeCheckerTest {

  @Test
  def testLambda(): Unit = {
    val inputType = BuiltinDataType()
    val tree =
      {
        val p = ParamDef(UnknownType)
        Lambda(p, ParamUse(p))
      }.call(Value(inputType))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == inputType)
  }

  @Test
  def testTypeLambda(): Unit = {
    val inputType = BuiltinDataType()
    val tree =
      {
        val dtv = BuiltinDataTypeVar()
        TypeLambda(
          dtv,
          {
            val p = ParamDef(dtv)
            Lambda(p, ParamUse(p))
          }
        )
      }.call(Value(BuiltinDataType()))
    val rebuiltTree = TypeChecker.check(tree)
    assert(!rebuiltTree.hasUnknownType)
    assert(rebuiltTree.t == inputType)
  }

  @Test
  def testUnresolvableArithmetic(): Unit = {
    val t = ArithTypeVar()
    val constraints = Seq(
      (t, ArithType(27)),
      (t, ArithType(1044484))
    )
    assert(TypeChecker.solveConstraints(constraints).isEmpty)
  }

  // This test fails because the type checker is buggy. We'll ignore the test for now
  @Test
  @Ignore
  def testConsistentTypeVars(): Unit = {
    val i32 = IntType(32)
    val len = ArithTypeVar()
    val retType = ArrayType(i32, len)
    val param = ParamDef(retType)
    val call = ExternFunctionCall("permute_array", Seq(ParamUse(param)), Seq(ArrayTypeVar(i32, len)), retType)
    assert(TypeChecker.check(call).t == retType)
  }

  // This test fails because the type checker is buggy. We'll ignore the test for now
  @Test
  @Ignore
  def testUnresolvableArithmetic2(): Unit = {
    val t1 = ArithTypeVar()
    val t2 = ArithTypeVar()
    val constraints = Seq(
      (t1, ArithType(100000)),
      (t2, ArithType(3)),
      (ArithType(t1 - t2 + 1), ArithType(100000))
    )
    assert(TypeChecker.solveConstraints(constraints).isEmpty)
  }
}
