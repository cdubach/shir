package core

import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class NodeKindComparisonTest {
  @Test
  def testSameExprSameKind(): Unit = {
    val func = ParamUse(ParamDef())
    val arg1 = ParamUse(ParamDef())
    val arg2 = ParamUse(ParamDef())
    assert(func.nodeKind == func.nodeKind)
    assert(arg1.nodeKind == arg1.nodeKind)
    assert(arg2.nodeKind == arg2.nodeKind)
    assert(FunctionCall(func, arg1).nodeKind == FunctionCall(func, arg1).nodeKind)
    assert(FunctionCall(func, arg2).nodeKind == FunctionCall(func, arg2).nodeKind)
    assert(FunctionCall(func, arg1).nodeKind == FunctionCall(func, arg2).nodeKind)
  }

  @Test
  def testDifferentExprDifferentKind(): Unit = {
    val func = ParamUse(ParamDef())
    val arg1 = ParamUse(ParamDef())
    val arg2 = ParamUse(ParamDef())
    assert(FunctionCall(func, arg1).nodeKind != func.nodeKind)
    assert(FunctionCall(func, arg2).nodeKind != arg1.nodeKind)
    assert(FunctionCall(func, arg1).nodeKind != arg2.nodeKind)
  }
}
