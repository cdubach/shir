package core

import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class TextFormatterTest {
  @Test
  def formatUnknownType(): Unit = {
    val formatter = TextFormatter(new VariableRenamer())
    assert(formatter.format(UnknownType) == "unknown")
  }

  @Test
  def formatParamDef(): Unit = {
    val formatter = TextFormatter(new VariableRenamer())
    val p = ParamDef(UnknownType)
    assert(formatter.format(p) == s"param 0: ${formatter.format(UnknownType)}")
  }

  @Test
  def formatParamUse(): Unit = {
    val formatter = TextFormatter(new VariableRenamer())
    val p = ParamUse(ParamDef(UnknownType))
    assert(formatter.format(p) == s"use param 0: ${formatter.format(UnknownType)}")
  }

  @Test
  def formatIdentityLambda(): Unit = {
    val formatter = TextFormatter(new VariableRenamer())
    val p = ParamDef(UnknownType)
    val lambda = Lambda(p, ParamUse(p))
    val unknown = formatter.format(UnknownType)
    assert(formatter.format(lambda) == s"λ0: $unknown. (use param 0: $unknown)")
  }

  @Test
  def formatFunctionCall(): Unit = {
    val formatter = TextFormatter(new VariableRenamer())
    val p1 = ParamUse(ParamDef(UnknownType))
    val p2 = ParamUse(ParamDef(UnknownType))
    val unknown = formatter.format(UnknownType)
    assert(formatter.format(FunctionCall(p1, p2)) == s"(use param 0: $unknown) (use param 1: $unknown)")
  }

  @Test
  def formatExprVar(): Unit = {
    val formatter = TextFormatter(new VariableRenamer())
    val v = ExprVar(UnknownType)
    val unknown = formatter.format(UnknownType)
    assert(formatter.format(v) == s"expr 0: $unknown")
  }
}
