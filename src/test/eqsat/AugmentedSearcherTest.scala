package eqsat

import algo.{Add, AlgoDataTypeVar, IntType, Mul, Tuple}
import core.{ExprVar, ParamDef, ParamUse, TypeChecker}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class AugmentedSearcherTest {
  @Test
  def producesCartesianProduct(): Unit = {
    val pattern = {
      val t = AlgoDataTypeVar()
      val x = ExprVar(t)
      val y = ExprVar(t)
      Pattern(TypeChecker.check(Mul(Tuple(x, y))))
    }

    val p1 = ParamDef(IntType(8))
    val p2 = ParamDef(IntType(8))
    val p3 = ParamDef(IntType(8))
    val p4 = ParamDef(IntType(8))
    val expr = Add(Tuple(Mul(Tuple(ParamUse(p1), ParamUse(p2))), Mul(Tuple(ParamUse(p3), ParamUse(p4)))))
    val graph = EGraph()
    graph.add(expr)
    graph.rebuild()

    assert(pattern.search(graph).length == 2)
    assert(AugmentedSearcher(pattern, pattern).search(graph).length == 4)
    assert(AugmentedSearcher(pattern, AugmentedSearcher(pattern, pattern)).search(graph).length == 8)
  }

  @Test
  def producesMeaningfulMatch(): Unit = {
    val x1 = ExprVar(AlgoDataTypeVar())
    val y1 = ExprVar(AlgoDataTypeVar())
    val mulPattern = Pattern(TypeChecker.check(Mul(Tuple(x1, y1))))

    val x2 = ExprVar(AlgoDataTypeVar())
    val y2 = ExprVar(AlgoDataTypeVar())
    val addPattern = Pattern(TypeChecker.check(Add(Tuple(x2, y2))))

    val t = IntType(8)
    val p1 = ParamDef(t)
    val p2 = ParamDef(t)
    val p3 = ParamDef(t)
    val p4 = ParamDef(t)
    val expr = Add(Tuple(Mul(Tuple(ParamUse(p1), ParamUse(p2))), Add(Tuple(ParamUse(p3), ParamUse(p4)))))

    val graph = EGraph(analyses = new AnalysesBuilder().add(ExtractionAnalysis.smallestExpressionExtractor).toAnalyses)
    graph.add(expr)
    graph.rebuild()

    assert(mulPattern.search(graph).length == 1)
    assert(addPattern.search(graph).length == 2)

    val dualPattern = AugmentedSearcher(mulPattern, addPattern)
    val matches = dualPattern.search(graph)

    assert(matches.length == 2)
    assert(matches.forall(m =>
      (m.instantiate(x1).toSmallestExpr match { case ParamUse(p) => p.id == p1.id case _ => false }) &&
        (m.instantiate(y1).toSmallestExpr match { case ParamUse(p) => p.id == p2.id case _ => false })))
    assert(matches.exists(m =>
      (m.instantiate(x2).toSmallestExpr match { case ParamUse(p) => p.id == p3.id case _ => false }) &&
        (m.instantiate(y2).toSmallestExpr match { case ParamUse(p) => p.id == p4.id case _ => false })))
    assert(matches.exists(m =>
      (m.instantiate(x2).toSmallestExpr match { case Mul(Tuple(_, _), _) => true case _ => false }) &&
        (m.instantiate(y2).toSmallestExpr match { case Add(Tuple(_, _), _) => true case _ => false })))
  }
}
