package eqsat

import eqsat.solver.{AgglomeratedClass, AgglomeratedNode}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class AgglomeratedClassTest {
  /**
    * Test that `AgglomeratedClass.directlyReachable` works for a linear graph.
    */
  @Test
  def directlyReachableWorks(): Unit = {
    val class1 = new AgglomeratedClass(null)
    val class2 = new AgglomeratedClass(null)

    class1.nodes += AgglomeratedNode(null, Seq(Right(class2)))
    class2.nodes += AgglomeratedNode(null, Seq())

    assert(class1.directlyReachable == Seq(class2))
  }

  /**
    * Test that `AgglomeratedClass.directlyReachable` works for a graph containing a cycle.
    */
  @Test
  def directlyReachableWorksForCycle(): Unit = {
    val class1 = new AgglomeratedClass(null)
    val class2 = new AgglomeratedClass(null)

    class1.nodes += AgglomeratedNode(null, Seq(Right(class1), Right(class2)))
    class2.nodes += AgglomeratedNode(null, Seq())

    assert(class1.directlyReachable == Seq(class1, class2))
  }

  /**
    * Test that `AgglomeratedClass.reachable` works for a linear graph.
    */
  @Test
  def reachableWorks(): Unit = {
    val class1 = new AgglomeratedClass(null)
    val class2 = new AgglomeratedClass(null)
    val class3 = new AgglomeratedClass(null)

    class1.nodes += AgglomeratedNode(null, Seq(Right(class2)))
    class2.nodes += AgglomeratedNode(null, Seq(Right(class3)))
    class3.nodes += AgglomeratedNode(null, Seq())

    assert(class1.reachable == Seq(class1, class2, class3))
  }

  /**
    * Test that `AgglomeratedClass.reachable` works for a linear graph.
    */
  @Test
  def strictlyReachableWorks(): Unit = {
    val class1 = new AgglomeratedClass(null)
    val class2 = new AgglomeratedClass(null)
    val class3 = new AgglomeratedClass(null)

    class1.nodes += AgglomeratedNode(null, Seq(Right(class2)))
    class2.nodes += AgglomeratedNode(null, Seq(Right(class3)))
    class3.nodes += AgglomeratedNode(null, Seq())

    assert(class1.strictlyReachable == Seq(class2, class3))
  }

  /**
    * Test that `AgglomeratedClass.strictlyReachable` works for a graph containing a cycle.
    */
  @Test
  def strictlyReachableWorksForCycle(): Unit = {
    val class1 = new AgglomeratedClass(null)
    val class2 = new AgglomeratedClass(null)
    val class3 = new AgglomeratedClass(null)

    class1.nodes += AgglomeratedNode(null, Seq(Right(class2)))
    class2.nodes += AgglomeratedNode(null, Seq(Right(class3)))
    class3.nodes += AgglomeratedNode(null, Seq(Right(class1)))

    assert(class1.strictlyReachable.toSet == Set(class1, class2, class3))
  }

  /**
    * Test that `AgglomeratedClass.stronglyConnectedComponents` works for a linear graph.
    */
  @Test
  def stronglyConnectedComponentsWorks(): Unit = {
    val class1 = new AgglomeratedClass(null)
    val class2 = new AgglomeratedClass(null)
    val class3 = new AgglomeratedClass(null)

    class1.nodes += AgglomeratedNode(null, Seq(Right(class2)))
    class2.nodes += AgglomeratedNode(null, Seq(Right(class3)))
    class3.nodes += AgglomeratedNode(null, Seq())

    assert(class1.stronglyConnectedComponents.map(_.toSet).toSet == Set(Set(class1), Set(class2), Set(class3)))
  }

  /**
    * Test that `AgglomeratedClass.stronglyConnectedComponents` works for a graph containing a cycle.
    */
  @Test
  def stronglyConnectedComponentsWorksForCycle(): Unit = {
    val class1 = new AgglomeratedClass(null)
    val class2 = new AgglomeratedClass(null)
    val class3 = new AgglomeratedClass(null)

    class1.nodes += AgglomeratedNode(null, Seq(Right(class2)))
    class2.nodes += AgglomeratedNode(null, Seq(Right(class3)))
    class3.nodes += AgglomeratedNode(null, Seq(Right(class1)))

    assert(class1.stronglyConnectedComponents.map(_.toSet).toSet == Set(Set(class1, class2, class3)))
  }

  /**
    * Test that `AgglomeratedClass.stronglyConnectedComponents` works for a graph containing two components.
    */
  @Test
  def stronglyConnectedComponentsWorksForTwoComponents(): Unit = {
    val class1 = new AgglomeratedClass(null)
    val class2 = new AgglomeratedClass(null)
    val class3 = new AgglomeratedClass(null)

    class1.nodes += AgglomeratedNode(null, Seq(Right(class2)))
    class2.nodes += AgglomeratedNode(null, Seq(Right(class3)))
    class3.nodes += AgglomeratedNode(null, Seq(Right(class2)))

    assert(class1.stronglyConnectedComponents.map(_.toSet).toSet == Set(Set(class1), Set(class2, class3)))
  }
}
