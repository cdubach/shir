package eqsat.solver

import org.junit.Test

class Z3PyTest {

  @Test
  def testPseudoBooleanLessThanOrEqual(): Unit = {
    val x = BooleanVar("x")
    val y = BooleanVar("y")
    val problem = SatisfiabilityProblem(
      Seq(
        PseudoBooleanLessThanOrEqual(Seq((x, 1), (y, 2)), 1),
        x || y))
    val sol = Z3Py().solve(problem)

    assert(sol.nonEmpty)
    assert(sol.get.apply(x) == BooleanConstant(true))
    assert(sol.get.apply(y) == BooleanConstant(false))
  }

  @Test
  def testMinimizationProcess1(): Unit = {
    val x = IntVar("x")
    val problem = MinimizationProblem(Seq.empty, x * x)
    val sol = Z3Py().solve(problem)

    // y = x^2: quadratic function with global minima of x = 0
    assert(sol.nonEmpty)
    assert(sol.get.apply(x) == IntConstant(0))
  }

  @Test
  def testMinimizationProcess2(): Unit = {
    val x = IntVar("x")
    val problem = MinimizationProblem(Seq.empty, x * x)
    val sol = IterativeZ3Py().solve(problem)

    // y = x^2: quadratic function with global minima of x = 0
    assert(sol.nonEmpty)
    assert(sol.get.apply(x) == IntConstant(0))
  }

  @Test
  def testIterativeTimeout1(): Unit = {
    import scala.concurrent.duration._

    val x = IntVar("x")
    val problem = MinimizationProblem(Seq.empty, x)
    val sol = IterativeZ3Py().solve(problem, 1.second)

    // y = x: linear function with global minima of x = -infty,
    //
    // but since the solver works iteratively, it will pick some x that
    // satisfies y = x < previous-maximum.
    assert(sol.nonEmpty)
  }

  @Test
  def testIterativeTimeout2(): Unit = {
    import scala.concurrent.duration._

    val x = IntVar("x")
    val problem = MinimizationProblem(Seq.empty, x)
    val sol = IterativeOptimizer(Z3Py(), 1.second).solve(problem)

    // y = x: linear function with global minima of x = -infty,
    //
    // but since the solver works iteratively, it will pick some x that
    // satisfies y = x < previous-maximum.
    assert(sol.nonEmpty)
  }
}
