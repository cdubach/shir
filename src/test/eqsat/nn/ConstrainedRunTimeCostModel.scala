package eqsat.nn

import algo.eqsat.TilingRewriteRules
import algo.{Add, AlgoDataTypeT, ClipBankersRound, ConstantInteger, Drop, Fold, Input, IntTypeT, Join, Map, MapAsync, MapParallel, Max, Mul, Pad, Reduce, Repeat, ResizeInteger, SeqTypeT, SlideGeneral, Split, TransposeND, Tuple, Zip}
import algo.nn.{DotProductLayer, DotProductLayerExpr, NetworkLayerExpr, ParallelDotProductsLayer, ParallelDotProductsLayerExpr, TransformAndOutline}
import backend.hdl.arch.costModel.CostModel.arria10MulBitWidthCheck
import core.{Expr, FunctionCall, LambdaT, ParamUse, Type, TypeChecker}
import eqsat.{DeBruijnLambda, DeBruijnParamUse, EClassT, ENodeT, ExprHelpers, OutliningExtractorCostModel, solver}
import eqsat.solver.{AgglomeratedClass, AgglomeratedNode, Formula, IntConstant}
import lift.arithmetic.ArithExpr

case class ConstrainedRunTimeCostModel(maxDotProductVectorization: Long = 64,
                                       maxDSPBlocks: Int = 1533 * 2,
                                       maxRAM: Int = 16 * 1024 * 1024 * 8,
                                       functionCallCost: Int = 5) extends OutliningExtractorCostModel {

  /**
   * Gets the number of dimensions in this cost model. Each dimension operates as a separate cost. Some dimensions may
   * be bounded by a maximal cost via the `budget` function. Costs are aggregated into a single optimization problem
   * using the `minimizationTarget` function.
   */
  override val dimensions: Int = 5

  /**
   * Computes the cost of a single instantiation of an e-node.
   *
   * @param dimension The cost dimension to generate a formula for.
   * @param node      The node to model a cost for.
   * @param argCosts  The use cost of each argument of `node`, expressed as a formula.
   * @return A formula that expresses the cost of `node`, including the use cost of its arguments.
   */
  override def useCost(dimension: Int)(node: ENodeT, argCosts: Seq[Formula]): Formula = {
    dimension match {
      case 0 => estimateRunCycles(node, argCosts)
      case 1 => estimateDSPBlocks(node, argCosts)
      case 2 => estimateBuffers(node, argCosts)
      case 3 => estimateDSPComputations(node, argCosts)
      case 4 => estimateDSPCycles(node, argCosts)
    }
  }

  /**
   * The cost associated with calling an outlined version of `node`.
   *
   * @param dimension The cost dimension to generate a formula for.
   * @param node      The node to model a cost for.
   * @param argCosts  The use cost of each argument of `node`, expressed as a formula.
   * @return A formula that expresses the cost of making a call to an outlined version of `node`.
   */
  override def callCost(dimension: Int)(node: ENodeT, argCosts: Seq[Formula]): Formula = {
    dimension match {
      case 0 => IntConstant(functionCallCost) + estimateRunCycles(node, argCosts)
      case 1 | 2 => IntConstant(0)
      case 3 => estimateDSPComputations(node, argCosts)
      case 4 => estimateDSPCycles(node, argCosts)
    }
  }

  override def totalCost(dimension: Int)(rootCost: Formula, outlinedExprCosts: Seq[Formula]): Formula = {
    dimension match {
      case 0 | 3 | 4 => rootCost
      case 1 | 2 => (rootCost +: outlinedExprCosts).reduce(_ + _)
    }
  }

  /**
   * The budget available for a certain cost dimension. The cost for that dimension cannot exceed the budget.
   *
   * @param dimension The dimension to determine a budget for.
   * @return A budget, if one exists; otherwise, `None`.
   */
  override def budget(dimension: Int): Option[Formula] = {
    dimension match {
      case 0 | 3 | 4 => None
      case 1 => Some(IntConstant(maxDSPBlocks))
      case 2 => Some(IntConstant(maxRAM))
    }
  }

  /**
   * The total minimization target to send to the solver.
   *
   * @param dimensionCosts The total costs of each dimension.
   * @return A formula describing the minimization target.
   */
  override def minimizationTarget(dimensionCosts: Seq[Formula]): Formula = dimensionCosts.head

  private def estimateDSPBlocks(node: ENodeT, argCosts: Seq[Formula]): Formula = {
    def withArgCost(value: Formula): Formula = argCosts.foldLeft(value)(_ + _)

    node.expr match {
      case Mul(i, _) => withArgCost(IntConstant(dspCountForMul(i.t)))

      case DotProductLayer(left, _, _) =>
        val vecType = left.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalLong
        val sliceSize = maxDotProductVectorization min size
        val elementType = vecType.et
        withArgCost(IntConstant(sliceSize * dspCountForMul(algo.TupleType(elementType, elementType))))

      case ParallelDotProductsLayer(left, _, simultaneousProducts, _) =>
        val matrixType = left.t.asInstanceOf[SeqTypeT]
        val vecType = matrixType.et.asInstanceOf[SeqTypeT]
        val vecSize = vecType.len.ae.evalLong
        val vectorChunkSize = simultaneousProducts.ae.evalLong
        val sliceSize = maxDotProductVectorization min vecSize
        val elementType = vecType.et
        withArgCost(IntConstant(vectorChunkSize * sliceSize * dspCountForMul(algo.TupleType(elementType, elementType))))

      case MapParallel(_, vectors, _) =>
        val vecType = vectors.head.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalLong
        IntConstant(size) * argCosts.head + sum(argCosts.tail)

      case layer: NetworkLayerExpr => exprCost(1)(TypeChecker.check(layer.expand()), node.args.zip(argCosts).toMap)
      case _ => sum(argCosts)
    }
  }

  /**
   * Detect buffers implied by SlideGenerals from ConvolutionLayer.
   */
  private def estimateBuffers(node: ENodeT, argCosts: Seq[Formula]): Formula = {
    def withArgCost(value: Formula): Formula = argCosts.foldLeft(value)(_ + _)

    node.expr match {
      case SlideGeneral(input, _, step, _) if step.ae.evalInt == 1 =>
        val inputType = input.t
        val bufferSize = sizeOf(inputType)
        withArgCost(IntConstant(bufferSize.evalLong))

      case Repeat(input, _, _) =>
        val inputType = input.t
        val bufferSize = sizeOf(inputType)
        withArgCost(IntConstant(bufferSize.evalLong))

      case MapParallel(_, vectors, _) =>
        val vecType = vectors.head.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + sum(argCosts.tail)

      case _: DotProductLayerExpr => sum(argCosts)
      case _: ParallelDotProductsLayerExpr => sum(argCosts)
      case layer: NetworkLayerExpr => exprCost(2)(TypeChecker.check(layer.expand()), node.args.zip(argCosts).toMap)

      case _ => sum(argCosts)
    }
  }

  private def estimateDSPComputations(node: ENodeT, argCosts: Seq[Formula]): Formula = {
    def withArgCost(value: Formula): Formula = argCosts.foldLeft(value)(_ + _)

    node.expr match {
      case Mul(i, _) => withArgCost(IntConstant(dspCountForMul(i.t)))
      case DotProductLayer(left, _, _) =>
        val vecType = left.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        val elementType = vecType.et
        withArgCost(IntConstant(size * dspCountForMul(algo.TupleType(elementType, elementType))))

      case ParallelDotProductsLayer(left, _, _, _) =>
        val matrixType = left.t.asInstanceOf[SeqTypeT]
        val vecType = matrixType.et.asInstanceOf[SeqTypeT]
        val vecSize = vecType.len.ae.evalInt
        val matrixSize = matrixType.len.ae.evalInt
        val elementType = vecType.et
        withArgCost(IntConstant(matrixSize * vecSize * dspCountForMul(algo.TupleType(elementType, elementType))))

      case layer: NetworkLayerExpr => exprCost(3)(TypeChecker.check(layer.expand()), node.args.zip(argCosts).toMap)

      case Map(_, vector, _) =>
        val vecType = vector.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + argCosts(1)

      case MapAsync(_, vectors, _) =>
        val vecType = vectors.head.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + sum(argCosts.tail)

      case MapParallel(_, vectors, _) =>
        val vecType = vectors.head.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + sum(argCosts.tail)

      case Fold(_, vector, _) =>
        val vecType = vector.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size - 1) * argCosts.head + argCosts(1)

      case Reduce(_, _, vector, _) =>
        val vecType = vector.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + argCosts(1) + argCosts(2)

      case _ => sum(argCosts)
    }
  }

  def dspCountForMul(t: Type): Int = {
    t match {
      case algo.TupleType(Seq(sit1: algo.SignedIntTypeT, sit2: algo.SignedIntTypeT)) if
        arria10MulBitWidthCheck(sit1.width.ae.evalDouble, sit2.width.ae.evalDouble) => 1
      case algo.TupleType(Seq(it1: algo.IntTypeT, it2: algo.IntTypeT)) if
        arria10MulBitWidthCheck(it1.width.ae.evalDouble, it2.width.ae.evalDouble) => 1
      case algo.TupleType(Seq(_: algo.FloatTypeT, _: algo.FloatTypeT)) => 2
      case _ => 2
    }
  }

  /**
   * Computes the size of a type in bits.
   * @param t The type to compute the size of.
   * @return The size of `t` in bits.
   */
  private def sizeOf(t: Type): ArithExpr = {
    t match {
      case seqT: SeqTypeT => sizeOf(seqT.et) * seqT.len.ae
      case intT: IntTypeT => intT.width.ae
      case _ => ???
    }
  }

  private def sum(formulas: Seq[Formula]): Formula = {
    if (formulas.isEmpty) {
      IntConstant(0)
    } else {
      formulas.reduce(_ + _)
    }
  }

  private def max(formulas: Seq[Formula]): Formula = {
    if (formulas.isEmpty) {
      IntConstant(0)
    } else {
      formulas.reduce(_ max _)
    }
  }

  private def estimateRunCycles(node: ENodeT, argCosts: Seq[Formula]): Formula = {
    node.expr match {
      case Map(_, vector, _) =>
        val vecType = vector.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + argCosts(1)

      case MapAsync(_, vectors, _) =>
        val vecType = vectors.head.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + max(argCosts.tail)

      case MapParallel(_, _, _) =>
        argCosts.head + max(argCosts.tail)

      case Fold(_, vector, _) =>
        val vecType = vector.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + argCosts(1)

      case Reduce(_, _, vector, _) =>
        val vecType = vector.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + (argCosts(1) max argCosts(2))

      case Split(_, _, _) => max(argCosts)
      case Join(_, _) => max(argCosts)
      case SlideGeneral(_, _, _, _) => max(argCosts)
      case TransposeND(_, _, _) => max(argCosts)
      case Zip(_, _, _) => max(argCosts)
      case Drop(_, _, _, _) => max(argCosts)
      case Repeat(_, _, _) => max(argCosts)

      case Pad(_, firstElements, lastElements, _, _) =>
        max(argCosts) + IntConstant(firstElements.ae.evalInt) + IntConstant(lastElements.ae.evalInt)

      case Input(_, elementType, dimensions, _) =>
        val bitWidth = elementType.asInstanceOf[IntTypeT].width.ae.evalInt
        val totalSize = dimensions.map(_.ae).reduce(_ * _).evalInt
        IntConstant(bitWidth * totalSize / 512 * 3)

      case Tuple(_, _) => max(argCosts)

      case DotProductLayer(left, _, _) =>
        val vecType = left.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalLong
        val sliceSize = maxDotProductVectorization min size
        val sliceCount = size / sliceSize
        val leftCost = argCosts.head
        val rightCost = argCosts(1)
        val inputCost = leftCost max rightCost
        val sliceCost = 1
        IntConstant(sliceCount * sliceCost) + inputCost

      case ParallelDotProductsLayer(left, _, simultaneousProducts, _) =>
        // Similar computation as for DotProductLayer, but with an additional dimension. The number of vectors does not
        // affect the run time cost, as they are processed in parallel. This is why we only consider the size of the
        // vectors, not the number of vectors. This parallelism comes at the cost of additional DSP blocks, computed
        // in the `estimateDSPBlocks` function.
        val matrixType = left.t.asInstanceOf[SeqTypeT]
        val vecType = matrixType.et.asInstanceOf[SeqTypeT]
        val vecSize = vecType.len.ae.evalLong
        val chunkCount = matrixType.len.ae.evalLong / simultaneousProducts.ae.evalLong
        val sliceSize = maxDotProductVectorization min vecSize
        val sliceCount = vecSize / sliceSize
        val leftCost = argCosts.head
        val rightCost = argCosts(1)
        val inputCost = leftCost max rightCost
        val sliceCost = 1
        IntConstant(chunkCount * sliceCount * sliceCost) + inputCost

      case Mul(_) => IntConstant(1) + max(argCosts)
      case Add(_) => IntConstant(1) + max(argCosts)
      case Max(_) => IntConstant(1) + max(argCosts)
      case ClipBankersRound(_, _, _, _) => IntConstant(1) + max(argCosts)
      case ResizeInteger(_, _, _) => IntConstant(1) + max(argCosts)

      case FunctionCall(_, _, _) => sum(argCosts)

      case _: DeBruijnLambda => max(argCosts)
      case _: LambdaT => max(argCosts)

      case ParamUse(_) => IntConstant(1)
      case _: DeBruijnParamUse => IntConstant(1)
      case ConstantInteger(_) => IntConstant(1)

      case layer: NetworkLayerExpr => {
        val expanded = TypeChecker.check(layer.expand())
        val argToCostMap = node.args.zip(argCosts).toMap
        exprCost(0)(expanded, argToCostMap)
      }
    }
  }

  /**
    * Counts the number of cycles of DSP activity required to compute the result of a node.
    * @param node The node to count the DSP cycles for.
    * @param argCosts The number of cycles of the arguments of the node.
    * @return The number of cycles of DSP activity required to compute the result of the node.
    */
  private def estimateDSPCycles(node: ENodeT, argCosts: Seq[Formula]): solver.Formula = {
    node.expr match {
      case Mul(_, _) => IntConstant(1)

      case DotProductLayer(left, _, _) =>
        val vecType = left.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalLong
        val sliceSize = maxDotProductVectorization min size
        val sliceCount = size / sliceSize
        IntConstant(sliceCount) + max(argCosts)

      case ParallelDotProductsLayer(left, _, simultaneousProducts, _) =>
        // Similar computation as for DotProductLayer, but with an additional dimension. The number of vectors does not
        // affect the run time cost, as they are processed in parallel. This is why we only consider the size of the
        // vectors, not the number of vectors. This parallelism comes at the cost of additional DSP blocks, computed
        // in the `estimateDSPBlocks` function.
        val matrixType = left.t.asInstanceOf[SeqTypeT]
        val vecType = matrixType.et.asInstanceOf[SeqTypeT]
        val vecSize = vecType.len.ae.evalLong
        val chunkCount = matrixType.len.ae.evalLong / simultaneousProducts.ae.evalLong
        val sliceSize = maxDotProductVectorization min vecSize
        val sliceCount = vecSize / sliceSize
        val leftCost = argCosts.head
        val rightCost = argCosts(1)
        IntConstant(chunkCount * sliceCount) + (leftCost max rightCost)

      case Map(_, vector, _) =>
        val vecType = vector.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + argCosts(1)

      case MapAsync(_, vectors, _) =>
        val vecType = vectors.head.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + max(argCosts.tail)

      case MapParallel(_, _, _) =>
        argCosts.head + max(argCosts.tail)

      case Fold(_, vector, _) =>
        val vecType = vector.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + argCosts(1)

      case Reduce(_, _, vector, _) =>
        val vecType = vector.t.asInstanceOf[SeqTypeT]
        val size = vecType.len.ae.evalInt
        IntConstant(size) * argCosts.head + (argCosts(1) max argCosts(2))

      case FunctionCall(_, _, _) => sum(argCosts)

      case _: DeBruijnLambda => max(argCosts)
      case _: LambdaT => max(argCosts)

      case layer: NetworkLayerExpr =>
        val expanded = TypeChecker.check(layer.expand())
        val argToCostMap = node.args.zip(argCosts).toMap
        exprCost(4)(expanded, argToCostMap)

      case _ => max(argCosts)
    }
  }

  /**
   * Computes the use cost of a single instantiation of an e-node.
   * @param dimension The cost dimension to generate a formula for.
   * @param expr The expression to model a cost for.
   * @param argCosts The use cost of each e-class appearing in `expr`, expressed as a formula.
   * @return A formula that expresses the cost of `expr`, including the use cost of its arguments.
   */
  private def exprCost(dimension: Int)(expr: Expr, argCosts: Map[EClassT, solver.Formula]): solver.Formula =
    TransformAndOutline(TilingRewriteRules).exprCost(this, dimension)(expr, argCosts)
}
