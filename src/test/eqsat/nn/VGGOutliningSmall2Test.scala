package eqsat.nn

import algo.eqsat.TilingRewriteRules
import algo.nn.{ConvolutionLayer, InputLayer, PoolingLayer, ReLULayer, RoundingLayer, SaveLayer, TransformAndOutline}
import core.TypeChecker
import eqsat.nn.Helpers.StolesCostModel
import eqsat.solver.{IterativeOptimizer, Z3Py}
import org.junit.Test

import scala.concurrent.duration.DurationInt

class VGGOutlingSmall2Test {
  val transformAndOutline = TransformAndOutline(TilingRewriteRules)

  @Test
  def testVGG(): Unit = {
    val input = InputLayer(64, 32, 32, 8, 0)
    /*val conv0 = ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)
    val round0 = RoundingLayer(conv0, 8, 8)
    val relu0 = ReLULayer(round0)*/

    val conv1 = ConvolutionLayer(input, 64, 64, 3, 1, 8, 1)
    val round1 = RoundingLayer(conv1, 8, 8)
    val relu1 = ReLULayer(round1)
    val conv1Pool = PoolingLayer(relu1, 2)

    val conv2 = ConvolutionLayer(conv1Pool, 64, 128, 3, 1, 8, 2)
    val round2 = RoundingLayer(conv2, 8, 8)
    val relu2 = ReLULayer(round2)
    val res = TypeChecker.check(SaveLayer(relu2))

    val costModel = StolesCostModel(maxDotProductVectorization=128, maxDSPBlocks = 128, maxRAM = 16384 * 16, solverInterface = IterativeOptimizer(Z3Py(), 1.hours))
    val initCosts = transformAndOutline.computeCosts(res, costModel)
    println(initCosts)
    val result = transformAndOutline.tryOutline(res, tile=true, pad=true, maxSteps = 14, costModel = costModel).get
    println(result.costs)
    println(s"DSP efficiency: ${initCosts(3).toDouble / result.costs(3).toDouble}")
    //    IRDotGraph(result).show()
    //    println(ExprToCode(result))
    //    ???
    //    HDLProject("vgg-all", result).writeHDLFiles()
  }
}
