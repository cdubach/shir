package eqsat.nn

import algo.eqsat.TilingRewriteRules
import algo.nn.{CostModel, OutliningResult, TransformAndOutline}
import core.Expr
import eqsat.nn.Helpers.DPStolesCostModel
import eqsat.{HierarchicalStopwatch, LimitedOutlining, MeasuredActivity, NeverOutline}
import eqsat.solver.{IterativeOptimizer, Optimizer, Z3Py}
import org.junit.Test

import scala.concurrent.duration.DurationInt

class OptimizerComparison {
  val transformAndOutline = TransformAndOutline(TilingRewriteRules)

  /**
    * The budgets for DSP blocks to evaluate.
    */
  private val dspBudget = 1533 * 2

  /**
    * The budgets for RAM to evaluate.
    */
  private val ramBudget = 16 * 1024 * 1024 * 8

  /**
    * The maximum vectorization factor for dot products.
    */
  private val maxDotProductVectorization = 256

  def timeExtractor(costModel: Expr => CostModel): (Option[OutliningResult], MeasuredActivity) = {
    val network = Networks.VGGUpToLayerN(6)

    val model = costModel(network)

    val saturated = transformAndOutline.padAndTile(
      network,
      pad = true,
      tile = true,
      sequentialize = true,
      debug = false,
      costModel = model)

    val outliningStopwatch = HierarchicalStopwatch.start("outlining")
    val outlined = transformAndOutline.tryOutline(
      saturated,
      pad = true,
      tile = true,
      sequentialize = true,
      debug = false,
      costModel = model)
    val outliningTime = outliningStopwatch.complete()

    (outlined, outliningTime)
  }

  private def optimizerToCostModel(optimizer: Optimizer): CostModel = {
    Helpers.StolesCostModel(
      maxDotProductVectorization, maxDSPBlocks = dspBudget, maxRAM = ramBudget, solverInterface = optimizer)
  }

  private def shrinkingOptimizer(network: Expr): Optimizer = {
    IterativeOptimizer(Z3Py(), 1.hour)
  }

  private def plainZ3(network: Expr): Optimizer = {
    Z3Py()
  }

  private def dynamicProgramming(network: Expr): CostModel = {
    DPStolesCostModel(maxDotProductVectorization, maxDSPBlocks = dspBudget, maxRAM = ramBudget)
  }

  @Test
  def compareOptimizers(): Unit = {
//    val (_, dpTime) = timeExtractor(dynamicProgramming)
//    println(s"Dynamic programming time: ${dpTime.duration.toSeconds}s")

    val (_, noOutliningTime) = timeExtractor(expr => Helpers.StolesCostModel(
      maxDotProductVectorization,
      maxDSPBlocks = dspBudget,
      maxRAM = ramBudget,
      solverInterface = shrinkingOptimizer(expr),
      outliningStrategy = NeverOutline))
    println(s"No outlining time: ${noOutliningTime.duration.toSeconds}s")

    val (_, shrinkingTime) = timeExtractor(shrinkingOptimizer _ andThen optimizerToCostModel)
    println(s"Shrinking time: ${shrinkingTime.duration.toSeconds}s")

    val (_, limitedOutliningTime) = timeExtractor(expr => Helpers.StolesCostModel(
      maxDotProductVectorization,
      maxDSPBlocks = dspBudget,
      maxRAM = ramBudget,
      solverInterface = shrinkingOptimizer(expr),
      outliningStrategy = LimitedOutlining(3)))
    println(s"Limited outlining time: ${limitedOutliningTime.duration.toSeconds}s")

    val (_, plainZ3Time) = timeExtractor(plainZ3 _ andThen optimizerToCostModel)
    println(s"Plain Z3 time: ${plainZ3Time.duration.toSeconds}s")
  }
}
