package eqsat.nn

import algo.eqsat.HierarchicalTilingRewriteRules
import algo.nn.{ConvolutionLayer, DotProductLayer, DotProductMatVecLayer, InputLayer, MatVecConvolutionLayer, MatVecLayer, PaddedConvHWLayer, PaddedMatVecLayer, ParallelDotProductsLayer, TiledConvHWLayer, TiledConvICHLayer, TiledConvOCHLayer, TiledDotProductLayer, TiledMatVecLayer, TransformAndOutline}
import algo.{AlgoLambda, SeqType, SignedIntType}
import core.{Expr, ExprVar, FunType, LambdaT, ParamDef, ParamUse, TypeChecker}
import eqsat.{DeBruijnLambda, DeBruijnParamUse, DeBruijnTransform, EGraphNullObserver, Pattern}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class HierarchicalTilingRulesTest {
  val transformAndOutline = TransformAndOutline(HierarchicalTilingRewriteRules)
  implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

  @Test
  def wrapMatVecTerminates(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val matrix = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector = ParamDef(SeqType(SignedIntType(8), 256))
    val matVec = DotProductMatVecLayer.from(MatVecLayer(ParamUse(matrix), ParamUse(vector)))
    val deBruijnMatVec = DeBruijnTransform.forward(matVec)

    eGraph.add(TypeChecker.check(deBruijnMatVec))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.wrapMatVec))

    // If we reach this point, then saturation must have terminated. At this point, we want to check that the lambda
    // abstraction has been created.
    val expected = TypeChecker.check(DeBruijnTransform.forward(AlgoLambda(Seq(matrix, vector).reverse, matVec)))
    assert(eGraph.contains(expected))
  }

  @Test
  def padMatVecWorks(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val matrix1 = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector1 = ParamDef(SeqType(SignedIntType(8), 256))
    val matVec1 = DotProductMatVecLayer.from(MatVecLayer(ParamUse(matrix1), ParamUse(vector1)))
    val deBruijnMatVec1 = DeBruijnTransform.forward(matVec1)

    val matrix2 = ParamDef(SeqType(SeqType(SignedIntType(8), 512), 8))
    val vector2 = ParamDef(SeqType(SignedIntType(8), 512))
    val matVec2 = DotProductMatVecLayer.from(MatVecLayer(ParamUse(matrix2), ParamUse(vector2)))
    val deBruijnMatVec2 = DeBruijnTransform.forward(matVec2)

    val mv1 = eGraph.add(TypeChecker.check(deBruijnMatVec1))
    val mv2 = eGraph.add(TypeChecker.check(deBruijnMatVec2))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.padMatVec))

    // Check that the matrix-vector padding rule has fired for mv1
    val expected = Pattern(
      PaddedMatVecLayer(
        ExprVar(matrix1.t),
        ExprVar(vector1.t),
        256,
        ExprVar(FunType(Seq(matrix2.t, vector2.t).reverse, TypeChecker.check(deBruijnMatVec2).t))))
    assert(eGraph.contains(expected, mv1))
  }

  @Test
  def tileMatVecWorks(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val matrix1 = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 8))
    val vector1 = ParamDef(SeqType(SignedIntType(8), 256))
    val matVec1 = DotProductMatVecLayer.from(MatVecLayer(ParamUse(matrix1), ParamUse(vector1)))
    val deBruijnMatVec1 = DeBruijnTransform.forward(matVec1)

    val matrix2 = ParamDef(SeqType(SeqType(SignedIntType(8), 512), 8))
    val vector2 = ParamDef(SeqType(SignedIntType(8), 512))
    val matVec2 = DotProductMatVecLayer.from(MatVecLayer(ParamUse(matrix2), ParamUse(vector2)))
    val deBruijnMatVec2 = DeBruijnTransform.forward(matVec2)

    val mv1 = eGraph.add(TypeChecker.check(deBruijnMatVec1))
    val mv2 = eGraph.add(TypeChecker.check(deBruijnMatVec2))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.tileMatVec))

    // Check that the matrix-vector padding rule has fired for mv1
    val expected = Pattern(
      TiledMatVecLayer(
        ExprVar(matrix2.t),
        ExprVar(vector2.t),
        256,
        ExprVar(FunType(Seq(matrix1.t, vector1.t).reverse, TypeChecker.check(deBruijnMatVec1).t))))
    assert(eGraph.contains(expected, mv2))
  }

  @Test
  def wrapConvTerminates(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val input = InputLayer(3, 32, 32, 8, 0)
    val p = ParamDef(TypeChecker.check(input).t)
    val conv = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p), 3, 64, 3, 1, 8, 0))
    val deBruijnConv = DeBruijnTransform.forward(conv)

    val (inputT, weightT) = conv match {
      case MatVecConvolutionLayer(input, weight, _, _) => (TypeChecker.check(input).t, TypeChecker.check(weight).t)
    }

    eGraph.add(TypeChecker.check(deBruijnConv))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.wrapConv))

    // If we reach this point, then saturation must have terminated. At this point, we want to check that the lambda
    // abstraction has been created.
    val expected = DeBruijnLambda(inputT,
      DeBruijnLambda(weightT,
        MatVecConvolutionLayer(
          DeBruijnParamUse(1, inputT),
          DeBruijnParamUse(0, weightT),
          ExprVar())))
    assert(eGraph.contains(expected))
  }

  @Test
  def padConvHWWorks(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val input1 = InputLayer(3, 32, 32, 8, 0)
    val p1 = ParamDef(TypeChecker.check(input1).t)
    val conv1 = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p1), 3, 64, 3, 1, 8, 0))
    val deBruijnConv1 = DeBruijnTransform.forward(conv1)

    val (input1T, weight1T) = conv1 match {
      case MatVecConvolutionLayer(input, weight, _, _) => (TypeChecker.check(input).t, TypeChecker.check(weight).t)
    }

    val input2 = InputLayer(3, 34, 34, 8, 1)
    val p2 = ParamDef(TypeChecker.check(input2).t)
    val conv2 = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p2), 3, 64, 3, 1, 8, 1))
    val deBruijnConv2 = DeBruijnTransform.forward(conv2)

    val (input2T, weight2T) = conv2 match {
      case MatVecConvolutionLayer(input, weight, _, _) => (TypeChecker.check(input).t, TypeChecker.check(weight).t)
    }

    val c1 = eGraph.add(TypeChecker.check(deBruijnConv1))
    val c2 = eGraph.add(TypeChecker.check(deBruijnConv2))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.padConvHW))

    // Check that the convolution padding rule has fired for c1
    val expected = Pattern(
      PaddedConvHWLayer(
        ExprVar(input1T),
        ExprVar(weight1T),
        ExprVar(FunType(Seq(input2T, weight2T).reverse, TypeChecker.check(deBruijnConv2).t))))
    assert(eGraph.contains(expected, c1))
  }

  @Test
  def tileConvHWWorks(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val input1 = InputLayer(3, 32, 32, 8, 0)
    val p1 = ParamDef(TypeChecker.check(input1).t)
    val conv1 = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p1), 3, 64, 3, 1, 8, 0))
    val deBruijnConv1 = DeBruijnTransform.forward(conv1)

    val (input1T, weight1T) = conv1 match {
      case MatVecConvolutionLayer(input, weight, _, _) => (TypeChecker.check(input).t, TypeChecker.check(weight).t)
    }

    val input2 = InputLayer(3, 64, 64, 8, 1)
    val p2 = ParamDef(TypeChecker.check(input2).t)
    val conv2 = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p2), 3, 64, 3, 1, 8, 1))
    val deBruijnConv2 = DeBruijnTransform.forward(conv2)

    val (input2T, weight2T) = conv2 match {
      case MatVecConvolutionLayer(input, weight, _, _) => (TypeChecker.check(input).t, TypeChecker.check(weight).t)
    }

    val c1 = eGraph.add(TypeChecker.check(deBruijnConv1))
    val c2 = eGraph.add(TypeChecker.check(deBruijnConv2))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.tileConvHW))

    // Check that the convolution tiling rule has fired for c2
    val expected = Pattern(
      TiledConvHWLayer(
        ExprVar(input2T),
        ExprVar(weight2T),
        ExprVar(FunType(Seq(input1T, weight1T).reverse, TypeChecker.check(deBruijnConv1).t))))
    assert(eGraph.contains(expected, c2))
  }

  @Test
  def tileConvOCHWorks(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val input1 = InputLayer(3, 32, 32, 8, 0)
    val p1 = ParamDef(TypeChecker.check(input1).t)
    val conv1 = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p1), 3, 32, 3, 1, 8, 0))
    val deBruijnConv1 = DeBruijnTransform.forward(conv1)

    val (input1T, weight1T) = conv1 match {
      case MatVecConvolutionLayer(input, weight, _, _) => (TypeChecker.check(input).t, TypeChecker.check(weight).t)
    }

    val input2 = InputLayer(3, 32, 32, 8, 1)
    val p2 = ParamDef(TypeChecker.check(input2).t)
    val conv2 = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p2), 3, 64, 3, 1, 8, 1))
    val deBruijnConv2 = DeBruijnTransform.forward(conv2)

    val (input2T, weight2T) = conv2 match {
      case MatVecConvolutionLayer(input, weight, _, _) => (TypeChecker.check(input).t, TypeChecker.check(weight).t)
    }

    val c1 = eGraph.add(TypeChecker.check(deBruijnConv1))
    val c2 = eGraph.add(TypeChecker.check(deBruijnConv2))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.tileConvOCH))

    // Check that the convolution tiling rule has fired for c2
    val expected = Pattern(
      TiledConvOCHLayer(
        ExprVar(input2T),
        ExprVar(weight2T),
        ExprVar(FunType(Seq(input1T, weight1T).reverse, TypeChecker.check(deBruijnConv1).t))))
    assert(eGraph.contains(expected, c2))
  }

  @Test
  def tileConvICHWorks(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val input1 = InputLayer(3, 32, 32, 8, 0)
    val p1 = ParamDef(TypeChecker.check(input1).t)
    val conv1 = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p1), 3, 64, 3, 1, 8, 0))
    val deBruijnConv1 = DeBruijnTransform.forward(conv1)

    val (input1T, weight1T) = conv1 match {
      case MatVecConvolutionLayer(input, weight, _, _) => (TypeChecker.check(input).t, TypeChecker.check(weight).t)
    }

    val input2 = InputLayer(6, 32, 32, 8, 1)
    val p2 = ParamDef(TypeChecker.check(input2).t)
    val conv2 = MatVecConvolutionLayer.from(ConvolutionLayer(ParamUse(p2), 6, 64, 3, 1, 8, 1))
    val deBruijnConv2 = DeBruijnTransform.forward(conv2)

    val (input2T, weight2T) = conv2 match {
      case MatVecConvolutionLayer(input, weight, _, _) => (TypeChecker.check(input).t, TypeChecker.check(weight).t)
    }

    val c1 = eGraph.add(TypeChecker.check(deBruijnConv1))
    val c2 = eGraph.add(TypeChecker.check(deBruijnConv2))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.tileConvICH))

    // Check that the convolution tiling rule has fired for c2
    val expected = Pattern(
      TiledConvICHLayer(
        ExprVar(input2T),
        ExprVar(weight2T),
        ExprVar(FunType(Seq(input1T, weight1T).reverse, TypeChecker.check(deBruijnConv1).t))))
    assert(eGraph.contains(expected, c2))
  }

  @Test
  def tileDotProductWorks(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val vector1 = ParamDef(SeqType(SignedIntType(8), 512))
    val vector2 = ParamDef(SeqType(SignedIntType(8), 512))
    val dotProduct1 = DotProductLayer(ParamUse(vector1), ParamUse(vector2))
    val deBruijnDotProduct1 = DeBruijnTransform.forward(dotProduct1)

    val matrix1 = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 2))
    val matrix2 = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 2))
    val dotProduct2 = ParallelDotProductsLayer(ParamUse(matrix1), ParamUse(matrix2), 2)
    val deBruijnDotProduct2 = DeBruijnTransform.forward(dotProduct2)

    val dp1 = eGraph.add(TypeChecker.check(deBruijnDotProduct1))
    val dp2 = eGraph.add(TypeChecker.check(deBruijnDotProduct2))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.tileDotProduct))

    // Check that the dot product tiling rule has fired for dp2
    val expected = Pattern(
      TiledDotProductLayer(
        ExprVar(vector1.t),
        ExprVar(vector2.t),
        256,
        ExprVar(FunType(Seq(matrix1.t, matrix2.t).reverse, TypeChecker.check(deBruijnDotProduct2).t))))
    assert(eGraph.contains(expected, dp1))
  }

  @Test
  def reparallelizeDotProductsWorks(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val matrix1 = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 16))
    val matrix2 = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 16))
    val dotProduct = ParallelDotProductsLayer(ParamUse(matrix1), ParamUse(matrix2), 16)
    val deBruijnDotProduct = DeBruijnTransform.forward(dotProduct)

    val (matrix1T, matrix2T) = dotProduct match {
      case ParallelDotProductsLayer(matrix1, matrix2, _, _) => (TypeChecker.check(matrix1).t, TypeChecker.check(matrix2).t)
    }

    val dp = eGraph.add(TypeChecker.check(deBruijnDotProduct))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.reparallelizeDotProducts))

    // Check that the dot product reparallelization rule has fired for dp
    val expected = ParallelDotProductsLayer(ExprVar(matrix1T), ExprVar(matrix2T), 2)
    assert(eGraph.contains(expected, dp))

    val expected2 = ParallelDotProductsLayer(ExprVar(matrix1T), ExprVar(matrix2T), 4)
    assert(eGraph.contains(expected2, dp))

    val expected3 = ParallelDotProductsLayer(ExprVar(matrix1T), ExprVar(matrix2T), 8)
    assert(eGraph.contains(expected3, dp))
  }

  @Test
  def sequentializeDotProductsWorks(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val matrix1 = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 16))
    val matrix2 = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 16))
    val dotProduct = ParallelDotProductsLayer(ParamUse(matrix1), ParamUse(matrix2), 16)
    val deBruijnDotProduct = DeBruijnTransform.forward(dotProduct)

    val (matrix1T, matrix2T) = dotProduct match {
      case ParallelDotProductsLayer(matrix1, matrix2, _, _) => (TypeChecker.check(matrix1).t, TypeChecker.check(matrix2).t)
    }

    val dp = eGraph.add(TypeChecker.check(deBruijnDotProduct))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.sequentializeDotProducts))

    // Check that the dot product reparallelization rule has fired for dp
    val expected = ParallelDotProductsLayer(ExprVar(matrix1T), ExprVar(matrix2T), 1)
    assert(eGraph.contains(expected, dp))
  }

  @Test
  def wrapParallelDotProductsTerminates(): Unit = {
    val eGraph = transformAndOutline.createEGraph(EGraphNullObserver)

    val matrix1 = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 16))
    val matrix2 = ParamDef(SeqType(SeqType(SignedIntType(8), 256), 16))
    val dotProduct = ParallelDotProductsLayer(ParamUse(matrix1), ParamUse(matrix2), 16)
    val deBruijnDotProduct = DeBruijnTransform.forward(dotProduct)

    val (matrix1T, matrix2T) = dotProduct match {
      case ParallelDotProductsLayer(matrix1, matrix2, _, _) => (TypeChecker.check(matrix1).t, TypeChecker.check(matrix2).t)
    }

    val dp = eGraph.add(TypeChecker.check(deBruijnDotProduct))
    eGraph.rebuild()

    eGraph.saturate(Seq(HierarchicalTilingRewriteRules.wrapParallelDotProducts))

    // Check that wrapping was successful
    val expected = DeBruijnLambda(
      matrix1T,
      DeBruijnLambda(
        matrix2T,
        ParallelDotProductsLayer(
          DeBruijnParamUse(1, matrix1T),
          DeBruijnParamUse(0, matrix2T),
          16)))
    assert(eGraph.contains(expected))
  }
}
