package eqsat.nn

import algo.eqsat.TilingRewriteRules
import algo.nn.TransformAndOutline
import org.junit.Test

/**
  * A test that evaluates STOLES by comparing it to a set of baselines.
  */
class ComparisonToBaselines {
  val transformAndOutline = TransformAndOutline(TilingRewriteRules)

  /**
    * The maximum vectorization factor for dot products.
    */
  private val maxDotProductVectorization = 256

  private val maxDSPs = 1500

  private val maxRAM = 16 * 1024 * 1024 * 8

  private val baselines = Baselines(maxDotProductVectorization, maxDSPs, maxRAM)

  private def printResult(key: String, result: ExperimentalResult): Unit = {
    result match {
      case ExperimentalFailure =>
        println(s"$key failed")
      case ExperimentalSuccess(_, _, costs) =>
        println(s"$key: $costs")
    }
  }

  private def generateCSV(results: Seq[(String, ExperimentalResult)]): String = {
    val baselineDSPComputations = results.find(_._1 == "Only lower").get._2 match {
      case success: ExperimentalSuccess => success.dspComputations
      case ExperimentalFailure => ???
    }

    def getDSPEfficiency(result: ExperimentalSuccess): Double = {
      baselineDSPComputations.toDouble / result.dspComputations.toDouble
    }

    ExperimentalResult.toCSV(results, Map("dspEfficiency" -> getDSPEfficiency))
  }

  @Test
  def fourVGGLayers(): Unit = {
    val store = ExperimentationStore("comparison-to-baselines/four-vgg-layers")
    val network = Networks.VGGUpToLayerN(4)
    val onlyLower = store.retrieveOrCompute("only-lower", Some(baselines.onlyLower(network)))
    printResult("only-lower", onlyLower)

    val alwaysOutline = store.retrieveOrCompute(
      "always-outline",
      baselines.alwaysOutline(network))
    printResult("always-outline", alwaysOutline)

    val transformAndOutlineThenLower = store.retrieveOrCompute(
      "transform-and-outline-then-lower",
      baselines.transformAndOutlineThenLower(network))
    printResult("transform-and-outline-then-lower", transformAndOutlineThenLower)

    val transformAndAlwaysOutlineThenLower = store.retrieveOrCompute(
      "transform-and-always-outline-then-lower",
      baselines.transformAndAlwaysOutlineThenLower(network))
    printResult("transform-and-always-outline-then-lower", transformAndAlwaysOutlineThenLower)

    val stoles = store.retrieveOrCompute(
      "stoles",
      transformAndOutline.tryOutline(
        network,
        tile = true,
        pad = true,
        sequentialize = true,
        debug = false,
        costModel = Helpers.StolesCostModel(maxDotProductVectorization, maxDSPBlocks = maxDSPs, maxRAM = maxRAM)))
    printResult("stoles", stoles)

    println(generateCSV(Seq(
      "Only lower" -> onlyLower,
      "Simple" -> transformAndAlwaysOutlineThenLower,
      "MLT" -> alwaysOutline,
      "VOD" -> transformAndOutlineThenLower,
      "STOLES (MLT+VOD)" -> stoles)))
  }
}
