package eqsat.nn

import com.github.plokhotnyuk.jsoniter_scala.core.{JsonValueCodec, readFromString, writeToString}
import com.github.plokhotnyuk.jsoniter_scala.macros.JsonCodecMaker
import algo.nn.OutliningResult
import eqsat.{ExprToCode, ExprToText}

/**
  * The result of an experiment.
  */
sealed trait ExperimentalResult

object ExperimentalResult {
  /**
    * Converts an outlining result to an experimental result.
    * @param data The outlining result.
    * @return The experimental result.
    */
  implicit def toExperimentalResult(data: Option[OutliningResult]): ExperimentalResult = {
    data match {
      case None => ExperimentalFailure
      case Some(data) => ExperimentalSuccess(
        ExprToCode(data.solution),
        ExprToText(data.solution),
        Map(
          ExperimentalSuccess.estimatedRunTimeKey -> data.costs.head,
          ExperimentalSuccess.dspUseKey -> data.costs(1),
          ExperimentalSuccess.ramUseKey -> data.costs(2),
          ExperimentalSuccess.dspComputationsKey -> data.costs(3),
          ExperimentalSuccess.dspCyclesKey -> data.costs(4),
          ExperimentalSuccess.outlinedClassCountKey -> data.outlinedClassCount,
          ExperimentalSuccess.eNodeCountKey -> data.eNodeCount,
          ExperimentalSuccess.eClassCountKey -> data.eClassCount,
          ExperimentalSuccess.functionCallNodeCountKey -> data.functionCallNodeCount,
          ExperimentalSuccess.functionClassCountKey -> data.functionClassCount))
    }
  }

  private def kebabCaseToCamelCase(kebab: String): String = {
    val parts = kebab.split("-")
    parts.head + parts.tail.map(_.capitalize).mkString
  }

  /**
   * Converts a sequence of experimental results to a CSV string.
   * @param results The experimental results.
   * @param computedMetrics The computed metrics to include in the CSV.
   */
  def toCSV(results: Seq[(String, ExperimentalResult)],
            computedMetrics: Map[String, ExperimentalSuccess => Any] = Map()): String = {
    val metricKeys = results.collect {
      case (_, ExperimentalSuccess(_, _, metrics)) => metrics.keys
    }.flatten.distinct.sorted
    val computedMetricKeys = computedMetrics.keys.toSeq.sorted
    val headerItems = "id" +: (metricKeys.map(kebabCaseToCamelCase) ++ computedMetricKeys)
    val header = headerItems.mkString(",")
    val entries = results.map {
      case (key, ExperimentalFailure) =>
        (key +: Seq.fill(metricKeys.length + computedMetricKeys.length)("N/A")).mkString(",")
      case (key, success: ExperimentalSuccess) =>
        val costs = success.getMany(metricKeys)
        val computedCosts = computedMetricKeys.map(k => computedMetrics(k)(success))
        (key +: (costs.map(_.toString) ++ computedCosts.map(_.toString))).mkString(",")
    }
    (header +: entries).mkString("\n")
  }
}

/**
  * The results associated with a successful experiment.
  * @param scalaIR The IR tree that was generated, expressed as a Scala program that regenerates the tree.
  * @param textIR The IR tree that was generated, expressed as a pseudocode program.
  * @param measurements The data associated with the experiment.
  */
final case class ExperimentalSuccess(scalaIR: String, textIR: String, measurements: Map[String, BigInt]) extends ExperimentalResult {
  /**
   * Retrieves the measurement associated with a key.
   * @param key The key to retrieve the measurement for.
   * @return The measurement.
   */
  def apply(key: String): BigInt = measurements(key)

  /**
   * Retrieves the measurements associated with a set of keys.
   * @param keys The keys to retrieve the measurements for.
   * @return The measurements, in the same order as the keys.
   */
  def getMany(keys: Seq[String]): Seq[BigInt] = keys.map(measurements)

  /**
   * The estimated run time of the generated code.
   * @return The estimated run time.
   */
  def estimatedRunTime: BigInt = this(ExperimentalSuccess.estimatedRunTimeKey)

  /**
   * The DSP use of the generated code.
   * @return The DSP use.
   */
  def dspUse: BigInt = this(ExperimentalSuccess.dspUseKey)

  /**
   * The RAM use of the generated code.
   * @return The RAM use.
   */
  def ramUse: BigInt = this(ExperimentalSuccess.ramUseKey)

  /**
   * The DSP computations of the generated code.
   * @return The DSP computations.
   */
  def dspComputations: BigInt = this(ExperimentalSuccess.dspComputationsKey)

  /**
   * The DSP cycles of the generated code.
   * @return The DSP cycles.
   */
  def dspCycles: BigInt = this(ExperimentalSuccess.dspCyclesKey)

  /**
   * The saturation time of the generated code.
   * @return The saturation time.
   */
  def saturationTime: BigInt = this(ExperimentalSuccess.saturationTimeKey)

  /**
   * The outlining time of the generated code.
   * @return The outlining time.
   */
  def extractionTime: BigInt = this(ExperimentalSuccess.extractionTimeKey)

  /**
   * Converts this experimental result to a JSON string.
   * @return The JSON string.
   */
  def json: String = writeToString(this)
}

/*
 * The JSON codec for an experimental success.
 */
object ExperimentalSuccess {
  /**
   * The JSON codec for an experimental success.
   */
  implicit val codec: JsonValueCodec[ExperimentalSuccess] = JsonCodecMaker.make

  /**
   * Parses an experimental success from a JSON string.
   * @param json The JSON string.
   * @return The experimental success.
   */
  def parse(json: String): ExperimentalSuccess = readFromString(json)

  /**
   * The key for the estimated run time measurement.
   */
  val estimatedRunTimeKey = "estimated-run-time"

  /**
   * The key for the DSP use measurement.
   */
  val dspUseKey = "dsp-use"

  /**
   * The key for the RAM use measurement.
   */
  val ramUseKey = "ram-use"

  /**
   * The key for the DSP computations measurement.
   */
  val dspComputationsKey = "dsp-computations"

  /**
   * The key for the DSP cycles measurement.
   */
  val dspCyclesKey = "dsp-cycles"

  /**
   * The key for the saturation time measurement.
   */
  val saturationTimeKey = "saturation-time"

  /**
   * The key for the outlining time measurement.
   */
  val extractionTimeKey = "extraction-time"

  /**
   * The key for the e-node count measurement.
   */
  val eNodeCountKey = "e-node-count"

  /**
   * The key for the e-class count measurement.
   */
  val eClassCountKey = "e-class-count"

  /**
    * The key for the function call node count measurement.
    */
  val functionCallNodeCountKey = "function-call-node-count"

  /**
    * The key for the function class count measurement.
    */
  val functionClassCountKey = "function-class-count"

  /**
   * The key for the outlined class count measurement.
   */
  val outlinedClassCountKey = "outlined-class-count"
}

/**
  * The results associated with a failed experiment.
  */
object ExperimentalFailure extends ExperimentalResult
