package eqsat.nn

import algo.eqsat.TilingRewriteRules
import algo.{IntType, SignedIntType}
import algo.nn._
import core._
import eqsat._
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class NNIROutliningTest {
  val transformAndOutline = TransformAndOutline(TilingRewriteRules)

  @Test
  def matVecPaddingRuleFires(): Unit = {
    val bitWidth = 8
    val t = SignedIntType(bitWidth)

    val n = 30
    val m = 16

    val input = algo.Input("weights1", t, n)
    val weights1 = algo.Input("weights1", t, n, m)
    val weights2 = algo.Input("weights2", t, m, m)

    val matVec1 = TypeChecker.check(MatVecLayer(weights1, input))
    val resized1 = TypeChecker.check(ResizeIntegerLayer(matVec1, bitWidth))
    val matVec2 = TypeChecker.check(MatVecLayer(weights2, resized1))

    val graph = transformAndOutline.createEGraph(EGraphNullObserver)
    val mv1 = graph.add(matVec1)
    val mv2 = graph.add(matVec2)
    graph.rebuild()

    transformAndOutline.saturate(graph, pad = true, sequentialize = false, costModel = Helpers.LegacyCostModel)

    // Quick sanity check
    assert(graph.contains(matVec1, mv1))
    assert(graph.contains(matVec2, mv2))

    // Check that the padded version of matvec2 is in mv2
    val padded = TypeChecker.check(
      DeBruijnTransform.forward(TypeChecker.check(MatVecLayer(weights2, resized1).pad(n - m))))
    assert(graph.contains(padded, mv2))

    // Another sanity check
    assert(!graph.contains(padded, mv1))

    // Check that the fully lowered version of matvec2 is in mv2
    val originalLowered = TypeChecker.check(DeBruijnTransform.forward(Network.lower(matVec2)))
    assert(graph.contains(originalLowered, mv2))

    // Check that the fully lowered equivalent of the padded expression is in mv2
    val paddedLowered = TypeChecker.check(DeBruijnTransform.forward(Network.lower(padded)))
    assert(graph.contains(paddedLowered, mv2))
  }

  @Test
  def convICHTilingRuleFires(): Unit = {
    val input = InputLayer(64, 32, 32, 8, 0)
    val conv1 = TypeChecker.check(ConvolutionLayer(input, 64, 128, 3, 1, 8, 1))
    val round1 = TypeChecker.check(RoundingLayer(conv1, 8, 8))
    val conv2 = TypeChecker.check(ConvolutionLayer(round1, 128, 128, 3, 1, 8, 2))
    val conv2Tiled = conv2 match {
      case ConvolutionLayer(in2, w2, _) => ConvolutionLayer(in2, w2).tileICH(64)
    }

    val conv1db = TypeChecker.check(DeBruijnTransform.forward(conv1))
    val conv2db = TypeChecker.check(DeBruijnTransform.forward(conv2))

    val graph = transformAndOutline.createEGraph(EGraphNullObserver)
    val c1 = graph.add(conv1db)
    val c2 = graph.add(conv2db)
    graph.rebuild()

    transformAndOutline.saturate(graph, tile = true, sequentialize = true, costModel = Helpers.LegacyCostModel)

    // Quick sanity check
    assert(graph.contains(conv1db, c1))
    assert(graph.contains(conv2db, c2))

    // Check that the tiled version of conv2 is in c2
    val tiled = TypeChecker.check(
      DeBruijnTransform.forward(TypeChecker.check(conv2Tiled)))
    assert(graph.contains(tiled, c2))

    // Another sanity check
    assert(!graph.contains(tiled, c1))

    // Check that the fully lowered version of conv2 is in c2
    val originalLowered = TypeChecker.check(DeBruijnTransform.forward(Network.lower(conv2)))
    assert(graph.contains(originalLowered, c2))

    // Check that the fully lowered equivalent of the padded expression is in mv2
    val tiledLowered = TypeChecker.check(DeBruijnTransform.forward(Network.lower(tiled)))
    assert(graph.contains(tiledLowered, c2))
  }
}
