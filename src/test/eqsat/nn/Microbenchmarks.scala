package eqsat.nn

import algo.Join
import algo.eqsat.TilingRewriteRules
import algo.nn._
import core.{Expr, TypeChecker}
import org.junit.Test

class Microbenchmarks {
  val transformAndOutline = TransformAndOutline(TilingRewriteRules)

  @Test
  def lowerMicrobenchmarks(): Unit = {
    val store = ExperimentationStore("microbenchmarks-lowering")
    val results = for (Microbenchmark(name, expr, costModel) <- benchmarks) yield {
      val baselines = Baselines(costModel.maxDotProductVectorization, costModel.maxDSPBlocks, costModel.maxRAM)
      val result = store.retrieveOrCompute(name, Some(baselines.onlyLower(expr)))

      result match {
        case ExperimentalFailure =>
          println(s"$name failed")
        case ExperimentalSuccess(_, _, costs) =>
          println(s"$name: $costs")
      }

      (name, result)
    }

    println(ExperimentalResult.toCSV(results))
  }

  @Test
  def optimizeMicrobenchmarks(): Unit = {
    val store = ExperimentationStore("microbenchmarks")
    val results = for (Microbenchmark(name, expr, costModel) <- benchmarks) yield {
      val result = store.retrieveOrCompute(
        name, {
          transformAndOutline.tryOutline(
            expr,
            tile = true,
            pad = true,
            sequentialize = true,
            maxSteps = 14,
            costModel = costModel)
        })

      result match {
        case ExperimentalFailure =>
          println(s"$name failed")
        case ExperimentalSuccess(_, _, costs) =>
          println(s"$name: $costs")
      }

      (name, result)
    }

    println(ExperimentalResult.toCSV(results))
  }

  final case class Microbenchmark(name: String, expr: Expr, costModel: Helpers.StolesCostModel)

  private val motivatingExample = Microbenchmark("motivating-example", {
    val bitWidth = 8
    val input = InputLayer(64, 4, 4, bitWidth, 0)
    val weights = algo.Input("weights", 64 * 2 * 2, 64, algo.SignedIntType(bitWidth))
    val conv = ConvolutionLayer(input, 64, 64, 3, 0, bitWidth, 1)
    val resized1 = ResizeIntegerLayer(conv, bitWidth)
    val matVec = MatVecLayer(weights, Join(Join(resized1)))
    val resized2 = ResizeIntegerLayer(matVec, bitWidth)

    TypeChecker.check(resized2)
  }, Helpers.StolesCostModel(maxDotProductVectorization = 128, maxDSPBlocks = 128, maxRAM = 16384 * 16))

  private val benchmarks: Seq[Microbenchmark] = Seq(
    motivatingExample
  )
}
