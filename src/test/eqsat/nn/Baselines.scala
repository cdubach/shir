package eqsat.nn

import algo.eqsat.TilingRewriteRules
import algo.nn.{OutliningResult, TransformAndOutline}
import core.Expr
import eqsat.{NeverOutline, OutlineIfUsedMoreThanOnce}

final case class Baselines(maxDotProductVectorization: Int, maxDSPs: Int, maxRAM: Int) {
  val transformAndOutline = TransformAndOutline(TilingRewriteRules)

  def onlyLower(network: Expr): OutliningResult = {
    val costModel = Helpers.StolesCostModel(
      maxDotProductVectorization,
      maxDSPBlocks = 1000 * 8192,
      maxRAM = 1000 * 16 * 1024 * 1024 * 8,
      outliningStrategy = NeverOutline)
    val saturated = transformAndOutline.padAndTile(network, pad = false, tile = false, sequentialize = false, debug = false, costModel = costModel)
    transformAndOutline.tryExtract(saturated, debug = false, costModel = costModel).get
  }

  private def onlyTransform(network: Expr): OutliningResult = {
    val costModel = Helpers.StolesCostModel(
      maxDotProductVectorization,
      maxDSPBlocks = maxDSPs,
      maxRAM = maxRAM,
      outliningStrategy = NeverOutline)
    val saturated = transformAndOutline.padAndTile(network, pad = true, tile = true, sequentialize = false, lower = false, debug = false, costModel = costModel)
    transformAndOutline.tryExtract(saturated, debug = false, costModel = costModel).get
  }

  private def onlyOutline(network: Expr): OutliningResult = {
    val costModel = Helpers.StolesCostModel(
      maxDotProductVectorization,
      maxDSPBlocks = 1000 * 8192,
      maxRAM = 1000 * 16 * 1024 * 1024 * 8)
    val saturated = transformAndOutline.padAndTile(network, pad = false, tile = false, sequentialize = false, lower = false, debug = false, costModel = costModel)
    transformAndOutline.tryExtract(saturated, debug = false, costModel = costModel).get
  }

  def separatePhases(network: Expr): OutliningResult = {
    onlyOutline(onlyLower(onlyTransform(network).solution).solution)
  }

  def alwaysOutline(network: Expr): Option[OutliningResult] = {
    val costModel = Helpers.StolesCostModel(
      maxDotProductVectorization,
      maxDSPBlocks = maxDSPs,
      maxRAM = maxRAM,
      outliningStrategy = OutlineIfUsedMoreThanOnce)
    val saturated = transformAndOutline.padAndTile(network, pad = true, tile = true, sequentialize = true, lower = true, debug = false, costModel = costModel)
    transformAndOutline.tryExtract(saturated, debug = false, costModel = costModel)
  }

  def transformAndOutlineThenLower(network: Expr): Option[OutliningResult] = {
    val costModel = Helpers.StolesCostModel(
      maxDotProductVectorization,
      maxDSPBlocks = maxDSPs,
      maxRAM = maxRAM,
      extractNetworkLayers = true)
    val saturated = transformAndOutline.padAndTile(network, pad = true, tile = true, sequentialize = true, lower = false, debug = false, costModel = costModel)
    transformAndOutline.tryExtract(saturated, debug = false, costModel = costModel)
  }

  def transformAndAlwaysOutlineThenLower(network: Expr): Option[OutliningResult] = {
    val costModel = Helpers.StolesCostModel(
      maxDotProductVectorization,
      maxDSPBlocks = maxDSPs,
      maxRAM = maxRAM,
      extractNetworkLayers = true,
      outliningStrategy = OutlineIfUsedMoreThanOnce)
    val saturated = transformAndOutline.padAndTile(network, pad = true, tile = true, sequentialize = true, lower = false, debug = false, costModel = costModel)
    transformAndOutline.tryExtract(saturated, debug = false, costModel = costModel)
  }
}
