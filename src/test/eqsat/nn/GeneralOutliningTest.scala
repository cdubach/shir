package eqsat.nn

import algo.eqsat.OutliningRewriteRules
import algo.{Add, AlgoExpr, AlgoLambda, IntType, Mul, Tuple}
import backend.hdl.arch.costModel.CostModel.estimateDSPBlocks
import core.{Expr, LambdaT, ParamDef, ParamUse, TypeChecker}
import eqsat.{AnalysesBuilder, DeBruijnIndexAnalysis, DeBruijnParamUse, DeBruijnShiftExpr, DeBruijnTransform, EGraph, ExprToText, ExtractionAnalysis, HierarchicalStopwatch, PostExtractionOutliner, SolverExtractor, TimingObserver}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class GeneralOutliningTest {
  val solverExtractor: SolverExtractor = {
    SolverExtractor.simple({
      case algo.Map(_, _, _) | algo.Zip(_, _, _) | algo.Fold(_, _, _) => 10
      case _ => 1
    }, !_.expr.isInstanceOf[DeBruijnShiftExpr], 1)
  }

  /**
   * Determines if an expression is a single op that takes only parameters as arguments.
   * @param expr The expression to examine.
   * @return `true` if `expr` is either a parameter or a single op that takes only parameters as arguments.
   */
  def isSingleOp(expr: Expr): Boolean = {
    expr match {
      case _: DeBruijnParamUse => true
      case expr: AlgoExpr =>
        expr.args match {
          case Seq(Tuple(args, _)) => args.forall(_.isInstanceOf[DeBruijnParamUse])
          case Seq() => false
          case args => args.forall(_.isInstanceOf[DeBruijnParamUse])
        }
      case _ => false
    }
  }

  /**
   * Applies the equality saturation-based outliner to an algo expression.
   * @param expr An algo expression to transform.
   * @param debug When set to `true`, this method will print information that might be helpful for debugging purposes;
   *              otherwise, the method does not print any additional information.
   * @param maxSteps The maximum number of saturation steps.
   * @return An algo expression that has been sent through the equality saturation engine and outliner.
   */
  def outline(expr: Expr, debug: Boolean = true, maxSteps: Int = 100): Expr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

    val transformed = TypeChecker.check(DeBruijnTransform.forward(expr))

    if (debug) {
      println(ExprToText(transformed))
      println(s"DSP blocks: ${estimateDSPBlocks(transformed)}")
      println(s"Expr size: ${ExtractionAnalysis.smallestExpressionExtractor.cost(transformed)}")
    }

    val extractorAnalysis = ExtractionAnalysis.smallestExpressionExtractor
    val extractor = extractorAnalysis.extractor
    val observer = new TimingObserver()
    val eGraph = EGraph(
      analyses = new AnalysesBuilder().add(extractorAnalysis).add(DeBruijnIndexAnalysis).toAnalyses,
      observer = observer)
    val root = eGraph.add(transformed)
    eGraph.rebuild()
    eGraph.saturate(OutliningRewriteRules(extractor).all, maxSteps)

    if (debug) {
      println(s"Saturation took ${observer.totalTime.getSeconds}s")
      println(ExprToText(solverExtractor(root).get))
    }

    val outliner = PostExtractionOutliner(solverExtractor, !isSingleOp(_))
    val stopwatch = HierarchicalStopwatch.start("extraction")
    val outlined = outliner(root).get
    val extractionTime = stopwatch.complete()
    if (debug) {
      println(ExprToText(outlined))
      println(s"Extraction took ${extractionTime.duration.getSeconds}s")
      println(s"DSP blocks: ${estimateDSPBlocks(outlined)}")
      println(s"Expr size: ${ExtractionAnalysis.smallestExpressionExtractor.cost(outlined)}")
    }

    val result = TypeChecker.check(DeBruijnTransform.backward(outlined))

    if (debug) {
      println(ExprToText(result))
      println(result)
    }

    result
  }

  @Test
  def willOutlineConstantMul(): Unit = {
    val t = IntType(32)
    val constMul = Mul(Tuple(algo.ConstantInteger(1, Some(t)), algo.ConstantInteger(42, Some(t))))
    val expr = Add(Tuple(constMul, constMul))

    val outlined = outline(expr, debug = false)
    assert(ExprToText(outlined) == "let param 0: i64 = mul (tuple (const 1: i32) (const 42: i32)) in add (tuple (use param 0: i64) (use param 0: i64))")
  }

//  @Test
//  def wontOutlineParamMul(): Unit = {
//    val t = IntType(32)
//    val p1 = ParamDef(t)
//    val p2 = ParamDef(t)
//    val mul = Mul(Tuple(ParamUse(p1), ParamUse(p2)))
//    val expr = AlgoLambda(p1, AlgoLambda(p2, Add(Tuple(mul, mul))))
//
//    val outlined = outline(expr, debug = false)
//    assert(ExprToText(outlined) == "λ0: i32. (λ1: i32. (add (tuple (mul (tuple (use param 0: i32) (use param 1: i32))) (mul (tuple (use param 0: i32) (use param 1: i32))))))")
//  }
}
