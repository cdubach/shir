package eqsat.nn

import algo.eqsat.TilingRewriteRules
import algo.nn.TransformAndOutline
import core.{Expr, FunctionCall}
import eqsat.HierarchicalStopwatch
import org.junit.Test

/**
 * A test that evaluates STOLES' scalability on a fixed hardware budget and increasingly complex networks.
 */
class Scalability {
  val transformAndOutline = TransformAndOutline(TilingRewriteRules)

  /**
   * The budgets for DSP blocks to evaluate.
   */
  private val dspBudget = 1533 * 2

  /**
   * The budgets for RAM to evaluate.
   */
  private val ramBudget = 16 * 1024 * 1024 * 8

  /**
   * The maximum vectorization factor for dot products.
   */
  private val maxDotProductVectorization = 256

  private val combinedScalabilityStore = ExperimentationStore("scalability")
  private val saturationScalabilityStore = ExperimentationStore("saturation-scalability")

  private def saturateNetwork(name: String, root: => Expr, store: ExperimentationStore): ExperimentalResult = {
    store.retrieveOrCompute(
      name,
      {
        val stopwatch = HierarchicalStopwatch.start("saturation")
        val saturated = transformAndOutline.padAndTile(
          root,
          pad = true,
          tile = true,
          sequentialize = true,
          debug = false,
          costModel = Helpers.StolesCostModel(maxDotProductVectorization, maxDSPBlocks = dspBudget, maxRAM = ramBudget))
        val saturationTime = stopwatch.complete()
        val eClasses = saturated.reachable
        val functionCallNodes = eClasses.flatMap(_.nodes).collect { case n if n.expr.isInstanceOf[FunctionCall] => n }
        val functionClasses = functionCallNodes.map(_.args.head).distinct
        ExperimentalSuccess("", "", Map(
          ExperimentalSuccess.saturationTimeKey -> BigInt(saturationTime.duration.toMillis),
          ExperimentalSuccess.eClassCountKey -> BigInt(eClasses.length),
          ExperimentalSuccess.eNodeCountKey -> BigInt(eClasses.map(_.nodes.length).sum),
          ExperimentalSuccess.functionCallNodeCountKey -> BigInt(functionCallNodes.length),
          ExperimentalSuccess.functionClassCountKey -> BigInt(functionClasses.length)))
      })
  }

  private def optimizeNetwork(name: String, root: => Expr, store: ExperimentationStore): ExperimentalResult = {
    store.retrieveOrCompute(
      name,
      {
        // Time saturation and outlining/extraction steps separately, then fold compile times into the
        // ExperimentalResult costs
        val saturationStopwatch = HierarchicalStopwatch.start("saturation")
        val saturated = transformAndOutline.padAndTile(
          root,
          pad = true,
          tile = true,
          sequentialize = true,
          debug = false,
          costModel = Helpers.StolesCostModel(maxDotProductVectorization, maxDSPBlocks = dspBudget, maxRAM = ramBudget))
        val saturationTime = saturationStopwatch.complete()

        val outliningStopwatch = HierarchicalStopwatch.start("outlining")
        val outlined = transformAndOutline.tryOutline(
          saturated,
          pad = true,
          tile = true,
          sequentialize = true,
          debug = false,
          costModel = Helpers.StolesCostModel(maxDotProductVectorization, maxDSPBlocks = dspBudget, maxRAM = ramBudget))
        val outliningTime = outliningStopwatch.complete()

        ExperimentalResult.toExperimentalResult(outlined) match {
          case ExperimentalFailure => ExperimentalFailure
          case ExperimentalSuccess(scalaIR, textIR, metrics) =>
            ExperimentalSuccess(
              scalaIR,
              textIR,
              metrics ++ Map(
                ExperimentalSuccess.saturationTimeKey -> BigInt(saturationTime.duration.toMillis),
                ExperimentalSuccess.extractionTimeKey -> BigInt(outliningTime.duration.toMillis)))
        }
      })
  }

  private def printCSV(results: Seq[ExperimentalResult]): Unit = {
    println(ExperimentalResult.toCSV(results.zipWithIndex.map(t => (t._2.toString, t._1))))
  }

  @Test
  def increasinglyComplexNetworks(): Unit = {
    val networks = for (i <- 1 to 6) yield Networks.VGGUpToLayerN(i)
    val results = networks.zipWithIndex.map { case (network, index) =>
      val result = optimizeNetwork(s"network-$index", network, combinedScalabilityStore)
      result match {
        case ExperimentalSuccess(_, _, costs) =>
          println(s"Layers 1 through ${index + 1}: $costs")
        case ExperimentalFailure =>
          println(s"Layers 1 through ${index + 1}: failed")
      }
      result
    }
    printCSV(results)
  }

  @Test
  def increasinglyComplexNetworksOnlySaturation(): Unit = {
    val networks = for (i <- 1 to Networks.VGGLayerCount) yield Networks.VGGUpToLayerN(i)
    val results = networks.zipWithIndex.map { case (network, index) =>
      val result = saturateNetwork(s"saturate-network-$index", network, saturationScalabilityStore)
      result match {
        case ExperimentalSuccess(_, _, costs) =>
          println(s"Layers 1 through ${index + 1}: $costs")
        case ExperimentalFailure =>
          println(s"Layers 1 through ${index + 1}: failed")
      }
      result
    }
    printCSV(results)
  }
}
