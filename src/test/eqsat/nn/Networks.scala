package eqsat.nn

import algo.nn.{ConvolutionLayer, InputLayer, MatVecLayer, PoolingLayer, ReLULayer, RoundingLayer, SaveLayer}
import core.{Expr, TypeChecker}

/**
 * A collection of neural network definitions. Each network is designed to be used as a test case for STOLES.
 */
object Networks {
  /**
   * A description of the VGG network.
   * @return The VGG network.
   */
  def VGG: Expr = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val conv0 = ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)
    val round0 = RoundingLayer(conv0, 8, 8)
    val relu0 = ReLULayer(round0)

    val conv1 = ConvolutionLayer(relu0, 64, 64, 3, 1, 8, 1)
    val round1 = RoundingLayer(conv1, 8, 8)
    val relu1 = ReLULayer(round1)
    val conv1Pool = PoolingLayer(relu1, 2)

    val conv2 = ConvolutionLayer(conv1Pool, 64, 128, 3, 1, 8, 2)
    val round2 = RoundingLayer(conv2, 8, 8)
    val relu2 = ReLULayer(round2)

    val conv3 = ConvolutionLayer(relu2, 128, 128, 3, 1, 8, 3)
    val round3 = RoundingLayer(conv3, 8, 8)
    val relu3 = ReLULayer(round3)
    val conv3Pool = PoolingLayer(relu3, 2)

    val conv4 = ConvolutionLayer(conv3Pool, 128, 256, 3, 1, 8, 4)
    val round4 = RoundingLayer(conv4, 8, 8)
    val relu4 = ReLULayer(round4)

    val conv5a = ConvolutionLayer(relu4, 256, 256, 3, 1, 8, 5)
    val round5a = RoundingLayer(conv5a, 8, 8)
    val relu5a = ReLULayer(round5a)

    val conv5b = ConvolutionLayer(relu5a, 256, 256, 3, 1, 8, 6)
    val round5b = RoundingLayer(conv5b, 8, 8)
    val relu5b = ReLULayer(round5b)
    val conv5Pool = PoolingLayer(relu5b, 2)

    val conv6 = ConvolutionLayer(conv5Pool, 256, 512, 3, 1, 8, 7)
    val round6 = RoundingLayer(conv6, 8, 8)
    val relu6 = ReLULayer(round6)

    val conv7a = ConvolutionLayer(relu6, 512, 512, 3, 1, 8, 8)
    val round7a = RoundingLayer(conv7a, 8, 8)
    val relu7a = ReLULayer(round7a)

    val conv7b = ConvolutionLayer(relu7a, 512, 512, 3, 1, 8, 9)
    val round7b = RoundingLayer(conv7b, 8, 8)
    val relu7b = ReLULayer(round7b)
    val conv7Pool = PoolingLayer(relu7b, 2)

    val conv8a = ConvolutionLayer(conv7Pool, 512, 512, 3, 1, 8, 10)
    val round8a = RoundingLayer(conv8a, 8, 8)
    val relu8a = ReLULayer(round8a)

    val conv8b = ConvolutionLayer(relu8a, 512, 512, 3, 1, 8, 11)
    val round8b = RoundingLayer(conv8b, 8, 8)
    val relu8b = ReLULayer(round8b)

    val conv8c = ConvolutionLayer(relu8b, 512, 512, 3, 1, 8, 12)
    val round8c = RoundingLayer(conv8c, 8, 8)
    val relu8c = ReLULayer(round8c)
    val conv8Pool = PoolingLayer(relu8c, 2)

    val flatten = algo.Join(algo.Join(conv8Pool))
    val matVecWgt = algo.Input("mat13", 512, 512, algo.SignedIntType(8))
    val matVec1 = MatVecLayer(matVecWgt, flatten)
    val roundMv1 = RoundingLayer(matVec1, 8, 8)
    val deFlatten = algo.Split(algo.Split(roundMv1, 512), 1)

    TypeChecker.check(SaveLayer(deFlatten))
  }

  /**
   * A description of the first four layers of the VGG network.
   * @return The first four layers of the VGG network.
   */
  def fourVGGLayers: Expr = {
    val input = InputLayer(3, 32, 32, 8, 0)
    val conv0 = ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)
    val round0 = RoundingLayer(conv0, 8, 8)
    val relu0 = ReLULayer(round0)

    val conv1 = ConvolutionLayer(relu0, 64, 64, 3, 1, 8, 1)
    val round1 = RoundingLayer(conv1, 8, 8)
    val relu1 = ReLULayer(round1)
    val conv1Pool = PoolingLayer(relu1, 2)

    val conv2 = ConvolutionLayer(conv1Pool, 64, 128, 3, 1, 8, 2)
    val round2 = RoundingLayer(conv2, 8, 8)
    val relu2 = ReLULayer(round2)

    val conv3 = ConvolutionLayer(relu2, 128, 128, 3, 1, 8, 3)
    val round3 = RoundingLayer(conv3, 8, 8)
    val relu3 = ReLULayer(round3)
    val conv3Pool = PoolingLayer(relu3, 2)

    val conv4 = ConvolutionLayer(conv3Pool, 128, 256, 3, 1, 8, 4)
    val round4 = RoundingLayer(conv4, 8, 8)
    val relu4 = ReLULayer(round4)

    TypeChecker.check(relu4)
  }

  /**
   * A description of the first N layers of the VGG network.
   * @param n The number of layers to include.
   * @return The first N layers of the VGG network.
   */
  def VGGUpToLayerN(n: Int): Expr = {
    val layers = VGGLayers.take(n)
    val network = layers.foldLeft(InputLayer(3, 32, 32, 8, 0): Expr)((input, layer) => layer(input))
    TypeChecker.check(network)
  }

  /**
   * The number of layers in the VGG network.
   * @return The number of layers in the VGG network.
   */
  def VGGLayerCount: Int = VGGLayers.length

  private val VGGLayers: Seq[Expr => Expr] = {
    Seq(
      input => {
        val conv0 = ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)
        val round0 = RoundingLayer(conv0, 8, 8)
        val relu0 = ReLULayer(round0)
        relu0
      },
      input => {
        val conv1 = ConvolutionLayer(input, 64, 64, 3, 1, 8, 1)
        val round1 = RoundingLayer(conv1, 8, 8)
        val relu1 = ReLULayer(round1)
        relu1
      },
      input => {
        val conv1Pool = PoolingLayer(input, 2)
        conv1Pool
      },
      input => {
        val conv2 = ConvolutionLayer(input, 64, 128, 3, 1, 8, 2)
        val round2 = RoundingLayer(conv2, 8, 8)
        val relu2 = ReLULayer(round2)
        relu2
      },
      input => {
        val conv3 = ConvolutionLayer(input, 128, 128, 3, 1, 8, 3)
        val round3 = RoundingLayer(conv3, 8, 8)
        val relu3 = ReLULayer(round3)
        relu3
      },
      input => {
        val conv3Pool = PoolingLayer(input, 2)
        conv3Pool
      },
      input => {
        val conv4 = ConvolutionLayer(input, 128, 256, 3, 1, 8, 4)
        val round4 = RoundingLayer(conv4, 8, 8)
        val relu4 = ReLULayer(round4)
        relu4
      },
      input => {
        val conv5a = ConvolutionLayer(input, 256, 256, 3, 1, 8, 5)
        val round5a = RoundingLayer(conv5a, 8, 8)
        val relu5a = ReLULayer(round5a)
        relu5a
      },
      input => {
        val conv5b = ConvolutionLayer(input, 256, 256, 3, 1, 8, 6)
        val round5b = RoundingLayer(conv5b, 8, 8)
        val relu5b = ReLULayer(round5b)
        relu5b
      },
      input => {
        val conv5Pool = PoolingLayer(input, 2)
        conv5Pool
      },
      input => {
        val conv6 = ConvolutionLayer(input, 256, 512, 3, 1, 8, 7)
        val round6 = RoundingLayer(conv6, 8, 8)
        val relu6 = ReLULayer(round6)
        relu6
      },
      input => {
        val conv7a = ConvolutionLayer(input, 512, 512, 3, 1, 8, 8)
        val round7a = RoundingLayer(conv7a, 8, 8)
        val relu7a = ReLULayer(round7a)
        relu7a
      },
      input => {
        val conv7b = ConvolutionLayer(input, 512, 512, 3, 1, 8, 9)
        val round7b = RoundingLayer(conv7b, 8, 8)
        val relu7b = ReLULayer(round7b)
        relu7b
      },
      input => {
        val conv7Pool = PoolingLayer(input, 2)
        conv7Pool
      },
      input => {
        val conv8a = ConvolutionLayer(input, 512, 512, 3, 1, 8, 10)
        val round8a = RoundingLayer(conv8a, 8, 8)
        val relu8a = ReLULayer(round8a)
        relu8a
      },
      input => {
        val conv8b = ConvolutionLayer(input, 512, 512, 3, 1, 8, 11)
        val round8b = RoundingLayer(conv8b, 8, 8)
        val relu8b = ReLULayer(round8b)
        relu8b
      },
      input => {
        val conv8c = ConvolutionLayer(input, 512, 512, 3, 1, 8, 12)
        val round8c = RoundingLayer(conv8c, 8, 8)
        val relu8c = ReLULayer(round8c)
        relu8c
      },
      input => {
        val conv8Pool = PoolingLayer(input, 2)
        conv8Pool
      },
      input => {
        val flatten = algo.Join(algo.Join(input))
        val matVecWgt = algo.Input("mat13", 512, 512, algo.SignedIntType(8))
        val matVec1 = MatVecLayer(matVecWgt, flatten)
        val roundMv1 = RoundingLayer(matVec1, 8, 8)
        val deFlatten = algo.Split(algo.Split(roundMv1, 512), 1)
        deFlatten
      }
    )
  }
}
