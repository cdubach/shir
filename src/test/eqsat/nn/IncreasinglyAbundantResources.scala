package eqsat.nn

import algo.{IntType, SeqType, SignedIntType}
import algo.eqsat.HierarchicalTilingRewriteRules
import algo.nn.{ConvolutionLayer, DotProductLayer, DotProductMatVecLayer, HierarchicalLayer, InputLayer, MatVecLayer, ParallelDotProductsLayer, TransformAndOutline}
import core.{ArithType, Expr, ParamDef, ParamUse}
import eqsat.EClassT
import org.junit.Test

/**
 * A test that evaluates STOLES on a fixed set of networks and increasingly abundant hardware resources.
 */
class IncreasinglyAbundantResources {
  val transformAndOutline = TransformAndOutline(HierarchicalTilingRewriteRules)

  /**
   * The budgets for DSP blocks to evaluate.
   */
  private val dspBudgets = Seq(64, 128, 256, 512, 1024, 2048, 4096).reverse

  /**
   * The budgets for RAM to evaluate.
   */
  private val ramBudgets = Seq(
    4 * 1024 * 8,
    8 * 1024 * 8,
    16 * 1024 * 8,
    32 * 1024 * 8,
    64 * 1024 * 8,
    128 * 1024 * 8,
    256 * 1024 * 8
  )

  /**
   * The Cartesian product of DSP and RAM budgets to evaluate.
   */
  private val resourceMatrix = {
    dspBudgets.flatMap(dsp => ramBudgets.map(ram => (dsp, ram)))
  }

  /**
   * The maximum vectorization factor for dot products.
   */
  private val maxDotProductVectorization = 64

  private def optimizeForResourceBudget(root: => EClassT, dspBudget: Int, ramBudget: Int, store: ExperimentationStore): ExperimentalResult = {
    val key = s"budgets-$dspBudget-$ramBudget"
    store.retrieveOrCompute(
      key,
      transformAndOutline.tryExtract(
        root,
        debug = false,
        costModel = Helpers.SkeletonStolesCostModel(maxDotProductVectorization, maxDSPBlocks = dspBudget, maxRAM = ramBudget)))
  }

  private def optimizeForAllResourceBudgets(network: Expr, store: ExperimentationStore): Seq[(Int, Int, ExperimentalResult)] = {
    // Saturate with a dummy cost model (used only to determine if an expression is lowerable, which is independent
    // of the resource budget)
    lazy val saturated = transformAndOutline.padAndTile(
      HierarchicalLayer.fromRec(network),
      pad = true, tile = true, sequentialize = true, lower = false, maxSteps = 14,
      costModel = Helpers.SkeletonStolesCostModel())
    resourceMatrix.map { case (dspBudget, ramBudget) =>
      // Extract and outline with the actual cost model
      val result = optimizeForResourceBudget(saturated, dspBudget, ramBudget, store)
      result match {
        case ExperimentalSuccess(_, _, costs) => println(s"Optimized for DSP budget $dspBudget and RAM budget $ramBudget -> costs: $costs")
        case ExperimentalFailure => println(s"Failed for DSP budget $dspBudget and RAM budget $ramBudget")
      }
      (dspBudget, ramBudget, result)
    }
  }

  private def printCSV(results: Seq[(Int, Int, ExperimentalResult)]): Unit = {
    val csv = new StringBuilder()
    csv.append("dsp-budget,ram-budget,estimated-run-time,dsp-use,ram-use\n")
    results.foreach {
      case (dspBudget, ramBudget, success: ExperimentalSuccess) =>
        csv.append(s"$dspBudget,$ramBudget,${success.estimatedRunTime},${success.dspUse},${success.ramUse}\n")
      case (_, _, ExperimentalFailure) =>
    }
    println(csv.toString())
  }

  @Test
  def parallelDotProducts(): Unit = {
    val store: ExperimentationStore = ExperimentationStore("increasingly-abundant-resources-dot")
    val matrixType = SeqType(Seq(ArithType(1024), ArithType(1024)), SignedIntType(8))
    val leftMatrix = ParamDef(matrixType)
    val rightMatrix = ParamDef(matrixType)
    val network = ParallelDotProductsLayer(ParamUse(leftMatrix), ParamUse(rightMatrix), 1024)
    val baselines = Baselines(maxDotProductVectorization, 2048, 256 * 1024 * 8)
    store.retrieveOrCompute("only-lower", Some(baselines.onlyLower(network))) match {
      case ExperimentalSuccess(_, _, costs) => println(s"Baseline costs: $costs")
    }
    val results = optimizeForAllResourceBudgets(network, store)
    printCSV(results)
  }

  @Test
  def matVecProduct(): Unit = {
    val store: ExperimentationStore = ExperimentationStore("increasingly-abundant-resources-matvec")
    val matrixType = SeqType(Seq(ArithType(1024), ArithType(1024)), SignedIntType(8))
    val leftMatrix = ParamDef(matrixType)
    val rightMatrix = ParamDef(SeqType(SignedIntType(8), 1024))
    val network = DotProductMatVecLayer.from(MatVecLayer(ParamUse(leftMatrix), ParamUse(rightMatrix)))
    val baselines = Baselines(maxDotProductVectorization, 2048, 256 * 1024 * 8)
    store.retrieveOrCompute("only-lower", Some(baselines.onlyLower(network))) match {
      case ExperimentalSuccess(_, _, costs) => println(s"Baseline costs: $costs")
    }
    val results = optimizeForAllResourceBudgets(network, store)
    printCSV(results)
  }

  @Test
  def convolution(): Unit = {
    val store: ExperimentationStore = ExperimentationStore("increasingly-abundant-resources-conv")
    val network = HierarchicalLayer.fromRec({
      val input = InputLayer(3, 32, 32, 8, 0)
      ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)
    })
    val baselines = Baselines(maxDotProductVectorization, 2048, 256 * 1024 * 8)
    store.retrieveOrCompute("only-lower", Some(baselines.onlyLower(network))) match {
      case ExperimentalSuccess(_, _, costs) => println(s"Baseline costs: $costs")
    }
    val results = optimizeForAllResourceBudgets(network, store)
    printCSV(results)
  }

  @Test
  def oneVGGLayer(): Unit = {
    val store: ExperimentationStore = ExperimentationStore("increasingly-abundant-resources-vgg1")
    val network = Networks.VGGUpToLayerN(1)
    val baselines = Baselines(maxDotProductVectorization, 2048, 256 * 1024 * 8)
    store.retrieveOrCompute("only-lower", Some(baselines.onlyLower(network))) match {
      case ExperimentalSuccess(_, _, costs) => println(s"Baseline costs: $costs")
    }
    val results = optimizeForAllResourceBudgets(network, store)
    printCSV(results)
  }

  @Test
  def twoVGGLayers(): Unit = {
    val store: ExperimentationStore = ExperimentationStore("increasingly-abundant-resources-vgg2")
    val network = Networks.VGGUpToLayerN(2)
    val baselines = Baselines(maxDotProductVectorization, 2048, 256 * 1024 * 8)
    store.retrieveOrCompute("only-lower", Some(baselines.onlyLower(network))) match {
      case ExperimentalSuccess(_, _, costs) => println(s"Baseline costs: $costs")
    }
    val results = optimizeForAllResourceBudgets(network, store)
    printCSV(results)
  }

  @Test
  def sevenVGGLayers(): Unit = {
    val store: ExperimentationStore = ExperimentationStore("increasingly-abundant-resources-vgg7")
    val network = Networks.VGGUpToLayerN(7)
    val baselines = Baselines(maxDotProductVectorization, 2048, 256 * 1024 * 8)
    store.retrieveOrCompute("only-lower", Some(baselines.onlyLower(network))) match {
      case ExperimentalSuccess(_, _, costs) => println(s"Baseline costs: $costs")
    }
    val results = optimizeForAllResourceBudgets(network, store)
    printCSV(results)
  }
}
