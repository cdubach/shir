package eqsat.nn

import algo._
import algo.eqsat.{HierarchicalTilingRewriteRules, TilingRewriteRules}
import algo.nn._
import backend.hdl.HDLProject
import core._
import core.util.IRDotGraph
import org.junit.Test

class Experiments {
  val transformAndOutline = TransformAndOutline(TilingRewriteRules)

  @Test
  def matVecTilingRuleWorks(): Unit = {
    val bitWidth = 8
    val t = SignedIntType(bitWidth)

    val network = {
      val input = ParamDef(SeqType(t, 32))
      val weights1 = ParamDef(SeqType(SeqType(t, 32), 16))
      val weights2 = ParamDef(SeqType(SeqType(t, 16), 16))

      val matVec1 = MatVecLayer(ParamUse(weights1), ParamUse(input))
      val resized1 = ResizeIntegerLayer(matVec1, bitWidth)
      val matVec2 = MatVecLayer(ParamUse(weights2), resized1)
      val resized2 = ResizeIntegerLayer(matVec2, bitWidth)

      TypeChecker.check(AlgoLambda(Seq(input, weights1, weights2), resized2))
    }

    val result = transformAndOutline.outline(network, tile = true, maxSteps = 14, costModel = Helpers.LegacyCostModel)
    HDLProject("tile-two-matvecs", result).writeHDLFiles()
  }

  @Test
  def matVecPaddingWorks(): Unit = {
    val bitWidth = 8
    val t = SignedIntType(bitWidth)

    val network = {
      val input = ParamDef(SeqType(t, 30))
      val weights1 = ParamDef(SeqType(SeqType(t, 30), 16))
      val weights2 = ParamDef(SeqType(SeqType(t, 16), 16))

      val matVec1 = MatVecLayer(ParamUse(weights1), ParamUse(input))
      val resized1 = ResizeIntegerLayer(matVec1, bitWidth)
      val matVec2 = MatVecLayer(ParamUse(weights2), resized1)
      val resized2 = ResizeIntegerLayer(matVec2, bitWidth)

      TypeChecker.check(AlgoLambda(Seq(input, weights1, weights2), resized2))
    }

    val result = transformAndOutline.outline(network, pad = true, sequentialize = false, maxSteps = 14, costModel = Helpers.LegacyCostModel)
    IRDotGraph(result).show()
    HDLProject("pad-two-matvecs", result).writeHDLFiles()
  }

  @Test
  def outlineVgg(): Unit = {
    Counter.resetAll()

    val network = {
      val input = InputLayer(3, 32, 32, 8, 0)
      val conv0 = ConvolutionLayer(input, 3, 64, 3, 1, 8, 0)
      val round0 = RoundingLayer(conv0, 8, 8)
      val conv1 = ConvolutionLayer(round0, 64, 64, 3, 1, 8, 1)
      val round1 = RoundingLayer(conv1, 8, 8)
      val conv1Pool = PoolingLayer(round1, 2)
      val conv2 = ConvolutionLayer(conv1Pool, 64, 128, 3, 1, 8, 2)
      val round2 = RoundingLayer(conv2, 8, 8)
      val res = SaveLayer(round2)

      TypeChecker.check(res)
    }

    //    println(network)
    val result = transformAndOutline.outline(network, Helpers.LegacyCostModel, maxSteps = 14)
    HDLProject("vgg-network-desc", result).writeHDLFiles()
  }

  @Test
  def motivatingExample(): Unit = {
    val bitWidth = 8
    //    val t = IntType(bitWidth)

    val network = {
      val input = InputLayer(64, 4, 4, bitWidth, 0)
      val weights = algo.Input("weights", 64 * 2 * 2, 64, algo.SignedIntType(bitWidth))
      val conv = ConvolutionLayer(input, 64, 64, 3, 0, bitWidth, 1)
      val resized1 = ResizeIntegerLayer(conv, bitWidth)
      val matVec = MatVecLayer(weights, Join(Join(resized1)))
      val resized2 = ResizeIntegerLayer(matVec, bitWidth)

      TypeChecker.check(resized2)
    }

    val transformAndOutline = TransformAndOutline(HierarchicalTilingRewriteRules)
    val result = transformAndOutline.outline(
      HierarchicalLayer.fromRec(network),
      pad = true, sequentialize = false, lower = false, maxSteps = 14,
      costModel = Helpers.LegacySkeletonCostModel)
    IRDotGraph(result).show()
    HDLProject("motivating-example", result).writeHDLFiles()
  }

  @Test
  def outlineVggPadHW(): Unit = {
    Counter.resetAll()

    val network = {
      val input = InputLayer(64, 32, 32, 8, 0)
      val conv1 = ConvolutionLayer(input, 64, 64, 3, 1, 8, 1)
      val round1 = RoundingLayer(conv1, 8, 8)
      val conv1Pool = PoolingLayer(round1, 2)
      val conv2 = ConvolutionLayer(conv1Pool, 64, 64, 3, 1, 8, 2)
      val round2 = RoundingLayer(conv2, 8, 8)
      val res = SaveLayer(round2)

      TypeChecker.check(res)
    }

    val result = transformAndOutline.outline(network, pad = true, maxSteps = 14, costModel = Helpers.StolesCostModel(maxDSPBlocks = 128, functionCallCost = 50))
    IRDotGraph(result).show()
    HDLProject("vgg-padhw", result).writeHDLFiles()
  }

  @Test
  def outlineVggTilePadHWLarge(): Unit = {
    val network = {
      val input = InputLayer(64, 32, 32, 8, 0)
      val conv1 = ConvolutionLayer(input, 64, 64, 3, 1, 8, 1)
      val round1 = RoundingLayer(conv1, 8, 8)
      val conv1Pool = PoolingLayer(round1, 2)
      val conv2 = ConvolutionLayer(conv1Pool, 64, 64, 3, 1, 8, 2)
      val round2 = RoundingLayer(conv2, 8, 8)
      val res = SaveLayer(round2)

      TypeChecker.check(res)
    }

    val result = transformAndOutline.outline(
      network,
      tile = true,
      pad = true,
      maxSteps = 14,
      costModel = Helpers.StolesCostModel(maxDSPBlocks = 128, maxRAM = 32768 * 8))
    IRDotGraph(result).show()
    HDLProject("vgg-tile-padhw-large", result).writeHDLFiles()
  }

  @Test
  def outlineVggTilePadHWSmall(): Unit = {
    Counter.resetAll()

    val network = {
      val input = InputLayer(64, 4, 4, 8, 0)
      val conv1 = ConvolutionLayer(input, 64, 64, 3, 1, 8, 1)
      val round1 = RoundingLayer(conv1, 8, 8)
      val conv1Pool = PoolingLayer(round1, 2)
      val conv2 = ConvolutionLayer(conv1Pool, 64, 64, 3, 1, 8, 2)
      val round2 = RoundingLayer(conv2, 8, 8)
      val res = SaveLayer(round2)

      TypeChecker.check(res)
    }

    val result = transformAndOutline.outline(network, tile = true, pad = true, maxSteps = 14, costModel = Helpers.StolesCostModel(maxDSPBlocks = 128, maxRAM = 2 * 3072 * 8))
    IRDotGraph(result).show()
    HDLProject("vgg-tile-padhw-small", result).writeHDLFiles()
  }

  @Test
  def outlineVggTileHW(): Unit = {
    Counter.resetAll()

    val network = {
      val input = InputLayer(64, 32, 32, 8, 0)
      val conv1 = ConvolutionLayer(input, 64, 64, 3, 1, 8, 1)
      val round1 = RoundingLayer(conv1, 8, 8)
      val conv1Pool = PoolingLayer(round1, 2)
      val conv2 = ConvolutionLayer(conv1Pool, 64, 64, 3, 1, 8, 2)
      val round2 = RoundingLayer(conv2, 8, 8)
      val res = SaveLayer(round2)

      TypeChecker.check(res)
    }

    val result = transformAndOutline.outline(network, tile = true, maxSteps = 14, costModel = Helpers.StolesCostModel(maxDSPBlocks = 128, functionCallCost = 50))
    IRDotGraph(result).show()
    HDLProject("vgg-tilehw", result).writeHDLFiles()
  }

  @Test
  def outlineVggTileOCH(): Unit = {
    Counter.resetAll()

    val network = {
      val input = InputLayer(64, 32, 32, 8, 0)
      val conv1 = ConvolutionLayer(input, 64, 64, 3, 1, 8, 1)
      val round1 = RoundingLayer(conv1, 8, 8)
      val conv2 = ConvolutionLayer(round1, 64, 128, 3, 1, 8, 2)
      val round2 = RoundingLayer(conv2, 8, 8)
      val res = SaveLayer(round2)

      TypeChecker.check(res)
    }

    val result = transformAndOutline.outline(network, tile = true, maxSteps = 14, costModel = Helpers.StolesCostModel(maxDSPBlocks = 128, functionCallCost = 50))
    IRDotGraph(result).show()
    HDLProject("vgg-tileOch", result).writeHDLFiles()
  }

  @Test
  def outlineVggTileICH(): Unit = {
    Counter.resetAll()

    val network = {
      val input = InputLayer(64, 32, 32, 8, 0)
      val conv1 = ConvolutionLayer(input, 64, 128, 3, 1, 8, 1)
      val round1 = RoundingLayer(conv1, 8, 8)
      val conv2 = ConvolutionLayer(round1, 128, 128, 3, 1, 8, 2)
      val round2 = RoundingLayer(conv2, 8, 8)
      val res = SaveLayer(round2)

      TypeChecker.check(res)
    }

    val result = transformAndOutline.outline(network, tile = true, maxSteps = 14, costModel = Helpers.StolesCostModel(maxDSPBlocks = 128, functionCallCost = 50))
    IRDotGraph(result).show()
    HDLProject("vgg-tileIch", result).writeHDLFiles()
  }
}
