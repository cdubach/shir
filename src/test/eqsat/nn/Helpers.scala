package eqsat.nn

import algo.SeqTypeT
import algo.nn._
import eqsat._
import eqsat.solver.{Optimizer, Z3Py}

object Helpers {
  object DotProductCostModel extends CostModel {
    override val extractor: SolverExtractor = {
      SolverExtractor.simple({
        case DotProductLayer(_, _, _) => 100
        case _ => 1
      }, _.expr match {
        case _: DeBruijnShiftExpr => false
        case _: DotProductLayerExpr => true
        case _: NetworkLayerExpr => false
        case _ => true
      })
    }

    override def isLowerable(layer: NetworkLayerExpr): Boolean = !layer.isInstanceOf[DotProductLayerExpr]
  }

  object LegacyCostModel extends CostModel {
    override val extractor: SolverExtractor = {
      SolverExtractor.simple({
        case algo.Map(_, _, _) | algo.Zip(_, _, _) | algo.Fold(_, _, _) => 10
        case _ => 1
      }, _.expr match {
        case _: DeBruijnShiftExpr => false
        case _: NetworkLayerExpr => false
        case _ => true
      })
    }

    override def isLowerable(layer: NetworkLayerExpr): Boolean = true
  }

  object LegacySkeletonCostModel extends CostModel {
    override val extractor: SolverExtractor = {
      SolverExtractor.simple({
        case algo.Map(_, _, _) | algo.Zip(_, _, _) | algo.Fold(_, _, _) => 10
        case ParallelDotProductsLayer(left, _, parallelism, _) =>
          val vectorCount = left.t.asInstanceOf[SeqTypeT].len
          val vectorSize = left.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
          (vectorCount.ae * vectorSize.ae / parallelism.ae).evalInt
        case _ => 1
      }, _.expr match {
        case _: DeBruijnShiftExpr => false
        case _: NetworkLayerExpr => true
        case _ => true
      })
    }

    override def isLowerable(layer: NetworkLayerExpr): Boolean = false
  }

  trait StolesCostModelBase extends CostModel {
    def maxDotProductVectorization: Int

    def maxDSPBlocks: Int

    def maxRAM: Int

    def functionCallCost: Int

    def hasNetworkLayerLowering: Boolean = true

    def costModel: OutliningExtractorCostModel = ConstrainedRunTimeCostModel(
      maxDotProductVectorization,
      maxDSPBlocks = maxDSPBlocks,
      maxRAM = maxRAM,
      functionCallCost = functionCallCost)

    override def isLowerable(layer: NetworkLayerExpr): Boolean = layer match {
      case _: DotProductLayerExpr => false
      case _: ParallelDotProductsLayerExpr => false
      case _: DataTransformationLayerExpr => false
      case _ => hasNetworkLayerLowering
    }
  }

  case class StolesCostModel(maxDotProductVectorization: Int = 64,
                             maxDSPBlocks: Int = 1533 * 2,
                             maxRAM: Int = 16 * 1024 * 1024 * 8,
                             functionCallCost: Int = 5,
                             extractorOptimizations: SolverExtractorOptimizations = SolverExtractorOptimizations(),
                             solverInterface: Optimizer = Z3Py(),
                             outliningStrategy: OutliningStrategy = VariableOutlining,
                             extractNetworkLayers: Boolean = false) extends StolesCostModelBase {

    override val extractor: SolverExtractor = {
      SolverExtractor(
        costModel,
        _.expr match {
          case _: DeBruijnShiftExpr => false
          case layer: NetworkLayerExpr if !isLowerable(layer) => true
          case _: NetworkLayerExpr if !extractNetworkLayers => false
          case _ => true
        },
        optimizations = extractorOptimizations,
        solverInterface = solverInterface,
        outliningStrategy = outliningStrategy)
    }
  }

  case class SkeletonStolesCostModel(maxDotProductVectorization: Int = 64,
                                     maxDSPBlocks: Int = 1533 * 2,
                                     maxRAM: Int = 16 * 1024 * 1024 * 8,
                                     functionCallCost: Int = 5,
                                     extractorOptimizations: SolverExtractorOptimizations = SolverExtractorOptimizations(),
                                     solverInterface: Optimizer = Z3Py(),
                                     outliningStrategy: OutliningStrategy = VariableOutlining) extends StolesCostModelBase {

    override def hasNetworkLayerLowering: Boolean = false

    override val extractor: SolverExtractor = {
      SolverExtractor(
        costModel,
        _.expr match {
          case _: DeBruijnShiftExpr => false
          case _ => true
        },
        optimizations = extractorOptimizations,
        solverInterface = solverInterface,
        outliningStrategy = outliningStrategy)
    }
  }

  case class DPStolesCostModel(maxDotProductVectorization: Int = 64,
                               maxDSPBlocks: Int = 1533 * 2,
                               maxRAM: Int = 16 * 1024 * 1024 * 8,
                               functionCallCost: Int = 5,
                               extractNetworkLayers: Boolean = false) extends StolesCostModelBase {

    override val extractor: DPOutliningExtractor = {
      DPOutliningExtractor(
        costModel,
        _.expr match {
          case _: DeBruijnShiftExpr => false
          case layer: NetworkLayerExpr if !isLowerable(layer) => true
          case _: NetworkLayerExpr if !extractNetworkLayers => false
          case _ => true
        })
    }
  }

  /**
   * A cost model that applies no lowering, resulting in high-level, domain-specific output expressions.
   * @param maxDotProductVectorization The maximum vectorization factor for dot products.
   * @param maxDSPBlocks The maximum number of DSP blocks.
   * @param maxRAM The maximum amount of RAM.
   * @param functionCallCost The cost of a function call.
   * @param extractorOptimizations The optimizations to apply to the extractor.
   * @param solverInterface The solver interface to use.
   * @param outliningStrategy The outlining strategy to use.
   */
  final case class HighLevelCostModel(maxDotProductVectorization: Int = 64,
                                      maxDSPBlocks: Int = 1533 * 2,
                                      maxRAM: Int = 16 * 1024 * 1024 * 8,
                                      functionCallCost: Int = 5,
                                      extractorOptimizations: SolverExtractorOptimizations = SolverExtractorOptimizations(),
                                      solverInterface: Optimizer = Z3Py(),
                                      outliningStrategy: OutliningStrategy = VariableOutlining) extends StolesCostModelBase {

    /**
     * The cost model used to guide the outlining process.
     */
    override val extractor: OutliningExtractor = {
      SolverExtractor(
        costModel,
        _.expr match {
          case _: DeBruijnShiftExpr => false
          case _ => true
        },
        optimizations = extractorOptimizations,
        solverInterface = solverInterface,
        outliningStrategy = outliningStrategy)
    }

    /**
     * Determines if a layer will be lowered within the equality saturation process.
     *
     * @param layer The layer to check.
     * @return `true` if the layer should be lowered; otherwise, `false`.
     */
    override def isLowerable(layer: NetworkLayerExpr): Boolean = false
  }
}