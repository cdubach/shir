package eqsat.nn

import algo.{IntType, SignedIntType}
import algo.eqsat.OutliningRewriteRules
import algo.nn.{MatVecLayer, Network}
import core.TypeChecker
import eqsat.{DeBruijnTransform, EGraph, Extractor, NoReason}
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class LoweringRulesTest {
  @Test
  def twoLayersInSameClass(): Unit = {
    val bitWidth = 8
    val t = SignedIntType(bitWidth)

    val n = 30
    val m = 16

    val input = algo.Input("input", t, m)
    val weights = algo.Input("weights", t, m, m)

    // Build two equivalent expressions. One is a regular matrix-vector product, the other is a padded matrix-vector
    // product. Crucially, the padded product is wrapped in a ResizeIntegerLayer, meaning we'll have two types of layers
    // in the same e-class. This challenges the lowering rule to lower both layers
    val matVec = TypeChecker.check(MatVecLayer(weights, input))
    val padded = TypeChecker.check(DeBruijnTransform.forward(MatVecLayer(weights, input).pad(n - m)))

    // Build an e-graph in which matVec and padded are in the same e-class
    val graph = EGraph()
    val mv = graph.add(matVec)
    graph.addAndMerge(graph.add(padded), mv, NoReason)
    graph.rebuild()

    // Saturate the graph with the lowering rule
    graph.saturate(Seq(OutliningRewriteRules(Extractor.DefaultSmallest).lowerNetworkLayer))

    // Sanity checks
    assert(graph.contains(matVec, mv))
    assert(graph.contains(padded, mv))

    // Check that the fully lowered version of matVec is in mv
    val originalLowered = TypeChecker.check(DeBruijnTransform.forward(Network.lower(matVec)))
    assert(graph.contains(originalLowered, mv))

    // Check that the fully lowered equivalent of the padded expression is in mv
    val paddedLowered = TypeChecker.check(DeBruijnTransform.forward(Network.lower(padded)))
    assert(graph.contains(paddedLowered, mv))
  }

  @Test
  def twoLayersInSameClassStaggered(): Unit = {
    val bitWidth = 8
    val t = SignedIntType(bitWidth)

    val n = 30
    val m = 16

    val input = algo.Input("input", t, m)
    val weights = algo.Input("weights", t, m, m)

    // Build two equivalent expressions. One is a regular matrix-vector product, the other is a padded matrix-vector
    // product. Crucially, the padded product is wrapped in a ResizeIntegerLayer, meaning we'll have two types of layers
    // in the same e-class. This challenges the lowering rule to lower both layers
    val matVec = TypeChecker.check(MatVecLayer(weights, input))
    val padded = TypeChecker.check(DeBruijnTransform.forward(MatVecLayer(weights, input).pad(n - m)))

    // Build an e-graph containing matVec
    val graph = EGraph()
    val mv = graph.add(matVec)
    graph.rebuild()

    // Saturate the graph with the lowering rule
    graph.saturate(Seq(OutliningRewriteRules(Extractor.DefaultSmallest).lowerNetworkLayer))

    // Add padded to the graph and make it equivalent to matVec
    graph.addAndMerge(graph.add(padded), mv, NoReason)
    graph.rebuild()

    // Once more saturate the graph with the lowering rule
    graph.saturate(Seq(OutliningRewriteRules(Extractor.DefaultSmallest).lowerNetworkLayer))

    // Sanity checks
    assert(graph.contains(matVec, mv))
    assert(graph.contains(padded, mv))

    // Check that the fully lowered version of matVec is in mv
    val originalLowered = TypeChecker.check(DeBruijnTransform.forward(Network.lower(matVec)))
    assert(graph.contains(originalLowered, mv))

    // Check that the fully lowered equivalent of the padded expression is in mv
    val paddedLowered = TypeChecker.check(DeBruijnTransform.forward(Network.lower(padded)))
    assert(graph.contains(paddedLowered, mv))
  }
}
