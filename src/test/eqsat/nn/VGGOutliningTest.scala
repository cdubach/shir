package eqsat.nn

import algo.eqsat.TilingRewriteRules
import algo.nn.TransformAndOutline
import eqsat.nn.Helpers.StolesCostModel
import eqsat.solver.{IterativeOptimizer, Z3Py}
import org.junit.Test

import scala.concurrent.duration.DurationInt

class VGGOutliningTest {
  val transformAndOutline = TransformAndOutline(TilingRewriteRules)

  private val vggStore = ExperimentationStore("vgg")

  @Test
  def testVGG(): Unit = {
    val res = Networks.VGG
    val result = vggStore.retrieveOrCompute(
      "vgg",
      transformAndOutline.tryOutline(res, pad = true, tile = true, sequentialize = true, maxSteps = 14, costModel = StolesCostModel(solverInterface = IterativeOptimizer(Z3Py(), 1.hour))))

    result match {
      case ExperimentalSuccess(_, solution, costs) =>
        println(costs)
        println(solution)

      case ExperimentalFailure =>
        println("Failed")
    }
  }
}
