package eqsat.nn

/**
  * A collection of helper functions that store and retrieve experimental data. Data is stored on disk.
  */
final case class ExperimentationStore(experiment: String) {
  /**
    * A path to the directory where the data for this experiment is stored.
    */
  private val storeDirectory = s"experiments/$experiment"

  private val failedToOptimizeString = "Failed to optimize"

  private def ensureStoreDirectoryExists(): Unit = {
    val storeDirectoryFile = new java.io.File(storeDirectory)
    if (!storeDirectoryFile.exists()) {
      storeDirectoryFile.mkdirs()
    }
  }

  /**
    * Stores the experimental data for a given key.
    * @param key The key to store the data under.
    * @param data The experimental data to store.
    */
  def store(key: String, data: ExperimentalResult): Unit = {
    ensureStoreDirectoryExists()

    val jsonPath = s"$storeDirectory/$key.json"
    data match {
      case ExperimentalFailure =>
        val codeFile = new java.io.PrintWriter(jsonPath)
        codeFile.write(failedToOptimizeString)
        codeFile.close()
      case success: ExperimentalSuccess =>
        val codeFile = new java.io.PrintWriter(jsonPath)
        codeFile.write(success.json)
        codeFile.close()
    }
  }

  /**
    * Retrieves the experimental data for a given key.
    * @param key The key to retrieve the data for.
    * @return The experimental data, if it exists.
    */
  def retrieve(key: String): Option[ExperimentalResult] = {
    ensureStoreDirectoryExists()

    val jsonPath = s"$storeDirectory/$key.json"

    if (!new java.io.File(jsonPath).exists()) {
      None
    } else {
      val jsonSource = scala.io.Source.fromFile(jsonPath)
      val json = jsonSource.getLines().mkString("\n")
      jsonSource.close()

      if (json == failedToOptimizeString) {
        return Some(ExperimentalFailure)
      }

      Some(ExperimentalSuccess.parse(json))
    }
  }

  /**
    * Retrieves the experimental data for a given key, computing it if it does not exist.
    * @param key The key to retrieve the data for.
    * @param compute The function to compute the data if it does not exist.
    * @return The experimental data.
    */
  def retrieveOrCompute(key: String, compute: => ExperimentalResult): ExperimentalResult = {
    retrieve(key) match {
      case Some(result) => result
      case None =>
        val result = compute
        store(key, result)
        result
    }
  }
}
