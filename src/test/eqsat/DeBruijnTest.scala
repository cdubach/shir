package eqsat

import core.{FunctionCall, ParamDef, ParamUse}
import cGen._
import org.junit.Test
import org.junit.experimental.categories.Category
import tags.EssentialTests

@Category(Array(classOf[EssentialTests]))
class DeBruijnTest {
  @Test
  def paramUseNodeKindsAreSensible(): Unit = {
    val expr1 = DeBruijnParamUse(0, IntType(32))
    val expr2 = DeBruijnParamUse(0, DoubleType())
    val expr3 = DeBruijnParamUse(1, IntType(32))

    assert(expr1.nodeKind == expr1.nodeKind)
    assert(expr2.nodeKind == expr2.nodeKind)
    assert(expr3.nodeKind == expr3.nodeKind)

    assert(expr1.nodeKind == expr2.nodeKind)
    assert(expr1.nodeKind != expr3.nodeKind)
  }

  @Test
  def equivalentLambdasAreEqual(): Unit = {
    val p1 = ParamDef(IntType(32))
    val p2 = ParamDef(IntType(32))
    assert(
      DeBruijnLambda.from(CLambda(p1, ParamUse(p1))) == DeBruijnLambda.from(CLambda(p2, ParamUse(p2))))
  }

  @Test
  def nonEquivalentLambdasAreNotEqual(): Unit = {
    val p1 = ParamDef(IntType(32))
    val p2 = ParamDef(IntType(32))
    assert(
      DeBruijnLambda.from(CLambda(p1, ParamUse(p1)))
        != DeBruijnLambda.from(CLambda(p2, BinaryOp(ParamUse(p2), ParamUse(p2), AlgoOp.Add))))
  }

  @Test
  def lambdaEquivalenceIsTypeSensitive(): Unit = {
    val p1 = ParamDef(IntType(32))
    val p2 = ParamDef(IntType(64))
    assert(
      DeBruijnLambda.from(CLambda(p1, ParamUse(p1))) != DeBruijnLambda.from(CLambda(p2, ParamUse(p2))))
  }

  @Test
  def lambdaCanRoundTrip(): Unit = {
    val p = ParamDef(IntType(32))
    val lambda = CLambda(p, BinaryOp(ParamUse(p), ParamUse(p), AlgoOp.Add))
    assert(DeBruijnLambda.from(DeBruijnLambda.from(lambda).toNamed) == DeBruijnLambda.from(lambda))
  }

  @Test
  def fromRecRetainsForall(): Unit = {
    val t = IntType(32)
    val expr = DeBruijnForall(t, DeBruijnParamUse(0, t))
    assert(DeBruijnLambda.fromRec(expr) == expr)
  }

  @Test
  def betaReduceWorks(): Unit = {
    val t = IntType(32)
    val lambda = DeBruijnLambda(t, DeBruijnParamUse(0, t))
    val arg = Constant(0, t)
    assert(lambda.betaReduce(arg) == arg)
  }

  @Test
  def twoLevelBetaReduceWorks(): Unit = {
    val t1 = IntType(32)
    val t2 = DoubleType()
    val lambda = DeBruijnLambda(t1, DeBruijnLambda(t2, DeBruijnParamUse(1, t1)))
    val arg1 = Constant(0, t1)
    val arg2 = Constant(100, t2)
    assert(lambda.betaReduce(arg1).asInstanceOf[DeBruijnLambda].betaReduce(arg2) == arg1)
  }

  @Test
  def twoLevelBetaReduceWorks2(): Unit = {
    val t1 = IntType(32)
    val t2 = DoubleType()
    val lambda = DeBruijnLambda(t1, DeBruijnLambda(t2, DeBruijnParamUse(0, t2)))
    val arg1 = Constant(0, t1)
    val arg2 = Constant(100, t2)
    assert(lambda.betaReduce(arg1).asInstanceOf[DeBruijnLambda].betaReduce(arg2) == arg2)
  }

  @Test
  def betaReduceWithParamWorks(): Unit = {
    // \x. \y. x
    val t = DoubleType()
    val lambda = DeBruijnLambda(t, DeBruijnLambda(t, DeBruijnParamUse(1, t)))

    // Now beta-reduce this lambda with an externally-defined parameter.
    assert(lambda.betaReduce(DeBruijnParamUse(0, t)) == DeBruijnLambda(t, DeBruijnParamUse(1, t)))
    assert(lambda.betaReduce(DeBruijnParamUse(1, t)) == DeBruijnLambda(t, DeBruijnParamUse(2, t)))
  }

  @Test
  def incrementUnboundWorks(): Unit = {
    val t = IntType(32)
    assert(
      DeBruijnParamUse.incrementUnbound(DeBruijnLambda(t, DeBruijnParamUse(1, t)))
        == DeBruijnLambda(t, DeBruijnParamUse(2, t)))

    assert(
      DeBruijnParamUse.incrementUnbound(DeBruijnLambda(t, DeBruijnParamUse(4, t)))
        == DeBruijnLambda(t, DeBruijnParamUse(5, t)))

    assert(
      DeBruijnParamUse.incrementUnbound(DeBruijnLambda(t, DeBruijnParamUse(3, t)), 3)
        == DeBruijnLambda(t, DeBruijnParamUse(6, t)))

    assert(
      DeBruijnParamUse.incrementUnbound(DeBruijnLambda(t, DeBruijnParamUse(0, t)))
        == DeBruijnLambda(t, DeBruijnParamUse(0, t)))

    assert(
      DeBruijnParamUse.incrementUnbound(DeBruijnLambda(t, DeBruijnLambda(t, DeBruijnParamUse(1, t))))
        == DeBruijnLambda(t, DeBruijnLambda(t, DeBruijnParamUse(1, t))))

    assert(
      DeBruijnParamUse.incrementUnbound(DeBruijnLambda(t, DeBruijnLambda(t, DeBruijnParamUse(2, t))))
        == DeBruijnLambda(t, DeBruijnLambda(t, DeBruijnParamUse(3, t))))
  }

  @Test
  def incrementUnboundWorks2(): Unit = {
    val t = IntType(32)
    val expr = DeBruijnLambda(t,
      FunctionCall(
        DeBruijnLambda(t, Tuple(DeBruijnParamUse(1, t), DeBruijnParamUse(0, t))),
        DeBruijnParamUse(0, t)))

    val incremented = DeBruijnParamUse.incrementUnbound(expr)
    assert(incremented == expr)
  }

  @Test
  def decrementUnboundWorks(): Unit = {
    // The De Bruijn indices in the expression below cannot be decremented. Doing so would change the meaning of the
    // expression.
    //
    //     \i32. p_0 + p_1
    //
    val t = IntType(32)
    val expr = DeBruijnLambda(t, BinaryOp(DeBruijnParamUse(0, t), DeBruijnParamUse(1, t), AlgoOp.Add))

    assert(DeBruijnParamUse.tryDecrementUnbound(expr).isEmpty)
  }

  @Test
  def canElideShift(): Unit = {
    val t = DoubleType()
    assert(DeBruijnShift(DeBruijnParamUse(0, t)).elided == DeBruijnParamUse(1, t))
    assert(
      DeBruijnShift.elideRec(DeBruijnLambda(t, DeBruijnLambda(t, DeBruijnShift(DeBruijnParamUse(0, t)))))
        == DeBruijnLambda(t, DeBruijnLambda(t, DeBruijnParamUse(1, t))))
  }

  @Test
  def canTestShiftForNop(): Unit = {
    val t = DoubleType()
    assert(!DeBruijnShift(DeBruijnParamUse(0, t)).isNop)
    assert(DeBruijnShift(Constant(0.0, t)).isNop)
    assert(DeBruijnShift(DeBruijnLambda(t, DeBruijnParamUse(0, t))).isNop)
    assert(!DeBruijnShift(DeBruijnLambda(t, DeBruijnParamUse(1, t))).isNop)
  }

  @Test
  def betaReduceWorksWithShift(): Unit = {
    // \x. \y. x
    // (but in a convoluted way)
    val t = DoubleType()
    val lambda = DeBruijnLambda(t, DeBruijnLambda(t, DeBruijnShift(DeBruijnParamUse(0, t))))

    // Now beta-reduce this lambda with a constant.
    assert(lambda.betaReduce(Constant(0.0, t)) == DeBruijnLambda(t, DeBruijnShift(Constant(0.0, t))))

    // What about an unbound variable?
    assert(lambda.betaReduce(DeBruijnParamUse(0, t)) == DeBruijnLambda(t, DeBruijnShift(DeBruijnParamUse(0, t))))

    // Let's try this lazily.
    assert(
      lambda.betaReduce(DeBruijnParamUse(0, t), shiftEagerly = false)
        == DeBruijnLambda(t, DeBruijnShift(DeBruijnParamUse(0, t))))

    // Same tests, but this time with \x. \y. \z. y
    val lambda2 = DeBruijnLambda(t, DeBruijnLambda(t, DeBruijnLambda(t, DeBruijnShift(DeBruijnParamUse(0, t)))))

    // Now beta-reduce this lambda with a constant.
    assert(lambda2.betaReduce(Constant(0.0, t)) == lambda2.body)

    // What about an unbound variable?
    assert(lambda2.betaReduce(DeBruijnParamUse(0, t)) == lambda2.body)

    // Let's try this lazily.
    assert(lambda2.betaReduce(DeBruijnParamUse(0, t), shiftEagerly = false) == lambda2.body)

    // Another lambda.
    val lambda3 = DeBruijnLambda(t, DeBruijnShift(DeBruijnLambda(t, DeBruijnParamUse(0, t))))
    assert(lambda3.betaReduce(Constant(0, t)) == lambda3.body)
  }

  @Test
  def checkParamTypes(): Unit = {
    val t = IntType(32)
    val expr = DeBruijnLambda(t,
      FunctionCall(
        DeBruijnLambda(t, Tuple(DeBruijnParamUse(1, t), DeBruijnParamUse(0, t))),
        DeBruijnParamUse(0, t)))

    assert(DeBruijnLambda.tryCheckParamTypes(expr))

    val t2 = DoubleType()
    val expr2 = DeBruijnLambda(t,
      FunctionCall(
        DeBruijnLambda(t, Tuple(DeBruijnParamUse(1, t2), DeBruijnParamUse(0, t))),
        DeBruijnParamUse(0, t)))

    assert(!DeBruijnLambda.tryCheckParamTypes(expr2))

    val expr3 = DeBruijnLambda(t, DeBruijnParamUse(1, t))
    assert(DeBruijnLambda.tryCheckParamTypes(expr3))
    assert(DeBruijnLambda.tryCheckParamTypes(DeBruijnLambda(t, expr3)))
    assert(!DeBruijnLambda.tryCheckParamTypes(DeBruijnLambda(t2, expr3)))
  }
}
