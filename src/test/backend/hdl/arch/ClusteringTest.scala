package backend.hdl.arch

import algo.util.Matrix
import backend.hdl.{HDLProject, HWDataTypeVar, IntType}
import core._
import core.util.IRDotGraph
import org.junit.Test

class ClusteringTest {

  @Test
  def scalaTest(): Unit = {
    val k = 2

    val data = Matrix.fromRandom(10, 2, cacheId = Some("data"))
    var centroids = Matrix(data.data.slice(0, k), "centroids")

    for (i <- 0 until 5) {
      println("centroids after " + i + "th iteration: " + centroids.data.map(_.mkString("(", ", ", ")")).mkString("; "))
      centroids = data.kMeansClustering(centroids.data, cache = false)
    }
  }

  @Test
  def test1(): Unit = {
    Counter.resetAll()

    val nValues = 10
    val k = 2
    val dims = 3

    val points = CounterInteger(0, 1, Seq(dims, nValues))
    val initialCentroids = CounterInteger(0, 1, Seq(dims, k))

    val clusteredPoints =
      MapOrderedStream({
        val pPoint = ParamDef()
        ArchLambda(pPoint,
          OrderedStreamToVector(MapOrderedStream(
            {
              val pCentroid = ParamDef()
              ArchLambda(pCentroid,
                FoldOrderedStream(
                  AddInt2.asFunction(),
                  MapOrderedStream(
                    {
                      val pCoordPair = ParamDef()
                      ArchLambda(pCoordPair,
                        // TODO difference, then square
                        SquareInt(ParamUse(pCoordPair))
                      )
                    },
                    Zip2OrderedStream(Tuple2(ParamUse(pPoint), ParamUse(pCentroid)))
                  )
                )
              )
            }, initialCentroids
          ))
        )
      }, points)

    println(TypeChecker.checkAssert(clusteredPoints).t)

    val clusteredPointsVec = MapOrderedStream(
      {
        val p = ParamDef()
        ArchLambda(p,
          UpdateVector(
            Tuple2(
              VectorGenerator(Tuple2(VectorGenerator(ConstantValue(0, Some(IntType(5))), 3), ConstantValue(0, Some(IntType(8)))), k),
              Tuple2(
                Select2(ParamUse(p), 1),
                Tuple2(Select2(ParamUse(p), 0), ConstantValue(1, Some(IntType(8))))
              )
            )
          )
        )
      },
      clusteredPoints
    )

    println(TypeChecker.checkAssert(clusteredPointsVec).t)

    val summedClusteredPoints = FoldOrderedStream(
      {
        val p1 =  ParamDef(HWDataTypeVar())
        val p2 =  ParamDef(HWDataTypeVar())
        ArchLambda(p1, ArchLambda(p2,
          MapVector({
            val p3 = ParamDef(HWDataTypeVar())
            ArchLambda(p3,
              Tuple2(
                MapVector(
                  {
                    val p4 = ParamDef(HWDataTypeVar())
                    ArchLambda(p4, TruncInteger(AddInt(ParamUse(p4)), 1, reduceBitWidth = true))
                  },
                  ZipVector(Tuple2(Select2(Select2(ParamUse(p3), 0), 0), Select2(Select2(ParamUse(p3), 1), 0)))
                ),
                TruncInteger(AddInt(Tuple2(Select2(Select2(ParamUse(p3), 0), 1), Select2(Select2(ParamUse(p3), 1), 1))), 1, reduceBitWidth = true)
              )
            )
          }, ZipVector(Tuple2(ParamUse(p1), ParamUse(p2))))
        ))
      },
      clusteredPointsVec
    )

    val finalCentroids = MapOrderedStream(
      {
        val p1 = ParamDef()
        ArchLambda(p1,
          MapVector(
            {
              val p2 = ParamDef()
              // TODO replace by division
              ArchLambda(p2, MulInt(Tuple2(ParamUse(p2), Select2(ParamUse(p1), 1))))
            },
            Select2(ParamUse(p1), 0)
          )
        )
      },
      summedClusteredPoints
    )

    val tree = TypeChecker.checkAssert(summedClusteredPoints)
    println(tree.t)

    HDLProject("kMeansClustering", tree).simulateStandalone()
  }

}
