package backend.hdl.arch.sharedMM

import backend.hdl.arch.mem.Input
import backend.hdl.arch.{AddInt2, ArchLambda, FoldOrderedStream, FoldVector, MapOrderedStream, MapVector, MulInt, OrderedStreamToVector, Registered, Repeat, Select2, SplitOrderedStream, TruncInteger, Tuple2, VectorToOrderedStream, Zip2OrderedStream, ZipVector}
import backend.hdl.{HostRamType, IntType, LogicType, OrderedStreamType, OrderedStreamTypeT, RamArrayType, SignedIntType, VectorType}
import core.{Expr, FunctionCall, Let, ParamDef, ParamUse, TextType, TypeChecker}

object MMMulTemplate {
  def dotProd(input1: Expr, input2: Expr): Expr = {
    FoldOrderedStream(
      AddInt2.asFunction(),
      MapOrderedStream(
        MulInt.asFunction(),
        Zip2OrderedStream(Tuple2(input1, input2))
      )
    )
  }

  def createMMMulBody(bitWidth: Int, multiplicand: Expr, multiplier: Expr): Expr = {
    val input1 = MapOrderedStream(2,
      {
        val param1 = ParamDef()
        ArchLambda(
          param1,
          MapVector(Registered.asFunction(), ParamUse(param1))
        )
      }, multiplicand
    )

    val input2 = MapOrderedStream(2,
      {
        val param1 = ParamDef()
        ArchLambda(
          param1,
          MapVector(Registered.asFunction(), ParamUse(param1))
        )
      }, multiplier
    )

    val zipIn = TypeChecker.check(MapOrderedStream(Zip2OrderedStream.asFunction(), Zip2OrderedStream(Tuple2(input1, input2))))

    val mmmul = TypeChecker.check(MapOrderedStream(
      2,
      {
        val param = ParamDef(zipIn.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(
          param,
          dotProd(VectorToOrderedStream(Select2(ParamUse(param), 0)), VectorToOrderedStream(Select2(ParamUse(param), 1)))
        )
      }, zipIn
    ))

    val res = MapOrderedStream(
      {
        val param3 = ParamDef(mmmul.t.asInstanceOf[OrderedStreamType].et)
        ArchLambda(
          param3,
          MapOrderedStream(
            {
              val param4 = ParamDef(mmmul.t.asInstanceOf[OrderedStreamType].et.asInstanceOf[OrderedStreamType].et)
              ArchLambda(
                param4,
                TruncInteger(ParamUse(param4), bitWidth)
              )
            }, ParamUse(param3)
          )
        )
      }, mmmul
    )

    res
  }

  def createMMMulFun(size: Int, bitWidth: Int): Expr = {
    val param1 = ParamDef(OrderedStreamType(OrderedStreamType(VectorType(IntType(bitWidth), size), size), size))
    val param2 = ParamDef(OrderedStreamType(OrderedStreamType(VectorType(IntType(bitWidth), size), size), size))

    val res = createMMMulBody(bitWidth, ParamUse(param1), ParamUse(param2))

    val fun = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, res)))
    fun
  }

  def createMMMulInput(matSize: Int, bitWidth: Int, name: String, isMultiplicand: Boolean): Expr = {
    isMultiplicand match {
      case true => MapOrderedStream(Repeat.asFunction(Seq(None), Seq(matSize)), MapOrderedStream(OrderedStreamToVector.asFunction(), Input(name, matSize, matSize, IntType(bitWidth))))
      case false => Repeat(
        MapOrderedStream(
          {
            val param = ParamDef()
            ArchLambda(
              param,
              MapVector(Registered.asFunction(), ParamUse(param))
            )
          }, MapOrderedStream(OrderedStreamToVector.asFunction(), Input(name, matSize, matSize, IntType(bitWidth)))
        ),
        matSize
      )
    }
  }
}
