package backend.hdl.arch

import backend.hdl.arch.rewrite.RemoveSelectRules
import backend.hdl.{IntType, OrderedStreamTypeT}
import core._
import core.rewrite.{RewriteAll, RewriteStep}
import core.util.IRDotGraph
import org.junit.Test

class TupleRewriteTest {

  @Test
  def testRemoveSelectRules1(): Unit = {
    val input1 = CounterInteger(0, 1, Seq(8, 8), Some(IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(8, 8), Some(IntType(8)))
    val input3 = CounterInteger(0, 1, Seq(8), Some(IntType(8)))
    val tupInput = Zip2OrderedStream(Tuple2(Zip2OrderedStream(Tuple2(input1, input2)), input3))
    val tupInputtc = TypeChecker.checkIfUnknown(tupInput, tupInput.t)
    val tree = TypeChecker.check(MapOrderedStream(
      {
        val param = ParamDef(tupInputtc.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(
          param,
          Tuple2(Select2(Select2(ParamUse(param), 0), 0), Repeat(Select2(ParamUse(param), 1), 8))
        )
      }, tupInputtc
    ))

    val treetc = RewriteStep(RewriteAll(), RemoveSelectRules.get()).apply(tree)
    assert(!treetc.hasUnknownType)
    assert(tree.t == treetc.t)
  }

  @Test
  def testRemoveSelectRules2(): Unit = {
    val tree = TypeChecker.check(MapOrderedStream(
      {
        val param = ParamDef()
        ArchLambda(
          param,
          ConcatOrderedStream(Select2(ParamUse(param), 0), Select2(ParamUse(param), 1))
        )
      }, Zip2OrderedStream(Tuple2(CounterInteger(0, 1, Seq(8, 8), Some(IntType(8))), CounterInteger(0, 1, Seq(8, 8), Some(IntType(8)))))
    ))

    val treetc = RewriteStep(RewriteAll(), RemoveSelectRules.get()).apply(tree)
    IRDotGraph(tree).show()
    IRDotGraph(treetc).show()
    println(tree.t)
    println(treetc.t)
    assert(!treetc.hasUnknownType)
    assert(tree.t == treetc.t)
  }

}
