package backend.hdl.arch

import algo.util.Matrix
import algo.{Add, Add2, AlgoLambda, DotProduct, Input, MMMul, Zip2}
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.{AsyncCommunication, AsyncRamInputType, HDLProject, HWFunType, IntType, LogicType, OnBoardRamType, OrderedStreamType, RequestType, VectorType}
import backend.hdl.arch.mem.{BufferStreamInBlockRam, BufferStreamInOnBoardRam, HostRam, MemFunctionsCompiler, MemoryImage, OnBoardRam, OnBoardRamBuffer, Read, ReadAsync, ReadAsyncExpr, ReadHostMemoryController, ReadLocalMemoryController, Write, WriteAsync}
import backend.hdl.arch.rewrite.{CleanupRules, FixTimingRules, InputBufferingRules, MoveDownSplitStreamRules, MoveUpJoinStreamRules, MoveUpVectorToStreamRules, ParallelizeDotProductRules, RemoveStreamVectorConversionRules}
import core.{Conversion, Counter, Expr, Let, ParamDef, ParamUse, TypeChecker, Value}
import lift.arithmetic.ArithExpr
import org.junit.Test
import backend.hdl.specs.{Arria10, FPGA, FPGASpecs}
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted}
import core.util.IRDotGraph

class OnBoardRamTest {

  @Test
  def testTypeCheckingReadLocalMemoryController(): Unit = {
    val input = Value(
      HWFunType(
        AsyncRamInputType(VectorType(LogicType(), 512), 128, 16),
        RequestType(VectorType(LogicType(), 512), VectorType(LogicType(), 16)),
        AsyncCommunication
      )
    )

    val inputParam = ParamDef()
    val expr = ReadLocalMemoryController(input, ParamUse(inputParam))

    val rebuiltExpr = TypeChecker.check(expr)
    IRDotGraph(rebuiltExpr).show()
    assert(!rebuiltExpr.hasUnknownType)
  }

  @Test
  def testTypeCheckingUnusedLet(): Unit = {
    val onBoardRamBankFunc = TypeChecker.check(OnBoardRamBuffer.asFunction(FPGA.current))
    val onBoardRamBankParam = ParamDef(onBoardRamBankFunc.t)

    val onBoardRamBankReadCtrlFunc: Expr = TypeChecker.check({
      val inputParam = ParamDef()
      ArchLambda(inputParam, ReadLocalMemoryController(ParamUse(onBoardRamBankParam), ParamUse(inputParam)), AsyncCommunication)
    })

    val onBoardRamBankReadCtrlParam = ParamDef(onBoardRamBankReadCtrlFunc.t)

    val expr = Let(
      Seq(onBoardRamBankParam),
      Let(
        Seq(onBoardRamBankReadCtrlParam),
        Value(IntType(32)),
        Seq(onBoardRamBankReadCtrlFunc)
      ),
      Seq(onBoardRamBankFunc)
    )

    val rebuiltExpr = TypeChecker.check(expr)
    IRDotGraph(rebuiltExpr).show()
    assert(!rebuiltExpr.hasUnknownType)
  }

  @Test
  def testTupleFunctions(): Unit = {
    val tree = algo.Map(
      {
        val p = ParamDef()
        AlgoLambda(p, algo.Tuple2(
          algo.Mul(algo.Tuple2(algo.Mul(algo.Tuple2(algo.Select2(ParamUse(p), 0), algo.ConstantInteger(2))), algo.ConstantInteger(2))),
          algo.Mul(algo.Tuple2(algo.Select2(ParamUse(p), 1), algo.ConstantInteger(3)))
        ))
      },
      algo.Zip2(algo.Tuple2(algo.CounterInteger(0, 1, 10), algo.CounterInteger(0, 1, 10)))
    )
    HDLProject("tuple_functions", tree).simulateStandalone()
  }

  @Test
  def testTupleFunctionsMem(): Unit = {
    val tree = algo.Map(
      {
        val p = ParamDef()
        AlgoLambda(p, algo.Add(algo.Tuple2(
          algo.Mul(algo.Tuple2(algo.Select2(ParamUse(p), 0), algo.ConstantInteger(2))),
          algo.Add(algo.Tuple2(algo.Select2(ParamUse(p), 1), algo.ConstantInteger(1)))
        )))
      },
      algo.Zip2(algo.Tuple2(algo.Input("a", algo.IntType(8), 256), algo.Input("b", algo.IntType(8), 256)))
    )

    val dataA = Matrix.fromCounter(1, 256, 2).data.head
    val dataB = Matrix.fromCounter(1, 256, 2).data.head
    val dataRes = dataA.zip(dataB).map(t => {(t._1 * 2) + (t._2 + 1)})

    HDLProject("tuple_functions", tree).simulateWithHostRam(
      Predef.Map(
        "a" -> dataA,
        "b" -> dataB,
        MemoryImage.RESULT_VAR_NAME -> dataRes)
      ).assertCorrect.print()
  }

//  /*
//  * Note that the buffering test is slow.
//  * If the matrix being buffered is nxn, then it sends out "n" reads/writes requests at a time
//  * */
//

  @Test
  def testOnBoardRamWrite(): Unit = {
    Counter.resetAll()

    for (idx <- 0 to 25) {

      val numElems = scala.math.pow(2, idx).intValue
      val dataSize = 512

      // Write values 0 to (numElems - 1) in address locations 0 to (numElems - 1)

      val tree = Write(OnBoardRam( // On Board RAM Type of numElems elements of size dataSize bit wide
        origElementType = IntType(dataSize),
        dimensions = Seq(numElems)
      ),
        IndexStream(
          Conversion(
            CounterInteger(0, 1, Seq(numElems), Some(IntType(dataSize))), // Counter from 0 to numElems of size dataSize bit wide
            OrderedStreamType(VectorType(LogicType(), dataSize), numElems) // Convert Counter Output into Ordered Stream of vectors, each vector = request for 1 element
          )
        )
      )

      HDLProject("OBR_Write_" + numElems + "_Elements_" + dataSize + "_bits_Profiling", tree).writeHDLFiles()
    }
  }

  @Test
  def testOnBoardRamRead(): Unit = {

//    for (idx <- 0 to 14) {
      Counter.resetAll()
      val numElems = 256
      val dataSize = 32

      val maxReqs = scala.math.pow(2, 6).intValue

      // Read Elements in address locations 0 to (numElems - 1)
      val tree = Read(OnBoardRam( // On Board RAM of numElems elements of size dataSize bit wide
                                origElementType = IntType(dataSize),
                                dimensions = Seq(numElems)))

      HDLProject( "OBR_Read_" + numElems + "_numElems_" + maxReqs + "_bufferSize_" + dataSize + "_bits_Profiling",
                  tree,
                  rewrites = Seq( (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(maxReqs)))),
                                  (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.doubleBufferRead)))
                  )).writeHDLFiles()

//    }
  }

  @Test
  def testHostRamToOnBoardRam(): Unit = {

    for (idx <- 0 to 14) {
      Counter.resetAll()

      val numElems = scala.math.pow(2, idx).intValue
      val dataSize = 512
      val maxReqs = 64

      val tree = Write(OnBoardRam(origElementType = IntType(dataSize), dimensions = Seq(numElems)), IndexStream(Read(HostRam(origElementType = IntType(dataSize), dimensions = Seq(numElems)))))

      HDLProject( "HR_to_OBR_" + numElems + "_Elements_" + dataSize + "_bits_Profiling",
                  tree,
                  rewrites = Seq( (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(maxReqs)))),
                                  (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.doubleBufferRead))))).writeHDLFiles()
    }
  }

  @Test
  def testOnBoardRamToHostRam(): Unit = {

    for (idx <- 0 to 14) {
      Counter.resetAll()

      val numElems = scala.math.pow(2, idx).intValue
      val dataSize = 512
      val maxReqs = 64

      val tree = Write(HostRam(origElementType = IntType(dataSize), dimensions = Seq(numElems)), IndexStream(Read(OnBoardRam(origElementType = IntType(dataSize), dimensions = Seq(numElems)))))

      HDLProject("OBR_to_HR_" + numElems + "_Elements_" + dataSize + "_bits_Profiling",
        tree,
        rewrites = Seq((MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(maxReqs)))),
          (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.doubleBufferRead))))).writeHDLFiles()

    }
  }

  @Test
  def testOnBoardRamBuffering(): Unit = {

    Counter.resetAll()
    val idx = 10
    val numRows = 1
    val numCols = scala.math.pow(2, idx).intValue
    val dataSize = 512
    val useOBR = true
    val maxParReadReqs = 64
    val numBanks = 1
    val mat = Matrix.fromRandom(rows = numRows, cols = numCols)

    val projName = "OBR_MemCpy_" + numRows +"_Rows_" + numCols + "_Cols_" + dataSize + "_Bits_Profiling"
    val tree = algo.Id(Input("matrix1", numCols, numRows, algo.IntType(dataSize)))

    HDLProject( projName,
                tree,
                CompilerPhase.first(),
                Seq((ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix(inputLabel = "matrix1", scale = numBanks, useOnBoardRam = useOBR)))),
                    (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(maxParReq = maxParReadReqs)))),
                    (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), RemoveStreamVectorConversionRules.get() ++ MoveUpVectorToStreamRules.get() ++ MoveUpJoinStreamRules.get() ++ MoveDownSplitStreamRules.get())),
                    (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.doubleBufferRead))),
                    (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
                    (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())))
    ).simulateWithHostRam(Predef.Map(
      "matrix1" -> mat.data,
      MemoryImage.RESULT_VAR_NAME -> mat.data),
      useOnBoardRAM = useOBR
    ).assertCorrect.print()

  }

  @Test
  def testMMMemInput(): Unit = { // 20 sec (38924 cycles) (9229 cycles with 16x parallel dot product)

    Counter.resetAll()
    val idx = 4
    val numRows = scala.math.pow(2, idx).intValue
    val numCols = scala.math.pow(2, idx).intValue
    val dataSize = 32
    val useOBR = true
    val numBanks = 1
    val maxParReadReqs = 64

    val matA_name = "matrix1"
    val matB_name = "matrix2"
    val matA = Matrix.fromRandom(numRows, numCols, 255, Some("A"))
    val matB = Matrix.fromRandom(numRows, numCols, 255, Some("B"))
    val matC = matA.mul(matB)

    val projName = "OBR_MxM_" + numRows +"_Rows_" + numCols + "_Cols_" + dataSize + "_Bits_Profiling"
    val tree = MMMul(Input(matA_name, numRows, numRows, algo.IntType(dataSize)), Input(matB_name, numRows, numRows, algo.IntType(dataSize)))

    HDLProject( projName,
                tree,
                CompilerPhase.first(),
                Seq((ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputRow(matA_name, numBanks, useOBR)))),
                    (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix(matB_name, numBanks, useOBR)))),
                    (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(maxParReq = maxParReadReqs)))),
                    (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.get(Some(numCols)))),
                    (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.doubleBufferRead))),
                    (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
                    (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
                )
              ).simulateWithHostRam(Predef.Map(
                matA_name -> matA.data,
                matB_name -> matB.data,
                MemoryImage.RESULT_VAR_NAME -> matC.data),
                useOnBoardRAM = useOBR).assertCorrect.print()
            }

////  @Test
////  def testMAddMemInput(): Unit = { // 20 sec (38924 cycles) (9229 cycles with 16x parallel dot product)
////    Counter.resetAll()
////    val num = 4
////    val dataSize = 512
////    val matA = Matrix.fromRandom(num, num, 9, Some("A"))
////    val matB = Matrix.fromRandom(num, num, 9, Some("B"))
////    val matC = matA.add(matB)
////    HDLProject("mm_red_"+num.toString, MAdd(Input("matrix1", num, num, algo.IntType(dataSize)), Input("matrix2", num, num, algo.IntType(dataSize))), CompilerPhase.first(),
////      Seq(
////        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInput("matrix1", 1, false)))),
////        (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInput("matrix2", 1, false)))),
////        (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.all)),
////      )
////    ).simulateWithHostRam(Predef.Map(
////      "matrix1" -> matA.data,
////      "matrix2" -> matB.data,
////      MemoryImage.RESULT_VAR_NAME -> matC.data
////    ), useOnBoardRAM = true).assertCorrect.print()
////  }

}

