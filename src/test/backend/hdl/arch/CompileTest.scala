package backend.hdl.arch

import algo.{Concat, DotProduct}
import backend.hdl._
import backend.hdl.arch.mem.{BankedBufferRWStream, BufferStream2DInBlockRam, BufferStreamInBlockRam, MemFunctionsCompiler}
import backend.hdl.arch.rewrite.transpose.TransposeConversionRules
import backend.hdl.arch.rewrite.{FixTimingRules, MoveUpJoinStreamRules, MoveUpVectorToStreamRules, RemoveSelectRules}
import core._
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, Rules}
import org.junit.Test

class CompileTest {

  @Test
  def testId(): Unit = {
    Counter.resetAll()
    val tree = Sink(Id(ConstantValue(4)))

    HDLProject("idconst", tree).compileHDL
  }

  @Test
  def testMapZip(): Unit = {
    Counter.resetAll()
    def zip(input1: Expr): LambdaT = {
      val stvp = ParamDef()
      ArchLambda(stvp, Zip2OrderedStream(Tuple2(input1, ParamUse(stvp))))
    }
    val tree = Sink(
      MapOrderedStream(
        zip(CounterInteger(0, 1, Seq(3, 4), Some(IntType(8)))),
        CounterInteger(0, 1, Seq(3, 4, 5), Some(IntType(8)))
      )
    )

    HDLProject("mapzip", tree).compileHDL
  }

  @Test
  def reduce(): Unit = {
    Counter.resetAll()
    val initValue = ConstantValue(0, Some(IntType(8)))
    val input = CounterInteger(0, 1, Seq(3), Some(IntType(8)))

    val tree = ReduceOrderedStream(
      {
        val accParam = ParamDef()
        val addParam = ParamDef()
        ArchLambdas(
          Seq(accParam, addParam),
          AddInt(Tuple2(
            ParamUse(accParam),
            ParamUse(addParam)
          ))
        )
      },
      initValue,
      input
    )

    HDLProject("reduce", tree).compileHDL
  }

  @Test
  def testUnorderedStream(): Unit = {
    Counter.resetAll()
    val tree = OrderedStreamToUnorderedStream(
      CounterInteger(0, 1, Seq(3, 4), Some(IntType(8)))
    )

    HDLProject("uos", tree).compileHDL
  }

  @Test
  def testMapVector(): Unit = {
    Counter.resetAll()
    val incF = {
      val in = ParamDef()
      ArchLambda(
        in,
        AddInt(Tuple2(ParamUse(in), ConstantValue(1)))
      )
    }
    val tree = MapVector(incF, OrderedStreamToVector(CounterInteger(0, 1, Seq(4), Some(IntType(8)))))

    HDLProject("mapvec", tree).compileHDL //simulateStandalone
  }

  @Test
  def testFoldVector(): Unit = {
    Counter.resetAll()
    val tree = FoldVector.sum(OrderedStreamToVector(CounterInteger(0, 1, Seq(32), Some(IntType(32)))))

    HDLProject("foldvector", tree).compileHDL //simulateStandalone
  }

  @Test
  def testFoldVectorNonPow2(): Unit = {
    Counter.resetAll()
    val tree = FoldVector.sum(OrderedStreamToVector(CounterInteger(0, 1, Seq(25), Some(IntType(32)))))

    HDLProject("foldvectorNonPow2", tree, CompilerPhase.first(), Seq(
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
    )).compileHDL
  }

  @Test
  def testFoldVectorMin1(): Unit = {
    Counter.resetAll()
    val tree = FoldVector.minimum(OrderedStreamToVector(CounterInteger(1, 1, Seq(25), Some(IntType(32)))))

    HDLProject("fold-vector-min1", tree, CompilerPhase.first()).compileHDL
  }

  @Test
  def testFoldVectorMin2(): Unit = {
    Counter.resetAll()
    val tree = FoldVector.minimum(OrderedStreamToVector(CounterInteger(1, 1, Seq(25), Some(SignedIntType(32)))))

    HDLProject("fold-vector-min2", tree, CompilerPhase.first()).compileHDL
  }

  @Test
  def testFoldVectorMax1(): Unit = {
    Counter.resetAll()
    val tree = FoldVector.maximum(OrderedStreamToVector(CounterInteger(1, 1, Seq(25), Some(IntType(32)))))

    HDLProject("fold-vector-max1", tree, CompilerPhase.first()).compileHDL
  }

  @Test
  def testFoldVectorMax2(): Unit = {
    Counter.resetAll()
    val tree = FoldVector.maximum(OrderedStreamToVector(CounterInteger(1, 1, Seq(25), Some(SignedIntType(32)))))

    HDLProject("fold-vector-max2", tree, CompilerPhase.first()).compileHDL
  }

  @Test
  def testIncreaseIntegerBitWidth(): Unit = {
    Counter.resetAll()
    val tree = TruncInteger(ConstantValue(9, Some(IntType(10))), 15)

    HDLProject("IncInt", tree, CompilerPhase.first(), Seq(
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
    )).compileHDL //simulateStandalone
  }

  @Test
  def testPermuteVector(): Unit = {
    Counter.resetAll()
    val inputLen = 9
    val input = CounterInteger(1, 1, 1, Seq(inputLen), None)
    val v = ArithTypeVar()

    val test = PermuteVector(OrderedStreamToVector(input), ArithLambdaType(v, (v+1)%9))
    HDLProject("permute", test).compileHDL
  }

  @Test
  def testSlideVector(): Unit = {
    Counter.resetAll()

    Rules.disabled = Seq("moveDownSlideVector_skipStreamToVector")

    val inputLen = 9
    val input = CounterInteger(1, 1, 1, Seq(inputLen), None)
    val test = SlideVector(OrderedStreamToVector(input), 3, 2)

    HDLProject("SlideVecotr", test).compileHDL
  }

  @Test
  def testMapRegisterBuffer(): Unit = {
    Counter.resetAll()
    val vectorBuffer: Expr = {
      val stvp = ParamDef()
      ArchLambda(stvp, VectorToOrderedStream(OrderedStreamToVector(ParamUse(stvp))))
    }
    val tree = Sink(
      MapOrderedStream(
        vectorBuffer,
        CounterInteger(0, 1, Seq(8, 4))
      )
    )

    HDLProject("mapbuffer", tree).compileHDL
  }

  @Test
  def testSharedFunction(): Unit = {
    Counter.resetAll()

    val idParam = ParamDef(IntType(10))
    val func = ArchLambda(idParam, Id(ParamUse(idParam)))
    val funcParam = ParamDef(func.t)
    val tree = Sink(
      Let(
        funcParam,
        FunctionCall(
          ParamUse(funcParam),
          FunctionCall(
            ParamUse(funcParam),
            ConstantValue(3, Some(IntType(10)))
          )
        ),
        func
      )
    )

    HDLProject("sharedfunc", tree).compileHDL //simulateStandalone
  }

  @Test
  def testAlternate(): Unit = {
    /**
     * Note that the ConstantValues have different values.
     * To pass type checking, the parameter types must be explicitly defined.
     * Or just make the two ConstantValues have the same value.
     */
    Counter.resetAll()
    val f1 = {
      val p = ParamDef(IntType(8))
      ArchLambda(p, AddInt(Tuple2(ParamUse(p), ConstantValue(1))))
    }
    val f2 = {
      val p = ParamDef(IntType(8))
      ArchLambda(p, AddInt(Tuple2(ParamUse(p), ConstantValue(2))))
    }
    val tree = Alternate(f1, f2, ConstantValue(10, Some(IntType(8))))

    HDLProject("alternate", tree).compileHDL //simulateStandalone
  }

  @Test
  def testBRam(): Unit = {
    Counter.resetAll()
    val tree = {
      BufferStreamInBlockRam(
//      CounterInteger(4, 1, Seq(4))
        CounterInteger(0, 1, Seq(8, 4))
      )
    }

    HDLProject("bram", tree).compileHDL
  }

  @Test
  def testMapBRam(): Unit = {
    Counter.resetAll()
    val input = CounterInteger(0, 1, Seq(8, 4))
    val tree = MapOrderedStream(
      {
        val param = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(param, BufferStreamInBlockRam(ParamUse(param)))
      }, input
    )

    HDLProject("bram", tree).compileHDL
  }

  @Test
  def testMapVectorBuffer(): Unit = {
    Counter.resetAll()
    val vectorBuffer: Expr = {
      val stvp = ParamDef()
      ArchLambda(stvp, UnorderedStreamToOrderedStream(OrderedStreamToUnorderedStream(ParamUse(stvp))))
    }
    val tree = Sink(
      MapOrderedStream(
        vectorBuffer,
        CounterInteger(0, 1, Seq(8, 4))
      )
    )

    HDLProject("mapbuffer", tree).compileHDL
  }

  @Test
  def testUntuple(): Unit = {
    val input = algo.Tuple2(algo.CounterInteger(0, 1, 4), algo.CounterInteger(0, 1, 4))
    val param = ParamDef()
    val tree = Let(
      param,
      Concat(algo.Select2(ParamUse(param), 0), algo.Select2(ParamUse(param), 1)),
      input
    )
    HDLProject("untuple", tree).compileHDL
  }

  @Test
  def testFold2DStream(): Unit = {
    val tree =
      FoldOrderedStream(
        {
          val p1 = ParamDef()
          val p2 = ParamDef()
          ArchLambdas(Seq(p1, p2),
            MapOrderedStream(
              AddInt.asFunction(),
              Zip2OrderedStream(Tuple2(ParamUse(p1), ParamUse(p2)))
            )
          )
        },
        CounterInteger(0, 1, Seq(8, 8))
      )

    HDLProject("fold2D", tree).compileHDL
  }

  @Test
  def testFold3DStream(): Unit = {
    val input = CounterInteger(0, 1, 1, Seq(8, 8, 8), None)
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val tree = MapOrderedStream(
      {
        val param = ParamDef(inputtc.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(
          param,
          FoldOrderedStream(
            {
              val p1 = ParamDef()
              val p2 = ParamDef()
              ArchLambdas(Seq(p1, p2),
                MapOrderedStream(
                  AddInt.asFunction(),
                  Zip2OrderedStream(Tuple2(ParamUse(p1), ParamUse(p2)))
                )
              )
            },
            ParamUse(param)
          )
        )
      }, inputtc
    )
    HDLProject("fold3D", tree).compileHDL
  }

  @Test
  def testConcatStreamDimsInnerMost(): Unit = {
    val input1 = CounterInteger(0, 1, 1, Seq(2, 8, 2), Some(IntType(8)))
    val input2 = CounterInteger(0, 1, 1, Seq(4, 8, 2), Some(IntType(8)))
    val tree = ConcatOrderedStream(input1, input2, 2)
    HDLProject("concatstm_innermost", tree).compileHDL
  }

  @Test
  def testConcatStreamDimsMid(): Unit = {
    val input1 = CounterInteger(0, 1, 1, Seq(2, 8, 2), Some(IntType(8)))
    val input2 = CounterInteger(0, 1, 1, Seq(2, 8, 2), Some(IntType(8)))
    val tree = ConcatOrderedStream(input1, input2, 1)
    HDLProject("concatstm_mid", tree).compileHDL
  }

  @Test
  def testConcatStreamDimsOuterMost(): Unit = {
    val input1 = CounterInteger(0, 1, 1, Seq(2, 8, 2), Some(IntType(8)))
    val input2 = CounterInteger(0, 1, 1, Seq(2, 8, 2), Some(IntType(8)))
    val tree = ConcatOrderedStream(input1, input2, 0)
    HDLProject("concatstm_outermost", tree).compileHDL
  }

  @Test
  def testRotateVector(): Unit = {
    val input = CounterInteger(0, 1, 1, Seq(8), Some(IntType(8)))
    val tree = RotateVector(Tuple2(OrderedStreamToVector(input), ConstantValue(2)), 0)
    HDLProject("rotate", tree).compileHDL
  }

  @Test
  def testBankedRamWriteAllBanks(): Unit = {
    val numBanks = 4
    val input = CounterInteger(0, 1, 1, Seq(8,8), Some(IntType(8)))
    val inputVec = MapOrderedStream(OrderedStreamToVector.asFunction(), input)
    val wAddr = MapOrderedStream(VectorGenerator.asFunction(Seq(None), Seq(numBanks)), CounterInteger(0, 1, 1, Seq(8), None))
    val wEn = Repeat(VectorGenerator(ConstantValue(1, Some(LogicType())), numBanks), 8)
    val rAddr = MapOrderedStream(VectorGenerator.asFunction(Seq(None), Seq(numBanks)), CounterInteger(0, 1, 1, Seq(8), None))
    val tree = BankedBufferRWStream(inputVec, wAddr, wEn, rAddr, numBanks, 8)
    HDLProject("bankedRam", tree).compileHDL
  }

  @Test
  def testBankedRamWriteSingleBank(): Unit = {
    val numBanks = 4
    val numOfAddresses = 2
    val totalElements = numBanks * numOfAddresses
    val input = CounterInteger(0, 1, 1, Seq(totalElements), Some(IntType(8)))
    val inputVec = MapOrderedStream(VectorGenerator.asFunction(Seq(None), Seq(numBanks)), input)
    val wAddr = MapOrderedStream(VectorGenerator.asFunction(Seq(None), Seq(numBanks)),
      JoinOrderedStream(MapOrderedStream(Repeat.asFunction(Seq(None), Seq(numBanks)), CounterInteger(0, 1, 1, Seq(numOfAddresses), None))))

    // Calculate write enable signals. Only one bank is accessed at a time
    val enableBase = ConcatVector(Tuple2(VectorGenerator(ConstantValue(1, Some(LogicType())), 1), VectorGenerator(ConstantValue(0, Some(LogicType())), numBanks - 1)))
    val enableBaseRepeat = Repeat(enableBase, totalElements)
    val enableBaseRotateDistance = JoinOrderedStream(Repeat(CounterInteger(0, 1, 1, Seq(numBanks), None), numOfAddresses))
    val wEn = MapOrderedStream(RotateVector.asFunction(Seq(None), Seq(1)), Zip2OrderedStream(Tuple2(enableBaseRepeat, enableBaseRotateDistance)))

    val rAddr = MapOrderedStream(VectorGenerator.asFunction(Seq(None), Seq(numBanks)), CounterInteger(0, 1, 1, Seq(numOfAddresses), None))
    val tree = BankedBufferRWStream(inputVec, wAddr, wEn, rAddr, numBanks)
    HDLProject("bankedRam", tree).compileHDL
  }

  @Test
  def testTest(): Unit = {
    val input1 = CounterInteger(0, 1, Seq(8,8), Some(IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(8,8), Some(IntType(8)))
    val input3 = CounterInteger(0, 1, Seq(8), Some(IntType(8)))
    val tupInput = Zip2OrderedStream(Tuple2(Zip2OrderedStream(Tuple2(input1, input2)), input3))
    val tupInputtc = TypeChecker.checkIfUnknown(tupInput, tupInput.t)
    val tree = TypeChecker.check(MapOrderedStream(
      {
        val param = ParamDef(tupInputtc.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(
          param,
          Tuple2(Select2(Select2(ParamUse(param), 0), 0), Repeat(Select2(ParamUse(param), 1), 8))
        )
      }, tupInputtc
    ))

    val treetc = RewriteStep(RewriteAll(), RemoveSelectRules.get()).apply(tree)
    assert(!treetc.hasUnknownType)
    assert(tree.t == treetc.t)
  }

  @Test
  def testIdStmFunc(): Unit = {
    Counter.resetAll()

    val input = CounterInteger(1, 1, Seq(8), Some(IntType(8)))
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val bufParam = ParamDef(inputtc.t)
    val func = ArchLambda(bufParam, Id(ParamUse(bufParam)))
    val functc = TypeChecker.check(func)
    val funcParam = ParamDef(functc.t)

    val tree = Sink(
      Let(
        funcParam,
        FunctionCall(
          ParamUse(funcParam),
          // The buffer is required to store the whole stream or the arbiter will get stuck.
          BufferStreamInBlockRam(FunctionCall(
            ParamUse(funcParam),
            inputtc
          ))
        ),
        functc
      )
    )

    HDLProject("IdStmFunc", tree).compileHDL
  }

  @Test
  def testIdStm2DFunc(): Unit = {
    Counter.resetAll()

    val input = CounterInteger(1, 1, Seq(5,5), Some(IntType(8)))
    val inputtc = TypeChecker.checkIfUnknown(input, input.t)
    val bufParam = ParamDef(inputtc.t)
    val func = ArchLambda(bufParam, Id(ParamUse(bufParam)))
    val functc = TypeChecker.check(func)
    val funcParam = ParamDef(functc.t)

    val tree = Sink(
      Let(
        funcParam,
        FunctionCall(
          ParamUse(funcParam),
          // The buffer is required to store the whole stream or the arbiter will get stuck.
          SplitOrderedStream(BufferStreamInBlockRam(JoinStream(FunctionCall(
            ParamUse(funcParam),
            inputtc
          ))), 5)
        ),
        functc
      )
    )

    HDLProject("IdStm2DFunc", tree).compileHDL
  }

  @Test
  def testRemoveTransposeND(): Unit = {
    val input = CounterInteger(0, 1, Seq(2, 3, 4, 5), Some(IntType(32)))
    val tree = TransposeNDOrderedStream(TransposeNDOrderedStream(input, Seq(0, 2, 1, 3)), Seq(0, 2, 1, 3))

    HDLProject("noTranspose", tree, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), TransposeConversionRules.get())),
    )).compileHDL
  }

  def testMoveUpJoin_skipMapNDSplit(): Unit = {
    val input = Id(CounterInteger(0, 1, Seq(9, 6, 5, 4, 3, 2), Some(IntType(32))))
    val joinedInput = TypeChecker.check(JoinOrderedStream(input))
    val tree = MapOrderedStream(4, SplitOrderedStream.asFunction(Seq(None), Seq(3)), joinedInput)


    HDLProject("moveUpJoin_skipMapNDSplit", tree, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(MoveUpJoinStreamRules.skipMapNDSplit))),
    )).compileHDL
  }

  @Test
  def testMoveUpVecToStm_mapNDFission(): Unit = {
    val input = TypeChecker.check(Id(CounterInteger(0, 1, Seq(6, 5, 4, 3, 2), Some(IntType(32)))))
    val tree = MapOrderedStream(4,
      {
        val param = ParamDef()
        ArchLambda(
          param,
          MapOrderedStream(VectorToOrderedStream.asFunction(), SlideOrderedStream(ParamUse(param), 3, 1))
        )
      }, input
    )

    HDLProject("moveUpVecToStm_mapNDFission", tree, CompilerPhase.first(), Seq(
      (ArchCompiler.phaseAfter, RewriteStep(RewriteAll(), MoveUpVectorToStreamRules.get())),
    )).compileHDL
  }

  @Test
  def testMapBuffer(): Unit = {
    Counter.resetAll()
    val input = CounterInteger(0, 1, Seq(5, 5), None)
    val test = MapOrderedStream(
      {
        val param = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(
          param,
          BufferStreamInBlockRam(ParamUse(param))
        )
      }, input
    )
    HDLProject("mapBuf", test).compileHDL
  }

  @Test
  def testTransposeIndexStream(): Unit = {
    Counter.resetAll()
    val tree = IndexStream(JoinOrderedStream(JoinOrderedStream(TransposeNDOrderedStream(Id(CounterInteger(0, 1, Seq(3,4,5))), Seq(2, 0, 1)))))

    HDLProject("transposeIndex", tree).compileHDL
  }

  @Test
  def testSubtraction(): Unit = {
    Counter.resetAll()

    val tree = Unsigned(SubInt(Tuple2(Signed(ConstantValue(10)), Signed(ConstantValue(3)))))

    HDLProject("subtraction", tree).compileHDL //simulateStandalone()
  }

  @Test
  def testReLU(): Unit = {
    Counter.resetAll()

    val tree = Unsigned(ReLUInt(SubInt(Tuple2(Signed(ConstantValue(3)), Signed(ConstantValue(10))))))

    HDLProject("relu", tree).compileHDL //.simulateStandalone()
  }

  @Test
  def testSignedConst(): Unit = {
    Counter.resetAll()

    val tree = ConstantValue(-1, Some(SignedIntType(8)))

    HDLProject("signedConst", tree).compileHDL
  }

  @Test
  def testSignedSum(): Unit = {
    Counter.resetAll()

    val tree = FoldOrderedStream(AddInt2.asFunction(), TypeChecker.check(MapOrderedStream(Signed.asFunction(), CounterInteger(0, 1, Seq(8)))))

    HDLProject("signedSum", tree).compileHDL
  }

  @Test
  def testFold2DStreamSigned(): Unit = {
    val tree =
      FoldOrderedStream(
        {
          val p1 = ParamDef()
          val p2 = ParamDef()
          ArchLambdas(Seq(p1, p2),
            MapOrderedStream(
              AddInt.asFunction(),
              Zip2OrderedStream(Tuple2(ParamUse(p1), ParamUse(p2)))
            )
          )
        },
        TypeChecker.check(MapOrderedStream(2, Signed.asFunction(), CounterInteger(0, 1, Seq(8, 8))))
      )

    HDLProject("fold2DSigned", tree).compileHDL
  }

  @Test
  def testFoldVectorSigned(): Unit = {
    Counter.resetAll()
    val tree = FoldVector.sum(OrderedStreamToVector(MapOrderedStream(Signed.asFunction(), CounterInteger(0, 1, Seq(32), Some(IntType(32))))))

    HDLProject("foldvectorsigned", tree).compileHDL
  }

  @Test
  def testCounterInFunction(): Unit = {
    Counter.resetAll()
    val input = CounterInteger(0, 1, Seq(5))
    val fCount = CounterInteger(5, 1, Seq(5))
    val iParam = ParamDef(input.t)
    val funBody = TypeChecker.check(MapOrderedStream(
      {
        val param = ParamDef()
        ArchLambda(
          param,
          Select2(Id(ParamUse(param)), 0)
        )
      }, Zip2OrderedStream(Tuple2(ParamUse(iParam), fCount))
    ))
    val fun = TypeChecker.check(ArchLambda(iParam, funBody))
    val funParam = ParamDef(fun.t)
    val tree = Let(
      funParam,
      FunctionCall(ParamUse(funParam), BufferStreamInBlockRam(FunctionCall(ParamUse(funParam), input))),
      fun
    )

    HDLProject("counterfun", tree).compileHDL
  }

  @Test
  def testCounterInFunctionWithMap(): Unit = {
    Counter.resetAll()
    val input = CounterInteger(0, 1, Seq(5))
    val fCount = CounterInteger(5, 1, Seq(5))
    val iParam = ParamDef(input.t)
    val funBody = TypeChecker.check(MapOrderedStream(
      {
        val param = ParamDef()
        ArchLambda(
          param,
          Select2(Id(ParamUse(param)), 0)
        )
      }, Zip2OrderedStream(Tuple2(ParamUse(iParam), fCount))
    ))
    val fun = TypeChecker.check(ArchLambda(iParam, funBody))
    val funParam = ParamDef(fun.t)
    val intermediate = BufferStreamInBlockRam(FunctionCall(ParamUse(funParam), input))
    val tree = Let(
      funParam,
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            FunctionCall(ParamUse(funParam), ParamUse(param)),
          )
        }, Repeat(intermediate, 3)
      ),
      fun
    )

    HDLProject("counterfunwithmap", tree).compileHDL
  }

  @Test
  def testCounterInFunctionWithMap2D(): Unit = {
    Counter.resetAll()
    val input = CounterInteger(0, 1, Seq(5))
    val fCount = CounterInteger(5, 1, Seq(5))
    val iParam = ParamDef(input.t)
    val funBody = TypeChecker.check(MapOrderedStream(
      {
        val param = ParamDef()
        ArchLambda(
          param,
          Select2(Id(ParamUse(param)), 0)
        )
      }, Zip2OrderedStream(Tuple2(ParamUse(iParam), fCount))
    ))
    val fun = TypeChecker.check(ArchLambda(iParam, funBody))
    val funParam = ParamDef(fun.t)
    val intermediate = BufferStreamInBlockRam(FunctionCall(ParamUse(funParam), input))
    val tree = Let(
      funParam,
      MapOrderedStream(
        2, {
          val param = ParamDef()
          ArchLambda(
            param,
            FunctionCall(ParamUse(funParam), ParamUse(param)),
          )
        }, Repeat(Repeat(intermediate, 3), 2)
      ),
      fun
    )

    HDLProject("counterfunwithmap2d", tree).compileHDL
  }

  @Test
  def testRepeatHiddenMultiplie(): Unit = {
    Counter.resetAll()
    val input0 = CounterInteger(0, 1, Seq(5))
    val input1 = CounterInteger(0, 1, Seq(5))
    val newInput = TypeChecker.check(Tuple2(input0, input1))
    val inputParam = ParamDef(newInput.t)
    val tree = Let(
      inputParam,
      Tuple2(RepeatHidden(Select2(RepeatHidden(ParamUse(inputParam), 1), 0), 1), Select2(RepeatHidden(RepeatHidden(ParamUse(inputParam), 1), 1), 1)),
      newInput
    )

    HDLProject("testrepeathiddenmultiple", tree).compileHDL
  }

  @Test
  def testBRAM2D(): Unit = {
    Counter.resetAll()
    val tree = BufferStream2DInBlockRam(CounterInteger(0, 1, Seq(5, 5)))

    HDLProject("testBRAM2D", tree).compileHDL
  }
  @Test
  def testConstIntVector1(): Unit = {
    Counter.resetAll()
    val tree = ConstantVector(Seq(1, 2, 3, -4))

    HDLProject("ConstantVector", tree).compileHDL
  }

  @Test
  def testConstIntVector2(): Unit = {
    Counter.resetAll()
    val tree = ConstantVector(Seq(1, 2, 3, 4), Some(SignedIntType(4)))

    HDLProject("ConstantVector", tree).compileHDL
  }

  @Test
  def testClipRound(): Unit = {
    Counter.resetAll()
    val tree = ClipBankersRound(ConstantValue(-126, Some(SignedIntType(8))), 3, 2)

    HDLProject("clip-round", tree).compileHDL
  }

  @Test
  def testClipRound2(): Unit = {
    Counter.resetAll()
    val tree = MapOrderedStream(ClipBankersRound.asFunction(types = Seq(2, 1)),
      CounterInteger(0, 1, Seq(16), Some(IntType(5))))

    HDLProject("clip-round2", tree).compileHDL
  }

  @Test
  def testMapStm2VecSize1(): Unit = {
    Counter.resetAll()
    val tree = MapOrderedStream(OrderedStreamToVector.asFunction(), CounterInteger(0, 1, Seq(1, 10)))

    HDLProject("testMapStm2VecSize1", tree).compileHDL
  }

  @Test
  def testAddSigned(): Unit = { // This should throw an error since Tuple(Int, SignedInt) is unsupported!
    Counter.resetAll()
    val tree = AddInt(TypeChecker.check(Tuple2(ConstantValue(-1, Some(IntType(2))), ConstantValue(0, Some(SignedIntType(3))))))

    HDLProject("AddSigned", tree).compileHDL
  }

  @Test
  def testMax(): Unit = {
    Counter.resetAll()
    val tree = MaxInt(Tuple2(ConstantValue(1, Some(SignedIntType(8))), ConstantValue(3, Some(SignedIntType(8)))))
    HDLProject("testMax", tree).compileHDL //simulateStandalone
  }

  @Test
  def testMaxWithConversion(): Unit = {
    /**
     * MaxInt can only take either Int-Int or SignedInt-Signed.
     * There will be an error if remove the Conversion around ConstantValue.
     */
    Counter.resetAll()
    val tree = MaxInt(Tuple2(ConstantValue(1, Some(SignedIntType(8))), Conversion(ConstantValue(3, Some(IntType(8))), SignedIntType(8))))
    HDLProject("testMaxWithConversion", tree).compileHDL //simulateStandalone
  }

  @Test
  def testArchConvCachelineUnpack(): Unit = {
    // the input here is [i7 x 100 x 1], which clearly does not perfectly fit
    // on the 512 bit line. this test makes sure the extra padding bits are
    // dropped accordingly (instead of triggering a type-checker error)
    Counter.resetAll()
    val tree = mem.Input("input", 100, 1, SignedIntType(7))
    HDLProject("testArchConvCachelineUnpack", tree).compileHDL
  }

  @Test
  def testPadWithVec(): Unit = {
    Rules.disabled = Seq("MoveDownPad_skipMap")
    Counter.resetAll()
    val tree = PadOrderedStream(MapOrderedStream(OrderedStreamToVector.asFunction(), CounterInteger(0, 1, Seq(5, 17), None)), 3, 2, 0)
    HDLProject("testPadWithVec", tree).compileHDL
  }

  @Test
  def testRepeatSplit(): Unit = {
    Counter.resetAll()
    val tree = MapOrderedStream(Repeat.asFunction(Seq(None), Seq(3)), SplitOrderedStream(Id(CounterInteger(0, 1, Seq(16))), 4))
    HDLProject("testRepeatSplit", tree).compileHDL
  }

  @Test
  def testOuterSplit(): Unit = {
    Counter.resetAll()
    val tree = MapOrderedStream({
      val p = ParamDef()
      ArchLambda(p,
        SplitOrderedStream(
          JoinOrderedStream(SplitOrderedStream(
            ConcatOrderedStream(ParamUse(p),
              CounterInteger(0, 1, Seq(3), Some(IntType(16)))),
            3, innerSize = false)),
          101, innerSize = false)
      )
    }, CounterInteger(0, 1, Seq(300, 2), Some(IntType(16))))
    HDLProject("testOuterSplit", tree).compileHDL
  }

  @Test
  def testSlideGeneral(): Unit = {
    Counter.resetAll()

    val input = CounterInteger(1, 1, Seq(10, 10), None)
    val test = SlideGeneralOrderedStream(input, 3, 1)

    HDLProject("testSlideGeneral", test).compileHDL
  }

  @Test
  def testUpperBoundedStream(): Unit = {
    Counter.resetAll()
    val testType1 = UpperBoundedStreamType(IntType(10), 3)
    val testType2 = OrderedStreamType(IntType(10), 3)
    print(testType1.isInstanceOf[OrderedStreamTypeT] + "\n")
    print(testType2.isInstanceOf[UpperBoundedStreamTypeT] + "\n")
  }

  @Test
  def testOrderedStreamToUpperBoundedStream(): Unit = {
    Counter.resetAll()
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(8), Some(IntType(8))))
    val tree = OrderedStreamToUpperBoundedStream(input, 16)
    HDLProject("testOrderedStreamToUpperBoundedStream", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testOrderedStreamToUpperBoundedStreamInvalid(): Unit = { // This should introduce an error!
    Counter.resetAll()
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(8), Some(IntType(8))))
    val tree = OrderedStreamToUpperBoundedStream(input, 4)
    HDLProject("testOrderedStreamToUpperBoundedStreamInvalid", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testFoldDifferentSizes(): Unit = {
    Counter.resetAll()
    val param = ParamDef(UpperBoundedStreamType(IntType(8), 16))
    val fun =
      ArchLambda(param, FoldOrderedStream(
        {
          val p1 = ParamDef()
          val p2 = ParamDef(IntType(8))
          ArchLambdas(Seq(p1, p2),
            AddInt(Tuple(ParamUse(p1), ParamUse(p2)))
          )
        },
        ParamUse(param)
      ))
    val input = TypeChecker.check(CounterInteger(0, 1, Seq(8), Some(IntType(8))))
    val tree = FunctionCall(fun, OrderedStreamToUpperBoundedStream(input, 16))
    HDLProject("testFoldDifferentSizes", tree).compileHDL//simulateStandalone()
  }

  @Test
  def Map2funcTest: Unit = {
    Counter.resetAll()
    val input1 = CounterInteger(0, 1, Seq(5, 5))
    val input2 = CounterInteger(0, 1, Seq(5, 5))
    val param1 = ParamDef(input1.t.asInstanceOf[OrderedStreamTypeT].et)
    val param2 = ParamDef(input2.t.asInstanceOf[OrderedStreamTypeT].et)
    val fun = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, ConcatOrderedStream(ParamUse(param2), ParamUse(param1)))))
    val tree = MapAsyncOrderedStream(fun, input1, input2)

    HDLProject("map2Input", tree).compileHDL //simulateStandalone()
  }

  @Test
  def Map2func2DTest: Unit = {
    Counter.resetAll()
    val input1 = CounterInteger(0, 1, Seq(5, 5, 5))
    val input2 = CounterInteger(0, 1, Seq(5, 5, 5))
    val param1 = ParamDef(input1.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
    val param2 = ParamDef(input2.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
    val fun = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, ConcatOrderedStream(ParamUse(param1), ParamUse(param2)))))
    val tree = MapAsyncOrderedStream(2, fun, input1, input2)

    HDLProject("map2Input", tree).compileHDL //simulateStandalone()
  }

  @Test
  def Map2func2DTestDifferntInnerStream: Unit = {
    Counter.resetAll()
    val input1 = CounterInteger(0, 1, Seq(5, 5, 5), Some(IntType(16)))
    val input2 = CounterInteger(0, 1, Seq(15, 5, 5), Some(IntType(16)))
    val param1 = ParamDef(input1.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
    val param2 = ParamDef(input2.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
    val fun = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, ConcatOrderedStream(ParamUse(param1), ParamUse(param2)))))
    val tree = MapAsyncOrderedStream(2, fun, input1, input2)

    HDLProject("map2Input", tree).compileHDL //simulateStandalone()
  }

  @Test
  def Map2InputNDTest: Unit = {
    Counter.resetAll()
    val input1 = CounterInteger(0, 1, Seq(5, 5, 5), Some(IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(5, 5, 5), Some(IntType(10)))
    val param1 = ParamDef(input1.t.asInstanceOf[OrderedStreamTypeT].et)
    val param2 = ParamDef(input2.t.asInstanceOf[OrderedStreamTypeT].et)
    val param3 = ParamDef(param1.t.asInstanceOf[OrderedStreamTypeT].et)
    val param4 = ParamDef(param2.t.asInstanceOf[OrderedStreamTypeT].et)

    val fun2 = TypeChecker.check(ArchLambda(param3, ArchLambda(param4, ConcatOrderedStream(MapOrderedStream(ResizeInteger.asFunction(Seq(None), Seq(10)), ParamUse(param3)), ParamUse(param4)))))
    val fun1 = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, MapAsyncOrderedStream(fun2, ParamUse(param1), ParamUse(param2)))))
    val tree = MapAsyncOrderedStream(fun1, input1, input2)

    HDLProject("Map2InputND", tree).compileHDL//simulateStandalone()
  }

  @Test
  def Map2InputNDTestFlipOrder: Unit = {
    Counter.resetAll()
    val input1 = CounterInteger(0, 1, Seq(5, 5, 5), Some(IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(5, 5, 5), Some(IntType(10)))
    val param1 = ParamDef(input1.t.asInstanceOf[OrderedStreamTypeT].et)
    val param2 = ParamDef(input2.t.asInstanceOf[OrderedStreamTypeT].et)
    val param3 = ParamDef(param1.t.asInstanceOf[OrderedStreamTypeT].et)
    val param4 = ParamDef(param2.t.asInstanceOf[OrderedStreamTypeT].et)

    val fun2 = TypeChecker.check(ArchLambda(param4, ArchLambda(param3, ConcatOrderedStream(MapOrderedStream(ResizeInteger.asFunction(Seq(None), Seq(10)), ParamUse(param3)), ParamUse(param4)))))
    val fun1 = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, MapAsyncOrderedStream(fun2, ParamUse(param2), ParamUse(param1)))))
    val tree = MapAsyncOrderedStream(fun1, input1, input2)

    HDLProject("Map2InputNDTestFlipOrder", tree).simulateStandalone()
  }

  @Test
  def Map2funcUnboundTest: Unit = {
    Counter.resetAll()
    val input1 = CounterInteger(0, 1, Seq(5), Some(IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(5), Some(IntType(8)))
    val input3 = CounterInteger(0, 1, Seq(3), Some(IntType(8)))
    val param1 = ParamDef(input1.t.asInstanceOf[OrderedStreamTypeT].et)
    val param2 = ParamDef(input2.t.asInstanceOf[OrderedStreamTypeT].et)
    val param3 = ParamDef(input3.t.asInstanceOf[OrderedStreamTypeT].et)
    val fun = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, AddInt2(AddInt2(ParamUse(param1), ParamUse(param2)), ParamUse(param3)))))
    val tree = MapOrderedStream(ArchLambda(param3, MapAsyncOrderedStream(fun, input1, input2)), input3)

    HDLProject("Map2funcUnboundTest", tree).compileHDL//simulateStandalone()
  }

  @Test
  def Map2funcUnboundTestManualFix: Unit = {
    Counter.resetAll()
    val input1 = CounterInteger(0, 1, Seq(5), Some(IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(5), Some(IntType(8)))
    val input3 = CounterInteger(0, 1, Seq(3), Some(IntType(8)))
    val param1 = ParamDef(input1.t.asInstanceOf[OrderedStreamTypeT].et)
    val param2 = ParamDef(input2.t.asInstanceOf[OrderedStreamTypeT].et)
    val param3 = ParamDef(input3.t.asInstanceOf[OrderedStreamTypeT].et)
    val param4 = ParamDef(param3.t)
    val fun = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, ArchLambda(param4, AddInt2(AddInt2(ParamUse(param1), ParamUse(param2)), ParamUse(param4))))))
    val tree = MapOrderedStream(ArchLambda(param3, MapAsyncOrderedStream(fun, input1, input2, Repeat(ParamUse(param3), 5))), input3)

    HDLProject("Map2funcUnboundTestManualFix", tree).compileHDL//simulateStandalone()
  }

  @Test
  def FunCallOrderTest: Unit = {
    Counter.resetAll()
    val input1 = CounterInteger(0, 1, Seq(5), Some(IntType(8)))
    val input2 = CounterInteger(0, 1, Seq(5), Some(IntType(10)))
    val param1 = ParamDef(input1.t)
    val param2 = ParamDef(input2.t)
    val param3 = ParamDef(param1.t)
    val param4 = ParamDef(param2.t)

    val fun2 = TypeChecker.check(ArchLambda(param4, ArchLambda(param3, ConcatOrderedStream(MapOrderedStream(ResizeInteger.asFunction(Seq(None), Seq(10)), ParamUse(param3)), ParamUse(param4)))))
    val fun1 = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, FunctionCall(FunctionCall(fun2, ParamUse(param2)), ParamUse(param1)))))
    val tree = FunctionCall(FunctionCall(fun1, input1), input2)

    HDLProject("FunCallOrderTest", tree).compileHDL//simulateStandalone()
  }

  @Test
  def testMapId(): Unit = {
    val input = CounterInteger(0, 1, Seq(8), Some(IntType(8)))
    val param = ParamDef(IntType(8))
    //val tree = TypeChecker.check(MapOrderedStream(Id.asFunction(), input))
    val tree = MapOrderedStream(ArchLambda(param, Id(ParamUse(param))), input)
    HDLProject("testMapId", tree).compileHDL //simulateStandalone()
  }

  @Test
  def testFoldSum(): Unit = {
    Counter.resetAll()

    val tree = TypeChecker.check(FoldOrderedStream(AddInt2.asFunction(), CounterInteger(0, 1, Seq(8))))

    HDLProject("testFoldSum", tree).compileHDL
  }

  @Test
  def testFoldSum1(): Unit = {
    Counter.resetAll()

    val tree = TypeChecker.check(FoldOrderedStream(AddInt2.asFunction(), CounterInteger(0, 1, Seq(1))))

    HDLProject("testFoldSum", tree).compileHDL
  }

  @Test
  def testCancelSplitJoin1(): Unit = {
    // In this case, moveDownConcat_cancelSplitJoin should *not* happen
    // (otherwise, the output would be incorrect)
    Counter.resetAll()
    val tree = TypeChecker.check(
      SplitOrderedStream(
        ConcatOrderedStream(
          JoinOrderedStream(CounterInteger(0, 1, Seq(4, 3), Some(IntType(4)))),
          Repeat(ConstantValue(0, Some(IntType(4))), 4),
          0), 8)
    )

    HDLProject("cancelSplitJoin1", tree).compileHDL
  }

  @Test
  def testCancelSplitJoin2(): Unit = {
    // In this case, moveDownConcat_cancelSplitJoin should *not* happen
    // (otherwise, the output would be incorrect)
    Counter.resetAll()
    val tree = TypeChecker.check(
      SplitOrderedStream(
        ConcatOrderedStream(
          Repeat(ConstantValue(0, Some(IntType(4))), 4),
          JoinOrderedStream(CounterInteger(0, 1, Seq(4, 3), Some(IntType(4)))),
          0), 8)
    )

    HDLProject("cancelSplitJoin2", tree).compileHDL
  }

  @Test
  def testPadAddr2D1(): Unit = {
    // here, the reconstructed counters have more bits, so we have an explicit
    // ResizeInteger after the ConstrainedSum to remove the extra high bits.
    Counter.resetAll()
    val tree =
      MapOrderedStream(PadOrderedStream.asFunction(types = Seq(10, 10, (1 << 3) - 1)),
        PadOrderedStream(CounterInteger(0, 1, Seq(1, 2, 2), Some(IntType(3))), 10, 10, (1 << 3) - 1))

    HDLProject("padAddr2D1", tree).compileHDL
  }

  @Test
  def testPadAddr2D2(): Unit = {
    // here, the original counter has more bits, so we need to make sure the
    // reconstructed counters are constructed with excess bits so that the
    // invalid after the ConstrainedSum has the correct values.
    Counter.resetAll()
    val tree =
      MapOrderedStream(PadOrderedStream.asFunction(types = Seq(10, 10, (1 << 16) - 1)),
        PadOrderedStream(CounterInteger(0, 1, Seq(1, 2, 2), Some(IntType(16))), 10, 10, (1 << 16) - 1))

    HDLProject("padAddr2D2", tree).compileHDL
  }

  @Test
  def testPadNonZeroOOB1(): Unit = {
    // this ends up turning into a ReadAsyncOrderedOutOfBounds
    // that pads non-zero value.
    Counter.resetAll()
    val PV = 1
    val initInput = TypeChecker.check(
      MapOrderedStream(2, SplitOrderedStream.asFunction(types = Seq(1)),
        SplitOrderedStream(mem.Input("image", 2, 4, SignedIntType(8)), 2)))
    val tree = mem.StoreResult(
      JoinOrderedStream(JoinOrderedStream(
        MapOrderedStream(PadOrderedStream.asFunction(types = Seq(1, 3, PV)),
          PadOrderedStream(initInput, 2, 1, PV)))))

    backend.hdl.HDLProject("padNonZeroOOB1", tree)
      .simulateWithHostRam(Predef.Map(
        "image" -> Seq(Seq(0, 0), Seq(0, 0), Seq(0, 0), Seq(0, 0)),
        "result" -> Seq(
          Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV),
          Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV),
          Seq(PV), Seq(PV), Seq(0),  Seq(0),  Seq(0),  Seq(0),  Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV),
          Seq(PV), Seq(PV), Seq(0),  Seq(0),  Seq(0),  Seq(0),  Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV),
          Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV),
        ),
      ))
      .assertCorrect
  }

  @Test
  def testPadNonZeroOOB2(): Unit = {
    // this ends up turning into a ReadAsyncOrderedOutOfBounds
    // that pads non-zero value.
    Counter.resetAll()
    val PV = 1
    val initInput = TypeChecker.check(
      MapOrderedStream(2, SplitOrderedStream.asFunction(types = Seq(1)),
        SplitOrderedStream(mem.Input("image", 2, 4, SignedIntType(8)), 2)))
    val tree = mem.StoreResult(
      JoinOrderedStream(JoinOrderedStream(
        MapOrderedStream(PadOrderedStream.asFunction(types = Seq(2, 3, PV)),
          initInput))))

    backend.hdl.HDLProject("padNonZeroOOB2", tree)
      .simulateWithHostRam(Predef.Map(
        "image" -> Seq(Seq(0, 0), Seq(0, 0), Seq(0, 0), Seq(0, 0)),
        "result" -> Seq(
          Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(0), Seq(0), Seq(0), Seq(0), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV),
          Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(0), Seq(0), Seq(0), Seq(0), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV), Seq(PV),
        ),
      ))
      .assertCorrect
  }

  @Test
  def testPadNonZeroOOB3(): Unit = {
    // this ends up turning into a ReadAsyncOrderedOutOfBounds
    // that pads non-zero value.
    Counter.resetAll()
    val PV = 1
    val initInput = TypeChecker.check(
      MapOrderedStream(2, SplitOrderedStream.asFunction(types = Seq(1)),
        SplitOrderedStream(mem.Input("image", 2, 4, SignedIntType(8)), 2)))
    val tree = mem.StoreResult(
      JoinOrderedStream(JoinOrderedStream(
          PadOrderedStream(initInput, 2, 3, PV))))

    backend.hdl.HDLProject("padNonZeroOOB3", tree)
      .simulateWithHostRam(Predef.Map(
        "image" -> Seq(Seq(0, 0), Seq(0, 0), Seq(0, 0), Seq(0, 0)),
        "result" -> Seq(
          Seq(PV), Seq(PV), Seq(PV), Seq(PV),
          Seq(PV), Seq(PV), Seq(PV), Seq(PV),
          Seq(0),  Seq(0),  Seq(0),  Seq(0),
          Seq(0),  Seq(0),  Seq(0),  Seq(0),
          Seq(PV), Seq(PV), Seq(PV), Seq(PV),
          Seq(PV), Seq(PV), Seq(PV), Seq(PV),
          Seq(PV), Seq(PV), Seq(PV), Seq(PV),
        ),
      ))
      .assertCorrect
  }

  @Test
  def testPadAndSlide(): Unit = {
    // produces Stm[Vec[Int, 18], 64].
    Counter.resetAll()

    val input = MapOrderedStream(OrderedStreamToVector.asFunction(), CounterInteger(0, 1, Seq(64, 16), None))
    val tree = PadAndSlideOrderedStreamOfVectors.expandWindowFromCenter(input, 1, 1)

    HDLProject("slidepad", tree).compileHDL
  }
}
