package backend.hdl.arch

import backend.hdl.arch.mem.BufferStreamInBlockRam
import backend.hdl.{HDLProject, IntType, OrderedStreamType}
import core.{Counter, FunctionCall, Let, Marker, ParamDef, ParamUse, TextType, TypeChecker}
import core.compile.CompilerPhase
import org.junit.Test

class ArchSharedFuncTest {

  /** Test arch high-order function. */
  @Test
  def testAddConstFuncMap(): Unit = {
    Counter.resetAll()
    // Define a function
    val param1 = ParamDef(OrderedStreamType(IntType(8), 10))
    val param2 = ParamDef(IntType(8))

    val func = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, MapOrderedStream({
      val param = ParamDef()
      ArchLambda(param, AddInt2(ParamUse(param), ParamUse(param2)))
    }, ParamUse(param1)))))
    val funcParam = ParamDef(func.t)

    val input1 = CounterInteger(0, 1, Seq(10, 4), Some(IntType(8)))
    val input2 = Registered(Registered(Registered(ConstantValue(10, Some(IntType(8))))))
    val input3 = CounterInteger(41, 1, Seq(10, 4), Some(IntType(8)))
    val input4 = ConstantValue(20, Some(IntType(8)))

    // Use Let and call the function
    val expr = TypeChecker.check(Let(
      funcParam,
      Tuple(
        BufferStreamInBlockRam(JoinOrderedStream(MapOrderedStream(
          {
            val p = ParamDef(OrderedStreamType(IntType(8), 10))
            ArchLambda(p, FunctionCall(FunctionCall(Marker(ParamUse(funcParam), TextType("1")), ParamUse(p)), input2))
          }, input1
        ))),
        BufferStreamInBlockRam(JoinOrderedStream(MapOrderedStream(
          {
            val p = ParamDef(OrderedStreamType(IntType(8), 10))
            ArchLambda(p, FunctionCall(FunctionCall(Marker(ParamUse(funcParam), TextType("0")), ParamUse(p)), input4))
          }, input3
        )))
      ),
      func
    ))

    HDLProject("testAddConstFuncMap", expr, CompilerPhase.first(), Seq()).simulateStandalone()
  }

  /** Test double buffering inside a function. */
  @Test
  def testAddConstDoubleBufferingFunc(): Unit = {
    Counter.resetAll()
    // Define a function
    val param1 = ParamDef(OrderedStreamType(IntType(8), 10))
    val param2 = ParamDef(IntType(8))
    val paramB1 = ParamDef(OrderedStreamType(IntType(8), 10))
    val paramB2 = ParamDef(OrderedStreamType(IntType(8), 10))
    val bufFunB1 = ArchLambda(paramB1, BufferStreamInBlockRam(ParamUse(paramB1)))
    val bufFunB2 = ArchLambda(paramB2, BufferStreamInBlockRam(ParamUse(paramB2)))

    val func = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, MapOrderedStream({
      val param = ParamDef()
      ArchLambda(param, AddInt2(ParamUse(param), ParamUse(param2)))
    }, Alternate(bufFunB1, bufFunB2, ParamUse(param1))))))
    val funcParam = ParamDef(func.t)

    val input1 = CounterInteger(0, 1, Seq(10, 4), Some(IntType(8)))
    val input2 = ConstantValue(1, Some(IntType(8)))
    val input3 = CounterInteger(0, 1, Seq(10, 4), Some(IntType(8)))
    val input4 = ConstantValue(1, Some(IntType(8)))

    // Use Let and call the function
    val expr = TypeChecker.check(Let(
      funcParam,
      Tuple(
        BufferStreamInBlockRam(JoinOrderedStream(MapOrderedStream(
          {
            val p = ParamDef(OrderedStreamType(IntType(8), 10))
            ArchLambda(p, FunctionCall(FunctionCall(ParamUse(funcParam), ParamUse(p)), input2))
          }, input1
        ))),
        BufferStreamInBlockRam(JoinOrderedStream(MapOrderedStream(
          {
            val p = ParamDef(OrderedStreamType(IntType(8), 10))
            ArchLambda(p, FunctionCall(FunctionCall(ParamUse(funcParam), ParamUse(p)), input4))
          }, input3
        )))
      ),
      func
    ))

    HDLProject("testAddConstDoubleBufferingFunc", expr, CompilerPhase.first(), Seq()).simulateStandalone()
  }

  @Test
  def testFuncConstInputs(): Unit = {
    Counter.resetAll()
    // Define a function
    val param1 = ParamDef(IntType(8))
    val param2 = ParamDef(IntType(8))
    val func = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, AddInt2(ParamUse(param1), ParamUse(param2)))))
    val funcParam = ParamDef(func.t)

    val input1 = ConstantValue(1, Some(IntType(8)))
    val input2 = ConstantValue(2, Some(IntType(8)))
    val input3 = ConstantValue(3, Some(IntType(8)))
    val input4 = ConstantValue(4, Some(IntType(8)))

    // Use Let and call the function
    val expr = TypeChecker.check(Let(
      funcParam,
      Tuple(
        Registered(FunctionCall(FunctionCall(ParamUse(funcParam), input1), input2)),
        Registered(FunctionCall(FunctionCall(ParamUse(funcParam), input3), input4))
      ),
      func
    ))

    HDLProject("testFuncConstInputs", expr, CompilerPhase.first(), Seq()).simulateStandalone()
  }

  @Test
  def testFuncWithRegisterAndConstInputs(): Unit = {
    Counter.resetAll()
    // Define a function
    val param1 = ParamDef(IntType(8))
    val param2 = ParamDef(IntType(8))
    val func = TypeChecker.check(ArchLambda(param1, ArchLambda(param2, Registered(AddInt2(ParamUse(param1), ParamUse(param2))))))
    val funcParam = ParamDef(func.t)

    val input1 = ConstantValue(1, Some(IntType(8)))
    val input2 = ConstantValue(2, Some(IntType(8)))
    val input3 = ConstantValue(3, Some(IntType(8)))
    val input4 = ConstantValue(4, Some(IntType(8)))

    // Use Let and call the function
    val expr = TypeChecker.check(Let(
      funcParam,
      Tuple(
        Registered(FunctionCall(FunctionCall(ParamUse(funcParam), input1), input2)),
        Registered(FunctionCall(FunctionCall(ParamUse(funcParam), input3), input4))
      ),
      func
    ))

    HDLProject("testFuncWithRegisterAndConstInputs", expr, CompilerPhase.first(), Seq()).simulateStandalone()
  }
}
