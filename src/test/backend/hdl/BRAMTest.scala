package backend.hdl

import algo.{Buffer, Id, Input}
import algo.util.Matrix
import backend.hdl.arch.mem.{MemFunctionsCompiler, MemoryImage}
import backend.hdl.arch.rewrite.{CleanupRules, InputBufferingRules}
import core.Counter
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted}
import org.junit.Test

class BRAMTest {
  def memcopy(cachelines: Int, width: Int = 512, buffer: Boolean = true, run: Boolean = true): Unit = {
    val data = Matrix.fromCounter(1, cachelines, 1, Some("memcopy")).data
    Counter.resetAll()
    val input = Input("data", cachelines, 1, algo.IntType(width))
    val alg = if(buffer) Buffer(input) else Id(input)
    val proj = HDLProject("memcopy" + cachelines + "x" + width + "_" + buffer, alg, CompilerPhase.first(), Seq(
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(64)))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(InputBufferingRules.doubleBufferRead)))
    ))
    if (run)
      proj.writeAllFiles(
        Predef.Map(
          "data" -> data,
          MemoryImage.RESULT_VAR_NAME -> data
        )
      )
    else
      proj.writeHDLFiles()
  }

  @Test
  def testMemCopy1(): Unit = memcopy(64, 64, true)// 64, 128, 192, 256, 320, 364, 448, 512
}
