package backend.hdl

import algo.{AlgoLambda, Tuple2}
import core.{BuiltinDataTypeVar, FunctionCall, Let, ParamDef, ParamUse, TypeChecker, TypeFunctionCall, TypeLambda}
import core.util.IRDotGraph
import org.junit.Test

class LetTypeLambdaTests {

  @Test
  def testLambda(): Unit = {
    val lParam1 = ParamDef(algo.IntTypeVar())
    val lBody = algo.Id(ParamUse(lParam1))
    val plusOneFun = TypeChecker.check(AlgoLambda(lParam1, lBody))
    //IRDotGraph(plusOneFun).show()
  }

  @Test
  def testTypeLambdaWithLet(): Unit = {
    val typeFunc = {
      val dtv = BuiltinDataTypeVar()
      TypeLambda(
        dtv,
        {
          val p = ParamDef(dtv)
          AlgoLambda(p, ParamUse(p))
        }
      )
    }

    val typeFuncParam = ParamDef(typeFunc.t)
    val input1 = algo.CounterInteger(0, 1, 10)
    val input2 = algo.CounterInteger(0, 1, 11)

    val tree = TypeChecker.check(Let(
      typeFuncParam,
      algo.Id(algo.Tuple2(FunctionCall(TypeFunctionCall(ParamUse(typeFuncParam), input1.t), input1),
        FunctionCall(TypeFunctionCall(ParamUse(typeFuncParam), input2.t), input2))),
      typeFunc
    ))

    // Note that there will be no unknownType in the output type.
    assert(!tree.t.hasUnknownType)
  }

  @Test
  def testTypeLambdaId(): Unit = {
    val typeFunc = {
      val dtv1 = algo.IntTypeVar()
      TypeLambda(
        Seq(dtv1),
        {
          val p1 = ParamDef(dtv1)
          AlgoLambda(Seq(p1), algo.Id(ParamUse(p1)))
        }
      )
    }
    val typeFuncParam = ParamDef(typeFunc.t)
    val tree = TypeChecker.check(Let(
      typeFuncParam,
      {
        algo.Id(FunctionCall(TypeFunctionCall(ParamUse(typeFuncParam), algo.IntType(8)), algo.ConstantInteger(10, Some(algo.IntType(8)))))
      },
      typeFunc
    ))


    assert(!tree.t.hasUnknownType)
  }

  @Test
  def testTypeLambdaAdd(): Unit = {
    val typeFunc = {
      val dtv1 = algo.IntTypeVar()
      val dtv2 = algo.IntTypeVar()
      TypeLambda(
        Seq(dtv1, dtv2).reverse,
        {
          val p1 = ParamDef(dtv1)
          val p2 = ParamDef(dtv2)
          AlgoLambda(Seq(p1), AlgoLambda(Seq(p2), algo.Add2(ParamUse(p1), ParamUse(p2))))
        }
      )
    }
    val typeFunctc = TypeChecker.check(typeFunc)
    val functc = TypeChecker.check(TypeFunctionCall(TypeFunctionCall(typeFunctc, algo.IntType(8)), algo.IntType(8)))

    assert(!functc.t.hasUnknownType)
  }

  @Test
  def testLetTypeLambdaAdd(): Unit = {
    val typeFunc = {
      val dtv1 = algo.IntTypeVar()
      val dtv2 = algo.IntTypeVar()
      TypeLambda(
        Seq(dtv1, dtv2).reverse,
        {
          val p1 = ParamDef(dtv1)
          val p2 = ParamDef(dtv2)
          AlgoLambda(Seq(p1), AlgoLambda(Seq(p2), algo.Add2(ParamUse(p1), ParamUse(p2))))
        }
      )
    }
    val typeFuncParam = ParamDef(typeFunc.t)
    val input1 = algo.ConstantInteger(1, Some(algo.IntType(8)))
    val input2 = algo.ConstantInteger(2, Some(algo.IntType(8)))
    val tree = TypeChecker.check(Let(
      typeFuncParam,
      algo.Id(FunctionCall(FunctionCall(TypeFunctionCall(TypeFunctionCall(ParamUse(typeFuncParam), algo.IntType(8)), algo.IntType(8)), input1), input2)),
      typeFunc
    ))

    assert(!tree.t.hasUnknownType)
  }

  @Test
  def testTypeLambdaAddDifferentBitWidth(): Unit = {
    val typeFunc = {
      val dtv1 = algo.IntTypeVar()
      val dtv2 = algo.IntTypeVar()
      TypeLambda(
        Seq(dtv1, dtv2).reverse,
        {
          val p1 = ParamDef(dtv1)
          val p2 = ParamDef(dtv2)
          AlgoLambda(Seq(p1, p2).reverse, algo.Add2(ParamUse(p1), ParamUse(p2)))
        }
      )
    }
    val typeFuncParam = ParamDef(typeFunc.t)
    val input1 = algo.ConstantInteger(10, Some(algo.IntType(8)))
    val input2 = algo.ConstantInteger(10, Some(algo.IntType(10)))

    val input3 = algo.ConstantInteger(10, Some(algo.IntType(12)))
    val input4 = algo.ConstantInteger(10, Some(algo.IntType(14)))

    val tree = TypeChecker.check(Let(
      typeFuncParam,
      Tuple2(
        algo.Id(FunctionCall(FunctionCall(TypeFunctionCall(TypeFunctionCall(ParamUse(typeFuncParam), input1.t), input2.t), input1), input2)),
        algo.Id(FunctionCall(FunctionCall(TypeFunctionCall(TypeFunctionCall(ParamUse(typeFuncParam), input3.t), input4.t), input3), input4))
      ),
      typeFunc
    ))

    assert(!tree.t.hasUnknownType)
  }

  @Test
  def testLetTypeLambdaAdd2Calls(): Unit = {
    val typeFunc = {
      val dtv1 = algo.IntTypeVar()
      val dtv2 = algo.IntTypeVar()
      TypeLambda(
        Seq(dtv1, dtv2).reverse,
        {
          val p1 = ParamDef(dtv1)
          val p2 = ParamDef(dtv2)
          AlgoLambda(Seq(p1), AlgoLambda(Seq(p2), algo.Add2(ParamUse(p1), ParamUse(p2))))
        }
      )
    }
    val typeFuncParam = ParamDef(typeFunc.t)
    val input1 = algo.ConstantInteger(1, Some(algo.IntType(8)))
    val input2 = algo.ConstantInteger(2, Some(algo.IntType(8)))
    val input3 = algo.ConstantInteger(3, Some(algo.IntType(9)))
    val input4 = algo.ConstantInteger(4, Some(algo.IntType(9)))
    val tree = TypeChecker.check(Let(
      typeFuncParam,
      algo.Tuple(
        algo.Id(FunctionCall(FunctionCall(TypeFunctionCall(TypeFunctionCall(ParamUse(typeFuncParam), algo.IntType(8)), algo.IntType(8)), input1), input2)),
        algo.Id(FunctionCall(FunctionCall(TypeFunctionCall(TypeFunctionCall(ParamUse(typeFuncParam), algo.IntType(9)), algo.IntType(9)), input3), input4))
      ),
      typeFunc
    ))

    assert(!tree.t.hasUnknownType)
  }
}
