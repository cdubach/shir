package backend.hdl

import algo.util.Matrix
import algo.{Id, Input}
import backend.hdl.arch.ArchCompiler
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.{MemFunctionsCompiler, MemoryImage}
import backend.hdl.arch.rewrite.{CleanupRules, FixTimingRules, InputBufferingRules, MoveDownSplitStreamRules, MoveUpDropStreamRules, MoveUpJoinStreamRules, MoveUpVectorToStreamRules, RemoveStreamVectorConversionRules}
import backend.hdl.arch.tiling.TranspositionCompiler
import core._
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted}
import org.junit.Test

class MemCopyTest {

  def memcopy(cachelines: Int, run: Boolean = true): Unit = {
    val data = Matrix.fromCounter(1, cachelines, 1, Some("memcopy")).data
    Counter.resetAll()
    val alg = Id(Input("data", cachelines, 1, algo.IntType(512)))
    val proj = HDLProject("memcopy", alg, CompilerPhase.first(), Seq(
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.limitParallelReadRequests(64)))),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteTargeted(0), Seq(InputBufferingRules.doubleBufferRead)))
    ))
    if (run)
      proj.simulateWithHostRam(
      Predef.Map(
        "data" -> data,
        MemoryImage.RESULT_VAR_NAME -> data
      )
    ).assertCorrect.print()
    else
      proj.writeHDLFiles()
  }

  @Test
  def testMemCopy1(): Unit = memcopy(1024) // fast sim: 1105 cycles single buffer, 1046 cycles double buffering

  @Test
  def testMemCopy2(): Unit = memcopy(16384) // fast sim: 17665 cycles single buffer, 16646 cycles double buffering

  @Test
  def testMemCopy3(): Unit = memcopy(131072)

  @Test
  def testMemCopy4(): Unit = memcopy(1048576)

  // 512MB = 8 * 1024 * 1024 * 64 bytes
  @Test
  def testMemCopy5(): Unit = memcopy(8388608, run = false) // too large to run in simulation

  @Test
  def testMemCopyWith2DDrops(): Unit = {
    Counter.resetAll()
    val height = 16
    val width = 16
    val newHeight = 8
    val newWidth = 8
    val channel = 64

    // DataGen
    val data = Matrix.fromRandom(height * width, channel, 9, Some("data")).data
    val result = data.zipWithIndex.filter(e => (e._2 % width < newWidth) && (e._2 / width < newHeight)).map(_._1)

    // Expression
    val input = Input("data", channel, height * width, algo.IntType(8))
    val splitInput = TypeChecker.check(algo.Split(input, width))
    val dropInput = TypeChecker.check(algo.Map(algo.Drop.asFunction(Seq(None), Seq(0, width - newWidth)),
      algo.Drop(splitInput, 0, height - newHeight)))
    val output = algo.Join(dropInput)
    HDLProject("testMemCopyWith2DDrops", output, CompilerPhase.first(), Seq(
      (TranspositionCompiler.phaseBefore, RewriteStep(RewriteAll(), MoveUpJoinStreamRules.get())),
      (TranspositionCompiler.phaseBefore, RewriteStep(RewriteAll(), MoveUpDropStreamRules.mergeIntoWriteAddress)),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), CleanupRules.get())),
    )).simulateWithHostRam(
      Predef.Map(
        "data" -> data,
        MemoryImage.RESULT_VAR_NAME -> result
      )
    ).print()
  }
}
