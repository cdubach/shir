package backend.hdl.conv

import algo.util.Matrix
import backend.hdl.HDLProject
import backend.hdl.arch.ArchCompiler
import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.mem.{MemFunctionsCompiler, MemoryImage}
import backend.hdl.arch.rewrite.conv.ParallelizeConvolutionRules
import backend.hdl.arch.rewrite.{FixTimingRules, InputBufferingRules, IntermediateBufferingRules, ParallelizeDotProductRules}
import backend.hdl.arch.rewrite.tiling.{ABTilingRules, TilingPreprocessingRules}
import backend.hdl.arch.tiling.{ParallelizationCompiler, TranspositionCompiler}
import core.Counter
import core.compile.CompilerPhase
import core.rewrite.{RewriteAll, RewriteStep, RewriteTargeted}
import org.junit.Test

class ConvTilingTest {

  def convMatrices(imageRows: Int, imageCols: Int, weightSize: Int, inChannel: Int, outChannel: Int): Map[String, Seq[Seq[Int]]] = {
    val imageOrig = Matrix.fromRandom(imageRows, imageCols * inChannel, 9, Some("A"))
    val weightOrig = Matrix.fromRandom(outChannel, weightSize * weightSize * inChannel, 9, Some("B"))
    val imagePad = imageOrig.pad(1, 1 ,inChannel, inChannel)
    val output = imagePad.conv(weightOrig, inChannel, false).mod(256)
    // Padding for cacheline alignment
    val cacheLineSize = 64
    val imagePadCache = imagePad.pad(0, 0 , cacheLineSize / 2 - inChannel % (cacheLineSize / 2), cacheLineSize / 2 - inChannel % (cacheLineSize / 2))
    val weightPadCache = weightOrig.pad(0, 0, 0, cacheLineSize - (weightSize * weightSize * inChannel) % cacheLineSize)
    Predef.Map(
      "image" -> imagePadCache.data,
      "weight" -> weightPadCache.data,
      MemoryImage.RESULT_VAR_NAME -> output.data
    )
  }

  @Test
  def testConvTiling(): Unit = {
    Counter.resetAll()
    // dimension inner -> outer
    // image dim: [ich, iw, ih]
    // weight dim: [ich, ih, iw, och]
    // Basic setup
    val inputWidth = 128 // 1024
    val inputHeight = 128 // 1024
    val inputChannel = 3
    val weightSize = 3
    val outputChannel = 4 // 64
    val elementBits = 8
    val inputPadding = 1

    val tiledHeightNoPad = 64 // 128
    val tiledWidthNoPad = 64 // 128

    val limitMulPerWindow = 27
    val limitMul = limitMulPerWindow * 32

    val conv = ConvTemplate.convHalfCachePaddingChannelMajor(inputWidth, inputHeight, inputChannel,
      weightSize, outputChannel, inputPadding, elementBits, elementBits)

    HDLProject("conv_tiling", conv, CompilerPhase.first(), Seq(
      // Tiling
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(TilingPreprocessingRules.moveDownInnerMapBody))),
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.tilingInnerMap(1, tiledHeightNoPad)))),
      (ArchCompiler.phaseBefore, RewriteStep(RewriteTargeted(0), Seq(ABTilingRules.tilingInnerMap(1, tiledWidthNoPad)))),

      // Parallelize
      (TranspositionCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputRow("image", 1)))),
      (TranspositionCompiler.phaseAfter, RewriteStep(RewriteAll(), Seq(InputBufferingRules.bufferInputMatrix("weight", 1)))),
      (TranspositionCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeDotProductRules.get(Some(limitMulPerWindow)))),
      (TranspositionCompiler.phaseAfter, RewriteStep(RewriteAll(), ParallelizeConvolutionRules.get(Some(limitMul)))),

      // Memory and timing fix
      (ParallelizationCompiler.phaseAfter, RewriteStep(RewriteAll(), IntermediateBufferingRules.get())),
      (MemFunctionsCompiler.phaseAfter, RewriteStep(RewriteAll(), InputBufferingRules.readDoubleBuffering)),
      (DeviceSpecificCompiler.phaseAfter, RewriteStep(RewriteAll(), FixTimingRules.get())),
    )).simulateWithHostRam(convMatrices(inputHeight, inputWidth, weightSize, inputChannel, outputChannel)).print()
  }
}
