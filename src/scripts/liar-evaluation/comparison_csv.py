#!/usr/bin/env python3

import io
from optparse import OptionParser
from optimization_log import OptimizationLog, write_comparison_csv

def main():
    parser = OptionParser()
    (options, args) = parser.parse_args()

    logs = [
        OptimizationLog.parse_file(file)
        for file in args
    ]

    file = io.StringIO()
    write_comparison_csv(logs, file)
    print(file.getvalue())


if __name__ == '__main__':
    main()
