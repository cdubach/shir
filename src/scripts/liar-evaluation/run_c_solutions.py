#!/usr/bin/env python3

"""Runs the C solutions for each step of an optimization log. Each solution's run time is recorded
   and written to an updated log file."""

from subprocess import CalledProcessError, TimeoutExpired, check_call, check_output
from os import PathLike
from pathlib import Path
from collections import OrderedDict
from tempfile import TemporaryDirectory
from optparse import OptionParser
from optimization_log import OptimizationLog, OptimizationPreamble, OptimizationStep, RunStatus, c_compiler_version, create_c_coverage_project, parse_properties, run_solutions

DEFAULT_TIMEOUT_IN_SECS = 60


def compile_project(directory: PathLike):
    check_call(['make', '-s'], cwd=directory)


def run_c_solutions(log: OptimizationLog, time_budget: float = DEFAULT_TIMEOUT_IN_SECS):
    def compile(step: OptimizationStep, preamble: OptimizationPreamble, tmpdir: str) -> str:
        # Create a coverage project.
        create_c_coverage_project(tmpdir, preamble, step)

        # Run the coverage project.
        compile_project(tmpdir)

        return tmpdir

    def run(step: OptimizationStep, tmpdir: str, timeout: float) -> None:
        step_output = check_output([tmpdir / 'main'], timeout=timeout).decode('utf-8').split('\n')
        run_times = parse_properties(step_output)
        step.c_compiler = c_compiler_version()
        step.run_time = float(run_times['total']) / 1e9
        step.extern_call_run_times = OrderedDict(
            (k, float(v) / 1e9)
            for k, v in run_times.items()
            if k != 'total')

    run_solutions(log, compile, run, time_budget)


def main():
    parser = OptionParser()
    parser.add_option("-t", "--time-budget", dest="timeout", default=DEFAULT_TIMEOUT_IN_SECS,
                      help="specify the time budget in seconds each solution gets", metavar="TIME_BUDGET", type='float')
    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error(f'expected exactly two arguments, got {len(args)}')

    log = OptimizationLog.parse_file(args[0])
    run_c_solutions(log, timeout=options.timeout)
    log.write_to(args[1])


if __name__ == '__main__':
    main()
