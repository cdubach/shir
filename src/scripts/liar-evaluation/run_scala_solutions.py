#!/usr/bin/env python3

"""Runs the Scala solutions for each step of an optimization log. Each solution's run time is recorded
   and written to an updated log file."""

from collections import OrderedDict
from tempfile import TemporaryDirectory
from optparse import OptionParser
from optimization_log import OptimizationLog, create_scala_coverage_project, run_with_sbt, parse_properties

def main():
    parser = OptionParser()
    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error(f'expected exactly two arguments, got {len(args)}')

    log = OptimizationLog.parse_file(args[0])
    for step in log.steps:
        if step.run_time is not None and step.extern_call_run_times is not None:
            continue

        with TemporaryDirectory() as tmpdir:
            # Create a coverage project.
            main_obj = create_scala_coverage_project(tmpdir, log.preamble, step)

            # Run the coverage project.
            step_output = run_with_sbt(main_obj, [], cwd=tmpdir)
            run_times = parse_properties(step_output)
            step.run_time = float(run_times['total'])
            step.extern_call_run_times = OrderedDict(
                (k, float(v))
                for k, v in run_times.items()
                if k != 'total')

    log.write_to(args[1])


if __name__ == '__main__':
    main()
