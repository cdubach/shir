#!/usr/bin/env python3

import os
import subprocess
import time
from typing import Dict, List, Optional
from pathlib import Path
from optparse import OptionParser

from optimization_log import PROJECT_ROOT, SbtStackOverflowException, Target, Strategy, parallel_tasks, run_with_sbt


class KernelDescription:
    def __init__(self, name: str, max_steps_per_target: Dict[Target, int]) -> None:
        self.name = name
        self.max_steps_per_target = max_steps_per_target

    def __repr__(self) -> str:
        return f'KernelDescription({repr(self.name)}, {repr(self.max_steps_per_target)})'


KERNEL_DESCRIPTIONS = [
    KernelDescription('custom.vsum', {Target.BLAS: 10, Target.NONE: 11, Target.PYTORCH: 10}), 
    KernelDescription('custom.axpy', {Target.BLAS: 11, Target.NONE: 11, Target.PYTORCH: 10}), 
    KernelDescription('custom.gemv', {Target.BLAS: 7, Target.NONE: 11, Target.PYTORCH: 7}), 
    KernelDescription('polybench.gesummv', {Target.BLAS: 7, Target.NONE: 11, Target.PYTORCH: 7}), 
    KernelDescription('polybench.gemver', {Target.BLAS: 5, Target.NONE: 7, Target.PYTORCH: 5}), 
    KernelDescription('polybench.gemm', {Target.BLAS: 7, Target.NONE: 10, Target.PYTORCH: 6}), 
    KernelDescription('custom.1mm', {Target.BLAS: 8, Target.NONE: 11, Target.PYTORCH: 7}), 
    KernelDescription('polybench.doitgen', {Target.BLAS: 8, Target.NONE: 11, Target.PYTORCH: 7}), 
    KernelDescription('polybench.mvt', {Target.BLAS: 7, Target.NONE: 11, Target.PYTORCH: 7}), 
    KernelDescription('custom.memset', {Target.BLAS: 11, Target.NONE: 11, Target.PYTORCH: 11}), 
    KernelDescription('polybench.atax', {Target.BLAS: 7, Target.NONE: 11, Target.PYTORCH: 7}), 
    KernelDescription('custom.blur1d', {Target.BLAS: 6, Target.NONE: 7, Target.PYTORCH: 5}), 
    KernelDescription('polybench.jacobi1d', {Target.BLAS: 5, Target.NONE: 6, Target.PYTORCH: 5}), 
    KernelDescription('custom.stencil2d', {Target.BLAS: 5, Target.NONE: 5, Target.PYTORCH: 5}), 
    KernelDescription('custom.slim-2mm', {Target.BLAS: 7, Target.NONE: 11, Target.PYTORCH: 6}), 
    KernelDescription('polybench.2mm', {Target.BLAS: 6, Target.NONE: 8, Target.PYTORCH: 5}), 
]

# Establish a limit on the number of steps for vsum when detecting idioms. At step 11,
# LIAR realizes that the dot product solution can be lifted to a `dgemv`/`torch.mv`.
# Since the cost model prefers higher-level BLAS calls, the matrix-vector product
# will be selected as the solution soon as it is available, i.e., at step 11.
# This result isn't problematic per se, but there appears to be a C codegen bug that
# makes the C code for the step 11 solution segfault. We hence exclude it using a
# hard limit on the number of saturation steps.
HARD_LIMITS = {
    'custom.vsum': KernelDescription('custom.vsum', {Target.BLAS: 10, Target.PYTORCH: 10})
}


KERNEL_NAMES = [desc.name for desc in KERNEL_DESCRIPTIONS]


DEFAULT_TIMEOUT_IN_SECS = 60


def optimize_kernel(
        name: str,
        timeout: float = DEFAULT_TIMEOUT_IN_SECS,
        target: Target = Target.BLAS,
        max_steps: Optional[int] = None) -> List[str]:
    """Optimizes a kernel, up to a certain timeout."""
    return run_with_sbt(
        "drivers.OptimizeKernel",
        [name, str(int(timeout * 1000)), str((max_steps or 11) - 1), f'--idioms={target.value}', f'--strategy={target.strategy.value}'],
        cwd=PROJECT_ROOT,
        timeout=timeout + 60)


def filter_output(lines: List[str]) -> List[str]:
    """Filters a kernel's output, eliding sbt cruft and type checker warnings."""
    # Take only those lines that start with [info], and remove that prefix.
    results = []
    found_header = False
    for item in lines:
        if item.startswith('running kernel'):
            found_header = True

        if found_header and 'Assignments: unsolved type vars left' not in item:
            results.append(item)

    return results


def optimize_all(
        log_directory: Path,
        overwrite: bool = False,
        timeout: float = DEFAULT_TIMEOUT_IN_SECS,
        target: Target = Target.BLAS,
        number_of_threads: int = 1,
        kernels: List[str] = KERNEL_NAMES,
        steps_per_kernel: Optional[Dict[str, int]] = None):

    error_log_path = log_directory / (f'error-{int(time.monotonic())}.txt')
    def optimize_one(kernel: str):
        log_path = log_directory / (f'log-{kernel.split(".")[-1]}.txt')
        if log_path.exists() and log_path.read_text():
            if overwrite:
                write = True
                print(f're-optimizing kernel {kernel} and overwriting existing results')
            else:
                write = False
                print(f'reusing stored results for kernel {kernel} (computed during previous run)')
        else:
            write = True
            print(f'optimizing kernel {kernel}')

        if write:
            try:
                max_steps = steps_per_kernel[kernel] if steps_per_kernel else None
                if not max_steps and kernel in HARD_LIMITS and target in HARD_LIMITS[kernel].max_steps_per_target:
                    max_steps = HARD_LIMITS[kernel].max_steps_per_target[target]

                output = optimize_kernel(
                    kernel,
                    timeout = timeout,
                    target = target,
                    max_steps = max_steps)
            except SbtStackOverflowException as ex:
                output = ex.processed_output
            except Exception as ex:
                error_log_path.write_text(str(ex))
                raise ex
            
            output = filter_output(output)
            log_path.write_text(''.join(line + '\n' for line in output))

    parallel_tasks(kernels, optimize_one, number_of_threads)


def main():
    parser = OptionParser()
    parser.add_option("--log-to", dest="log_directory", default='logs',
                      help="log results to LOG_DIRECTORY", metavar="LOG_DIRECTORY")
    parser.add_option("-t", "--timeout", dest="timeout", default=DEFAULT_TIMEOUT_IN_SECS,
                      help="specify the timeout in seconds each kernel gets", metavar="TIMEOUT", type='float')
    parser.add_option("--overwrite", dest="overwrite", default=False, action='store_true',
                      help="overwrites existing, nonempty log files")
    parser.add_option("--idioms", dest="target", default='blas',
                      help="selects the type of idioms to detect (blas, tensorflow or none)")

    (options, args) = parser.parse_args()

    log_directory = Path(options.log_directory)
    if not log_directory.exists():
        print(f"error: cannot send log files to nonexistent directory '{log_directory}'")
        return

    optimize_all(log_directory, options.overwrite, options.timeout, Target(options.target))


if __name__ == '__main__':
    main()
