#!/usr/bin/env python3

"""Generates kernel descriptions from logs."""

from pathlib import Path
from evaluate_all import TARGETS, directory_for, find_logs
from optimize_all import KERNEL_NAMES, KernelDescription
from optimization_log import OptimizationLog, Target


def main():
    descriptions = {
        name: KernelDescription(name, {})
        for name in KERNEL_NAMES
    }

    for target in TARGETS:
        for log_path in find_logs(directory_for(target)):
            log = OptimizationLog.parse_file(log_path)
            if log.kernel_qualified_name in descriptions:
                descriptions[log.kernel_qualified_name].max_steps_per_target[target] = len(log.steps)

    print('[')
    for name in KERNEL_NAMES:
        print(f'    {repr(descriptions[name])}, ')
    print(']')


if __name__ == '__main__':
    main()
