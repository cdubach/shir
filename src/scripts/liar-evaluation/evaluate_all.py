#!/usr/bin/env python3

"""Optimizes all benchmarks and computes their C coverage."""

from optparse import OptionParser
from pathlib import Path
import shutil
import subprocess
from typing import List, Optional
from run_reference_kernels import run_all_reference_kernels
from optimization_log import OptimizationLog, Target, Strategy, generate_speedup_bars, parallel_tasks, sort_logs, write_comparison_csv, write_run_time_csv
from run_python_solutions import run_python_solutions
from run_c_solutions import run_c_solutions
from optimize_all import KERNEL_DESCRIPTIONS, KERNEL_NAMES, optimize_all, DEFAULT_TIMEOUT_IN_SECS


TARGETS = [Target.BLAS, Target.NONE, Target.PYTORCH, Target.BLAS.with_strategy(Strategy.PER_RULE_RANDOM), Target.BLAS.with_strategy(Strategy.ISARIA), Target.BLAS.with_strategy(Strategy.ISARIA_NO_REBASE)]
RUN_TIME_TARGETS = [Target.BLAS, Target.NONE, Target.BLAS.with_strategy(Strategy.PER_RULE_RANDOM), Target.BLAS.with_strategy(Strategy.ISARIA), Target.BLAS.with_strategy(Strategy.ISARIA_NO_REBASE)] # TARGETS + [Target.NONE_PYTHON]
TIME_COMPARISON_TARGETS = RUN_TIME_TARGETS + [Target.REFERENCE]

PLOTS_DIR = Path('plots')

LOG_PREFIX = 'log-'
COV_PREFIX = 'cov-'


def directory_for(target: Target) -> Path:
    return Path(f'{target.kebab}-logs')


def find_logs(log_directory: Path, prefix: str = LOG_PREFIX) -> List[Path]:
    return [
        file_path
        for file_path in log_directory.iterdir()
        if file_path.name.startswith(prefix)
    ]


def run_solutions(
        log_directory: Path,
        overwrite: bool = False,
        timeout: float = DEFAULT_TIMEOUT_IN_SECS,
        number_of_threads: int = 1,
        kernels: List[str] = KERNEL_NAMES):

    def run_one(log_path: Path):
        cov_path = log_path.with_name(COV_PREFIX + log_path.name[len(LOG_PREFIX):])
        log = OptimizationLog.parse_file(log_path)
        overwrite_log = overwrite
        if not overwrite_log:
            if cov_path.exists():
                cov_log = OptimizationLog.parse_file(cov_path)
                if cov_log.preamble.date_time != log.preamble.date_time:
                    overwrite_log = True
            else:
                overwrite_log = True

        if overwrite_log and log.kernel_qualified_name in kernels:
            if log.preamble.target in (Target.TENSORFLOW, Target.PYTORCH, Target.NONE_PYTHON):
                run_python_solutions(log, timeout, enable_compilation=False)
            else:
                run_c_solutions(log, timeout)

            log.write_to(cov_path)

    parallel_tasks(find_logs(log_directory), run_one, number_of_threads)


def retarget_solutions(log_directory: Path, new_target: Target, new_log_directory: Path) -> None:
    for log_path in find_logs(log_directory):
        log = OptimizationLog.parse_file(log_path)
        log.preamble.target = new_target
        log.write_to(new_log_directory / log_path.name)


def plot_solutions_over_time(log_directory: Path, plot_directory: Path):
    """Plots solutions-over-time charts for each log in a log directory."""

    for log_path in find_logs(log_directory):
        log = OptimizationLog.parse_file(log_path)
        plot_path = plot_directory / f'nodes-over-time-{log.kernel_name}-{log.preamble.target}.tex'
        plot_path.write_text(log.node_count_chart)
        compilation_time_plot_path = plot_directory / f'comp-time-over-time-{log.kernel_name}-{log.preamble.target}.tex'
        compilation_time_plot_path.write_text(log.compilation_time_chart)


def plot_run_time_over_time(log_directory: Path, plot_directory: Path):
    """Plots run-time over time charts for each log in a log directory."""

    for log_path in find_logs(log_directory, COV_PREFIX):
        log = OptimizationLog.parse_file(log_path)
        plot_path = plot_directory / f'run-time-{log.kernel_name}-{log.preamble.target}.tex'
        plot_path.write_text(log.run_time_chart + '\n')


def plot_coverage_over_time(log_directory: Path, plot_directory: Path):
    """Plots coverage charts for each log in a log directory."""

    for log_path in find_logs(log_directory, COV_PREFIX):
        log = OptimizationLog.parse_file(log_path)
        plot_path = plot_directory / f'coverage-over-time-{log.kernel_name}-{log.preamble.target}.tex'
        plot_path.write_text(log.coverage_chart)


def generate_overview_csv(log_directory: Path, csv_path: Path):
    logs = sort_logs([
        OptimizationLog.parse_file(log_path)
        for log_path in find_logs(log_directory)
    ])
    with open(csv_path, 'w') as csv_file: 
        write_comparison_csv(logs, csv_file)


def generate_run_time_csv(csv_path: Path):
    logs = sort_logs([
        OptimizationLog.parse_file(log_path)
        for target in TIME_COMPARISON_TARGETS
        for log_path in find_logs(directory_for(target), COV_PREFIX)
    ])
    with open(csv_path, 'w') as csv_file: 
        write_run_time_csv(logs, csv_file)


def generate_speedup_bars_tex(output_path: Path):
    logs = sort_logs([
        OptimizationLog.parse_file(log_path)
        for target in [Target.BLAS, Target.NONE, Target.REFERENCE]
        for log_path in find_logs(directory_for(target), COV_PREFIX)
    ])
    output_path.write_text(generate_speedup_bars(logs, baseline=Target.REFERENCE) + '\n')


def try_recognize_kernel(name: str) -> Optional[str]:
    for kernel in KERNEL_NAMES:
        if kernel == name or kernel.split('.')[-1] == name:
            return kernel
        
    return None


def main():
    parser = OptionParser(usage='usage: %prog [options] [kernel...]')
    parser.add_option("-t", "--optimizer-timeout", dest="timeout", default=DEFAULT_TIMEOUT_IN_SECS,
                      help="specify the timeout in seconds the optimizer gets for each kernel", metavar="OPTIMIZER_TIMEOUT", type='float')
    parser.add_option("--executable-timeout", dest="executable_timeout", default=DEFAULT_TIMEOUT_IN_SECS,
                      help="specify the timeout in seconds each kernel gets to execute", metavar="EXECUTABLE_TIMEOUT", type='float')
    parser.add_option("--overwrite", dest="overwrite", default=False, action='store_true',
                      help="overwrites existing, nonempty log files")
    parser.add_option("--threads", dest="number_of_threads", type=int, default=1,
                      help="specifies the number of parallel threads to use for tasks")
    parser.add_option("--limit-steps", dest="limit_steps", default=False, action='store_true',
                      help="specifies that the number of steps cannot exceed hardcoded maximums")
    parser.add_option("--optimize-only", dest="run_kernels", default=True, action='store_false',
                      help="only optimizes kernels; does not run them")

    (options, args) = parser.parse_args()

    if args:
        kernels = []
        for name in args:
            kernel = try_recognize_kernel(name)
            if kernel:
                kernels.append(kernel)
            else:
                parser.error(f'unknown kernel {name}')

    else:
        kernels = KERNEL_NAMES


    # Create log directories.
    all_targets = set(TARGETS + RUN_TIME_TARGETS + TIME_COMPARISON_TARGETS)
    for target in all_targets:
        directory_for(target).mkdir(parents=True, exist_ok=True)

    PLOTS_DIR.mkdir(parents=True, exist_ok=True)

    # Optimize benchmarks for all targets.
    for target in TARGETS:
        if target.strategy == target.strategy.SIMPLE:
            max_steps = {
                desc.name: desc.max_steps_per_target[target]
                for desc in KERNEL_DESCRIPTIONS
            }
        else:
            max_steps = {
                desc.name: 200
                for desc in KERNEL_DESCRIPTIONS
            }

        optimize_all(
            directory_for(target),
            options.overwrite,
            options.timeout,
            target,
            kernels = kernels,
            steps_per_kernel = max_steps if options.limit_steps else None
            # options.number_of_threads
        )

    # Write overview CSVs.
    for target in TARGETS:
        generate_overview_csv(directory_for(target), PLOTS_DIR / f'{target.kebab}-overview.csv')

    # Plot solutions over time charts for benchmarks with idiom recognition.
    for target in TARGETS:
        if target != Target.NONE:
            plot_solutions_over_time(directory_for(target), PLOTS_DIR)

    if options.run_kernels:
        if Target.NONE_PYTHON in all_targets:
            # Split no-idiom logs into C and Python logs.
            retarget_solutions(directory_for(Target.NONE), Target.NONE_PYTHON, directory_for(Target.NONE_PYTHON))

        # Run reference kernels.
        run_all_reference_kernels(options.executable_timeout, options.number_of_threads, kernels = kernels)

        # Run solutions for all targets.
        for target in RUN_TIME_TARGETS:
            run_solutions(directory_for(target), options.overwrite, options.executable_timeout, options.number_of_threads, kernels = kernels)

        # Plot coverage for all targets with idiom recognition.
        for target in RUN_TIME_TARGETS:
            if target != Target.NONE and target != Target.NONE_PYTHON:
                plot_coverage_over_time(directory_for(target), PLOTS_DIR)

        # Build run time/coverage overview table for all targets combined.
        generate_run_time_csv(PLOTS_DIR / 'run-time-and-coverage.csv')

        # Build run time charts for all targets.
        for target in RUN_TIME_TARGETS:
            plot_run_time_over_time(directory_for(target), PLOTS_DIR)

        # Draw speedup bars
        generate_speedup_bars_tex(PLOTS_DIR / 'speedup-bars.tex')

if __name__ == '__main__':
    main()
