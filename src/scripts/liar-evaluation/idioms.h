#pragma once

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <cblas.h>
#include <time.h>

#define timed(timer, body) \
    do { \
        struct timespec start, end; \
        clock_gettime(CLOCK_MONOTONIC, &start); \
        body \
        clock_gettime(CLOCK_MONOTONIC, &end); \
        timer += (double)(end.tv_sec - start.tv_sec) * 1.0e9 + (double)(end.tv_nsec - start.tv_nsec); \
    } while (0)

extern double ns_for_memset;
extern double ns_for_axpy;
extern double ns_for_dot;
extern double ns_for_gemv;
extern double ns_for_gemm;
extern double ns_for_transpose;

void idiom_memset(int ch, size_t count, void *dest);
void idiom_axpy(int n, double alpha, double *dx, int incx, double *dy, int incy, double *result);
void idiom_ddot(int n, double *dx, int incx, double *dy, int incy, double *result);
void idiom_dgemv(int aTransposed, int m, int n, double alpha, double *a, double *b, double beta, double *c, double *result);
void idiom_dgemm(int aTransposed, int bTransposed, int m, int n, int k, double alpha, double *a, double *b, double beta, double *c, double *result);
void idiom_transpose(int m, int n, double *matrix, double *result);

double drand_range(double min, double max);
double drand();
