#!/usr/bin/env python3

"""Runs the C solutions for each step of an optimization log. Each solution's run time is recorded
   and written to an updated log file."""

from subprocess import check_call
from os import PathLike
from optparse import OptionParser
from optimization_log import OptimizationLog, create_c_coverage_project


def compile_project(directory: PathLike):
    check_call('make', cwd=directory)


def main():
    parser = OptionParser()
    parser.add_option("-s", "--solution", dest="solution", default=-1,
                      help="specify the index of the solution for which a C project is generated", metavar="STEP", type='int')
    parser.add_option("--no-compile", dest="compile", default=True, action='store_false',
                      help="specify whether the project is compiled right away")
    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error(f'expected exactly two arguments, got {len(args)}')

    log = OptimizationLog.parse_file(args[0])
    create_c_coverage_project(args[1], log.preamble, log.steps[options.solution])
    if options.compile:
        compile_project(args[1])


if __name__ == '__main__':
    main()
