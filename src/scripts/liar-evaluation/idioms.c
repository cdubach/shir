#include <stdlib.h>
#include <string.h>
#include <cblas.h>

#include "idioms.h"

double ns_for_memset = 0.0;
double ns_for_axpy = 0.0;
double ns_for_dot = 0.0;
double ns_for_gemv = 0.0;
double ns_for_gemm = 0.0;
double ns_for_transpose = 0.0;

void idiom_memset(int ch, size_t count, void *dest) {
    timed(ns_for_memset, { memset(dest, ch, count); });
}

void idiom_axpy(int n, double alpha, double *dx, int incx, double *dy, int incy, double *result) {
    timed(ns_for_axpy, {
        memcpy(result, dx, n);
        cblas_daxpy(n, alpha, dx, incx, result, incy);
    });
}

void idiom_ddot(int n, double *dx, int incx, double *dy, int incy, double *result) {
    timed(ns_for_dot, {
        *result = cblas_ddot(n, dx, incx, dy, incy);
    });
}

void idiom_dgemv(int aTransposed, int m, int n, double alpha, double *a, double *b, double beta, double *c, double *result) {
    timed(ns_for_gemv, {
        memcpy(result, c, n);
        cblas_dgemv(CblasColMajor, aTransposed ? CblasTrans : CblasNoTrans, m, n, alpha, a, m, b, 1, beta, result, 1);
    });
}

void idiom_dgemm(int aTransposed, int bTransposed, int m, int n, int k, double alpha, double *a, double *b, double beta, double *c, double *result) {
    timed(ns_for_gemm, {
        memcpy(result, c, n * m);
        cblas_dgemm(CblasColMajor, aTransposed ? CblasTrans : CblasNoTrans, bTransposed ? CblasTrans : CblasNoTrans, m, n, k, alpha, a, m, b, k, beta, result, m);
    });
}

void idiom_transpose(int m, int n, double *matrix, double *result) {
    timed(ns_for_transpose, {
        cblas_domatcopy(CblasColMajor, CblasTrans, m, n, 1.0, matrix, m, result, n);
    });
}

double drand_range(double min, double max) {
    double range = (max - min);
    double div = RAND_MAX / range;
    return min + (rand() / div);
}

double drand() {
    return drand_range(0.0, 1.0);
}
