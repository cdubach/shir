#!/usr/bin/env python3

import time
from functools import wraps

TIMING_RESULTS = {}


def time_function(func):
    """A decorator that times a function's execution."""

    # Logic adapted from *Python decorator to measure execution time* by Suresh Kumar.
    # https://dev.to/kcdchennai/python-decorator-to-measure-execution-time-54hk

    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        func_name = func.__name__
        TIMING_RESULTS[func_name] = TIMING_RESULTS.get(func_name, 0) + total_time
        # print(f'Function {func.__name__}{args} {kwargs} Took {total_time:.4f} seconds')
        return result

    return wrapper


def print_timing_stats():
    """Prints timing statistics for each instrumented function."""

    for key, value in TIMING_RESULTS.items():
        print(f'{key}: {value}')
