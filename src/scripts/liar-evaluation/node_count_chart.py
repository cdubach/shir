#!/usr/bin/env python3

from optparse import OptionParser
from optimization_log import OptimizationLog

def main():
    parser = OptionParser()
    (options, args) = parser.parse_args()

    if len(args) != 1:
        parser.error(f'expected exactly one argument, got {len(args)}')

    print(OptimizationLog.parse_file(args[0]).node_count_chart)


if __name__ == '__main__':
    main()
