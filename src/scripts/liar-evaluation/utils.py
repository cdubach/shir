#!/usr/bin/env python3

def ifold(length, init, func):
    accumulator = init
    for i in range(length):
        accumulator = func(i)(accumulator)

    return accumulator


def print_pytorch_profiler_results(profiler):
    for entry in profiler.key_averages():
        key = entry.key
        aten_prefix = 'aten::'
        if key.startswith(aten_prefix):
            key = key[len(aten_prefix):]

        key = key.replace(':', '-')

        print(f'{key}: {entry.self_cpu_time_total / 1e6}')
