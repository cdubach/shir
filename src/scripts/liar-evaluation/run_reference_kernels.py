#!/usr/bin/env python3

"""A script that runs the reference kernels and stores the results as optimization logs."""


from collections import OrderedDict
from optparse import OptionParser
from pathlib import Path
from subprocess import check_output
from typing import Iterable, Tuple
from optimize_all import KERNEL_NAMES

from optimization_log import OptimizationLog, OptimizationPreamble, OptimizationStep, Target, c_compiler_version, parallel_tasks, run_solutions


REFERENCE_KERNELS_DIR = Path(__file__).parent / 'reference-kernels'
REFERENCE_LOGS_DIR = Path('reference-logs')
DEFAULT_TIMEOUT_IN_SECS = 60


def collect_reference_kernels() -> Iterable[Tuple[str, str, Path]]:
    """Collects a list of all reference kernels."""
    for directory in REFERENCE_KERNELS_DIR.iterdir():
        if directory.is_dir():
            for kernel_dir in directory.iterdir():
                if kernel_dir.is_dir():
                    yield (directory.name, kernel_dir.name, kernel_dir)


def compile_reference_kernel(kernel_path: Path, executable_path: Path) -> None:
    """Compiles a single reference kernel to an executable."""
    source_files = []
    for src_dir in (kernel_path, kernel_path.parent):
        for file in src_dir.iterdir():
            if file.is_file() and file.name.endswith('.c'):
                source_files.append(str(file))

    check_output(['cc', '-O3', '-std=gnu17', '-o', executable_path, '-DPOLYBENCH_TIME', f'-I{kernel_path}', f'-I{kernel_path.parent}'] + source_files)


def run_reference_kernel(suite: str, kernel: str, kernel_path: Path, time_budget: float) -> OptimizationLog:
    """Runs a reference kernel until a time budget is reached. Returns the result as an optimization log
       with a single step."""
    step = OptimizationStep(OrderedDict())
    log = OptimizationLog(OptimizationPreamble(OrderedDict()), [step])
    log.kernel_qualified_name = f'{suite}.{kernel}'
    log.preamble.target = Target.REFERENCE

    def compile(step: OptimizationStep, preamble: OptimizationPreamble, tmp_dir: str) -> Path:
        exe_path = Path(tmp_dir) / 'a.out'
        compile_reference_kernel(kernel_path, exe_path)
        return exe_path

    def run(step: OptimizationStep, exe_path: Path, timeout: float):
        step_output = check_output([str(exe_path)], timeout=timeout).decode('utf-8')
        step.c_compiler = c_compiler_version()
        step.run_time = float(step_output)
        step.extern_call_run_times = OrderedDict()

    run_solutions(log, compile, run, time_budget)
    return log


def run_all_reference_kernels(time_budget_per_kernel: float, number_of_threads: int = 1, kernels = KERNEL_NAMES):
    def run_one(triple: Tuple[str, str, Path]):
        suite, kernel, path = triple
        destination_path = REFERENCE_LOGS_DIR / f'cov-{kernel}.txt'
        if not destination_path.exists() and f'{suite}.{kernel}' in kernels:
            log = run_reference_kernel(suite, kernel, path, time_budget_per_kernel)
            log.write_to(destination_path)

    parallel_tasks(list(collect_reference_kernels()), run_one, number_of_threads)


def main():
    parser = OptionParser()
    parser.add_option("-t", "--time-budget", dest="timeout", default=DEFAULT_TIMEOUT_IN_SECS,
                      help="specify the time budget in seconds each solution gets", metavar="TIME_BUDGET", type='float')
    (options, args) = parser.parse_args()

    REFERENCE_LOGS_DIR.mkdir(parents=True, exist_ok=True)
    run_all_reference_kernels(options.timeout)


if __name__ == '__main__':
    main()
