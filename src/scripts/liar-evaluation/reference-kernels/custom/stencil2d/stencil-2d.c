/* stencil-2d.c: this file is a custom kernel reference implementation */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

/* Include polybench common header. */
#include <polybench.h>

/* Include benchmark-specific header. */
#include "stencil-2d.h"


/* Array initialization. */
static
void init_array (int n, int stencil_size,
		 DATA_TYPE POLYBENCH_2D(A,N,N,n,n),
         DATA_TYPE POLYBENCH_2D(stencil, STENCIL_SIZE, STENCIL_SIZE, stencil_size, stencil_size))
{
  int i, j;

  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      {
	A[i][j] = ((DATA_TYPE) i*(j+2) + 2) / n;
      }

  for (i = 0; i < stencil_size; i++) {
    for (j = 0; j < stencil_size; j++) {
        stencil[i][j] = SCALAR_VAL(1.0 / (stencil_size * stencil_size));
    }
  }
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int m,
		 DATA_TYPE POLYBENCH_2D(B,M,M,m,m))

{
  int i, j;

  POLYBENCH_DUMP_START;
  POLYBENCH_DUMP_BEGIN("B");
  for (i = 0; i < m; i++)
    for (j = 0; j < m; j++) {
      if ((i * m + j) % 20 == 0) fprintf(POLYBENCH_DUMP_TARGET, "\n");
      fprintf(POLYBENCH_DUMP_TARGET, DATA_PRINTF_MODIFIER, B[i][j]);
    }
  POLYBENCH_DUMP_END("B");
  POLYBENCH_DUMP_FINISH;
}


/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_stencil_2d(
			    int n,
                int stencil_size,
			    DATA_TYPE POLYBENCH_2D(A,N,N,n,n),
			    DATA_TYPE POLYBENCH_2D(B,M,M,n - stencil_size + 1,n - stencil_size + 1),
                DATA_TYPE POLYBENCH_2D(stencil,STENCIL_SIZE,STENCIL_SIZE,stencil_size,stencil_size))
{
  int i, j, k, l, offset;

  offset = _PB_STENCIL_SIZE / 2;

#pragma scop
    for (i = offset; i < _PB_N - offset; i++) {
        for (j = offset; j < _PB_N - offset; j++) {
            B[i - offset][j - offset] = SCALAR_VAL(0.0);
            for (k = -_PB_STENCIL_SIZE / 2; k <= _PB_STENCIL_SIZE / 2; k++) {
                for (l = -_PB_STENCIL_SIZE / 2; l <= _PB_STENCIL_SIZE / 2; l++) {
                    B[i - offset][j - offset] += stencil[k][l] * A[i + k][j + l];
                }
            }
        }
    }
#pragma endscop

}


int main(int argc, char** argv)
{
  /* Retrieve problem size. */
  int n = N;
  int m = M;
  int stencil_size = STENCIL_SIZE;

  /* Variable declaration/allocation. */
  POLYBENCH_2D_ARRAY_DECL(A, DATA_TYPE, N, N, n, n);
  POLYBENCH_2D_ARRAY_DECL(B, DATA_TYPE, M, M, m, m);
  POLYBENCH_2D_ARRAY_DECL(stencil, DATA_TYPE, STENCIL_SIZE, STENCIL_SIZE, stencil_size, stencil_size);


  /* Initialize array(s). */
  init_array(n, stencil_size, POLYBENCH_ARRAY(A), POLYBENCH_ARRAY(stencil));

  /* Start timer. */
  polybench_start_instruments;

  /* Run kernel. */
  kernel_stencil_2d(n, stencil_size, POLYBENCH_ARRAY(A), POLYBENCH_ARRAY(B), POLYBENCH_ARRAY(stencil));

  /* Stop and print timer. */
  polybench_stop_instruments;
  polybench_print_instruments;

  /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
  polybench_prevent_dce(print_array(m, POLYBENCH_ARRAY(B)));

  /* Be clean. */
  POLYBENCH_FREE_ARRAY(A);
  POLYBENCH_FREE_ARRAY(B);

  return 0;
}
