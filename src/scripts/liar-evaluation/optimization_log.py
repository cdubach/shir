#!/usr/bin/env python3

import copy
import csv
from functools import total_ordering
import math
from queue import Queue, Empty
import subprocess
from tempfile import TemporaryDirectory
from threading import Thread
import time
import distro
from abc import ABC
from enum import Enum
from os import PathLike
from collections import OrderedDict
from typing import Any, Callable, Iterable, List, Dict, Optional, Tuple, TypeVar
from pathlib import Path


RUN_TIME_KEY = 'run time'
RUN_STATUS_KEY = 'run status'
RUN_ERROR_KEY = 'run error'
EXTERN_RUN_TIMES_KEY = 'extern run times'
RUNS_KEY = 'number of runs'


class Strategy(str, Enum):
    """A saturation strategy. Saturation strategies specify how the optimizer should saturate the solution space."""

    SIMPLE = 'simple'
    PER_RULE_RANDOM = 'per-rule-random'
    ISARIA = 'isaria'
    ISARIA_NO_REBASE = 'isaria-no-rebase'

    def __repr__(self) -> str:
        return f'{type(self).__name__}.{self.name}'

@total_ordering
class Target(object):
    """A target for optimization. The target specifies the environment for which idioms are recognized. Each environment
       is implicitly associated with a target programming language. The target also specifies a saturation strategy."""

    def __init__(self, value: str, strategy: Strategy = Strategy.SIMPLE):
        """Creates a new target with the specified environment and saturation strategy."""

        self.value = value
        self.strategy = strategy

    @property
    def language(self) -> str:
        """The target programming language."""

        if self.with_strategy(Strategy.SIMPLE) in (Target.NONE, Target.BLAS, Target.REFERENCE):
            return 'c'
        else:
            return 'python'

    def __repr__(self) -> str:
        """The representation of the target."""

        return f'Target({repr(self.value)}, {repr(self.strategy)})'

    def __str__(self) -> str:
        """The string representation of the target."""

        return f'{self.value}.{self.strategy.value}'

    def __eq__(self, other: Any) -> bool:
        return self.value == other.value and self.strategy == other.strategy

    def __lt__(self, other: Any) -> bool:
        return (self.value, self.strategy) < (other.value, other.strategy)

    def __hash__(self) -> int:
        return hash((self.value, self.strategy))

    @property
    def kebab(self) -> str:
        """The kebab-case representation of the target."""

        if self.strategy == Strategy.SIMPLE:
            return self.value
        else:
            return f'{self.value}-{self.strategy.value}'

    def with_strategy(self, strategy) -> 'Target':
        """Returns a new target with the specified saturation strategy."""

        return Target(self.value, strategy)

Target.NONE = Target('none')
Target.NONE_PYTHON = Target('none-python')
Target.BLAS = Target('blas')
Target.TENSORFLOW = Target('tensorflow')
Target.PYTORCH = Target('pytorch')
Target.REFERENCE = Target('reference')


class RunStatus(str, Enum):
    SUCCESS = 'success'
    ERROR = 'error'
    TIMEOUT = 'timeout'


class ArithType(ABC):
    def _parse_impl(data: str) -> Tuple['ArithType', str]:
        first_nondigit_index = len(data)
        for i, c in enumerate(data):
            if not c.isdigit():
                first_nondigit_index = i
                break

        return ConstantType(int(data[0:first_nondigit_index])), data[first_nondigit_index:]

    @staticmethod
    def parse(data: str) -> 'ArithType':
        result, remainder = ArithType._parse_impl(data)
        if remainder.strip():
            raise Exception(f"Cannot parse type '{data}' with unknown tail '{remainder}'")

        return result


class ConstantType(ArithType):
    def __init__(self, value: int) -> None:
        super().__init__()
        self.value = value

    def __str__(self) -> str:
        return str(self.value)


class DataType(ABC):
    @staticmethod
    def _parse_impl(data: str) -> Tuple['DataType', str]:
        data = data.strip()
        if data.startswith('double'):
            return (DoubleType(), data[len('double'):])
        elif data[0] == 'i':
            first_nondigit_index = len(data) - 1
            for i, c in enumerate(data[1:]):
                if not c.isdigit():
                    first_nondigit_index = i
                    break

            return IntType(int(data[1:first_nondigit_index])), data[first_nondigit_index:]

        elif data[0] == '[':
            element_type, remainder = DataType._parse_impl(data[1:])
            remainder = remainder.strip()
            if remainder[0] != 'x':
                raise Exception(f"Cannot parse array-like type '{data}'")

            length, remainder = ArithType._parse_impl(remainder[1:].strip())
            remainder = remainder.strip()
            if remainder[0] != ']':
                raise Exception(f"Cannot parse array-like type '{data}'")

            return ArrayType(element_type, length), remainder[1:]

        elif data[0] == '(':
            remainder = data[1:].lstrip()
            elements = []
            while remainder and not remainder.startswith(')'):
                element_type, remainder = DataType._parse_impl(remainder)
                remainder = remainder.lstrip()
                elements.append(element_type)

                if remainder.startswith(','):
                    remainder = remainder[1:].lstrip()
                elif remainder.startswith(')'):
                    pass
                else:
                    raise Exception(f"Cannot parse tuple-like type '{data}'")

            if not remainder.startswith(')'):
                raise Exception(f"Cannot parse tuple-like type '{data}'")

            return TupleType(elements), remainder[1:]

        else:
            raise Exception(f"Cannot parse unknown type '{data}'")

    @staticmethod
    def parse(data: str) -> 'DataType':
        result, remainder = DataType._parse_impl(data)
        if remainder.strip():
            raise Exception(f"Cannot parse type '{data}' with unknown tail '{remainder}'")

        return result


class ArrayType(DataType):
    def __init__(self, element_type: DataType, length: ArithType) -> None:
        super().__init__()
        self.element_type = element_type
        self.length = length

    @property
    def flat_element_type(self) -> DataType:
        match self.element_type:
            case ArrayType():
                return self.element_type.flat_element_type
            case other:
                return self.element_type

    @property
    def flat_length(self) -> ArithType:
        match self.element_type:
            case ArrayType():
                inner_flat_length = self.element_type.flat_length
                match inner_flat_length, self.length:
                    case ConstantType(), ConstantType():
                        return ConstantType(inner_flat_length.value * self.length.value)
                    case _, _:
                        raise Exception(f'Cannot multiply non-constant arithmetic types {inner_flat_length} and {self.length}')
            case _:
                return self.length

    def __str__(self) -> str:
        return f'[{str(self.element_type)} x {str(self.length)}]'


class TupleType(DataType):
    def __init__(self, fields: List[DataType]) -> None:
        super().__init__()
        self.fields = fields

    def __str__(self) -> str:
        return f'({", ".join([str(field) for field in self.fields])})'


class ScalarType(DataType):
    pass


class DoubleType(ScalarType):
    def __str__(self) -> str:
        return 'double'


class IntType(ScalarType):
    def __init__(self, width: int) -> None:
        super().__init__()
        self.width = width

    def __str__(self) -> str:
        return f'i{self.width}'


def format_properties(properties: Dict[str, str]) -> str:
    lines = []
    for k, v in properties.items():
        if '\n' in v:
            lines.append(f'{k}: <<<')
            lines.append(v.rstrip())
            lines.append('>>>')
        else:
            lines.append(f'{k}: {v}')
    return '\n'.join(lines)


def parse_properties(lines: List[str]) -> OrderedDict[str, str]:
    parsed = OrderedDict()
    text_key = None
    text = None
    for line in lines:
        if text_key:
            if line.strip() == '>>>':
                parsed[text_key] = ''.join(text)
                text = None
                text_key = None
            else:
                text.append(line)
        elif line and not line.isspace():
            key, value = line.split(':', maxsplit=1)
            key, value = key.strip(), value.strip()
            if value == '<<<':
                text_key = key
                text = []
            else:
                parsed[key] = value

    return parsed


def format_dict_property(items: OrderedDict[str, str]) -> str:
    return ', '.join(f'{v} x {k}' for k, v in items.items())


T = TypeVar('T')
def parse_dict_property(value: Optional[str], parse_item: Callable[[str], T]) -> OrderedDict[str, T]:
    result = OrderedDict()
    if value:
        for piece in value.split(','):
            value, name = piece.split('x', maxsplit=1)
            result[name.strip()] = parse_item(value.strip())

    return result


def simplify_extern_function_name(name: str) -> str:
    """Simplifies the name of an external function."""
    prefixes = ('cblas_d', 'idiom_d', 'cblas_', 'idiom_', 'torch.')
    if name.endswith('$'):
        return simplify_extern_function_name(name[:-1])
    elif name.startswith(prefixes):
        for prefix in prefixes:
            if name.startswith(prefix):
                return simplify_extern_function_name(name[len(prefix):])

        assert False
    else:
        return name


class OptimizationStep:
    """A single step in an optimization log."""

    def __init__(self, properties: OrderedDict[str, str]) -> None:
        self.properties = properties

    def copy(self) -> 'OptimizationStep':
        return OptimizationStep(self.properties.copy())

    def __copy__(self) -> 'OptimizationStep':
        return self.copy()

    def __deepcopy__(self) -> 'OptimizationStep':
        return self.copy()

    @property
    def full_iteration(self) -> bool:
        """Whether this step is a full iteration."""
        return self.properties.get('full iteration') == 'true'
    
    @property
    def applied_matches(self) -> int:
        """The number of applied matches in this step."""
        return int(self.properties['applied matches'])

    @property
    def node_count(self) -> int:
        """The number of nodes in the e-graph at the end of this step."""
        return int(self.properties['node count'])

    @property
    def class_count(self) -> int:
        """The number of classes in the e-graph at the end of this step."""
        return int(self.properties['class count'])

    @property
    def elapsed_nanoseconds(self) -> int:
        """The number of nanoseconds that have elapsed since the start of the optimization."""
        return int(self.properties['elapsed nanoseconds'])

    @property
    def solution_as_text(self) -> str:
        """The solution at this step in a textual, lambda-calculus based representation."""
        return self.properties['solution']

    @property
    def solution_as_scala(self) -> str:
        """The solution at this step as a Scala program."""
        return self.properties['scala solution']

    @property
    def solution_as_c(self) -> str:
        """The solution at this step as a C program."""
        return self.properties['C solution']

    @property
    def solution_as_python(self) -> str:
        """The solution at this step as a Python program."""
        return self.properties['python solution']

    @property
    def run_time(self) -> Optional[float]:
        """The total run time of an execution of this step's solution."""
        result = self.properties.get(RUN_TIME_KEY)
        if result:
            return float(result)
        else:
            return None

    @run_time.setter
    def run_time(self, value: Optional[float]) -> None:
        if value is None:
            del self.properties[RUN_TIME_KEY]
        else:
            self.properties[RUN_TIME_KEY] = str(value)

    @property
    def run_status(self) -> Optional[RunStatus]:
        status = self.properties.get(RUN_STATUS_KEY)
        if status:
            return RunStatus(status)
        else:
            return status

    @run_status.setter
    def run_status(self, value: Optional[RunStatus]) -> None:
        if value is None:
            del self.properties[RUN_STATUS_KEY]
        else:
            self.properties[RUN_STATUS_KEY] = value.value

    @property
    def runs(self) -> Optional[int]:
        result = self.properties.get(RUNS_KEY)
        if result:
            return int(result)
        else:
            return None
        
    @runs.setter
    def runs(self, value: Optional[int]) -> None:
        if value is None:
            del self.properties[RUNS_KEY]
        else:
            self.properties[RUNS_KEY] = str(value)

    @property
    def run_error(self) -> Optional[str]:
        return self.properties.get(RUN_ERROR_KEY)
    
    @run_error.setter
    def run_error(self, value: Optional[str]) -> None:
        if value is None:
            del self.properties[RUN_ERROR_KEY]
        else:
            self.properties[RUN_ERROR_KEY] = value

    @property
    def extern_call_run_times(self) -> Optional[OrderedDict[str, float]]:
        """The run time of each external call on an execution of this step's solution."""
        return parse_dict_property(self.properties.get(EXTERN_RUN_TIMES_KEY), float)

    @extern_call_run_times.setter
    def extern_call_run_times(self, value: Optional[OrderedDict[str, float]]) -> None:
        if value is None:
            del self.properties[EXTERN_RUN_TIMES_KEY]
        else:
            self.properties[EXTERN_RUN_TIMES_KEY] = format_dict_property(value)

    @property
    def extern_call_coverages(self) -> Optional[OrderedDict[str, float]]:
        """The run time of each external call as a percentage of this step's total execution time."""
        run_times = self.extern_call_run_times
        total_run_time = self.run_time
        if run_times:
            return OrderedDict((k, v / total_run_time) for k, v in run_times.items())
        else:
            return run_times
        
    @property
    def total_coverage(self) -> Optional[float]:
        """The combined run time of all external calls as a percentage of this step's total execution time."""
        coverages = self.extern_call_coverages
        if coverages:
            return sum(coverages.values())
        else:
            return None

    @property
    def extern_calls(self) -> OrderedDict[str, int]:
        return parse_dict_property(self.properties.get('extern calls'), int)

    @property
    def simplified_extern_calls(self) -> OrderedDict[str, int]:
        elements = sorted([
            (simplify_extern_function_name(k), v)
            for k, v in self.extern_calls.items()
        ])
        return OrderedDict(elements)

    @property
    def c_compiler(self) -> Optional[str]:
        return self.properties.get('c compiler')

    @c_compiler.setter
    def c_compiler(self, value: Optional[str]):
        if value:
            self.properties['c compiler'] = value
        else:
            del self.properties['c compiler']

    @property
    def python_interpreter(self) -> Optional[str]:
        return self.properties.get('python interpreter')

    @python_interpreter.setter
    def python_interpreter(self, value: Optional[str]):
        if value:
            self.properties['python interpreter'] = value
        else:
            del self.properties['python interpreter']

    @staticmethod
    def parse(lines: List[str]) -> 'OptimizationStep':
        """Parses an optimization step described in textual format."""
        return OptimizationStep(parse_properties(lines))

    def __repr__(self) -> str:
        return f'OptimizationStep({repr(self.properties)})'

    def __str__(self) -> str:
        return f'===   step   ===\n{format_properties(self.properties)}\n=== step end ==='


class OptimizationPreamble:
    """The preamble of an optimization log."""
    def __init__(self, properties: OrderedDict[str, str]) -> None:
        self.properties = properties

    @property
    def name(self) -> str:
        _, result = self.qualified_name.split('.', maxsplit=1)
        return result

    @property
    def suite(self) -> str:
        result, _ = self.qualified_name.split('.', maxsplit=1)
        return result

    @property
    def qualified_name(self) -> str:
        return self.properties['running kernel']

    @qualified_name.setter
    def qualified_name(self, value: str) -> None:
        self.properties['running kernel'] = value

    @property
    def date_time(self) -> str:
        return self.properties['date/time']
    
    @date_time.setter
    def date_time(self, value: str) -> None:
        self.properties['date/time'] = value

    @property
    def commit_id(self) -> str:
        return self.properties['commit id']

    @property
    def idiom_recognition_enabled(self) -> bool:
        return self.properties['idiom recognition enabled'] == 'true'

    @property
    def target(self) -> Target:
        return Target(self.properties['target'], Strategy(self.properties['strategy']))

    @target.setter
    def target(self, target: Target) -> None:
        self.properties['target'] = target.value
        self.properties['strategy'] = target.strategy.value

    @property
    def return_type(self) -> DataType:
        return DataType.parse(self.properties['return type'])

    @property
    def param_types(self) -> List[DataType]:
        return [
            DataType.parse(line)
            for line in self.properties['parameter types'].split('\n')
            if line
        ]

    @property
    def machine_name(self) -> str:
        return self.properties['machine name']

    @property
    def optimizer_scala_version(self) -> Optional[str]:
        return self.properties.get('optimizer scala version')

    @optimizer_scala_version.setter
    def optimizer_scala_version(self, value: Optional[str]):
        if value:
            self.properties['optimizer scala version'] = value
        else:
            del self.properties['optimizer scala version']

    def __repr__(self) -> str:
        return f'OptimizationPreamble({self.properties})'

    def __str__(self) -> str:
        return format_properties(self.properties)

    @staticmethod
    def parse(lines: List[str]) -> 'OptimizationPreamble':
        return OptimizationPreamble(parse_properties(lines))


class OptimizationLog:
    """The contents of an optimization log."""

    def __init__(self, preamble: OptimizationPreamble, steps: List[OptimizationStep]) -> None:
        self.preamble = preamble
        self.steps = steps

    @property
    def kernel_name(self) -> str:
        return self.preamble.name

    @property
    def kernel_suite(self) -> str:
        return self.preamble.suite

    @property
    def kernel_qualified_name(self) -> str:
        return self.preamble.qualified_name

    @kernel_qualified_name.setter
    def kernel_qualified_name(self, value: str) -> None:
        self.preamble.qualified_name = value

    @property
    def time_per_step(self) -> List[int]:
        last_step_time = 0
        step_times = []
        for step in self.steps:
            t = step.elapsed_nanoseconds
            step_times.append((t - last_step_time) / 1e9)
            last_step_time = t

        return step_times

    @staticmethod
    def parse(log_contents: List[str]) -> 'OptimizationLog':
        """Parses an optimization log's contents, represented as a list of lines."""
        magic_string = 'running kernel:'
        if not log_contents[0].startswith(magic_string):
            raise Exception(f"Expected magic string '{magic_string}' in log, found '{log_contents[0]}'")

        preamble_lines = []
        step_lines = [[]]
        found_first_step = False
        for line in log_contents:
            if line.startswith('==='):
                # We found a marker line.
                if 'step end' in line:
                    step_lines.append([])

                elif 'step' in line:
                    found_first_step = True

                else:
                    raise Exception(f"Ill-understood marker line '{line}'")

            elif found_first_step:
                step_lines[-1].append(line)

            else:
                preamble_lines.append(line)

        # Discard last step. It's either empty or incomplete.
        step_lines = step_lines[:-1]

        return OptimizationLog(
            OptimizationPreamble.parse(preamble_lines),
            [OptimizationStep.parse(lines) for lines in step_lines])

    @staticmethod
    def parse_file(path: PathLike) -> 'OptimizationLog':
        """Parses the contents of an optimization log stored in a file."""
        with open(path, 'r') as file:
            lines = file.readlines()
            return OptimizationLog.parse(lines)

    def write_to(self, path: PathLike) -> None:
        Path(path).write_text(str(self))

    def __repr__(self) -> str:
        return f'OptimizationLog({repr(self.preamble)}, {repr(self.steps)})'

    def __str__(self) -> str:
        lines = [str(self.preamble)] + [str(step) for step in self.steps]
        return '\n'.join(lines)

    def _line_chart_with_annotations(self, y_values: List[float],
                                     drop_annotations: bool = False,
                                     annotations_on_left: bool = True,
                                     label: Optional[str] = None,
                                     **properties) -> str:

        basic_line_plot = tex_line_plot(enumerate(y_values), **properties)
        if label:
            basic_line_plot += f' \\label{{{label}}}'

        if drop_annotations:
            return basic_line_plot

        lines = [basic_line_plot]
        last_solution = ''
        for i, step in enumerate(self.steps):
            if step.solution_as_text != last_solution and step.extern_calls:
                last_solution = step.solution_as_text
                times = tex_command('times')
                def texttt(arg): return tex_call('texttt', arg)

                if y_values[i] is not None:
                    note = f'{tex_command("scriptsize")} ${", ".join([f"{v} {times} {texttt(k)}" for k, v in step.simplified_extern_calls.items()])}$'
                    arrow_placement = "+(-5pt,10pt) node[left]" if annotations_on_left else "+(5pt,10pt) node[right]"
                    arrow = f'{tex_command("draw")} [<-] (axis cs:{i},{y_values[i]})-- {arrow_placement} {{{note}}};'

                    lines.append(arrow)

        return '\n'.join(lines)

    @property
    def node_count_chart(self) -> str:
        """A chart that plots the number of optimization steps on the X-axis and the node count
           for each step on the Y-axis. The chart has an annotation for each change to the
           composition of extern calls."""
        return self._line_chart_with_annotations(
            [step.node_count for step in self.steps],
            label = f'fig:nodes-over-time-{self.kernel_name}-{self.preamble.target.value}:node-count-plot',
            color = 'blue',
            mark = 'none')

    @property
    def compilation_time_chart(self) -> str:
        """A chart that plots the number of optimization steps on the X-axis and the compilation
           time for each step on the Y-axis."""
        return tex_line_plot(enumerate(self.time_per_step), color='red', mark='none')

    @property
    def run_time_chart(self) -> str:
        """A chart that plots the number of optimization steps on the X-axis and the compiled
           program run time for each step on the Y-axis. The chart has an annotation for each
           change to the composition of extern calls."""

        return self._line_chart_with_annotations(
            [step.run_time for step in self.steps],
            annotations_on_left = False,
            drop_annotations=True)

    @property
    def coverage_chart(self) -> str:
        """A bar chart that plots a sequence of stacked bars, one for each saturation step.
           Each bar depicts the coverage achieved by the extern function calls."""

        all_extern_functions = sorted({
            key
            for step in self.steps
            for key in step.extern_calls
        })

        lines = []
        step_coverages = [step.extern_call_coverages for step in self.steps]
        for extern_function in all_extern_functions:
            function_coverages = [
                coverages.get(extern_function, coverages.get(simplify_extern_function_name(extern_function), 0))
                for coverages in step_coverages
            ]
            coords = " ".join([
                f'({i}, {coverage})'
                for i, coverage in enumerate(function_coverages)
                if coverage is not None
            ])
            lines.append(f'{tex_command("addplot+")}[ybar] plot coordinates {{{coords}}};')

        lines.append(tex_legend(simplify_extern_function_name(func) for func in all_extern_functions))

        return '\n'.join(lines)


def sort_logs(logs: List[OptimizationLog]) -> List[OptimizationLog]:
    return sorted(logs, key=lambda log: (log.kernel_suite == 'custom', log.kernel_suite, log.kernel_name))


def tex_command(name: str) -> str:
    return '\\' + name


def tex_call(name: str, arg: str) -> str:
    return f'{tex_command(name)}{{{arg}}}'


def tex_line_plot(coordinates: Iterable[Tuple[float, float]], **properties):
    formatted_props = ', '.join([f'{k}={v}' for k, v in properties.items()])
    header = tex_command(f'addplot[{formatted_props}]') if properties else tex_command('addplot')
    lines = [header + ' coordinates {'] + \
        [f'    ({x}, {y})' for x, y in coordinates if y is not None] + \
        ['};']
    return '\n'.join(lines)


def tex_legend(entries: Iterable[str]) -> str:
    return tex_call('legend', ', '.join(entries)) + ';'


def write_comparison_csv(logs: List[OptimizationLog], file):
    # Collect the names of all extern functions.
    all_extern_functions = sorted({
        key
        for log in logs
        for step in log.steps
        for key in step.simplified_extern_calls
    })

    headers = ['name', 'suite', 'externs', 'steps', 'classes', 'nodes', 'time'] + \
        all_extern_functions

    writer = csv.DictWriter(file, fieldnames=headers)
    writer.writeheader()
    for log in logs:
        fields = {
            'name': log.kernel_name,
            'suite': log.kernel_suite,
            'externs': ' +~'.join(f'${v}{tex_command("times")}{tex_call("textrm", k)}$' for k, v in log.steps[-1].simplified_extern_calls.items()),
            'steps': len(log.steps),
            'classes': log.steps[-1].class_count,
            'nodes': log.steps[-1].node_count,
            'time': log.steps[-1].elapsed_nanoseconds / 1e9,
            **log.steps[-1].simplified_extern_calls
        }
        writer.writerow(fields)


def group_logs_by_benchmark(logs: List[OptimizationLog]) -> OrderedDict[str, List[OptimizationLog]]:
    logs_by_benchmark = OrderedDict()
    for log in logs:
        name = log.kernel_qualified_name
        if name not in logs_by_benchmark:
            logs_by_benchmark[name] = []

        logs_by_benchmark[name].append(log)

    return logs_by_benchmark


def log_for_target(target: Target, logs: List[OptimizationLog]) -> OptimizationLog:
    for log in logs:
        if log.preamble.target == target:
            return log

    found = ', '.join(log.preamble.target for log in logs)
    raise Exception(f'Cannot find {logs[0].kernel_qualified_name} log for target {target}; found: {found}')


def write_run_time_csv(logs: List[OptimizationLog], file):
    """Writes a CSV that summarizes run-time properties."""

    logs_by_benchmark = group_logs_by_benchmark(logs)
    targets = sorted({log.preamble.target for log in logs})

    headers = ['name', 'suite'] + [f'time{target.kebab}' for target in targets] + [f'cov{target.kebab}' for target in targets]
    writer = csv.DictWriter(file, fieldnames=headers)
    writer.writeheader()
    for logs_for_benchmark in logs_by_benchmark.values():
        fields = {
            'name': logs_for_benchmark[0].kernel_name,
            'suite': logs_for_benchmark[0].kernel_suite
        }
        for log in logs_for_benchmark:
            fields[f'time{log.preamble.target.kebab}'] = log.steps[-1].run_time
            fields[f'cov{log.preamble.target.kebab}'] = log.steps[-1].total_coverage

        writer.writerow(fields)


def geomean(xs: List[float]) -> float:
    return math.exp(math.fsum(math.log(x) for x in xs) / len(xs))


def generate_speedup_bars(logs: List[OptimizationLog], included: List[Target] = [], baseline: Target = Target.NONE) -> str:
    """Generates a TikZ bar chart representing the speedup of each solution relative to baseline."""

    logs_by_benchmark = group_logs_by_benchmark(logs)
    targets = sorted({log.preamble.target for log in logs})
    if included:
        targets = [target for target in targets if target in included and target != baseline]

    def final_run_time(target, benchmark_logs) -> Optional[float]:
        return log_for_target(target, benchmark_logs).steps[-1].run_time
    
    def speedup(target, benchmark_logs) -> float:
        return final_run_time(baseline, benchmark_logs) / final_run_time(target, benchmark_logs)
    
    def include_benchmark(target, benchmark_logs) -> bool:
        return len(benchmark_logs) > 1 \
            and final_run_time(baseline, benchmark_logs) is not None \
            and final_run_time(target, benchmark_logs) is not None

    def format_addplot(coords):
        coords_tex = ' '.join(f'({k}, {v})' for k, v in coords if v is not None)
        return f'{tex_command("addplot+[mark=none]")} coordinates {{{coords_tex}}};'

    # Add one bar for each target/benchmark combo
    lines = []
    for target in targets:
        if target == baseline:
            continue

        coords = [
            (benchmark_logs[0].kernel_name, speedup(target, benchmark_logs))
            for benchmark_logs in logs_by_benchmark.values()
            if include_benchmark(target, benchmark_logs)
        ]

        # If there are no valid results for this benchmark, skip it
        if not coords:
            continue

        coords.append(('geomean', geomean([v for _, v in coords])))
        lines.append(format_addplot(coords))

    # Add a bar for the per-benchmark max speedup
    max_coords = [
        (benchmark_logs[0].kernel_name, max(
            speedup(target, benchmark_logs)
            for target in targets
            if target != baseline and include_benchmark(target, benchmark_logs)))
        for benchmark_logs in logs_by_benchmark.values()
        if any(
            target
            for target in targets
            if target != baseline and include_benchmark(target, benchmark_logs))
    ]
    lines.append(format_addplot([('geomean', geomean([v for _, v in max_coords]))]))

    legend_names = {
        Target.NONE: 'Pure C',
        Target.NONE_PYTHON: 'Pure Python',
        Target.BLAS: 'BLAS',
        Target.TENSORFLOW: 'TensorFlow',
        Target.PYTORCH: 'PyTorch',
        Target.REFERENCE: 'Reference'
    }

    # Include a legend
    lines.append(tex_legend([legend_names[target] for target in targets if target != baseline] + ['Best']))

    return '\n'.join(lines)


DRIVERS_DIR = Path(__file__).absolute().parent
PROJECT_ROOT = DRIVERS_DIR.parent.parent.parent


def create_scala_coverage_project(directory: PathLike, preamble: OptimizationPreamble, step: OptimizationStep) -> str:
    root_dir = PROJECT_ROOT / "src" / "main"
    main_object = "Test"

    time_queries = [
        f'val {extern_call_name} = time.children.filter(_.name == "{extern_call_name}").map(_.duration.toMillis).sum.toDouble'
        for extern_call_name in step.extern_calls
    ]
    print_statements = [
        f'println(s"{extern_call_name}: ${extern_call_name}")'
        for extern_call_name in step.extern_calls
    ]

    time_and_print = '\n'.join(' ' * 4 + line for line in time_queries + print_statements)

    wrapper = f'''
import cGen.eqsat.{{HierarchicalStopwatch, MeasuredActivity}}
import cGen.idioms.Blas._

import scala.util.Random

object {main_object} {{
{step.solution_as_scala}

  def main(args: Array[String]): Unit = {{
    val time = {preamble.name}()
    val name = time.name
    val totalTime = time.duration.toMillis
    println(s"total: $totalTime")
{time_and_print}
  }}
}}
'''
    build_description = f'''
name := "{preamble.name}"
version := "1.0"
scalaVersion := "2.12.7"
scalaSource in Compile := baseDirectory(_ / ".").value
'''
    main_dir = Path(directory)
    eqsat_dir = main_dir / "cGen" / "eqsat"
    root_eqsat_dir = root_dir / "cGen" / "eqsat"
    idioms_dir = main_dir / "cGen" / "idioms"
    root_idioms_dir = root_dir / "cGen" / "idioms"

    main_dir.mkdir(parents=True, exist_ok=True)
    eqsat_dir.mkdir(parents=True, exist_ok=True)
    idioms_dir.mkdir(parents=True, exist_ok=True)

    (main_dir / "Test.scala").write_text(wrapper)
    (main_dir / "build.sbt").write_text(build_description)
    (eqsat_dir / "HierarchicalStopwatch.scala").write_text(
        (root_eqsat_dir / "HierarchicalStopwatch.scala").read_text())
    (idioms_dir / "Blas.scala").write_text(
        (root_idioms_dir / "Blas.scala").read_text())

    return main_object


def create_c_coverage_project(directory: PathLike, preamble: OptimizationPreamble, step: OptimizationStep):
    root_src_dir = PROJECT_ROOT / "src"
    main_dir = Path(directory)
    root_liar_scripts_dir = root_src_dir / "scripts" / "liar-evaluation"

    def name_type(t: DataType) -> str:
        match t:
            case DoubleType():
                return 'DoubleType'

            case ArrayType():
                return f'ARRAY_{name_type(t.flat_element_type)}'

            case TupleType():
                return f'tuple_{"_".join([name_type(field) for field in t.fields])}'

            case _:
                raise NotImplementedError()

    def c_type(t: DataType) -> str:
        match t:
            case DoubleType():
                return 'double'

            case ArrayType():
                return f'{c_type(t.flat_element_type)}*'

            case TupleType():
                return name_type(t)

            case _:
                raise NotImplementedError()

    temporaries = {}

    def print_value(t: DataType, ptr: str) -> str:
        """Synthesizes a statement that print a value of a certain type. The value is identified by a pointer, encoded as a string."""
        match t:
            case DoubleType():
                return f'printf("%0.2lf\\n", *{ptr});'

            case ArrayType():
                return '\n'.join([
                    f'for (int i = 0; i < {t.flat_length}; i += 20) {{',
                    print_value(t.flat_element_type, f'&({ptr})[i]'),
                    '}'
                ])

            case TupleType():
                inner_prints = [print_value(field, f'({ptr})->t{i}') for i, field in enumerate(t.fields)]
                return f'{{ {" ".join(inner_prints)} }}'

            case _:
                raise NotImplementedError()

    def rand_value(t: DataType) -> str:
        match t:
            case DoubleType():
                return 'drand()'

            case ArrayType():
                elem_type = t.flat_element_type
                length = t.flat_length
                tmp_name = f'tmp{len(temporaries)}'
                temporaries[tmp_name] = (t, [
                    f'{tmp_name} = ({c_type(t)})malloc(sizeof({c_type(elem_type)}) * {length});',
                    f'for (int i = 0; i < {length}; i++) {{',
                    f'{tmp_name}[i] = {rand_value(elem_type)};',
                    '}'
                ])
                return tmp_name

            case _:
                raise NotImplementedError()

    def storage_location(t: DataType) -> str:
        match t:
            case DoubleType():
                tmp_name = f'tmp{len(temporaries)}'
                temporaries[tmp_name] = (t, [])
                return f'&{tmp_name}'

            case ArrayType():
                tmp_name = f'tmp{len(temporaries)}'
                elem_type = t.flat_element_type
                length = t.flat_length
                temporaries[tmp_name] = (t, [
                    f'{tmp_name} = ({c_type(t)})malloc(sizeof({c_type(elem_type)}) * {length});'
                ])
                return tmp_name

            case TupleType():
                values = [
                    (i, storage_location(field_type))
                    for i, field_type in enumerate(t.fields)
                    if not isinstance(field_type, ScalarType)
                ]
                tmp_name = f'tmp{len(temporaries)}'
                temporaries[tmp_name] = (t, [
                    f'{tmp_name}.t{i} = {value};'
                    for i, value in values
                ])
                return f'&{tmp_name}'

            case _:
                raise NotImplementedError(f"Unsupported type '{t}'")

    result_location = storage_location(preamble.return_type)
    func0_args = ', '.join([rand_value(t) for t in preamble.param_types] + [result_location])
    main_func_lines = ['int main(int argc, char **argv) {'] + \
        ['double ns_for_total;'] + \
        [f'{c_type(t)} {name};' for name, (t, _) in temporaries.items()] + \
        ['ns_for_total = 0.0;'] + \
        [line for _, init in temporaries.values() for line in init] + \
        [f'timed(ns_for_total, {{ func0({func0_args}); }});'] + \
        [f'printf("total: %f\\n", ns_for_total);'] + \
        [f'printf("{name}: %f\\n", ns_for_{simplify_extern_function_name(name)});' for name in step.extern_calls] + \
        [f'if (argc > 42) {{ {print_value(preamble.return_type, result_location)} }}'] + \
        ['return 0;'] + \
        ['}']

    main_func = '\n'.join(main_func_lines)

    main_dir.mkdir(parents=True, exist_ok=True)

    if distro.id() == 'centos':
        include_option = ' -I/usr/include/openblas'
    else:
        include_option = ''

    makefile = f'''
main: main.c idioms.c idioms.h
    cc -O3 -std=gnu17 -o main main.c idioms.c -lopenblas{include_option}

.PHONY: clean
clean:
    rm main
'''.replace(4 * ' ', '\t')

    tweaked_c = ''.join(
        f'__attribute__ ((noinline)) {line}' if 'func0' in line else line
        for line in step.solution_as_c.splitlines(keepends=True))
    (main_dir / "main.c").write_text('#include "idioms.h"\n' + tweaked_c + '\n' + main_func)
    (main_dir / "idioms.h").write_text(
        (root_liar_scripts_dir / "idioms.h").read_text())
    (main_dir / "idioms.c").write_text(
        (root_liar_scripts_dir / "idioms.c").read_text())
    (main_dir / "Makefile").write_text(makefile)


def c_compiler_version() -> str:
    return subprocess.check_output(['cc', '--version']).decode('utf-8').split('\n')[0]


def create_python_coverage_project(directory: PathLike, preamble: OptimizationPreamble, step: OptimizationStep, enable_compilation: bool = True) -> PathLike:
    drivers_dir = DRIVERS_DIR
    main_dir = Path(directory)

    main_dir.mkdir(parents=True, exist_ok=True)

    python_code = step.solution_as_python
    if not enable_compilation:
        python_code = python_code.replace("torch.compile(kernel)", "torch.compile(kernel, disable=True)")

    (main_dir / "run.py").write_text(python_code)
    (main_dir / "time_function.py").write_text(
        (drivers_dir / "time_function.py").read_text())
    (main_dir / "utils.py").write_text(
        (drivers_dir / "utils.py").read_text())

    return main_dir / 'run.py'


class SbtRunException(Exception):
    def __init__(self, cmd_args: List[str], return_code: int, output: str, errors: str, processed_output: str) -> None:
        super().__init__(f'{" ".join(cmd_args)} produced nonzero exit code {return_code}; output: {output}\n{errors}')
        self.return_code = return_code
        self.output = output
        self.errors = errors
        self.processed_output = processed_output


class SbtStackOverflowException(SbtRunException):
    def __init__(self, cmd_args: List[str], return_code: int, output: str, errors: str, processed_output: List[str]) -> None:
        super().__init__(cmd_args, return_code, output, errors, processed_output)


def run_with_sbt(main: str, args: List[str], cwd: Optional[PathLike] = None, timeout: Optional[float] = None) -> List[str]:
    """Runs a program with sbt. Returns its output as a sequence of lines."""
    sbt_args = ['runMain', main] + args
    cmd_args = ['sbt', ' '.join(sbt_args)]
    proc = subprocess.Popen(cmd_args, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:
        outs, errs = proc.communicate(timeout=timeout)
        timeout = False
    except subprocess.TimeoutExpired:
        proc.kill()
        outs, errs = proc.communicate()
        timeout = True

    decoded_output = outs.decode('utf8')
    def process_output():
        found_header = False
        lines = []
        for line in decoded_output.split('\n'):
            if line.strip().startswith((f'[info] running {main}', f'[info] running (fork) {main}')):
                found_header = True
            elif found_header:
                prefix = '[info]'
                if line.startswith(prefix):
                    line = line[len(prefix):]
                    if line.startswith(' '):
                        line = line[1:]

                    lines.append(line)
                elif line.startswith('['):
                    pass
                else:
                    lines.append(line)

        return lines

    if not timeout and proc.returncode:
        error_output = errs.decode('utf8')
        stack_overflow_message = '[error] Exception in thread "main" java.lang.StackOverflowError'
        if stack_overflow_message in error_output or stack_overflow_message in decoded_output:
            raise SbtStackOverflowException(cmd_args, proc.returncode, decoded_output, error_output, process_output())
        else:
            raise SbtRunException(cmd_args, proc.returncode, decoded_output, error_output, process_output())
    else:
        return process_output()


def run_solutions(log: OptimizationLog,
                  compile: Callable[[OptimizationStep, OptimizationPreamble, str], Any],
                  run: Callable[[OptimizationStep, Any, float], None],
                  timeout: float):
    for i, step in reversed(list(enumerate(log.steps))):
        if step.run_time is not None and step.extern_call_run_times is not None:
            continue

        prefix = f'[{log.preamble.qualified_name} ({log.preamble.target}/{log.preamble.target.language})]'
        print(f'{prefix} Running step {i} solution...')
        with TemporaryDirectory() as tmpdir:
            tmpdir = Path(tmpdir)

            # Package and compile the kernel
            run_input = compile(step, log.preamble, tmpdir)
            successes = []
            other_exception = None
            timeout_exception = None
            start_time = time.monotonic()
            while time.monotonic() < start_time + timeout:
                try:
                    # Run the kernel
                    step_copy = step.copy()
                    run(step_copy, run_input, start_time + timeout - time.monotonic())
                    successes.append(step_copy)
                except subprocess.TimeoutExpired as e:
                    step.run_status = RunStatus.TIMEOUT
                    timeout_exception = e
                    break
                except subprocess.CalledProcessError as e:
                    other_exception = e
                    break

            if not successes and not other_exception:
                step.run_status = RunStatus.TIMEOUT
                print(f'{prefix} Step {i} timed out after {timeout_exception.timeout} seconds')
            elif other_exception:
                step.run_status = RunStatus.ERROR
                step.run_error = str(other_exception)
                print(f'{prefix} Step {i} threw an error: {other_exception}')
            else:
                # Average out run times and extern times
                step.run_time = sum(s.run_time for s in successes) / len(successes)
                step.extern_call_run_times = OrderedDict(
                    (key, sum(s.extern_call_run_times[key] for s in successes) / len(successes))
                    for key in successes[0].extern_call_run_times
                )

                # Add any other properties from successes
                for success in successes:
                    for k, v in success.properties.items():
                        if k not in step.properties:
                            step.properties[k] = v

                step.run_status = RunStatus.SUCCESS
                step.runs = len(successes)


T = TypeVar('T')
def parallel_tasks(inputs: List[T], run: Callable[[T], None], number_of_threads: int) -> None:
    """Launches a number of parallel threads to process tasks."""
    if number_of_threads <= 1:
        for value in inputs:
            run(value)

    else:
        queue = Queue()

        for value in inputs:
            queue.put(value)

        def run_thread():
            while not queue.empty():
                try:
                    value = queue.get()
                    run(value)
                    queue.task_done()
                except Empty:
                    pass

        for _ in range(number_of_threads):
            thread = Thread(target=run_thread)
            thread.daemon = True
            thread.start()

        queue.join()
