#!/usr/bin/env python3

"""Runs the C solutions for each step of an optimization log. Each solution's run time is recorded
   and written to an updated log file."""

from subprocess import CalledProcessError, TimeoutExpired, check_call, check_output
from os import PathLike
from pathlib import Path
from collections import OrderedDict
from tempfile import TemporaryDirectory
from optparse import OptionParser
from optimization_log import OptimizationLog, OptimizationPreamble, OptimizationStep, RunStatus, create_python_coverage_project, parse_properties, run_solutions


DEFAULT_TIMEOUT_IN_SECS = 60


def run_python_solutions(log: OptimizationLog, time_budget: float = DEFAULT_TIMEOUT_IN_SECS, enable_compilation: bool = True):
    def compile(step: OptimizationStep, preamble: OptimizationPreamble, tmpdir: str) -> Path:
        return create_python_coverage_project(tmpdir, preamble, step, enable_compilation)

    def run(step: OptimizationStep, main_file: Path, timeout: float) -> None:
        step_output = check_output(['python3', main_file], timeout=timeout).decode('utf-8').split('\n')
        run_times = parse_properties(step_output)
        step.python_interpreter = check_output(['python3', '--version']).decode('utf-8').split('\n')[0]
        step.run_time = float(run_times['kernel'])
        step.extern_call_run_times = OrderedDict(
            (k, float(v))
            for k, v in run_times.items()
            if k != 'kernel')

    run_solutions(log, compile, run, time_budget)


def main():
    parser = OptionParser()
    parser.add_option("-t", "--time-budget", dest="timeout", default=DEFAULT_TIMEOUT_IN_SECS,
                      help="specify the time budget in seconds each solution gets", metavar="TIME_BUDGET", type='float')
    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error(f'expected exactly two arguments, got {len(args)}')

    log = OptimizationLog.parse_file(args[0])
    run_python_solutions(log, time_budget=options.timeout)
    log.write_to(args[1])


if __name__ == '__main__':
    main()
