package core

import scala.annotation.tailrec

final case class MalformedExprException(private val message: String, private val cause: Throwable = None.orNull) extends Exception(message, cause)

sealed trait CoreExpr extends Expr with CoreIR

final case class ParamDef private (t: Type, fixedId: Option[Long] = None) extends CoreExpr {
  val id: Long = fixedId.getOrElse(Counter(ParamDef.getClass).next())

  override def children: Seq[ShirIR] = Seq(t)

  override def build(newChildren: Seq[ShirIR]): ParamDef =
    new ParamDef(newChildren.head.asInstanceOf[Type], Some(id))

  override def toShortString: String = super.toShortString + "(" + id + ")"

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter@TextFormatter(renamer, _) =>
        Some(formatter.makeAtomic(s"param ${renamer.name(this)}: ${formatter.formatAtomic(t)}"))
      case formatter@CodeFormatter(renamer) => Some(s"p${renamer.name(this)}")
      case _ => None
    }
  }
}
object ParamDef {
  private def apply(t: Type, fixedId: Option[Long]): ParamDef = new ParamDef(t, fixedId)
  def apply(t: Type = UnknownType): ParamDef = new ParamDef(t)
}

final case class ParamUse private[core] (p: ParamDef) extends CoreExpr {
  override val t: Type = p.t

  override def children: Seq[ShirIR] = Seq(p)

  override def build(newChildren: Seq[ShirIR]): ParamUse = ParamUse(newChildren.head.asInstanceOf[ParamDef])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter@TextFormatter(renamer, _) =>
        Some(formatter.makeAtomic(s"use ${formatter.formatNonAtomic(p)}"))
      case _ => None
    }
  }
}

final case class Value(t: Type) extends CoreExpr {
  override def children: Seq[ShirIR] = Seq(t)

  override def build(newChildren: Seq[ShirIR]): Value = Value(newChildren.head.asInstanceOf[Type])
}

/**
  * enforce a certain output type regardless of input type
  * only use if you know what you are doing ;)
  */
final case class Conversion(input: Expr, t: Type) extends CoreExpr {
  override def children: Seq[ShirIR] = Seq(input, t)

  override def build(newChildren: Seq[ShirIR]): Conversion = Conversion(newChildren.head.asInstanceOf[Expr], newChildren(1).asInstanceOf[Type])
}

/**
  * can be used to mark a certain part of an IR
  */
final case class Marker(input: Expr, label: TextTypeT) extends CoreExpr {
  override val t: Type = input.t

  override def children: Seq[ShirIR] = Seq(input, label, t)

  override def build(newChildren: Seq[ShirIR]): Marker = Marker(newChildren.head.asInstanceOf[Expr], newChildren(1).asInstanceOf[TextTypeT])
}

sealed trait FunctionT extends CoreExpr {
  override val t: FunTypeT

  /**
    * this call applies all given arguments (one or more required) to a fun call for this instance.
    * this call is not safe, e.g. the user can provide too many arguments, which will only be detected in runtime.
    *
    * could be refactored to $ to allow calls like: ir $ (3)
    *
    * @param arg0 first argument
    * @param args arguments 2 - n (can be omitted if there is only one argument)
    * @return
    */
  def call(arg0: Expr, args: Expr*): Expr = FunctionCall(this, Seq(arg0) ++ args)
}

trait LambdaT extends FunctionT {
  val param: ParamDef
  val body: Expr
  override def children: Seq[ShirIR] = Seq(param, body, t)

  /**
   * Specifies if this lambda's body refers to the lambda's parameter exclusively by name.
   * @return `true` if the lambda's parameter is referred to by name; `false` if the parameter is referred to by some
   *         other mechanism (e.g., De Bruijn indices).
   */
  def refersToParamByName: Boolean = true

  def isIdentity: Boolean = body match {
    case ParamUse(pu) if param.id == pu.id => true
    case _ => false
  }

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter@TextFormatter(renamer, _) =>
        Some(formatter.makeAtomic(
          s"λ${renamer.name(param)}: ${formatter.formatNonAtomic(param.t)}. ${formatter.formatAtomic(body)}"))

      case _ => None
    }
  }
}

final case class Lambda private (param: ParamDef, body: Expr, t: FunTypeT) extends LambdaT {
  override def build(newChildren: Seq[ShirIR]): Lambda = new Lambda(newChildren.head.asInstanceOf[ParamDef], newChildren(1).asInstanceOf[Expr], newChildren(2).asInstanceOf[FunTypeT])
}

object Lambda {

  // hide this constructor (we do not want to specify the fun type separately)
  private def apply(param: ParamDef, initialBody: Expr, t: FunTypeT): Lambda = new Lambda(param, initialBody, t)

  private[core] def apply(param: ParamDef, body: Expr): Lambda = new Lambda(param, body, FunType(param.t, body.t))

  @tailrec
  private[core] def apply(params: Seq[ParamDef], body: Expr): Lambda = {
    if (params.isEmpty) {
      throw MalformedExprException("Lambda expression must have at least one parameter")
    } else if (params.size == 1) {
      Lambda(params.head, body)
    } else {
      Lambda(params.tail, Lambda(params.head, body))
    }
  }
}

final case class BuiltinFunction(t: FunTypeT) extends FunctionT {
  override def children: Seq[ShirIR] = Seq(t)

  override def build(newChildren: Seq[ShirIR]): BuiltinFunction = BuiltinFunction(newChildren.head.asInstanceOf[FunTypeT])
}

case class FunctionCall private(f: Expr, arg: Expr, t: Type) extends CoreExpr {
  override def children: Seq[ShirIR] = Seq(f, arg, t)

  override def build(newChildren: Seq[ShirIR]): Expr = FunctionCall(newChildren.head.asInstanceOf[Expr], newChildren(1).asInstanceOf[Expr], newChildren(2).asInstanceOf[Type])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        f match {
          case lam: LambdaT if lam.refersToParamByName =>
            Some(formatter.makeAtomic(
              s"let ${formatter.formatNonAtomic(lam.param)} = ${formatter.formatNonAtomic(arg)} in ${formatter.formatNonAtomic(lam.body)}"))
          case _ => Some(formatter.makeAtomic(s"${formatter.formatAtomic(f)} ${formatter.formatAtomic(arg)}"))
        }

      case formatter: CodeFormatter =>
        Some(s"FunctionCall(${formatter.format(f)}, ${formatter.format(arg)})")

      case _ => None
    }
  }
}

object FunctionCall {

  final def applyBetaReductionDefault = true

  @tailrec
  final private[core] def getArgument(functionCall: Expr, argPos: Int): Option[Expr] = functionCall match {
    case fc: FunctionCall =>
      if (argPos == 0) {
        Some(fc.arg)
      } else {
        FunctionCall.getArgument(fc.f, argPos - 1)
      }
    case _ => None
  }

  def apply(f: Expr, arg: Expr): Expr = FunctionCall(f, arg, applyBetaReduction = applyBetaReductionDefault)

  def apply(f: Expr, arg: Expr, applyBetaReduction: Boolean): Expr = FunctionCall(f, arg, UnknownType, applyBetaReduction)

  def apply(f: Expr, arg: Expr, t: Type, applyBetaReduction: Boolean = applyBetaReductionDefault): Expr = (applyBetaReduction, f) match {
    case (true, Lambda(param: ParamDef, body: Expr, _)) => // do not use more generic LambdaT here, because ArchLambda may be asynchronous and must not be reduced!
      body.visitAndRebuild({
        case ParamUse(pd) if pd.id == param.id => arg  // beta reduction
        case ir => ir
      }).asInstanceOf[Expr]
    case _ => new FunctionCall(f, arg, t)
  }

  def apply(f: Expr, args: Seq[Expr]): Expr = FunctionCall(f, args, applyBetaReduction = applyBetaReductionDefault)

  def apply(f: Expr, args: Seq[Expr], applyBetaReduction: Boolean): Expr = {
    if (args.isEmpty) {
      throw MalformedExprException("Function Call expression must have at least one argument")
    } else if (args.size == 1) {
      FunctionCall(f, args.head, applyBetaReduction)
    } else {
      FunctionCall(FunctionCall(f, args.tail, applyBetaReduction), args.head, applyBetaReduction)
    }
  }
}

object Let {
  def apply(param: ParamDef, body: Expr, arg: Expr): Expr = Let(param, body, arg, UnknownType)

  def apply(param: ParamDef, body: Expr, arg: Expr, t: Type): Expr = FunctionCall(Lambda(param, body), arg, t, applyBetaReduction = false)

  def apply(params: Seq[ParamDef], body: Expr, args: Seq[Expr]): Expr = {
    assert(params.nonEmpty)
    assert(params.length == args.length)
    // ugly nesting: FunctionCall(Lambda(params, body), args, applyBetaReduction = false)
    // clean nesting:
    params.length match {
      case 1 => apply(params.head, body, args.head)
      case _ => apply(params.head, apply(params.tail, body, args.tail), args.head)
    }
  }

  def unapply(expr: FunctionCall): Option[(ParamDef, Expr, Expr, Type)] = expr.f match {
    case l: LambdaT => Some(l.param, l.body, expr.arg, expr.t)
    case _ => None
  }
}

sealed trait TypeFunctionT extends CoreExpr {
  override val t: TypeFunTypeT

  /**
    * allow to instantiate a type function multiple times with different types
    * by copying the type vars
    */
  def copy(): TypeFunctionT

  /**
    * this call applies all given arguments (one or more required) to a type fun call for this instance.
    * this call is not safe, e.g. the user can provide too many arguments, which will only be detected at runtime.
    *
    * @param arg0 first argument
    * @param args arguments 2 - n (can be omitted if there is only one argument)
    * @return
    */
  def call(arg0: Expr, args: Expr*): Expr = {
    FunctionCall(
      TypeFunctionCall(
        this,
        arg0.t +: args.map(_.t)
      ),
      arg0 +: args
    )
  }
}

case class TypeLambda(body: Expr, t: TypeFunTypeT) extends TypeFunctionT {
  override def children: Seq[ShirIR] = Seq(body, t)

  override def build(newChildren: Seq[ShirIR]): TypeLambda = TypeLambda(newChildren.head.asInstanceOf[Expr], newChildren(1).asInstanceOf[TypeFunTypeT])

  override def copy(): TypeFunctionT = {
    val newTft = t.copy()
    val newTypes: Map[ShirIR, ShirIR] = t.tv.zip(newTft.tv).map(p => p._1 -> p._2).toMap
    TypeLambda(
      body.visitAndRebuild(t => if (newTypes.isDefinedAt(t)) newTypes(t) else t).asInstanceOf[Expr],
      newTft
    )
  }
}

object TypeLambda {
  def apply(typeVarParam: TypeVarT, body: Expr): TypeLambda = {
    TypeChecker.registerBoundedTypeVars(typeVarParam)
    TypeLambda(body, TypeFunType(typeVarParam, body.t))
  } // , Unknowntype))

  @tailrec
  def apply(typeVarParams: Seq[TypeVarT], body: Expr): TypeLambda = {
    if (typeVarParams.isEmpty) {
      throw MalformedExprException("Type Lambda expression must have at least one type variable")
    } else if (typeVarParams.size == 1) {
      TypeLambda(typeVarParams.head, body)
    } else {
      TypeLambda(typeVarParams.tail, TypeLambda(typeVarParams.head, body))
    }
  }
}

case class BuiltinTypeFunction(t: TypeFunTypeT) extends TypeFunctionT {
  override def children: Seq[ShirIR] = Seq(t)

  override def build(newChildren: Seq[ShirIR]): BuiltinTypeFunction = BuiltinTypeFunction(newChildren.head.asInstanceOf[TypeFunTypeT])

  override def copy(): TypeFunctionT = BuiltinTypeFunction(t.copy())
}

case class TypeFunctionCall private(tf: Expr, arg: Type, t: Type) extends CoreExpr {
  override def children: Seq[ShirIR] = Seq(tf, arg, t)

  override def build(newChildren: Seq[ShirIR]): Expr = TypeFunctionCall(newChildren.head.asInstanceOf[Expr], newChildren(1).asInstanceOf[Type], newChildren(2).asInstanceOf[Type])
}

object TypeFunctionCall {

  def apply(tf: Expr, arg: Type): Expr = TypeFunctionCall(tf, arg, UnknownType)

  def apply(tf: Expr, arg: Type, t: Type): Expr = tf match {
    // if the provided arg type is not a subtype of tft.tv or is a type var / unknown type
    // do not reduce / shortcut this expression and leave this potential problem to the TypeChecker later
    case tf: TypeFunctionT if arg.hasUnknownType || arg.isSubTypeOf(tf.t.tv).contains(false) =>
      new TypeFunctionCall(tf.copy(), arg, t)
    case TypeLambda(body: Expr, tft: TypeFunTypeT) =>
      val newTypes: Map[ShirIR, ShirIR] = tft.tv.zip(arg).map(p => p._1 -> p._2).toMap
      body.visitAndRebuild(t => if (newTypes.isDefinedAt(t)) newTypes(t) else t).asInstanceOf[Expr] // shortcut
    case BuiltinTypeFunction(tft: TypeFunTypeT) =>
      val newTypes: Map[ShirIR, ShirIR] = tft.tv.zip(arg).map(p => p._1 -> p._2).toMap
      val newTft = tft.outType.visitAndRebuild(t => if (newTypes.isDefinedAt(t)) newTypes(t) else t) // shortcut
      newTft match {
        case tft: TypeFunTypeT => BuiltinTypeFunction(tft)
        case ft: FunTypeT => BuiltinFunction(ft)
        case v: Type  => Value(v)
      }
    case _ =>
      new TypeFunctionCall(tf, arg, t)
  }

  def apply(tf: Expr, args: Seq[Type]): Expr = {
    if (args.isEmpty) {
      throw MalformedExprException("Type Function Call expression must have at least one argument")
    } else if (args.size == 1) {
      TypeFunctionCall(tf, args.head)
    } else {
      TypeFunctionCall(TypeFunctionCall(tf, args.tail), args.head)
    }
  }
}

/**
 * Represents a syntactic variable for use in expression pattern matching. Expression variables serve as a stand-in
 * for concrete expressions.
 * @param t The wildcard's type.
 * @param fixedId The wildcard's ID. IDs uniquely identify wildcards.
 */
final case class ExprVar private(t: Type, fixedId: Option[Long] = None) extends CoreExpr {
  val id: Long = fixedId.getOrElse(Counter(ParamDef.getClass).next())

  override def children: Seq[ShirIR] = Seq(t)

  override def build(newChildren: Seq[ShirIR]): ExprVar =
    new ExprVar(newChildren.head.asInstanceOf[Type], Some(id))

  override def toShortString: String = super.toShortString + "(" + id + ")"

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter@TextFormatter(renamer, _) =>
        Some(formatter.makeAtomic(s"expr ${renamer.name(this)}: ${formatter.formatAtomic(t)}"))
      case _ => None
    }
  }
}
object ExprVar {
  private def apply(t: Type, fixedId: Option[Long]): ExprVar = {
    // Type checker must know which TypeVars cannot be replaced by non-concrete types.
    TypeChecker.registerBoundedTypeVars(t)
    new ExprVar(t, fixedId)
  }

  /**
   * Creates a new wildcard expression.
   * @param t The type of the wildcard expression. Only expressions with a compatible type may match the wildcard.
   * @return A wildcard expression.
   */
  def apply(t: Type = UnknownType): ExprVar = {
    // Type checker must know which TypeVars cannot be replaced by non-concrete types.
    TypeChecker.registerBoundedTypeVars(t)
    new ExprVar(t)
  }
}
