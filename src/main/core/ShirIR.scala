package core

import lift.arithmetic.{ArithExpr, Cst, ExtensibleVar, Range, RangeUnknown, SimplifiedExpr, Var}

import java.util.concurrent.atomic.AtomicLong
import scala.annotation.tailrec
import scala.language.implicitConversions

object Type {
  implicit def aeToATT(ae: ArithExpr): ArithTypeT = ArithType(ae)

  implicit def intToATT(i: Int): ArithTypeT = ArithType(Cst(i))
}

/**
  * superclass for counter objects that provide unique identifiers
  * (used in e.g. TypeVars and Parameters)
  * call Counter.reset() after each test to reset all the counters in the entire Lift system
  */

final class Counter {
  private val cnt = new AtomicLong(0)
  private def reset(): Unit = cnt.set(0)
  def next(): Long = cnt.getAndIncrement()
  def get(): Long = cnt.get()
}
object Counter {
  private var counters: Map[Class[_], Counter] = Map()
  def apply(cl: Class[_]): Counter = {
    if (!counters.isDefinedAt(cl)) {
      val c = new Counter()
      counters = counters ++ Map(cl -> c)
    }
    counters(cl)
  }
  def resetAll(): Unit = counters.foreach(_._2.reset())
}

/**
  * the root node of all expressions and types
  */
trait ShirIR extends BuildableTreeNode[ShirIR] {
  def toShortString: String = getClass.getSimpleName.split('$').head

  override def toString: String = toShortString + (if (children.nonEmpty) "[" + children.mkString(";") + "]" else "")

  /**
    * Tries to format this IR tree using a formatter.
    * @param formatter A formatter that specifies the formatting style.
    * @return A formatted string if this IR tree wishes to intervene in the formatting process; otherwise, `None`.
    */
  def tryFormat(formatter: Formatter): Option[String] = None

  final def collectBoundTVs: Seq[TypeVarT] = {
    var tvs: Seq[TypeVarT] = Seq()
    visit({
      case tft: TypeFunTypeT => tvs = tvs :+ tft.tv
      case alt: ArithLambdaType => tvs = tvs :+ alt.v
      case _ =>
    })
    tvs.groupBy(_.tvid).map(_._2.head).toSeq
  }

  final def collectFreeTVs: Seq[TypeVarT] = {
    var tvs: Seq[TypeVarT] = Seq()
    val boundTVIDs = collectBoundTVs.flatMap(_.flatten).distinct.filter(_.isInstanceOf[TypeVarT]).map(_.asInstanceOf[TypeVarT].tvid)
    visit({
      case tv: TypeVarT if !boundTVIDs.contains(tv.tvid) => tvs = tvs :+ tv
      case _ =>
    })
    tvs.groupBy(_.tvid).map(_._2.head).toSeq
  }

  final def hasUnknownType: Boolean = collectFreeTVs.nonEmpty || _hasUnknownType

  private final def _hasUnknownType: Boolean = {
    visit({
      case UnknownType => return true
      case _ =>
    })
    false
  }

}

/**
  * the root expression
  */

trait Expr extends ShirIR {
  val t: Type

  override def toString: String = {
    toShortString + "(" +
    (if (children.nonEmpty)
      children.map{
        case e: Expr => e.toString
        case _ => ""
      }.mkString(", ")
    ) + ")"
  }

  override def build(newChildren: Seq[ShirIR]): Expr

  final def countTypeVars: Int = {
    var count = 0
    visit({
      case _: TypeVarT => count += 1
      case _ =>
    })
    count
  }
}

/**
  * an extensible node for expression
  * extend this trait if you want to add custom expressions
  */

trait BuiltinExpr extends Expr {
  val innerIR: Expr

  def types: Seq[Type]

  final def args: Seq[Expr] = _args(innerIR)
  private def _args(innerIRPart: Expr): Seq[Expr] = innerIRPart match {
    case FunctionCall(f, arg, _) => arg +: _args(f)
    case _ => Seq()
  }

  /**
   * Rebuilds this expression from a list of arguments. This method is to `args` as `build` is to `children`.
   *
   * @param newArgs A list of arguments.
   * @return An expression that is exactly like this expression, save for the arguments, which are replaced by
   *         `newArgs`.
   */
  final def buildFromArgs(newArgs: Seq[Expr]): BuiltinExpr = {
    val oldArgs = args
    if (oldArgs.length != newArgs.length) {
      throw MalformedExprException(
        s"buildFromArgs argument arity is incorrect; expected ${oldArgs.length} arguments, got ${newArgs.length}.")
    } else {
      def replaceArgs(expr: Expr, argList: Seq[Expr]): Expr = {
        expr match {
          case _ if argList.isEmpty => expr
          case FunctionCall(f, _, t) =>
            FunctionCall(replaceArgs(f, argList.tail), argList.head, t, applyBetaReduction = false)
          case _ => ???
        }
      }

      build(replaceArgs(innerIR, newArgs), types)
    }
  }

  final def replaceArgs(newArgs: Seq[Expr]): BuiltinExpr = build(_replaceArgs(newArgs, innerIR), types)
  private def _replaceArgs(newArgs: Seq[Expr], innerIRPart: Expr): Expr = innerIRPart match {
    case FunctionCall(f, _, _) => FunctionCall(_replaceArgs(newArgs.tail, f), newArgs.head)
    case e => e
  }

  def build(newInnerIR: Expr, newTypes: Seq[Type]): BuiltinExpr

  override def toString: String = this.getClass.getSimpleName + "(" + args.mkString(", ") + ")"

  final override val t: Type = innerIR.t

  final override def children: Seq[ShirIR] = Seq(innerIR) ++ types

  final override def build(newChildren: Seq[ShirIR]): BuiltinExpr = build(newChildren.head.asInstanceOf[Expr], newChildren.tail.map(_.asInstanceOf[Type]))
}

trait BuiltinExprFun {
  // represents the number of arguments required to create this builtin expressions
  def args: Int

  protected def apply(inputs: Seq[Expr], types: Seq[Type]): Expr

  final def asTypeFunction(types: Seq[Type] = Seq()): TypeLambda = {
    val paramTypes = Seq.fill(args)(AnyTypeVar())
    TypeLambda(
      paramTypes,
      {
        val paramDefs = paramTypes.map(ParamDef(_))
        val paramUses = paramDefs.map(ParamUse(_))
        Lambda(
          paramDefs,
          apply(paramUses, types)
        )
      }
    )
  }

  final def asFunction(args: Seq[Option[Expr]] = Seq.fill(this.args)(None), types: Seq[Type] = Seq())
                      (implicit buildLambda: (ParamDef, Expr) => LambdaT = Lambda(_, _)): LambdaT = {
    assert(args.length == this.args, "Must provide " + (this.args - args.length) + " more arguments (or 'None')!")
    val allArgs = args.map{
      case None => ParamUse(ParamDef(AnyTypeVar()))
      case Some(e) => e
    }
    val paramDefs = allArgs.zip(args).collect{
      case (ParamUse(pd), None) => pd
    }

    @tailrec
    def buildMultiArgLambda(params: Seq[ParamDef], body: Expr): LambdaT = {
      if (params.isEmpty) {
        throw MalformedExprException("Lambda expression must have at least one parameter")
      } else if (params.size == 1) {
        buildLambda(params.head, body)
      } else {
        buildMultiArgLambda(params.tail, buildLambda(params.head, body))
      }
    }

    buildMultiArgLambda(
      paramDefs,
      apply(allArgs, types)
    )
  }
}


/**
  * the type of a type
  */

trait Kind

/**
  * the root type
  */

trait Type extends ShirIR {
  def kind: Kind

  def superType: Type

  // restrict children to be of type TypeT (no further Expr can be found in the children)
  // remove these 3 lines the enable Types to contain further Expr
  override def children: Seq[Type]
  override def build(newChildren: Seq[ShirIR]): Type
  override def zip(l: ShirIR): Seq[(Type, Type)] = l match {
    case t: Type =>
      val commonKind = this.getCommonSuperKind(t)
      (this, t) +: this.getAsKind(commonKind).children.zip(t.getAsKind(commonKind).children).flatMap(p => p._1.zip(p._2))
    case _ => Seq()
  }

  final def getSuperKinds: Seq[Kind] = kind match {
    case AnyTypeKind => Seq(kind)
    case _ => Seq(kind) ++ superType.getSuperKinds
  }

  final def getCommonSuperKind(that: Type): Kind = {
    val thatSuperKinds = that.getSuperKinds

    for (elem <- this.getSuperKinds) {
      if (thatSuperKinds.contains(elem))
        return elem
    }

    AnyTypeKind
  }

  @tailrec
  final def getAsKind(k: Kind): Type = {
    if (k == kind) {
      this
    } else {
      superType.getAsKind(k)
    }
  }


  final def isSubTypeOf(that: Type): Option[Boolean] = {
    (this, that) match {
      // We can never tell whether a UnknownType is a sub oder super type
      case (UnknownType, _) => None
      case (_, UnknownType) => None

      // TypeVars
      case (tv1: TypeVarT, tv2: TypeVarT) if tv1 == tv2 => Some(true)

      // We can never tell whether a ConditionalTypeVar is a sub oder super type, because it depends on the conditions
      case (_: ConditionalTypeT, _) => None
      case (_, _: ConditionalTypeT) => None

      // Arith (handle these special cases first)
      case (thisTVA: TypeVarArithT, thatTVA: TypeVarArithT) =>
        (thisTVA.range.max - thatTVA.range.max, thisTVA.range.min - thatTVA.range.min) match {
          case (Cst(le), Cst(ge)) => Some(le <= 0 && ge >= 0)
          case _ => None
        }
      case (thisAT: ArithTypeT, thatTVA: TypeVarArithT) =>
        (thisAT.ae - thatTVA.range.min, thisAT.ae - thatTVA.range.max) match {
          case (Cst(ge), Cst(le)) => Some(ge >= 0 && le <= 0) // use ArithExpr.isSmaller(,) if not working
          case _ => None
        }
      case (thisTVA: TypeVarArithT, thatAT: ArithTypeT) =>
        if (thatAT.ae == thisTVA.range.min && thatAT.ae == thisTVA.range.max)
          Some(true)
        else
          None
      case (thisAT: ArithTypeT, thatAT: ArithTypeT) =>
        // If both ArithTypes are the same, perhaps we do not need to care about their evaluability?
        if ((thisAT.ae.isEvaluable && thatAT.ae.isEvaluable) || thisAT.ae == thatAT.ae)
          Some(thisAT.ae == thatAT.ae)
        else
          None
      case (_: TypeVarArithT, _) => None
      //case (_, _ : TypeVarArithT) => ??? // TODO missing?

      // Fun
      // special case: functions are contravariant over its argument type, and covariant over its return type
      // do not use FunTypeT here. We just want to compare functions, when they have climbed the type tree up until they became FunType
      case (thisFt: FunType, thatFt: FunType) =>
        (thatFt.inType.isSubTypeOf(thisFt.inType), thisFt.outType.isSubTypeOf(thatFt.outType)) match {
          case (Some(true), Some(true)) => Some(true)
          case (Some(false), _) | (_, Some(false)) => Some(false)
          case (_, _) => None
        }

      case (_: TypeVarT, _) if this.kind == AnyTypeKind => None // if this is AnyTypeVar we cannot say anything
      case (_, _: TypeVarT) if that.kind == AnyTypeKind => None // if that is AnyTypeVar we cannot say anything
      case (_, tv: TypeVarGenT) =>
        if (this.isSubTypeOf(tv.upperBound) == Option(false))
          Some(false)
        else
          None
      case (tv: TypeVarGenT, _) =>
        if (tv.upperBound.isSubTypeOf(that) == Option(true))
          Some(true)
        else
          None

      // general cases
      case (_, _) if this.kind == that.kind =>
        if (this.children.zip(that.children).forall(p => p._1.isSubTypeOf(p._2).getOrElse(false)))
          Some(true)
        else if (this.children.zip(that.children).forall(p => p._1.isSubTypeOf(p._2).getOrElse(true)))
          None
        else
          Some(false)
      case (_, _) if that.kind == AnyTypeKind => Some(true)
      case (_, _) if this.kind == AnyTypeKind => Some(false)

      // else
      case (_, _) =>
        this.superType.isSubTypeOf(that)
    }
  }
}

case object AnyTypeKind extends Kind

sealed trait AnyTypeT extends Type {
  override def kind: Kind = AnyTypeKind

  override def superType: Type = AnyType()

  override def children: Seq[Type] = Seq()
}

case object UnknownType extends AnyTypeT {
  override def build(newChildren: Seq[ShirIR]): Type = this

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case TextFormatter(_, _) => Some("unknown")
      case _ => None
    }
  }
}

final case class AnyType() extends AnyTypeT {
  override def build(newChildren: Seq[ShirIR]): AnyTypeT = this
}

final case class AnyTypeVar(tvFixedId: Option[Long] = None) extends AnyTypeT with TypeVarT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = AnyTypeVar(tvFixedId)
}

sealed trait TypeVarT extends Type {
  // must not override the kind

  override final def build(newChildren: Seq[ShirIR]): Type = buildTV(newChildren, Some(tvid))

  def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long] = None): Type

  final def newTV(tvFixedId: Option[Long] = None): TypeVarT = buildTV(children.map({
    case tv: TypeVarT => tv.newTV()
    case t => t
  }), tvFixedId).asInstanceOf[TypeVarT]

  def tvFixedId: Option[Long]
  val tvid: Long = tvFixedId.getOrElse(Counter(AnyTypeVar.getClass).next())

  override def equals(that: Any): Boolean =
    that match {
      case thatTV: TypeVarT => this.tvid == thatTV.tvid
      case _ => false
    }

  override def hashCode(): Int = 8 * 79 + tvid.hashCode // need to be the same hash function as in ArithExpr Var class

  override def toShortString: String = super.toShortString + "(" + tvid + ")"
}

trait TypeVarGenT extends TypeVarT {
  def upperBound: Type
}

trait TypeVarArithT extends TypeVarT {
  def range: Range
}

trait ValueTypeT extends AnyTypeT

/**
  * A value type that is defined by its children only.
  * If the children of this type are known, the type itself is known.
  */
trait ComposedValueTypeT extends ValueTypeT

trait MetaTypeT extends AnyTypeT

case object ConditionalTypeKind extends Kind

sealed trait ConditionalTypeT extends AnyTypeT {
  def inputType: Type

  override def kind: Kind = ConditionalTypeKind

  override def superType: Type = AnyType()

  override def children: Seq[Type] = Seq(inputType)
}

final case class ConditionalTypeVar(inputType: Type, conditionalSubstitutions: PartialFunction[Type, Type], tvFixedId: Option[Long] = None) extends ConditionalTypeT with TypeVarT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): Type = {
    val newInputType = newChildren.head.asInstanceOf[Type]
    conditionalSubstitutions.applyOrElse[Type, Type](newInputType, _ => ConditionalTypeVar(newInputType, conditionalSubstitutions, tvFixedId))
  }
}

case object IdTypeKind extends Kind

sealed trait IdTypeT extends MetaTypeT {
  val id: Option[Long]

  override def kind: Kind = IdTypeKind

  override def superType: Type = AnyType()

  override def toShortString: String = id match {
    case Some(i) => super.toShortString + "(" + i + ")"
    case None => super.toShortString
  }
}

final case class IdType(id: Option[Long] = Some(Counter(IdType.getClass).next())) extends IdTypeT {
  override def build(newChildren: Seq[ShirIR]): Type = this
}

final case class IdTypeVar(tvFixedId: Option[Long] = None) extends IdTypeT with TypeVarGenT {
  override val id: Option[Long] = None

  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = IdTypeVar(tvFixedId)

  override def upperBound: MetaTypeT = IdType(id)
}

case object TextTypeKind extends Kind

sealed trait TextTypeT extends MetaTypeT {
  val s: String

  override def kind: Kind = TextTypeKind

  override def superType: Type = AnyType()

  override def toShortString: String = super.toShortString + "(\"" + s + "\")"
}

final case class TextType(s: String) extends TextTypeT {
  override def build(newChildren: Seq[ShirIR]): Type = this

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case _: TextFormatter => Some("\"" + s + "\"")
      case _: CodeFormatter => Some("\"" + s + "\"")
      case _ => None
    }
  }
}

case object ArithTypeKind extends Kind

sealed trait ArithTypeT extends MetaTypeT {
  val ae: ArithExpr

  final def evalBool: Boolean = ae.evalInt > 0

  override def kind: Kind = ArithTypeKind

  override def superType: Type = AnyType()

  // filter '_ != this' to avoid infinite recursion (and stack overflow exceptions)
  override def children: Seq[TypeVarT] = ae.varList.filter(_ != this).filter(_.isInstanceOf[TypeVarArithT]).map(_.asInstanceOf[TypeVarArithT])
}

final case class ArithType(ae: ArithExpr) extends ArithTypeT {
  override def build(newChildren: Seq[ShirIR]): ArithTypeT = ArithType(
    ae.visitAndRebuild({
      case at: ArithTypeT if children.contains(at) => newChildren(children.indexOf(at)).asInstanceOf[ArithTypeT].ae
      case at => at
    })
  )

  override def map(f: ShirIR => ShirIR): ArithTypeT = ArithType(
    ae.visitAndRebuild({
      case at: ArithTypeVar => f(at).asInstanceOf[ArithTypeT].ae
      case at => at
    })
  )

  override def toShortString: String = super.toShortString + "(" + ae.toString + ")"

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case _: TextFormatter => Some(ae.toString)
      case _: CodeFormatter => Some(ae.toString)
      case _ => None
    }
  }
}

sealed case class ArithTypeVar(override val range: Range = RangeUnknown, tvFixedId: Option[Long] = None, arithFixedId: Option[Long] = None) extends ExtensibleVar("", range, arithFixedId) with ArithTypeT with TypeVarArithT {
  override def cloneSimplified(): Var with SimplifiedExpr = new ArithTypeVar(range, Some(tvid), Some(id)) with SimplifiedExpr

  override val tvid: Long = tvFixedId.getOrElse(Counter(AnyTypeVar.getClass).next())

  override val ae: ArithExpr = this

  override def children: Seq[TypeVarT] = this.varList.filter(_ != this).filter(_.isInstanceOf[TypeVarArithT]).map(_.asInstanceOf[TypeVarArithT])

  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarArithT = {
    val arithFixedId: Option[Long] = tvFixedId match {
      case Some(tvid1) if tvid1 == tvid => Some(id)
      case _ => None
    }
    new ArithTypeVar(range = this.range, tvFixedId, arithFixedId) {
      override val ae: ArithExpr = this.visitAndRebuild({
        case at: ArithTypeT if children.contains(at) => newChildren(children.indexOf(at)).asInstanceOf[ArithTypeT].ae
        case at => at
      })
    }
  }

  override lazy val hashCode: Int = super[TypeVarArithT].hashCode()
  override lazy val toString: String = super[TypeVarArithT].toString()

  override def copy(r: Range): ArithTypeVar = ArithTypeVar(r, None, Some(id))

  override def visitAndRebuild(f: ArithExpr => ArithExpr): ArithExpr = f(ArithTypeVar(range.visitAndRebuild(f), Some(this.tvid), Some(id)))
}

case object ArithLambdaTypeKind extends Kind

// v -> (v+1)%len
// val v = Var()
// ArithLmbdaType(v, (v+1)%len)

case class ArithLambdaType (v: ArithTypeVar, b: ArithTypeT) extends MetaTypeT{
  //def call(arg: ArithExpr) = ArithExpr.substitute(this.b.ae, Map(this.v.ae -> arg))
  def call(arg: ArithType): ArithType = ArithType(ArithExpr.substitute(this.b.ae, Map(this.v.ae -> arg.ae)))

  override def build(newChildren: Seq[ShirIR]): Type = newChildren.head match {
    // only replace tv, if the new child is a type var too
    case atv: ArithTypeVar => ArithLambdaType(atv, newChildren(1).asInstanceOf[ArithTypeT])
    case _ => ArithLambdaType(v, newChildren(1).asInstanceOf[ArithTypeT])
  }

  override def kind: Kind = ArithLambdaTypeKind

  override def superType: Type = AnyType()

  override def children: Seq[Type] = Seq(v, b)

}

case object BitVectorTypeKind extends Kind

final case class BitVectorType(s: scala.collection.immutable.BitSet, w: Int) extends MetaTypeT {
  override def build(newChildren: Seq[ShirIR]): Type = this
  override def kind: Kind = BitVectorTypeKind
  override def children: Seq[Type] = Seq()

  /**
   * @return MSB first, LSB last
   */
  def toArithBits: Seq[ArithTypeT] =
    Seq.range(0, w).reverse.view.map(i => ArithType(if (s(i)) 1 else 0))

  /**
   * @return MSB first, LSB last
   */
  def toBoolSeq: Seq[Boolean] =
    Seq.range(0, w).reverse.view.map(s(_))
}

/**
  * an extensible node for types
  * extend this trait if you want to add custom types
  */

case object BuiltinDataTypeKind extends Kind

trait BuiltinDataTypeT extends ValueTypeT {
  override def kind: Kind = BuiltinDataTypeKind

  override def superType: Type = AnyType()
}

final case class BuiltinDataType() extends BuiltinDataTypeT {
  override def build(newChildren: Seq[ShirIR]): BuiltinDataTypeT = this
}

final case class BuiltinDataTypeVar(tvFixedId: Option[Long] = None) extends BuiltinDataTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = BuiltinDataTypeVar(tvFixedId)

  override def upperBound: BuiltinDataTypeT = BuiltinDataType()
}
