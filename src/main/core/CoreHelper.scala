package core

import lift.arithmetic.RangeUnknown

object CoreHelper {
  /**
   * A helper that check if an ArithExpr is an ArithTypeVar with RangeUnknown.
   */
  def isRangeUnknown(a: ArithTypeT): Boolean = a match {
    case ArithTypeVar(RangeUnknown, _, _) => true
    case _ => false
  }
}
