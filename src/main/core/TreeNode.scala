package core

import core.util.Log
/**
 * Describes a tree node's kind. A node kind contains all the information in a tree node except for its children.
 * Kinds can be compared for equality and two equal expressions will always have equal kinds.
 */
trait TreeNodeKind
private case class DefaultTreeNodeKind(c: Class[_], numberOfChildren: Int) extends TreeNodeKind

private case class Zipper[+T](old: T, uplink: Zipper[T], hd: List[T], tl: Seq[T])

trait TreeNode[T <: TreeNode[T]] {
  def children: Seq[T]

  /**
   * Gets this tree node's kind.
   * @return A tree node kind.
   */
  def nodeKind: TreeNodeKind = {
    DefaultTreeNodeKind(this.getClass, children.length)
  }

  final def foreach(f: T => Unit): Unit = {
    children.foreach(f(_))
  }

  def zip(t: T): Seq[(T, T)] = (this.asInstanceOf[T], t) +: children.zip(t.children).flatMap(p => p._1.zip(p._2))

  final def flatten: Seq[T] = this.asInstanceOf[T] +: children.flatMap(_.flatten)

  private final def visit(pre: T => Unit, post: T => Unit): Unit = {
    @scala.annotation.tailrec
    def go(uplink: Zipper[T], t0: T, descending: Boolean): Unit = {
      if (descending) {
        pre(t0)
        val children = t0.children
        if (children.isEmpty)
          go(uplink, t0, descending = false)
        else
          go(Zipper(t0, uplink, Nil, children.tail), children.head, descending = true)
      } else {
        post(t0)
        uplink match {
          case null => ()
          case Zipper(old, uplink, _, tl) =>
            if (tl.isEmpty)
              go(uplink, old, descending = false)
            else
              go(Zipper(old, uplink, Nil, tl.tail), tl.head, descending = true)
        }
      }
    }

    go(null, this.asInstanceOf[T], descending = true)
  }

  final def visit(pre: T => Unit): Unit = visit(pre, t => Unit)

  /**
   * Tests whether a predicate holds for some subtree of this tree node.
   * @param pred A predicate.
   * @return True if the predicate holds for some subtree of this tree node; otherwise, false.
   */
  final def contains(pred: T => Boolean): Boolean = {
    visit {
      case u if pred(u) => return true
      case _ =>
    }
    false
  }
}

trait BuildableTreeNode[T <: BuildableTreeNode[T]] extends TreeNode[T] {
  def build(newChildren: Seq[T]): T

  def map(f: T => T): T = {
    build(children.map(f(_)))
  }

  final def visitAndRebuild(pre: T => T, post: T => T): T = {
    @scala.annotation.tailrec
    def go(uplink: Zipper[T], t0: T, descending: Boolean): T = {
      if (descending) {
        val t1 = pre(t0)
        val children = t1.children
        if (children.isEmpty)
          go(uplink, t1, descending = false)
        else
          go(Zipper(t1, uplink, Nil, children.tail), children.head, descending = true)
      } else {
        val t1 = post(t0)
        uplink match {
          case null => t1
          case Zipper(old, uplink, hd, tl) =>
            if (tl.isEmpty)
              go(uplink, old.build((t1 :: hd).reverse), descending = false)
            else
              go(Zipper(old, uplink, t1 :: hd, tl.tail), tl.head, descending = true)
        }
      }
    }

    go(null, this.asInstanceOf[T], descending = true)
  }

  final def visitAndMaybeRebuild(pre: T => T, post: T => T): T = {
    var childrenHaveChanged = false
    val newChildren = collection.mutable.ListBuffer[T]()
    for (child <- children) {
      val possibleNewChild = child.visitAndMaybeRebuild(pre, post)
      childrenHaveChanged |= !possibleNewChild.eq(child)
      newChildren += possibleNewChild
    }
    post(pre(
      if (childrenHaveChanged) this.build(newChildren.toList)
      else this.asInstanceOf[T]))
  }

  final def visitAndRebuild(post: T => T): T = {
    val test = visitAndRebuild(t => t, post)
    test
  }

  final def visitAndMaybeRebuild(post: T => T): T = {
    val test = visitAndMaybeRebuild(t => t, post)
    test
  }
}
