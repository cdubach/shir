package core

import scala.collection.mutable

/**
  * The style in which an IR tree is formatted.
  */
trait FormattingStyle

/**
  * A formatting style that prints IR trees as human-readable text.
  */
object TextFormattingStyle extends FormattingStyle

/**
 * A formatting style that prints IR trees as Scala code.
 */
object ScalaCodeFormattingStyle extends FormattingStyle

/**
  * A common interface to IR tree formatting implementations.
  */
trait Formatter {
  /**
    * The formatter's style.
    * @return A formatting style.
    */
  def style: FormattingStyle

  /**
    * Formats an IR tree.
    * @param tree The tree to format.
    * @return A string representing the formatted tree.
    */
  def format(tree: ShirIR): String
}

/**
  * A tool that automatically assigns new names to variables. These names are generated according to a local numbering
  * scheme, which is more predictable than the global numbering scheme.
  * @param renameVariables Specifies whether this formatter actually renames variables. If set to `false`, this renamer
  *                        returns global variable numbers rather than freshly-assigned local numbers.
  */
final class VariableRenamer(renameVariables: Boolean = true) {
  private val paramRenaming = mutable.Map[Long, Long]()
  private val exprVarRenaming = mutable.Map[Long, Long]()

  private def name(id: Long, renamingMap: mutable.Map[Long, Long]): Long = {
    if (renameVariables)
      renamingMap.getOrElseUpdate(id, renamingMap.size)
    else
      id
  }

  def name(p: ParamDef): String = name(p.id, paramRenaming).toString

  def name(v: ExprVar): String = name(v.id, exprVarRenaming).toString
}

/**
  * A formatter that prints IR trees as human-readable text.
  * @param renamer The variable renamer to use.
  * @param atomicResults Specifies whether calls to `format` will produce atomic results. This atomicity is enforced
  *                      by parenthesizing expressions where necessary.
  */
final case class TextFormatter(renamer: VariableRenamer = new VariableRenamer(false),
                               atomicResults: Boolean = false) extends Formatter {
  /**
    * The formatter's style.
    *
    * @return A formatting style.
    */
  override def style: FormattingStyle = TextFormattingStyle

  /**
    * Formats an IR tree.
    * @param tree The tree to format.
    * @return A string representing the formatted tree.
    */
  override def format(tree: ShirIR): String = {
    tree.tryFormat(this) match {
      case Some(specialized) => specialized
      case None =>
        tree.getClass.getSimpleName.split('$').head +
          (if (tree.children.nonEmpty) "[" + tree.children.map(format).mkString(";") + "]" else "")
    }
  }

  def makeAtomic(str: String): String = if (atomicResults) s"($str)" else str

  def formatAtomic(tree: ShirIR): String = TextFormatter(renamer, atomicResults = true).format(tree)
  def formatNonAtomic(tree: ShirIR): String = TextFormatter(renamer, atomicResults = false).format(tree)
}

/**
 * A formatter that prints IR trees as Scala code.
 * @param renamer The variable renamer to use.
 */
final case class CodeFormatter(renamer: VariableRenamer = new VariableRenamer(false)) extends Formatter {
  override def style: FormattingStyle = ScalaCodeFormattingStyle

  override def format(tree: ShirIR): String = {
    tree.tryFormat(this) match {
      case Some(specialized) => specialized
      case None =>
        val className = tree.getClass.getSimpleName.split('$').head
        val constructorName = if (className.endsWith("Expr"))
          className.dropRight(4)
        else
          className

        val children = tree match {
          case tree: BuiltinExpr => tree.args ++ tree.types
          case _ => tree.children
        }
        constructorName + (if (children.nonEmpty) "(" + children.map(format).mkString(", ") + ")" else "")
    }
  }
}
