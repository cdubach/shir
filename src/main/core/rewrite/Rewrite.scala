package core.rewrite

import core.util.Log
import core.{Expr, ShirIR, TypeChecker}

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.Random

case class RewriteStep(mode: RewriteMode, rules: Seq[Rule]) {
  def apply(expr: Expr): Expr = mode.rewrite(rules, expr)
}

sealed trait RewriteMode {

  def rewrite(rules: Seq[Rule], expr: Expr): Expr

  @tailrec
  final def rewriteSets(ruleSets: Seq[Seq[Rule]], expr: Expr): Expr = ruleSets.length match {
    case 1 => rewrite(ruleSets.head, expr)
    case _ => rewriteSets(ruleSets.tail, rewrite(ruleSets.head, expr))
  }

  protected final def collectPossibleRewrites(expr: Expr, rules: Seq[Rule]): Seq[(Expr, Rule)] = {
    rules.filter(_.isDefinedAt(expr)).map((expr, _)) ++
      expr.children.flatMap {
        case e: Expr => collectPossibleRewrites(e, rules)
        case _ => Seq()
      }
  }

  protected final def getPossibleRewrite(expr: Expr, rules: Seq[Rule]): Option[(Expr, Rule)] = {
    rules.foreach(rule =>
      if (rule.isDefinedAt(expr))
        return Some(expr, rule)
    )
    expr.children.foreach {
      case e: Expr =>
        val rw = getPossibleRewrite(e, rules)
        if (rw.isDefined)
          return rw
      case _ =>
    }
    None
  }

  protected final def applyRule(expr: Expr, toBeRewritten: Expr, rule: Rule): Expr = {
    if (expr == toBeRewritten) {
      Log.info(this, "apply rewrite rule " + rule.desc)
      // IRDotGraph(expr).show()//toFile("rewrite_expr_before.dot")
      val rewrittenExpr = TypeChecker.check(rule.rewrite.apply(TypeChecker.check(toBeRewritten)))
      // IRDotGraph(rewrittenExpr).show()//toFile("rewrite_expr_after.dot")
      rewrittenExpr
    } else {
      expr.build(expr.children.map {
        case e: Expr => applyRule(e, toBeRewritten, rule)
        case t: ShirIR => t
      })
    }
  }

}

final case class RewriteAll() extends RewriteMode {
  /*@tailrec
  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = {
    val rewriteOptions = getPossibleRewrite(expr, rules)
    rewriteOptions match {
      case None => expr
      case Some((toBeReplaced, rewriteRule)) =>
        val rewrittenExpr = applyRule(expr, toBeReplaced, rewriteRule)
        // IRDotGraph(rewrittenExpr).show()
        rewrite(rules, rewrittenExpr)
    }
  }*/

  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = {
    val cache = new java.util.IdentityHashMap[Expr, Unit]()
    rewriteEfficientTraversal(rules, expr, cache)
  }

  /**
   * Use foldLeft built-in to apply rewrite rules to the current root only.
   * Keep rewrite the expression until nothing can be changed.
   *
   * @param rules The set of rewrite rules.
   * @param expr  The input expression.
   * @return The rewritten expression.
   */
  @tailrec
  private def applyFixePointOnNode(rules: Seq[Rule], expr: Expr): Expr = {
    val possibleNewExprNew: Expr = rules.foldLeft(expr)((e, r) => {
      val possibleNewExpr = r.rewrite.applyOrElse[Expr, Expr](e, e => e)
      if (possibleNewExpr.eq(e))
        e
      else {
        Log.info(this, "apply rewrite rule " + r.desc)
        TypeChecker.check(possibleNewExpr)
      }
    })

    if (possibleNewExprNew.eq(expr))
      expr
    else
      applyFixePointOnNode(rules, possibleNewExprNew)
  }

  /**
   * A more efficient way to apply rewriting. First apply rules to the root (Call applyFixePointOnNode). If nothing can
   * be changed, move to the children. If the entire subtree cannot be rewritten, move to the parent. A cache is used
   * for tracking if the paths are visited or not. If the subtree has been visited, register it in the cache.
   *
   * @param rules The set of rewrite rules.
   * @param expr  The input expression.
   * @param cache The cache used to avoid revisiting subtrees
   * @return The rewritten expression.
   */
  private def rewriteEfficientTraversal(rules: Seq[Rule], expr: Expr, cache: java.util.IdentityHashMap[Expr, Unit]): Expr = {
    var possibleNewExpr = applyFixePointOnNode(rules, expr)
    var shouldVisitChildren = true
    while (shouldVisitChildren) {
      var childrenHaveChanged = false
      val newChildren = collection.mutable.ListBuffer[ShirIR]()
      for (child <- possibleNewExpr.children) {
        newChildren += (child match {
          case child: Expr if !cache.containsKey(child) =>
            val possibleNewChild = rewriteEfficientTraversal(rules, child, cache)
            childrenHaveChanged |= possibleNewChild.ne(child)
            possibleNewChild
          case e => e
        })
      }

      if (!childrenHaveChanged)
        // in this case, the fixed point computed at the start of this method
        // IS the fixed point of this expression, so stop visiting the children.
        shouldVisitChildren = false
      else {
        // in this case, by induction, the new children are at a fixed point,
        // but the current node might not be (the children have changed after all).
        //
        // only re-visit the children if it turns out the current node truly is NOT
        // at a fixed point. (assume any modifications imply not fixed point)
        val expr1 = possibleNewExpr.build(newChildren.toList)
        possibleNewExpr = applyFixePointOnNode(rules, expr1)
        shouldVisitChildren = possibleNewExpr.ne(expr1)
      }
    }

    // at this point, our (modified) node can no longer change,
    // so cache it and return it
    cache.put(possibleNewExpr, ())
    possibleNewExpr
  }
}

/**
 * Randomly rewrites the expression by applying rules to the expression.
 * @param iterations the maximum number of rewrites.
 * @param exclude a function to filter out unwanted expressions when randomly rewriting;
 *                if the function returns true, the expr will be excluded from the rewriting.
 */
final case class RewriteRandom(iterations: Int, exclude: Expr => Boolean = _ => false) extends RewriteMode {
  private final val random = new Random(1)

  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = _rewrite(rules, expr)

  @tailrec
  private def _rewrite(rules: Seq[Rule], expr: Expr, iterations: Int = this.iterations): Expr = {
    val rewriteOptions = collectPossibleRewrites(expr, rules)
    if (iterations == 0 || rewriteOptions.isEmpty) {
      expr
    } else {
      val optionId = random.nextInt(rewriteOptions.length)
      Log.info(this, "random rewriting (option " + (optionId + 1) + " of " + rewriteOptions.length + ") ...")
      val (toBeReplaced, rewriteRule) = rewriteOptions(optionId)
      val rewrittenExpr = applyRule(expr, toBeReplaced, rewriteRule)
      if (exclude(rewrittenExpr)) _rewrite(rules, expr, iterations - 1)
      else _rewrite(rules, rewrittenExpr, iterations - 1)
    }
  }
}

final case class RewriteTargeted(targetIds: Int*) extends RewriteMode {

  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = _rewrite(rules, expr)

  private def _rewrite(rules: Seq[Rule], expr: Expr, targetIds: Seq[Int] = this.targetIds): Expr = targetIds.length match {
    case 1 =>
      val rewriteOptions = collectPossibleRewrites(expr, rules)
      if (rewriteOptions.size <= targetIds.head)
        throw new Exception("Cannot apply targeted rewrite. No expressions found to apply rule!")
      val (toBeReplaced, rewriteRule) = rewriteOptions(targetIds.head)
      applyRule(expr, toBeReplaced, rewriteRule)
    case _ => _rewrite(rules, _rewrite(rules, expr, Seq(targetIds.head)), targetIds.tail)
  }

}

final case class RewriteOptimised(iterations: Int = 1, performanceModel: Expr => Int) extends RewriteMode {

  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = _rewrite(rules, expr)

  @tailrec
  private def _rewrite(rules: Seq[Rule], expr: Expr, iterations: Int = this.iterations): Expr = {
    val rewriteOptions = collectPossibleRewrites(expr, rules)
    if (iterations == 0 || rewriteOptions.isEmpty) {
      expr
    } else {
      Log.info(this, "optimised rewriting ...")
      val currentResult = (expr, performanceModel(expr))
      val newResults: Seq[(Expr, Int)] = rewriteOptions.map(e => applyRule(expr, e._1, e._2)).map(e => (e, performanceModel(e)))
      // find best result with optimal value
      val best = (newResults :+ currentResult).maxBy(_._2)
      // if our currentResult is still the best, stop rewriting and return current expression
      if (best == currentResult) {
        expr
      } else {
        _rewrite(rules, best._1, iterations - 1)
      }
    }
  }
}

/**It rewrites applied the rules to exhaustively explore the whole search space,
 *  and returns all possible variants
 * @param filter: filters out the potentially unwanted variants to narrow down the search space
 */
final case class RewriteExhaustively(val filter: Seq[Expr] => Seq[Expr]) extends RewriteMode {
  override def rewrite(rules: Seq[Rule], expr: Expr): Expr = ???

  def rewrite(rules: Seq[Rule], exprs: Seq[Expr]): Seq[Expr] = filter(_rewrite(rules, exprs, Seq()))

  @tailrec
  private def _rewrite(rules: Seq[Rule], exprs: Seq[Expr], result: Seq[Expr]): Seq[Expr] = {
    Log.warn(this, exprs.length.toString)
    val _exprs = filter(exprs)
    val exprsRewrites = _exprs.map(collectPossibleRewrites(_, rules))
    if (exprsRewrites.isEmpty) result
    else _rewrite(rules, _exprs.zip(exprsRewrites).flatMap({ e => e._2.map(ops => applyRule(e._1, ops._1, ops._2))}), _exprs ++ result)
  }
}
