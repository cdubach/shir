package core.util

import core.Counter

import scala.annotation.tailrec
import scala.sys.process._

sealed trait DotGraphComponent

final case class DotGraph(graph: DotSubGraph, attributes: Map[String, String] = Map.empty)
  extends DotGraphComponent with ToFile with DotAttributes {

  val DOT_FILE_EXTENSION: String = "dot"
  val DOT_VIEWER: String = "xdot"

  override def toStringIterator: Iterator[String] =
    (
      Seq("digraph G {") ++
        attributesSeq.map("\t" + _ + ";") ++
        graph.toStrings.map("\t" + _) ++
        Seq("}")
      ).iterator

  def show(): Unit = Process(DOT_VIEWER + " " + toFile("tmp." + DOT_FILE_EXTENSION)).!
}

final case class DotSubGraph(nodes: Seq[DotNode] = Seq(), connections: Seq[DotConnection] = Seq()) extends DotGraphComponent {
  def toStrings: Seq[String] = nodes.flatMap(_.toStrings) ++ connections.map(_.toString)

  @tailrec
  def add(c: DotGraphComponent): DotSubGraph = c match {
    case DotSubGraph(newNodes, newConnections) =>
      DotSubGraph(nodes ++ newNodes, connections ++ newConnections)
    case node: DotNode =>
      DotSubGraph(nodes :+ node, connections)
    case con: DotConnection =>
      DotSubGraph(nodes, connections :+ con)
    case DotGraph(sub, _) => add(sub)
  }

  def addAll(c: Seq[DotGraphComponent]): DotSubGraph = c.foldLeft(this)((g1, g2) => g1.add(g2))

  def connectAsRootNode(root: DotNode, style: String = "", enumerateConnections: Boolean = true): DotSubGraph = {
    val oldRoots: Seq[DotNode] = nodes.filter(n => !connections.exists(_.destination == n.id))

    val newConnections: Seq[DotConnection] = if (enumerateConnections && oldRoots.length > 1)
      oldRoots.zipWithIndex.map(n => DotConnection(root, n._1, (n._2 + 1) + "", style))
    else {
      oldRoots.map(n => DotConnection(root, n, "", style))
    }
    this.addAll(root +: newConnections)
  }
}

object DotSubGraph {
  def apply(c: Seq[DotGraphComponent]): DotSubGraph = DotSubGraph().addAll(c)
}

object DotColor {
  final def HIGHLIGHT: String = BLUE
  final val BLUE: String = "blue"
}

sealed trait DotAttributes {
  def attributes: Map[String, String]

  def attributesSeq: Seq[String] = {
    def escapeStr(s: String): String = s.replace("\"", "\\\"")
    attributes.filter(p => p._2.nonEmpty).map(p => {
      if (p._1 == "label" || p._1 == "tooltip")
        p._1 + "=\"" + escapeStr(p._2) + "\""
      else
        p._1 + "=" + p._2
    }).toSeq
  }

  def attributesString: String = {
    val str = attributesSeq.mkString(", ")
    if (str.isEmpty)
      ""
    else
      "[" + str + "]"
  }
}

final case class DotNode(label: String, shape: String = "", style: String = "", color: String = "", tooltip: String = "", subGraph: Option[DotSubGraph] = None, id: String = "node_" + Counter(DotNode.getClass).next()) extends DotGraphComponent with DotAttributes {
  override def attributes: Map[String, String] = Map("label" -> label, "shape" -> shape, "style" -> style, "color" -> color, "tooltip" -> tooltip)

  def toStrings: Seq[String] = subGraph match {
    case None => Seq(id + attributesString + ";")
    case Some(g) =>
      Seq("subgraph cluster_" + id + " {") ++ (attributesSeq.map(_ + ";") ++ g.toStrings).map("\t" + _) ++ Seq("}")
  }

  override def toString: String = toStrings.mkString("\n")
}
object DotNode {
  final val RECTANGLE_SHAPE = "rectangle"
  final val SEPTAGON_SHAPE = "septagon"
  final val TRIPLE_OCTAGON_SHAPE = "tripleoctagon"
  final val FILLED_STYLE = "filled"
}

final case class DotConnection(source: String,
                               destination: String,
                               label: String,
                               style: String,
                               lhead: String,
                               ltail: String) extends DotGraphComponent with DotAttributes {
  override def attributes: Map[String, String] =
    Map("label" -> label, "style" -> style, "lhead" -> lhead, "ltail" -> ltail)

  override def toString: String = source + " -> " + destination + attributesString
}
object DotConnection {
  final val DOTTED_STYLE = "dotted"
  def apply(source: DotNode,
            destination: DotNode,
            label: String = "",
            style: String = "",
            lhead: Option[DotNode] = None,
            ltail: Option[DotNode] = None): DotConnection = {

    def unwrapCluster(value: Option[DotNode]) = value match {
      case Some(x) => s"cluster_${x.id}"
      case None => ""
    }
    DotConnection(
      source.id,
      destination.id,
      label,
      style,
      unwrapCluster(lhead),
      unwrapCluster(ltail))
  }
}