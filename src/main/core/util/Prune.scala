package core.util

import core.{BuiltinExpr, Expr, ShirIR, TypeChecker}

/**
 * Prunes an IR tree for as long as it satisfies an arbitrary criterion. This is helpful for test case reduction.
 */
object Prune {
  def apply(ir: ShirIR, predicate: ShirIR => Boolean): Option[ShirIR] = {
    Some(ir).filter(predicate).map(ir => {
      // We know the top-level tree satisfies the criterion. Can we find a child that also satisfies it?
      val children = ir match {
        case expr: BuiltinExpr => expr.args ++ expr.types
        case _ => ir.children
      }

      for (arg <- children) {
        val better = Prune(arg, predicate)
        if (better.isDefined) {
          return better
        }
      }
      ir
    })
  }

  /**
   * Prunes `expr` to produce a smaller tree that still produces a type error, if `expr` produces one in the first
   * place.
   * @param expr An expression to prune.
   * @return A subtree of `expr` that produces a type error, if there is one; otherwise, `None`.
   */
  def findTypeError(expr: Expr): Option[Expr] = {
    Prune(expr, {
      case expr: Expr =>
        try {
          TypeChecker.check(expr)
          false
        } catch {
          case _: Throwable => true
        }
      case _ =>
        false
    }).map(_.asInstanceOf[Expr])
  }

  /**
   * Prunes `expr` to produce a smaller tree that still contains an unsolved type variable after running type checking,
   * if `expr` produces one in the first place.
   * @param expr An expression to prune.
   * @return A subtree of `expr` that contains an unsolved type variable, if there is such a subtree; otherwise,
   *         `None`.
   */
  def findUnsolvedTypeVar(expr: Expr): Option[Expr] = {
    Prune(expr, {
      case expr: Expr =>
        TypeChecker.solveTypeVars(expr).values.exists(_.hasUnknownType)

      case _ => false
    }).map(_.asInstanceOf[Expr])
  }
}
