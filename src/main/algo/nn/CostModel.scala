package algo.nn

import eqsat.OutliningExtractor

/**
 * A cost model for the outlining process. The cost model is used to determine the cost of an expression, which is
 * used to guide the outlining process. The cost model also determines if a layer will be lowered within the equality
 * saturation process.
 */
trait CostModel {
  /**
   * The cost model used to guide the outlining process.
   */
  val extractor: OutliningExtractor

  /**
   * Determines if a layer will be lowered within the equality saturation process.
   * @param layer The layer to check.
   * @return `true` if the layer should be lowered; otherwise, `false`.
   */
  def isLowerable(layer: NetworkLayerExpr): Boolean
}