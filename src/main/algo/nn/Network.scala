package algo.nn

import algo.AlgoExpr
import core.{BuiltinExpr, Expr}

/**
 * Defines functionality that manipulates neural network expressions.
 */
object Network {
  /**
   * Lowers all network layers in an expression to more primitive operators.
   * @param network An expression containing network layers to lower.
   * @param isLowerable A predicate that determines whether a network layer should be lowered.
   * @return An expression that is equivalent in functionality to `network` and contains no `NetworkLayerExpr`.
   */
  def lower(network: Expr, isLowerable: NetworkLayerExpr => Boolean = _ => true): Expr = {
    network.visitAndRebuild({
      case layer: NetworkLayerExpr if isLowerable(layer) => layer.expand()
      case e => e
    }, x => x).asInstanceOf[Expr]
  }

  /**
   * Counts the number of `NetworkLayerExpr` instances in an expression. In a typical neural network, this simply counts
   * the number of layers in the network.
   * @param expr An expression that potentially contains network layers.
   * @return The number of `NetworkLayerExpr` instances in `expr`.
   */
  def countLayers(expr: Expr): Int = {
    val argLayers = expr match {
      case e: BuiltinExpr => e.args.map(countLayers).sum
      case e => e.children.collect({ case arg: Expr => countLayers(arg) }).sum
    }

    expr match {
      case _: NetworkLayerExpr => 1 + argLayers
      case _ => argLayers
    }
  }
}
