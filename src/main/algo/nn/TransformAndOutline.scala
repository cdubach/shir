package algo.nn

import algo.AlgoLambda
import algo.eqsat.{RewriteRules, TilingRewriteRules}
import backend.hdl.arch.costModel.CostModel.estimateDSPBlocks
import core._
import eqsat._
import eqsat.solver.{AgglomeratedClass, AgglomeratedNode}

import scala.collection.mutable

/**
 * Defines functionality that transforms and outlines expressions. Transformations include tiling, padding, and
 * sequentialization. Transformation and outlining are performed using equality saturation.
 */
case class TransformAndOutline(rewrites: RewriteRules) {
  implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

  private val codeSizeAnalysis = ExtractionAnalysis.smallestExpressionExtractor

  /**
   * Creates an e-graph with a given observer.
   * @param observer The observer to use.
   * @return An e-graph with the given observer.
   */
  def createEGraph(observer: EGraphObserver): DeferredMergingEGraph[HashConsENode, HashConsEClass] = {
    EGraph(
      analyses = new AnalysesBuilder().add(codeSizeAnalysis).add(DeBruijnIndexAnalysis).toAnalyses,
      observer = observer)
  }

  /**
   * Applies the equality saturation-based tiling and padding process to an e-graph.
   * @param eGraph The e-graph to transform.
   * @param tile Enables tiling rules if set to `true`.
   * @param pad Enables padding rules if set to `true`.
   * @param sequentialize Enables sequentialization rules if set to `true`.
   * @param lower Enables lowering rules if set to `true`.
   * @param maxSteps The maximum number of saturation steps.
   * @param costModel The cost model to use for the saturation process.
   */
  def saturate[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass],
                                                   costModel: CostModel,
                                                   tile: Boolean = false,
                                                   pad: Boolean = false,
                                                   sequentialize: Boolean = false,
                                                   wrap: Boolean = true,
                                                   lower: Boolean = true,
                                                   maxSteps: Int = 100): Unit = {
    implicit val isLowerable: NetworkLayerExpr => Boolean = costModel.isLowerable
    val rules = Seq(
      (lower, rewrites.lowering),
      (tile, rewrites.tiling),
      (pad, rewrites.padding),
      (sequentialize, rewrites.sequentialization),
      (wrap, rewrites.wrapping))
      .collect { case (true, rewrites) => rewrites }
      .flatten

    val engine = eGraph.defaultSaturationEngine // .withMatchesAppliedIndefinitely
    engine.saturate(rules, maxSteps)

    if (lower) {
      // If lowering is enabled, we also want to partially lower parallel dot products. This is done as a separate step
      // to avoid introducing a cascade of unnecessary e-nodes.
      engine.saturate(TilingRewriteRules.lowering)
    }
  }

  /**
   * Applies the equality saturation-based tiling and padding engine to an expression, producing a saturated e-graph.
   * @param expr The expression to transform.
   * @param tile Enables tiling rules if set to `true`.
   * @param pad Enables padding rules if set to `true`.
   * @param sequentialize Enables sequentialization rules if set to `true`.
   * @param lower Enables lowering rules if set to `true`.
   * @param debug When set to `true`, this method will print information that might be helpful for debugging purposes;
   *              otherwise, the method does not print any additional information.
   * @param maxSteps The maximum number of saturation steps.
   * @param costModel The cost model to use for the saturation process.
   * @return The root expression of the saturated e-graph.
   */
  def padAndTile(expr: Expr,
                 costModel: CostModel,
                 tile: Boolean = false,
                 pad: Boolean = false,
                 sequentialize: Boolean = true,
                 wrap: Boolean = true,
                 debug: Boolean = true,
                 lower: Boolean = true,
                 maxSteps: Int = 100): EClassT = {
    val transformed = TypeChecker.check(DeBruijnTransform.forward(expr))

    if (debug) {
      println(ExprToText(transformed))
      println(s"DSP blocks: ${estimateDSPBlocks(transformed)}")
      println(s"Expr size: ${ExtractionAnalysis.smallestExpressionExtractor.cost(transformed)}")
    }

    val observer = new TimingObserver()
    val eGraph = createEGraph(observer) // EGraphCompositeObserver(observer, EGraphVisualizingObserver))
    val root = eGraph.add(transformed)
    eGraph.rebuild()

    saturate(
      eGraph,
      tile = tile, pad = pad, sequentialize = sequentialize, lower = lower, wrap = wrap,
      maxSteps = maxSteps, costModel = costModel)

    if (debug) {
      println(s"Saturation took ${observer.totalTime.toSeconds}s")
    }

    root
  }

  /**
   * Applies the equality saturation-based outliner to a saturated e-graph.
   * @param root The root expression of the e-graph to outline and extract an expression from.
   * @param debug When set to `true`, this method will print information that might be helpful for debugging purposes;
   *              otherwise, the method does not print any additional information.
   * @param costModel The cost model to use for the extraction process.
   * @param lower Enables post-extraction lowering if set to `true`.
   * @param retainDeBruijnIndices When set to `true`, the resulting expression will retain De Bruijn indices.
   * @return An algo expression that has been sent through the equality saturation engine and outliner.
   */
  def tryExtract(root: EClassT,
                 costModel: CostModel,
                 debug: Boolean = true,
                 lower: Boolean = true,
                 retainDeBruijnIndices: Boolean = false): Option[OutliningResult] = {
    val outliner = costModel.extractor
    val stopwatch = HierarchicalStopwatch.start("extraction")
    outliner.solve(root).map { solution =>
      val outlined = outliner.solutionToExpr(root, solution)
      val extractionTime = stopwatch.complete()
      if (debug) {
        println(ExprToText(outlined))
        println(ExprToText(outlined))
        println(s"Extraction took ${extractionTime.duration.toSeconds}s")
      }

      val outlinedWithNames = if (retainDeBruijnIndices) {
        TypeChecker.check(outlined)
      } else {
        TypeChecker.check(DeBruijnTransform.backward(outlined))
      }

      val result = if (lower) {
        TypeChecker.check(Network.lower(outlinedWithNames))
      } else {
        outlinedWithNames
      }

      if (debug) {
        println(ExprToText(result, renameVars = false))
        println(result)
      }

      val eClasses = root.reachable
      val functionCallNodes = eClasses.flatMap(_.nodes).collect { case n if n.expr.isInstanceOf[FunctionCall] => n }
      val functionClasses = functionCallNodes.map { n => n.args.head }.distinct
      OutliningResult(result, solution.outlinedClassCount(root), eClasses.length, eClasses.flatMap(_.nodes).length, functionCallNodes.length, functionClasses.length, solution.costs)
    }
  }

  /**
   * Applies the equality saturation-based outliner to an algo expression.
   * @param expr An algo expression to transform.
   * @param tile Enables tiling rules if set to `true`.
   * @param pad Enables padding rules if set to `true`.
   * @param sequentialize Enables sequentialization rules if set to `true`.
   * @param debug When set to `true`, this method will print information that might be helpful for debugging purposes;
   *              otherwise, the method does not print any additional information.
   * @param maxSteps The maximum number of saturation steps.
   * @return An algo expression that has been sent through the equality saturation engine and outliner.
   */
  def tryOutline(expr: Expr,
                 costModel: CostModel,
                 tile: Boolean = false,
                 pad: Boolean = false,
                 sequentialize: Boolean = true,
                 lower: Boolean = true,
                 wrap: Boolean = true,
                 debug: Boolean = true,
                 maxSteps: Int = 100): Option[OutliningResult] = {
    val root = padAndTile(
      expr,
      tile = tile, pad = pad, sequentialize = sequentialize, wrap = wrap,
      lower = lower, debug = debug, maxSteps = maxSteps, costModel = costModel)
    tryExtract(root, costModel, lower = lower, debug = debug)
  }

  /**
   * Applies the equality saturation-based outliner to an algo expression.
   * @param expr An algo expression to transform.
   * @param tile Enables tiling rules if set to `true`.
   * @param pad Enables padding rules if set to `true`.
   * @param sequentialize Enables sequentialization rules if set to `true`.
   * @param debug When set to `true`, this method will print information that might be helpful for debugging purposes;
   *              otherwise, the method does not print any additional information.
   * @param maxSteps The maximum number of saturation steps.
   * @return An algo expression that has been sent through the equality saturation engine and outliner.
   */
  def outline(expr: Expr,
              costModel: CostModel,
              tile: Boolean = false,
              pad: Boolean = false,
              sequentialize: Boolean = true,
              lower: Boolean = true,
              wrap: Boolean = true,
              debug: Boolean = true,
              maxSteps: Int = 100): Expr = {
    tryOutline(
      expr, costModel,
      tile = tile, pad = pad, sequentialize = sequentialize, lower = lower, wrap = wrap,
      debug = debug, maxSteps = maxSteps).get.solution
  }

  /**
   * Computes the use cost of a single instantiation of an e-node.
   * @param costModel The cost model to use for the computation.
   * @param dimension The cost dimension to generate a formula for.
   * @param expr The expression to model a cost for.
   * @param argCosts The use cost of each e-class appearing in `expr`, expressed as a formula.
   * @return A formula that expresses the cost of `expr`, including the use cost of its arguments.
   */
  def exprCost(costModel: OutliningExtractorCostModel, dimension: Int)(expr: Expr, argCosts: Map[EClassT, solver.Formula]): solver.Formula = {
    // To compute the cost of an expression, we construct a dummy AgglomeratedNode to model the expression. Then we
    // compute the cost of the expression by calling AgglomeratedNode.useCost.

    // Define a class for the dummy e-nodes
    case class DummyENode(expr: Expr) extends ENodeT {
      override def args: Seq[EClassT] = Seq()
    }

    val classMap = mutable.Map.empty[EClassT, AgglomeratedClass]

    // Recursively construct a tree of AgglomeratedNode to model the expression
    def constructAgglomeratedNode(expr: Expr): AgglomeratedNode = {
      val exprArgs = ExprHelpers.args(expr)
      val nodeArgs = exprArgs.map({
        case c: EClassT => Right(classMap.getOrElseUpdate(c, new AgglomeratedClass(c)))
        case e => Left(constructAgglomeratedNode(e))
      })
      AgglomeratedNode(DummyENode(expr), nodeArgs)
    }

    val agglomeratedNode = constructAgglomeratedNode(expr)
    //    assert(argCosts.length == agglomeratedNode.args.length, "The number of argument costs must match the number of arguments of the expression")

    val reorderedArgCosts = agglomeratedNode.args.map(_.eClass).map(argCosts)
    agglomeratedNode.useCost(costModel, dimension, reorderedArgCosts)
  }

  /**
   * Computes the costs of an expression using the cost model. This method only works for expressions that have not yet
   * been sent through the equality saturation engine.
   * @param expr The expression to compute costs for.
   * @param costModel The cost model to use for the computation.
   * @return The costs of the expression, expressed as one cost per cost dimension of the cost model.
   */
  def computeCosts(expr: Expr, costModel: CostModel): Seq[BigInt] = {
    val lowered = TypeChecker.check(Network.lower(expr, {
      case _: DotProductLayerExpr => false
      case _ => true
    }))
    val extractor = costModel.extractor
    val extractorCostModel = extractor.costModel
    val dimensions = extractorCostModel.dimensions
    for (dimension <- 0 until dimensions) yield {
      exprCost(extractorCostModel, dimension)(lowered, Map.empty).evalInt
    }
  }
}
