package algo.nn
import algo.{AlgoDataTypeT, AlgoLambda, Join, Repeat, SeqType, SeqTypeT, SignedIntTypeVar}
import core.{ArithTypeT, ArithTypeVar, BuiltinExpr, BuiltinTypeFunction, Expr, FunType, FunTypeVar, FunctionCall, LambdaT, ParamDef, ParamUse, Type, TypeChecker, TypeFunType, TypeFunctionCall, UnknownType}

/**
 * A network layer expression that transforms its data arguments and then uses a function argument to perform a
 * computation on the transformed data.
 */
trait HierarchicalLayerExpr extends DataTransformationLayerExpr

/**
 * A companion object for the `HierarchicalLayerExpr` trait.
 */
object HierarchicalLayer {
  /**
   * Recursively transforms all eligible subexpressions of an expression into hierarchical layer expressions.
   * @param expr The expression.
   * @return The expression with all eligible subexpressions transformed into hierarchical layer expressions.
   */
  def fromRec(expr: Expr): Expr = expr.visitAndRebuild({
    case expr: Expr => from(expr)
    case other => other
  }, identity).asInstanceOf[Expr]

  /**
   * Constructs a `HierarchicalLayerExpr` from an expression if possible; otherwise, returns the expression unchanged.
   * @param expr The expression.
   * @return The hierarchical layer expression or the original expression.
   */
  private def from(expr: Expr): Expr = expr match {
    case layer: ConvolutionLayerExpr => MatVecConvolutionLayer.from(layer)
    case layer: MatVecLayerExpr => DotProductMatVecLayer.from(layer)
    case other => other
  }
}

/**
 * A convolutional layer expression that is implemented using a matrix-vector function input to perform the convolution.
 * @param innerIR The inner IR of the layer.
 * @param types The types of the layer.
 */
final case class MatVecConvolutionLayerExpr(innerIR: Expr, types: Seq[Type]) extends HierarchicalLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): BuiltinExpr = MatVecConvolutionLayerExpr(newInnerIR, newTypes)

  def W: ArithTypeT = this match {
    case MatVecConvolutionLayer(in, _, _, _) => ConvolutionLayer.getW(in)
  }

  def H: ArithTypeT = this match {
    case MatVecConvolutionLayer(in, _, _, _) => ConvolutionLayer.getH(in)
  }

  def ICH: ArithTypeT = this match {
    case MatVecConvolutionLayer(in, _, _, _) => ConvolutionLayer.getICH(in)
  }

  def OCH: ArithTypeT = this match {
    case MatVecConvolutionLayer(_, weight, _, _) => ConvolutionLayer.getOCH(weight)
  }

  override def expand(): Expr = {
    this match {
      case MatVecConvolutionLayer(input, weight, matVec, _) =>
        implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

        val inputtc = Helpers.checkIfUnknown(input, input.t)
        Helpers.assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")
        Helpers.assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.length == 3, "input must be 3D!")
        // Helpers.assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.head.ae.evalInt == inputChannels.ae.evalInt, "Layer and input channels do not match!")

        val weighttc = Helpers.checkIfUnknown(weight, weight.t)

        val kernelSizeH = weighttc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len.ae
        val kernelSizeW = weighttc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len.ae
        val kernelIch = weighttc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len.ae
        val inputBasicType = inputtc.t.asInstanceOf[SeqTypeT].leafType
        // TODO: check weight type to conform to the same type as
        // algo.Input("weight" + id, inputChannels, kernelSize.ae * kernelSize.ae * kernelNum.ae, algo.SignedIntType(weightBitWidth))

        val inputParam = ParamDef(inputtc.t)
        val weightParam = ParamDef(weighttc.t)

        val slideHeight = Helpers.check(algo.SlideGeneral(ParamUse(inputParam), kernelSizeH, 1))

        val slideWidth = Helpers.check(algo.Map(2, algo.SlideGeneral.asFunction(Seq(None), Seq(kernelSizeW, 1)),
          slideHeight))

        val inputSlided = Helpers.check(algo.TransposeND(slideWidth, Seq(0, 1, 3, 2, 4)))

        val paramX = ParamDef(inputSlided.t.asInstanceOf[SeqTypeT].et)
        val inputSemiReshapedLambda = Helpers.check({
          val param = ParamDef(inputSlided.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
          AlgoLambda(param, Join(ParamUse(param)))
        })
        val inputSemiReshapedInner = Helpers.check(algo.Map(inputSemiReshapedLambda, ParamUse(paramX)))
        val inputSemiReshaped = Helpers.check(algo.Map(AlgoLambda(paramX, inputSemiReshapedInner), inputSlided))
        val inputReshaped = Helpers.check(algo.Map(2, algo.Join.asFunction(), inputSemiReshaped))

        val weightReshaped = algo.Map(Join.asFunction(), algo.Map(Join.asFunction(), ParamUse(weightParam)))
        assert(TypeChecker.check(weightReshaped).t == SeqType(SeqType(inputBasicType, kernelSizeH * kernelSizeW * kernelIch), OCH))

        val conv = algo.Map(
          2,
          {
            val paramI = ParamDef(SeqType(inputBasicType, kernelSizeH * kernelSizeW * kernelIch))
            AlgoLambda(
              paramI,
              FunctionCall(matVec, Seq(ParamUse(paramI), weightReshaped))
            )
          }, inputReshaped
        )

        FunctionCall(FunctionCall(AlgoLambda(weightParam, AlgoLambda(inputParam, conv)), weight), input)
    }
  }
}

/**
 * A companion object for the `MatVecConvolutionLayerExpr` class. This object provides an `apply` method that constructs
 * a `MatVecConvolutionLayerExpr` from its arguments and an `unapply` method that deconstructs a `MatVecConvolutionLayerExpr`
 * into its arguments.
 */
object MatVecConvolutionLayer {
  /**
   * Constructs a `MatVecConvolutionLayerExpr` from a `ConvolutionLayerExpr`.
   * @param convolution The convolution layer expression.
   * @return The matrix-vector convolution layer expression.
   */
  def from(convolution: ConvolutionLayerExpr): MatVecConvolutionLayerExpr = {
    convolution match {
      case ConvolutionLayer(input, weight, _) =>
        val inputtc = Helpers.checkIfUnknown(input)
        val weighttc = Helpers.checkIfUnknown(weight)

        val inputParam = ParamDef(ConvolutionLayer.computeMatVecInputType(
          inputtc.t.asInstanceOf[AlgoDataTypeT], weighttc.t.asInstanceOf[AlgoDataTypeT]))

        val weightParam = ParamDef(ConvolutionLayer.computeMatVecWeightType(
          inputtc.t.asInstanceOf[AlgoDataTypeT], weighttc.t.asInstanceOf[AlgoDataTypeT]))

        val matVec = TypeChecker.check(MatVecLayer(ParamUse(weightParam), ParamUse(inputParam)))

        apply(inputtc, weighttc, AlgoLambda(Seq(inputParam, weightParam), matVec))
    }
  }

  /**
   * Constructs a `MatVecConvolutionLayerExpr` from its arguments.
   * @param input The input argument.
   * @param weight The weight argument.
   * @param matVecFunction The matrix-vector function argument.
   * @return The matrix-vector convolution layer expression.
   */
  def apply(input: Expr, weight: Expr, matVecFunction: Expr): MatVecConvolutionLayerExpr = {
    val W = ArithTypeVar()
    val H = ArithTypeVar()
    val ICH = ArithTypeVar()
    val OCH = ArithTypeVar()
    val KW = ArithTypeVar()
    val KH = ArithTypeVar()

    val idtv = SignedIntTypeVar()

    val instt = SeqType(SeqType(SeqType(idtv, ICH), W), H)
    val wt = SeqType(SeqType(SeqType(SeqType(idtv, ICH), KW), KH), OCH)

    val matVecInput1 = ConvolutionLayer.computeMatVecInputType(idtv, ICH, KW, KH)
    val matVecInput2 = ConvolutionLayer.computeMatVecWeightType(idtv, ICH, OCH, KW, KH)
    val matVecOutput = ConvolutionLayer.computeMatVecOutputType(instt, wt)
    val matVecFunType = FunTypeVar(Seq(matVecInput1, matVecInput2), matVecOutput)

    val ost = ConvolutionLayer.computeConvolutionResultType(instt, wt)

    val innerCraziness = FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(
      Seq(W, H, ICH, OCH, KW, KH, idtv, matVecFunType),
      FunType(Seq(instt, wt, matVecFunType), ost))),
      Seq.fill(8)(UnknownType)), Seq(input, weight, matVecFunction))

    MatVecConvolutionLayerExpr(innerCraziness, Seq())
  }

  /**
   * Deconstructs a `MatVecConvolutionLayerExpr` into its arguments.
   * @param expr The matrix-vector convolution layer expression.
   * @return The input argument, the weight argument, the matrix-vector function argument, and the type of the layer.
   */
  def unapply(expr: MatVecConvolutionLayerExpr): Some[(Expr, Expr, Expr, Type)] = {
    Some((
      expr.args.head,
      expr.args(1),
      expr.args(2),
      expr.t
    ))
  }
}

/**
 * A matrix-vector layer expression that is implemented using a dot product function input to perform the matrix-vector
 * multiplication.
 * @param innerIR The inner IR of the layer.
 * @param types The types of the layer.
 */
final case class DotProductMatVecLayerExpr(innerIR: Expr, types: Seq[Type]) extends HierarchicalLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): BuiltinExpr = DotProductMatVecLayerExpr(newInnerIR, newTypes)

  override def expand(): Expr = {
    this match {
      case DotProductMatVecLayer(matrix, vector, dotProduct, _) =>
        val matrixtc = Helpers.checkIfUnknown(matrix)
        val rows = matrixtc.t.asInstanceOf[SeqTypeT].len
        FunctionCall(dotProduct, Seq(matrixtc, Repeat(vector, rows)))
    }
  }
}

/**
 * A companion object for the `DotProductMatVecLayerExpr` class. This object provides an `apply` method that constructs
 * a `DotProductMatVecLayerExpr` from its arguments and an `unapply` method that deconstructs a `DotProductMatVecLayerExpr`
 * into its arguments.
 */
object DotProductMatVecLayer {
  /**
   * Constructs a `DotProductMatVecLayerExpr` from a `MatVecLayerExpr`.
   * @param matVec The matrix-vector layer expression.
   * @return The dot product matrix-vector layer expression.
   */
  def from(matVec: MatVecLayerExpr): DotProductMatVecLayerExpr = {
    matVec match {
      case MatVecLayer(matrix, vector, _) =>
        val matrixtc = Helpers.checkIfUnknown(matrix)

        val rows = matrixtc.t.asInstanceOf[SeqTypeT].len

        val pm = ParamDef(matrixtc.t)
        val pv = ParamDef(matrixtc.t)

        val dotProduct = TypeChecker.check(ParallelDotProductsLayer(ParamUse(pm), ParamUse(pv), rows))

        apply(matrixtc, vector, AlgoLambda(Seq(pm, pv), dotProduct))
    }
  }

  /**
   * Constructs a `DotProductMatVecLayerExpr` from its arguments.
   * @param matrix The matrix argument.
   * @param vector The vector argument.
   * @param dotProductFunction The dot product function argument.
   * @return The dot product matrix-vector layer expression.
   */
  def apply(matrix: Expr, vector: Expr, dotProductFunction: Expr): DotProductMatVecLayerExpr = {
    val M = ArithTypeVar()
    val N = ArithTypeVar()
    val idtv = SignedIntTypeVar()
    val odtv = SignedIntTypeVar()

    val seqTypeIdtvN = SeqType(idtv, N)
    val instt = SeqType(seqTypeIdtvN, M)
    val ost = SeqType(odtv, M)

    val dotProductFunType = FunTypeVar(Seq(instt, instt), ost)

    val innerCraziness = FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(
      Seq(M, N, idtv, dotProductFunType),
      FunType(Seq(instt, seqTypeIdtvN, dotProductFunType), ost))),
      Seq.fill(4)(UnknownType)), Seq(matrix, vector, dotProductFunction))

    DotProductMatVecLayerExpr(innerCraziness, Seq())
  }

  /**
   * Deconstructs a `DotProductMatVecLayerExpr` into its arguments.
   * @param expr The dot product matrix-vector layer expression.
   * @return The matrix argument, the vector argument, the dot product function argument, and the type of the layer.
   */
  def unapply(expr: DotProductMatVecLayerExpr): Some[(Expr, Expr, Expr, Type)] = {
    Some((
      expr.args.head,
      expr.args(1),
      expr.args(2),
      expr.t
    ))
  }
}