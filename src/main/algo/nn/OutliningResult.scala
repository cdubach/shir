package algo.nn

import core.Expr

/**
   * The result of the outlining process.
 *
 * @param solution The solution expression, which contains outlined functions.
   * @param outlinedClassCount The number of outlined e-classes in `solution`.
   * @param eClassCount The number of e-classes in the e-graph from which the expression is extracted.
   * @param eNodeCount The number of e-nodes in the e-graph from which the expression is extracted.
   * @param functionCallNodeCount The number of function call nodes in the outlined expression.
   * @param functionClassCount The number of function call classes in the e-graph from which the expression is extracted.
   * @param costs The costs of the outlined expression, expressed as one cost per cost dimension of the cost model.
   */
  final case class OutliningResult(solution: Expr,
                                   outlinedClassCount: BigInt, eClassCount: BigInt, eNodeCount: BigInt,
                                   functionCallNodeCount: BigInt, functionClassCount: BigInt,
                                   costs: Seq[BigInt])
