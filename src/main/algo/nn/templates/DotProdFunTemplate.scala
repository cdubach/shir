package algo.nn.templates

import algo.{Add2, AlgoLambda, Fold, Map, Mul, SeqType, SignedIntType, Tuple2, Zip2}
import core.{Expr, ParamDef, ParamUse, TypeChecker}

object DotProdFunTemplate {
  def createDotProdFun(
    dotProdSize: Int,
    bitWidth: Int,
  ): Expr = {
    val paramInput0 = ParamDef(SeqType(SignedIntType(bitWidth), dotProdSize))
    val paramInput1 = ParamDef(SeqType(SignedIntType(bitWidth), dotProdSize))
    val dotProduct = Fold(
      Add2.asFunction(),
      TypeChecker.check(Map(
        Mul.asFunction(),
        Zip2(Tuple2(ParamUse(paramInput0), ParamUse(paramInput1)))
      ))
    )
    val fun = TypeChecker.check(AlgoLambda(paramInput0, AlgoLambda(paramInput1, TypeChecker.check(dotProduct))))
    fun
  }
}