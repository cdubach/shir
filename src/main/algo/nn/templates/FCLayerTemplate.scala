package algo.nn.templates

import algo._
import core._

object FCLayerTemplate {

  def createMVWrapper(
    dotProdParam: ParamDef,
    width: Int,
    height: Int,
    bitWidth: Int,
    id: Int = 0,
    bias: Boolean = false,
    optInput: Option[Expr] = None
  ): Expr = {

    val cacheLineWidth = 512 / bitWidth
    val input = optInput match {
      case Some(i) => i
      case None => Input("vec" + id.toString, width, 1, SignedIntType(bitWidth))
    }

    val inputtc = TypeChecker.check(Join(input))
    val mat = Input("mat" + id.toString, width, height, SignedIntType(bitWidth))

    val padInput = {
      val splitBuf = TypeChecker.check(Split(inputtc, 64))
      val concatBuf = TypeChecker.check(Concat(splitBuf, Repeat(Repeat(ConstantInteger(0, Some(SignedIntType(8))), 64), 24)))
      concatBuf
    }

    val padMat = TypeChecker.check(Split(algo.Map(
      {
        val param = ParamDef()
        AlgoLambda(
          param,
          Split(ParamUse(param), 32)
        )
      }, mat
    ), 64))

    val splitInput = TypeChecker.check(Repeat(padInput, padMat.t.asInstanceOf[SeqTypeT].len))
    val splitMat = TypeChecker.check(padMat)

    val compute = TypeChecker.check(Map2Input(
      {
        val param2 = ParamDef(splitInput.t.asInstanceOf[SeqTypeT].et)
        val param1 = ParamDef(splitMat.t.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param2,
          AlgoLambda(
            param1,
            {
              val regW =
                TypeChecker.check(algo.Map(
                  {
                    val param = ParamDef(param1.t.asInstanceOf[SeqTypeT].et)
                    AlgoLambda(
                      param,
                      {
                        val padW = TypeChecker.check(Concat(ParamUse(param), Repeat(Repeat(ConstantInteger(0, Some(SignedIntType(bitWidth))), 32), 2)))
                        algo.Map(Join.asFunction(), Split(padW, 2))
                      }
                    )
                  }, ParamUse(param1)
                ))

              val regI = TypeChecker.check(Concat(ParamUse(param2), Repeat(Repeat(ConstantInteger(0, Some(SignedIntType(bitWidth))), 64), 4)))
              val input0 = TypeChecker.check(Repeat(regI, 64))
              val input1 = TypeChecker.check(algo.Map(Join.asFunction(), algo.Map(Repeat.asFunction(Seq(None), Seq(4)), regW)))
              val zipInput = TypeChecker.check(algo.Zip2(Tuple2(input0, input1)))
              val partialSum = TypeChecker.check(algo.Map(
                {
                  val param3 = ParamDef(zipInput.t.asInstanceOf[SeqTypeT].et)
                  val zipZipInput = TypeChecker.check(algo.Zip2(Tuple2(Select2(ParamUse(param3), 0), Select2(ParamUse(param3), 1))))
                  AlgoLambda(
                    param3,
                    algo.Map(
                      {
                        val param2 = ParamDef(zipZipInput.t.asInstanceOf[SeqTypeT].et)
                        AlgoLambda(param2, FunctionCall(FunctionCall(ParamUse(dotProdParam), Select2(ParamUse(param2), 0)), Select2(ParamUse(param2), 1)))
                      }, zipZipInput
                    )
                  )
                },
                zipInput
              ))
              partialSum
            }
          )
        )
      }, splitInput, splitMat
    ))

    val sumUp = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(compute.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param,
          {
            val dropRedundant = TypeChecker.check(Drop(ParamUse(param), 0, param.t.asInstanceOf[SeqTypeT].len.ae.evalInt - width / 32))
            Fold(Add2.asFunction(), dropRedundant)
          }
        )
      }, compute
    ))

    val addBias = if(bias) {
      val initBias = Input("bias" + id, height, 1, SignedIntType(bitWidth * 2))
      val biasInReshaped = TypeChecker.check(algo.Buffer(algo.Split(algo.Join(initBias), 64)))
      val zipBias = TypeChecker.check(algo.Map(algo.Zip2.asFunction(), algo.Zip2(algo.Tuple2(sumUp, biasInReshaped))))
      val sum = TypeChecker.check(algo.Map(2, algo.Add.asFunction(), zipBias))
      sum
    } else {
      sumUp
    }

    val truncOut = TypeChecker.check(algo.Map(2,
      {
        val param1 = ParamDef(sumUp.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param1,
          {
            Max(Tuple2(TypeChecker.check(ClipBankersRound(ParamUse(param1), bitWidth, param1.t.asInstanceOf[SignedIntType].width.ae.evalInt - bitWidth - bitWidth)),
              ConstantInteger(0, Some(SignedIntType(bitWidth)))))
          }
        )
      }, addBias
    ))

    Split(Join(truncOut), height)
  }
}