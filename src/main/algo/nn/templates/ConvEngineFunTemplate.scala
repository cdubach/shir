package algo.nn.templates

import algo._
import core._

object ConvEngineFunTemplate {
  def createConvFunEngineV1(
    dotProdFunParam: ParamDef,
    innerDotProdSize: Int = 64,
    outerDotProdSize: Int = 9,
    dotProdWindows: Int = 4,
    dotProdReuse: Int = 64,
    bitWidth: Int = 8
  ): Expr = {
    val paramInput0 = ParamDef(SeqType(SeqType(SignedIntType(bitWidth), innerDotProdSize), outerDotProdSize * dotProdWindows))
    val paramInput1 = ParamDef(SeqType(SeqType(SeqType(SignedIntType(bitWidth), innerDotProdSize), outerDotProdSize), dotProdReuse))

    val input0 = Repeat(ParamUse(paramInput0), dotProdReuse)
    val input1 = algo.Map(Join.asFunction(), algo.Map(Repeat.asFunction(Seq(None), Seq(dotProdWindows)), ParamUse(paramInput1)))
    val zipInput = TypeChecker.check(Zip2(Tuple2(input0, input1)))
    val partialSum = TypeChecker.check(algo.Map(
      {
        val param1 = ParamDef(zipInput.t.asInstanceOf[SeqTypeT].et)
        val zipZipInput = TypeChecker.check(Zip2(Tuple2(Select2(ParamUse(param1), 0), Select2(ParamUse(param1), 1))))
        AlgoLambda(
          param1,
          algo.Map(
            {
              val param2 = ParamDef(zipZipInput.t.asInstanceOf[SeqTypeT].et)
              AlgoLambda(param2, FunctionCall(FunctionCall(ParamUse(dotProdFunParam), Select2(ParamUse(param2), 0)), Select2(ParamUse(param2), 1)))
            }, zipZipInput
          )
        )
      },
      zipInput
    ))
    val partialSumChunk = TypeChecker.check(algo.Map(Split.asFunction(Seq(None), Seq(outerDotProdSize)), partialSum))
    val result = TypeChecker.check(
      algo.Map(2,
        {
          val param = ParamDef(partialSumChunk.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
          AlgoLambda(
            param,
            TypeChecker.check(Fold(Add2.asFunction(), ParamUse(param)))
          )
        }, partialSumChunk
      )
    )
    TypeChecker.check(AlgoLambda(paramInput0, AlgoLambda(paramInput1, result)))
  }
}