package algo.nn.templates

import algo._
import core._

object OuterWrapperFunTemplate {

  def createConvWrapper(
    convFunParam: ParamDef,
    inputFunParam: ParamDef,
    weightFunParam: ParamDef,
    widthTileSize: Int = 8,
    heightTileSize: Int = 8,
    ichTileSize: Int = 64,
    ochTileSize: Int = 64,
    wSize: Int = 3,
    padSize: Int = 1,
    bitWidth: Int = 8
  ): Expr = {
    val iParam = ParamDef(SeqType(SeqType(SeqType(SignedIntType(bitWidth), ichTileSize), widthTileSize + 2 * padSize), heightTileSize + 2 * padSize))
    val wParam = ParamDef(SeqType(SeqType(SignedIntType(bitWidth), ichTileSize * wSize * wSize), ochTileSize))

    val bufferInput0 = TypeChecker.check(FunctionCall(ParamUse(inputFunParam), ParamUse(iParam)))
    val bufferInput1 = TypeChecker.check(FunctionCall(ParamUse(weightFunParam), ParamUse(wParam)))
    val result = TypeChecker.check(
      Map2Input(2,
        {
          val param0 = ParamDef(bufferInput0.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
          val param1 = ParamDef(bufferInput1.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
          AlgoLambda(param0, AlgoLambda(param1, FunctionCall(FunctionCall(ParamUse(convFunParam), ParamUse(param0)), ParamUse(param1))))
        }, bufferInput0, bufferInput1
      )
    )
    TypeChecker.check(AlgoLambda(iParam, AlgoLambda(wParam, result)))
  }
}