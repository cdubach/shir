package algo.nn.templates

import algo._
import core._

object ConvLayerTemplate {

  def createConvWrapper(
    mainConvFunParam: ParamDef,
    width: Int,
    height: Int,
    ich: Int,
    och: Int,
    widthTileSize: Int = 8,
    heightTileSize: Int = 8,
    ichTileSize: Int = 64,
    ochTileSize: Int = 64,
    wSize: Int = 3,
    padSize: Int = 1,
    bitWidth: Int = 8,
    pool: Boolean = true,
    id: Int = 0,
    bias: Boolean = false,
    optInput: Option[Expr] = None
  ): Expr = {
    val initWeight = Input("weight" + id, ich * wSize * wSize, och, SignedIntType(bitWidth))

    val initInput = optInput match {
      case Some(origInput) => origInput
      case None => Input("image" + id, ich, width * height, SignedIntType(bitWidth))
    }

    val splitInput = TypeChecker.check(Split(initInput, width))
    // Extra padding if tile size is too large
    val padInput = TypeChecker.check(algo.Map(Pad.asFunction(Seq(None),
      Seq(padSize, padSize + (if(width < widthTileSize) widthTileSize - width else 0), 0)), TypeChecker.check(
      Pad(splitInput, padSize, padSize + (if(height < heightTileSize) heightTileSize - height else 0), 0))))

    val foldSum = mainConvTemplate(
      mainConvFunParam,
      width, height, ich, och, widthTileSize, heightTileSize, ichTileSize, ochTileSize,
      padInput, initWeight
    )

    val addBias = biasTemplate(
      width, height, ich, och, widthTileSize, heightTileSize, ichTileSize, ochTileSize,
      bitWidth, id, bias,
      foldSum
    )

    val truncOut = roundingReLUTemplate(
      ich, och, ichTileSize, ochTileSize,
      bitWidth,
      addBias
    )

    val poolOut = poolingTemplate(
      ich, och, ichTileSize, ochTileSize,
      pool,
      truncOut
    )

    poolOut
  }

  def createConvWrapperSimple(
    dotProdParam: ParamDef,
    width: Int,
    height: Int,
    ich: Int,
    och: Int,
    wSize: Int = 3,
    padSize: Int = 1,
    bitWidth: Int = 8,
    id: Int = 0,
    bias: Boolean = false,
    optInput: Option[Expr] = None
  ): Expr = {
    val initWeight = Input("weight" + id, ich * wSize * wSize, och, SignedIntType(bitWidth))

    val initInput = optInput match {
      case Some(origInput) => origInput
      case None => Input("image" + id, ich * width,  height, SignedIntType(bitWidth))
    }

    val padWeight = TypeChecker.check(
      algo.Map(
        {
          val param1 = ParamDef(initWeight.t.asInstanceOf[SeqTypeT].et)
          AlgoLambda(
            param1,
            Concat(ParamUse(param1), Repeat(ConstantInteger(0, Some(SignedIntType(bitWidth))), 5 + 32))
          )
        }, initWeight
      )
    )
    val reshapeWeight = TypeChecker.check(
      Repeat(Split(padWeight, 32), height)
    )

    val repWeight = TypeChecker.check(
      Concat(
        reshapeWeight,
        Repeat(Repeat(Repeat(Repeat(ConstantInteger(0, Some(SignedIntType(8))), 64), 4), 2), height), 2
      )
    )

    val vecInput = initInput

    val padIWidth = TypeChecker.check(algo.Map({
      val param = ParamDef(vecInput.t.asInstanceOf[SeqTypeT].et)
      AlgoLambda(
        param,
        Pad(ParamUse(param), padSize * ich, padSize * ich, 0)
      )
    }, vecInput))

    val padHeight = TypeChecker.check(Pad(padIWidth, padSize, padSize, 0))

    val slideInput = TypeChecker.check(Slide(algo.Map(SlideGeneral.asFunction(Seq(None), Seq(wSize * ich, ich)), padHeight), wSize, 1))
    val transposedInput = algo.Map({
      val param = ParamDef(slideInput.t.asInstanceOf[SeqTypeT].et)
      AlgoLambda(
        param,
        Transpose(ParamUse(param))
      )
    }, slideInput)

    val joinedInput = TypeChecker.check(algo.Map(Repeat.asFunction(Seq(None), Seq(2)), algo.Map(2, Join.asFunction(), transposedInput)))
    val padInput2 = TypeChecker.check(algo.Map(2,
      {
        val param = ParamDef(joinedInput.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param,
          Concat(ParamUse(param), Repeat(Repeat(ConstantInteger(0, Some(SignedIntType(8))), 27), 32))
        )
      }, joinedInput
    ))

    val newPadInput = TypeChecker.check(
      algo.Map(
        3,
        {
          val param = ParamDef()
          AlgoLambda(
            param,
            Concat(ParamUse(param), Repeat(ConstantInteger(0, Some(SignedIntType(8))), 5))
          )
        }, padInput2
      )
    )
    val repInput = TypeChecker.check(
      algo.Map(
        3,
        {
          val param = ParamDef(newPadInput.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
          AlgoLambda(
            param,
            {
              val repVec = TypeChecker.check(Repeat(ParamUse(param), (och + 8) / 4))
              val reshapeVec = algo.Map(Join.asFunction(), Split(repVec, 2))
              reshapeVec
            }
          )
        },
        newPadInput
      ))

    val mainConv = TypeChecker.check(
      Map2Input(
        2,
        {
          val param1 = ParamDef(repWeight.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
          val param2 = ParamDef(repInput.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
          AlgoLambda(
            param1,
            AlgoLambda(
              param2,
              {
                val input0 = TypeChecker.check(Repeat(ParamUse(param1), och))
                val input1 = TypeChecker.check(algo.Map(Join.asFunction(), algo.Map(Repeat.asFunction(Seq(None), Seq(4)), ParamUse(param2))))
                val zipInput = TypeChecker.check(algo.Zip2(Tuple2(input0, input1)))
                val partialSum = TypeChecker.check(algo.Map(
                  {
                    val param3 = ParamDef(zipInput.t.asInstanceOf[SeqTypeT].et)
                    val zipZipInput = TypeChecker.check(algo.Zip2(Tuple2(Select2(ParamUse(param3), 0), Select2(ParamUse(param3), 1))))
                    AlgoLambda(
                      param3,
                      algo.Map(
                        {
                          val param2 = ParamDef(zipZipInput.t.asInstanceOf[SeqTypeT].et)
                          AlgoLambda(param2, FunctionCall(FunctionCall(ParamUse(dotProdParam), Select2(ParamUse(param2), 0)), Select2(ParamUse(param2), 1)))
                        }, zipZipInput
                      )
                    )
                  },
                  zipInput
                ))
                algo.Map({
                  val param = ParamDef()
                  AlgoLambda(
                    param,
                    Drop(ParamUse(param), 0, 4)
                  )
                },
                  Drop(partialSum, 0, 32))
              }
            )
          )
        }, repWeight, repInput
      )
    )


    val biasAdd = if(bias) {
      val initBias = Input("bias" + id, och, 1, SignedIntType(bitWidth * 2))
      val biasInReshaped = TypeChecker.check(algo.Join(algo.Map(algo.Split.asFunction(Seq(None), Seq(32)), initBias)))
      val bufferBias = TypeChecker.check(algo.Buffer(biasInReshaped))
      val repBias = TypeChecker.check(algo.Repeat(algo.Map(Repeat.asFunction(Seq(None), Seq(32)), bufferBias), 32))
      val zipBias = TypeChecker.check(algo.Map(3, algo.Zip2.asFunction(), algo.Map(2, algo.Zip2.asFunction(),
        algo.Map(algo.Zip2.asFunction(), algo.Zip2(Tuple2(mainConv, repBias))))))
      val sum = TypeChecker.check(algo.Map(4, algo.Add.asFunction(), zipBias))
      sum
    } else {
      mainConv
    }

    val truncOut = TypeChecker.check(algo.Map(
      3,
      {
        val param1 = ParamDef(biasAdd.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
        AlgoLambda(
          param1,
          algo.Map(
            {
              val param2 = ParamDef(param1.t.asInstanceOf[SeqTypeT].et)
              AlgoLambda(
                param2,
                Max(Tuple2(ClipBankersRound(ParamUse(param2), 8, 6), ConstantInteger(0, Some(SignedIntType(bitWidth)))))
              )
            }, ParamUse(param1)
          )
        )
      }, biasAdd
    ))

    val tpOut2 = TypeChecker.check(
      algo.Map(
        {
          val param = ParamDef(truncOut.t.asInstanceOf[SeqTypeT].et)
          AlgoLambda(
            param,
            algo.Map(Join.asFunction(), Transpose(ParamUse(param)))
          )
        }, truncOut
      )
    )
    // outputReshape
    Join(tpOut2)
  }


  /**
   * Components?
   */

  def mainConvTemplate(
    mainConvFunParam: ParamDef,
    width: Int,
    height: Int,
    ich: Int,
    och: Int,
    widthTileSize: Int = 8,
    heightTileSize: Int = 8,
    ichTileSize: Int = 64,
    ochTileSize: Int = 64,
    padInput: Expr,
    initWeight: Expr
  ): Expr = {
    val slidInput = TypeChecker.check(SlideGeneral(TypeChecker.check(algo.Map(SlideGeneral.asFunction(Seq(None),
      Seq(heightTileSize + 2, heightTileSize)), padInput)), widthTileSize + 2, widthTileSize))

    val weight = (ich, och) match {
      case (i, o) if i > ichTileSize && o > ochTileSize =>
        val splitIch = TypeChecker.check(algo.Map(Split.asFunction(Seq(None), Seq(ich)), initWeight))
        val splitCache = TypeChecker.check(algo.Map(2, Split.asFunction(Seq(None), Seq(ichTileSize)), splitIch))
        val splitOch = TypeChecker.check(Split(splitCache, ochTileSize))
        val transposeWgt = TypeChecker.check(TransposeND(splitOch, Seq(0, 2, 3, 1, 4)))
        TypeChecker.check(algo.Map(3, Join.asFunction(), transposeWgt))
      case (i, o) if i == ichTileSize && o > ochTileSize => TypeChecker.check(Split(initWeight, ochTileSize))
      case (i, o) if i == ichTileSize && o == ochTileSize => initWeight
      case _ => ???
    }

    val input = (ich, och) match {
      case (i, o) if i > ichTileSize && o > ochTileSize =>
        val splitIchInput = TypeChecker.check(algo.Map(4, Split.asFunction(Seq(None), Seq(ichTileSize)), slidInput))
        TypeChecker.check(TransposeND(splitIchInput, Seq(0, 2, 4, 3, 5, 1)))
      case (i, o) if i == ichTileSize && o >= ochTileSize =>
        TypeChecker.check(TransposeND(slidInput, Seq(0, 1, 3, 2, 4)))
      case _ => ???
    }

    val mainReadCall = TypeChecker.check(
      {
        val weightParam = ParamDef(weight.t.asInstanceOf[SeqTypeT].et)
        val weightInnerParam = ParamDef(weight.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)

        val weightInner = (ich, och) match {
          case (i, o) if i > ichTileSize && o > ochTileSize => ParamUse(weightInnerParam)
          case (i, o) if i == ichTileSize && o > ochTileSize => ParamUse(weightParam)
          case (i, o) if i == ichTileSize && o == ochTileSize => weight
          case _ => ???
        }

        val inputParam = ParamDef(input.t.asInstanceOf[SeqTypeT].et)
        val inputInner = if(ich > ichTileSize) ParamUse(inputParam) else input

        val innerRes = TypeChecker.check(algo.Map(
          2,
          {
            val param = ParamDef(inputInner.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
            AlgoLambda(
              param,
              {
                val tileRes = TypeChecker.check(FunctionCall(FunctionCall(ParamUse(mainConvFunParam), ParamUse(param)), weightInner))
                tileRes
              }
            )
          }, inputInner
        ))

        (ich, och) match {
          case (i, o) if i > ichTileSize && o > ochTileSize =>
            algo.Map(AlgoLambda(weightParam, TypeChecker.check(algo.Map2Input(AlgoLambda(inputParam, AlgoLambda(weightInnerParam, innerRes)), input, ParamUse(weightParam)))), weight)
          case (i, o) if i == ichTileSize && o > ochTileSize => algo.Map(AlgoLambda(weightParam, innerRes), weight)
          case (i, o) if i == ichTileSize && o == ochTileSize => innerRes
          case _ => ???
        }
      }
    )

    val dropRedundant = if(height < heightTileSize && width < widthTileSize && ich > ichTileSize && och > ochTileSize) {
      TypeChecker.check(algo.Map(4, {
        val param = ParamDef(mainReadCall.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
        AlgoLambda(param, TypeChecker.check(algo.Map(Drop.asFunction(Seq(None), Seq(0, (widthTileSize - width)/2)),
          TypeChecker.check(Drop(ParamUse(param), 0, (heightTileSize - height) / 2)))))
      }, Marker(mainReadCall, TextType("guard"))))
    } else {
      mainReadCall
    }

    val foldSum = if(ich > ichTileSize) {
      TypeChecker.check(algo.Map(
        {
          val param = ParamDef(dropRedundant.t.asInstanceOf[SeqTypeT].et)
          AlgoLambda(
            param,
            {
              val joinInnerAll = TypeChecker.check(algo.Map(
                {
                  val param1 = ParamDef(param.t.asInstanceOf[SeqTypeT].et)
                  AlgoLambda(param1, JoinAll(ParamUse(param1)))
                }, ParamUse(param)
              ))
              val foldAll = TypeChecker.check(
                Fold(
                  {
                    val p1 = ParamDef()
                    val p2 = ParamDef()
                    AlgoLambda(Seq(p1, p2),
                      algo.Map(
                        Add.asFunction(),
                        algo.Zip2(Tuple2(ParamUse(p1), ParamUse(p2)))
                      )
                    )
                  },
                  joinInnerAll
                )
              )
              // Remove outer dims before and after summation
              SplitAll(foldAll, param.t.asInstanceOf[SeqTypeT].dimensions.dropRight(2).reverse)
            }
          )
        }, dropRedundant
      ))
    } else {
      dropRedundant
    }
    foldSum
  }

  def biasTemplate(
    width: Int,
    height: Int,
    ich: Int,
    och: Int,
    widthTileSize: Int = 8,
    heightTileSize: Int = 8,
    ichTileSize: Int = 64,
    ochTileSize: Int = 64,
    bitWidth: Int = 8,
    id: Int = 0,
    bias: Boolean = false,
    foldSum: Expr
  ): Expr = {
    val addBias = if(bias) {
      val initBias = Input("bias" + id, och, 1, SignedIntType(bitWidth * 2))
      (ich, och) match {
        case (i, o) if i >= ichTileSize && o > ochTileSize =>
          val biasInReshaped = TypeChecker.check(algo.Join(initBias))
          val bufferBias = TypeChecker.check(algo.Split(algo.Buffer(biasInReshaped), ochTileSize))
          val innerH = foldSum.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
          val innerW = foldSum.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
          val repBias = TypeChecker.check(
            algo.Map(Repeat.asFunction(Seq(None), Seq(if(height > heightTileSize) height / heightTileSize else 1)),
              algo.Map(Repeat.asFunction(Seq(None), Seq(if(width > widthTileSize) width / widthTileSize else 1)),
                algo.Map(Repeat.asFunction(Seq(None), Seq(innerH)),
                  algo.Map(Repeat.asFunction(Seq(None), Seq(innerW)), algo.Map(2, Repeat.asFunction(Seq(None), Seq(4)), bufferBias))))))
          val zipBias = TypeChecker.check(algo.Map(6, algo.Zip2.asFunction(),algo.Map(5, algo.Zip2.asFunction(),algo.Map(4, algo.Zip2.asFunction(), algo.Map(3, algo.Zip2.asFunction(), algo.Map(2, algo.Zip2.asFunction(),
            algo.Map(algo.Zip2.asFunction(), algo.Zip2(Tuple2(foldSum, repBias)))))))))
          val sum = TypeChecker.check(algo.Map(7, algo.Add.asFunction(), zipBias))
          sum
        case (i, o) if i == ichTileSize && o == ochTileSize =>
          val biasInReshaped = TypeChecker.check(algo.Join(initBias))
          val bufferBias = TypeChecker.check(algo.Buffer(biasInReshaped))
          val innerH = foldSum.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
          val innerW = foldSum.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
          val repBias = TypeChecker.check(algo.Repeat(algo.Repeat(algo.Repeat(algo.Repeat(
            algo.Map(Repeat.asFunction(Seq(None), Seq(4)), bufferBias), innerW), innerH), width / widthTileSize), height / heightTileSize))
          val zipBias = TypeChecker.check(algo.Map(5, algo.Zip2.asFunction(),algo.Map(4, algo.Zip2.asFunction(), algo.Map(3, algo.Zip2.asFunction(), algo.Map(2, algo.Zip2.asFunction(),
            algo.Map(algo.Zip2.asFunction(), algo.Zip2(Tuple2(foldSum, repBias))))))))
          val sum = TypeChecker.check(algo.Map(6, algo.Add.asFunction(), zipBias))
          sum
        case _ => ???
      }
    } else {
      foldSum
    }
    addBias
  }

  def roundingReLUTemplate(
    ich: Int,
    och: Int,
    ichTileSize: Int = 64,
    ochTileSize: Int = 64,
    bitWidth: Int = 8,
    addBias: Expr
  ): Expr = {
    val truncOut = TypeChecker.check(algo.Map(
      (ich, och) match {
        case (i, o) if i >= ichTileSize && o > ochTileSize => 6
        case (i, o) if i == ichTileSize && o == ochTileSize => 5
        case _ => ???
      },
      {
        val baseType = addBias.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et
        val param1 = (ich, och) match {
          case (i, o) if i >= ichTileSize && o > ochTileSize => ParamDef(baseType.asInstanceOf[SeqTypeT].et)
          case (i, o) if i == ichTileSize && o == ochTileSize => ParamDef(baseType)
          case _ => ???
        }

        AlgoLambda(
          param1,
          algo.Map(
            {
              val param2 = ParamDef(param1.t.asInstanceOf[SeqTypeT].et)
              AlgoLambda(
                param2,
                {
                  Max(Tuple2(TypeChecker.check(ClipBankersRound(ParamUse(param2), bitWidth, param2.t.asInstanceOf[SignedIntTypeT].width.ae.evalInt - bitWidth - bitWidth)), ConstantInteger(0, Some(SignedIntType(bitWidth)))))
                }
              )
            }, ParamUse(param1)
          )
        )
      }, addBias
    ))
    truncOut
  }

  def poolingTemplate(
    ich: Int,
    och: Int,
    ichTileSize: Int = 64,
    ochTileSize: Int = 64,
    pool: Boolean = true,
    truncOut: Expr
  ): Expr = {
    val poolOut = if (pool) {
      val poolRes = TypeChecker.check(algo.Map(
        (ich, och) match {
          case (i, o) if i >= ichTileSize && o > ochTileSize => 6
          case (i, o) if i == ichTileSize && o == ochTileSize => 5
          case _ => ???
        },
        {
          val baseType = truncOut.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et
          val param = (ich, och) match {
            case (i, o) if i >= ichTileSize && o > ochTileSize => ParamDef(baseType.asInstanceOf[SeqTypeT].et)
            case (i, o) if i == ichTileSize && o == ochTileSize => ParamDef(baseType)
            case _ => ???
          }

          AlgoLambda(
            param,
            {
              TypeChecker.check(Fold(Max2.asFunction(), ParamUse(param)))
            }
          )
        }, truncOut
      ))

      (ich, och) match {
        case (i, o) if i >= ichTileSize && o > ochTileSize =>
          val reshapedOutput = TypeChecker.check(TransposeND(poolRes, Seq(0, 5, 1, 3, 2, 4)))
          TypeChecker.check(algo.Map(Join.asFunction(), TypeChecker.check(Join(TypeChecker.check(Join(TypeChecker.check(Join(reshapedOutput))))))))
        case (i, o) if i == ichTileSize && o == ochTileSize =>
          val reshapedOutput = TypeChecker.check(TransposeND(poolRes, Seq(0, 1, 3, 2, 4)))
          TypeChecker.check(Join(TypeChecker.check(Join(TypeChecker.check(Join(reshapedOutput))))))
        case _ => ???
      }
    } else {
      val transposeInner = TypeChecker.check(algo.Map(
        (ich, och) match {
          case (i, o) if i >= ichTileSize && o > ochTileSize => 5
          case (i, o) if i == ichTileSize && o == ochTileSize => 4
          case _ => ???
        },
        {
          val baseType = truncOut.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et
          val param = (ich, och) match {
            case (i, o) if i >= ichTileSize && o > ochTileSize => ParamDef(baseType.asInstanceOf[SeqTypeT].et)
            case (i, o) if i == ichTileSize && o == ochTileSize => ParamDef(baseType)
            case _ => ???
          }
          AlgoLambda(param, Split(TypeChecker.check(Transpose(ParamUse(param))), 2))
        }, truncOut
      ))

      (ich, och) match {
        case (i, o) if i >= ichTileSize && o > ochTileSize =>
          val reshapedOutput = TypeChecker.check(TransposeND(transposeInner, Seq(0, 7, 1, 3, 5, 2, 4, 6)))
          val reshapedOutput2 = TypeChecker.check(Join(TypeChecker.check(Join(TypeChecker.check(Join(TypeChecker.check(Join(TypeChecker.check(Join(reshapedOutput))))))))))
          TypeChecker.check(algo.Map({
            val param = ParamDef(reshapedOutput2.t.asInstanceOf[SeqTypeT].et)
            AlgoLambda(param, Join(ParamUse(param)))
          }, reshapedOutput2))
        case (i, o) if i == ichTileSize && o == ochTileSize =>
          val reshapedOutput = TypeChecker.check(TransposeND(transposeInner, Seq(0, 1, 3, 5, 2, 4, 6)))
          val reshapedOutput2 = TypeChecker.check(Join(TypeChecker.check(Join(TypeChecker.check(Join(TypeChecker.check(Join(TypeChecker.check(Join(reshapedOutput))))))))))
          reshapedOutput2
        case _ => ???
      }
    }
    poolOut
  }
}
