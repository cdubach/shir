package algo.nn.templates

import algo._
import core.{Expr, ParamDef, ParamUse, TypeChecker}

object DoubleBufferingFunTemplate {

  /**
   * There is no buffer info at the Algo level. The buffer stuff will be added by rewriting.
   */
  def createSlidingBuffer(
    widthTileSize: Int = 8,
    heightTileSize: Int = 8,
    ichTileSize: Int = 64,
    wSize: Int = 3,
    padSize: Int = 1,
    bitWidth: Int = 8
  ): Expr = {
    val iParam = ParamDef(SeqType(SeqType(SeqType(SignedIntType(bitWidth), ichTileSize), widthTileSize + 2 * padSize), heightTileSize + 2 * padSize))

    val slidInput = TypeChecker.check(
      {
        val bufferInput = TypeChecker.check(ParamUse(iParam))
        val inputSlideHeight = TypeChecker.check(SlideGeneral(bufferInput, wSize, 1))
        val inputSlideWidth = TypeChecker.check(algo.Map(2, SlideGeneral.asFunction(Seq(None), Seq(wSize, 1)), inputSlideHeight))
        val splitSlide = TypeChecker.check(Split(algo.Map(2, Split.asFunction(Seq(None), Seq(2)), inputSlideWidth), 2))
        val transposedSlide = TypeChecker.check(TransposeND(splitSlide, Seq(0, 1, 4, 2, 5, 3, 6)))
        transposedSlide
      }
    )

    val vecBufferInput4Fold = TypeChecker.check(
      algo.Map(
        2,
        {
          val param = ParamDef()
          AlgoLambda(
            param,
            Join(Join(Join(ParamUse(param))))
          )
        }, slidInput
      )
    )

    TypeChecker.check(AlgoLambda(iParam, vecBufferInput4Fold))
  }

  def createRepeatingBuffer(
    widthTileSize: Int = 8,
    heightTileSize: Int = 8,
    ichTileSize: Int = 64,
    ochTileSize: Int = 64,
    wSize: Int = 3,
    bitWidth: Int = 8
  ): Expr = {
    val wParam = ParamDef(SeqType(SeqType(SignedIntType(bitWidth), ichTileSize * wSize * wSize), ochTileSize))

    val repeatWeight = TypeChecker.check(Repeat(Repeat(ParamUse(wParam), widthTileSize / 2), heightTileSize / 2))

    val bufferWeight4Fold = TypeChecker.check(
      algo.Map(3, {
        val param = ParamDef()
        AlgoLambda(
          param,
          Split(ParamUse(param), ochTileSize)
        )
      }, repeatWeight)
    )

    TypeChecker.check(AlgoLambda(wParam, bufferWeight4Fold))
  }
}