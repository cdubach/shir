package algo.nn

import algo._
import core._
import lift.arithmetic
import lift.arithmetic.ceil

/**
 * An IR node that represents a neural network layer. Layers can always be lowered to more primitive operators.
 */
trait NetworkLayerExpr extends AlgoMacroExpr

/**
 * An IR node that represents a data transformation neural network layer that can be expanded to a sequence of more
 * primitive operators.
 */
trait DataTransformationLayerExpr extends NetworkLayerExpr

/**
 * A neural network layer that describes the input of a network.
 */
final case class InputLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): InputLayerExpr = InputLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR
}

object InputLayer {
  /**
   * Creates a layer to describe the input of a network.
   * @param inputChannels The number of input channels.
   * @param inputWidth The input width.
   * @param inputHeight The input height.
   * @param bitWidth The bit width of each input.
   * @param id A unique identifier for the input.
   * @return An input layer.
   */
  def apply(inputChannels: ArithTypeT, inputWidth: ArithTypeT, inputHeight: ArithTypeT, bitWidth: ArithTypeT, id: ArithTypeT): InputLayerExpr = {
    InputLayerExpr(
      algo.Split(
        algo.Input("input" + id.ae.evalInt, inputChannels, inputWidth.ae * inputHeight.ae, algo.SignedIntType(bitWidth)),
        inputWidth),
      Seq(inputChannels, inputWidth, inputHeight, bitWidth, id))
  }

  def unapply(expr: InputLayerExpr): Some[(ArithTypeT, ArithTypeT, ArithTypeT, ArithTypeT, ArithTypeT, Type)] =
    Some((
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.types(1).asInstanceOf[ArithTypeT],
      expr.types(2).asInstanceOf[ArithTypeT],
      expr.types(3).asInstanceOf[ArithTypeT],
      expr.types(4).asInstanceOf[ArithTypeT],
      expr.t
    ))
}

/**
 * A neural network layer that describes an output of a network.
 */
final case class SaveLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): SaveLayerExpr = SaveLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR
}

object SaveLayer {
  /**
   * Creates a new save layer to describe the output of a network.
   * @param result The output that is saved.
   * @return A save layer.
   */
  def apply(result: Expr): SaveLayerExpr = {
    val inputtc = Helpers.checkIfUnknown(result, result.t)
    Helpers.assert(inputtc.t.isInstanceOf[SeqTypeT], "result must be a SeqType!")
    Helpers.assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.length == 3, "result must be 3D!")
    val p = ParamDef(result.t)
    SaveLayerExpr(
      FunctionCall(AlgoLambda(p, Helpers.check(algo.Join(ParamUse(p)))), result),
      Seq())
  }

  def unapply(expr: SaveLayerExpr): Some[(Expr, Type)] =
    Some((
      expr.args.head,
      expr.t
    ))
}

/**
 * A tiled version of a convolution layer in a neural network. The input and weight are tiled before the convolution is
 * applied. The convolution itself is specified as a function, which is applied to the tiled input and weight.
 * The convolution layer is tiled in the width and height dimensions.
 */
final case class TiledConvHWLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends DataTransformationLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TiledConvHWLayerExpr = TiledConvHWLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = {
    this match {
      case TiledConvHWLayer(input, weight, tiledConvFun, _) =>
        implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

        val inputtc = Helpers.checkIfUnknown(input, input.t)
        val weighttc = Helpers.checkIfUnknown(weight, weight.t)
        val tiledConvFuntc = Helpers.checkIfUnknown(tiledConvFun, tiledConvFun.t)

        val inputParam = ParamDef(inputtc.t)
        val weightParam = ParamDef(weighttc.t)

        val tiledConvInputT = tiledConvFuntc.t.asInstanceOf[FunTypeT].inType

        val tileSizeH = tiledConvInputT.asInstanceOf[SeqTypeT].len
        val tileSizeW = tiledConvInputT.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len

        val kernelSizeH = weighttc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
        val kernelSizeW = weighttc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
        val tileStepH = (tileSizeH.ae - kernelSizeH.ae) / 1 + 1
        val tileStepW = (tileSizeW.ae - kernelSizeW.ae) / 1 + 1

        val splitInput = TypeChecker.check(
          SlideGeneral(
            algo.Map(
              SlideGeneral.asFunction(Seq(None), Seq(tileSizeW, tileStepW)),
              ParamUse(inputParam)),
            tileSizeH,
            tileStepH))

        val transposedInput = TypeChecker.check(TransposeND(splitInput, Seq(0, 1, 3, 2, 4)))
        val output = TypeChecker.check({
          val inputTile = ParamDef(transposedInput.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
          val result = algo.Map(2,
            buildLambda(
              inputTile,
              FunctionCall(FunctionCall(tiledConvFun, ParamUse(inputTile)), ParamUse(weightParam))
            ),
            transposedInput)
          FunctionCall(FunctionCall(buildLambda(weightParam, buildLambda(inputParam, result)), weighttc), inputtc)
        })
        val transposedOutput = TypeChecker.check(TransposeND(output, Seq(0, 1, 3, 2, 4)))
        TypeChecker.check(algo.Map(Join.asFunction(), Join(transposedOutput)))
    }
  }
}

object TiledConvHWLayer {
  def apply(input: Expr, weight: Expr, tiledConv: Expr): TiledConvHWLayerExpr = {
    val W = ArithTypeVar()
    val H = ArithTypeVar()
    val ICH = ArithTypeVar()
    val OCH = ArithTypeVar()
    val KW = ArithTypeVar()
    val KH = ArithTypeVar()

    val tiledW = ArithTypeVar()
    val tiledH = ArithTypeVar()

    val idtv = SignedIntTypeVar()

    val instt = SeqType(SeqType(SeqType(idtv, ICH), W), H)
    val wt = SeqType(SeqType(SeqType(SeqType(idtv, ICH), KW), KH), OCH)
    val ost = ConvolutionLayer.computeConvolutionResultType(instt, wt)

    val tiledInstt = SeqType(SeqType(SeqType(idtv, ICH), tiledW), tiledH)
    val tiledWt = wt
    val tiledOst = ConvolutionLayer.computeConvolutionResultType(tiledInstt, tiledWt)

    val tiledConvFun = FunType(Seq(tiledWt, tiledInstt), tiledOst)

    val innerCraziness = FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(W, H, ICH, OCH, KW, KH, idtv, tiledW, tiledH),
      FunType(Seq(instt, wt, tiledConvFun), ost))),
      Seq.fill(9)(UnknownType)), Seq(input, weight, tiledConv))

    TiledConvHWLayerExpr(innerCraziness, Seq())
  }

  def unapply(expr: TiledConvHWLayerExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.args(2),
      expr.t
    ))
}

/**
 * A tiled version of a convolution layer in a neural network. The input and weight are tiled before the convolution is
 * applied. The convolution itself is specified as a function, which is applied to the tiled input and weight.
 * The convolution layer is tiled in the output channels dimension.
 */
final case class TiledConvOCHLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends DataTransformationLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TiledConvOCHLayerExpr = TiledConvOCHLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = {
    this match {
      case TiledConvOCHLayer(input, weight, tiledConvFun, _) =>
        implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)
        val inputtc = Helpers.checkIfUnknown(input, input.t)
        val weighttc = Helpers.checkIfUnknown(weight, weight.t)
        val tiledConvFuntc = Helpers.checkIfUnknown(tiledConvFun, tiledConvFun.t)

        val tiledConvWeightT = tiledConvFuntc.t.asInstanceOf[FunTypeT].outType.asInstanceOf[FunTypeT].inType
        val tileSizeOCH = tiledConvWeightT.asInstanceOf[SeqTypeT].len

        val splitWeight = TypeChecker.check(Split(weighttc, tileSizeOCH))
        val output = TypeChecker.check({
          algo.Map(
            FunctionCall(tiledConvFun, inputtc),
            splitWeight)
        })

        val transposedOutput = TypeChecker.check(TransposeND(output, Seq(0, 3, 1, 2)))
        TypeChecker.check(algo.Map(2, Join.asFunction(), transposedOutput))
    }
  }
}

object TiledConvOCHLayer {
  def apply(input: Expr, weight: Expr, tiledConv: Expr): TiledConvOCHLayerExpr = {
    val W = ArithTypeVar()
    val H = ArithTypeVar()
    val ICH = ArithTypeVar()
    val OCH = ArithTypeVar()
    val KW = ArithTypeVar()
    val KH = ArithTypeVar()

    val TiledOCH = ArithTypeVar()

    val idtv = SignedIntTypeVar()

    val instt = SeqType(SeqType(SeqType(idtv, ICH), W), H)
    val wt = SeqType(SeqType(SeqType(SeqType(idtv, ICH), KW), KH), OCH)
    val ost = ConvolutionLayer.computeConvolutionResultType(instt, wt)

    val tiledInstt = SeqType(SeqType(SeqType(idtv, ICH), W), H)
    val tiledWt = SeqType(SeqType(SeqType(SeqType(idtv, ICH), KW), KH), TiledOCH)
    val tiledOst = ConvolutionLayer.computeConvolutionResultType(tiledInstt, tiledWt)

    val tiledConvFun = FunType(Seq(tiledWt, tiledInstt), tiledOst)

    val innerCraziness = FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(W, H, ICH, OCH, KW, KH, idtv, TiledOCH),
      FunType(Seq(instt, wt, tiledConvFun), ost))),
      Seq.fill(8)(UnknownType)), Seq(input, weight, tiledConv))

    TiledConvOCHLayerExpr(innerCraziness, Seq())
  }

  def unapply(expr: TiledConvOCHLayerExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.args(2),
      expr.t
    ))
}

final case class TiledConvICHLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends DataTransformationLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TiledConvICHLayerExpr = TiledConvICHLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = {
    this match {
      case TiledConvICHLayer(input, weight, tiledConvFun, _) =>
        implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)
        val inputtc = Helpers.checkIfUnknown(input, input.t)
        val weighttc = Helpers.checkIfUnknown(weight, weight.t)
        val tiledConvFuntc = Helpers.checkIfUnknown(tiledConvFun, tiledConvFun.t)
        val tiledConvInputT = tiledConvFuntc.t.asInstanceOf[FunTypeT].inType
        val tileSizeICH = tiledConvInputT.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len

        val splitWeight = TypeChecker.check(algo.Map(3, Split.asFunction(Seq(None), Seq(tileSizeICH)), weighttc))
        val transposedWeight = TypeChecker.check(TransposeND(splitWeight, Seq(0, 2, 3, 4, 1)))

        val splitInput = TypeChecker.check(algo.Map(2, Split.asFunction(Seq(None), Seq(tileSizeICH)), inputtc))
        val transposedInput = TypeChecker.check(TransposeND(splitInput, Seq(0, 2, 3, 1)))

        val output = TypeChecker.check({
          algo.Map2Input(
            tiledConvFun,
            transposedInput, transposedWeight
          )
        })

        val newCH = output.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len.ae
        val newWidth = output.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len.ae

        val joinedInnerOutput = TypeChecker.check(algo.Map(Join.asFunction(), algo.Map(Join.asFunction(), output)))
        val joinedLen = joinedInnerOutput.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len.ae

        val reducedOutput = TypeChecker.check(algo.Fold({
          val param1 = ParamDef(SeqType(AlgoDataTypeVar(), joinedLen))
          val param2 = ParamDef(SeqType(AlgoDataTypeVar(), joinedLen))
          buildLambda(
            param1,
            buildLambda(
              param2,
              algo.Map(algo.Add.asFunction(), algo.Zip2(algo.Tuple2(ParamUse(param1), ParamUse(param2))))
            )
          )
        }, joinedInnerOutput))
        TypeChecker.check(Split(Split(reducedOutput, newCH), newWidth))
    }
  }
}

object TiledConvICHLayer{
  def apply(input: Expr, weight: Expr, tiledConv: Expr): TiledConvICHLayerExpr = {
    val W = ArithTypeVar()
    val H = ArithTypeVar()
    val ICH = ArithTypeVar()
    val OCH = ArithTypeVar()
    val KW = ArithTypeVar()
    val KH = ArithTypeVar()

    val TiledICH = ArithTypeVar()

    val idtv = SignedIntTypeVar()

    val instt = SeqType(SeqType(SeqType(idtv, ICH), W), H)
    val wt = SeqType(SeqType(SeqType(SeqType(idtv, ICH), KW), KH), OCH)
    val ost = ConvolutionLayer.computeConvolutionResultType(instt, wt)

    val tiledInstt = SeqType(SeqType(SeqType(idtv, TiledICH), W), H)
    val tiledWt = SeqType(SeqType(SeqType(SeqType(idtv, TiledICH), KW), KH), OCH)
    val tiledOst = ConvolutionLayer.computeConvolutionResultType(tiledInstt, tiledWt)

    val tiledConvFun = FunType(Seq(tiledWt, tiledInstt), tiledOst)

    val innerCraziness = FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(W, H, ICH, OCH, KW, KH, idtv, TiledICH),
      FunType(Seq(instt, wt, tiledConvFun), ost))),
      Seq.fill(8)(UnknownType)), Seq(input, weight, tiledConv))

    TiledConvICHLayerExpr(innerCraziness, Seq())
  }

  def unapply(expr: TiledConvICHLayerExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.args(2),
      expr.t
    ))
}

/**
 * A padded version of a convolution layer in a neural network. The input and weight are tiled before the convolution is
 * applied. The convolution itself is specified as a function, which is applied to the tiled input and weight.
 * The convolution layer is tiled in the width and height dimensions.
 */
final case class PaddedConvHWLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends DataTransformationLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): PaddedConvHWLayerExpr = PaddedConvHWLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = {
    this match {
      case PaddedConvHWLayer(input, weight, paddedConvFun, _) =>
        implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)
        val inputtc = Helpers.checkIfUnknown(input, input.t)
        val weighttc = Helpers.checkIfUnknown(weight, weight.t)

        val paddedConvFuntc = Helpers.checkIfUnknown(paddedConvFun, paddedConvFun.t)

        val paddedConvInputT = paddedConvFuntc.t.asInstanceOf[FunTypeT].inType

        val H = inputtc.t.asInstanceOf[SeqTypeT].len
        val W = inputtc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
        val padSizeH = paddedConvInputT.asInstanceOf[SeqTypeT].len
        val padSizeW = paddedConvInputT.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len

        val paddedInput = TypeChecker.check(algo.Pad(algo.Map(algo.Pad.asFunction(Seq(None), Seq(0, padSizeW.ae - W.ae, 0)), inputtc), 0, padSizeH.ae - H.ae, 0))

        val output = TypeChecker.check(FunctionCall(FunctionCall(paddedConvFun, paddedInput), weighttc))
        TypeChecker.check(Drop(algo.Map(Drop.asFunction(Seq(None), Seq(0, padSizeW.ae - W.ae)), output), 0, padSizeH.ae - H.ae))
    }
  }
}

object PaddedConvHWLayer {
  def apply(input: Expr, weight: Expr, paddedConv: Expr): PaddedConvHWLayerExpr = {
    val W = ArithTypeVar()
    val H = ArithTypeVar()
    val ICH = ArithTypeVar()
    val OCH = ArithTypeVar()
    val KW = ArithTypeVar()
    val KH = ArithTypeVar()

    val paddedW = ArithTypeVar()
    val paddedH = ArithTypeVar()

    val idtv = SignedIntTypeVar()

    val instt = SeqType(SeqType(SeqType(idtv, ICH), W), H)
    val wt = SeqType(SeqType(SeqType(SeqType(idtv, ICH), KW), KH), OCH)
    val ost = ConvolutionLayer.computeConvolutionResultType(instt, wt)

    val paddedInstt = SeqType(SeqType(SeqType(idtv, ICH), paddedW), paddedH)
    val paddedWt = wt
    val paddedOst = ConvolutionLayer.computeConvolutionResultType(paddedInstt, paddedWt)

    val PaddedConvFun = FunType(Seq(paddedWt, paddedInstt), paddedOst)

    val innerCraziness = FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(W, H, ICH, OCH, KW, KH, idtv, paddedW, paddedH),
      FunType(Seq(instt, wt, PaddedConvFun), ost))),
      Seq.fill(9)(UnknownType)), Seq(input, weight, paddedConv))

    PaddedConvHWLayerExpr(innerCraziness, Seq())
  }

  def unapply(expr: PaddedConvHWLayerExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.args(2),
      expr.t
    ))
}

/**
 * A convolution layer in a neural network.
 */
final case class ConvolutionLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ConvolutionLayerExpr = ConvolutionLayerExpr(newInnerIR, newTypes)

  def W: ArithTypeT = this match {
    case ConvolutionLayer(in, _, _) => ConvolutionLayer.getW(in)
  }

  def H: ArithTypeT = this match {
    case ConvolutionLayer(in, _, _) => ConvolutionLayer.getH(in)
  }

  def ICH: ArithTypeT = this match {
    case ConvolutionLayer(in, _, _) => ConvolutionLayer.getICH(in)
  }

  def OCH: ArithTypeT = this match {
    case ConvolutionLayer(_, weight, _) => ConvolutionLayer.getOCH(weight)
  }

  override def expand(): Expr = {
    this match {
      case ConvolutionLayer(input, weight, _) =>
        implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

        val inputtc = Helpers.checkIfUnknown(input, input.t)
        Helpers.assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")
        Helpers.assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.length == 3, "input must be 3D!")
        // Helpers.assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.head.ae.evalInt == inputChannels.ae.evalInt, "Layer and input channels do not match!")

        val weighttc = Helpers.checkIfUnknown(weight, weight.t)

        val kernelSizeH = weighttc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len.ae
        val kernelSizeW = weighttc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len.ae
        val kernelIch = weighttc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len.ae
        val inputBasicType = inputtc.t.asInstanceOf[SeqTypeT].leafType
        // TODO: check weight type to conform to the same type as
        // algo.Input("weight" + id, inputChannels, kernelSize.ae * kernelSize.ae * kernelNum.ae, algo.SignedIntType(weightBitWidth))

        val inputParam = ParamDef(inputtc.t)
        val weightParam = ParamDef(weighttc.t)

        val slideHeight = Helpers.check(algo.SlideGeneral(ParamUse(inputParam), kernelSizeH, 1))

        val slideWidth = Helpers.check(algo.Map(2, algo.SlideGeneral.asFunction(Seq(None), Seq(kernelSizeW, 1)),
          slideHeight))

        val inputSlided = Helpers.check(algo.TransposeND(slideWidth, Seq(0, 1, 3, 2, 4)))

        val paramX = ParamDef(inputSlided.t.asInstanceOf[SeqTypeT].et)
        val inputSemiReshapedLambda = Helpers.check({
          val param = ParamDef(inputSlided.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
          AlgoLambda(param, Join(ParamUse(param)))
        })
        val inputSemiReshapedInner = Helpers.check(algo.Map(inputSemiReshapedLambda, ParamUse(paramX)))
        val inputSemiReshaped = Helpers.check(algo.Map(AlgoLambda(paramX, inputSemiReshapedInner), inputSlided))
        val inputReshaped = Helpers.check(algo.Map(2, algo.Join.asFunction(), inputSemiReshaped))

        val weightReshaped = algo.Map(Join.asFunction(), algo.Map(Join.asFunction(), ParamUse(weightParam)))
        assert(TypeChecker.check(weightReshaped).t == SeqType(SeqType(inputBasicType, kernelSizeH * kernelSizeW * kernelIch), OCH))

        val conv = algo.Map(
          2,
          {
            val paramI = ParamDef(SeqType(inputBasicType, kernelSizeH * kernelSizeW * kernelIch))
            AlgoLambda(
              paramI,
              MatVecLayer(weightReshaped, ParamUse(paramI))
            )
          }, inputReshaped
        )

        FunctionCall(FunctionCall(AlgoLambda(weightParam, AlgoLambda(inputParam, conv)), weight), input)
    }
  }

  def padHW(padSizeW: ArithTypeT, padSizeH: ArithTypeT)(implicit buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)): Expr = {
    this match {
      case ConvolutionLayer(input, weight, _) =>
        val inputtc = Helpers.checkIfUnknown(input, input.t)
        val weighttc = Helpers.checkIfUnknown(weight, weight.t)

        val paddedInput = TypeChecker.check(algo.Pad(algo.Map(algo.Pad.asFunction(Seq(None), Seq(0, padSizeW, 0)), inputtc), 0, padSizeH, 0))

        val inputPadParam = ParamDef(paddedInput.t)
        val weightParam = ParamDef(weighttc.t)

        PaddedConvHWLayer(
          inputtc,
          weighttc,
          buildLambda(
            inputPadParam,
            buildLambda(
              weightParam,
              ConvolutionLayer(ParamUse(inputPadParam), ParamUse(weightParam)))))
    }
  }

  def tileHW(tileSizeW: ArithTypeT, tileSizeH: ArithTypeT)(implicit buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)): Expr = {
    this match {
      case ConvolutionLayer(input, weight, _) =>
        val inputtc = Helpers.checkIfUnknown(input, input.t)
        val weighttc = Helpers.checkIfUnknown(weight, weight.t)

        val kernelSizeH = weighttc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
        val kernelSizeW = weighttc.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
        val tileStepH = (tileSizeH.ae - kernelSizeH.ae) / 1 + 1
        val tileStepW = (tileSizeW.ae - kernelSizeW.ae) / 1 + 1

        val splitInput = TypeChecker.check(SlideGeneral(algo.Map(SlideGeneral.asFunction(Seq(None), Seq(tileSizeW, tileStepW)), inputtc), tileSizeH, tileStepH))
        val transposedInput = TypeChecker.check(TransposeND(splitInput, Seq(0, 1, 3, 2, 4)))

        val inputTileParam = ParamDef(transposedInput.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
        val weightParam = ParamDef(weighttc.t)
        TiledConvHWLayer(
          inputtc,
          weighttc,
          buildLambda(
            inputTileParam,
            buildLambda(
              weightParam,
              ConvolutionLayer(ParamUse(inputTileParam), ParamUse(weightParam)))))
    }
  }

  def tileOCH(tileSizeOCH: ArithTypeT)(implicit buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)): Expr = {
    this match {
      case ConvolutionLayer(input, weight, _) =>

        val inputtc = Helpers.checkIfUnknown(input, input.t)
        val weighttc = Helpers.checkIfUnknown(weight, weight.t)

        val splitWeight = TypeChecker.check(Split(weighttc, tileSizeOCH))

        val inputParam = ParamDef(inputtc.t)
        val weightTileParam = ParamDef(splitWeight.t.asInstanceOf[SeqTypeT].et)
        TiledConvOCHLayer(
          inputtc,
          weighttc,
          buildLambda(
            inputParam,
            buildLambda(
              weightTileParam,
              ConvolutionLayer(ParamUse(inputParam), ParamUse(weightTileParam)))))
    }
  }

  def tileICH(tileSizeICH: ArithTypeT)(implicit buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)): Expr = {
    this match {
      case ConvolutionLayer(input, weight, _) =>
        implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

        val inputtc = Helpers.checkIfUnknown(input, input.t)
        val weighttc = Helpers.checkIfUnknown(weight, weight.t)

        val splitWeight = TypeChecker.check(algo.Map(3, Split.asFunction(Seq(None), Seq(tileSizeICH)), weighttc))
        val transposedWeight = TypeChecker.check(TransposeND(splitWeight, Seq(0, 2, 3, 4, 1)))

        val splitInput = TypeChecker.check(algo.Map(2, Split.asFunction(Seq(None), Seq(tileSizeICH)), inputtc))
        val transposedInput = TypeChecker.check(TransposeND(splitInput, Seq(0, 2, 3, 1)))

        val inputTileParam = ParamDef(transposedInput.t.asInstanceOf[SeqTypeT].et)
        val weightTileParam = ParamDef(transposedWeight.t.asInstanceOf[SeqTypeT].et)

        TiledConvICHLayer(
          inputtc,
          weighttc,
          buildLambda(
            inputTileParam,
            buildLambda(
              weightTileParam,
              ConvolutionLayer(ParamUse(inputTileParam), ParamUse(weightTileParam)))))
    }
  }
}

object ConvolutionLayer {
  def getW(in: Expr): ArithTypeT = {
    in.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
  }

  def getH(in: Expr): ArithTypeT = {
    in.t.asInstanceOf[SeqTypeT].len
  }

  def getICH(in: Expr): ArithTypeT = {
    in.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqType].len
  }

  def getOCH(weight: Expr): ArithTypeT = {
    weight.t.asInstanceOf[SeqTypeT].len
  }

  def getKW(weight: Expr): ArithTypeT = {
    weight.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
  }

  def getKH(weight: Expr): ArithTypeT = {
    weight.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].len
  }

  private def computeType(input: AlgoDataTypeT, weight: AlgoDataTypeT, func: (SignedIntType, ArithTypeT, ArithTypeT, ArithTypeT, ArithTypeT, ArithTypeT, ArithTypeT) => Type): ConditionalTypeVar = {
    ConditionalTypeVar(
      TupleType(input, weight), {
        case TupleType(Seq(
        SeqType(SeqType(SeqType(_, _), w: ArithType), h: ArithType),
        SeqType(SeqType(SeqType(SeqType(idt: SignedIntType, ich: ArithType), kw: ArithType), kh: ArithType), och: ArithType))) =>

          func(idt, w, h, ich, och, kw, kh)
      })
  }

  private[nn] def computeConvolutionResultType(input: AlgoDataTypeT, weight: AlgoDataTypeT): ConditionalTypeVar = {
    computeType(input, weight, (idt, W, H, ICH, OCH, KW, KH) => {
      val odt = SignedIntType(2 * idt.width.ae + ceil(arithmetic.Log(2, KW.ae * KH.ae * ICH.ae)))
      SeqType(SeqType(SeqType(odt, OCH), W.ae - KW.ae + 1), H.ae - KH.ae + 1)
    })
  }

  private[nn] def computeMatVecInputType(input: AlgoDataTypeT, weight: AlgoDataTypeT): Type = {
    computeType(input, weight, (idt, W, H, ICH, OCH, KW, KH) => {
      computeMatVecInputType(idt, ICH, KW, KH)
    })
  }

  private[nn] def computeMatVecInputType(idt: SignedIntTypeT, ICH: ArithTypeT, KW: ArithTypeT, KH: ArithTypeT): Type = {
    SeqType(idt, KH.ae * KW.ae * ICH.ae)
  }

  private[nn] def computeMatVecWeightType(input: AlgoDataTypeT, weight: AlgoDataTypeT): Type = {
    computeType(input, weight, (idt, W, H, ICH, OCH, KW, KH) => {
      computeMatVecWeightType(idt, ICH, OCH, KW, KH)
    })
  }

  private[nn] def computeMatVecWeightType(idt: SignedIntTypeT, ICH: ArithTypeT, OCH: ArithTypeT, KW: ArithTypeT, KH: ArithTypeT): Type = {
    SeqType(SeqType(idt, KH.ae * KW.ae * ICH.ae), OCH.ae)
  }

  private[nn] def computeMatVecOutputType(input: AlgoDataTypeT, weight: AlgoDataTypeT): Type = {
    computeType(input, weight, (idt, W, H, ICH, OCH, KW, KH) => {
      computeMatVecOutputType(idt, ICH, OCH, KW, KH)
    })
  }

  private[nn] def computeMatVecOutputType(idt: SignedIntTypeT, ICH: ArithTypeT, OCH: ArithTypeT, KW: ArithTypeT, KH: ArithTypeT): Type = {
    val odt = SignedIntType(2 * idt.width.ae + ceil(arithmetic.Log(2, KW.ae * KH.ae * ICH.ae)))
    SeqType(odt, OCH.ae)
  }

  /**
   * Creates a new convolution layer.
   * @param input An input to the convolution layer.
   * @param inputChannels The number of input channels in the input data.
   * @param kernelNum The number of convolution kernels or filters to be applied.
   * @param kernelSize The size of each convolution kernel.
   * @param padSize The size of zero-padding to be applied to the input data.
   * @param weightBitWidth The bit width of the weights used in the convolution layer.
   * @param id An identifier for the convolution layer.
   * @return A `ConvolutionLayerExpr` representing the newly created convolution layer.
   */
  def apply(input: Expr,
            inputChannels: ArithTypeT,
            kernelNum: ArithTypeT,
            kernelSize: ArithTypeT,
            padSize: ArithTypeT,
            weightBitWidth : ArithTypeT,
            id: ArithTypeT): ConvolutionLayerExpr = {

    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)
    ConvolutionLayer(
      PaddingLayer(input, padSize),
      Helpers.check(algo.Map(algo.Split.asFunction(Seq(None), Seq(kernelSize)),
        algo.Map(algo.Split.asFunction(Seq(None), Seq(inputChannels)), algo.Input(
          "weight" + id.ae.evalInt,
          kernelSize.ae * kernelSize.ae * inputChannels.ae,
          kernelNum,
          algo.SignedIntType(weightBitWidth)))))
    )
  }

  /**
   * Creates a new convolution layer.
   * @param input An input to the convolution layer.
   * @param weight The bit width of the weights used in the convolution layer.
   * @return A `ConvolutionLayerExpr` representing the newly created convolution layer.
   */
  def apply(input: Expr, weight: Expr): ConvolutionLayerExpr = {
    val W = ArithTypeVar()
    val H = ArithTypeVar()
    val ICH = ArithTypeVar()
    val OCH = ArithTypeVar()
    val KW = ArithTypeVar()
    val KH = ArithTypeVar()

    val idtv = SignedIntTypeVar()

    val instt = SeqType(SeqType(SeqType(idtv, ICH), W), H)
    val wt = SeqType(SeqType(SeqType(SeqType(idtv, ICH), KW), KH), OCH)
    val ost = ConvolutionLayer.computeConvolutionResultType(instt, wt)

    val innerCraziness = FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(W, H, ICH, OCH, KW, KH, idtv),
      FunType(Seq(instt, wt), ost))),
      Seq.fill(7)(UnknownType)), Seq(input, weight))

    ConvolutionLayerExpr(innerCraziness, Seq())
  }

  def unapply(expr: ConvolutionLayerExpr): Some[(Expr, Expr, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.t
    ))
}


/**
 * A Padding layer in a neural network.
 */
final case class PaddingLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): PaddingLayerExpr = PaddingLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR
}

object PaddingLayer {
  /**
   * Padding with uniform size.
   */
  def apply(input: Expr, padSize: ArithTypeT): PaddingLayerExpr =
    PaddingLayer(input, padSize, padSize, padSize, padSize)

  /**
   * A Padding layer in a neural network. There are four directions: Top, Bottom, Left, and Right.
   * @param input An input to the convolution layer.
   * @param padTopSize The size of zero-padding to be applied to the input data.
   * @param padBottomSize The size of zero-padding to be applied to the input data.
   * @param padLeftSize The size of zero-padding to be applied to the input data.
   * @param padRightSize The size of zero-padding to be applied to the input data.
   * @return A `PaddingLayerExpr` representing the newly created zero-padding for the input.
   */
  def apply(input: Expr, padTopSize: ArithTypeT, padBottomSize: ArithTypeT, padLeftSize: ArithTypeT, padRightSize: ArithTypeT): PaddingLayerExpr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

    val inputtc = Helpers.checkIfUnknown(input, input.t)
    Helpers.assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")
    Helpers.assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.length == 3, "input must be 3D!")

    val inputParam = ParamDef(inputtc.t)

    val inputPadded = algo.Map(algo.Pad.asFunction(Seq(None), Seq(padLeftSize, padRightSize, 0)),
      algo.Pad(ParamUse(inputParam), padTopSize, padBottomSize, 0))

    PaddingLayerExpr(FunctionCall(AlgoLambda(inputParam, inputPadded), input),
      Seq(padTopSize, padBottomSize, padLeftSize, padRightSize))
  }

  def unapply(expr: PaddingLayerExpr): Some[(Expr, ArithTypeT, ArithTypeT, ArithTypeT, ArithTypeT, Type)] =
    Some((
      expr.args.head,
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.types(1).asInstanceOf[ArithTypeT],
      expr.types(2).asInstanceOf[ArithTypeT],
      expr.types(3).asInstanceOf[ArithTypeT],
      expr.t
    ))
}

final case class PoolingLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): PoolingLayerExpr = PoolingLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR
}

object PoolingLayer {
  def apply(input: Expr, kernelSize: ArithTypeT): PoolingLayerExpr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

    val inputtc = Helpers.checkIfUnknown(input, input.t)
    Helpers.assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")
    Helpers.assert(inputtc.t.asInstanceOf[SeqTypeT].dimensions.length == 3, "input must be 3D!")

    val inputParam = ParamDef(inputtc.t)

    val splitWins = Helpers.check(algo.Split(algo.Map(algo.Split.asFunction(Seq(None), Seq(kernelSize)), ParamUse(inputParam)), kernelSize))
    val transposedConv = Helpers.check(algo.TransposeND(splitWins, Seq(1, 3, 0, 2, 4)))
    val joinedTpConv = Helpers.check(algo.Map(3, algo.Join.asFunction(), transposedConv))
    val maxRes = Helpers.check(algo.Map(3, {
      val param = ParamDef(joinedTpConv.t.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et.asInstanceOf[SeqTypeT].et)
      AlgoLambda(
        param,
        algo.Fold(algo.Max2.asFunction(), ParamUse(param))
      )
    }, joinedTpConv))

    PoolingLayerExpr(FunctionCall(AlgoLambda(inputParam, maxRes), input), Seq(kernelSize))
  }

  def unapply(expr: PoolingLayerExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((
      expr.args.head,
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.t
    ))
}

/**
 * A rounding layer in a neural network.
 */
final case class RoundingLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): RoundingLayerExpr = RoundingLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR
}

object RoundingLayer {
  def apply(input: Expr, bitWidth: ArithTypeT, bitRound: ArithTypeT): RoundingLayerExpr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

    val inputtc = Helpers.checkIfUnknown(input, input.t)
    Helpers.assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")
    val dimLevels = inputtc.t.asInstanceOf[SeqTypeT].dimensions.length
    val newElementWidth = inputtc.t.asInstanceOf[SeqTypeT].leafType.asInstanceOf[algo.SignedIntTypeT].width.ae
    val p = ParamDef(inputtc.t)
    val round = Helpers.check(algo.Map(dimLevels, algo.ClipBankersRound.asFunction(Seq(None),
      Seq(bitRound, newElementWidth - bitWidth.ae - bitRound.ae)), ParamUse(p)))

    RoundingLayerExpr(
      FunctionCall(AlgoLambda(p, round), input),
      Seq(bitWidth, bitRound))
  }

  def unapply(expr: RoundingLayerExpr): Some[(Expr, ArithTypeT, ArithTypeT, Type)] =
    Some((
      expr.args.head,
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.types(1).asInstanceOf[ArithTypeT],
      expr.t
    ))
}

/**
 * A dot product layer that is implemented by splitting the inputs and feeding them to a parallel dot product.
 * The results of the parallel dot products are then reduced to a single value.
 * @param innerIR The inner IR of the layer.
 * @param types The types of the layer.
 */
final case class TiledDotProductLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TiledDotProductLayerExpr = TiledDotProductLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR
}

/**
 * A dot product layer that is implemented by splitting the inputs and feeding them to a parallel dot product.
 * The results of the parallel dot products are then reduced to a single value.
 */
object TiledDotProductLayer {
  def apply(left: Expr, right: Expr, tileSize: ArithTypeT, parallelDotProductFunc: Expr)
           (implicit buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)): TiledDotProductLayerExpr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)
    val lefttc = Helpers.checkIfUnknown(left, left.t)
    val righttc = Helpers.checkIfUnknown(right, right.t)
    val functtc = Helpers.checkIfUnknown(parallelDotProductFunc, parallelDotProductFunc.t)

    val leftParam = ParamDef(lefttc.t)
    val rightParam = ParamDef(righttc.t)
    val funcParam = ParamDef(functtc.t)

    val impl = {
      val splitLeft = Split(ParamUse(leftParam), tileSize)
      val splitRight = Split(ParamUse(rightParam), tileSize)
      val partialProducts = TypeChecker.check(FunctionCall(ParamUse(funcParam), Seq(splitLeft, splitRight).reverse))
      val partialProductsType = partialProducts.t.asInstanceOf[SeqTypeT]
      val elementType = partialProductsType.et.asInstanceOf[SignedIntType]
      val len = partialProductsType.len.ae
      val accumulatorVar = ParamDef(SignedIntType(elementType.width.ae + ceil(arithmetic.Log(2, len))))
      val elementVar = ParamDef(elementType)
      Fold(
        buildLambda(elementVar,
          buildLambda(accumulatorVar,
            algo.Add(algo.Tuple(ParamUse(accumulatorVar), ParamUse(elementVar))))),
        partialProducts)
    }

    TiledDotProductLayerExpr(
      FunctionCall(
        FunctionCall(
          FunctionCall(
            buildLambda(funcParam,
              buildLambda(rightParam,
                buildLambda(leftParam, impl))),
            parallelDotProductFunc),
          right),
        left),
      Seq(tileSize))
  }

  def unapply(expr: TiledDotProductLayerExpr): Some[(Expr, Expr, ArithTypeT, Expr, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.args(2),
      expr.t
    ))
}

/**
 * A dot product layer in a neural network.
 * @param innerIR The inner IR of the layer.
 * @param types The types of the layer.
 */
final case class DotProductLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): DotProductLayerExpr =
    DotProductLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR

  /**
    * Rewrites the dot product as a parallel dot product followed by a reduction.
    * @param tileSize The size of the tiles to use for the parallel dot product.
    * @param parallelProductsPerTile The number of dot products to compute in parallel per tile.
    * @return The rewritten expression.
    */
  def tile(tileSize: ArithTypeT, parallelProductsPerTile: ArithTypeT)
          (implicit buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)): TiledDotProductLayerExpr = {
    this match {
      case DotProductLayer(left, right, _) =>
        val splitLeft = TypeChecker.check(Split(left, tileSize))
        val splitRight = TypeChecker.check(Split(right, tileSize))
        val splitLeftParam = ParamDef(splitLeft.t)
        val splitRightParam = ParamDef(splitRight.t)
        val parallelDotProductsFunc = buildLambda(
          splitLeftParam,
          buildLambda(
            splitRightParam,
            ParallelDotProductsLayer(ParamUse(splitLeftParam), ParamUse(splitRightParam), parallelProductsPerTile)))

        TiledDotProductLayer(left, right, tileSize, parallelDotProductsFunc)
    }
  }
}

/**
 * A dot product layer in a neural network.
 */
object DotProductLayer extends BuiltinExprFun {
  /**
   * Creates a layer that computes a dot product.
   *
   * @param left An input vector operand.
   * @param right Another input vector operand,
   * @return A scalar value corresponding to the dot product of `left` and `right`.
   */
  def apply(left: Expr, right: Expr): DotProductLayerExpr = {
    val lefttc = Helpers.checkIfUnknown(left, left.t)
    val righttc = Helpers.checkIfUnknown(right, right.t)

    if (!lefttc.t.hasUnknownType) {
      Helpers.assert(lefttc.t.isInstanceOf[SeqTypeT], "left must be a SeqType!")
    }
    if (!righttc.t.hasUnknownType) {
      Helpers.assert(righttc.t.isInstanceOf[SeqTypeT], "right must be a SeqType!")
    }

    val p1 = ParamDef(left.t)
    val p2 = ParamDef(right.t)

    DotProductLayerExpr(
      FunctionCall(
        FunctionCall(
          AlgoLambda(p2, AlgoLambda(p1, algo.DotProduct(ParamUse(p1), ParamUse(p2)))),
          right),
        left),
      Seq())
  }

  def unapply(expr: DotProductLayerExpr): Some[(Expr, Expr, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.t
    ))

  override def args: Int = 2

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): Expr = apply(inputs.head, inputs(1))
}

/**
 * A layer in a neural network that computes a sequence of dot products in parallel.
 * @param innerIR The inner IR of the layer.
 * @param types The types of the layer.
 */
final case class ParallelDotProductsLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  this match {
    case ParallelDotProductsLayer(left, right, simultaneousProducts, _) if !left.t.hasUnknownType && !right.t.hasUnknownType && !simultaneousProducts.hasUnknownType =>
      Helpers.assert(left.t.isInstanceOf[SeqTypeT], "left must be a SeqType!")
      Helpers.assert(right.t.isInstanceOf[SeqTypeT], "right must be a SeqType!")
      Helpers.assert(left.t.asInstanceOf[SeqTypeT].dimensions.length == 2, "left must be 2D!")
      Helpers.assert(right.t.asInstanceOf[SeqTypeT].dimensions.length == 2, "right must be 2D!")
      Helpers.assert(simultaneousProducts.ae.evalLong > 0, "simultaneousProducts must be greater than 0")
      Helpers.assert(simultaneousProducts.ae.evalLong <= left.t.asInstanceOf[SeqTypeT].len.ae.evalLong, "simultaneousProducts must be less than or equal to the length of the input vectors")
      val vectorCount = left.t.asInstanceOf[SeqTypeT].len
      Helpers.assert(vectorCount.ae.evalLong % simultaneousProducts.ae.evalLong == 0, "Number of vectors must be a multiple of simultaneousProducts")
    case _ =>
  }

  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ParallelDotProductsLayerExpr =
    ParallelDotProductsLayerExpr(newInnerIR, newTypes)

  override def expand(): Expr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)
    this match {
      case ParallelDotProductsLayer(left, right, simultaneousProducts, _) =>
        val leftLen = left.t.asInstanceOf[SeqTypeT].len
        if (simultaneousProducts.ae.isEvaluable && leftLen.ae.isEvaluable && simultaneousProducts.ae.evalLong == leftLen.ae.evalLong) {
          // If the number of simultaneous products is equal to the length of the input vectors, we can use a single parallel map.
          algo.MapParallel(DotProductLayer.asFunction(), left, right)
        } else if (simultaneousProducts.ae.isEvaluable && simultaneousProducts.ae.evalLong == 1) {
          // If the number of simultaneous products is 1, we can use a single sequential map.
          sequential
        } else {
          // Otherwise, we need to split the input vectors into chunks. Chunks are processed sequentially but dot
          // products within a chunk are processed in parallel.
          val splitLeft = TypeChecker.check(Split(left, simultaneousProducts))
          val splitRight = TypeChecker.check(Split(right, simultaneousProducts))
          algo.Join(
            algo.MapAsync({
                val chunkLeft = ParamDef(splitLeft.t.asInstanceOf[SeqTypeT].et)
                val chunkRight = ParamDef(splitRight.t.asInstanceOf[SeqTypeT].et)
                buildLambda(chunkLeft, buildLambda(chunkRight,
                  algo.MapParallel(DotProductLayer.asFunction(), ParamUse(chunkLeft), ParamUse(chunkRight))))
              },
              splitLeft, splitRight))
        }
    }
  }

  /**
    * Rewrites the parallel dot products layer as a partially expanded layer. This is a layer where the input vectors
    * are split into chunks and dot products within a chunk are processed in parallel by a ParallelDotProductsLayer.
    * No transformation is performed if the number of simultaneous products is equal to the length of the input vectors
    * or if the number of simultaneous products is 1.
    * @return The partially expanded layer.
    */
  def partiallyExpanded: Expr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)
    this match {
      case ParallelDotProductsLayer(left, right, simultaneousProducts, _) =>
        val leftLen = left.t.asInstanceOf[SeqTypeT].len
        if (simultaneousProducts.ae.isEvaluable && leftLen.ae.isEvaluable && simultaneousProducts.ae.evalLong == leftLen.ae.evalLong) {
          // If the number of simultaneous products is equal to the length of the input vectors, we can use a single parallel map.
          this
        } else if (simultaneousProducts.ae.isEvaluable && simultaneousProducts.ae.evalLong == 1) {
          // If the number of simultaneous products is 1, we can use a single sequential map.
          this
        } else {
          // Otherwise, we need to split the input vectors into chunks. Chunks are processed sequentially but dot
          // products within a chunk are processed in parallel.
          val splitLeft = TypeChecker.check(Split(left, simultaneousProducts))
          val splitRight = TypeChecker.check(Split(right, simultaneousProducts))
          algo.Join(
            algo.MapAsync({
                val chunkLeft = ParamDef(splitLeft.t.asInstanceOf[SeqTypeT].et)
                val chunkRight = ParamDef(splitRight.t.asInstanceOf[SeqTypeT].et)
                buildLambda(chunkLeft, buildLambda(chunkRight,
                  ParallelDotProductsLayer(ParamUse(chunkLeft), ParamUse(chunkRight), simultaneousProducts)))
              },
              splitLeft, splitRight))
        }
    }
  }

  /**
   * Sequentializes the parallel dot products layer, turning it into a map over dot products.
   * @return The sequentialized layer.
   */
  def sequential: Expr = {
    implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)
    this match {
      case ParallelDotProductsLayer(left, right, _, _) =>
        algo.MapAsync(DotProductLayer.asFunction(), left, right)
    }
  }
}

/**
 * A layer in a neural network that computes a sequence of dot products in parallel.
 */
object ParallelDotProductsLayer {
  /**
   * Creates a layer that computes a sequence of dot products in parallel.
   *
   * @param left An input vector operand.
   * @param right Another input vector operand.
   * @param simultaneousProducts The number of dot products to compute in parallel.
   * @return A vector corresponding to the sequence of dot products of `left` and `right`.
   */
  def apply(left: Expr, right: Expr, simultaneousProducts: ArithTypeT): ParallelDotProductsLayerExpr = {
    left.t match {
      case SeqType(_, count: ArithType) if count.ae.isEvaluable && simultaneousProducts.ae.isEvaluable=>
        Helpers.assert(count.ae.evalLong >= simultaneousProducts.ae.evalLong, "Number of vectors must be greater than or equal to simultaneousProducts")
        Helpers.assert(count.ae.evalLong % simultaneousProducts.ae.evalLong == 0, "Number of vectors must be a multiple of simultaneousProducts")

      case _ =>
    }

    val vectorLen = ArithTypeVar()
    val vectorCount = ArithTypeVar()
    val inputBitWidth = ArithTypeVar()
    val inputScalar = SignedIntType(inputBitWidth)
    val inputt = SeqType(SeqType(inputScalar, vectorLen), vectorCount)

    val outputt = ConditionalTypeVar(
      inputt, {
        case SeqType(SeqType(SignedIntType(width: ArithType), w: ArithType), h: ArithType) =>
          val outputBitWidth = 2 * width.ae + ceil(arithmetic.Log(2, w.ae))
          SeqType(SignedIntType(outputBitWidth), h)
      })

    val innerCraziness = FunctionCall(
      TypeFunctionCall(
        BuiltinTypeFunction(TypeFunType(
          Seq(vectorLen, vectorCount, inputBitWidth),
          FunType(Seq(inputt, inputt), outputt))),
        Seq.fill(3)(UnknownType)),
      Seq(left, right))

    ParallelDotProductsLayerExpr(innerCraziness, Seq(simultaneousProducts))
  }

  def unapply(expr: ParallelDotProductsLayerExpr): Some[(Expr, Expr, ArithTypeT, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.t
    ))
}

/**
 * A layer in a neural network that computes a padded matrix-vector product.
 */
final case class PaddedMatVecLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): PaddedMatVecLayerExpr = PaddedMatVecLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR
}

/**
 * A layer in a neural network that computes a padded matrix-vector product.
 */
object PaddedMatVecLayer {
  /**
   * Creates a layer that computes a padded matrix-vector product.
   *
   * @param matrix An input matrix (M x N) operand.
   * @param vector An input vector (N) operand.
   * @param paddingAmount The amount to pad the vector size and inner matrix dimension with.
   * @param matVecFunction A function that computes a matrix-vector product on the larger, padded inputs.
   * @return An M-dimensional vector corresponding to the result of multiplying `matrix` and `vector`.
   */
  def apply(matrix: Expr, vector: Expr, paddingAmount: ArithTypeT, matVecFunction: Expr)
           (implicit buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)): PaddedMatVecLayerExpr = {
    val vectorT = Helpers.check(vector).t
    val resultT = Helpers.check(MatVecLayer(matrix, vector)).t
    val intWidth = resultT.asInstanceOf[SeqType].et.asInstanceOf[IntTypeT].width
    val vectorToPad = ParamDef(vectorT)
    val vectorParam = ParamDef(vectorT)
    val matrixParam = ParamDef(Helpers.check(matrix).t)
    val funcParam = ParamDef(Helpers.check(matVecFunction).t)
    val innerIR = ResizeIntegerLayer(
      FunctionCall(
        ParamUse(funcParam),
        Seq(
          Map(buildLambda(vectorToPad, Pad(ParamUse(vectorToPad), 0, paddingAmount, 0)), ParamUse(matrixParam)),
          Pad(ParamUse(vectorParam), 0, paddingAmount, 0)).reverse),
      intWidth)
    PaddedMatVecLayerExpr(
      FunctionCall(
        FunctionCall(
          FunctionCall(
            buildLambda(funcParam, buildLambda(vectorParam, buildLambda(matrixParam, innerIR))),
            matVecFunction),
          vector),
        matrix),
      Seq(paddingAmount))
  }

  /**
   * Decomposes a padded matrix-vector product into its components.
   * @param expr The expression to decompose.
   * @return The components of the padded matrix-vector product: the input matrix, the input vector, and the function
   *         that computes a padded matrix-vector product.
   */
  def unapply(expr: PaddedMatVecLayerExpr): Some[(Expr, Expr, ArithTypeT, Expr, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.args(2),
      expr.t
    ))
}

/**
 * A layer in a neural network that computes a tiled matrix-vector product.
 */
final case class TiledMatVecLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): TiledMatVecLayerExpr = TiledMatVecLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR
}

/**
 * A layer in a neural network that computes a tiled matrix-vector product.
 */
object TiledMatVecLayer {
  /**
   * Creates a layer that computes a tiled matrix-vector product.
   *
   * @param matrix An input matrix (M x N) operand.
   * @param vector An input vector (N) operand.
   * @param tileSize The tile size to use. The vector size must be a multiple of the tile size.
   * @param matVecFunction A function that computes a matrix-vector product on the smaller, tiled inputs.
   * @return An M-dimensional vector corresponding to the result of multiplying `matrix` and `vector`.
   */
  def apply(matrix: Expr, vector: Expr, tileSize: ArithTypeT, matVecFunction: Expr)
           (implicit buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)): TiledMatVecLayerExpr = {
    val matrixT = Helpers.check(matrix).t
    val vectorT = Helpers.check(vector).t
    val funcT = Helpers.check(matVecFunction).t
    val vectorParam = ParamDef(vectorT)
    val matrixParam = ParamDef(matrixT)
    val funcParam = ParamDef(funcT)
    val innerIR = {
      val splitVector = Helpers.check(Split(ParamUse(vectorParam), tileSize))
      val splitMatrix = Helpers.check(TransposeND(Map(
        {
          val row = ParamDef(vectorT)
          buildLambda(row, Split(ParamUse(row), tileSize))
        },
        ParamUse(matrixParam)), Seq(0, 2, 1)))

      val tiles = Helpers.check({
        MapAsync(
          ParamUse(funcParam),
          splitMatrix,
          splitVector)
      })

      val tileResultType = tiles.t.asInstanceOf[SeqType].et
      val merged = {
        val element = ParamDef(tileResultType)
        val accumulator = ParamDef()
        Helpers.check(Fold(
          buildLambda(element, buildLambda(accumulator,
            MapAsync(
              Add2.asFunction(),
              ParamUse(accumulator),
              ParamUse(element)))),
          tiles))
      }

      merged
    }
    TiledMatVecLayerExpr(
      FunctionCall(
        FunctionCall(
          FunctionCall(
            buildLambda(funcParam, buildLambda(vectorParam, buildLambda(matrixParam, innerIR))),
            matVecFunction),
          vector),
        matrix),
      Seq(tileSize))
  }

  /**
   * Decomposes a tiled matrix-vector product into its components.
   * @param expr The expression to decompose.
   * @return The components of the tiled matrix-vector product: the input matrix, the input vector, the tile size, and
   *         the function that computes a tiled matrix-vector product.
   */
  def unapply(expr: TiledMatVecLayerExpr): Some[(Expr, Expr, ArithTypeT, Expr, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.args(2),
      expr.t
    ))
}

/**
 * A layer in a neural network that computes a matrix-vector product.
 */
final case class MatVecLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends NetworkLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): MatVecLayerExpr = MatVecLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR

  /**
   * Create a padded matrix-vector product.
   * @param paddingAmount The amount to pad the vector size and inner matrix dimension with.
   * @return A padded matrix-vector product.
   */
  def pad(paddingAmount: ArithTypeT)(implicit buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)): PaddedMatVecLayerExpr = {
    this match {
      case MatVecLayer(matrix, vector, _) =>
        val vectorT = Helpers.check(vector).t
        val paddedVectorT = SeqType(
          vectorT.asInstanceOf[SeqTypeT].et,
          paddingAmount.ae + vectorT.asInstanceOf[SeqTypeT].len.ae)
        val paddedMatrixT = SeqType(paddedVectorT, TypeChecker.check(matrix).t.asInstanceOf[SeqTypeT].len)
        val paddedVector = ParamDef(paddedVectorT)
        val paddedMatrix = ParamDef(paddedMatrixT)
        PaddedMatVecLayer(
          matrix,
          vector,
          paddingAmount,
          buildLambda(paddedMatrix, buildLambda(paddedVector, MatVecLayer(ParamUse(paddedMatrix), ParamUse(paddedVector)))))
    }
  }

  /**
   * Create a tiled matrix-vector product.
   *
   * @param tileSize The tile size to use. The vector size must be a multiple of the tile size.
   * @return A tiled matrix-vector product.
   */
  def tile(tileSize: ArithTypeT)(implicit buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)): TiledMatVecLayerExpr = {
    this match {
      case MatVecLayer(matrix, vector, _) =>
        val splitVector = Helpers.check(Split(vector, tileSize))
        val splitMatrix = Helpers.check(TransposeND(Map(
          {
            val row = ParamDef(vector.t)
            buildLambda(row, Split(ParamUse(row), tileSize))
          },
          matrix), Seq(0, 2, 1)))

        val vectorTileType = splitVector.t.asInstanceOf[SeqType].et
        val matrixTileType = splitMatrix.t.asInstanceOf[SeqType].et

        val matrixTile = ParamDef(matrixTileType)
        val vectorTile = ParamDef(vectorTileType)
        val tileFunc = buildLambda(
          matrixTile,
          buildLambda(
            vectorTile,
            MatVecLayer(ParamUse(matrixTile), ParamUse(vectorTile))))

        TiledMatVecLayer(matrix, vector, tileSize, tileFunc)
    }
  }
}

object MatVecLayer {
  /**
   * Creates a layer that computes a matrix-vector product.
   *
   * @param matrix An input matrix (M x N) operand.
   * @param vector An input vector (N) operand,
   * @return An M-dimensional vector corresponding to the result of multiplying `matrix` and `vector`.
   */
  def apply(matrix: Expr, vector: Expr): MatVecLayerExpr = {
    val matrixtc = Helpers.checkIfUnknown(matrix, matrix.t)
    val vectortc = Helpers.checkIfUnknown(vector, vector.t)
    Helpers.assert(matrixtc.t.isInstanceOf[SeqTypeT], "matrix must be a SeqType!")
    Helpers.assert(vectortc.t.isInstanceOf[SeqTypeT], "vector must be a SeqType!")

    val rows = matrixtc.t.asInstanceOf[SeqTypeT].len

    val pm = ParamDef(matrixtc.t)
    val pv = ParamDef(vectortc.t)
    val body = ParallelDotProductsLayer(ParamUse(pm), Repeat(ParamUse(pv), rows), rows)

    MatVecLayerExpr(FunctionCall(FunctionCall(AlgoLambda(pv, AlgoLambda(pm, body)), vector), matrix), Seq())
  }

  def unapply(expr: MatVecLayerExpr): Some[(Expr, Expr, Type)] =
    Some((
      expr.args.head,
      expr.args(1),
      expr.t
    ))
}

/**
 * A neural network layer that corresponds to a scalar operator, which is applied to every element of its input.
 */
trait UniformLayerExpr extends NetworkLayerExpr

object UniformLayer {
  def applyUniformly(input: Expr, scalarOp: Expr => Expr): Expr = {
    val inputtc = Helpers.checkIfUnknown(input, input.t)
    Helpers.assert(inputtc.t.isInstanceOf[SeqTypeT], "input must be a SeqType!")

    val dimLevels = inputtc.t.asInstanceOf[SeqTypeT].dimensions.length
    val p = ParamDef(inputtc.t)
    val body = Helpers.check(algo.Map(dimLevels, {
      val param = ParamDef()
      AlgoLambda(param, scalarOp(ParamUse(param)))
    }, ParamUse(p)))

    FunctionCall(AlgoLambda(p, body), input)
  }
}

final case class ReLULayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends UniformLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ReLULayerExpr = ReLULayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR
}

object ReLULayer {
  /**
   * Applies the ReLU (Rectified Linear Unit) activation function to the input expression.
   *
   * @param input The input expression to which the ReLU activation function is applied.
   * @return A new expression representing the result of applying the ReLU activation function.
   */
  def apply(input: Expr): ReLULayerExpr = {
    ReLULayerExpr(
      UniformLayer.applyUniformly(input, x => algo.Max2(x, ConstantInteger(0, Some(algo.SignedIntType(8))))),
      Seq())
  }

  def unapply(expr: ReLULayerExpr): Some[(Expr, Type)] =
    Some((
      expr.args.head,
      expr.t
    ))
}

final case class ResizeIntegerLayerExpr private[algo](innerIR: Expr, types: Seq[Type]) extends UniformLayerExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): ResizeIntegerLayerExpr =
    ResizeIntegerLayerExpr(newInnerIR, newTypes)
  override def expand(): Expr = innerIR
}

object ResizeIntegerLayer {
  def apply(input: Expr, newWidth: ArithTypeT): ResizeIntegerLayerExpr = {
    ResizeIntegerLayerExpr(
      UniformLayer.applyUniformly(input, ResizeInteger(_, newWidth)),
      Seq(newWidth))
  }

  def unapply(expr: ResizeIntegerLayerExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((
      expr.args.head,
      expr.types.head.asInstanceOf[ArithTypeT],
      expr.t
    ))
}

/**
 * The `Helpers` object provides utility methods for various operations,
 * such as expression checking and assertion, with an optional check
 * for intermediate steps based on the value of `CHECK_INTERMEDIATE_STEPS`.
 */
private object Helpers {

  /**
   * A global flag indicating whether to perform intermediate steps checks.
   * If set to `true`, checks will be performed; otherwise, they will be skipped.
   */
  private val CHECK_INTERMEDIATE_STEPS = true

  /**
   * Type checks the given expression using the TypeChecker, if the
   * `CHECK_INTERMEDIATE_STEPS` flag is set to `true`.
   *
   * @param expr The expression to be checked.
   * @return The checked expression or the original expression if checks are skipped.
   */
  def check(expr: Expr): Expr = if (CHECK_INTERMEDIATE_STEPS) TypeChecker.check(expr) else expr

  /**
   * Type checks the given expression if it is of unknown type using the TypeChecker,
   * with an optional type provided, if the `CHECK_INTERMEDIATE_STEPS` flag is set to `true`.
   *
   * @param expr The expression to be checked.
   * @param t    A type that will be compared against UnknownType.
   * @return The checked expression or the original expression if checks are skipped.
   */
  def checkIfUnknown(expr: Expr, t: Type): Expr =
    if (CHECK_INTERMEDIATE_STEPS) TypeChecker.checkIfUnknown(expr, t) else expr

  /**
   * Type checks the given expression if it is of unknown type using the TypeChecker,
   * with an optional type provided, if the `CHECK_INTERMEDIATE_STEPS` flag is set to `true`.
   *
   * @param expr The expression to be checked.
   * @return The checked expression or the original expression if checks are skipped.
   */
  def checkIfUnknown(expr: Expr): Expr = checkIfUnknown(expr, expr.t)

  /**
   * Asserts the given condition, throwing an exception with the specified message
   * if the `CHECK_INTERMEDIATE_STEPS` flag is set to `true`.
   *
   * @param condition The condition to be checked.
   * @param message   The message to be associated with the exception.
   * @throws AssertionError if checks are performed and the condition is not met.
   */
  def assert(condition: => Boolean, message: => String): Unit = {
    if (CHECK_INTERMEDIATE_STEPS) {
      scala.Predef.assert(condition, message)
    }
  }
}
