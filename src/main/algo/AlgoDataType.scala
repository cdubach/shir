package algo

import core._

import scala.annotation.tailrec
import scala.collection.immutable.SortedMap

/**
  * This type is the root of all types in the algo package / in the AlgoIR
  */

case object AlgoDataTypeKind extends Kind

sealed trait AlgoDataTypeT extends BuiltinDataTypeT with AlgoIR {
  override def kind: Kind = AlgoDataTypeKind

  override def superType: Type = BuiltinDataType()
}

final case class AlgoDataType() extends AlgoDataTypeT {
  override def build(newChildren: Seq[ShirIR]): AlgoDataTypeT = this
}

final case class AlgoDataTypeVar(tvFixedId: Option[Long] = None) extends AlgoDataTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = AlgoDataTypeVar(tvFixedId)

  override def upperBound: AlgoDataTypeT = AlgoDataType()
}

/**
  * abstract super type for all collection like types
  */

case object AbstractCollectionTypeKind extends Kind

sealed trait AbstractCollectionTypeT extends AlgoDataTypeT {
  override def kind: Kind = AbstractCollectionTypeKind

  override def superType: Type = AlgoDataType()

  override def children: Seq[Type] = Seq()
}

final case class AbstractCollectionType() extends AbstractCollectionTypeT {
  override def build(newChildren: Seq[ShirIR]): AbstractCollectionTypeT = AbstractCollectionType()
}

final case class AbstractCollectionTypeVar(tvFixedId: Option[Long] = None) extends AbstractCollectionTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = AbstractCollectionTypeVar(tvFixedId)

  override def upperBound: AlgoDataTypeT = AbstractCollectionType()
}

/**
  * This type represents sequences with unknown length. (But upper bound is known!)
  *
  */

case object UpperBoundedSeqTypeKind extends Kind

sealed trait UpperBoundedSeqTypeT extends AbstractCollectionTypeT {
  val et: AlgoDataTypeT
  val len: ArithTypeT

  override def kind: Kind = UpperBoundedSeqTypeKind

  override def superType: Type = AbstractCollectionType()

  override def children: Seq[Type] = Seq(et, len)
}

final case class UpperBoundedSeqType(et: AlgoDataTypeT, len: ArithTypeT) extends UpperBoundedSeqTypeT {
  override def build(newChildren: Seq[ShirIR]): UpperBoundedSeqTypeT = UpperBoundedSeqType(newChildren.head.asInstanceOf[AlgoDataTypeT], newChildren(1).asInstanceOf[ArithTypeT])
}
object UpperBoundedSeqType {
  /**
    * @param dimensions sequence of arithmetic values with the innermost dimension first and the outermost dimension last
    */
  def apply(dimensions: Seq[ArithTypeT], leafType: AlgoDataTypeT): UpperBoundedSeqType = dimensions.size match {
    case 0 => throw MalformedExprException("dimensions must not be empty!")
    case 1 => UpperBoundedSeqType(leafType, dimensions.last)
    case _ => UpperBoundedSeqType(apply(dimensions.init, leafType), dimensions.last)
  }
}

final case class UpperBoundedSeqTypeVar(et: AlgoDataTypeT = AlgoDataTypeVar(), len: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends UpperBoundedSeqTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = UpperBoundedSeqTypeVar(newChildren.head.asInstanceOf[AlgoDataTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = UpperBoundedSeqType(et, len)
}
object UpperBoundedSeqTypeVar {
  def apply(dimensions: Seq[ArithTypeT], leafType: AlgoDataTypeT): UpperBoundedSeqTypeVar = dimensions.size match {
    case 0 => throw MalformedExprException("dimensions must not be empty!")
    case 1 => UpperBoundedSeqTypeVar(leafType, dimensions.last)
    case _ => UpperBoundedSeqTypeVar(apply(dimensions.init, leafType), dimensions.last)
  }
}

/**
  * This type represents sequences.
  *
  */

case object SeqTypeKind extends Kind

sealed trait SeqTypeT extends UpperBoundedSeqTypeT {
  val et: AlgoDataTypeT
  val len: ArithTypeT

  override def kind: Kind = SeqTypeKind

  override def superType: Type = UpperBoundedSeqType(et, len)

  override def children: Seq[Type] = Seq(et, len)

  /**
    * @return sequence of arithmetic values with the innermost dimension first and the outermost dimension last
    */
  final def dimensions: Seq[ArithTypeT] = et match {
    case st: SeqTypeT => st.dimensions :+ len
    case _ => Seq(len)
  }

  @tailrec
  final def leafType: AlgoDataTypeT = et match {
    case st: SeqTypeT => st.leafType
    case t: AbstractScalarTypeT => t
    case _ => throw new RuntimeException("could not extract leaf type from sequence")
  }
}

final case class SeqType(et: AlgoDataTypeT, len: ArithTypeT) extends SeqTypeT {
  override def build(newChildren: Seq[ShirIR]): SeqTypeT = SeqType(newChildren.head.asInstanceOf[AlgoDataTypeT], newChildren(1).asInstanceOf[ArithTypeT])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter => Some(s"[${formatter.formatAtomic(et)} x ${formatter.formatAtomic(len)}]")
      case _ => None
    }
  }
}
object SeqType {
  /**
    * @param dimensions sequence of arithmetic values with the innermost dimension first and the outermost dimension last
    */
  def apply(dimensions: Seq[ArithTypeT], leafType: AlgoDataTypeT): SeqType = dimensions.size match {
    case 0 => throw MalformedExprException("dimensions must not be empty!")
    case 1 => SeqType(leafType, dimensions.last)
    case _ => SeqType(apply(dimensions.init, leafType), dimensions.last)
  }
}

final case class SeqTypeVar(et: AlgoDataTypeT = AlgoDataTypeVar(), len: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends SeqTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = SeqTypeVar(newChildren.head.asInstanceOf[AlgoDataTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = SeqType(et, len)
}
object SeqTypeVar {
  def apply(dimensions: Seq[ArithTypeT], leafType: AlgoDataTypeT): SeqTypeVar = dimensions.size match {
    case 0 => throw MalformedExprException("dimensions must not be empty!")
    case 1 => SeqTypeVar(leafType, dimensions.last)
    case _ => SeqTypeVar(apply(dimensions.init, leafType), dimensions.last)
  }
}


case object NamedTupleTypeKind extends Kind

sealed trait NamedTupleTypeT extends AbstractCollectionTypeT with ComposedValueTypeT {
  // think in terms of OCaml records, which means:
  // *  each field has a name and type
  // *  the names ARE significant, but ordering is not
  //
  // that is why we use a *Sorted*Map here to enforce some canonical ordering.
  val namedTypes: SortedMap[String, AlgoDataTypeT]
  assert(namedTypes.nonEmpty)

  override def kind: Kind = NamedTupleTypeKind

  override def superType: Type = AbstractCollectionType()

  override def children: Seq[Type] = {
    // recall that names matter, so we need to squeeze in their names to make
    // sure type checking takes that into account.
    //
    // implementing it this way means build and buildTV needs to account for
    // this extra name value.
    namedTypes.flatMap(p => Seq(TextType(p._1), p._2)).toSeq
  }
}

final case class NamedTupleType(namedTypes: SortedMap[String, AlgoDataTypeT]) extends NamedTupleTypeT {
  override def build(newChildren: Seq[ShirIR]): NamedTupleTypeT = {
    val m = newChildren
      .grouped(2)
      .map(p => (p.head.asInstanceOf[TextType].s, p.last.asInstanceOf[AlgoDataTypeT]))
      .foldLeft(SortedMap[String, AlgoDataTypeT]())(_ + _)
    assert(m.keySet == namedTypes.keySet, "NamedTupleType field names have changed")
    NamedTupleType(m)
  }

  private def hasDefaultNames: Boolean = namedTypes.keys.zipWithIndex.forall(
    pair => pair._1 == TupleType.TUPLE_PREFIX + pair._2)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        val inner = if (hasDefaultNames) {
          namedTypes.values.map(formatter.formatAtomic).mkString(", ")
        } else {
          namedTypes.map(pair => s"${pair._1}: ${formatter.formatAtomic(pair._2)}").mkString(", ")
        }
        Some(s"{$inner}")

      case _: CodeFormatter if hasDefaultNames =>
        val fields = namedTypes.values.map(formatter.format)
        Some(fields.mkString("TupleType(", ", ", ")"))

      case _: CodeFormatter =>
        val fields = namedTypes.map(p => s"${formatter.format(TextType(p._1))} -> ${formatter.format(p._2)}")
        Some(fields.mkString("NamedTupleType(SortedMap(", ", ", "))"))

      case _ => None
    }
  }
}

final case class NamedTupleTypeVar(namedTypes: SortedMap[String, AlgoDataTypeT], tvFixedId: Option[Long] = None) extends NamedTupleTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = {
    val m = newChildren
      .grouped(2)
      .map(p => (p.head.asInstanceOf[TextType].s, p.last.asInstanceOf[AlgoDataTypeT]))
      .foldLeft(SortedMap[String, AlgoDataTypeT]())(_ + _)
    assert(m.keySet == namedTypes.keySet, "NamedTupleTypeVar field names have changed")
    NamedTupleTypeVar(m, tvFixedId)
  }

  override def upperBound: AlgoDataTypeT = NamedTupleType(namedTypes)
}

object TupleType {
  final val TUPLE_PREFIX: String = "t"
  def apply(types: AlgoDataTypeT*): NamedTupleType =
    NamedTupleType(types
      .zipWithIndex
      .map(p => (TupleType.TUPLE_PREFIX + p._2, p._1))
      .foldLeft(SortedMap[String, AlgoDataTypeT]())(_ + _))

  def unapply(namedTupleTypeT: NamedTupleTypeT): Option[Seq[AlgoDataTypeT]] = {
    val isTuple = namedTupleTypeT.namedTypes
      .keys
      .zipWithIndex
      .forall(p => p._1 == TupleType.TUPLE_PREFIX + p._2)
    if (isTuple) Some(namedTupleTypeT.namedTypes.values.toSeq) else None
  }
}
object TupleTypeVar {
  def apply(types: AlgoDataTypeT*): NamedTupleTypeVar =
    NamedTupleTypeVar(types
      .zipWithIndex
      .map(p => (TupleType.TUPLE_PREFIX + p._2, p._1))
      .foldLeft(SortedMap[String, AlgoDataTypeT]())(_ + _))
}

case object AbstractScalarTypeKind extends Kind

sealed trait AbstractScalarTypeT extends AlgoDataTypeT {
  override def kind: Kind = AbstractScalarTypeKind

  override def superType: Type = AlgoDataType()

  override def children: Seq[Type] = Seq()
}

final case class AbstractScalarType() extends AbstractScalarTypeT {
  override def build(newChildren: Seq[ShirIR]): AbstractScalarTypeT = AbstractScalarType()
}

final case class AbstractScalarTypeVar(tvFixedId: Option[Long] = None) extends AbstractScalarTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = AbstractScalarTypeVar(tvFixedId)

  override def upperBound: AlgoDataTypeT = AbstractScalarType()
}

case object IntTypeKind extends Kind

sealed trait IntTypeT extends AbstractScalarTypeT {
  val width: ArithTypeT

  override def kind: Kind = IntTypeKind

  override def superType: Type = AbstractScalarType()

  override def children: Seq[Type] = Seq(width)
}

final case class IntType(width: ArithTypeT) extends IntTypeT {
  override def build(newChildren: Seq[ShirIR]): IntTypeT = IntType(newChildren.head.asInstanceOf[ArithTypeT])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter => Some(s"i${formatter.formatAtomic(width)}")
      case _ => None
    }
  }
}

final case class IntTypeVar(width: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends IntTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = IntTypeVar(newChildren.head.asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = IntType(width)
}

case object SignedIntTypeKind extends Kind

sealed trait SignedIntTypeT extends IntTypeT {

  override def kind: Kind = SignedIntTypeKind

  override def superType: Type = IntType(width)

  override def children: Seq[Type] = Seq(width)
}

final case class SignedIntType(width: ArithTypeT) extends SignedIntTypeT {
  override def build(newChildren: Seq[ShirIR]): SignedIntTypeT = SignedIntType(newChildren.head.asInstanceOf[ArithTypeT])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter => Some(s"si${formatter.formatAtomic(width)}")
      case _ => None
    }
  }
}

final case class SignedIntTypeVar(width: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends SignedIntTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = SignedIntTypeVar(newChildren.head.asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = SignedIntType(width)
}

/**
  * abstract super type for all float types
  */

case object FloatTypeKind extends Kind

sealed trait FloatTypeT extends AbstractScalarTypeT {
  override def kind: Kind = FloatTypeKind

  override def superType: Type = AbstractScalarType()
}

final case class FloatType() extends FloatTypeT {
  override def build(newChildren: Seq[ShirIR]): FloatTypeT = FloatType()
}

final case class FloatTypeVar(tvFixedId: Option[Long] = None) extends FloatTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT = FloatTypeVar(tvFixedId)

  override def upperBound: Type = FloatType()
}

case object FloatHalfTypeKind extends Kind

sealed trait FloatHalfTypeT extends FloatTypeT {
  override def kind: Kind = FloatHalfTypeKind

  override def superType: Type = FloatType()
}

final case class FloatHalfType() extends FloatHalfTypeT {
  override def build(newChildren: Seq[ShirIR]): FloatHalfTypeT = FloatHalfType()
}

final case class FloatHalfTypeVar(tvFixedId: Option[Long] = None) extends FloatHalfTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = FloatHalfTypeVar(tvFixedId)

  override def upperBound: Type = FloatHalfType()
}

case object FloatSingleTypeKind extends Kind

sealed trait FloatSingleTypeT extends FloatTypeT {
  override def kind: Kind = FloatDoubleTypeKind

  override def superType: Type = FloatType()
}

final case class FloatSingleType() extends FloatSingleTypeT {
  override def build(newChildren: Seq[ShirIR]): FloatSingleTypeT = FloatSingleType()
}

final case class FloatSingleTypeVar(tvFixedId: Option[Long] = None) extends FloatSingleTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = FloatSingleTypeVar(tvFixedId)

  override def upperBound: Type = FloatSingleType()
}

case object FloatDoubleTypeKind extends Kind

sealed trait FloatDoubleTypeT extends FloatTypeT {
  override def kind: Kind = FloatDoubleTypeKind

  override def superType: Type = FloatType()
}

final case class FloatDoubleType() extends FloatDoubleTypeT {
  override def build(newChildren: Seq[ShirIR]): FloatDoubleTypeT = FloatDoubleType()

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case _: TextFormatter => Some("double")
      case _ => None
    }
  }
}

final case class FloatDoubleTypeVar(tvFixedId: Option[Long] = None) extends FloatDoubleTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = FloatDoubleTypeVar(tvFixedId)

  override def upperBound: Type = FloatDoubleType()
}
