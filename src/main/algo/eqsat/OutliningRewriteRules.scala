package algo.eqsat

import algo.nn.NetworkLayerExpr
import algo.{AlgoDataTypeVar, AlgoLambda, InputExpr, SeqType, Tuple, Zip, Zip2}
import core.{AnyTypeVar, ArithTypeVar, Expr, ExprVar, FunTypeVar, FunctionCall, LambdaT, ParamDef, TypeChecker}
import eqsat.{ConditionalSearcher, DeBruijnIndexAnalysis, DeBruijnLambda, DeBruijnParamUse, DeBruijnShift, DeBruijnTransform, ENodeT, ExtractionBasedInliner, Extractor, GeneratedApplier, HoistArgument, HoistLet, MultiApplier, NopApplier, Pattern, Rewrite}
import lift.arithmetic.StartFromRange

/**
 * A collection of rewrite rules that are tailored for the outlining use case.
 * @param extractor An extractor to use for shift-expansion.
 */
final case class OutliningRewriteRules(extractor: Extractor) {
  private implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

  /**
   * A rewrite rule that expands De Bruijn shifts through extraction.
   *
   * shift(x) → shifted x
   */
  val expandShift: Rewrite = {
    val x = ExprVar()
    Rewrite("ExpandShift", Pattern(DeBruijnShift(x)), GeneratedApplier((matchInfo, _) => {
      extractor(matchInfo.where) match {
        case Some(DeBruijnShift(expr, _)) => Pattern(DeBruijnParamUse.incrementUnbound(expr))
        case _ => NopApplier
      }
    }))
  }

  /**
   * The inlining rule, which takes calls to lambdas and performs beta reduction.
   *
   * @return A rule that encodes beta reduction.
   */
  val betaReduce: Rewrite = {
    // (\x. e) y -> e[y/x]
    val t = AnyTypeVar()
    val body = ExprVar(AnyTypeVar())
    val arg = ExprVar(t)
    Rewrite(
      "InlineLambda",
      Pattern(FunctionCall(DeBruijnLambda(t, body), arg)),
      ExtractionBasedInliner(inlineMaximally = true, extractor))
  }

  /**
   * A rewrite rule that hoists inputs out of slides.
   *
   * slide window_width step_size xs → (\ slide window_width step_size p_0) xs
   * if xs != p_0
   */
  val hoistSlideGeneralArg: Rewrite = {
    val t1 = AlgoDataTypeVar()
    val xs = ExprVar(SeqType(t1, ArithTypeVar()))
    val windowWidth = ArithTypeVar()
    val stepSize = ArithTypeVar()
    Rewrite(
      "HoistSlideGeneralArg",
      ConditionalSearcher(
        Pattern(TypeChecker.check(algo.SlideGeneral(xs, windowWidth, stepSize))),
        !_.instantiate(xs).nodes.exists(_.expr match {
          case p: DeBruijnParamUse => p.index == 0
          case _ => false
        })),
      Pattern(
        FunctionCall(DeBruijnLambda(xs.t, algo.SlideGeneral(DeBruijnParamUse(0, xs.t), windowWidth, stepSize)), xs)))
  }

  /**
   * A rewrite rule that hoists inputs out of maps.
   *
   * map f xs → (\ map (shift f) p_0) xs
   * if xs != p_0
   */
  val hoistMapArg: Rewrite = {
    val t1 = AlgoDataTypeVar()
    val t2 = AlgoDataTypeVar()
    val f = ExprVar(FunTypeVar(t1, t2))
    val xs = ExprVar(SeqType(t1, ArithTypeVar()))
    Rewrite(
      "HoistMapArg",
      ConditionalSearcher(
        Pattern(TypeChecker.check(algo.Map(f, xs))),
        !_.instantiate(xs).nodes.exists(_.expr match {
          case p: DeBruijnParamUse => p.index == 0
          case _ => false
        })),
      Pattern(
        FunctionCall(DeBruijnLambda(xs.t, algo.Map(DeBruijnShift(f), DeBruijnParamUse(0, xs.t))), xs)))
  }

  /**
   * A rewrite rule that hoists constants out of map functions.
   *
   * map (\ f c) xs → (\ map (\ f' p_1) (shift xs)) c'
   * if c does not depend on p_0 and is not p_1
   * where f' is a variant of f' that renumbers p_i -> p_(i + 1) for i >= 1
   * and c' is a variant of c that decrements all De Bruijn indices
   */
  val hoistMapFunctionConst: Rewrite = {
    val tIn = AlgoDataTypeVar()
    val tC = AlgoDataTypeVar()
//    val tOut = AlgoDataTypeVar()
    val f = ExprVar(FunTypeVar())
    val c = ExprVar(tC)
    val xs = ExprVar(SeqType(tIn, ArithTypeVar()))
    Rewrite(
      "HoistMapFuncConst",
      ConditionalSearcher(
        Pattern(TypeChecker.check(algo.Map(DeBruijnLambda(tIn, FunctionCall(f, c)), xs))),
        matchInfo => {
          val cClass = matchInfo.instantiate(c)
          !cClass.analysisResult(DeBruijnIndexAnalysis).exists(_.index == 0) &&
            !cClass.nodes.exists(_.expr match {
              case p: DeBruijnParamUse => p.index == 1
              case _ => false
            })
        }),
      GeneratedApplier((matchInfo, _) => {
        (extractor(matchInfo.instantiate(f)), extractor(matchInfo.instantiate(c))) match {
          case (Some(f), Some(c)) =>
            val fPrime = DeBruijnParamUse.tryRenumberUnbound(f, {
              case 0 => 0
              case i => i + 1
            }).get

            val cPrime = DeBruijnParamUse.tryDecrementUnbound(c).get

            Pattern(
              FunctionCall(
                DeBruijnLambda(
                  tC,
                  algo.Map(DeBruijnLambda(tIn, FunctionCall(fPrime, DeBruijnParamUse(1, tC))), DeBruijnShift(xs))),
                cPrime))

          case _ => NopApplier
        }
      })
    )
  }

  /**
   * A rewrite rule that hoists the first input out of zips.
   *
   * zip xs ys → (\ zip p_0 (shift ys)) xs
   * if xs != p_0
   */
  val hoistZipArg1: Rewrite = {
    val t1 = AlgoDataTypeVar()
    val length = ArithTypeVar()
    val xs = ExprVar(SeqType(t1, length))
    val ys = ExprVar(SeqType(t1, length))
    Rewrite(
      "HoistZipArg1",
      ConditionalSearcher(
        Pattern(TypeChecker.check(Zip2(Tuple(xs, ys)))),
        !_.instantiate(xs).nodes.exists(_.expr match {
          case p: DeBruijnParamUse => p.index == 0
          case _ => false
        })),
      Pattern(
        FunctionCall(DeBruijnLambda(xs.t, Zip2(Tuple(DeBruijnParamUse(0, xs.t), DeBruijnShift(ys)))), xs)))
  }

  /**
   * A rewrite rule that hoists inputs out of folds.
   *
   * fold f xs → (\ fold (shift f) p_0) xs
   * if xs != p_0
   */
  val hoistFoldArg2: Rewrite = {
    val t1 = algo.IntTypeVar()
    val t2 = algo.IntTypeVar()
    val t3 = algo.IntTypeVar()
    val f = ExprVar(FunTypeVar(t1, FunTypeVar(t2, t3)))
    val xs = ExprVar(SeqType(t1, ArithTypeVar(StartFromRange(1))))
    Rewrite(
      "HoistFoldArg2",
      ConditionalSearcher(
        Pattern(TypeChecker.check(algo.Fold(f, xs))),
        !_.instantiate(xs).nodes.exists(_.expr match {
          case p: DeBruijnParamUse => p.index == 0
          case _ => false
        })),
      Pattern(
        FunctionCall(DeBruijnLambda(xs.t, algo.Fold(DeBruijnShift(f), DeBruijnParamUse(0, xs.t))), xs)))
  }

  /**
   * A rewrite rule that turns input arguments into let bindings.
   *
   * F(..., input, ...) → (\ F(shift(...), p_0, shift(...))) input
   * if F is not a lambda abstraction or function call
   */
  val hoistInput: Rewrite = {
    val input = ExprVar()
    val hoister = HoistArgument(
      ConditionalSearcher(
        Pattern(input),
        _.instantiate(input).nodes.exists(_.expr.isInstanceOf[InputExpr])))

    Rewrite(
      "HoistInput",
      hoister,
      hoister)
  }

  /**
   * A rewrite rule that hoists let bindings.
   *
   * F(..., (\ x) y, ...) → (\ F(shift(...), x, shift(...))) y
   * if F is not a lambda abstraction
   */
  val hoistArg: Rewrite = {
    val hoister = HoistLet()
    Rewrite("HoistArg", hoister, hoister)
  }

  /**
   * A rewrite rule that exchanges let bindings.
   *
   * (\ (\ body) arg2) arg1 = (\ (\ body') shift(arg1)) arg2'
   * if arg2 does not depend on p_0
   * where body' is a version of body that has p_0 and p_1 renumbered
   */
  val exchangeLet: Rewrite = {
    val t1 = AnyTypeVar()
    val t2 = AnyTypeVar()
    val arg1 = ExprVar(t1)
    val arg2 = ExprVar(t2)
    val body = ExprVar()
    Rewrite(
      "ExchangeLet",
      ConditionalSearcher(
        Pattern(TypeChecker.check(FunctionCall(DeBruijnLambda(t1, FunctionCall(DeBruijnLambda(t2, body), arg2)), arg1))),
        !_.instantiate(arg2).analysisResult(DeBruijnIndexAnalysis).exists(_.index == 0)),
      GeneratedApplier((matchInfo, _) => {
        (extractor(matchInfo.instantiate(body)), extractor(matchInfo.instantiate(body))) match {
          case (Some(body), Some(arg2)) =>
            val newBody = DeBruijnParamUse.tryRenumberUnbound(DeBruijnShift.elideRec(body).asInstanceOf[Expr], {
              case 0 => 1
              case 1 => 0
              case i => i
            })
            val newArg2 = DeBruijnParamUse.tryDecrementUnbound(DeBruijnShift.elideRec(arg2).asInstanceOf[Expr])

            (newBody, newArg2) match {
              case (Some(newBody), Some(newArg2)) =>
                Pattern(FunctionCall(DeBruijnLambda(t2, FunctionCall(DeBruijnLambda(t1, newBody), DeBruijnShift(arg1))), newArg2))

              case _ => NopApplier
            }
          case _ => NopApplier
        }
      }))
  }

  /**
   * A rewrite rule that lowers neural network layers.
   */
  val lowerNetworkLayer: Rewrite = {
    val input = ExprVar()
    def isLayer(node: ENodeT): Boolean = node.expr.isInstanceOf[NetworkLayerExpr]
    val searcher = ConditionalSearcher(
        Pattern(input),
        _.instantiate(input).nodes.exists(isLayer))

    Rewrite(
      "LowerNetworkLayer",
      searcher,
      GeneratedApplier((m, _) => {
        MultiApplier(
          m.instantiate(input).nodes
            .collect({ case n if isLayer(n) => n.expr.asInstanceOf[NetworkLayerExpr] })
            .map(layer => Pattern(TypeChecker.check(DeBruijnTransform.forward(layer.expand())))))
      }))
  }

  /**
   * A sequence of all rewrite rules for the outlining use case.
   */
  val all: Seq[Rewrite] = Seq(betaReduce, expandShift, hoistSlideGeneralArg, hoistMapArg, hoistMapFunctionConst, hoistZipArg1, hoistFoldArg2, hoistInput, hoistArg) //, exchangeLet)
}
