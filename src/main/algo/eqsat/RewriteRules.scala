package algo.eqsat

import algo.nn.NetworkLayerExpr
import eqsat.Rewrite

trait RewriteRules {
  /**
   * A sequence of all rewrite rules that are relevant for wrapping neural network layers. That is, the rules that
   * package a neural network layer into a lambda abstraction. This is useful for making the layer an outlining
   * candidate.
   */
  val wrapping: Seq[Rewrite]

  /**
   * A sequence of all rewrite rules that are relevant for tiling neural network layers.
   */
  val tiling: Seq[Rewrite]

  /**
   * A sequence of all rewrite rules that are relevant for padding neural network layers.
   */
  val padding: Seq[Rewrite]

  /**
   * A sequence of all rewrite rules that are relevant for sequentializing dot products.
   */
  val sequentialization: Seq[Rewrite]

  /**
   * A sequence of all rewrite rules that are relevant for transforming neural network layers.
   */
  def transformations: Seq[Rewrite] = wrapping ++ tiling ++ padding ++ sequentialization

  /**
   * Returns a sequence of all rewrite rules that are relevant for lowering neural network layers.
   * @param isLowerable A function that determines whether a given network layer is lowerable.
   * @return A sequence of rewrite rules that are relevant for lowering neural network layers.
   */
  def lowering(implicit isLowerable: NetworkLayerExpr => Boolean): Seq[Rewrite]
}
