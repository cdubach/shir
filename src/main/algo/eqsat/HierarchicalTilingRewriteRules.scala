package algo.eqsat

import algo.nn.{DotProductLayer, DotProductMatVecLayer, MatVecConvolutionLayer, NetworkLayerExpr, PaddedConvHWLayer, PaddedMatVecLayer, ParallelDotProductsLayer, TiledConvHWLayer, TiledConvICHLayer, TiledConvOCHLayer, TiledDotProductLayer, TiledMatVecLayer}
import algo.{AlgoLambda, IntTypeVar, SeqType}
import core._
import eqsat._

object HierarchicalTilingRewriteRules extends RewriteRules {
  private implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

  /**
   * Packages a matrix-vector multiplication layer into a lambda abstraction that takes the matrix and vector as
   * arguments. This is useful for making the layer an outlining candidate.
   */
  val wrapMatVec: Rewrite = {
    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val matrix = ExprVar(SeqType(SeqType(t, n), m))
    val vector = ExprVar(SeqType(t, n))
    val func = ExprVar()
    Rewrite(
      "WrapMatVecSkeleton",
      Pattern(DotProductMatVecLayer(matrix, vector, func)),
      GeneratedApplier((found, _) => {
        val matrixT = found.instantiate(matrix).t
        val vectorT = found.instantiate(vector).t
        val matVec = DotProductMatVecLayer(
          DeBruijnParamUse(1, matrixT),
          DeBruijnParamUse(0, vectorT),
          found.instantiate(func))
        val matVecFunc = DeBruijnLambda(matrixT, DeBruijnLambda(vectorT, matVec))

        Pattern(
          DeBruijnTransform.forward(
            FunctionCall(
              matVecFunc,
              Seq(found.instantiate(matrix), found.instantiate(vector)).reverse)))
      }))
  }

  /**
   * A rule that pads matrix-vector multiplication layers. The rule rewrites matrix-vector multiplication layers such
   * that their size matches the size of another matrix multiplication in the graph. This is only possible if the matvec
   * layer's vector size smaller than the dot product layer's vector size.
   */
  val padMatVec: Rewrite = {
    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val matrix1 = ExprVar(SeqType(SeqType(t, n), m))
    val vector1 = ExprVar(SeqType(t, n))
    val func1 = ExprVar()

    val k = ArithTypeVar()
    val matrix2 = ExprVar(SeqType(SeqType(t, k), m))
    val vector2 = ExprVar(SeqType(t, k))
    val func2 = ExprVar()

    Rewrite(
      "PadMatVecSkeleton",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(DotProductMatVecLayer(matrix1, vector1, func1)),
          Pattern(DotProductMatVecLayer(matrix2, vector2, func2))),
        candidate => {
          val smallVectorSize = candidate.instantiate(n).asInstanceOf[ArithTypeT].ae.evalLong
          val largeVectorSize = candidate.instantiate(k).asInstanceOf[ArithTypeT].ae.evalLong
          smallVectorSize < largeVectorSize
        }),
      GeneratedApplier((found, _) => {
        val oldVectorSize = found.instantiate(n).asInstanceOf[ArithTypeT]
        val newVectorSize = found.instantiate(k).asInstanceOf[ArithTypeT]
        val paddingAmount = newVectorSize.ae - oldVectorSize.ae
        val matrixTileT = found.instantiate(matrix2).t
        val vectorTileT = found.instantiate(vector2).t
        Pattern(
          DeBruijnTransform.forward(
            PaddedMatVecLayer(
              found.instantiate(matrix1),
              found.instantiate(vector1),
              paddingAmount,
              DeBruijnLambda(matrixTileT,
                DeBruijnLambda(vectorTileT,
                  DotProductMatVecLayer(
                    DeBruijnParamUse(1, matrixTileT),
                    DeBruijnParamUse(0, vectorTileT),
                    found.instantiate(func2)))))))
      }))
  }

  /**
   * A rule that tiles matrix-vector multiplication layers. The rule rewrites matrix-vector multiplication layers such
   * that their tile size matches the size of dot product layers in the graph. This is only possible if the matvec
   * layer's vector size is a multiple of the dot product layer's vector size.
   */
  val tileMatVec: Rewrite = {
    //  FC layer
    //
    //  MVMUL(mat: MxN, vec: N): M
    //
    //  Map(x -> Reudce(+, x),
    //    MapTup(x, y, => MVMUL(x, y), Map(x-> Split(x, N), mat), Split(vec, X))
    //  )

    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val matrix1 = ExprVar(SeqType(SeqType(t, n), m))
    val vector1 = ExprVar(SeqType(t, n))
    val func1 = ExprVar()

    val tileSize = ArithTypeVar()
    val matrix2 = ExprVar(SeqType(SeqType(t, tileSize), m))
    val vector2 = ExprVar(SeqType(t, tileSize))
    val func2 = ExprVar()

    Rewrite(
      "TileMatVecSkeleton",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(DotProductMatVecLayer(matrix1, vector1, func1)),
          Pattern(DotProductMatVecLayer(matrix2, vector2, func2))),
        candidate => {
          val vectorSizeValue = candidate.instantiate(n).asInstanceOf[ArithTypeT].ae.evalLong
          val tileSizeValue = candidate.instantiate(tileSize).asInstanceOf[ArithTypeT].ae.evalLong
          vectorSizeValue > tileSizeValue && vectorSizeValue % tileSizeValue == 0
        }),
      GeneratedApplier((found, _) => {
        val size = found.instantiate(tileSize).asInstanceOf[ArithTypeT]
        val matrixTileT = found.instantiate(matrix2).t
        val vectorTileT = found.instantiate(vector2).t
        Pattern(
          DeBruijnTransform.forward(
            TiledMatVecLayer(
              found.instantiate(matrix1),
              found.instantiate(vector1),
              size,
              DeBruijnLambda(matrixTileT,
                DeBruijnLambda(vectorTileT,
                  DotProductMatVecLayer(
                    DeBruijnParamUse(1, matrixTileT),
                    DeBruijnParamUse(0, vectorTileT),
                    found.instantiate(func2)))))))
      }))
  }

  /**
   * Packages a convolution layer into a lambda abstraction that takes the input and weight as arguments. This is useful
   * for making the layer an outlining candidate.
   */
  val wrapConv: Rewrite = {
    val t = IntTypeVar()
    val h = ArithTypeVar()
    val w = ArithTypeVar()
    val ksH = ArithTypeVar()
    val ksW = ArithTypeVar()
    val ich = ArithTypeVar()
    val och = ArithTypeVar()

    val input = ExprVar(SeqType(SeqType(SeqType(t, ich), w), h))
    val weight = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och))
    val func = ExprVar()

    Rewrite(
      "WrapConvSkeleton",
      Pattern(MatVecConvolutionLayer(input, weight, func)),
      GeneratedApplier((found, _) => {
        val inputT = found.instantiate(input).t
        val weightT = found.instantiate(weight).t
        val conv = MatVecConvolutionLayer(
          DeBruijnParamUse(1, inputT),
          DeBruijnParamUse(0, weightT),
          found.instantiate(func))
        val convFunc = DeBruijnLambda(inputT, DeBruijnLambda(weightT, conv))

        Pattern(
          DeBruijnTransform.forward(
            FunctionCall(
              convFunc,
              Seq(found.instantiate(input), found.instantiate(weight)).reverse)))
      }))
  }

  /**
   * A rule that pads convolution layers. The rule rewrites convolution layers such that their size matches the size of
   * another convolution layer in the graph. This is only possible if the conv layer's input size is smaller than the
   * other conv layer's input size.
   */
  val padConvHW: Rewrite = {
    val t = IntTypeVar()
    val h1 = ArithTypeVar()
    val w1 = ArithTypeVar()
    val ksH = ArithTypeVar()
    val ksW = ArithTypeVar()
    val ich = ArithTypeVar()
    val och = ArithTypeVar()

    val input1 = ExprVar(SeqType(SeqType(SeqType(t, ich), w1), h1))
    val weight1 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och))
    val func1 = ExprVar()

    val h2 = ArithTypeVar()
    val w2 = ArithTypeVar()

    val input2 = ExprVar(SeqType(SeqType(SeqType(t, ich), w2), h2))
    val weight2 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och))
    val func2 = ExprVar()

    Rewrite(
      "PadConvHWSkeleton",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(MatVecConvolutionLayer(input1, weight1, func1)),
          Pattern(MatVecConvolutionLayer(input2, weight2, func2))),
        candidate => {
          val h1tc = candidate.instantiate(h1).asInstanceOf[ArithTypeT].ae.evalLong
          val w1tc = candidate.instantiate(w1).asInstanceOf[ArithTypeT].ae.evalLong
          val h2tc = candidate.instantiate(h2).asInstanceOf[ArithTypeT].ae.evalLong
          val w2tc = candidate.instantiate(w2).asInstanceOf[ArithTypeT].ae.evalLong
          val padTooMany = (h2tc - h1tc) > (32L max (h1tc / 5)) && (w2tc - w1tc) > (32L max (w1tc / 5))
          h1tc < h2tc && w1tc < w2tc && ! padTooMany
        }),
      GeneratedApplier((found, _) => {
        val inputPaddedT = found.instantiate(input2).t
        val weightPaddedT = found.instantiate(weight2).t
        Pattern(
          DeBruijnTransform.forward(
            PaddedConvHWLayer(
              found.instantiate(input1),
              found.instantiate(weight1),
              DeBruijnLambda(inputPaddedT,
                DeBruijnLambda(weightPaddedT,
                  MatVecConvolutionLayer(
                    DeBruijnParamUse(1, inputPaddedT),
                    DeBruijnParamUse(0, weightPaddedT),
                    found.instantiate(func2)))))))
      }))
  }

  val tileConvHW: Rewrite = {
    val t = IntTypeVar()
    val h1 = ArithTypeVar()
    val w1 = ArithTypeVar()
    val ksH = ArithTypeVar()
    val ksW = ArithTypeVar()
    val ich = ArithTypeVar()
    val och = ArithTypeVar()

    val input1 = ExprVar(SeqType(SeqType(SeqType(t, ich), w1), h1))
    val weight1 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och))
    val func1 = ExprVar()

    val h2 = ArithTypeVar()
    val w2 = ArithTypeVar()

    val input2 = ExprVar(SeqType(SeqType(SeqType(t, ich), w2), h2))
    val weight2 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och))
    val func2 = ExprVar()

    Rewrite(
      "TileConvHWSkeleton",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(MatVecConvolutionLayer(input1, weight1, func1)),
          Pattern(MatVecConvolutionLayer(input2, weight2, func2))),
        candidate => {
          val ksHtc = candidate.instantiate(ksH).asInstanceOf[ArithTypeT].ae.evalLong
          val ksWtc = candidate.instantiate(ksW).asInstanceOf[ArithTypeT].ae.evalLong
          val h1tc = candidate.instantiate(h1).asInstanceOf[ArithTypeT].ae.evalLong
          val w1tc = candidate.instantiate(w1).asInstanceOf[ArithTypeT].ae.evalLong
          val h2tc = candidate.instantiate(h2).asInstanceOf[ArithTypeT].ae.evalLong
          val w2tc = candidate.instantiate(w2).asInstanceOf[ArithTypeT].ae.evalLong
          val h1Tiles = (h1tc - ksHtc) / 1 + 1
          val w1Tiles = (w1tc - ksWtc) / 1 + 1
          val h2Tiles = (h2tc - ksHtc) / 1 + 1
          val w2Tiles = (w2tc - ksWtc) / 1 + 1
          val tileTooSmall = w2Tiles < 4 && h2Tiles < 4
          h1Tiles > h2Tiles && h1Tiles % h2Tiles == 0 && w1Tiles > w2Tiles && w1Tiles % w2Tiles == 0 && !tileTooSmall
        }),
      GeneratedApplier((found, _) => {
        val tiledInputT = found.instantiate(input2).t
        val tiledWeightT = found.instantiate(weight2).t
        Pattern(
          DeBruijnTransform.forward(
            TiledConvHWLayer(
              found.instantiate(input1),
              found.instantiate(weight1),
              DeBruijnLambda(tiledInputT,
                DeBruijnLambda(tiledWeightT,
                  MatVecConvolutionLayer(
                    DeBruijnParamUse(1, tiledInputT),
                    DeBruijnParamUse(0, tiledWeightT),
                    found.instantiate(func2)))))))
      }))
  }

  val tileConvOCH: Rewrite = {
    val t = IntTypeVar()
    val h = ArithTypeVar()
    val w = ArithTypeVar()
    val ksH = ArithTypeVar()
    val ksW = ArithTypeVar()
    val ich = ArithTypeVar()
    val och1 = ArithTypeVar()

    val input1 = ExprVar(SeqType(SeqType(SeqType(t, ich), w), h))
    val weight1 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och1))
    val func1 = ExprVar()

    val och2 = ArithTypeVar()

    val input2 = ExprVar(SeqType(SeqType(SeqType(t, ich), w), h))
    val weight2 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och2))
    val func2 = ExprVar()

    Rewrite(
      "TileConvOCHSkeleton",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(MatVecConvolutionLayer(input1, weight1, func1)),
          Pattern(MatVecConvolutionLayer(input2, weight2, func2))),
        candidate => {
          val och1tc = candidate.instantiate(och1).asInstanceOf[ArithTypeT].ae.evalLong
          val och2tc = candidate.instantiate(och2).asInstanceOf[ArithTypeT].ae.evalLong
          och1tc > och2tc && och1tc % och2tc == 0
        }),
      GeneratedApplier((found, _) => {
        val tiledInputT = found.instantiate(input2).t
        val tiledWeightT = found.instantiate(weight2).t
        Pattern(
          DeBruijnTransform.forward(
            TiledConvOCHLayer(
              found.instantiate(input1),
              found.instantiate(weight1),
              DeBruijnLambda(tiledInputT,
                DeBruijnLambda(tiledWeightT,
                  MatVecConvolutionLayer(
                    DeBruijnParamUse(1, tiledInputT),
                    DeBruijnParamUse(0, tiledWeightT),
                    found.instantiate(func2)))))))
      }))
  }

  val tileConvICH: Rewrite = {
    val t = IntTypeVar()
    val h = ArithTypeVar()
    val w = ArithTypeVar()
    val ksH = ArithTypeVar()
    val ksW = ArithTypeVar()
    val ich1 = ArithTypeVar()
    val och = ArithTypeVar()

    val input1 = ExprVar(SeqType(SeqType(SeqType(t, ich1), w), h))
    val weight1 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich1), ksW), ksH), och))
    val func1 = ExprVar()

    val ich2 = ArithTypeVar()

    val input2 = ExprVar(SeqType(SeqType(SeqType(t, ich2), w), h))
    val weight2 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich2), ksW), ksH), och))
    val func2 = ExprVar()

    Rewrite(
      "TileConvICHSkeleton",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(MatVecConvolutionLayer(input1, weight1, func1)),
          Pattern(MatVecConvolutionLayer(input2, weight2, func2))),
        candidate => {
          val ich1tc = candidate.instantiate(ich1).asInstanceOf[ArithTypeT].ae.evalLong
          val ich2tc = candidate.instantiate(ich2).asInstanceOf[ArithTypeT].ae.evalLong
          ich1tc > ich2tc && ich1tc % ich2tc == 0
        }),
      GeneratedApplier((found, _) => {
        val tiledInputT = found.instantiate(input2).t
        val tiledWeightT = found.instantiate(weight2).t
        Pattern(
          DeBruijnTransform.forward(
            TiledConvICHLayer(
              found.instantiate(input1),
              found.instantiate(weight1),
              DeBruijnLambda(tiledInputT,
                DeBruijnLambda(tiledWeightT,
                  MatVecConvolutionLayer(
                    DeBruijnParamUse(1, tiledInputT),
                    DeBruijnParamUse(0, tiledWeightT),
                    found.instantiate(func2)))))))
      }))
  }

  /**
   * A rule that wraps parallel dot product layers in lambda abstractions.
   */
  val wrapParallelDotProducts: Rewrite = {
    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val left = ExprVar(SeqType(SeqType(t, n), m))
    val right = ExprVar(SeqType(SeqType(t, n), m))
    val parallelism = ArithTypeVar()

    Rewrite(
      "WrapParallelDotProducts",
      Pattern(ParallelDotProductsLayer(left, right, parallelism)),
      GeneratedApplier((found, _) => {
        val leftT = found.instantiate(left).t
        val rightT = found.instantiate(right).t
        val parallelDotProducts = ParallelDotProductsLayer(
          DeBruijnParamUse(1, leftT),
          DeBruijnParamUse(0, rightT),
          found.instantiate(parallelism).asInstanceOf[ArithTypeT])
        val parallelDotProductsFunc = DeBruijnLambda(leftT, DeBruijnLambda(rightT, parallelDotProducts))

        Pattern(
          DeBruijnTransform.forward(
            FunctionCall(
              parallelDotProductsFunc,
              Seq(found.instantiate(left), found.instantiate(right)).reverse)))
      }))
  }

  /**
   * A rule that tiles dot products by rewriting them as smaller parallel dot products. The rule rewrites dot products
   * such that their tiled version corresponds to an existing parallel dot product layer in the graph.
   */
  val tileDotProduct: Rewrite = {
    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val leftVector = ExprVar(SeqType(t, n))
    val rightVector = ExprVar(SeqType(t, n))

    val leftMatrix = ExprVar(SeqType(SeqType(t, m), k))
    val rightMatrix = ExprVar(SeqType(SeqType(t, m), k))

    val parallelProducts = ArithTypeVar()

    Rewrite(
      "TileDotProduct",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(DotProductLayer(leftVector, rightVector)),
          Pattern(ParallelDotProductsLayer(leftMatrix, rightMatrix, parallelProducts))),
        candidate => {
          val totalSize = candidate.instantiate(n).asInstanceOf[ArithTypeT].ae.evalLong
          val chunkSize = candidate.instantiate(m).asInstanceOf[ArithTypeT].ae.evalLong
          val productCount = candidate.instantiate(parallelProducts).asInstanceOf[ArithTypeT].ae.evalLong
          val chunkCount = totalSize / chunkSize
          totalSize > chunkSize && totalSize % chunkSize == 0 && chunkCount >= productCount && chunkCount % productCount == 0
        }),
      GeneratedApplier((found, _) => {
        val tiledLeftT = found.instantiate(leftMatrix).t
        val tiledRightT = found.instantiate(rightMatrix).t
        Pattern(
          DeBruijnTransform.forward(
            TiledDotProductLayer(
              found.instantiate(leftVector),
              found.instantiate(rightVector),
              found.instantiate(m).asInstanceOf[ArithTypeT],
              DeBruijnLambda(tiledLeftT,
                DeBruijnLambda(tiledRightT,
                  ParallelDotProductsLayer(
                    DeBruijnParamUse(1, tiledLeftT),
                    DeBruijnParamUse(0, tiledRightT),
                    parallelProducts))))))
      }))
  }

  /**
   * A rule that re-parallelizes parallel dot product layers. The rule rewrites parallel dot product layers by
   * repeatedly halving the number of simultaneous dot products until the number of simultaneous dot products is no
   * longer divisible by two.
   */
  val reparallelizeDotProducts: Rewrite = {
    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val simultaneousProducts = ArithTypeVar()
    val left = ExprVar(SeqType(SeqType(t, n), m))
    val right = ExprVar(SeqType(SeqType(t, n), m))

    def computeTilingFactors(originalParallelism: Long): Seq[Long] = {
      // Repeatedly divide the original parallelism by two until we cannot divide further.
      if (originalParallelism % 2 == 0) {
        originalParallelism +: computeTilingFactors(originalParallelism / 2)
      } else {
        Seq(originalParallelism)
      }
    }

    Rewrite(
      "ReparallelizeDotProducts",
      ConditionalSearcher(
        Pattern(ParallelDotProductsLayer(left, right, simultaneousProducts)),
        candidate => {
          val simultaneousProductsValue = candidate.instantiate(simultaneousProducts).asInstanceOf[ArithTypeT].ae.evalLong
          simultaneousProductsValue > 1 && simultaneousProductsValue % 2 == 0
        }),
      GeneratedApplier((found, _) => {
        val tilingFactors = computeTilingFactors(found.instantiate(simultaneousProducts).asInstanceOf[ArithTypeT].ae.evalLong)
        MultiApplier(tilingFactors.map(factor => {
          Pattern(
            DeBruijnTransform.forward(
              ParallelDotProductsLayer(
                found.instantiate(left),
                found.instantiate(right),
                ArithType(factor))))
        }))
      }))
  }

  /**
   * A rule that sequentializes parallel dot product layers. The rule changes the parallelism of parallel dot product
   * layers to one.
   */
  val sequentializeDotProducts: Rewrite = {
    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val simultaneousProducts = ArithTypeVar()
    val left = ExprVar(SeqType(SeqType(t, n), m))
    val right = ExprVar(SeqType(SeqType(t, n), m))

    Rewrite(
      "SequentializeDotProduct",
      Pattern(ParallelDotProductsLayer(left, right, simultaneousProducts)),
      GeneratedApplier((found, _) => {
        Pattern(
          DeBruijnTransform.forward(
            ParallelDotProductsLayer(
              found.instantiate(left),
              found.instantiate(right),
              1)))
      }))
  }

  /**
   * A sequence of all rewrite rules that are relevant for wrapping neural network layers. That is, the rules that
   * package a neural network layer into a lambda abstraction. This is useful for making the layer an outlining
   * candidate.
   */
  val wrapping: Seq[Rewrite] = Seq(wrapMatVec, wrapConv, wrapParallelDotProducts)

  /**
   * A sequence of all rewrite rules that are relevant for padding neural network layers.
   */
  val padding: Seq[Rewrite] = Seq(padMatVec, padConvHW)

  /**
   * A sequence of all rewrite rules that are relevant for tiling neural network layers.
   */
  val tiling: Seq[Rewrite] = Seq(tileMatVec, tileConvHW, tileConvOCH, tileConvICH, tileDotProduct)

  /**
   * A sequence of all rewrite rules that are relevant for sequentializing dot products.
   */
  val sequentialization: Seq[Rewrite] = Seq(sequentializeDotProducts, reparallelizeDotProducts)

  /**
   * Returns a sequence of all rewrite rules that are relevant for lowering neural network layers.
   *
   * @param isLowerable A function that determines whether a given network layer is lowerable.
   * @return A sequence of rewrite rules that are relevant for lowering neural network layers.
   */
  override def lowering(implicit isLowerable: NetworkLayerExpr => Boolean): Seq[Rewrite] = Seq()
}
