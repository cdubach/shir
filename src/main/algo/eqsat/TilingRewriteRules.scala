package algo.eqsat

import algo.nn.{ConvolutionLayer, DotProductLayer, MatVecLayer, NetworkLayerExpr, ParallelDotProductsLayer}
import algo.{AlgoLambda, IntTypeVar, SeqType}
import core._
import eqsat._

object TilingRewriteRules extends RewriteRules {
  private implicit val buildLambda: (ParamDef, Expr) => LambdaT = AlgoLambda(_, _)

  /**
   * A rewrite rule that lowers neural network layers.
   */
  def lowerNetworkLayer(implicit isLowerable: NetworkLayerExpr => Boolean): Rewrite = {
    val input = ExprVar()
    def isLayer(node: ENodeT): Boolean = node.expr.isInstanceOf[NetworkLayerExpr]
    val searcher = ConditionalSearcher(
      Pattern(input),
      _.instantiate(input).nodes.exists(isLayer))

    Rewrite(
      "LowerNetworkLayer",
      AnnotateWithNodeCount(searcher),
      GeneratedApplier((m, _) => {
        MultiApplier(
          m.instantiate(input).nodes
            .collect({
              case n if isLayer(n) && isLowerable(n.expr.asInstanceOf[NetworkLayerExpr]) =>
                n.expr.asInstanceOf[NetworkLayerExpr]
            })
            .map(layer => Pattern(TypeChecker.check(DeBruijnTransform.forward(layer.expand())))))
      }))
  }

  /**
    * A rule that partially lowers parallel dot products.
    */
  val partiallyLowerParallelDotProducts: Rewrite = {
    val t = IntTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()

    val leftMatrix = ExprVar(SeqType(SeqType(t, m), k))
    val rightMatrix = ExprVar(SeqType(SeqType(t, m), k))

    val parallelProducts = ArithTypeVar()

    Rewrite(
      "PartiallyLowerParallelDotProducts",
      ConditionalSearcher(
        Pattern(ParallelDotProductsLayer(leftMatrix, rightMatrix, parallelProducts)),
        found => {
          val kValue = found.instantiate(k).asInstanceOf[ArithTypeT].ae.evalLong
          val parallelProductsValue = found.instantiate(parallelProducts).asInstanceOf[ArithTypeT].ae.evalLong
          parallelProductsValue > 1 && parallelProductsValue < kValue
        }),
      GeneratedApplier((found, _) => {
        Pattern(
          DeBruijnTransform.forward(
            ParallelDotProductsLayer(
              found.instantiate(leftMatrix),
              found.instantiate(rightMatrix),
              found.instantiate(parallelProducts).asInstanceOf[ArithTypeT]).partiallyExpanded))
      }))
  }

  /**
   * A rule that pads matrix-vector multiplication layers. The rule rewrites matrix-vector multiplication layers such
   * that their size matches the size of another matrix multiplication in the graph. This is only possible if the matvec
   * layer's vector size smaller than the dot product layer's vector size.
   */
  val padMatVec: Rewrite = {
    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val matrix1 = ExprVar(SeqType(SeqType(t, n), m))
    val vector1 = ExprVar(SeqType(t, n))

    val k = ArithTypeVar()
    val matrix2 = ExprVar(SeqType(SeqType(t, k), m))
    val vector2 = ExprVar(SeqType(t, k))

    Rewrite(
      "PadMatVec",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(MatVecLayer(matrix1, vector1)),
          Pattern(MatVecLayer(matrix2, vector2))),
        candidate => {
          val smallVectorSize = candidate.instantiate(n).asInstanceOf[ArithTypeT].ae.evalLong
          val largeVectorSize = candidate.instantiate(k).asInstanceOf[ArithTypeT].ae.evalLong
          smallVectorSize < largeVectorSize
        }),
      GeneratedApplier((found, _) => {
        val oldVectorSize = found.instantiate(n).asInstanceOf[ArithTypeT]
        val newVectorSize = found.instantiate(k).asInstanceOf[ArithTypeT]
        val paddingAmount = newVectorSize.ae - oldVectorSize.ae
        Pattern(
          DeBruijnTransform.forward(
            MatVecLayer(found.instantiate(matrix1), found.instantiate(vector1)).pad(paddingAmount)))
      }))
  }

  /**
   * A rule that tiles matrix-vector multiplication layers. The rule rewrites matrix-vector multiplication layers such
   * that their tile size matches the size of dot product layers in the graph. This is only possible if the matvec
   * layer's vector size is a multiple of the dot product layer's vector size.
   */
  val tileMatVec: Rewrite = {
    //  FC layer
    //
    //  MVMUL(mat: MxN, vec: N): M
    //
    //  Map(x -> Reudce(+, x),
    //    MapTup(x, y, => MVMUL(x, y), Map(x-> Split(x, N), mat), Split(vec, X))
    //  )

    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val matrix1 = ExprVar(SeqType(SeqType(t, n), m))
    val vector1 = ExprVar(SeqType(t, n))

    val tileSize = ArithTypeVar()
    val matrix2 = ExprVar(SeqType(SeqType(t, tileSize), m))
    val vector2 = ExprVar(SeqType(t, tileSize))

    Rewrite(
      "TileMatVec",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(MatVecLayer(matrix1, vector1)),
          Pattern(MatVecLayer(matrix2, vector2))),
        candidate => {
          val vectorSizeValue = candidate.instantiate(n).asInstanceOf[ArithTypeT].ae.evalLong
          val tileSizeValue = candidate.instantiate(tileSize).asInstanceOf[ArithTypeT].ae.evalLong
          vectorSizeValue > tileSizeValue && vectorSizeValue % tileSizeValue == 0
        }),
      GeneratedApplier((found, _) => {
        Pattern(
          DeBruijnTransform.forward(
            MatVecLayer(found.instantiate(matrix1), found.instantiate(vector1))
              .tile(found.instantiate(tileSize).asInstanceOf[ArithTypeT])))
      }))
  }

  val padConvHW: Rewrite = {
    val t = IntTypeVar()
    val h1 = ArithTypeVar()
    val w1 = ArithTypeVar()
    val ksH = ArithTypeVar()
    val ksW = ArithTypeVar()
    val ich = ArithTypeVar()
    val och = ArithTypeVar()

    val input1 = ExprVar(SeqType(SeqType(SeqType(t, ich), w1), h1))
    val weight1 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och))

    val h2 = ArithTypeVar()
    val w2 = ArithTypeVar()

    val input2 = ExprVar(SeqType(SeqType(SeqType(t, ich), w2), h2))
    val weight2 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och))

    Rewrite(
      "PadConvHW",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(ConvolutionLayer(input1, weight1)),
          Pattern(ConvolutionLayer(input2, weight2))),
        candidate => {
          val h1tc = candidate.instantiate(h1).asInstanceOf[ArithTypeT].ae.evalLong
          val w1tc = candidate.instantiate(w1).asInstanceOf[ArithTypeT].ae.evalLong
          val h2tc = candidate.instantiate(h2).asInstanceOf[ArithTypeT].ae.evalLong
          val w2tc = candidate.instantiate(w2).asInstanceOf[ArithTypeT].ae.evalLong
          val padTooMany = (h2tc - h1tc) > (32L max (h1tc / 5)) && (w2tc - w1tc) > (32L max (w1tc / 5))
          h1tc < h2tc && w1tc < w2tc && ! padTooMany
        }),
      GeneratedApplier((found, _) => {
        Pattern(
          DeBruijnTransform.forward(
            ConvolutionLayer(found.instantiate(input1), found.instantiate(weight1))
              .padHW(found.instantiate(w2).asInstanceOf[ArithTypeT].ae - found.instantiate(w1).asInstanceOf[ArithTypeT].ae,
                found.instantiate(h2).asInstanceOf[ArithTypeT].ae - found.instantiate(h1).asInstanceOf[ArithTypeT].ae)))
      }))
  }

  val tileConvHW: Rewrite = {
    val t = IntTypeVar()
    val h1 = ArithTypeVar()
    val w1 = ArithTypeVar()
    val ksH = ArithTypeVar()
    val ksW = ArithTypeVar()
    val ich = ArithTypeVar()
    val och = ArithTypeVar()


    val input1 = ExprVar(SeqType(SeqType(SeqType(t, ich), w1), h1))
    val weight1 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och))

    val h2 = ArithTypeVar()
    val w2 = ArithTypeVar()

    val input2 = ExprVar(SeqType(SeqType(SeqType(t, ich), w2), h2))
    val weight2 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och))

    Rewrite(
      "TileConvHW",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(ConvolutionLayer(input1, weight1)),
          Pattern(ConvolutionLayer(input2, weight2))),
        candidate => {
          val ksHtc = candidate.instantiate(ksH).asInstanceOf[ArithTypeT].ae.evalLong
          val ksWtc = candidate.instantiate(ksW).asInstanceOf[ArithTypeT].ae.evalLong
          val h1tc = candidate.instantiate(h1).asInstanceOf[ArithTypeT].ae.evalLong
          val w1tc = candidate.instantiate(w1).asInstanceOf[ArithTypeT].ae.evalLong
          val h2tc = candidate.instantiate(h2).asInstanceOf[ArithTypeT].ae.evalLong
          val w2tc = candidate.instantiate(w2).asInstanceOf[ArithTypeT].ae.evalLong
          val h1Tiles = (h1tc - ksHtc) / 1 + 1
          val w1Tiles = (w1tc - ksWtc) / 1 + 1
          val h2Tiles = (h2tc - ksHtc) / 1 + 1
          val w2Tiles = (w2tc - ksWtc) / 1 + 1
          val tileTooSmall = w2Tiles < 4 && h2Tiles < 4
          h1Tiles > h2Tiles && h1Tiles % h2Tiles == 0 && w1Tiles > w2Tiles && w1Tiles % w2Tiles == 0 && !tileTooSmall
        }),
      GeneratedApplier((found, _) => {
        Pattern(
          DeBruijnTransform.forward(
            ConvolutionLayer(found.instantiate(input1), found.instantiate(weight1))
              .tileHW(found.instantiate(w2).asInstanceOf[ArithTypeT], found.instantiate(h2).asInstanceOf[ArithTypeT])))
      }))
  }

  val tileConvOCH: Rewrite = {
    val t = IntTypeVar()
    val h = ArithTypeVar()
    val w = ArithTypeVar()
    val ksH = ArithTypeVar()
    val ksW = ArithTypeVar()
    val ich = ArithTypeVar()
    val och1 = ArithTypeVar()

    val input1 = ExprVar(SeqType(SeqType(SeqType(t, ich), w), h))
    val weight1 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och1))

    val och2 = ArithTypeVar()

    val input2 = ExprVar(SeqType(SeqType(SeqType(t, ich), w), h))
    val weight2 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich), ksW), ksH), och2))

    Rewrite(
      "TileConvOCH",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(ConvolutionLayer(input1, weight1)),
          Pattern(ConvolutionLayer(input2, weight2))),
        candidate => {
          val och1tc = candidate.instantiate(och1).asInstanceOf[ArithTypeT].ae.evalLong
          val och2tc = candidate.instantiate(och2).asInstanceOf[ArithTypeT].ae.evalLong
          och1tc > och2tc && och1tc % och2tc == 0
        }),
      GeneratedApplier((found, _) => {
        Pattern(
          DeBruijnTransform.forward(
            ConvolutionLayer(found.instantiate(input1), found.instantiate(weight1))
              .tileOCH(found.instantiate(och2).asInstanceOf[ArithTypeT])))
      }))
  }

  val tileConvICH: Rewrite = {
    val t = IntTypeVar()
    val h = ArithTypeVar()
    val w = ArithTypeVar()
    val ksH = ArithTypeVar()
    val ksW = ArithTypeVar()
    val ich1 = ArithTypeVar()
    val och = ArithTypeVar()

    val input1 = ExprVar(SeqType(SeqType(SeqType(t, ich1), w), h))
    val weight1 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich1), ksW), ksH), och))

    val ich2 = ArithTypeVar()

    val input2 = ExprVar(SeqType(SeqType(SeqType(t, ich2), w), h))
    val weight2 = ExprVar(SeqType(SeqType(SeqType(SeqType(t, ich2), ksW), ksH), och))

    Rewrite(
      "TileConvICH",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(ConvolutionLayer(input1, weight1)),
          Pattern(ConvolutionLayer(input2, weight2))),
        candidate => {
          val ich1tc = candidate.instantiate(ich1).asInstanceOf[ArithTypeT].ae.evalLong
          val ich2tc = candidate.instantiate(ich2).asInstanceOf[ArithTypeT].ae.evalLong
          ich1tc > ich2tc && ich1tc % ich2tc == 0
        }),
      GeneratedApplier((found, _) => {
        Pattern(
          DeBruijnTransform.forward(
            ConvolutionLayer(found.instantiate(input1), found.instantiate(weight1))
              .tileICH(found.instantiate(ich2).asInstanceOf[ArithTypeT])))
      }))
  }

  /**
    * A rule that tiles dot products by rewriting them as smaller parallel dot products. The rule rewrites dot products
    * such that their tiled version corresponds to an existing parallel dot product layer in the graph.
    */
  val tileDotProduct: Rewrite = {
    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val leftVector = ExprVar(SeqType(t, n))
    val rightVector = ExprVar(SeqType(t, n))

    val leftMatrix = ExprVar(SeqType(SeqType(t, m), k))
    val rightMatrix = ExprVar(SeqType(SeqType(t, m), k))

    val parallelProducts = ArithTypeVar()

    Rewrite(
      "TileDotProduct",
      ConditionalSearcher(
        AugmentedSearcher(
          Pattern(DotProductLayer(leftVector, rightVector)),
          Pattern(ParallelDotProductsLayer(leftMatrix, rightMatrix, parallelProducts))),
        candidate => {
          val totalSize = candidate.instantiate(n).asInstanceOf[ArithTypeT].ae.evalLong
          val chunkSize = candidate.instantiate(m).asInstanceOf[ArithTypeT].ae.evalLong
          val productCount = candidate.instantiate(parallelProducts).asInstanceOf[ArithTypeT].ae.evalLong
          val chunkCount = totalSize / chunkSize
          totalSize > chunkSize && totalSize % chunkSize == 0 && chunkCount >= productCount && chunkCount % productCount == 0
        }),
      GeneratedApplier((found, _) => {
        Pattern(
          DeBruijnTransform.forward(
            DotProductLayer(found.instantiate(leftVector), found.instantiate(rightVector))
              .tile(
                found.instantiate(m).asInstanceOf[ArithTypeT],
                found.instantiate(parallelProducts).asInstanceOf[ArithTypeT])))
      }))
  }

  /**
    * A rule that re-parallelizes parallel dot product layers. The rule rewrites parallel dot product layers by
    * repeatedly halving the number of simultaneous dot products until the number of simultaneous dot products is no
    * longer divisible by two.
    */
  val reparallelizeDotProducts: Rewrite = {
    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val simultaneousProducts = ArithTypeVar()
    val left = ExprVar(SeqType(SeqType(t, n), m))
    val right = ExprVar(SeqType(SeqType(t, n), m))

    def computeTilingFactors(originalParallelism: Long): Seq[Long] = {
      // Repeatedly divide the original parallelism by two until we cannot divide further.
      if (originalParallelism % 2 == 0) {
        originalParallelism +: computeTilingFactors(originalParallelism / 2)
      } else {
        Seq(originalParallelism)
      }
    }

    Rewrite(
      "TileParallelDotProduct",
      ConditionalSearcher(
        Pattern(ParallelDotProductsLayer(left, right, simultaneousProducts)),
        candidate => {
          val simultaneousProductsValue = candidate.instantiate(simultaneousProducts).asInstanceOf[ArithTypeT].ae.evalLong
          simultaneousProductsValue > 1 && simultaneousProductsValue % 2 == 0
        }),
      GeneratedApplier((found, _) => {
        val tilingFactors = computeTilingFactors(found.instantiate(simultaneousProducts).asInstanceOf[ArithTypeT].ae.evalLong)
        MultiApplier(tilingFactors.map(factor => {
          Pattern(
            DeBruijnTransform.forward(
              ParallelDotProductsLayer(
                found.instantiate(left),
                found.instantiate(right),
                ArithType(factor))))
        }))
      }))
  }

  /**
    * A rule that sequentializes parallel dot product layers. The rule rewrites parallel dot product layers by
    * transforming them into sequential dot product layers.
    */
  val sequentializeDotProducts: Rewrite = {
    val t = IntTypeVar()
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val simultaneousProducts = ArithTypeVar()
    val left = ExprVar(SeqType(SeqType(t, n), m))
    val right = ExprVar(SeqType(SeqType(t, n), m))

    Rewrite(
      "SequentializeDotProduct",
      Pattern(ParallelDotProductsLayer(left, right, simultaneousProducts)),
      GeneratedApplier((found, _) => {
        Pattern(
          DeBruijnTransform.forward(
            ParallelDotProductsLayer(
              found.instantiate(left),
              found.instantiate(right),
              found.instantiate(simultaneousProducts).asInstanceOf[ArithTypeT]).sequential))
      }))
  }

  /**
    * A sequence of all rewrite rules that are relevant for tiling neural network layers.
    */
  val tiling: Seq[Rewrite] = Seq(tileMatVec, tileConvHW, tileConvOCH, tileConvICH, tileDotProduct)

  /**
    * A sequence of all rewrite rules that are relevant for padding neural network layers.
    */
  val padding: Seq[Rewrite] = Seq(padMatVec, padConvHW)

  /**
    * A sequence of all rewrite rules that are relevant for sequentializing dot products.
    */
  val sequentialization: Seq[Rewrite] = Seq(sequentializeDotProducts, reparallelizeDotProducts)

  /**
    * Returns a sequence of all rewrite rules that are relevant for lowering neural network layers.
    * @param isLowerable A function that determines whether a given network layer is lowerable.
    * @return A sequence of rewrite rules that are relevant for lowering neural network layers.
    */
  def lowering(implicit isLowerable: NetworkLayerExpr => Boolean): Seq[Rewrite] =
    Seq(lowerNetworkLayer, partiallyLowerParallelDotProducts)

  /**
   * A sequence of all rewrite rules that are relevant for wrapping neural network layers. That is, the rules that
   * package a neural network layer into a lambda abstraction. This is useful for making the layer an outlining
   * candidate.
   */
  override val wrapping: Seq[Rewrite] = Seq()
}
