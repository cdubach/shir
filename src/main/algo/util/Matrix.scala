package algo.util

import algo.util.Matrix.{MATRICES_PATH, MATRIX_FILE_ENDING}

import java.io.{File, PrintWriter}
import scala.io.Source

case class Matrix(data: Seq[Seq[Int]], name: String) {
  def mul(thatInverted: Matrix, cache: Boolean = true): Matrix = {
    val fileName = this.name + "_mul_" + thatInverted.name
    if (cache) {
      Matrix.fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(mul(this.data, thatInverted.data), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(mul(this.data, thatInverted.data), fileName)
    }
  }

  private def mul(A: Seq[Seq[Int]], Binv: Seq[Seq[Int]]): Seq[Seq[Int]] = mul(A.map(_.toArray[Int]).toArray[Array[Int]], Binv.map(_.toArray[Int]).toArray[Array[Int]])
  private def mul(A: Array[Array[Int]], Binv: Array[Array[Int]]): Seq[Seq[Int]] = {
    assert(A(0).length == Binv(0).length)
    val result: Array[Array[Int]] = Array.ofDim[Int](A.length, Binv.length)

    // use for loop to allow scala to automatically parallelize the computation (with 'map' it would take ages)
    for (i <- A.indices; j <- Binv.indices) {
      var accum = 0
      for (k <- A(0).indices) {
        accum += A(i)(k) * Binv(j)(k)
      }
      result(i)(j) = accum
    }
    result.map(_.toSeq).toSeq
  }

  def tiledMul(thatInverted: Matrix, tileSize: Int, cache: Boolean = true): Matrix = {
    val fileName = this.name + "_mul_" + thatInverted.name
    if (cache) {
      Matrix.fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(tiledMul(this.data, thatInverted.data, tileSize), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(tiledMul(this.data, thatInverted.data, tileSize), fileName)
    }
  }

  private def tiledMul(A: Seq[Seq[Int]], Binv: Seq[Seq[Int]], tileSize: Int): Seq[Seq[Int]] = tiledMul(A.map(_.toArray[Int]).toArray[Array[Int]], Binv.map(_.toArray[Int]).toArray[Array[Int]], tileSize)
  private def tiledMul(A: Array[Array[Int]], Binv: Array[Array[Int]], tileSize: Int): Seq[Seq[Int]] = {
    assert(A(0).length == Binv(0).length)
    val result: Array[Array[Int]] = Array.ofDim[Int](A.length, Binv.length)

    val numOfATiles = A.length / tileSize
    val numOfBinvTiles = Binv.length / tileSize
    assert(tileSize % numOfBinvTiles == 0)
    // use for loop to allow scala to automatically parallelize the computation (with 'map' it would take ages)

    for (i <- 0 until numOfATiles; j <- 0 until numOfBinvTiles) {
      for (ii <- 0 until tileSize; jj <- 0 until tileSize) {
        var accum = 0
        for (k <- A(0).indices) {
          accum += A(i * tileSize + ii)(k) * Binv(j * tileSize + jj)(k)
        }
        result(i * tileSize + (ii / numOfBinvTiles + j * tileSize / numOfBinvTiles))((ii % numOfBinvTiles) * tileSize + jj) = accum
      }
    }
    result.map(_.toSeq).toSeq
  }

  def add(that: Matrix, cache: Boolean = true): Matrix = {
    val fileName = this.name + "_add_" + that.name
    if (cache) {
      Matrix.fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(add(this.data, that.data), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(add(this.data, that.data), fileName)
    }
  }

  private def add(A: Seq[Seq[Int]], B: Seq[Seq[Int]]): Seq[Seq[Int]] = add(A.map(_.toArray[Int]).toArray[Array[Int]], B.map(_.toArray[Int]).toArray[Array[Int]])
  private def add(A: Array[Array[Int]], B: Array[Array[Int]]): Seq[Seq[Int]] = {
    assert(A(0).length == B(0).length)
    assert(A(1).length == B(1).length)
    val result: Array[Array[Int]] = Array.ofDim[Int](A.length, B.length)

    // use for loop to allow scala to automatically parallelize the computation (with 'map' it would take ages)
    for (i <- A.indices; j <- B.indices) {
      result(i)(j) = A(i)(j) + B(i)(j)
    }
    result.map(_.toSeq).toSeq
  }

  def pad(topSize: Int, bottomSize: Int, leftSize: Int, rightSize: Int, cache: Boolean = true, verbose: Boolean = false): Matrix = {
    val fileName = if(verbose) this.name + "_pad_" + topSize.toString + "_" + bottomSize.toString + "_" + leftSize.toString + "_" + rightSize.toString else this.name + "P"
    if (cache) {
      Matrix.fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(pad(this.data, topSize, bottomSize, leftSize, rightSize), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(pad(this.data, topSize, bottomSize, leftSize, rightSize), fileName)
    }
  }

  private def pad(A: Seq[Seq[Int]], topSize: Int, bottomSize: Int, leftSize: Int, rightSize: Int): Seq[Seq[Int]] = pad(A.map(_.toArray[Int]).toArray[Array[Int]], topSize, bottomSize, leftSize, rightSize)
  private def pad(A: Array[Array[Int]], topSize: Int, bottomSize: Int, leftSize: Int, rightSize: Int): Seq[Seq[Int]] = {
    val result: Array[Array[Int]] = Array.ofDim[Int](topSize + A.length + bottomSize, leftSize + A(0).length + rightSize)
    for (i <- result.indices) {
      for (j <- result(0).indices) {
        if(i < topSize || i > result.length - 1 - bottomSize || j < leftSize || j > result(0).length - 1 - rightSize) {
          result(i)(j) = 0
        } else {
          result(i)(j) = A(i - topSize)(j - leftSize)
        }
      }
    }
    result.map(_.toSeq).toSeq
  }

  def conv(weight: Matrix, inChannel: Int, cache: Boolean = true, weightTranspose: Boolean = true, verbose: Boolean = false): Matrix = {
    val fileName = if(verbose) this.name + "_mul_" + weight.name + "_" + inChannel.toString else  this.name + "C"
    if (cache) {
      Matrix.fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(conv(this.data, weight.data, inChannel, weightTranspose), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(conv(this.data, weight.data, inChannel, weightTranspose), fileName)
    }
  }

  private def conv(A: Seq[Seq[Int]], weight: Seq[Seq[Int]], inChannel: Int, weightTranspose: Boolean): Seq[Seq[Int]] = conv(A.map(_.toArray[Int]).toArray[Array[Int]], weight.map(_.toArray[Int]).toArray[Array[Int]], inChannel, weightTranspose)
  private def conv(A: Array[Array[Int]], weight: Array[Array[Int]], inChannel: Int, weightTranspose: Boolean): Seq[Seq[Int]] = {
    if(weightTranspose) {
      val wSize = math.sqrt(weight(0).length / inChannel)
      assert(wSize % 1 == 0 && A(0).length % inChannel == 0 && A.length > wSize && A(0).length / inChannel > wSize)
      val newIWidth = (A(0).length / inChannel).toInt - wSize.toInt + 1
      val result: Array[Array[Int]] = Array.ofDim[Int](A.length - wSize.toInt + 1, newIWidth * weight.length)

      // use for loop to allow scala to automatically parallelize the computation (with 'map' it would take ages)
      for (i <- result.indices) {
        for (j <- 0 to newIWidth - 1) {
          for (k <- weight.indices) {
            var accum = 0
            for(m <- 0 to wSize.toInt - 1) {
              for(n <- 0 to wSize.toInt - 1) {
                for(l <- 0 to inChannel - 1) {
                  accum += A(i + n)((j + m) * inChannel + l) * weight(k)(m * wSize.toInt * inChannel + n * inChannel + l)
                }
              }
            }
            result(i)(j * weight.length + k) = accum
          }
        }
      }
      result.map(_.toSeq).toSeq
    } else {
      val wSize = math.sqrt(weight(0).length / inChannel)
      assert(wSize % 1 == 0 && A(0).length % inChannel == 0 && A.length > wSize && A(0).length / inChannel > wSize,
        "Matrix Size " + A.length + " " + A(0).length + " " + inChannel)
      val newIWidth = (A(0).length / inChannel).toInt - wSize.toInt + 1
      val result: Array[Array[Int]] = Array.ofDim[Int](A.length - wSize.toInt + 1, newIWidth * weight.length)

      // use for loop to allow scala to automatically parallelize the computation (with 'map' it would take ages)
      for (i <- result.indices) {
        for (j <- 0 to newIWidth - 1) {
          for (k <- weight.indices) {
            var accum = 0
            for(m <- 0 to wSize.toInt - 1) {
              for(n <- 0 to wSize.toInt - 1) {
                for(l <- 0 to inChannel - 1) {
                  accum += A(i + n)((j + m) * inChannel + l) * weight(k)(n * wSize.toInt * inChannel + m * inChannel + l)
                }
              }
            }
            result(i)(j * weight.length + k) = accum
          }
        }
      }
      result.map(_.toSeq).toSeq
    }
  }

  def reshape(innerChunk: Int, cache: Boolean = true, verbose: Boolean = false): Matrix = {
    val fileName = if(verbose) this.name + "_reshape_" + innerChunk.toString else this.name + "R"
    if (cache) {
      Matrix.fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(reshape(this.data, innerChunk), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(reshape(this.data, innerChunk), fileName)
    }
  }

  private def reshape(A: Seq[Seq[Int]], innerChunk: Int): Seq[Seq[Int]] = reshape(A.map(_.toArray[Int]).toArray[Array[Int]], innerChunk)
  private def reshape(A: Array[Array[Int]], innerChunk: Int): Seq[Seq[Int]] = {

    val result: Array[Array[Int]] = Array.ofDim[Int](A.length * innerChunk, A(0).length / innerChunk)
    val innerSize = A(0).length / innerChunk
    for (i <- A.indices) {
      for (j <- A(0).indices) {
        result(i * innerChunk + j / innerSize)(j % innerSize) = A(i)(j)
      }
    }
    result.map(_.toSeq).toSeq
  }

  //
  def deReshape(innerChunk: Int, cache: Boolean = true, verbose: Boolean = false): Matrix = {
    val fileName = if(verbose) this.name + "_Dreshape_" + innerChunk.toString else this.name + "DR"
    if (cache) {
      Matrix.fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(deReshape(this.data, innerChunk), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(deReshape(this.data, innerChunk), fileName)
    }
  }

  private def deReshape(A: Seq[Seq[Int]], innerChunk: Int): Seq[Seq[Int]] = deReshape(A.map(_.toArray[Int]).toArray[Array[Int]], innerChunk)
  private def deReshape(A: Array[Array[Int]], innerChunk: Int): Seq[Seq[Int]] = {

    val result: Array[Array[Int]] = Array.ofDim[Int](A.length / innerChunk, A(0).length * innerChunk)
    for (i <- A.indices) {
      for (j <- A(0).indices) {
        result(i / innerChunk)(j + (i % innerChunk) * A(0).length) = A(i)(j)
      }
    }
    result.map(_.toSeq).toSeq
  }

  def pool(windowSize: Int, stepSize: Int, inChannel: Int, cache: Boolean = true, verbose: Boolean = false): Matrix = {
    val fileName = if(verbose) this.name + "_pool_" + windowSize.toString + "_" + stepSize.toString + "_" + inChannel.toString else this.name + "P"
    if (cache) {
      Matrix.fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(pool(this.data, windowSize, stepSize, inChannel), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(pool(this.data, windowSize, stepSize, inChannel), fileName)
    }
  }

  private def pool(A: Seq[Seq[Int]], windowSize: Int, stepSize: Int, inChannel: Int): Seq[Seq[Int]] = pool(A.map(_.toArray[Int]).toArray[Array[Int]], windowSize, stepSize, inChannel)
  private def pool(A: Array[Array[Int]], windowSize: Int, stepSize: Int, inChannel: Int): Seq[Seq[Int]] = {
    val innerDimSize = A(0).length / inChannel
    val newInnerDimSize = (innerDimSize - windowSize) / stepSize + 1
    val result: Array[Array[Int]] = Array.ofDim[Int]((A.length - windowSize) / stepSize + 1, newInnerDimSize * inChannel)
    for (i <- result.indices) {
      for (j <- 0 until newInnerDimSize) {
        for (k <- 0 until inChannel) {
          result(i)(j * inChannel + k) = A(i * stepSize)(j * stepSize * inChannel + k)
          for(m <- 0 until windowSize) {
            for(n <- 0 until windowSize) {
              if(result(i)(j * inChannel + k) < A(i * stepSize + m)((j * stepSize + n) * inChannel + k)) {
                result(i)(j * inChannel + k) = A(i * stepSize + m)((j * stepSize + n) * inChannel + k)
              }
            }
          }
        }
      }
    }
    result.map(_.toSeq).toSeq
  }

  def addBias(that: Matrix, cache: Boolean = true, verbose: Boolean = false): Matrix = {
    val fileName = if(verbose) this.name +  "_add_bias_" + that.name else this.name + "b"
    if (cache) {
      Matrix.fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(addBias(this.data, that.data), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(addBias(this.data, that.data), fileName)
    }
  }

  private def addBias(A: Seq[Seq[Int]], B: Seq[Seq[Int]]): Seq[Seq[Int]] = addBias(A.map(_.toArray[Int]).toArray[Array[Int]], B.map(_.toArray[Int]).toArray[Array[Int]])
  private def addBias(A: Array[Array[Int]], B: Array[Array[Int]]): Seq[Seq[Int]] = {
    assert(B.length == 1)
    val result: Array[Array[Int]] = Array.ofDim[Int](A.length, A(0).length)

    // Add a few cases for if bias shape does not match with input matrix.
    if(A(0).length == B(0).length)
      for (i <- A.indices; j <- A(0).indices)
        result(i)(j) = A(i)(j) + B(0)(j)
    else if(A(0).length % B(0).length == 0)
      for (i <- A.indices; j <- A(0).indices)
        result(i)(j) = A(i)(j) + B(0)(j % B(0).length)
    else if(A(0).length == 1 && A.length == B(0).length)
      for (i <- A.indices; j <- A(0).indices)
        result(i)(j) = A(i)(j) + B(0)(i)
    else
      ???
    result.map(_.toSeq).toSeq
  }

  /**
   *
   * @param initialCentroids
   *   a sequence of centroids for each cluster. The length of the outer sequence is the 'k' in k-means-clustering.
   *   Each centroid is defined by a sequence of N values to locate it in the N-dim space.
   * @param cache
   *   whether the matrix is read from memory or recomputed each time
   * @return
   *   a new sequence of centroids. It can be used as the new initial centroids to achieve an iterative algorithm.
   */
  def kMeansClustering(initialCentroids: Seq[Seq[Int]], cache: Boolean = true): Matrix = {
    val fileName = this.name + "_kMeansClustering"
    if (cache) {
      Matrix.fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(kMeansClustering(this.data, initialCentroids), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(kMeansClustering(this.data, initialCentroids), fileName)
    }
  }

  private def kMeansClustering(points: Seq[Seq[Int]], initialCentroids: Seq[Seq[Int]]): Seq[Seq[Int]] = {
    assert(points.head.length == initialCentroids.head.length) // same dimensions for points from the data set as well as initial centroids of the clusters

    val clusteredPoints: Map[Seq[Int], Int] = points.map(p => {
      val distances = initialCentroids.map(centroid => p.zip(centroid).map(t => (t._1 - t._2) * (t._1 - t._2)).sum)
      val clusterId = distances.zipWithIndex.reduce((m1, m2) => if (m1._1 > m2._1) m2 else m1)._2 // same as minBy

      (p, clusterId)
    }).toMap

    val clusteredPointsVec: Seq[Seq[(Seq[Int], Int)]] =
      clusteredPoints.map(p => Seq.fill(initialCentroids.length)((Seq.fill(initialCentroids.head.length)(0), 0)).updated(p._2, (p._1, 1))).toSeq

    val centroidsZero: Seq[(Seq[Int], Int)] = Seq.fill(initialCentroids.length)((Seq.fill(initialCentroids.head.length)(0), 0))

    val summedClusteredPoints: Seq[(Seq[Int], Int)] =
      clusteredPointsVec.foldLeft(centroidsZero)((centroid, newPoint) =>
        centroid.zip(newPoint).map(t => (t._1._1.zip(t._2._1).map(x => x._1 + x._2), t._1._2 + t._2._2))
      )

    val finalCentroids = summedClusteredPoints.map(c => c._1.map(point => point / c._2))

    finalCentroids
  }

  def sumRows(): Matrix = Matrix(Seq(data.map(_.sum)), this.name + "_sumrows")

  def inc(): Matrix = Matrix(data.map(_.map(_ + 1)), name + "_inc")

  def mod(divisor: Int, verbose: Boolean = false): Matrix = Matrix(data.map(_.map(_ % divisor)), if(verbose == true) name + "_mod_" + divisor else name + "m")

  def mult(mutiplier: Int, verbose: Boolean = false): Matrix = Matrix(data.map(_.map(_ * mutiplier)), if(verbose == true) name + "_mult_" + mutiplier else name + "m")

  def max(comparee: Int, verbose: Boolean = false): Matrix = Matrix(data.map(_.map(in => if(in < comparee) comparee else in)), if(verbose == true) name + "_mod_" + comparee else name + "m")

  def round(pow2: Int): Matrix = Matrix(data.map(_.map(x => Math.rint(x / Math.pow(2, pow2)).toInt)), name + "_r_" + pow2)

  def clip(pow2: Int): Matrix = Matrix(data.map(_.map(x => if (x >= Math.pow(2, pow2 - 1).toInt) Math.pow(2, pow2 - 1).toInt - 1 else
    if(x < -Math.pow(2, pow2 - 1).toInt) -Math.pow(2, pow2 - 1).toInt else  x)), name + "_c_" + pow2)

  def toCSV(): Unit = {
    val folder = new File(MATRICES_PATH)
    if (!folder.exists() || !folder.isDirectory) {
      folder.mkdirs
    }
    val file = new PrintWriter(new File(MATRICES_PATH + name + MATRIX_FILE_ENDING))
    file.write(data.map(_.mkString(", ")).mkString("\n"))
    file.close()
  }
}

object Matrix {

  final val MATRICES_PATH = "resources/matrices/"
  final val MATRIX_FILE_ENDING = ".csv"

  def fromCSV(fileName: String): Option[Matrix] = {
    val fullPath = MATRICES_PATH + fileName + MATRIX_FILE_ENDING
    if (!new File(fullPath).exists()) {
      None
    } else {
      val file = Source.fromFile(fullPath)
      val matrix = Matrix(file.getLines().map(_.split(",").map(_.filter(ch =>
        ch == '-' || ch.isDigit).toInt).toList).toList, fileName)
      file.close()
      Some(matrix)
    }
  }

  def fromRandom(rows: Int, cols: Int, maxInt: Int = 256, cacheId: Option[String] = None): Matrix = {
    val r = scala.util.Random
    val fileName = cacheId match {
      case Some(c) => s"matrix_${rows}_${cols}_random${maxInt}_${c}"
      case None => s"matrix_${rows}_${cols}_random${maxInt}"
    }
    if (cacheId.isDefined) {
      fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(Seq.fill(rows)(Seq.fill(cols)(r.nextInt(maxInt))), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(Seq.fill(rows)(Seq.fill(cols)(r.nextInt(maxInt))), fileName)
    }
  }

  def fromCounter(rows: Int, cols: Int, repetitions: Int = 1, cacheId: Option[String] = None): Matrix = {
    val fileName = cacheId match {
      case Some(c) => s"matrix_${rows}_${cols}_counter_rep${repetitions}_${c}"
      case None => s"matrix_${rows}_${cols}_counter_rep${repetitions}"
    }
    if (cacheId.isDefined) {
      fromCSV(fileName) match {
        case Some(matrix) => matrix
        case None =>
          val newMatrix = Matrix(Seq.fill(rows)((1 to (cols / repetitions)).flatMap(Seq.fill(repetitions)(_))), fileName)
          newMatrix.toCSV()
          newMatrix
      }
    } else {
      Matrix(Seq.fill(rows)((1 to (cols / repetitions)).flatMap(Seq.fill(repetitions)(_))), fileName)
    }
  }

}
