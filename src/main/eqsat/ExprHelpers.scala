package eqsat

import core.{BuiltinExpr, Expr, ParamDef, ShirIR}

object ExprHelpers {
  /**
   * Gets an expression's arguments or expression children.
   * @param expr The expression to inspect.
   * @param stripParamDefs Strips `ParamDef` values from the results if set to `true`.
   * @return A sequence of child expressions.
   */
  def args(expr: Expr, stripParamDefs: Boolean = true): Seq[Expr] = expr match {
    case builtin: BuiltinExpr => removeParamDefs(builtin.args, stripParamDefs)
    case other => removeParamDefs(other.children collect { case e: Expr => e }, stripParamDefs)
  }

  private def removeParamDefs(values: Seq[Expr], proceed: Boolean = true) = {
    if (proceed)
      values collect { case e if !e.isInstanceOf[ParamDef] => e }
    else
      values
  }

  /**
   * Gets all parameter-valued arguments or expression children.
   * @param expr The expression to examine.
   * @return A sequence of parameter definitions.
   */
  def params(expr: Expr): Seq[ParamDef] =
    args(expr, stripParamDefs = false).collect({ case p: ParamDef => p })

  /**
   * Builds a version of an expression with updated arguments or expression children.
   * @param expr The expression to build.
   * @param newArgs A sequence of updated arguments or expression children.
   * @return An expression that is identical to `expr` save for its arguments.
   */
  def buildFromArgs(expr: Expr, newArgs: Seq[Expr]): Expr = expr match {
    case builtin: BuiltinExpr =>
      val newChildren = substituteArgs(builtin.args.toList, newArgs.toList).map(x => x.asInstanceOf[Expr])
      builtin.buildFromArgs(newChildren)
    case _ =>
      val newChildren = substituteArgs(expr.children.toList, newArgs.toList)
      expr.build(newChildren)
  }

  private def substituteArgs(oldChildren: List[ShirIR], args: List[Expr]): List[ShirIR] = oldChildren match {
    case Nil => Nil
    case (x: ParamDef) :: xs => x :: substituteArgs(xs, args) // Skip ParamDef values.
    case (_: Expr) :: xs => args.head :: substituteArgs(xs, args.tail)
    case x :: xs => x :: substituteArgs(xs, args)
  }

  def isSameOp(first: Expr, second: Expr): Boolean = first.nodeKind == second.nodeKind
}