package eqsat

import scala.util.Random

/**
 * A rewrite rule annotated with a match limit and a ban length. The match limit is the maximum number of matches that
 * can be applied for this rule. Once this limit is reached, the rule is banned for a number of iterations equal to the
 * ban length. After the ban length has passed, the rule can be applied again. At this point both the match limit and
 * the ban length are doubled.
 *
 * @param rule The rewrite rule.
 * @param initialMatchLimit The initial match limit.
 * @param initialBanLength The initial ban length.
 */
final case class BackoffRule(rule: Rewrite, initialMatchLimit: Int, initialBanLength: Int)

/**
 * A saturation strategy that applies a set of rewrite rules with backoff scheduling. The strategy applies rules freely
 * until a match limit is reached, at which point the rule is banned for a number of iterations. After the ban length
 * has passed, the rule can be applied again. At this point both the match limit and the ban length are doubled.
 * @param rules A sequence of rules annotated with match limits and ban lengths.
 */
final case class BackoffSchedulingSaturationStrategy(rules: Seq[BackoffRule], random: Random = new Random(0)) extends SaturationStrategy {

  override def instantiate(): SaturationRunner = new BackoffSchedulingSaturationRunner(
    rules.map(rule => new RuleStats(rule.rule, rule.initialMatchLimit, rule.initialBanLength)), random)
}

/**
 * Statistics for a rule in the backoff scheduling strategy.
 * @param rule The rewrite rule.
 * @param matchLimit The current match limit.
 * @param banLength The current ban length.
 */
private final class RuleStats(val rule: Rewrite, var matchLimit: Int, var banLength: Int) {
  /**
   * The iteration at which the rule is banned until. If the rule is not banned, this value is `None`.
   */
  var bannedUntil: Option[Int] = None

  /**
   * The number of matches remaining for this rule before it gets banned.
   */
  var remainingMatches: Int = matchLimit
}

private final class BackoffSchedulingSaturationRunner(rules: Seq[RuleStats], random: Random) extends SaturationRunner {

  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = {
    // Unban previously-banned rules
    val previouslyBanned = rules.filter(_.bannedUntil.contains(iteration))
    for (rule <- previouslyBanned) {
      rule.bannedUntil = None
      rule.matchLimit *= 2
      rule.banLength *= 2
      rule.remainingMatches = rule.matchLimit
    }

    val notBanned = rules.filter(_.bannedUntil.forall(_ < iteration))
    val matches = engine.findMatches(notBanned.map(_.rule))

    // For each rule, take a random selection of rules to apply, up to the match limit. Decrement remaining match counts
    // on the fly and ban rules that have reached their match limit.
    val selectedMatches = matches.map { case (rule, matches) =>
      val ruleStats = notBanned.find(_.rule == rule).get
      val limit = ruleStats.remainingMatches min matches.length
      ruleStats.remainingMatches -= limit
      if (ruleStats.remainingMatches == 0) {
        ruleStats.bannedUntil = Some(iteration + ruleStats.banLength)
      }
      (rule, random.shuffle(matches).take(limit))
    }

    engine.applyMatches(selectedMatches).nonEmpty
  }
}
