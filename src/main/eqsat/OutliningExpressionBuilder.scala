package eqsat
import cGen.CLambda
import core._

import scala.collection.mutable

/**
  * A helper that constructs expressions from e-classes and outlines on the fly.
  * @param selection A mapping of e-classes to the e-nodes that are selected for them.
  * @param mustOutline A mapping of e-classes to the determination of whether they are outlined or not.
  * @param buildLambda Constructs a new lambda.
  * @param buildFunctionCall Constructs a function call.
  */
final case class OutliningExpressionBuilder(selection: PartialFunction[EClassT, ENodeT],
                                            mustOutline: PartialFunction[EClassT, Boolean])
                                           (implicit buildLambda: (ParamDef, Expr) => LambdaT = (p, body) => CLambda(p, body),
                                            buildFunctionCall: (Expr, Expr) => Expr = FunctionCall(_, _)){

  private case class AnonymousOutlinedFunction(arguments: Seq[DeBruijnParamUse], body: Expr) {
    val function: Expr = {
      arguments.foldLeft(body)((body, arg) => DeBruijnLambda(arg.t, body))
    }
  }

  private case class OutlinedFunction(name: ParamDef, anonymous: AnonymousOutlinedFunction) {
    val function: Expr = anonymous.function

    def arguments: Seq[DeBruijnParamUse] = anonymous.arguments

    val call: Expr = {
      arguments.reverse.foldLeft(ParamUse(name): Expr)((f, arg) => buildFunctionCall(f, arg))
    }

    def bind(expr: Expr): Expr = {
      val func = function
      buildFunctionCall(buildLambda(name, DeBruijnParamUse.incrementUnbound(expr)), func)
    }
  }

  private class TreeBuilder {
    val outlinedFunctions: mutable.LinkedHashMap[EClassT, OutlinedFunction] = mutable.LinkedHashMap[EClassT, OutlinedFunction]()

    private def incrementAll(uses: Seq[DeBruijnParamUse], amount: Int): Seq[DeBruijnParamUse] = {
      uses.collect({
        case use: DeBruijnParamUse if use.index + amount >= 0 => DeBruijnParamUse(use.index + amount, use.t)
      })
    }

    private def collectArgs(expr: ShirIR): Seq[DeBruijnParamUse] = {
      expr match {
        case p: DeBruijnParamUse => Seq(p)
        case lambda: LambdaT => incrementAll(collectArgs(lambda.body), -1)
        case DeBruijnShift(expr, _) => incrementAll(collectArgs(expr), 1)
        case _ => expr.children.flatMap(collectArgs)
      }
    }

    private def renumberArgs(expr: ShirIR, argRenumbering: Map[Int, Int]): ShirIR = {
      expr match {
        case p: DeBruijnParamUse =>
          argRenumbering.get(p.index) match {
            case Some(newIndex) => DeBruijnParamUse(newIndex, p.t)
            case None => p
          }

        case lambda: LambdaT =>
          val newNumbering = argRenumbering.map(p => (p._1 + 1, p._2 + 1))
          lambda.build(lambda.children.map(renumberArgs(_, newNumbering)))

        case DeBruijnShift(expr, _) =>
          val newNumbering = argRenumbering.map(p => (p._1 - 1, p._2 - 1))
          DeBruijnShift(renumberArgs(expr, newNumbering).asInstanceOf[Expr])

        case _ => expr.build(expr.children.map(renumberArgs(_, argRenumbering)))
      }
    }

    private def outline(expr: Expr): OutlinedFunction = {
      val args = collectArgs(expr).filter(p => p.index >= 0).sortBy(_.index)
      val argRenumbering = args.map(_.index).zipWithIndex.toMap
      val exprWithRenumberedArgs = renumberArgs(expr, argRenumbering)
      val anon = AnonymousOutlinedFunction(args, exprWithRenumberedArgs.asInstanceOf[Expr])
      OutlinedFunction(ParamDef(anon.function.t), anon)
    }

    private def extract(eClass: EClassT): Expr = {
      val node = selection(eClass)
      node.instantiate(node.args.map(toExpr))
    }

    def toExpr(eClass: EClassT): Expr = {
      if (mustOutline(eClass)) {
        outlinedFunctions.get(eClass) match {
          case Some(f) => f.call
          case None =>
            val expr = extract(eClass)
            val f = outline(expr)
            outlinedFunctions(eClass) = f
            f.call
        }
      } else {
        extract(eClass)
      }
    }
  }

  def toExpr(eClass: EClassT): Expr = {
    val builder = new TreeBuilder()
    val extracted = builder.toExpr(eClass)
    val functions = builder.outlinedFunctions.values.toSeq.reverse
    val bound = functions.foldLeft(extracted)((expr, func) => func.bind(expr))
    DeBruijnTransform.forward(bound)
  }
}
