package eqsat

import scala.util.Random

/**
 * A rebasing rewrite strategy that reimplements the Sympy strategy. This strategy involves repeatedly saturating the
 * e-graph for a fixed number of cycles, each of which consists of a fixed number of iterations. The strategy uses a
 * backoff scheduling strategy to limit the number of times a rule can be applied in a single iteration. The strategy
 * also rebases the e-graph after each cycle, except the last one.
 *
 * @param rules The rewrite rules to apply.
 * @param cycles The number of equality saturation cycles to run the strategy for.
 * @param limitCycle A function that takes a saturation strategy and returns a new saturation strategy that limits the
 *                   duration of a cycle. By default, the function limits the number of iterations per cycle to 30.
 * @param ruleApplicationLimit The maximum number of times a rule can be applied in a single iteration.
 * @param ruleBanLength The number of iterations a rule is banned for after reaching the rule application limit.
 * @param rebase Whether to rebase the e-graph after each cycle, except the last one. Default is `true`.
 * @param random A random number generator.
 * @tparam ENode The type of e-node in the e-graph.
 * @tparam EClass The type of e-class in the e-graph.
 */
final case class SympyStrategy[ENode <: ENodeT, EClass <: EClassT](rules: Seq[Rewrite],
                                                                   cycles: Int = 2,
                                                                   limitCycle: SaturationStrategy => SaturationStrategy =
                                                                     IterationLimitedSaturationStrategy(_, 30),
                                                                   ruleApplicationLimit: Int = 2500,
                                                                   ruleBanLength: Int = 5,
                                                                   rebase: Boolean = true,
                                                                   random: Random = new Random(0)) extends RebasingStrategy[ENode, EClass] {
  override def apply(engine: RebasingRewriteEngine[ENode, EClass]): Unit = {
    val strategy = BackoffSchedulingSaturationStrategy(rules.map(BackoffRule(_, ruleApplicationLimit, ruleBanLength)), random)
    for (i <- 0 until cycles) {
      engine.saturate(limitCycle(strategy))
      if (rebase && i != cycles - 1) {
        engine.rebase()
      }
    }
  }
}
