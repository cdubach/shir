package eqsat.explain

import core.{Expr, Formatter, TextFormatter, Type}
import eqsat.EqualitySaturationExpr

case class AnnotationExpr(override val innerIR: Expr, annotation: String, annotationArgs: Seq[Expr]) extends EqualitySaturationExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): AnnotationExpr = AnnotationExpr(newInnerIR, annotation, annotationArgs)
  override def types: Seq[Type] = Seq()

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        val arg = if (annotationArgs.length == 1) {
          formatter.formatNonAtomic(annotationArgs.head)
        } else {
          val argString = annotationArgs.map(formatter.formatAtomic).mkString(" ")
          s"{ $argString }"
        }
        Some(formatter.makeAtomic(s"[$annotation] $arg"))
      case _ => None
    }
  }
}

object Annotation {
  def apply(annotation: String, args: Seq[Expr]): AnnotationExpr = AnnotationExpr(args.last, annotation, args)

  def unapply(expr: AnnotationExpr): Some[(String, Seq[Expr])] = Some(expr.annotation, expr.annotationArgs)
}
