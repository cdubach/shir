package eqsat.explain

/**
 * The reason why an e-node exists in an e-graph.
 * @tparam C The type of a node in the explainer's explain-find data structure.
 */
sealed trait NodeProvenance[C] {
  def isTentative: Boolean

  final def isPermanent: Boolean = !isTentative
}

/**
 * Notes that a node was created for reasons that are as of yet unclear. This existence reason serves as a temporary
 * placeholder until the providence of a node is resolved.
 * @tparam C The type of a node in the explainer's explain-find data structure.
 */
final case class TentativeProvenance[C]() extends NodeProvenance[C] {
  override def isTentative: Boolean = true
}

/**
 * Asserts that a node was inserted directly.
 * @tparam C The type of a node in the explainer's explain-find data structure.
 */
final case class InsertedDirectly[C]() extends NodeProvenance[C] {
  override def isTentative: Boolean = false
}

/**
 * Asserts that a node was created by rewriting some adjacent node in the explain-find data structure.
 * @param adjacentNode The adjacent node in the explain-find from which the node was created.
 * @tparam C The type of a node in the explainer's explain-find data structure.
 */
final case class ExistsDueToRewrite[C](adjacentNode: C) extends NodeProvenance[C] {
  override def isTentative: Boolean = false
}

/**
 * Asserts that a node was created by rewriting some adjacent node in the explain-find data structure, but that the
 * union operation to confirm this has not yet been performed.
 * @param adjacentNode The adjacent node in the explain-find from which the node was created.
 * @tparam C The type of a node in the explainer's explain-find data structure.
 */
final case class TentativelyExistsDueToRewrite[C](adjacentNode: C) extends NodeProvenance[C] {
  override def isTentative: Boolean = true
}

/**
 * Asserts that a node was created as a result of inserting a parent node.
 * @param parentNode A parent node, which features the created node as one of its arguments.
 * @tparam C The type of a node in the explainer's explain-find data structure.
 */
final case class ExistsDueToParent[C](parentNode: C) extends NodeProvenance[C] {
  override def isTentative: Boolean = false
}
