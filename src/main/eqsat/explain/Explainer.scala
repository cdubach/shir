package eqsat.explain

import eqsat.{EClassT, ENodeT}

import scala.collection.mutable

/**
 * A node in the explainer's union-find data structure.
 * @param node The node whose presence and equivalence to other nodes is explained.
 * @param next A node to which this node is equivalent.
 * @param current The non-canonical e-class that gave rise to this node.
 * @param justification A justification for why this node exists.
 * @param provenance The reason why this node was created.
 * @param isRewriteForward If `justification` is a rewrite, indicates whether the rewrite was performed forward or
 *                         backward.
 * @tparam L The type of an e-node.
 * @tparam C The type of an e-class.
 */
final class ExplainNode[L <: ENodeT, C <: EClassT](val node: L,
                                                   var next: C,
                                                   val current: C,
                                                   var justification: Justification,
                                                   var provenance: NodeProvenance[C],
                                                   var isRewriteForward: Boolean)

final class Explainer[L <: ENodeT, C <: EClassT] {
  type ExplainCache = mutable.HashMap[(C, C), TreeTerm[L]]
  type NodeExplanationCache = mutable.HashMap[C, TreeTerm[L]]

  private val explainFind = mutable.HashMap[C, ExplainNode[L, C]]()
  private val uncanonMemo = mutable.HashMap[L, C]()

  private def checkInvariants(node: ExplainNode[L, C]): Unit = {
    // If the node is justified by congruence, then we ought to check that congruence.
    node.justification match {
      case Congruence =>
        assert(node.node.matches(explainFind(node.next).node))

      case _ =>
    }

    checkProvenance(node)
  }

  private def checkProvenance(node: ExplainNode[L, C]): Unit = {
    node.provenance match {
      case TentativeProvenance() | InsertedDirectly() | TentativelyExistsDueToRewrite(_) =>
      case ExistsDueToRewrite(adjacentNode) =>
      //        assert(adjacentNode == node.next || explainFind(adjacentNode).next == node.current)
      case ExistsDueToParent(parentNode) =>
        assert(explainFind(parentNode).node.args.contains(node.current))
    }
  }

  def areEquivalent(left: C, right: C): Boolean = tryFindCommonAncestor(left, right).isDefined

  def add(node: L, set: C, provenance: NodeProvenance[C]): C = {
    assert(!uncanonMemo.contains(node))
    assert(!explainFind.contains(set))

    uncanonMemo(node) = set
    val nodeData = new ExplainNode(node, set, set, Congruence, provenance, false)
    explainFind(set) = nodeData
    set
  }

  /**
   * Takes a (possibly uncanonicalized) node and maps it to its (possibly uncanonicalized) class.
   * @param node The node to map to a class.
   * @return A class for `node`, if this explainer has one; otherwise, `None`.
   */
  def nodeToClass(node: L): Option[C] = uncanonMemo.get(node)

  def setProvenance(node: C, provenance: NodeProvenance[C]): Unit = {
    val nodeData = explainFind(node)
    nodeData.provenance = provenance
  }

  def getProvenance(node: C): NodeProvenance[C] = explainFind(node).provenance

  def union(node1: C, node2: C, justification: Justification): Unit = {
    val node1Data = explainFind(node1)
    val node2Data = explainFind(node2)

    // If we previously flagged either node as tentatively existing due to a rewrite, then now is the time to formalize
    // and check that relationship.
    (node1Data.provenance, node2Data.provenance) match {
      // Steer clear of situations wherein we might falsely resolve tentative provenances.
      case (TentativelyExistsDueToRewrite(_), ExistsDueToRewrite(adjacent)) if adjacent == node1 =>
      case (ExistsDueToRewrite(adjacent), TentativelyExistsDueToRewrite(_)) if adjacent == node2 =>

      case (TentativelyExistsDueToRewrite(adjacent), _) =>
        assert(node2 == adjacent)
        node1Data.provenance = ExistsDueToRewrite(adjacent)

      case (_, TentativelyExistsDueToRewrite(adjacent)) =>
        assert(node1 == adjacent)
        node2Data.provenance = ExistsDueToRewrite(adjacent)

      case _ =>
    }

    justification match {
      case Congruence =>
        assert(explainFind(node1).node.matches(explainFind(node2).node))

      case _ =>
    }

    if (!areEquivalent(node1, node2)) {
      makeLeader(node1)
      node1Data.next = node2
      node1Data.justification = justification
      node1Data.isRewriteForward = true
    }

    checkInvariants(node1Data)
    checkInvariants(node2Data)
  }

  /**
   * Asserts that this explainer's data structures are consistent. Moreover, asserts that all node provenances are
   * permanent.
   */
  def assertConsistency(): Unit = {
    for ((_, c) <- uncanonMemo) {
      checkInvariants(explainFind(c))
      assert(explainFind(c).provenance.isPermanent)
    }
  }

  /**
   * Reverses edges recursively to make `node` the leader.
   */
  private def makeLeader(node: C): Unit = {
    val next = explainFind(node).next
    if (next != node) {
      makeLeader(next)
      val nextData = explainFind(next)
      val nodeData = explainFind(node)
      nextData.justification = nodeData.justification
      nextData.isRewriteForward = !nodeData.isRewriteForward
      nextData.next = node
      checkInvariants(nextData)
      checkInvariants(nodeData)
    }
  }

  def explainExistence(left: C): TreeExplanation[L] = {
    val cache = new ExplainCache()
    val eNodeCache = new NodeExplanationCache()
    explainENodeExistence(left, nodeToExplanation(left, eNodeCache), cache, eNodeCache)
  }

  def explainEquivalence(left: C, right: C): TreeExplanation[L] = {
    val cache = new ExplainCache()
    val eNodeCache = new NodeExplanationCache()
    explainENodes(left, right, cache, eNodeCache)
  }

  private def tryFindCommonAncestor(left: C, right: C): Option[C] = {
    var currentLeft = left
    var currentRight = right
    val seenLeft = new mutable.HashSet[C]()
    val seenRight = new mutable.HashSet[C]()
    while (true) {
      seenLeft.add(currentLeft)
      if (seenRight.contains(currentLeft)) {
        return Some(currentLeft)
      }

      seenRight.add(currentRight)
      if (seenLeft.contains(currentRight)) {
        return Some(currentRight)
      }

      val nextLeft = explainFind(currentLeft).next
      val nextRight = explainFind(currentRight).next

      if (nextLeft == currentLeft && nextRight == currentRight) {
        return None
      }
      currentLeft = nextLeft
      currentRight = nextRight
    }

    ???
  }

  private def commonAncestor(left: C, right: C): C = tryFindCommonAncestor(left, right).get

  private def getNodes(node: C, ancestor: C): Seq[ExplainNode[L, C]] = {
    if (node == ancestor) {
      Seq.empty
    } else {
      var currentNode = node
      val nodes = mutable.ArrayBuffer[ExplainNode[L, C]]()
      while (true) {
        val next = explainFind(currentNode).next
        nodes.append(explainFind(currentNode))
        if (next == ancestor) {
          return nodes
        }

        assert(next != currentNode)
        currentNode = next
      }

      ???
    }
  }

  private def explainENodes(left: C,
                            right: C,
                            cache: ExplainCache,
                            nodeExplanationCache: NodeExplanationCache): TreeExplanation[L] = {
    val proof = mutable.ArrayBuffer(nodeToExplanation(left, nodeExplanationCache))
    val ancestor = commonAncestor(left, right)
    val leftNodes = getNodes(left, ancestor)
    val rightNodes = getNodes(right, ancestor)

    for ((node, i) <- (leftNodes ++ rightNodes.reverse).zipWithIndex) {
      var direction = node.isRewriteForward
      var next = node.next
      var current = node.current
      if (i >= leftNodes.length) {
        direction = !direction
        val tmp = next
        next = current
        current = tmp
      }

      proof.append(explainAdjacent(current, next, direction, node.justification, cache, nodeExplanationCache))
    }

    TreeExplanation(proof)
  }

  private def explainAdjacent(current: C,
                              next: C,
                              ruleDirection: Boolean,
                              justification: Justification,
                              cache: ExplainCache,
                              nodeExplanationCache: NodeExplanationCache): TreeTerm[L] = {
    val fingerprint = (current, next)

    cache.get(fingerprint) match {
      case Some(answer) => answer
      case None =>

        val term = justification match {
          case Rule(name) =>
            val rewritten = nodeToExplanation(next, nodeExplanationCache)
            if (ruleDirection) {
              rewritten.withForwardRule(Some(name))
            } else {
              rewritten.withBackwardRule(Some(name))
            }

          case Congruence =>
            val currentNode = explainFind(current).node
            val nextNode = explainFind(next).node

            if (!currentNode.matches(nextNode)) {
              println(currentNode)
              println(nextNode)
            }

            assert(currentNode.matches(nextNode))

            val subProofs = mutable.ArrayBuffer[TreeExplanation[L]]()
            for ((leftChild, rightChild) <- currentNode.args.zip(nextNode.args)) {
              subProofs.append(
                explainENodes(leftChild.asInstanceOf[C], rightChild.asInstanceOf[C], cache, nodeExplanationCache))
            }

            TreeTerm(currentNode, None, None, subProofs)
        }

        cache(fingerprint) = term
        term
    }
  }

  private def explainENodeExistence(nodeId: C,
                                    restOfProof: TreeTerm[L],
                                    cache: ExplainCache,
                                    eNodeCache: NodeExplanationCache): TreeExplanation[L] = {
    val graphNode = explainFind(nodeId)
    graphNode.provenance match {
      case TentativeProvenance() | TentativelyExistsDueToRewrite(_) => ???

      case InsertedDirectly() =>
        TreeExplanation(Seq(nodeToExplanation(nodeId, eNodeCache), restOfProof))

      case ExistsDueToRewrite(existence) =>
        val existenceNode = explainFind(existence)
        val (direction, justification) = if (graphNode.next == existence) {
          (!graphNode.isRewriteForward, graphNode.justification)
        } else {
          (existenceNode.isRewriteForward, existenceNode.justification)
        }

        val explanation = explainENodeExistence(
          existence,
          explainAdjacent(existence, nodeId, direction, justification, cache, eNodeCache),
          cache,
          eNodeCache)
        TreeExplanation(explanation.steps :+ restOfProof)

      case ExistsDueToParent(existence) =>
        val existenceNode = explainFind(existence)
        val indexOfChild = existenceNode.node.args.indexOf(nodeId)
        var newRestOfProof = nodeToExplanation(existence, eNodeCache)

        newRestOfProof = newRestOfProof.withChildProofs(
          newRestOfProof.childProofs.updated(
            indexOfChild,
            TreeExplanation(newRestOfProof.childProofs(indexOfChild).steps :+ restOfProof)))

        explainENodeExistence(existence, newRestOfProof, cache, eNodeCache)
    }
  }

  private def nodeToExplanation(nodeId: C, cache: NodeExplanationCache): TreeTerm[L] = {
    cache.get(nodeId) match {
      case Some(existing) => existing
      case None =>
        val node = explainFind(nodeId).node
        val children = node.args.map(c => TreeExplanation(Seq(nodeToExplanation(c.asInstanceOf[C], cache))))
        val res = TreeTerm(node, None, None, children)
        cache(nodeId) = res
        res
    }
  }
}
