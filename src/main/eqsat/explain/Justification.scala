package eqsat.explain

trait Justification
final case class Rule(symbol: String) extends Justification
object Congruence extends Justification
