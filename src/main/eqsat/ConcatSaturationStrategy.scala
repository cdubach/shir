package eqsat

/**
 * A saturation strategy that applies two saturation strategies in sequence. The first saturation strategy is applied
 * until it saturates, and then the second saturation strategy is applied.
 */
final case class ConcatSaturationStrategy(first: SaturationStrategy, second: SaturationStrategy) extends SaturationStrategy {
  override def instantiate(): SaturationRunner = {
    val firstRunner = first.instantiate()
    val secondRunner = second.instantiate()
    new ConcatSaturationRunner(firstRunner, secondRunner)
  }
}

private final class ConcatSaturationRunner(val first: SaturationRunner, val second: SaturationRunner) extends SaturationRunner {
  private var firstIterationCount: Option[Int] = None

  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = {
    firstIterationCount match {
      case None =>
        val firstNotDone = first.runIteration(iteration, engine)
        if (!firstNotDone) {
          firstIterationCount = Some(iteration)
        }
        false
      case Some(firstIteration) =>
        second.runIteration(iteration - firstIteration, engine)
    }
  }
}
