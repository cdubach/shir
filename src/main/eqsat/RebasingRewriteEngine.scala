package eqsat

import core.Expr

/**
 * A rebasing rewrite engine is a rewrite engine that supports the rebase operation, which involves extracting an
 * expression from the e-graph and creating a new e-graph with the extracted expression as the root.
 * @tparam ENode The type of e-node in the e-graph.
 * @tparam EClass The type of e-class in the e-graph.
 */
trait RebasingRewriteEngine[ENode <: ENodeT, EClass <: EClassT] {
  /**
   * The e-graph that the rewrite engine is operating on.
   * @return The e-graph.
   */
  def graph: EGraph[ENode, EClass]

  /**
   * The root of the e-graph.
   * @return The root.
   */
  def root: EClassT

  /**
   * The current optimal expression that the rewrite engine has found.
   * @return The expression.
   */
  def expr: Expr

  /**
   * Rebase the rewrite engine. This involves extracting am expression from the e-graph and creating a new graph
   * with the extracted expression as the root.
   */
  def rebase(): Unit

  /**
   * Saturate the e-graph with a given saturation strategy until a termination criterion is met.
   * @param strategy The saturation strategy to use.
   */
  def saturate(strategy: SaturationStrategy): Unit
}

/**
 * A factory for creating rebasing rewrite engines.
 */
object RebasingRewriteEngine {
  def apply[ENode <: ENodeT, EClass <: EClassT](initialExpr: Expr,
                                                extractor: Extractor,
                                                createGraph: Expr => (EGraph[ENode, EClass], EClass),
                                                createGraphEngine: EGraph[ENode, EClass] => SaturationEngine[ENode, EClass] = (g: EGraph[ENode, EClass]) => g.defaultSaturationEngine): RebasingRewriteEngine[ENode, EClass] = {
    new RebasingRewriteEngineImpl(initialExpr, extractor, createGraph, createGraphEngine)
  }
}

/**
 * A rewrite engine that supports the rebase operation, which involves extracting an expression from the e-graph and
 * creating a new e-graph with the extracted expression as the root.
 * @param initialExpr The initial expression to start the rewrite engine with.
 * @param extractor An extractor that can extract an expression from an e-class.
 * @param createGraph A function that creates a new e-graph from an expression.
 * @param createGraphEngine A function that creates a saturation engine from an e-graph.
 * @tparam ENode The type of e-nodes in the e-graph.
 * @tparam EClass The type of e-classes in the e-graph.
 */
private class RebasingRewriteEngineImpl[ENode <: ENodeT, EClass <: EClassT](initialExpr: Expr,
                                                                            extractor: Extractor,
                                                                            createGraph: Expr => (EGraph[ENode, EClass], EClass),
                                                                            createGraphEngine: EGraph[ENode, EClass] => SaturationEngine[ENode, EClass])
  extends RebasingRewriteEngine[ENode, EClass] {

  private var (_graph, _root) = createGraph(initialExpr)

  /**
   * The e-graph that the rewrite engine is operating on.
   * @return The e-graph.
   */
  def graph: EGraph[ENode, EClass] = _graph

  /**
   * The root of the e-graph.
   * @return The root.
   */
  def root: EClassT = _root

  /**
   * The current optimal expression that the rewrite engine has found.
   * @return The expression.
   */
  def expr: Expr = extractor(_root).get

  /**
   * Rebase the rewrite engine. This involves extracting am expression from the e-graph and creating a new graph
   * with the extracted expression as the root.
   */
  def rebase(): Unit = {
    val newExpr = extractor(_root).get
    val (newGraph, newRoot) = createGraph(newExpr)
    _graph = newGraph
    _root = newRoot
  }

  /**
   * Saturate the e-graph with a given saturation strategy until a termination criterion is met.
   * @param strategy The saturation strategy to use.
   */
  def saturate(strategy: SaturationStrategy): Unit = {
    val engine = createGraphEngine(_graph)
    engine.saturate(strategy)
  }
}
