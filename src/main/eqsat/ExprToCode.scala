package eqsat

import core.{BuiltinExpr, CodeFormatter, Expr, Formatter, FunctionCall, LambdaT, ParamDef, ShirIR, Type, Value, VariableRenamer}

import scala.collection.mutable

object ExprToCode {
  private final case class SharedExpr(name: String, innerIR: Expr) extends BuiltinExpr {
    override def types: Seq[Type] = Seq()
    override def build(newInnerIR: Expr, newTypes: Seq[Type]): BuiltinExpr = SharedExpr(name, innerIR)

    override def tryFormat(formatter: Formatter): Option[String] = {
      formatter match {
        case _: CodeFormatter => Some(name)
        case _ => None
      }
    }
  }

  private object Shared {
    def apply(name: String, t: Type): SharedExpr = SharedExpr(name, Value(t))
    def unapply(expr: SharedExpr): Option[(String, Type)] = Some((expr.name, expr.t))
  }

  private def collectParamDefs(expr: ShirIR): Seq[ParamDef] = {
    val paramDefs = mutable.LinkedHashSet[ParamDef]()
    expr.visit {
      case p: ParamDef => paramDefs += p
      case _ =>
    }
    paramDefs.toSeq
  }

  private val maxExprSize = 20

  private def exprSize(expr: ShirIR): Int = {
    var size = 1
    expr.visit {
      case _: ParamDef => size += 1
      case _: BuiltinExpr => size += 1
      case _: FunctionCall => size += 1
      case _: LambdaT => size += 1
      case _ => // Do nothing
    }
    size
  }

  private def mustSplit(expr: ShirIR): Boolean = {
    val size = exprSize(expr)
    size > maxExprSize
  }

  private def tryGetArgs(expr: ShirIR): Option[Seq[Expr]] = {
    expr match {
      case e: BuiltinExpr => Some(e.args)
      case FunctionCall(f, arg, _) => Some(Seq(f, arg))
      case _ => None
    }
  }

  private def tryBuildFromArgs(expr: ShirIR, args: Seq[Expr]): Option[Expr] = {
    expr match {
      case e: BuiltinExpr => Some(e.buildFromArgs(args))
      case FunctionCall(_, _, _) => Some(FunctionCall(args(0), args(1)))
      case _ => None
    }
  }

  private def splitChildren(expr: ShirIR, baseIndex: Int): (ShirIR, Seq[(Expr, SharedExpr)]) = {
    expr match {
      case e: BuiltinExpr =>
        val sharedExprs = mutable.ArrayBuffer[(Expr, SharedExpr)]()
        val newArgs = e.args.map { arg =>
          val (newArg, newSharedExprs) = splitExpr(arg, baseIndex + sharedExprs.length)
          sharedExprs ++= newSharedExprs
          newArg.asInstanceOf[Expr]
        }
        val newExpr = e.buildFromArgs(newArgs)
        newExpr -> sharedExprs

      case e: Expr =>
        val sharedExprs = mutable.ArrayBuffer[(Expr, SharedExpr)]()
        val newChildren = e.children.map { child =>
          val (newChild, newSharedExprs) = splitExpr(child, baseIndex + sharedExprs.length)
          sharedExprs ++= newSharedExprs
          newChild
        }
        val newExpr = e.build(newChildren)
        newExpr -> sharedExprs
      case e => e -> Seq()
    }
  }

  private def splitExpr(expr: ShirIR, baseIndex: Int): (ShirIR, Seq[(Expr, SharedExpr)]) = {
    expr match {
      case expr: Expr if mustSplit(expr) =>
        val (newExpr, sharedExprs) = splitChildren(expr, baseIndex)

        tryGetArgs(newExpr) match {
          case Some(args) if mustSplit(newExpr) =>
            val sharedArgs = args.zipWithIndex.map { case (arg, index) => arg -> Shared(s"shared${baseIndex + sharedExprs.length + index}", arg.t) }
            tryBuildFromArgs(newExpr, sharedArgs.map(_._2)).get -> (sharedExprs ++ sharedArgs)

          case _ => newExpr -> sharedExprs
        }

      case _ => expr -> Seq()
    }
  }

  def apply(expr: ShirIR, renameVars: Boolean = true): String = {
    // Collect all ParamDefs
    val paramDefs = collectParamDefs(expr)

    // Collect shared expressions
    val (root, sharedExprs) = splitExpr(expr, 0)

    // Format root and shared expressions
    val formatter = CodeFormatter(new VariableRenamer(renameVars))
    val formattedRootExpr = formatter.format(root)

    val paramBindings = paramDefs.map(p => s"val p${formatter.renamer.name(p)} = ParamDef(${formatter.format(p.t)})")
    val exprBindings = sharedExprs.map { case (e, s) => s"val ${formatter.format(s)} = ${formatter.format(e)}" }

    val bindings = (paramBindings ++ exprBindings).mkString("\n")
    s"$bindings\n$formattedRootExpr"
  }
}
