package eqsat

import eqsat.explain._
import core.{BuiltinExpr, Counter, Expr, Formatter, TextFormatter, Type, TypeChecker, Value}

import scala.collection.mutable

/**
 * A partial e-graph implementation rooted in a hash cons data structure.
 */
trait HashConsEGraph extends EGraph[HashConsENode, HashConsEClass] {
  def analyses: Analyses

  /**
   * Tests if a particular analysis is enabled for this e-graph.
   * @param analysis A program analysis.
   * @tparam A The type of value produced by the analysis.
   * @return `true` if `analysis` is enabled for this e-graph; otherwise, `false`.
   */
  def hasAnalysis[A](analysis: Analysis[A]): Boolean = analyses.analyses.exists(_.identifier == analysis.identifier)

  /**
   * A disjoint set forest describing which e-class objects are equivalent (and hence logically the same e-class).
   */
  protected val unionFind = new DisjointSetForest[HashConsEClass]()

  /**
   * A mapping of canonical e-nodes to non-canonical e-classes. Each key is mapped to an e-class that was created by
   * the addition of a node congruent to the key. Values can be canonicalized by sending them through the `find`
   * function.
   */
  protected val hashCons = new mutable.HashMap[HashConsENode, HashConsEClass]()

  protected val explainerImpl: Option[Explainer[HashConsENode, HashConsEClass]]

  /**
   * Gets the set of all distinct e-classes defined in this graph.
   * @return A set of e-classes.
   */
  final def classes: Seq[HashConsEClass] = hashCons.values.toSeq.map(canonicalize).distinct.sorted

  final def explainer: Option[ExplanationService[HashConsENode]] = explainerImpl.map(
    impl => new ExplanationService[HashConsENode] {
      override def explainExistence(expr: Expr): TreeExplanation[HashConsENode] =
        impl.explainExistence(addImpl(expr, AlreadyThere)._1)

      override def explainEquivalence(left: Expr, right: Expr): TreeExplanation[HashConsENode] =
        impl.explainEquivalence(addImpl(left, AlreadyThere)._1, addImpl(right, AlreadyThere)._1)
    })

  final def canonicalize(eNode: HashConsENode): HashConsENode = {
    eNode mapArgs canonicalize
  }

  final override def merge(left: HashConsEClass, right: HashConsEClass, reason: NodeReason): Boolean = {
    val ruleName = reason match {
      case reason: AddedByRewriteT => reason.rule.name
      case _ => "unknown"
    }
    performMerge(left, right, Rule(ruleName))
  }

  /**
   * Merges two e-classes. This indicates that `left` and `right` produce the exact same value.
   *
   * @param left  The first e-class to merge.
   * @param right The second e-class to merge.
   * @param reason The justification for why `left` and `right` are merged.
   * @return `true` if merging the e-classes changed the graph; otherwise, `false`.
   */
  protected def performMerge(left: HashConsEClass, right: HashConsEClass, reason: Justification): Boolean

  final override def canonicalize(eClass: HashConsEClass): HashConsEClass = unionFind.find(eClass)

  final override def add(expr: Expr, reason: NodeReason): HashConsEClass =
    inferProvenance(addImpl(expr, reason), reason)

  final protected def add(node: HashConsENode, reason: NodeReason): HashConsEClass =
    inferProvenance(addImpl(node, reason), reason)

  private def inferProvenance(classAndNewness: (HashConsEClass, Boolean), reason: NodeReason): HashConsEClass = {
    (explainerImpl, reason, classAndNewness._2) match {
      case (Some(explainer), DirectlyAdded, true) =>
        explainer.setProvenance(classAndNewness._1, InsertedDirectly())

      case (Some(explainer), reason: AddedByRewriteT, true) =>
        explainer.setProvenance(
          classAndNewness._1,
          TentativelyExistsDueToRewrite(reason.original.asInstanceOf[HashConsEClass]))

      case _ =>
    }
    classAndNewness._1
  }

  private final def addImpl(expr: Expr, reason: NodeReason): (HashConsEClass, Boolean) = expr match {
    // If we are "adding" an e-class, we will canonicalize the e-class and return its canonicalized version.
    case e: HashConsEClass => (e, false)

    // If we are adding any other type of expression, then we will first recurse on its arguments and then add the expr
    // as an e-node.
    case _ =>
      val tvs = expr.children.collect({ case t: core.ArithTypeVar => t })
      val args = ExprHelpers.args(expr).map(addImpl(_, reason))
      val rewrittenNode = ExprHelpers.buildFromArgs(expr, args.map(_._1))
      val c = addImpl(HashConsENode(TypeChecker.check(rewrittenNode)), reason)
      explainerImpl match {
        case None =>
        case Some(explainer) =>
          for ((arg, newlyAdded) <- args) {
            // Link child nodes to the parent node in the explain-find data structure.
            (newlyAdded, explainer.getProvenance(arg)) match {
              case (true, _) => explainer.setProvenance(arg, ExistsDueToParent(c._1))
              case (false, TentativelyExistsDueToRewrite(_)) => explainer.setProvenance(arg, ExistsDueToParent(c._1))
              case (false, _) =>
            }
          }
      }
      c
  }

  /**
   * Adds an e-node to this PEG and returns the uncanonicalized e-class to which it belongs.
   * @param expr The node to add.
   * @param reason The reason why the node is being added.
   * @return An e-class containing `expr` and a Boolean that specifies whether the e-graph was changed.
   */
  private final def addImpl(expr: HashConsENode, reason: NodeReason): (HashConsEClass, Boolean) = {
    val canonicalNode = canonicalize(expr)
    hashCons.get(canonicalNode) match {
      case None =>
        assert(reason != AlreadyThere)

        val eClass = makeNewEClass(canonicalNode)
        hashCons.put(canonicalNode, eClass)

        // If we have an explainer set up, we want to return a non-canonical (and more specific!) version of the
        // canonical e-class we're creating. This is a little tricky due to node provenances in the explainer. If we
        // set the canonical class' provenance to a `TentativeProvenance`, then it will never receive a permanent
        // provenance---the non-canonical class will receive it instead because that is the class we'll return!
        //
        // To work around this, we'll flag the canonical node as derived from the non-canonical node instead of the
        // other way around.
        explainerImpl match {
          case None => (eClass, true)
          case Some(explainer) if canonicalNode.args == expr.args =>
            explainer.add(canonicalNode, eClass, TentativeProvenance())
            (eClass, true)

          case Some(explainer) =>
            // Create a representative for the non-canonical node.
            val newId = HashConsEClass(expr.t, this)
            explainer.add(canonicalNode, eClass, ExistsDueToRewrite(newId))
            explainer.add(expr, newId, TentativeProvenance())
            unionFind.union(eClass, newId)
            explainer.union(eClass, newId, Congruence)
            (newId, true)
        }

      case Some(existing) =>
        registerWithExplainer(expr, existing)
    }
  }

  private def registerWithExplainer(node: HashConsENode, eClass: HashConsEClass) = {
    explainerImpl match {
      case None => (eClass, false)
      case Some(explainer) =>
        explainer.nodeToClass(node) match {
          case Some(c) =>
            (c, false)

          case None =>
            // Create a new representative for this e-node.
            val newId = HashConsEClass(node.t, this)
            explainer.add(node, newId, ExistsDueToRewrite(eClass))
            unionFind.union(eClass, newId)
            explainer.union(eClass, newId, Congruence)
            (newId, true)
        }
    }
  }

  private final def makeNewEClass(eNode: HashConsENode): HashConsEClass = {
    // Create an e-class.
    val eClassId = HashConsEClass(eNode.t, this)

    // Add the node to the e-class.
    eClassId.nodeSet.add(eNode)

    // Make the node's arguments point to this e-class.
    for (child <- eNode.args) {
      child.parents.append((eNode, eClassId))
    }

    // Register the e-class with the union-find data structure.
    val eClass = unionFind.find(eClassId)

    // Set up the e-class' analysis results.
    for (analysis <- analyses.analyses) {
      val result = analysis.make(eNode)
      eClassId.setAnalysisResult(analysis, result)
      analysis.modify(this)(eClassId, result)
    }

    eClass
  }

  /**
   * Recomputes an analysis' results for the entire graph.
   * @param analysis The analysis to recompute.
   * @tparam A The type of the analysis' results.
   */
  final def recomputeAnalysis[A](analysis: Analysis[A]): Unit = {
    // Clear the analysis results.
    for (eClass <- classes) {
      eClass.clearAnalysisResult(analysis)
    }

    // Compute analyses until we reach a fixpoint.
    var worklist = mutable.ArrayBuffer[HashConsEClass]()
    worklist ++= classes
    while (worklist.nonEmpty) {
      val newWorklist = mutable.ArrayBuffer[HashConsEClass]()
      for (eClass <- classes) {
        val eligibleNodes = eClass.nodes.filter(_.args.forall(_.hasAnalysisResult(analysis)))
        val nodeResults = eligibleNodes.map(analysis.make)
        if (nodeResults.nonEmpty) {
          var classResult = nodeResults.reduce(analysis.join)
          val hasChanged = if (eClass.hasAnalysisResult(analysis)) {
            val oldResult = eClass.analysisResult(analysis)
            classResult = analysis.join(oldResult, classResult)
            classResult != oldResult
          } else {
            true
          }

          if (hasChanged) {
            eClass.setAnalysisResult(analysis, classResult)
            newWorklist ++= eClass.parents.map(_._2).map(canonicalize)
          }
        }
      }
      worklist = newWorklist
    }

    // Check that we computed analyses for all e-classes.
    for (eClass <- classes) {
      assert(eClass.hasAnalysisResult(analysis))
    }
  }

  final protected def assertConsistency(): Unit = {
    assertNoDanglingClasses()
    for (eClass <- classes) {
      // Check that the parents list is consistent.
      for ((parentNode, parentClass) <- eClass.parents) {
        assert(parentNode == canonicalize(parentNode))
        assert(canonicalize(hashCons(parentNode)) == canonicalize(parentClass))
        assert(parentNode.args.contains(eClass))
      }

      // Check that the nodes set is consistent.
      for (node <- eClass.nodeSet) {
        assert(canonicalize(hashCons(node)) == eClass)
        for (arg <- node.args) {
          assert(arg.parents.map(t => (t._1, canonicalize(t._2))).contains((node, eClass)))
        }
      }
    }

    explainerImpl.foreach(_.assertConsistency())
  }

  final protected def assertNoDanglingClasses(): Unit = {
    // Hashcons values may be stale (i.e., non-canonical) but the keys must be canonical at all times.
    for (eNode <- hashCons.keys) {
      for (arg <- eNode.args) {
        if (arg.isStale) {
          println(s"Stale node $eNode with canonical variant ${canonicalize(eNode)} (in class ${hashCons(eNode)})")
          println(s"$arg parents: ${arg.parents}")
          println(s"${canonicalize(arg)} parents: ${canonicalize(arg).parents}")
        }
        assert(!arg.isStale)
      }
    }
  }
}

/**
 * An e-Node: An expression that takes only e-class expressions and types as arguments.
 * @param expr The expression to wrap in an e-node.
 */
case class HashConsENode(expr: Expr) extends ENodeT {
  ExprHelpers.args(expr) foreach {
    case _: HashConsEClass =>
    case e: Expr => throw new IllegalArgumentException(
      s"e-nodes must take only e-classes, parameters, and types as arguments; cannot construct e-node with expr " +
        s"argument '$e'.")
    case _ =>
  }

  val args: Seq[HashConsEClass] = ExprHelpers.args(expr).map(_.asInstanceOf[HashConsEClass])

  def mapArgs(f: HashConsEClass => HashConsEClass): HashConsENode = {
    HashConsENode(ExprHelpers.buildFromArgs(
      expr,
      ExprHelpers.args(expr).map({
        case e: HashConsEClass => f(e)
        case other => other
      })))
  }
}

/**
 * The e-class implementation for hash cons-based e-graphs.
 */
class HashConsEClass private(val innerIR: Expr, val graph: HashConsEGraph) extends EClassT with Ordered[HashConsEClass] {
  val id: Long = Counter(HashConsEClass.getClass).next()

  override def hashCode(): Int = id.hashCode()

  override def equals(obj: Any): Boolean = obj match {
    case e: HashConsEClass => id == e.id
    case _ => false
  }

  override def compare(that: HashConsEClass): Int = id.compare(that.id)

  override def toString: String = s"HashConsEClass($id)"

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter => Some(formatter.makeAtomic(s"e-class $id: ${formatter.formatNonAtomic(t)}"))
      case _ => super.tryFormat(formatter)
    }
  }

  /**
   * The set of e-nodes that have this expression as a child, described as a mapping of canonical e-nodes to
   * non-canonical e-classes for those nodes.
   */
  var parents: mutable.Buffer[(HashConsENode, HashConsEClass)] =
    new mutable.ArrayBuffer[(HashConsENode, HashConsEClass)]()

  /**
   * The set of all e-nodes in this e-class.
   */
  var nodeSet = new mutable.HashSet[HashConsENode]()

  def isStale: Boolean = graph.canonicalize(this) != this

  /**
   * This e-class' analysis results. Each entry maps the unique identifier of an analysis to an analysis result.
   */
  private val analysisResults: mutable.HashMap[String, Any] = mutable.HashMap[String, Any]()

  /**
   * Gets the analysis result for a particular analysis.
   *
   * @param analysis The analysis to query for a result.
   * @tparam A The type of the analysis' result.
   * @return The analysis' result for this e-class.
   */
  override def analysisResult[A](analysis: Analysis[A]): A =
    graph.canonicalize(this).analysisResults(analysis.identifier).asInstanceOf[A]

  /**
   * Sets the result of an e-class analysis to a given value.
   * @param analysis The analysis whose result is to be set for this class.
   * @param result The analysis result to assign to this class.
   * @tparam A The type of `result`.
   */
  def setAnalysisResult[A](analysis: Analysis[A], result: A): Unit =
    graph.canonicalize(this).analysisResults(analysis.identifier) = result

  def clearAnalysisResult[A](analysis: Analysis[A]): Option[Any] =
    graph.canonicalize(this).analysisResults.remove(analysis.identifier)

  def hasAnalysisResult[A](analysis: Analysis[A]): Boolean =
    graph.canonicalize(this).analysisResults.contains(analysis.identifier)

  override def nodes: Seq[HashConsENode] = graph.canonicalize(this).nodeSet.toSeq

  override def types: Seq[Type] = Seq()

  override def build(newInnerIR: Expr, newTypes: Seq[Type]): BuiltinExpr = this
}

object HashConsEClass {
  def apply(t: Type, graph: HashConsEGraph): HashConsEClass = new HashConsEClass(Value(t), graph)
}
