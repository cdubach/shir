package eqsat

/**
 * A rebasing strategy is a strategy that can be applied to a rebasing rewrite engine. The rebasing strategy is used to
 * apply rewrite rules to the e-graph in the rebasing rewrite engine.
 * @tparam ENode The type of e-node in the e-graph.
 * @tparam EClass The type of e-class in the e-graph.
 */
trait RebasingStrategy[ENode <: ENodeT, EClass <: EClassT] {
  /**
   * Applies the rebasing strategy to a rebasing rewrite engine.
   * @param engine The rebasing rewrite engine to apply the strategy to.
   */
  def apply(engine: RebasingRewriteEngine[ENode, EClass]): Unit
}
