package eqsat

import core.Expr

import scala.concurrent.duration.Duration
import scala.util.control.ControlThrowable

/**
 * A rebasing strategy that applies another rebasing strategy for a limited amount of time. The strategy is applied
 * until the time limit is reached, at which point the strategy stops.
 * @param strategy The rebasing strategy to apply.
 * @param timeLimit The maximum amount of time to apply the rebasing strategy for.
 */
final case class TimeLimitedRebasingStrategy[ENode <: ENodeT, EClass <: EClassT](strategy: RebasingStrategy[ENode, EClass],
                                                                                 timeLimit: Duration) extends RebasingStrategy[ENode, EClass] {
  private object EngineOutOfTimeException extends ControlThrowable

  private case class InternalTimeLimitedSaturationStrategy(strategy: SaturationStrategy, timeLimit: Duration) extends SaturationStrategy {
    override def instantiate(): SaturationRunner = new SaturationRunner {
      private val startTime = System.nanoTime()
      private val runner = strategy.instantiate()
      override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = {
        engine.pushPreemptor(TimePreemptor(startTime, timeLimit))
        try {
          runner.runIteration(iteration, engine)
        } catch {
          case PreemptionException => throw EngineOutOfTimeException
        } finally {
          engine.popPreemptor()
        }
      }
    }
  }

  private class TimeLimitingEngine(engine: RebasingRewriteEngine[ENode, EClass]) extends RebasingRewriteEngine[ENode, EClass] {
    private val startTime = System.nanoTime()

    override def saturate(strategy: SaturationStrategy): Unit = {
      val timeRemaining = timeLimit.toNanos - (System.nanoTime() - startTime)
      engine.saturate(InternalTimeLimitedSaturationStrategy(strategy, Duration.fromNanos(timeRemaining)))
    }

    /**
     * The e-graph that the rewrite engine is operating on.
     *
     * @return The e-graph.
     */
    override def graph: EGraph[ENode, EClass] = engine.graph

    /**
     * The root of the e-graph.
     *
     * @return The root.
     */
    override def root: EClassT = engine.root

    /**
     * The current optimal expression that the rewrite engine has found.
     *
     * @return The expression.
     */
    override def expr: Expr = engine.expr

    /**
     * Rebase the rewrite engine. This involves extracting am expression from the e-graph and creating a new graph
     * with the extracted expression as the root.
     */
    override def rebase(): Unit = {
      val timeRemaining = timeLimit.toNanos - (System.nanoTime() - startTime)
      if (timeRemaining <= 0) {
        throw EngineOutOfTimeException
      }
      engine.rebase()
    }
}

  def apply(engine: RebasingRewriteEngine[ENode, EClass]): Unit = {
    val timeLimitedEngine = new TimeLimitingEngine(engine)
    try {
      strategy.apply(timeLimitedEngine)
    } catch {
      case EngineOutOfTimeException =>
    }
  }
}
