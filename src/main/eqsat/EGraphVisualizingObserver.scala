package eqsat

import cGen.{ArrayType, BinaryOp, Constant, DoubleType, IntType}
import core.{ArithType, Expr, ParamDef, ParamUse, Type}
import core.util.{DotConnection, DotGraph, DotNode, DotSubGraph}

/**
 * A dot graph printer for `ProgramEquivalenceGraph` objects.
 */
object EGraphVisualizingObserver extends EGraphObserver {
  /**
   * Notes that the graph is about to perform a step in the Equality Saturation algorithm.
   * @param graph The graph that is about to take a step
   */
  override def beforeSaturationStep[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = apply(graph).show()

  /**
   * Creates a dot graph that visualizes a `ProgramEquivalenceGraph`.
   * @param graph A `ProgramEquivalenceGraph` to process.
   * @return A dot graph that visualizes `graph`.
   */
  def apply[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): DotGraph = {
    def typeToString(t: Type): String = t match {
      case IntType(width, unsigned) if width.ae.isEvaluable && unsigned.ae.isEvaluable && unsigned.ae.evalLong == 0 =>
        s"int${width.ae.evalLong}"
      case IntType(width, unsigned) if width.ae.isEvaluable && unsigned.ae.isEvaluable && unsigned.ae.evalLong != 0 =>
        s"uint${width.ae.evalLong}"
      case DoubleType() => s"double"
      case ArithType(ae) if ae.isEvaluable => ae.evalLong.toString
      case ArrayType(element, length) => s"[${typeToString(element)} x ${typeToString(length)}]"
      case _ =>
        t.toShortString + (if (t.children.nonEmpty) "[" + t.children.map(typeToString).mkString(";") + "]" else "")
    }

    def eliminateSuffixes(name: String) = {
      val meaninglessSuffix = "Expr"
      if (name.endsWith(meaninglessSuffix))
        name.substring(0, name.length - meaninglessSuffix.length)
      else
        name
    }

    def exprToLabel(expr: Expr) = expr match {
      case p: DeBruijnParamUse => s"Param ${p.index} : ${typeToString(p.t)}"
      case ParamUse(ParamDef(t, _)) => s"Var : ${typeToString(t)}"
      case lambda: DeBruijnLambda => s"&lambda; ${typeToString(lambda.param.t)}"
      case forall: DeBruijnForall => s"&forall; ${typeToString(forall.param.t)}"
      case _: DeBruijnShiftExpr => "&uarr;"
      case Constant(value, t) => s"$value : ${typeToString(t)}"
      case BinaryOp(_, _, op, false, _) => op.toString
      case BinaryOp(_, _, op, true, _) => s"Boolean $op"
//      case cGenExpr: CGenExpr => eliminateSuffixes(cGenExpr.rightmostViewType.toString.head + expr.toShortString)
      case _ => eliminateSuffixes(expr.toShortString)
    }

    val exprNodes = graph
      .classes
      .toSeq
      .map(c => (c, c.nodes))
      .map(t => (t._1, t._2, t._2.map(x => DotNode(label = exprToLabel(x.expr), tooltip = x.t.toString))))

    val classNodes = exprNodes
      .map(t => (t._1, DotNode(
        label = "",
        style = "dotted",
        subGraph = Some(DotSubGraph(t._3)))))
      .toMap

    val connections = exprNodes.flatMap(t =>
      t._2.zip(t._3).flatMap(pair =>
        pair._1.args.map(arg => {
          if (arg == t._1) {
            DotConnection(
              pair._2.id,
              s"${pair._2.id}:n",
              "",
              "",
              s"cluster_${classNodes(t._1).id}",
              "")
          }
          else {
            DotConnection(
              pair._2,
              classNodes(arg.asInstanceOf[EClass]).subGraph.get.nodes.head,
              lhead = Some(classNodes(arg.asInstanceOf[EClass])))
          }
        })))

    DotGraph(
      DotSubGraph(classNodes.values.toSeq ++ connections),
      attributes = Map("compound" -> "true", "clusterrank" -> "local"))
  }
}