package eqsat

/**
 * A saturation strategy that applies another saturation strategy for a limited number of iterations. The strategy is
 * applied until the iteration limit is reached, at which point the strategy stops. The iteration limit is exclusive, so
 * the strategy will stop before the iteration limit is reached. If the iteration limit is 0, the strategy will not be
 * applied at all.
 * @param strategy The saturation strategy to apply.
 * @param iterationLimit The maximum number of iterations to apply the saturation strategy for.
 */
final case class IterationLimitedSaturationStrategy(strategy: SaturationStrategy, iterationLimit: Int) extends SaturationStrategy {
  override def instantiate(): SaturationRunner =
    new IterationLimitedSaturationRunner(strategy.instantiate(), iterationLimit)
}

private final class IterationLimitedSaturationRunner(strategy: SaturationRunner, iterationLimit: Int) extends SaturationRunner {
  private var iterationCount = 0

  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = {
    if (iterationCount < iterationLimit) {
      iterationCount += 1
      strategy.runIteration(iteration, engine)
    } else {
      false
    }
  }
}
