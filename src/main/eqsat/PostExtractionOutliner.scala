package eqsat
import cGen.CLambda
import core._

import scala.collection.mutable

/**
 * An extractor that outlines common subexpressions into function definitions that are called from extracted
 * expressions.
 * @param extractor A selecting extractor that is used to determine which e-nodes are extracted.
 */
case class PostExtractionOutliner(extractor: SelectingExtractor,
                                  isWorthOutlining: Expr => Boolean = !_.isInstanceOf[DeBruijnParamUse])
                                 (implicit buildLambda: (ParamDef, Expr) => LambdaT = (p, body) => CLambda(p, body),
                              buildFunctionCall: (Expr, Expr) => Expr = FunctionCall(_, _)) extends Extractor {
  private class Outliner(selection: PartialFunction[EClassT, ENodeT]) {
    private val choicesPerClass: mutable.Map[EClassT, ENodeT] = mutable.Map[EClassT, ENodeT]()
    private val extractionCounts: mutable.Map[EClassT, Int] = mutable.Map[EClassT, Int]()

    def inspect(eClass: EClassT): Unit = {
      extractionCounts.get(eClass) match {
        case Some(count) =>
          extractionCounts(eClass) = count + 1

        case None =>
          extractionCounts(eClass) = 1
          val node = selection(eClass)
          choicesPerClass(eClass) = node
          for (arg <- node.args) {
            inspect(arg)
          }
      }
    }

    def toExpr(eClass: EClassT): Expr = {
      OutliningExpressionBuilder(choicesPerClass, Function.unlift(extractionCounts.get(_).map(_ > 1))).toExpr(eClass)
    }
  }

  override def apply(eClass: EClassT): Option[Expr] = {
    // This extractor relies on a three-step process. First, it recursively chooses one node for each e-class in
    // a full expression tree. Then, common expressions in that tree are outlined into function definitions.
    // Finally, the extractor synthesizes an expression tree that includes calls to the outlined functions.
    val outliner = new Outliner(extractor.select(eClass))
    outliner.inspect(eClass)
    Some(outliner.toExpr(eClass))
  }
}
