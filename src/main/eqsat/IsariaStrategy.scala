package eqsat

import core.Expr

import scala.concurrent.duration.Duration

/**
 * A rebasing strategy based on Isaria. This strategy involves running a recurrent phase of saturation until the e-graph
 * stabilizes, and then running a final phase of saturation.
 *
 * @param recurrentPhase The saturation strategy to use for the recurrent phase.
 * @param finalPhase The saturation strategy to use for the final phase.
 * @param areEquivalent A function that determines if two expressions are equivalent.
 * @param rebase Whether to rebase the e-graph after each iteration of the recurrent phase.
 * @tparam ENode The type of e-node in the e-graph.
 * @tparam EClass The type of e-class in the e-graph.
 */
final case class IsariaStrategy[ENode <: ENodeT, EClass <: EClassT](recurrentPhase: SaturationStrategy,
                                                                    finalPhase: SaturationStrategy,
                                                                    recurrentPhaseTimeout: Option[Duration] = None,
                                                                    areEquivalent: (Expr, Expr) => Boolean =
                                                                      (left, right) => ExprToCode(left) == ExprToCode(right),
                                                                    rebase: Boolean = true)
  extends RebasingStrategy[ENode, EClass] {

  override def apply(engine: RebasingRewriteEngine[ENode, EClass]): Unit = {
    // Logic based on Figure 3 of the Isaria paper: Automatic Generation of Vectorizing Compilers for
    // Customizable Digital Signal Processors by Thomas and Bornholt.
    recurrentPhase(engine)
    engine.saturate(finalPhase)
  }

  private def recurrentPhase(engine: RebasingRewriteEngine[ENode, EClass]): Unit = {
    val start = System.nanoTime()
    while (true) {
      val oldExpr = engine.expr
      recurrentPhaseTimeout match {
        case None => engine.saturate(recurrentPhase)
        case Some(timeout) =>
          val end = System.nanoTime()
          val elapsed = Duration.fromNanos(end - start)
          if (elapsed >= timeout) {
            return
          }
          val remaining = timeout - elapsed
          engine.saturate(TimeLimitedSaturationStrategy(recurrentPhase, remaining))
      }

      val newExpr = engine.expr
      if (areEquivalent(oldExpr, newExpr)) {
        return
      }

      if (rebase) {
        engine.rebase()
      }
    }
  }
}
