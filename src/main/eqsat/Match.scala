package eqsat

import core.{ExprVar, ParamDef, Type, TypeChecker, TypeVarT}

/**
 * A match as found by a searcher. Can be consumed by an applier.
 *
 * @tparam EClass The type of an e-class in the graph.
 */
trait Match[+EClass <: EClassT] {
  /**
   * An inner match that this match wraps around.
   */
  def innerOrNone: Option[Match[EClass]] = None

  /**
    * The chain of inner matches that led to this match.
    * @return A sequence of matches.
    */
  def chain: Seq[Match[EClass]] = innerOrNone match {
    case Some(tail) => this +: tail.chain
    case None => Seq(this)
  }

  /**
   * The e-class that was matched in the e-graph.
   * @return An e-class.
   */
  def where: EClass

  /**
   * The set of all type variables that are mapped to concrete types by this match.
   */
  def typeVars: Seq[TypeVarT]

  /**
   * Takes an expression variable in the matched pattern and converts the variable to a concrete expression.
   * @param exprVar An expression variable.
   * @return The concrete expression to which `exprVar` was matched, if there is one; otherwise, `None`.
   */
  def tryInstantiate(exprVar: ExprVar): Option[EClass]

  /**
   * Takes a type variable in the matched pattern and converts the variable to a concrete type.
   * @param typeVar A type variable.
   * @return The concrete type to which `typeVar` was matched, if there is one; otherwise, `None`.
   */
  def tryInstantiate(typeVar: TypeVarT): Option[Type]

  /**
   * Takes a parameter definition in the matched pattern and converts it to another parameter definition.
   * @param param A parameter definition.
   * @return The parameter definition to which `param` was matched, if there is one; otherwise, `None`.
   */
  def tryInstantiate(param: ParamDef): Option[ParamDef]

  /**
   * Takes an expression variable in the matched pattern and converts the variable to a concrete expression.
   * @param exprVar An expression variable.
   * @return The concrete expression to which `exprVar` was matched.
   */
  final def instantiate(exprVar: ExprVar): EClass = tryInstantiate(exprVar).get

  /**
   * Takes a type variable in the matched pattern and converts the variable to a concrete type.
   * @param typeVar A type variable.
   * @return The concrete type to which `typeVar` was matched.
   */
  final def instantiate(typeVar: TypeVarT): Type = tryInstantiate(typeVar).get

  /**
   * Takes a parameter definition in the matched pattern and converts it to another parameter definition.
   * @param param A parameter definition.
   * @return The parameter definition to which `param` was matched.
   */
  final def instantiate(param: ParamDef): ParamDef = tryInstantiate(param).get

  /**
   * Tells if this match includes an expression variable.
   * @param exprVar An expression variable.
   * @return `true` if this match maps `exprVar` to an expression; otherwise, `false`.
   */
  def hasMappingFor(exprVar: ExprVar): Boolean = tryInstantiate(exprVar).isDefined

  /**
   * Tells if this match includes a type variable.
   * @param typeVar A type variable.
   * @return `true` if this match maps `typeVar` to a type; otherwise, `false`.
   */
  def hasMappingFor(typeVar: TypeVarT): Boolean = tryInstantiate(typeVar).isDefined

  /**
   * Tells if this match includes a parameter definition.
   * @param param A parameter definition.
   * @return `true` if this match maps `param` to another parameter definition; otherwise, `false`.
   */
  def hasMappingFor(param: ParamDef): Boolean = tryInstantiate(param).isDefined
}

/**
 * A match that consists of the merger of two other matches.
 * @param first A first, primary pattern match.
 * @param second A second pattern match.
 * @tparam EClass The type of an e-class in the graph.
 */
final case class MergedMatch[EClass <: EClassT](first: Match[EClass], second: Match[EClass]) extends Match[EClass] {
  /**
   * The e-class that was matched in the e-graph.
   *
   * @return An e-class.
   */
  override def where: EClass = first.where

  override def typeVars: Seq[TypeVarT] = first.typeVars ++ second.typeVars

  /**
   * Takes an expression variable in the matched pattern and converts the variable to a concrete expression.
   *
   * @param exprVar An expression variable.
   * @return The concrete expression to which `exprVar` was matched, if there is one; otherwise, `None`.
   */
  override def tryInstantiate(exprVar: ExprVar): Option[EClass] =
    first.tryInstantiate(exprVar).orElse(second.tryInstantiate(exprVar))

  /**
   * Takes a type variable in the matched pattern and converts the variable to a concrete type.
   *
   * @param typeVar A type variable.
   * @return The concrete type to which `typeVar` was matched, if there is one; otherwise, `None`.
   */
  override def tryInstantiate(typeVar: TypeVarT): Option[Type] =
    first.tryInstantiate(typeVar).orElse(second.tryInstantiate(typeVar))

  /**
   * Takes a parameter definition in the matched pattern and converts it to another parameter definition.
   *
   * @param param A parameter definition.
   * @return The parameter definition to which `param` was matched, if there is one; otherwise, `None`.
   */
  override def tryInstantiate(param: ParamDef): Option[ParamDef] =
    first.tryInstantiate(param).orElse(second.tryInstantiate(param))
}

object MergedMatch {
  /**
   * Tests if two matches have consistent types.
   * @param m1 A first match.
   * @param m2 A second match.
   * @tparam EClass The type of e-class in the graph.
   * @return `true` if the matches have consistent types; otherwise, `false`.
   */
  def haveConsistentTypes[EClass <: EClassT](m1: Match[EClass], m2: Match[EClass]): Boolean = {
    TypeChecker.solveConstraints({
      m1.typeVars.map(tv => tv -> m1.instantiate(tv)) ++
        m2.typeVars.map(tv => tv -> m2.instantiate(tv))
    }).nonEmpty
  }

}

/**
 * A pattern match that is augmented by a single type variable to type mapping.
 *
 * @param inner The inner pattern match.
 * @param key A type variable that is mapped to `value`.
 * @param value The value to which `key` is mapped.
 * @tparam EClass The type of an e-class in the graph.
 */
final case class TypeAugmentedMatch[EClass <: EClassT](inner: Match[EClass],
                                                       key: TypeVarT,
                                                       value: Type) extends Match[EClass] {
  /**
   * The e-class that was matched in the e-graph.
   *
   * @return An e-class.
   */
  override def where: EClass = inner.where

  override def innerOrNone: Option[Match[EClass]] = Some(inner)

  /**
   * Takes an expression variable in the matched pattern and converts the variable to a concrete expression.
   *
   * @param exprVar An expression variable.
   * @return The concrete expression to which `exprVar` was matched, if there is one; otherwise, `None`.
   */
  override def tryInstantiate(exprVar: ExprVar): Option[EClass] = inner.tryInstantiate(exprVar)

  /**
   * Takes a type variable in the matched pattern and converts the variable to a concrete type.
   *
   * @param typeVar A type variable.
   * @return The concrete type to which `typeVar` was matched, if there is one; otherwise, `None`.
   */
  override def tryInstantiate(typeVar: TypeVarT): Option[Type] =
    if (typeVar == key) Some(value) else inner.tryInstantiate(typeVar)

  /**
   * Takes a parameter definition in the matched pattern and converts it to another parameter definition.
   *
   * @param param A parameter definition.
   * @return The parameter definition to which `param` was matched, if there is one; otherwise, `None`.
   */
  override def tryInstantiate(param: ParamDef): Option[ParamDef] = inner.tryInstantiate(param)

  /**
   * The set of all type variables that are mapped to concrete types by this match.
   */
  override def typeVars: Seq[TypeVarT] = inner.typeVars :+ key
}
