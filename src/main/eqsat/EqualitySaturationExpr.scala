package eqsat

import core.BuiltinExpr

/**
  * A root type for expressions within the equality saturation package.
  */
trait EqualitySaturationExpr extends BuiltinExpr
