package eqsat

import core.LambdaT

/**
 * An analysis that computes the set of used parameter for each e-class.
 */
object ParameterUseAnalysis extends Analysis[Set[DeBruijnParamUse]] {
  /**
   * The analysis' unique identifier.
   */
  override val identifier: String = "parameter-use"

  /**
   * Constructs a new analysis result for a newly created singleton e-class.
   *
   * @param node The node in the singleton e-class.
   * @return An analysis result for the singleton e-class containing `node`.
   */
  override def make(node: ENodeT): Set[DeBruijnParamUse] = {
    val childResults = node.args.flatMap(_.analysisResult(this))
    node.expr match {
      case use: DeBruijnParamUse => Set(use)
      case _: LambdaT => childResults.collect({
        case use: DeBruijnParamUse if use.index > 0 => DeBruijnParamUse(use.index - 1, use.t)
      }).toSet
      case DeBruijnShift(_, _) => childResults.map(use => DeBruijnParamUse(use.index, use.t)).toSet
      case _ => childResults.toSet
    }
  }

  /**
   * When two e-classes are merged, join their analysis results into a new analysis result for the merged e-class.
   *
   * @param left  The analysis result of the first e-class being merged.
   * @param right The analysis result of the second e-class being merged.
   * @return A new analysis result for the merged e-class.
   */
  override def join(left: Set[DeBruijnParamUse], right: Set[DeBruijnParamUse]): Set[DeBruijnParamUse] = left ++ right

  /**
   * Optionally modify an e-class based on its analysis result. Calling `modify` on the same e-class more than once
   * must produce the same result as calling it only once.
   *
   * @param graph          The graph that defines `eClass`.
   * @param eClass         The e-class to potentially modify.
   * @param analysisResult This analysis' result for `eClass`.
   */
  override def modify[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                         (eClass: EClass,
                                                          analysisResult: Set[DeBruijnParamUse]): Unit = ()
}
