package eqsat

import scala.util.control.ControlThrowable

/**
 * An interface to the rewrite engine that applies rewrite rules to an e-graph. Rewrite engines can apply rewrite rules
 * in batch.
 */
trait RewriteEngine {
  /**
    * A read-only view of the e-graph that the rewrite engine operates on.
    * @return The e-graph.
    */
  def graph: ReadOnlyEGraph[ENodeT, EClassT]

  /**
    * Pushes a preemptor onto the stack of preemptors. Preemptors are used to check if the rewrite engine should shut
    * down. If any of the preemptors on the stack request a shutdown, the rewrite engine will throw a
    * `PreemptionException` during calls to `findMatches` and `applyRules`.
    * @param preemptor The preemptor to push.
    */
  def pushPreemptor(preemptor: EnginePreemptor): Unit

  /**
    * Pops a preemptor from the stack of preemptors.
    */
  def popPreemptor(): Unit

  /**
    * Finds all matches for a set of rewrite rules.
    * @param rules The rules to find matches for in the e-graph.
    * @return A sequence of pairs, where each pair consists of a rewrite rule and a sequence of matches for that rule.
    */
  def findMatches(rules: Seq[Rewrite]): Seq[(Rewrite, Seq[Match[EClassT]])]

  /**
    * Applies a set of matches to the e-graph. This method is used to apply the (filtered) results of a call to
    * `findMatches`.
    * @param matches The matches to apply.
    * @return The set of classes that were changed by applying the matches.
    */
  def applyMatches(matches: Seq[(Rewrite, Seq[Match[EClassT]])]): Set[EClassT]

  /**
   * Applies a set of rewrite rules in batch.
   * @param rules The rules to apply.
   * @return The set of classes that were changed by applying the rules.
   */
  final def applyRules(rules: Seq[Rewrite]): Set[EClassT] = applyMatches(findMatches(rules))
}

/**
 * An exception that is thrown when a rewrite engine is preempted.
 */
object PreemptionException extends ControlThrowable

/**
 * A saturation strategy: a way to apply rewrite rules to an e-graph until a termination criterion is met.
 * Saturation strategies can be simple (e.g., applying a fixed set of rules) or more complex (e.g., applying rules
 * based on the iteration number).
 */
trait SaturationStrategy {
  /**
   * Creates a saturation runner that can be used to run the saturation strategy.
   * @return A saturation runner.
   */
  def instantiate(): SaturationRunner
}

/**
 * A saturation runner is an instance of a saturation strategy. It is used to run a saturation strategy until a
 * termination criterion is met.
 */
trait SaturationRunner {
  /**
   * Runs a single equality saturation iteration.
   * @param iteration The index of the iteration being run.
   * @param engine A rewrite engine to use.
   * @return `true` if the saturation runner may have more work to do; otherwise, `false`.
   */
  def runIteration(iteration: Int, engine: RewriteEngine): Boolean
}

/**
 * A saturation strategy that applies a single set of rewrite rules on every iteration.
 * @param rules The rewrite rules to apply.
 */
final case class SimpleSaturationStrategy(rules: Seq[Rewrite]) extends SaturationStrategy with SaturationRunner {
  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = engine.applyRules(rules).nonEmpty

  override def instantiate(): SaturationRunner = this
}

/**
 * A saturation strategy that is computed for each iteration. State is not transferred between iterations.
 * @param strategy Computes the strategy rules to apply.
 */
final case class ComputedSaturationStrategy(strategy: Int => SaturationStrategy) extends SaturationStrategy with SaturationRunner {
  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean =
    strategy(iteration).instantiate().runIteration(iteration, engine)

  override def instantiate(): SaturationRunner = this
}

/**
 * A saturation strategy that, on every iteration, runs an expansion strategy once and runs a simplification strategy
 * until a fixpoint is reached.
 * @param expansionStrategy An expansion strategy to run once on every iteration.
 * @param simplificationStrategy A simplification strategy to run on every iteration until a fixpoint is reached.
 */
final case class BimodalSaturationStrategy(expansionStrategy: SaturationStrategy,
                                           simplificationStrategy: SaturationStrategy) extends SaturationStrategy {
  override def instantiate(): SaturationRunner =
    BimodalSaturationRunner(expansionStrategy.instantiate(), simplificationStrategy.instantiate())
}

/**
 * A saturation runner that, on every iteration, runs an expansion strategy once and runs a simplification strategy
 * until a fixpoint is reached.
 * @param expansionRunner An expansion strategy to run once on every iteration.
 * @param simplificationRunner A simplification strategy to run on every iteration until a fixpoint is reached.
 */
private final case class BimodalSaturationRunner(expansionRunner: SaturationRunner,
                                                 simplificationRunner: SaturationRunner) extends SaturationRunner {
  /**
   * Applies the simplification strategy until a fixpoint is reached.
   */
  private def simplify(iteration: Int, engine: RewriteEngine): Boolean = {
    var anyChanges = simplificationRunner.runIteration(iteration, engine)
    if (anyChanges) {
      while (anyChanges) {
        anyChanges = simplificationRunner.runIteration(iteration, engine)
      }
      true
    } else {
      false
    }
  }

  /**
   * Runs a single iteration of this saturation strategy.
   *
   * @param iteration The index of the iteration being run.
   * @param engine    A rewrite engine that can execute the strategy.
   * @return `true` if the saturation strategy may have more work to do; otherwise, `false`.
   */
  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = {
    if (iteration == 0) {
      // Simplify before expanding on the first iteration.
      simplify(iteration, engine)
    }
    expansionRunner.runIteration(iteration, engine) | simplify(iteration, engine)
  }
}
