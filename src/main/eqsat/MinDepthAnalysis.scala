package eqsat

/**
 * An analysis that computes the minimum depth of an extractable expression. This analysis is primarily intended
 * as a canary that flags buggy analysis computation implementations.
 */
object MinDepthAnalysis extends Analysis[Int] {
  /**
   * The analysis' unique identifier.
   */
  override val identifier: String = "min-depth"

  /**
   * Constructs a new analysis result for a newly created singleton e-class.
   *
   * @param node The node in the singleton e-class.
   * @return An analysis result for the singleton e-class containing `node`.
   */
  override def make(node: ENodeT): Int = (0 +: node.args.map(_.analysisResult(this))).max + 1

  /**
   * When two e-classes are merged, join their analysis results into a new analysis result for the merged e-class.
   *
   * @param left  The analysis result of the first e-class being merged.
   * @param right The analysis result of the second e-class being merged.
   * @return A new analysis result for the merged e-class.
   */
  override def join(left: Int, right: Int): Int = Math.min(left, right)

  /**
   * Optionally modify an e-class based on its analysis result. Calling `modify` on the same e-class more than once
   * must produce the same result as calling it only once.
   *
   * @param graph          The graph that defines `eClass`.
   * @param eClass         The e-class to potentially modify.
   * @param analysisResult This analysis' result for `eClass`.
   */
  override def modify[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                         (eClass: EClass, analysisResult: Int): Unit = ()
}
