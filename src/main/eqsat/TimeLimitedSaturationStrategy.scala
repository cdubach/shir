package eqsat

import scala.concurrent.duration.Duration

/**
 * A saturation strategy that applies another saturation strategy for a limited amount of time. The strategy is applied
 * until the time limit is reached, at which point the strategy stops. If the time limit is 0, the strategy will not be
 * applied at all.
 * @param strategy The saturation strategy to apply.
 * @param timeLimit The maximum amount of time to apply the saturation strategy for.
 */
final case class TimeLimitedSaturationStrategy(strategy: SaturationStrategy, timeLimit: Duration) extends SaturationStrategy {
  override def instantiate(): SaturationRunner =
    new TimeLimitedSaturationRunner(strategy.instantiate(), timeLimit)
}

/**
 * A saturation runner that applies another saturation runner for a limited amount of time. The runner is applied until
 * the time limit is reached, at which point the runner stops. If the time limit is 0, the runner will not be applied at
 * all.
 * @param strategy The saturation runner to apply.
 * @param timeLimit The maximum amount of time to apply the saturation runner for.
 */
private final class TimeLimitedSaturationRunner(strategy: SaturationRunner, timeLimit: Duration) extends SaturationRunner {
  private val startTime = System.nanoTime()

  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = {
    val currentTime = System.nanoTime()
    val elapsedTime = Duration.fromNanos(currentTime - startTime)
    if (elapsedTime >= timeLimit) {
      false
    } else {
      engine.pushPreemptor(TimePreemptor(startTime, timeLimit))
      try {
        val result = strategy.runIteration(iteration, engine)
        engine.popPreemptor()
        result
      } catch {
        case PreemptionException =>
          engine.popPreemptor()
          false
      }
    }
  }
}

/**
 * A preemptor that checks if a time limit has been reached.
 * @param startTime The time at which the saturation strategy started.
 * @param timeLimit The maximum amount of time to apply the saturation strategy for.
 */
final case class TimePreemptor(startTime: Long, timeLimit: Duration) extends EnginePreemptor {
  override def shutdownRequested: Boolean = {
    val currentTime = System.nanoTime()
    val elapsedTime = Duration.fromNanos(currentTime - startTime)
    elapsedTime >= timeLimit
  }
}
