package eqsat

import cGen.CLambda
import core.util.Log
import core.{Expr, ExprVar, FunctionCall, LambdaT, ParamDef, Type, TypeChecker, TypeVarT}

trait Hoist extends Searcher with Applier {
  implicit def buildLambda: (ParamDef, Expr) => LambdaT = (p, d) => CLambda(p, d)

  def isEligible: ENodeT => Boolean = Hoist.isNoFunctionCallOrLambda

  def argPattern: Searcher

  protected def argInAbstraction[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                    (rule: Rewrite,
                                                                     matchInfo: HoistMatch[EClass],
                                                                     stopwatch: HierarchicalStopwatch): Expr

  protected def argInApplication[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                                    (rule: Rewrite,
                                                                     matchInfo: HoistMatch[EClass],
                                                                     stopwatch: HierarchicalStopwatch): Expr

  /**
   * Tests if an e-class matches the pattern this searcher looks for; returns all resulting matches.
   *
   * @param eGraph    The e-graph that defines the e-class.
   * @param eClass    An e-class to search.
   * @param stopwatch A stopwatch that times the search.
   * @return All matches for this pattern and e-class combo.
   */
  override def searchEClass[ENode <: ENodeT, EClass <: EClassT](eGraph: ReadOnlyEGraph[ENode, EClass])
                                                               (eClass: EClass, stopwatch: HierarchicalStopwatch): Seq[Match[EClass]] = {

    eClass.nodes
      .flatMap({
        case node if isEligible(node) =>
          node.args
            .map(arg => argPattern.searchEClass(eGraph)(arg.asInstanceOf[EClass], stopwatch).headOption)
            .zipWithIndex
            .collect({ case (Some(m), i) => HoistMatch(eClass, node, i, m) })

        case _ => Nil
      })
  }

  /**
   * Rewrites an e-class based on an expression and type substitution map.
   *
   * @param eGraph    The graph that contains the e-class to rewrite.
   * @param rule      The rewrite rule that triggered this applier.
   * @param matchInfo The match to rewrite.
   * @param stopwatch A stopwatch that times the application.
   * @return A set of all e-classes that were _directly_ modified as a result of the application.
   */
  override def apply[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                        (rule: Rewrite,
                                                         matchInfo: Match[EClass],
                                                         stopwatch: HierarchicalStopwatch): Set[EClass] = {
    matchInfo.chain.collectFirst({ case m: HoistMatch[EClass] => m }) match {
      case Some(innerMatch@HoistMatch(where, eNode, i, _)) =>
        val newArg = TypeChecker.check(argInApplication(eGraph)(rule, innerMatch, stopwatch))
        val newArgs = eNode.args.map(DeBruijnShift(_)).updated(i, argInAbstraction(eGraph)(rule, innerMatch, stopwatch))
        val expr = FunctionCall(
          DeBruijnLambda(newArg.t, eNode.instantiate(newArgs)),
          newArg)

        val checkedExpr = stopwatch.child("Type checking hoisted expression", TypeChecker.check(expr))

        val original = rule.searcher.instantiateMatchIfUseful(eGraph, matchInfo)
        val reason = AddedByRewrite(rule, original, matchInfo)
        val newClass = stopwatch.child(
          "Adding hoisted expression to PEG",
          eGraph.add(checkedExpr, reason))

        if (newClass.t != original.t) {
          Log.error(
            this,
            s"Application of rule ${rule.name} results in ill-typed expression. Expected: ${ExprToText(original.t)}; got: ${ExprToText(newClass.t)}")
          assert(false)
        }

        if (eGraph.merge(original, newClass, reason)) {
          Set(where)
        } else {
          Set()
        }

      case None => ???
    }
  }

  case class HoistMatch[EClass <: EClassT](where: EClass,
                                           eNode: ENodeT,
                                           argIndex: Int,
                                           argMatch: Match[EClass]) extends Match[EClass] {

    override def innerOrNone: Option[Match[EClass]] = Some(argMatch)

    override def typeVars: Seq[TypeVarT] = argMatch.typeVars

    /**
     * Takes an expression variable in the matched pattern and converts the variable to a concrete expression.
     *
     * @param exprVar An expression variable.
     * @return The concrete expression to which `exprVar` was matched, if there is one; otherwise, `None`.
     */
    override def tryInstantiate(exprVar: ExprVar): Option[EClass] = argMatch.tryInstantiate(exprVar)

    /**
     * Takes a type variable in the matched pattern and converts the variable to a concrete type.
     *
     * @param typeVar A type variable.
     * @return The concrete type to which `typeVar` was matched, if there is one; otherwise, `None`.
     */
    override def tryInstantiate(typeVar: TypeVarT): Option[Type] = argMatch.tryInstantiate(typeVar)

    /**
     * Takes a parameter definition in the matched pattern and converts it to another parameter definition.
     *
     * @param param A parameter definition.
     * @return The parameter definition to which `param` was matched, if there is one; otherwise, `None`.
     */
    override def tryInstantiate(param: ParamDef): Option[ParamDef] = argMatch.tryInstantiate(param)
  }
}

object Hoist {
  def isNoFunctionCallOrLambda(node: ENodeT): Boolean = {
    val e = node.expr
    !e.isInstanceOf[FunctionCall] && !e.isInstanceOf[LambdaT] && !e.isInstanceOf[DeBruijnShiftT]
  }
}