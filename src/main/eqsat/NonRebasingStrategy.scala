package eqsat

/**
 * A nominally rebasing saturation strategy that does not rebase the e-graph. Rather, it applies a non-rebasing
 * saturation strategy.
 * @param strategy The saturation strategy to apply.
 */
final case class NonRebasingStrategy[ENode <: ENodeT, EClass <: EClassT](strategy: SaturationStrategy) extends RebasingStrategy[ENode, EClass] {
  /**
   * Applies the non-rebasing strategy to a rebasing rewrite engine.
   * @param engine The rebasing rewrite engine to apply the strategy to.
   */
  def apply(engine: RebasingRewriteEngine[ENode, EClass]): Unit = {
    engine.saturate(strategy)
  }
}
