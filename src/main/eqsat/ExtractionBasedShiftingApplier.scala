package eqsat

import core.{Expr, ExprVar, ParamDef, Type, TypeVarT}

import scala.collection.mutable

/**
 * An applier that shifts a match's expression variables before sending the results to another applier.
 * @param applier An inner applier that takes as argument an updated match with shifted expression variables.
 * @param extractor An extractor for extraction-based shifting.
 * @param shiftAmounts A mapping of expression variables to the amount by which they ought to be shifted.
 */
case class ExtractionBasedShiftingApplier(applier: Applier,
                                          extractor: Extractor,
                                          shiftAmounts: Seq[(ExprVar, Int)]) extends Applier {
  override def tryReverseIntoSearcher: Option[(Searcher, Applier => Applier)] = {
    applier.tryReverseIntoSearcher match {
      case Some((searcher, update)) => Some((
        searcher,
        app => ExtractionBasedShiftingApplier(update(app), extractor, shiftAmounts.map(t => (t._1, -t._2)))))
      case None => None
    }
  }

  /**
   * Rewrites an e-class based on an expression and type substitution map.
   *
   * @param eGraph    The graph that contains the e-class to rewrite.
   * @param rule      The rewrite rule that triggered this applier.
   * @param matchInfo The match to rewrite.
   * @param stopwatch A stopwatch that times the application.
   * @return A set of all e-classes that were _directly_ modified as a result of the application.
   */
  def apply[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                               (rule: Rewrite,
                                                matchInfo: Match[EClass],
                                                stopwatch: HierarchicalStopwatch): Set[EClass] = {

    // Try to shift every expression variable.
    val shiftedExprs = mutable.Map[Long, Expr]()
    for ((exprVar, shiftAmount) <- shiftAmounts) {
      val shifted = matchInfo.tryInstantiate(exprVar)
        .flatMap(extractor.apply)
        .map(DeBruijnShift.elideRec(_).asInstanceOf[Expr])
        .flatMap(DeBruijnParamUse.tryIncrementUnbound(_, shiftAmount))
      shifted match {
        case Some(result) => shiftedExprs.put(exprVar.id, result)

        // If an expression variable cannot be shifted, abort.
        case None => return Set()
      }
    }

    val original = rule.searcher.instantiateMatchIfUseful(eGraph, matchInfo)
    val shiftedClasses = shiftedExprs.mapValues(eGraph.add(_, AddedByRewrite(rule, original, matchInfo))).view.force.toMap
    val newMatch = ShiftedMatch(matchInfo, shiftedClasses)
    applier(eGraph)(rule, newMatch, stopwatch)
  }

  private case class ShiftedMatch[+EClass <: EClassT](inner: Match[EClass], shiftedExprs: Map[Long, EClass]) extends Match[EClass] {
    override def innerOrNone: Option[Match[EClass]] = Some(inner)

    /**
     * The e-class that was matched in the e-graph.
     *
     * @return An e-class.
     */
    override def where: EClass = inner.where

    override def typeVars: Seq[TypeVarT] = inner.typeVars

    /**
     * Takes an expression variable in the matched pattern and converts the variable to a concrete expression.
     *
     * @param exprVar An expression variable.
     * @return The concrete expression to which `exprVar` was matched, if there is one; otherwise, `None`.
     */
    override def tryInstantiate(exprVar: ExprVar): Option[EClass] = {
      shiftedExprs.get(exprVar.id) match {
        case Some(result) => Some(result)
        case None => inner.tryInstantiate(exprVar)
      }
    }

    /**
     * Takes a type variable in the matched pattern and converts the variable to a concrete type.
     *
     * @param typeVar A type variable.
     * @return The concrete type to which `typeVar` was matched, if there is one; otherwise, `None`.
     */
    override def tryInstantiate(typeVar: TypeVarT): Option[Type] = inner.tryInstantiate(typeVar)

    /**
     * Takes a parameter definition in the matched pattern and converts it to another parameter definition.
     *
     * @param param A parameter definition.
     * @return The parameter definition to which `param` was matched, if there is one; otherwise, `None`.
     */
    override def tryInstantiate(param: ParamDef): Option[ParamDef] = inner.tryInstantiate(param)
  }
}
