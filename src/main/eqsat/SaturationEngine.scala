package eqsat

import scala.collection.mutable

/**
 * An engine that applies the equality saturation algorithm to an e-graph.
 * @param graph The e-graph to operate on.
 * @param observer An observer that tracks actions performed by the saturation engine.
 * @param useBatchSearch Makes the saturation engine search the e-graph in batch if set to `true`. Makes the engine
 *                       search every e-class individually if set to `false`.
 * @param applyMatchesOnce Makes the saturation engine apply matches only once if set to `true`.
 * @param shouldPrintProgress Makes the saturation engine print progress messages to standard output if set to `true`.
 * @param rebuildAfterEveryApplication Makes the saturation engine rebuild the e-graph after every rule application if
 *                                     set to `true`.
 * @tparam ENode The type of e-node to operate on.
 * @tparam EClass The type of e-class to operate on.
 */
case class SaturationEngine[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass],
                                                                observer: EGraphObserver = EGraphNullObserver,
                                                                useBatchSearch: Boolean = true,
                                                                applyMatchesOnce: Boolean = true,
                                                                shouldPrintProgress: Boolean = false,
                                                                rebuildAfterEveryApplication: Boolean = false) {

  private def mutableGraph = graph

  def withGraph[ENode2 <: ENodeT, EClass2 <: EClassT](newGraph: EGraph[ENode2, EClass2]): SaturationEngine[ENode2, EClass2] =
    SaturationEngine(newGraph, observer, useBatchSearch, applyMatchesOnce, shouldPrintProgress, rebuildAfterEveryApplication)

  def withMatchesAppliedOnce: SaturationEngine[ENode, EClass] =
    SaturationEngine(graph, observer, useBatchSearch, applyMatchesOnce = true,
                     shouldPrintProgress = shouldPrintProgress,
                     rebuildAfterEveryApplication = rebuildAfterEveryApplication)

  def withMatchesAppliedIndefinitely: SaturationEngine[ENode, EClass] =
    SaturationEngine(graph, observer, useBatchSearch, applyMatchesOnce = false,
      shouldPrintProgress = shouldPrintProgress,
      rebuildAfterEveryApplication = rebuildAfterEveryApplication)

  /**
   * Applies rewrite rules until a fixpoint is reached or until a termination criterion is triggered.
   * @param rewrites The set of rewrite rules to apply.
   * @return The number of iterations performed by the saturation engine.
   */
  def saturate(rewrites: Seq[Rewrite]): Int = {
    saturate(SimpleSaturationStrategy(rewrites))
  }

  /**
   * Applies rewrite rules until a fixpoint is reached or until a termination criterion is triggered.
   * @param rewrites The set of rewrite rules to apply.
   * @param iterationLimit The maximum number of iterations to perform.
   * @return The number of iterations performed by the saturation engine.
   */
  def saturate(rewrites: Seq[Rewrite], iterationLimit: Int): Int = {
    saturate(IterationLimitedSaturationStrategy(SimpleSaturationStrategy(rewrites), iterationLimit))
  }

  /**
   * Applies rewrite rules until a fixpoint is reached or until a termination criterion is triggered.
   * @param rewrites The set of rewrite rules that apply in a given iteration.
   * @return The number of iterations performed by the saturation engine.
   */
  def saturate(rewrites: Int => Seq[Rewrite]): Int = {
    saturate(ComputedSaturationStrategy(i => SimpleSaturationStrategy(rewrites(i))))
  }

  /**
   * Applies rewrite rules until a fixpoint is reached.
   * @param strategy A saturation strategy.
   * @return The number of iterations performed by the saturation engine.
   */
  def saturate(strategy: SaturationStrategy): Int = {
    var iteration = 0
    val engine = new RewriteEngineImpl()
    val runner = strategy.instantiate()
    while (true) {
      observer.beforeSaturationStep(graph)

      if (shouldPrintProgress) {
        println(s"Running saturation iteration $iteration")
      }

      engine.appliedMatchesBefore = engine.appliedMatchCount
      val more = runner.runIteration(iteration, engine)

      observer.afterSaturationStep(
        graph, fullIteration = true, appliedMatchCount = engine.appliedMatchCount - engine.appliedMatchesBefore)

      if (!more) {
        observer.onFinish(graph)
        return iteration
      }
      iteration += 1
    }
    iteration
  }

  private final class RewriteEngineImpl extends RewriteEngine {
    private val appliedMatches = mutable.Map[Rewrite, mutable.HashSet[Match[EClassT]]]()
    private val preemptorStack = mutable.Stack[EnginePreemptor]()

    /**
     * The number of matches that have been applied so far by the rewrite engine.
     */
    def appliedMatchCount: Int = appliedMatches.values.map(_.size).sum

    /**
     * The number of matches that were applied by the rewrite engine before the current iteration.
     */
    var appliedMatchesBefore: Int = 0

    private def shutdownRequested: Boolean = preemptorStack.exists(_.shutdownRequested)

    private case class RebuildingApplier(innerApplier: Applier) extends Applier {
      /**
       * Rewrites an e-class based on an expression and type substitution map.
       * @param eGraph The graph that contains the e-class to rewrite.
       * @param rule The rewrite rule that triggered this applier.
       * @param matchInfo The match to rewrite.
       * @param stopwatch A stopwatch that times the application.
       * @return A set of all e-classes that were _directly_ modified as a result of the application.
       */
      def apply[ENode2 <: ENodeT, EClass2 <: EClassT](eGraph: EGraph[ENode2, EClass2])
                                                     (rule: Rewrite,
                                                      matchInfo: Match[EClass2],
                                                      stopwatch: HierarchicalStopwatch): Set[EClass2] = {
        val result = innerApplier(eGraph)(rule, matchInfo, stopwatch)
        mutableGraph.rebuild()
        result
      }
    }

    private case class PreemptingApplier(innerApplier: Applier) extends Applier {
      /**
       * Rewrites an e-class based on an expression and type substitution map.
       * @param eGraph The graph that contains the e-class to rewrite.
       * @param rule The rewrite rule that triggered this applier.
       * @param matchInfo The match to rewrite.
       * @param stopwatch A stopwatch that times the application.
       * @return A set of all e-classes that were _directly_ modified as a result of the application.
       */
      def apply[ENode2 <: ENodeT, EClass2 <: EClassT](eGraph: EGraph[ENode2, EClass2])
                                                     (rule: Rewrite,
                                                      matchInfo: Match[EClass2],
                                                      stopwatch: HierarchicalStopwatch): Set[EClass2] = {
        if (shutdownRequested) {
          // Rebuild graph to make sure we leave it in a consistent state.
          mutableGraph.rebuild()

          // Abort the saturation process.
          shutdown(eGraph)
        }
        innerApplier(eGraph)(rule, matchInfo, stopwatch)
      }
    }

    private def shutdown[ENode2 <: ENodeT, EClass2 <: EClassT](eGraph: EGraph[ENode2, EClass2]): Unit = {
      // Notify the observer that the saturation engine is aborting.
      observer.afterSaturationStep(
        eGraph, fullIteration = false, appliedMatchCount = appliedMatchCount - appliedMatchesBefore)
      observer.onAbort(eGraph)

      // If a shutdown was requested, throw a PreemptionException.
      throw PreemptionException
    }

    override def findMatches(rules: Seq[Rewrite]): Seq[(Rewrite, Seq[Match[EClassT]])] = {
      // First, search the e-graph for matches for each rule.
      val matches = rules.map(rw => {
        val matches = ObservedRuleSearcher(rw, observer, useBatchSearch).search(graph)
        if (shutdownRequested) {
          shutdown(mutableGraph)
        }
        (rw, matches)
      })

      // Then, filter out matches that have already been applied.
      matches.map(pair => {
        val (rw, matchList) = pair
        val applied = appliedMatches.getOrElse(rw, mutable.HashSet[Match[EClassT]]())
        val newMatches = if (applyMatchesOnce) matchList.filterNot(applied.contains) else matchList
        (rw, newMatches)
      })
    }

    override def applyMatches(matches: Seq[(Rewrite, Seq[Match[EClassT]])]): Set[EClassT] = {
      // Invariants may be temporarily broken in a write phase, but that doesn't matter because we won't be matching
      // during this phase.
      val changedClasses = matches
        .map(pair => {
          val (rw, matchList) = pair
          val applied = appliedMatches.getOrElseUpdate(rw, mutable.HashSet[Match[EClassT]]())
          var applier: Applier = ObservedRuleApplier(observer)
          if (rebuildAfterEveryApplication) {
            applier = RebuildingApplier(applier)
          }
          applier = PreemptingApplier(applier)
          val changed = applier.applyMany(mutableGraph)(rw, matchList.map(_.asInstanceOf[Match[EClass]]))
          matchList.forall(applied.add)
          changed
        })
        .foldLeft(Set[EClassT]())(_ ++ _)

      // Rebuild the graph's invariants.
      mutableGraph.rebuild()

      // Return the set of modified classes.
      changedClasses
    }

    override def graph: ReadOnlyEGraph[ENodeT, EClassT] = TypeErasedReadOnlyEGraph(mutableGraph)

    /**
     * Pushes a preemptor onto the stack of preemptors. Preemptors are used to check if the rewrite engine should shut
     * down. If any of the preemptors on the stack request a shutdown, the rewrite engine will no longer perform work
     * during calls to `findMatches` and `applyRules`.
     *
     * @param preemptor The preemptor to push.
     */
    override def pushPreemptor(preemptor: EnginePreemptor): Unit = preemptorStack.push(preemptor)

    /**
     * Pops a preemptor from the stack of preemptors.
     */
    override def popPreemptor(): Unit = preemptorStack.pop()
  }
}
