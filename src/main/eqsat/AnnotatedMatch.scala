package eqsat

import core.{ExprVar, ParamDef, Type, TypeVarT}

/**
 * A match that has been annotated with an arbitrary piece of information.
 * @param inner The inner match that is annotated.
 * @param annotation The annotation that is attached to `inner`.
 * @tparam EClass The type of an e-class in the graph.
 * @tparam Annotation The type of the annotation data.
 */
final case class AnnotatedMatch[EClass <: EClassT, Annotation](inner: Match[EClass],
                                                               annotation: Annotation) extends Match[EClass] {
  override def innerOrNone: Option[Match[EClass]] = Some(inner)

  override def typeVars: Seq[TypeVarT] = inner.typeVars
  override def where: EClass = inner.where
  override def tryInstantiate(exprVar: ExprVar): Option[EClass] = inner.tryInstantiate(exprVar)
  override def tryInstantiate(typeVar: TypeVarT): Option[Type] = inner.tryInstantiate(typeVar)
  override def tryInstantiate(param: ParamDef): Option[ParamDef] = inner.tryInstantiate(param)
}
