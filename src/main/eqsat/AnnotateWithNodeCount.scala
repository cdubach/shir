package eqsat

/**
 * A searcher that annotates each match with the number of nodes in the e-class where the match applies.
 * @param searcher An inner searcher that produces matches to annotate.
 */
final case class AnnotateWithNodeCount(searcher: Searcher) extends Searcher {
  /**
   * Tests if an e-class matches the pattern this searcher looks for; returns all resulting matches.
   *
   * @param eGraph    The e-graph that defines the e-class.
   * @param eClass    An e-class to search.
   * @param stopwatch A stopwatch that times the search.
   * @return All matches for this pattern and e-class combo.
   */
  override def searchEClass[ENode <: ENodeT, EClass <: EClassT](eGraph: ReadOnlyEGraph[ENode, EClass])
                                                               (eClass: EClass,
                                                                stopwatch: HierarchicalStopwatch): Seq[Match[EClass]] = {
    searcher.searchEClass(eGraph)(eClass, stopwatch).map(m => AnnotatedMatch(m, m.where.nodes.length))
  }

  /**
   * Finds all e-classes in an e-graph that match the pattern this searcher looks for.
   * @param eGraph An e-graph to search.
   * @param createStopwatch Creates a stopwatch to measure a search.
   * @return A sequence of matches.
   */
  override def search[ENode <: ENodeT, EClass <: EClassT](eGraph: ReadOnlyEGraph[ENode, EClass],
                                                          createStopwatch: () => HierarchicalStopwatch): Seq[Match[EClass]] = {

    searcher.search(eGraph, createStopwatch).map(m => AnnotatedMatch(m, m.where.nodes.length))
  }
}
