package eqsat

import cGen.CLambda
import core.{Expr, FunctionCall, LambdaT, ParamDef}
import eqsat.solver.{AgglomeratedClass, AgglomeratedNode, IntConstant}

import scala.collection.mutable

/**
 * Optimization settings for the dynamic programming outlining extractor.
 * @param enableAgglomeration Determines whether e-class agglomeration is enabled. Default is `true`.
 */
final case class DPOutliningExtractorOptimizations(enableAgglomeration: Boolean = true)

/**
 * A dynamic programming-based outlining extractor that uses a cost model to determine the cost of outlining an
 * expression. This extractor uses a cost model to determine the cost of outlining an expression and uses a dynamic
 * programming algorithm to find the optimal outlining strategy. The extractor can be configured with a cost model, a
 * predicate that determines whether an e-node is eligible for outlining, and a set of optimizations.
 *
 * @param costModel The cost model that determines the cost of outlining an expression.
 * @param isEligible A predicate that determines whether an e-node is eligible for outlining. Default is all e-nodes.
 * @param optimizations The optimization settings for the extractor. Default is all optimizations enabled.
 * @param buildLambda A function that constructs a new lambda.
 * @param buildFunctionCall A function that constructs a function call.
 */
final case class DPOutliningExtractor(costModel: OutliningExtractorCostModel,
                                      isEligible: ENodeT => Boolean = _ => true,
                                      optimizations: DPOutliningExtractorOptimizations = DPOutliningExtractorOptimizations())
                                     (implicit override val buildLambda: (ParamDef, Expr) => LambdaT = (p, body) => CLambda(p, body),
                                      override val buildFunctionCall: (Expr, Expr) => Expr = FunctionCall(_, _)) extends OutliningExtractor {

  /**
   * Determines whether e-class agglomeration is enabled.
   *
   * @return `true` if e-class agglomeration is enabled; otherwise, `false`.
   */
  override protected def enableAgglomeration: Boolean = optimizations.enableAgglomeration

  /**
   * Formulates and solves the extraction/outlining optimization problem for a given agglomerated class.
   *
   * @param root The root agglomerated class.
   * @return The solution to the optimization problem.
   */
  override def solve(root: AgglomeratedClass): Option[AgglomeratedSolution] = {
    val classes = root.reachable
    val analysisResults = computeAnalysis(classes)
    val rootResults = analysisResults(root)
    val allSelections = rootResults.fronts.values.flatMap(_.elements)

    // Compute the total cost of each selection and filter out selections that exceed the budget.
    val selectionsAndCosts = allSelections
      .map { selection => (selection, selection.totalCost) }
      .filter {
        case (_, cost) => cost.zipWithIndex.forall {
          case (c, i) => costModel.budget(i) match {
            case Some(budget) => c <= budget.evalInt
            case None => true
          }
        }
      }

    if (selectionsAndCosts.isEmpty) {
      return None
    }

    // Find the selection with the minimum cost.
    val bestSelection = selectionsAndCosts.minBy { pair =>
      val totalCost = pair._2
      costModel.minimizationTarget(totalCost.map(IntConstant)).evalInt
    }

    Some(AgglomeratedSolution({
      case c if bestSelection._1.selection.contains(c) => bestSelection._1.selection(c)
    }, {
      case c if bestSelection._1.outlinedFunctionCosts.contains(c) => true
      case _ => false
    }, bestSelection._2))
  }

  private def cartesianProduct[T](elements: List[Seq[T]]): Seq[Seq[T]] = {
    elements match {
      case Nil => Seq(Seq.empty)
      case head :: tail =>
        for {
          elem <- head
          rest <- cartesianProduct(tail)
        } yield elem +: rest
    }
  }

  private def computeAnalysis(classes: Seq[AgglomeratedClass]): Map[AgglomeratedClass, AnalysisData] = {
    // First, determine which classes are eligible for outlining.
    val inDegrees = countInDegrees(classes)
    val outlineableClasses = classes.filter(inDegrees.getOrElse(_, 0) > 1)

    // Next, build a mapping of e-classes to their parents.
    val parents = classes.flatMap { c =>
      c.nodes.flatMap { n =>
        n.args.map { a =>
          (c, a)
        }
      }
    }.distinct.groupBy(_._2).mapValues(_.map(_._1))

    val analysisResults = mutable.Map[AgglomeratedClass, AnalysisData]()
    def make(node: AgglomeratedNode, c: AgglomeratedClass): AnalysisData = {
      if (node.args.isEmpty) {
        val useCostVector = costModel.dimensionIndices.map(dim => node.useCost(costModel, dim, Seq.empty)).map(_.evalInt)
        if (outlineableClasses.contains(c)) {
          val callCostVector = costModel.dimensionIndices.map(dim => node.callCost(costModel, dim, Seq.empty)).map(_.evalInt)
          return AnalysisData(
            Map(OutliningDecisions(Map(c -> false)) -> ParetoFront(Seq(NodeSelectionsAndCost(Map(c -> node), useCostVector, Map.empty))),
                OutliningDecisions(Map(c -> true)) -> ParetoFront(Seq(NodeSelectionsAndCost(Map(c -> node), callCostVector, Map(c -> useCostVector))))))
        } else {
          return AnalysisData(
            Map(OutliningDecisions(Map(c -> false)) -> ParetoFront(Seq(NodeSelectionsAndCost(Map(c -> node), useCostVector, Map.empty)))))
        }
      }

      val argData = node.args.map(analysisResults(_))

      // Harmonize the considered classes of the arguments.
      val consideredClasses = argData.flatMap(_.consideredClasses).toSet
      val harmonizedArgData = argData.map(_.considerClasses(consideredClasses))

      // Now, for each decision set, compute the Pareto front. We do so by computing the cartesian product of all
      // Pareto front elements. We then filter out incompatible tuples.
      val allFronts = harmonizedArgData.map(_.fronts)
      val allDecisions = allFronts.flatMap(_.keys)
      val newFronts = allDecisions.flatMap { decision =>
        val frontsForDecision = allFronts.map(_(decision))
        val products = cartesianProduct(frontsForDecision.map(_.elements).toList)
        val compatibleProducts = products.filter(selections => selections.forall(sel => selections.forall(_.isCompatibleWith(sel))))
        val inlinedFrontElements = compatibleProducts.map { selections =>
          val costVectors = selections.map(_.costVector)
          // TODO: compute cost equations once, then evaluate them for each selection.
          val decisions = selections.map(_.selection).reduce(_ ++ _)
          val nodeCostVector = costModel.dimensionIndices.map(dim => node.useCost(costModel, dim, costVectors.map(_(dim)).map(IntConstant))).map(_.evalInt)
          val outlinedFunctionCosts = selections.map(_.outlinedFunctionCosts).reduce(_ ++ _)
          NodeSelectionsAndCost(decisions ++ Map(c -> node), nodeCostVector, outlinedFunctionCosts)
        }

        if (outlineableClasses.contains(c)) {
          Seq(decision -> ParetoFront(inlinedFrontElements))
        } else {
          val outlinedFrontElements = compatibleProducts.map { selections =>
            val costVectors = selections.map(_.costVector)
            // TODO: compute cost equations once, then evaluate them for each selection.
            val decisions = selections.map(_.selection).reduce(_ ++ _)
            val useCostVector = costModel.dimensionIndices.map(dim => node.useCost(costModel, dim, costVectors.map(_(dim)).map(IntConstant))).map(_.evalInt)
            val callCostVector = costModel.dimensionIndices.map(dim => node.callCost(costModel, dim, costVectors.map(_(dim)).map(IntConstant))).map(_.evalInt)
            val outlinedFunctionCosts = selections.map(_.outlinedFunctionCosts).reduce(_ ++ _) ++ Map(c -> useCostVector)
            NodeSelectionsAndCost(decisions ++ Map(c -> node), callCostVector, outlinedFunctionCosts)
          }
          val inlineDecision = OutliningDecisions(decision.decisions ++ Map(c -> false))
          val outlineDecision = OutliningDecisions(decision.decisions ++ Map(c -> true))
          Seq(inlineDecision -> ParetoFront(inlinedFrontElements), outlineDecision -> ParetoFront(outlinedFrontElements))
        }
      }.toMap

      AnalysisData(newFronts)
    }

    // Next, we run the equivalent of an e-graph analysis on the agglomerated graph. This analysis determines the
    // Pareto front of the cost vectors of each class for each relevant set of outlining decisions.
    // Compute analyses until we reach a fixpoint.
    var worklist = mutable.ArrayBuffer[AgglomeratedClass]()
    worklist ++= classes
    while (worklist.nonEmpty) {
      val newWorklist = mutable.ArrayBuffer[AgglomeratedClass]()
      for (eClass <- classes) {
        val eligibleNodes = eClass.nodes.filter(_.args.forall(analysisResults.contains))
        val nodeResults = eligibleNodes.map(make(_, eClass))
        if (nodeResults.nonEmpty) {
          var classResult = nodeResults.reduce(_.merge(_))
          val hasChanged = if (analysisResults.contains(eClass)) {
            val oldResult = analysisResults(eClass)
            classResult = oldResult.merge(classResult)
            classResult != oldResult
          } else {
            true
          }

          if (hasChanged) {
            analysisResults(eClass) = classResult
            newWorklist ++= parents.getOrElse(eClass, Seq.empty)
          }
        }
      }
      worklist = newWorklist
    }

    // Check that we computed analyses for all e-classes.
    for (eClass <- classes) {
      assert(analysisResults.contains(eClass))
    }

    analysisResults.toMap
  }

  private case class NodeSelectionsAndCost(selection: Map[AgglomeratedClass, AgglomeratedNode],
                                           costVector: Seq[BigInt],
                                           outlinedFunctionCosts: Map[AgglomeratedClass, Seq[BigInt]]) {
    def dominates(other: NodeSelectionsAndCost): Boolean = {
      costVector.zip(other.costVector).forall { case (f, s) => f <= s }
    }

    def isCompatibleWith(other: NodeSelectionsAndCost): Boolean = {
      selection.keySet.intersect(other.selection.keySet).forall { c =>
        selection(c) == other.selection(c)
      } && outlinedFunctionCosts.keySet.intersect(other.outlinedFunctionCosts.keySet).forall { c =>
        outlinedFunctionCosts(c).zip(other.outlinedFunctionCosts(c)).forall { case (f, s) => f == s }
      }
    }

    def totalCost: Seq[BigInt] = {
      val outlinedFuncCosts = outlinedFunctionCosts.values
      costModel.dimensionIndices.map { dim =>
        costModel.totalCost(dim)(IntConstant(costVector(dim)), outlinedFuncCosts.map(_(dim)).map(IntConstant).toSeq).evalInt
      }
    }
  }

  private case class ParetoFront(elements: Seq[NodeSelectionsAndCost]) {
    def merge(other: ParetoFront): ParetoFront = {
      val newElements = (elements ++ other.elements).distinct
      val newFront = newElements.filterNot(v => newElements.exists(v2 => v2.costVector != v.costVector && v2.dominates(v)))
      ParetoFront(newFront)
    }
  }

  /**
   * Outlining decisions for a set of agglomerated classes.
   * @param decisions The outlining decisions for each class.
   */
  private case class OutliningDecisions(decisions: Map[AgglomeratedClass, Boolean]) {
    def consideredClasses: Set[AgglomeratedClass] = decisions.keySet

    def isOutlined(c: AgglomeratedClass): Option[Boolean] = decisions.get(c)
  }

  /**
   * The analysis data attached to each agglomerated class.
   * @param fronts The Pareto fronts for each set of outlining decisions.
   */
  private case class AnalysisData(fronts: Map[OutliningDecisions, ParetoFront]) {
    def consideredClasses: Set[AgglomeratedClass] = fronts.keySet.flatMap(_.consideredClasses)

    def considerClasses(classes: Set[AgglomeratedClass]): AnalysisData = {
      // Find the set of new classes we do not yet consider in our outlining decisions.
      val newClasses = classes -- consideredClasses

      // Build the Cartesian product of all outlining decisions for the new classes.
      val newDecisions = newClasses.subsets().map { subset =>
        newClasses.map {
          case c if subset.contains(c) => c -> false
          case c => c -> true
        }.toMap
      }.toSeq

      // Duplicate each front for the new decisions.
      val newFronts = fronts.flatMap { front =>
        newDecisions.map { decisions =>
          val newDecisions = front._1.decisions ++ decisions
          OutliningDecisions(newDecisions) -> front._2
        }
      }

      AnalysisData(newFronts)
    }

    def merge(other: AnalysisData): AnalysisData = {
      // First, harmonize the set of classes considered by each front.
      val allClasses = consideredClasses ++ other.consideredClasses
      val newThis = considerClasses(allClasses)
      val newOther = other.considerClasses(allClasses)

      // Next, merge fronts.
      val newFronts = newThis.fronts.map { case (decisions, front) =>
        decisions -> front.merge(newOther.fronts(decisions))
      }

      // Finally, pack the new fronts into a new AnalysisData.
      AnalysisData(newFronts)
    }
  }
}
