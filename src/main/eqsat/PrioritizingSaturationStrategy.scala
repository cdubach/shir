package eqsat

import scala.util.Random

/**
  * A strategy that determines how to prioritize matches found by a rewrite engine. PrioritizingSaturationStrategy uses
  * this strategy to determine which matches to apply in each iteration.
  */
trait PrioritizationStrategy {
  /**
    * Assigns priorities to the matches found by the rewrite engine.
    * @param matches The matches found by the rewrite engine.
    * @param graph The e-graph that the matches were found in.
    * @return A sequence of priorities, one for each match.
    */
  def priorities(matches: Seq[(Rewrite, Match[EClassT])], graph: ReadOnlyEGraph[ENodeT, EClassT]): Seq[Double]

  /**
    * Determines how many matches to pick in a given iteration.
    * @param iteration The index of the iteration being run.
    * @param prioritizedMatches The matches found by the rewrite engine, along with their priorities.
    * @param graph The e-graph that the matches were found in.
    * @return The number of matches to pick.
    */
  def numberOfMatchesToPick(iteration: Int,
                            prioritizedMatches: Seq[(Rewrite, Match[EClassT], Double)],
                            graph: ReadOnlyEGraph[ENodeT, EClassT]): Int
}

/**
  * A saturation strategy that prioritizes matches based on some criterion. Each match is assigned a numerical priority,
  * and the strategy picks matches to apply based on these priorities. A match's probability to be chosen is equal to
  * the match's priority divided by the sum of all priorities.
  */
final case class PrioritizingSaturationStrategy(rules: Seq[Rewrite],
                                                prioritizationStrategy: PrioritizationStrategy,
                                                random: Random = new Random(0)) extends SaturationStrategy with SaturationRunner {

  private def priorities(matches: Seq[(Rewrite, Match[EClassT])],
                         graph: ReadOnlyEGraph[ENodeT, EClassT]): Seq[Double] =
    prioritizationStrategy.priorities(matches, graph)

  private def numberOfMatchesToPick(iteration: Int,
                                    prioritizedMatches: Seq[(Rewrite, Match[EClassT], Double)],
                                    graph: ReadOnlyEGraph[ENodeT, EClassT]): Int =
    prioritizationStrategy.numberOfMatchesToPick(iteration, prioritizedMatches, graph)

  /**
    * Runs a single iteration of this saturation strategy.
    *
    * @param iteration The index of the iteration being run.
    * @param engine    A rewrite engine that can execute the strategy.
    * @return `true` if the saturation strategy may have more work to do; otherwise, `false`.
    */
  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = {
    // First final all matches
    val matches = engine.findMatches(rules)

    // Then, assign priorities to each match
    val flatMatches = matches.flatMap { case (rule, matches) => matches.map { match_ => rule -> match_ } }
    val assignedPriorities = priorities(flatMatches, engine.graph)
    val prioritizedMatches = flatMatches.zip(assignedPriorities).map { case ((rule, match_), priority) => (rule, match_, priority) }

    // Pick a subset of the matches to apply
    val selectedMatches = pickMultiple(
      numberOfMatchesToPick(iteration, prioritizedMatches, engine.graph) min prioritizedMatches.length,
      prioritizedMatches)
    val selectedMatchedByRule = selectedMatches
      .groupBy { case (rule, _) => rule }
      .toSeq
      .map { case (rule, matches) => rule -> matches.map { case (_, match_) => match_ } }

    // Apply the selected matches
    engine.applyMatches(selectedMatchedByRule).nonEmpty
  }

  private def pickMultiple(n: Int, prioritizedMatches: Seq[(Rewrite, Match[EClassT], Double)]): Seq[(Rewrite, Match[EClassT])] = {
    assert(n < prioritizedMatches.length, "Cannot pick more matches than there are in the list")

    def pickWithoutReplacement(n: Int,
                               sortedMatches: Seq[(Rewrite, Match[EClassT], Double)]): List[(Rewrite, Match[EClassT])] = {
      if (n <= 0) {
        List.empty
      } else {
        val cumulativePriorities = sortedMatches.scanLeft(0.0) { case (acc, (_, _, priority)) => acc + priority }
        val totalPriority = cumulativePriorities.last
        val randomValue = random.nextDouble() * totalPriority
        val index = cumulativePriorities.indexWhere(_ > randomValue)
        val item = sortedMatches(index)._1 -> sortedMatches(index)._2

        item :: pickWithoutReplacement(n - 1, sortedMatches.patch(index, Seq.empty, 1))
      }
    }

    pickWithoutReplacement(n, prioritizedMatches.sortBy { case (_, _, priority) => -priority })
  }

  /**
   * Creates a saturation runner that can be used to run the saturation strategy.
   *
   * @return A saturation runner.
   */
  override def instantiate(): SaturationRunner = this
}
