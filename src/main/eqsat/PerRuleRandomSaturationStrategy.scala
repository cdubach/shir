package eqsat

import scala.util.Random

/**
 * A saturation strategy that applies a fixed number of matches for each rule in each iteration. The matches are chosen
 * randomly.
 * @param rulesAndLimits A sequence of pairs, where each pair consists of a rewrite rule and the number of matches to
 *                       apply for that rule in each iteration.
 * @param random A random number generator.
 */
final case class PerRuleRandomSaturationStrategy(rulesAndLimits: Seq[(Rewrite, Int)],
                                                 random: Random = new Random(0)) extends SaturationStrategy with SaturationRunner {

  private def rules: Seq[Rewrite] = rulesAndLimits.map(_._1)

  private def limit(rule: Rewrite): Int = rulesAndLimits.find(_._1 == rule).get._2

  /**
   * Runs a single iteration of this saturation strategy.
   *
   * @param iteration The index of the iteration being run.
   * @param engine    A rewrite engine that can execute the strategy.
   * @return `true` if the saturation strategy may have more work to do; otherwise, `false`.
   */
  override def runIteration(iteration: Int, engine: RewriteEngine): Boolean = {
    val matches = engine.findMatches(rules)

    val selectedMatches = matches.map { case (rule, matches) =>
      val limit_ = limit(rule)
      (rule, random.shuffle(matches).take(limit_))
    }

    engine.applyMatches(selectedMatches).nonEmpty
  }

  /**
   * Creates a saturation runner that can be used to run the saturation strategy.
   *
   * @return A saturation runner.
   */
  override def instantiate(): SaturationRunner = this
}
