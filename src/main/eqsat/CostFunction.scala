package eqsat

import core.Expr

/**
 * A cost function that computes the cost of expressions based on the costs of their arguments.
 * @tparam C The type of the cost metric computed by this function.
 */
trait CostFunction[C] {
  /**
   * Computes the total cost of an expression given its top-level node nad the size of its arguments.
   * @param node The top-level node of an expression.
   * @param argCosts The cost of `node`'s arguments.
   * @return The total cost of `node`.
   */
  def apply(node: Expr, argCosts: Seq[C]): C

  /**
   * Computes the total cost of an expression.
   * @param expr An expression.
   * @return The total cost of `expr`.
   */
  final def apply(expr: Expr): C = {
    apply(expr, ExprHelpers.args(expr).map(apply))
  }
}

object CostFunction {
  /**
   * A cost function that measures the total size of expressions.
   */
  object Size extends CostFunction[BigInt] {
    override def apply(node: Expr, argCosts: Seq[BigInt]): BigInt = 1 + argCosts.sum
  }

  /**
   * A cost function that measures the tree depth of expressions.
   */
  object Depth extends CostFunction[BigInt] {
    override def apply(node: Expr, argCosts: Seq[BigInt]): BigInt = 1 + argCosts.max
  }
}
