package cGen.eqsat

import core.Expr
import eqsat.{EClassT, Extractor}

/**
 * An extractor wrapper that filters out undesirable expressions.
 * @param extractor An inner extractor to which all extraction is delegated.
 * @param accept A predicate that tells whether expressions are acceptable or not. Unacceptable expressions are
 *               replaced by a `None` output.
 */
final case class ConditionalExtractor(extractor: Extractor, accept: Expr => Boolean) extends Extractor {
  override def apply(eClass: EClassT): Option[Expr] = {
    extractor(eClass) match {
      case Some(expr) if accept(expr) => Some(expr)
      case _ => None
    }
  }
}
