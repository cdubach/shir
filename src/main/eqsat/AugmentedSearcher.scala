package eqsat

/**
 * A searcher implementation that consists of two searchers. The primary searcher acts like a conventional searcher and
 * produces initial matches. For every match, the secondary searcher performs an additional search through the e-graph,
 * producing its own set of matches. These matches' type and expression variable mappings are folded into the match
 * produced by the primary searcher.
 *
 * @param primary A primary searcher.
 * @param secondary A secondary searcher.
 */
final case class AugmentedSearcher(primary: Searcher, secondary: Searcher) extends Searcher {
  /**
   * Tests if an e-class matches the pattern this searcher looks for; returns all resulting matches.
   *
   * @param eGraph    The e-graph that defines the e-class.
   * @param eClass    An e-class to search.
   * @param stopwatch A stopwatch that times the search.
   * @return All matches for this pattern and e-class combo.
   */
  override def searchEClass[ENode <: ENodeT, EClass <: EClassT](eGraph: ReadOnlyEGraph[ENode, EClass])
                                                               (eClass: EClass,
                                                                stopwatch: HierarchicalStopwatch): Seq[Match[EClass]] = {

    // Instead of performing a separate search for each primary match, we'll search for primary and secondary matches at
    // the same time. Then, we'll see which matches we can unify.
    val primaryMatches = primary.searchEClass(eGraph)(eClass, stopwatch)
    val secondaryMatches = secondary.search(eGraph, stopwatch)

    mergeMatches(primaryMatches, secondaryMatches)
  }

  /**
   * Finds all e-classes in an e-graph that match the pattern this searcher looks for.
   * @param eGraph An e-graph to search.
   * @param createStopwatch Creates a stopwatch to measure a search.
   * @return A sequence of matches.
   */
  override def search[ENode <: ENodeT, EClass <: EClassT](eGraph: ReadOnlyEGraph[ENode, EClass],
                                                          createStopwatch: () => HierarchicalStopwatch): Seq[Match[EClass]] = {

    val primaryMatches = primary.search(eGraph, createStopwatch)
    val secondaryMatches = secondary.search(eGraph, createStopwatch)

    mergeMatches(primaryMatches, secondaryMatches)
  }

  private def mergeMatches[EClass <: EClassT](primaryMatches: Seq[Match[EClass]],
                                              secondaryMatches: Seq[Match[EClass]]): Seq[Match[EClass]] = {

    primaryMatches.flatMap(m1 => secondaryMatches.collect({
      case m2 if MergedMatch.haveConsistentTypes(m1, m2) => MergedMatch(m1, m2)
    }))
  }
}
