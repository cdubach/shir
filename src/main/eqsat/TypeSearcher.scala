package eqsat

import core.{Type, TypeChecker, TypeVarT}

/**
 * A searcher whose matches consist of the cartesian product of another searcher's matches and all possible type
 * assignments for a type variable, where only types already in the e-graph are considered for type assignments.
 * @param searcher A searcher that produces matches.
 * @param typeVar A type variable that will be mapped to all possible types from the graph.
 */
case class TypeSearcher(searcher: Searcher, typeVar: TypeVarT) extends Searcher {
  /**
   * Tests if an e-class matches the pattern this searcher looks for; returns all resulting matches.
   *
   * @param eGraph    The e-graph that defines the e-class.
   * @param eClass    An e-class to search.
   * @param stopwatch A stopwatch that times the search.
   * @return All matches for this pattern and e-class combo.
   */
  override def searchEClass[ENode <: ENodeT, EClass <: EClassT](eGraph: ReadOnlyEGraph[ENode, EClass])
                                                               (eClass: EClass,
                                                                stopwatch: HierarchicalStopwatch): Seq[Match[EClass]] = {
    val matches = searcher.searchEClass(eGraph)(eClass, stopwatch)
    val types = findAllMatchingTypes(eGraph).toList
    foldTypesIntoMatches(matches, types)
  }

  /**
   * Finds all e-classes in an e-graph that match the pattern this searcher looks for.
   * @param eGraph An e-graph to search.
   * @param createStopwatch Creates a stopwatch to measure a search.
   * @return A sequence of matches.
   */
  override def search[ENode <: ENodeT, EClass <: EClassT](eGraph: ReadOnlyEGraph[ENode, EClass],
                                                          createStopwatch: () => HierarchicalStopwatch): Seq[Match[EClass]] = {

    val matches = searcher.search(eGraph, createStopwatch)
    val types = findAllMatchingTypes(eGraph)
    foldTypesIntoMatches(matches, types)
  }

  private def findAllMatchingTypes[ENode <: ENodeT, EClass <: EClassT](eGraph: ReadOnlyEGraph[ENode, EClass]) = {
    eGraph
      .classes
      .flatMap(eClass => eClass.nodes)
      .flatMap(node => node.expr.children.collect({ case t: Type => t }) ++ Seq(node.t))
      .distinct
      .filter(t => TypeChecker.solveConstraints(Seq((typeVar, t))).isDefined)
  }

  private def foldTypesIntoMatches[A <: EClassT](matches: Seq[Match[A]], types: Seq[Type]) = {
    matches.flatMap(m => types.map(TypeAugmentedMatch(m, typeVar, _)))
  }
}
