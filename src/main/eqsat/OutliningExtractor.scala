package eqsat

import core.{Expr, LambdaT, ParamDef}
import eqsat.solver.{AgglomeratedClass, AgglomeratedNode, Formula}

import scala.annotation.tailrec
import scala.collection.mutable

/**
 * An extractor that outlines expressions in an e-graph as part of the extraction process.
 */
trait OutliningExtractor extends SelectingExtractor {
  /**
   * Determines whether e-class agglomeration is enabled.
   * @return `true` if e-class agglomeration is enabled; otherwise, `false`.
   */
  protected def enableAgglomeration: Boolean

  /**
    * The cost model for this outlining extractor.
    * @return The cost model for this outlining extractor.
    */
  def costModel: OutliningExtractorCostModel

  /**
   * Determines whether a given e-node is eligible for extraction.
   * @return A function that determines whether a given e-node is eligible for extraction.
   */
  def isEligible: ENodeT => Boolean

  /**
   * Constructs a lambda from a parameter definition and a body expression.
   */
  protected implicit val buildLambda: (ParamDef, Expr) => LambdaT

  /**
   * Constructs a function call from two expressions: a callee and an argument.
   */
  protected implicit val buildFunctionCall: (Expr, Expr) => Expr

  /**
   * A solution produced by an extractor that considers outlining.
   * @param selection Maps every selected agglomerated class to its associated agglomerated node.
   * @param outline Maps every selected agglomerated class to a Boolean that determines whether the class has been nominated
   *                for outlining.
   * @param costs The total costs of the solution, per cost dimension.
   */
  final case class AgglomeratedSolution(selection: PartialFunction[AgglomeratedClass, AgglomeratedNode],
                                        outline: PartialFunction[AgglomeratedClass, Boolean],
                                        costs: Seq[BigInt])

  /**
   * A solution produced by an extractor that considers outlining.
   * @param selection Maps every selected e-class to its associated e-node.
   * @param outline Maps every selected e-class to a Boolean that determines whether the e-class has been nominated for
   *                outlining.
   * @param costs The total costs of the solution, per cost dimension.
   */
  final case class EGraphSolution(selection: PartialFunction[EClassT, ENodeT],
                                  outline: PartialFunction[EClassT, Boolean],
                                  costs: Seq[BigInt]) {
    /**
      * Computes the set of all classes in the solution starting from a given root class.
      * @param root The root class.
      * @return The set of all classes in the solution.
      */
    def classesInSolution(root: EClassT): Set[EClassT] = {
      val seen = mutable.Set[EClassT]()
      val stack = mutable.Stack[EClassT](root)
      while (stack.nonEmpty) {
        val current = stack.pop()
        seen += current

        val node = selection(current)
        stack.pushAll(node.args.filterNot(seen.contains))
      }
      seen.toSet
    }

    /**
     * Computes the set of all outlined classes starting from a given root class.
     * @param root The root class.
     * @return The set of all outlined classes.
     */
    def outlinedClasses(root: EClassT): Set[EClassT] = {
      classesInSolution(root).filter(outline)
    }

    /**
     * Computes the number of outlined classes starting from a given root class.
     * @param root The root class.
     * @return The number of outlined classes.
     */
    def outlinedClassCount(root: EClassT): Int = outlinedClasses(root).size
  }

  /**
   * Determines whether a given e-class is a boundary for agglomeration. An e-class is a boundary for agglomeration if
   * multiple e-nodes in the e-class are eligible for extraction, or if the e-class is used more than once. In the
   * latter case, the e-class is also an outlining candidate.
   * @param c The e-class to consider.
   * @param eligible The set of eligible e-classes.
   * @param useCount A mapping of e-classes to their in-degrees.
   * @return `true` if the e-class is a boundary for agglomeration; otherwise, `false`.
   */
  private def isAgglomerationBoundary(c: EClassT, eligible: Set[EClassT], useCount: Map[EClassT, Int]): Boolean =
    !enableAgglomeration || c.nodes.count(isNodeEligible(_, eligible)) != 1 || useCount.getOrElse(c, 1) > 1

  private def agglomerate(root: ENodeT,
                          eligible: Set[EClassT],
                          useCounts: Map[EClassT, Int],
                          classMap: mutable.Map[EClassT, AgglomeratedClass]): AgglomeratedNode = {
    val children = root.args.map(arg => {
      if (isAgglomerationBoundary(arg, eligible, useCounts))
        Right(agglomerate(arg, eligible, useCounts, classMap))
      else {
        Left(agglomerate(arg.nodes.find(isNodeEligible(_, eligible)).get, eligible, useCounts, classMap))
      }
    })

    AgglomeratedNode(root, children)
  }

  /**
   * Turns an e-graph rooted at `root` into a graph of `AgglomeratedClass` and `AgglomeratedNode` instances.
   * @param root The root element.
   * @return The same root, as an `AgglomeratedClass`.
   */
  private def agglomerate(root: EClassT,
                          eligible: Set[EClassT],
                          useCounts: Map[EClassT, Int],
                          classMap: mutable.Map[EClassT, AgglomeratedClass]): AgglomeratedClass = {
    classMap.get(root) match {
      case Some(existing) => existing
      case None =>
        val c = new AgglomeratedClass(root)
        classMap(root) = c
        for (node <- root.nodes.filter(isNodeEligible(_, eligible))) {
          c.nodes.append(agglomerate(node, eligible, useCounts, classMap))
        }
        c
    }
  }

  /**
   * Turns an e-graph rooted at `root` into a graph of `AgglomeratedClass` and `AgglomeratedNodes` instances.
   * @param root The root element.
   * @return The same root, as an `AgglomeratedClass`.
   */
  private def agglomerate(root: EClassT, eligible: Set[EClassT], useCounts: Map[EClassT, Int]): AgglomeratedClass = {
    agglomerate(root, eligible, useCounts, mutable.Map[EClassT, AgglomeratedClass]())
  }

  /**
   * Formulates and solves the extraction/outlining optimization problem for a given agglomerated class.
   * @param root The root agglomerated class.
   * @return The solution to the optimization problem.
   */
  def solve(root: AgglomeratedClass): Option[AgglomeratedSolution]

  /**
   * Formulates and solves the extraction/outlining optimization problem for a given e-class.
   * @param root The root e-class.
   * @return The solution to the optimization problem.
   */
  def solve(root: EClassT): Option[EGraphSolution] = {
    val eligible = removeIneligibleClasses(root.reachable)
    val eligibleSet = eligible.toSet
    val useCounts = countInDegrees(eligible, eligibleSet)
    val agglomeratedRoot = agglomerate(root, eligibleSet, useCounts)
    val agglomerationMap = agglomeratedRoot.reachable.map(c => (c.eClass, c)).toMap
    solve(agglomeratedRoot).map { solution =>
      val agglomeratedSelection = solution.selection

      EGraphSolution({
        case c if eligibleSet.contains(c) && !isAgglomerationBoundary(c, eligibleSet, useCounts) => c.nodes.find(isNodeEligible(_, eligibleSet)).get
        case c if agglomeratedSelection.isDefinedAt(agglomerationMap(c)) => agglomeratedSelection(agglomerationMap(c)).node
      }, {
        case c if agglomerationMap.isDefinedAt(c) =>
          val agglomerated = agglomerationMap(c)
          if (solution.outline.isDefinedAt(agglomerated)) {
            solution.outline(agglomerated)
          } else {
            false
          }
        case c if eligibleSet.contains(c) => false
      }, solution.costs)
    }
  }

  override def select(eClass: EClassT): PartialFunction[EClassT, ENodeT] = {
    solve(eClass).get.selection
  }

  /**
   * Extracts an expression from an e-graph using a solution produced by a solver-based extractor.
   * @param root The root e-class.
   * @param solution The solution produced by the extractor.
   * @return The extracted expression.
   */
  def solutionToExpr(root: EClassT, solution: EGraphSolution): Expr = {
    OutliningExpressionBuilder(solution.selection, solution.outline)(buildLambda, buildFunctionCall).toExpr(root)
  }

  override def apply(eClass: EClassT): Option[Expr] = {
    solve(eClass).map(solutionToExpr(eClass, _))
  }

  /**
   * Counts the number of times each e-class is used as the argument to an e-node. Ineligible nodes do not contribute
   * to this count.
   * @param classes The set of e-classes to consider.
   * @return A mapping of e-classes to their in-degrees.
   */
  protected def countInDegrees(classes: Seq[AgglomeratedClass]): Map[AgglomeratedClass, Int] = {
    val inDegrees = mutable.Map[AgglomeratedClass, Int]()
    for (c <- classes) {
      for (node <- c.nodes) {
        for (arg <- node.args) {
          inDegrees(arg) = inDegrees.getOrElse(arg, 0) + 1
        }
      }
    }
    inDegrees.toMap
  }

  /**
   * Counts the number of times each e-class is used as the argument to an e-node. Ineligible nodes do not contribute
   * to this count.
   * @param classes The set of e-classes to consider.
   * @return A mapping of e-classes to their in-degrees.
   */
  private def countInDegrees(classes: Seq[EClassT], eligible: Set[EClassT]): Map[EClassT, Int] = {
    val inDegrees = mutable.Map[EClassT, Int]()
    for (c <-  classes.filter(eligible.contains)) {
      for (node <- c.nodes) {
        if (isNodeEligible(node, eligible)) {
          for (arg <- node.args) {
            inDegrees(arg) = inDegrees.getOrElse(arg, 0) + 1
          }
        }
      }
    }
    inDegrees.toMap
  }

  @tailrec
  private def removeIneligibleClasses(classes: Seq[EClassT]): Seq[EClassT] = {
    // Classes are ineligible either if all of their nodes are ineligible *or* if all of their nodes takes at least one
    // argument that is not in `classes`.
    val classSet = classes.toSet
    val newClasses = classes.filter(_.nodes.exists(isNodeEligible(_, classSet)))

    // Run this function until we reach a fixpoint
    if (newClasses == classes) {
      classes
    } else {
      removeIneligibleClasses(newClasses)
    }
  }

  private def isNodeEligible(node: ENodeT, eligibleClasses: Set[EClassT]): Boolean = {
    isEligible(node) && node.args.forall(eligibleClasses.contains)
  }
}

/**
 * A cost model for an outlining extractor.
 */
trait OutliningExtractorCostModel {
  /**
   * Gets the number of dimensions in this cost model. Each dimension operates as a separate cost. Some dimensions may
   * be bounded by a maximal cost via the `budget` function. Costs are aggregated into a single optimization problem
   * using the `minimizationTarget` function.
   */
  val dimensions: Int

  /**
    * The indices of the dimensions in this cost model.
    * @return The indices of the dimensions in this cost model.
    */
  final def dimensionIndices: Seq[Int] = 0 until dimensions

  /**
   * Computes the cost of a single instantiation of an e-node.
   * @param dimension The cost dimension to generate a formula for.
   * @param node The node to model a cost for.
   * @param argCosts The use cost of each argument of `node`, expressed as a formula.
   * @return A formula that expresses the cost of `node`, including the use cost of its arguments.
   */
  def useCost(dimension: Int)(node: ENodeT, argCosts: Seq[solver.Formula]): solver.Formula

  /**
   * Computes the cost associated with calling an outlined version of `node`.
   * @param dimension The cost dimension to generate a formula for.
   * @param node The node to model a cost for.
   * @param argCosts The use cost of each argument of `node`, expressed as a formula.
   * @return A formula that expresses the cost of making a call to an outlined version of `node`.
   */
  def callCost(dimension: Int)(node: ENodeT, argCosts: Seq[solver.Formula]): solver.Formula

  /**
   * Determines the total cost of a cost dimension.
   * @param dimension The dimension to assess.
   * @param rootCost The cost of the root expression.
   * @param outlinedExprCosts The use cost of each outlined expression.
   * @return The total cost of the cost dimension.
   */
  def totalCost(dimension: Int)(rootCost: solver.Formula, outlinedExprCosts: Seq[solver.Formula]): solver.Formula = {
    (rootCost +: outlinedExprCosts).reduce(_ + _)
  }

  /**
   * The budget available for a certain cost dimension. The cost for that dimension cannot exceed the budget.
   * @param dimension The dimension to determine a budget for.
   * @return A budget, if one exists; otherwise, `None`.
   */
  def budget(dimension: Int): Option[solver.Formula]

  /**
   * The total minimization target to send to the solver.
   * @param dimensionCosts The total costs of each dimension.
   * @return A formula describing the minimization target.
   */
  def minimizationTarget(dimensionCosts: Seq[solver.Formula]): solver.Formula
}

/**
 * A simple, one-dimensional cost model for solver-based extractors.
 * @param costOf Determines the cost of a node, excluding the cost of its arguments.
 * @param callOverhead Determines the cost of calling an outlined version of a node.
 */
case class SimpleOutliningExtractorCostModel(costOf: Expr => BigInt,
                                             callOverhead: Expr => BigInt) extends OutliningExtractorCostModel {
  /**
   * Gets the number of dimensions in this cost model. Each dimension operates as a separate cost. Some dimensions may
   * be bounded by a maximal cost via the `budget` function. Costs are aggregated into a single optimization problem
   * using the `minimizationTarget` function.
   */
  override val dimensions: Int = 1

  /**
   * Computes the cost of a single instantiation of an e-node.
   *
   * @param dimension The cost dimension to generate a formula for.
   * @param node      The node to model a cost for.
   * @param argCosts  The use cost of each argument of `node`, expressed as a formula.
   * @return A formula that expresses the cost of `node`, including the use cost of its arguments.
   */
  override def useCost(dimension: Int)(node: ENodeT, argCosts: Seq[Formula]): Formula = {
    val nodeCost: Formula = solver.IntConstant(costOf(node.expr))
    argCosts.foldLeft(nodeCost)(_ + _)
  }

  /**
   * The cost associated with calling an outlined version of `node`.
   *
   * @param dimension The cost dimension to generate a formula for.
   * @param node      The node to model a cost for.
   * @param argCosts  The use cost of each argument of `node`, expressed as a formula.
   * @return A formula that expresses the cost of making a call to an outlined version of `node`.
   */
  override def callCost(dimension: Int)(node: ENodeT, argCosts: Seq[Formula]): Formula = {
    solver.IntConstant(callOverhead(node.expr))
  }

  /**
   * The budget available for a certain cost dimension. The cost for that dimension cannot exceed the budget.
   *
   * @param dimension The dimension to determine a budget for.
   * @return A budget, if one exists; otherwise, `None`.
   */
  override def budget(dimension: Int): Option[Formula] = None

  /**
   * The total minimization target to send to the solver.
   *
   * @param dimensionCosts The total costs of each dimension.
   * @return A formula describing the minimization target.
   */
  override def minimizationTarget(dimensionCosts: Seq[Formula]): Formula = dimensionCosts.head
}
