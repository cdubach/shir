package eqsat.solver

import scala.concurrent.duration._

/**
 * An optimizer that uses an iterative approach to minimize the cost function.
 * @param solver The satisfiability checker to use.
 * @param timeout The default-supplied maximum time to spend on optimization.
 */
final case class IterativeOptimizer(solver: SatisfiabilityChecker, timeout: Duration) extends Optimizer {
  private def solve(problem: MinimizationProblem, upperBound: Option[BigInt], start: Deadline, timeout: Duration): Option[Map[Var, Constant]] = {
    val elapsed = Deadline.now - start
    if (elapsed >= timeout) {
      return None
    }

    val costVar = IntVar("__cost")
    val costBound = upperBound match {
      case Some(ub) => Seq(costVar < IntConstant(ub))
      case None => Seq.empty
    }
    val result = solver.solve(SatisfiabilityProblem((problem.constraints :+ (costVar == problem.cost)) ++ costBound), timeout = timeout - elapsed)
    result match {
      case None => None
      case Some(assignment) =>
        val cost = assignment(costVar).asInstanceOf[IntConstant].value
        Some(solve(problem, Some(cost), start, timeout).getOrElse(assignment))
    }
  }

  /**
   * Solves the given minimization problem.
   *
   * @param problem The minimization problem to solve.
   * @return A map from variables to their values that minimizes the cost function subject to the constraints.
   */
  override def solve(problem: MinimizationProblem): Option[Map[Var, Constant]] =
    solve(problem, None, Deadline.now, timeout)

  override def solve(problem: MinimizationProblem, timeout: Duration): Option[Map[Var, Constant]] = {
    solve(problem, None, Deadline.now, timeout)
  }
}
