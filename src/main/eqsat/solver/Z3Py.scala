package eqsat.solver

import java.io.PrintWriter
import sys.process._
import java.nio.file.Files
import scala.concurrent.duration._

private class CannotParseZ3Output(output: String, cause: Throwable)
  extends Exception(s"Cannot parse Z3 output: $output", cause)

private class Z3FormulaContext {

  private val builder = new collection.mutable.ListBuffer[String]()
  private val mapping = new java.util.IdentityHashMap[Formula, String]()
  private var counter = BigInt(0)

  def exists(formula: Formula): Boolean = formula match {
    case _: Var | _: BooleanConstant | _: IntConstant => true
    case _ => mapping.containsKey(formula)
  }

  def add(formula: Formula): Unit = {
    if (!exists(formula)) {
      formula.args.foreach(add)

      val es = toPython(formula, true)
      val id = s"t${counter}"
      counter += 1
      builder += s"${id} = ${es}"
      mapping.put(formula, id)
    }
  } 

  def addArgs(formula: Formula): Unit =
    formula.args.foreach(add)

  def seqToPython(seq: Seq[Formula], isAtomic: Boolean): String = {
    seq.map(toPython(_, isAtomic = isAtomic)).mkString(", ")
  }

  def toPython(formula: Formula, isAtomic: Boolean = false): String = {
    def parenthesize(expr: String) = if (isAtomic) expr else s"($expr)"
    formula match {
      case v: Var => s"v${v.name}"
      case BooleanConstant(true) => "True"
      case BooleanConstant(false) => "False"
      case IntConstant(n) => s"IntVal(${n})"
      case _ if mapping.containsKey(formula) => mapping.get(formula)

      case Add(lhs: Add, c) => parenthesize(s"${toPython(lhs, isAtomic = true)} + ${toPython(c)}")
      case Add(lhs, rhs) => parenthesize(s"${toPython(lhs)} + ${toPython(rhs)}")
      case Sub(lhs, rhs) => parenthesize(s"${toPython(lhs)} - ${toPython(rhs)}")
      case Mul(lhs, rhs) => parenthesize(s"${toPython(lhs)} * ${toPython(rhs)}")
      case Div(lhs, rhs) => parenthesize(s"${toPython(lhs)} / ${toPython(rhs)}")
      case Rem(lhs, rhs) => parenthesize(s"${toPython(lhs)} % ${toPython(rhs)}")
      case Pow(lhs, rhs) => parenthesize(s"${toPython(lhs)} ** ${toPython(rhs)}")
      case Equals(lhs, rhs) => parenthesize(s"${toPython(lhs)} == ${toPython(rhs)}")
      case LessThan(lhs, rhs) => parenthesize(s"${toPython(lhs)} < ${toPython(rhs)}")
      case LessThanOrEqual(lhs, rhs) => parenthesize(s"${toPython(lhs)} <= ${toPython(rhs)}")
      case And(args) => s"And(${seqToPython(args, isAtomic = true)})"
      case Or(args) => s"Or(${seqToPython(args, isAtomic = true)})"
      case Implies(lhs, rhs) => s"Implies(${toPython(lhs, isAtomic = true)}, ${toPython(rhs, isAtomic = true)})"
      case Not(op) => s"Not(${toPython(op, isAtomic = true)})"
      case If(condition, ifTrue, ifFalse) =>
        s"If(${toPython(condition, isAtomic = true)}, ${toPython(ifTrue, isAtomic = true)}, ${toPython(ifFalse, isAtomic = true)})"
      case Max(lhs, rhs) => toPython(If(LessThan(lhs, rhs), rhs, lhs))
      case Min(lhs, rhs) => toPython(If(LessThan(lhs, rhs), lhs, rhs))
      case PseudoBooleanLessThanOrEqual(operands, maxSum) =>
        val operandPairs = operands.map(pair =>
          s"(${toPython(pair._1, isAtomic = true)}, ${pair._2})").mkString(", ")
        s"PbLe([$operandPairs], $maxSum)"
      case PseudoBooleanGreaterThanOrEqual(operands, minSum) =>
        val operandPairs = operands.map(pair =>
          s"(${toPython(pair._1, isAtomic = true)}, ${pair._2})").mkString(", ")
        s"PbGe([$operandPairs], $minSum)"
      case PseudoBooleanEquals(operands, sum) =>
        val operandPairs = operands.map(pair =>
          s"(${toPython(pair._1, isAtomic = true)}, ${pair._2})").mkString(", ")
        s"PbEq([$operandPairs], $sum)"
    }
  }

  def statements: Seq[String] =
    builder.toList
}

final case class Z3PyConfig(PYTHON3_BIN: String = "python3",
                            // see https://microsoft.github.io/z3guide/programming/Parameters/#global-parameters
                            PARAMETERS: Map[String, String] = Map.empty) {
  private[solver] def generateVarDef(variable: Var): String = {
    val varType = variable match {
      case BooleanVar(_) => "Bool"
      case IntVar(_) => "Int"
    }
    s"v${variable.name} = $varType" + "(\"" + variable.name + "\")"
  }

  private[solver] def generateScriptPreamble(vars: Seq[Var], engine: String): Seq[String] = {
    val importStatement = "from z3 import *"
    val glbOpts = PARAMETERS.map(p => s"set_option('${p._1}', '${p._2}')").toSeq
    val varDefs = vars.map(generateVarDef)
    val solverDef = s"s = ${engine}()"
    importStatement +: (glbOpts ++ varDefs) :+ solverDef
  }

  private[solver] def generateScriptCheck(): Seq[String] = {
    Seq(
      "if s.check() == unsat: print(\"unsat\"); exit()",
    )
  }

  private[solver] def generateScriptEpilogue(vars: Seq[Var]): Seq[String] = {
    val prints = vars.map(v => s"print(f'{v${v.name}} = {model[v${v.name}]},')")
    "print(\"[\")" +: prints :+ "print(\"]\")"
  }

  private[solver] def parseScriptOutput(output: String, vars: Seq[Var]): Option[Map[Var, Constant]] = {
    if (output.trim == "unsat") {
      None
    } else {
      assert(output(0) == '[')
      assert(output(output.length - 1) == ']')

      val varMap = vars.map(v => (v.name, v)).toMap
      val assignmentLines = output.substring(1, output.length - 1).split(',').filter(_.trim.nonEmpty)
      val assignments = assignmentLines.flatMap { line =>
        val Array(lhs, rhs) = line.split('=')
        val name = lhs.trim
        varMap.get(name).map { variable =>
          val valueStr = rhs.trim
          val value = variable match {
            case IntVar(_) => IntConstant(BigInt(valueStr))
            case BooleanVar(_) =>
              valueStr match {
                case "True" => BooleanConstant(true)
                case "False" => BooleanConstant(false)
                case _ => ???
              }
          }
          (variable, value)
        }
      }
      Some(assignments.toMap)
    }
  }

  private[solver] def runZ3Script(script: String): String = {
    // Create temporary file
    val file = Files.createTempFile("", ".py")
    try {
      // Write script to temporary file
      val out = new PrintWriter(file.toFile)
      try out.println(script)
      finally out.close()

      // Run the script, invoking Z3
      s"${PYTHON3_BIN} $file" !!
    } finally {
      // Delete temporary script file
      Files.delete(file)
    }
  }
}

final case class Z3Py(config: Z3PyConfig = Z3PyConfig()) extends SatisfiabilityChecker with Optimizer {

  override def solve(problem: MinimizationProblem, timeout: Duration): Option[Map[Var, Constant]] = {
    if (timeout <= Duration.Zero)
      None
    else {
      // Generate script to invoke Z3
      val context = new Z3FormulaContext()
      problem.constraints.foreach(context.addArgs)
      context.addArgs(problem.cost)

      val preamble = config.generateScriptPreamble(problem.vars, "Optimize")
      val constraints = problem.constraints.map(formula => s"s.add(${context.toPython(formula, isAtomic = true)})")
      val timing = if (timeout >= Duration.Inf) Seq() else Seq(s"s.set('timeout', ${timeout.toMillis})")
      val minimization = (s"result = s.minimize(${context.toPython(problem.cost, isAtomic = true)})" +: config.generateScriptCheck() :+ "s.lower(result)")
      val epilogue = "model = s.model()" +: config.generateScriptEpilogue(problem.vars)
      val script = (preamble ++ context.statements ++ constraints ++ timing ++ minimization ++ epilogue).mkString("\n")

      // Invoke Z3
      val scriptOutput = config.runZ3Script(script)

      // Parse the script's output
      try config.parseScriptOutput(scriptOutput.trim, problem.vars)
      catch { case ex: Throwable => throw new CannotParseZ3Output(scriptOutput, ex) }
    }
  }

  override def solve(problem: SatisfiabilityProblem, timeout: Duration): Option[Map[Var, Constant]] = {
    if (timeout <= Duration.Zero)
      None
    else {
      // Generate script to invoke Z3
      val context = new Z3FormulaContext()
      problem.constraints.foreach(context.addArgs)

      val preamble = config.generateScriptPreamble(problem.vars, "Solver")
      val constraints = problem.constraints.map(formula => s"s.add(${context.toPython(formula, isAtomic = true)})")
      val timing = if (timeout >= Duration.Inf) Seq() else Seq(s"s.set('timeout', ${timeout.toMillis})")
      val minimization = config.generateScriptCheck()
      val epilogue = "model = s.model()" +: config.generateScriptEpilogue(problem.vars)
      val script = (preamble ++ context.statements ++ constraints ++ timing ++ minimization ++ epilogue).mkString("\n")

      // Invoke Z3
      val scriptOutput = config.runZ3Script(script)

      // Parse the script's output
      try config.parseScriptOutput(scriptOutput.trim, problem.vars)
      catch { case ex: Throwable => throw new CannotParseZ3Output(scriptOutput, ex) }
    }
  }
}

/**
 * This one does not reset the Solver's state, meaning Z3 does not discard the
 * "lemmas", potentially allowing faster solving.
 */
final case class IterativeZ3Py(config: Z3PyConfig = Z3PyConfig()) extends Optimizer {

  override def solve(problem: MinimizationProblem, timeout: Duration): Option[Map[Var, Constant]] = {
    if (timeout <= Duration.Zero)
      None
    else {
      // Generate the constraints as a SAT problem
      val context = new Z3FormulaContext()
      problem.constraints.foreach(context.addArgs)
      context.addArgs(problem.cost)

      val preamble = config.generateScriptPreamble(problem.vars, "Solver")
      val constraints = problem.constraints.map(formula => s"s.add(${context.toPython(formula, isAtomic = true)})")

      // Generate the optimization loop
      val minimization = Seq(
        s"""
          |${if (timeout >= Duration.Inf) "" else s"import time"}
          |${if (timeout >= Duration.Inf) "" else s"start = time.perf_counter_ns()"}
          |costf = ${context.toPython(problem.cost, isAtomic = true)}
          |dummy = Int("__cost")
          |s.add(costf < dummy)
          |${if (timeout >= Duration.Inf) "" else s"s.set('timeout', ${timeout.toMillis})"}
          |if s.check() != sat:
          |  print("unsat")
          |  exit()
          |
          |model = s.model()
          |minimum = model.eval(costf)
          |s.add(dummy == model[dummy])  # make sure dummy has fixed value
          |while True:
          |  ${if (timeout >= Duration.Inf) "" else "t = (time.perf_counter_ns() - start) // 1_000_000"}
          |  ${if (timeout >= Duration.Inf) "" else s"if t >= ${timeout.toMillis}: break"}
          |  s.add(costf < minimum)
          |  ${if (timeout >= Duration.Inf) "" else s"s.set('timeout', ${timeout.toMillis} - t)"}
          |  if s.check() != sat: break
          |  model = s.model()
          |  minimum = model.eval(costf)
          |""".stripMargin
      )
      val epilogue = config.generateScriptEpilogue(problem.vars)

      val script = (preamble ++ context.statements ++ constraints ++ minimization ++ epilogue).mkString("\n")

      // Invoke Z3
      val scriptOutput = config.runZ3Script(script)

      // Parse the script's output
      try config.parseScriptOutput(scriptOutput.trim, problem.vars)
      catch { case ex: Throwable => throw new CannotParseZ3Output(scriptOutput, ex) }
    }
  }
}
