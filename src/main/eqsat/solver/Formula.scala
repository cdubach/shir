package eqsat.solver

import scala.concurrent.duration._

/**
  * A formula in first-order logic.
  */
sealed trait Formula {
  /**
    * The arguments of this formula.
    * @return The arguments of this formula.
    */
  def args: Seq[Formula]

  /**
   * Rebuilds this formula with the given arguments.
   * @param args The arguments to use.
   */
  def build(args: Seq[Formula]): Formula

  /**
    * The variables that occur in this formula.
    * @return The variables that occur in this formula.
    */
  final def vars: Seq[Var] = {
    this match {
      case v: Var => Seq(v)
      case _ => args.flatMap(_.vars).distinct
    }
  }

  /**
    * Adds the given formula to this formula.
    * @param that The formula to add.
    * @return The sum of this formula and the given formula.
    */
  final def +(that: Formula): Formula = Add(this, that)

  /**
    * Multiplies this formula with the given formula.
    * @param that The formula to multiply this formula with.
    * @return The product of this formula and the given formula.
    */
  final def *(that: Formula): Formula = Mul(this, that)

  /**
    * Constructs the minimum of this formula and the given formula.
    * @param that The formula to compare this formula with.
    * @return The minimum of this formula and the given formula.
    */
  final def min(that: Formula): Formula = Min(this, that)

  /**
    * Constructs the maximum of this formula and the given formula.
    * @param that The formula to compare this formula with.
    * @return The maximum of this formula and the given formula.
    */
  final def max(that: Formula): Formula = Max(this, that)

  /**
    * Constructs the negation of this formula.
    * @return The negation of this formula.
    */
  final def unary_! : Formula = Not(this)

  /**
    * Constructs a formula that represents the disjunction of this formula and the given formula.
    * @param that The formula to combine this formula with.
    * @return A formula that represents the disjunction of this formula and the given formula.
    */
  final def ||(that: Formula): Formula = {
    (this, that) match {
      case (Or(leftOperands), Or(rightOperands)) => Or(leftOperands ++ rightOperands)
      case (Or(leftOperands), _) => Or(leftOperands :+ that)
      case (_, Or(rightOperands)) => Or(this +: rightOperands)
      case _ => Or(Seq(this, that))
    }
  }

  /**
    * Constructs a formula that represents the conjunction of this formula and the given formula.
    * @param that The formula to combine this formula with.
    * @return A formula that represents the conjunction of this formula and the given formula.
    */
  final def &&(that: Formula): Formula = {
    (this, that) match {
      case (And(leftOperands), And(rightOperands)) => And(leftOperands ++ rightOperands)
      case (And(leftOperands), _) => And(leftOperands :+ that)
      case (_, And(rightOperands)) => And(this +: rightOperands)
      case _ => And(Seq(this, that))
    }
  }

  /**
    * Constructs a formula that is true if this formula evaluates to the same value as the given formula.
    * @param that The formula to compare this formula with.
    * @return A formula that is true if this formula evaluates to the same value as the given formula.
    */
  final def ==(that: Formula): Formula = Equals(this, that)

  /**
    * Constructs a formula that is true if this formula evaluates to a value that is less than the value of the given formula.
    * @param that The formula to compare this formula with.
    * @return A formula that is true if this formula evaluates to a value that is less than the value of the given formula.
    */
  final def <(that: Formula): Formula = LessThan(this, that)

  /**
    * Constructs a formula that is true if this formula evaluates to a value that is less than or equal to the value of the given formula.
    * @param that The formula to compare this formula with.
    * @return A formula that is true if this formula evaluates to a value that is less than or equal to the value of the given formula.
    */
  final def <=(that: Formula): Formula = LessThanOrEqual(this, that)

  /**
   * Constructs an equivalent, simplified formula.
   * @return An equivalent, simplified formula.
   */
  def simplify: Formula = build(args.map(_.simplify))

  /**
   * Attempts to evaluate this formula to an Integer constant.
   * @return The value of this formula if it is an integer constant, `None` otherwise.
   */
  final def tryEvalInt: Option[BigInt] = simplify match {
    case IntConstant(i) => Some(i)
    case _ => None
  }

  /**
   * Attempts to evaluate this formula to an Integer constant.
   * @return The value of this formula if it is an integer constant, `None` otherwise.
   */
  final def evalInt: BigInt  = tryEvalInt.get
}

/**
  * A variable in a formula.
  */
sealed trait Var extends Formula {
  override def args: Seq[Formula] = Seq.empty

  override def build(args: Seq[Formula]): Formula = this

  /**
    * The name of the variable.
    */
  val name: String
}

/**
  * A Boolean variable.
  * @param name The name of the variable.
  */
final case class BooleanVar(name: String) extends Var

/**
  * An integer variable.
  * @param name The name of the variable.
  */
final case class IntVar(name: String) extends Var

/**
  * A constant in a formula.
  */
sealed trait Constant extends Formula {
  override def args: Seq[Formula] = Seq.empty

  override def build(args: Seq[Formula]): Formula = this
}

/**
  * An integer constant.
  * @param value The value of the constant.
  */
final case class IntConstant(value: BigInt) extends Constant

/**
  * A Boolean constant.
  * @param value The value of the constant.
  */
final case class BooleanConstant(value: Boolean) extends Constant

/**
  * A unary operation.
  */
sealed trait UnaryOp extends Formula {
  /**
    * The operand of the operation.
    */
  val operand: Formula

  override def args: Seq[Formula] = Seq(operand)
}

/**
  * A Boolean negation.
  * @param operand The formula to negate.
  */
final case class Not(operand: Formula) extends UnaryOp {
  override def build(args: Seq[Formula]): Formula = Not(args.head)

  override def simplify: Formula = operand.simplify match {
    case Not(inner) => inner
    case BooleanConstant(value) => BooleanConstant(!value)
    case operand => Not(operand)
  }
}

/**
 * An integer/floating-point additive inverse.
 * @param operand The formula to negate.
 */
final case class Neg(operand: Formula) extends UnaryOp {
  override def build(args: Seq[Formula]): Formula = Neg(args.head)

  override def simplify: Formula = operand.simplify match {
    case IntConstant(i) => IntConstant(-i)
    case Neg(inner) => inner
    case operand => Neg(operand)
  }
}

/**
  * A binary operation.
  */
sealed trait BinaryOp extends Formula {
  /**
    * The left operand of the operation.
    */
  val left: Formula

  /**
    * The right operand of the operation.
    */
  val right: Formula

  override def args: Seq[Formula] = Seq(left, right)
}

/**
  * An addition.
  * @param left The left operand.
  * @param right The right operand.
  */
final case class Add(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = Add(args(0), args(1))

  override def simplify: Formula = (left.simplify, right.simplify) match {
    case (IntConstant(i), IntConstant(j)) => IntConstant(i + j)
    case (IntConstant(i), operand) if i == 0 => operand
    case (operand, IntConstant(j)) if j == 0 => operand
    case (operand, IntConstant(i)) if i < 0 => Sub(operand, IntConstant(-i))
    case (IntConstant(i), operand) if i < 0 => Sub(operand, IntConstant(-i))
    case (operand1, operand2) => Add(operand1, operand2)
  }
}

/**
  * A subtraction.
  * @param left The left operand.
  * @param right The right operand.
  */
final case class Sub(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = Sub(args(0), args(1))

  override def simplify: Formula = (left.simplify, right.simplify) match {
    case (IntConstant(i), IntConstant(j)) => IntConstant(i - j)
    case (IntConstant(i), operand) if i == 0 => Neg(operand)
    case (operand, IntConstant(j)) if j == 0 => operand
    case (operand, IntConstant(i)) if i < 0 => Add(operand, IntConstant(-i))
    case (IntConstant(i), operand) if i < 0 => Neg(Add(operand, IntConstant(-i)))
    case (operand1, operand2) => Sub(operand1, operand2)
  }
}

/**
  * A multiplication.
  * @param left The left operand.
  * @param right The right operand.
  */
final case class Mul(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = Mul(args(0), args(1))

  override def simplify: Formula = (left.simplify, right.simplify) match {
    case (IntConstant(i), IntConstant(j)) => IntConstant(i * j)
    case (IntConstant(i), _) if i == 0 => IntConstant(0)
    case (_, IntConstant(j)) if j == 0 => IntConstant(0)
    case (IntConstant(i), operand) if i == 1 => operand
    case (operand, IntConstant(j)) if j == 1 => operand
    case (IntConstant(i), operand) if i == -1 => Neg(operand)
    case (operand, IntConstant(j)) if j == -1 => Neg(operand)
    case (IntConstant(i), Mul(IntConstant(j), operand)) => Mul(IntConstant(i * j), operand)
    case (Mul(IntConstant(i), operand), IntConstant(j)) => Mul(IntConstant(i * j), operand)
    case (IntConstant(i), Mul(operand, IntConstant(j))) => Mul(IntConstant(i * j), operand)
    case (Mul(operand, IntConstant(i)), IntConstant(j)) => Mul(IntConstant(i * j), operand)
    case (operand1, operand2) => Mul(operand1, operand2)
  }
}

/**
 * A division.
 * @param left The left operand.
 * @param right The right operand.
 */
final case class Div(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = Div(args(0), args(1))
}

/**
 * A remainder.
 * @param left The left operand.
 * @param right The right operand.
 */
final case class Rem(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = Rem(args(0), args(1))
}

/**
  * An exponent.
  * @param left The left operand.
  * @param right The right operand.
  */
final case class Pow(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = Pow(args(0), args(1))
}

/**
  * A formula that represents an equality relation.
  * @param left The left operand.
  * @param right The right operand.
  */
final case class Equals(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = Equals(args(0), args(1))

  override def simplify: Formula = (left.simplify, right.simplify) match {
    case (BooleanConstant(i), BooleanConstant(j)) => BooleanConstant(i == j)
    case (IntConstant(i), IntConstant(j)) => BooleanConstant(i == j)
    case (operand1, operand2) => Equals(operand1, operand2)
  }
}

/**
  * A formula that represents a less-than relation.
  * @param left The left operand.
  * @param right The right operand.
  */
final case class LessThan(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = LessThan(args(0), args(1))

  override def simplify: Formula = (left.simplify, right.simplify) match {
    case (IntConstant(i), IntConstant(j)) => BooleanConstant(i < j)
    case (operand1, operand2) => LessThan(operand1, operand2)
  }
}

/**
  * A formula that represents a less-than-or-equal relation.
  * @param left The left operand.
  * @param right The right operand.
  */
final case class LessThanOrEqual(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = LessThanOrEqual(args(0), args(1))

  override def simplify: Formula = (left.simplify, right.simplify) match {
    case (IntConstant(i), IntConstant(j)) => BooleanConstant(i <= j)
    case (operand1, operand2) => LessThanOrEqual(operand1, operand2)
  }
}

/**
  * A formula that represents an implication.
  * @param left The left operand.
  * @param right The right operand.
  */
final case class Implies(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = Implies(args(0), args(1))

  override def simplify: Formula = (left.simplify, right.simplify) match {
    case (BooleanConstant(i), BooleanConstant(j)) => BooleanConstant(!i || j)
    case (operand1, operand2) => Implies(operand1, operand2)
  }
}

/**
  * A formula that represents the maximum of two formulas.
  * @param left The left operand.
  * @param right The right operand.
  */
final case class Min(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = Min(args(0), args(1))

  override def simplify: Formula = (left.simplify, right.simplify) match {
    case (IntConstant(i), IntConstant(j)) => IntConstant(i min j)
    case (operand1, operand2) => Min(operand1, operand2)
  }
}

/**
  * A formula that represents the minimum of two formulas.
  * @param left The left operand.
  * @param right The right operand.
  */
final case class Max(left: Formula, right: Formula) extends BinaryOp {
  override def build(args: Seq[Formula]): Formula = Max(args(0), args(1))

  override def simplify: Formula = (left.simplify, right.simplify) match {
    case (IntConstant(i), IntConstant(j)) => IntConstant(i max j)
    case (operand1, operand2) => Max(operand1, operand2)
  }
}

/**
  * A formula that represents a disjunction.
  * @param operands The operands of the disjunction.
  */
final case class Or(operands: Seq[Formula]) extends Formula {
  override def args: Seq[Formula] = operands

  override def build(args: Seq[Formula]): Formula = Or(args)

  override def simplify: Formula = {
    val simplified = operands.map(_.simplify)
    val trues = simplified.collect { case BooleanConstant(true) => true }
    if (trues.nonEmpty) {
      BooleanConstant(true)
    } else {
      val remaining = simplified.filterNot(_ == BooleanConstant(false))
      if (remaining.isEmpty) {
        BooleanConstant(false)
      } else if (remaining.size == 1) {
        remaining.head
      } else {
        Or(remaining)
      }
    }
  }
}

/**
  * A formula that represents a conjunction.
  * @param operands The operands of the conjunction.
  */
final case class And(operands: Seq[Formula]) extends Formula {
  override def args: Seq[Formula] = operands

  override def build(args: Seq[Formula]): Formula = And(args)

  override def simplify: Formula = {
    val simplified = operands.map(_.simplify)
    val falses = simplified.collect { case BooleanConstant(false) => false }
    if (falses.nonEmpty) {
      BooleanConstant(false)
    } else {
      val remaining = simplified.filterNot(_ == BooleanConstant(true))
      if (remaining.isEmpty) {
        BooleanConstant(true)
      } else if (remaining.size == 1) {
        remaining.head
      } else {
        And(remaining)
      }
    }
  }
}

/**
  * A pseudo-boolean constraint that enforces a relation on the sum of the given formulas.
  */
trait PseudoBooleanSum extends Formula {
  /**
    * The formulas to sum up.
    */
  val operands: Seq[(Formula, BigInt)]

  /**
    * The sum. The relation enforced by the constraint is defined by the type of the constraint.
    */
  val sum: BigInt

  override def args: Seq[Formula] = operands.map { case (lhs, _) => lhs }
}

/**
  * A pseudo-boolean constraint that enforces that the sum of the given formulas is greater than or equal to the given minimum.
  * @param operands The formulas to sum up and their coefficients.
  * @param sum The minimum sum.
  */
final case class PseudoBooleanGreaterThanOrEqual(operands: Seq[(Formula, BigInt)], sum: BigInt) extends PseudoBooleanSum {
  override def build(args: Seq[Formula]): Formula =
    PseudoBooleanGreaterThanOrEqual(args.zip(operands.map(_._2)), sum)
}

/**
  * A pseudo-boolean constraint that enforces that the sum of the given formulas is less than or equal to the given maximum.
  * @param operands The formulas to sum up.
  * @param sum The maximum sum.
  */
final case class PseudoBooleanLessThanOrEqual(operands: Seq[(Formula, BigInt)], sum: BigInt) extends PseudoBooleanSum {
  override def build(args: Seq[Formula]): Formula =
    PseudoBooleanLessThanOrEqual(args.zip(operands.map(_._2)), sum)
}

/**
  * A pseudo-boolean constraint that enforces that the sum of the given formulas is equal to the given sum.
  * @param operands The formulas to sum up.
  * @param sum The sum.
  */
final case class PseudoBooleanEquals(operands: Seq[(Formula, BigInt)], sum: BigInt) extends PseudoBooleanSum {
  override def build(args: Seq[Formula]): Formula =
    PseudoBooleanEquals(args.zip(operands.map(_._2)), sum)
}

/**
  * A helper object to create a pseudo-boolean constraint that enforces that at least `n` of the given formulas are true.
  */
object AtLeastN {
  /**
    * Creates a pseudo-boolean constraint that enforces that at least `n` of the given formulas are true.
    * @param n The minimum number of formulas that must be true.
    * @param operands The formulas to enforce the constraint on.
    * @return The pseudo-boolean constraint.
    */
  def apply(n: Int, operands: Seq[Formula]): Formula = {
    val pairs = operands.map((_, BigInt(1)))
    PseudoBooleanGreaterThanOrEqual(pairs, n)
  }
}

/**
  * A helper object to create a pseudo-boolean constraint that enforces that at least one of the given formulas is true.
  */
object AtLeastOne {
  /**
    * Creates a pseudo-boolean constraint that enforces that at least one of the given formulas is true.
    * @param operands The formulas to enforce the constraint on.
    * @return The pseudo-boolean constraint.
    */
  def apply(operands: Seq[Formula]): Formula = AtLeastN(1, operands)
}

/**
  * A helper object to create a pseudo-boolean constraint that enforces that at most `n` of the given formulas are true.
  */
object AtMostN {
  /**
    * Creates a pseudo-boolean constraint that enforces that at most `n` of the given formulas are true.
    * @param n The maximum number of formulas that must be true.
    * @param operands The formulas to enforce the constraint on.
    * @return The pseudo-boolean constraint.
    */
  def apply(n: Int, operands: Seq[Formula]): Formula = {
    val pairs = operands.map((_, BigInt(1)))
    PseudoBooleanLessThanOrEqual(pairs, n)
  }
}

/**
  * A helper object to create a pseudo-boolean constraint that enforces that at most one of the given formulas is true.
  */
object AtMostOne {
  /**
    * Creates a pseudo-boolean constraint that enforces that at most one of the given formulas is true.
    * @param operands The formulas to enforce the constraint on.
    * @return The pseudo-boolean constraint.
    */
  def apply(operands: Seq[Formula]): Formula = AtMostN(1, operands)
}

/**
  * A helper object to create a pseudo-boolean constraint that enforces that exactly `n` of the given formulas are true.
  */
object ExactlyN {
  def apply(n: Int, operands: Seq[Formula]): Formula = {
    val pairs = operands.map((_, BigInt(1)))
    PseudoBooleanEquals(pairs, n)
  }
}

/**
  * A helper object to create a pseudo-boolean constraint that enforces that exactly one of the given formulas is true.
  */
object ExactlyOne {
  /**
    * Creates a pseudo-boolean constraint that enforces that exactly one of the given formulas is true.
    * @param operands The formulas to enforce the constraint on.
    * @return The pseudo-boolean constraint.
    */
  def apply(operands: Seq[Formula]): Formula = ExactlyN(1, operands)
}

/**
  * An if-then-else formula.
  * @param condition The condition.
  * @param ifTrue The formula to evaluate if the condition is true.
  * @param ifFalse The formula to evaluate if the condition is false.
  */
final case class If(condition: Formula, ifTrue: Formula, ifFalse: Formula) extends Formula {
  override def args: Seq[Formula] = Seq(condition, ifTrue, ifFalse)

  override def build(args: Seq[Formula]): Formula = If(args(0), args(1), args(2))
}

/**
 * A satisfiability problem.
 * @param constraints The constraints that must be satisfied.
 */
final case class SatisfiabilityProblem(constraints: Seq[Formula]) {
  /**
   * The variables that occur in the problem.
   */
  def vars: Seq[Var] = constraints.flatMap(_.vars).distinct
}

/**
  * A minimization problem.
  * @param constraints The constraints that must be satisfied.
  * @param cost The cost to minimize.
  */
final case class MinimizationProblem(constraints: Seq[Formula], cost: Formula) {
  /**
    * The variables that occur in the problem.
    */
  def vars: Seq[Var] = (cost.vars ++ constraints.flatMap(_.vars)).distinct
}

/**
  * A solver that checks the satisfiability of formulas.
  */
trait SatisfiabilityChecker {

  /**
   * Checks if the given problem is satisfiable.
   *
   * @param problem The satisfiability problem to solve.
   * @return A mapping of variables to constants if the formula is satisfiable, `None` if the formula is unsatisfiable.
   */
  def solve(problem: SatisfiabilityProblem): Option[Map[Var, Constant]] =
    solve(problem, Duration.Inf)

  /**
    * Checks if the given problem is satisfiable.
    * @param problem The satisfiability problem to solve.
    * @param timeout A hint for maximum time spent on solving.
    * @return A mapping of variables to constants if the formula is satisfiable, `None` if the formula is unsatisfiable or timed-out.
    */
  def solve(problem: SatisfiabilityProblem, timeout: Duration): Option[Map[Var, Constant]]
}

/**
  * An optimizer that minimizes a cost function subject to constraints.
  */
trait Optimizer {

  /**
   * Solves the given minimization problem.
   * This should behave the same as supplying infinite timeout.
   *
   * @param problem The minimization problem to solve.
   * @return A map from variables to their values that minimizes the cost function subject to the constraints.
   */
  def solve(problem: MinimizationProblem): Option[Map[Var, Constant]] =
    solve(problem, Duration.Inf)

  /**
    * Solves the given minimization problem.
    * @param problem The minimization problem to solve.
    * @param timeout A hint for maximum time spent on optimizing.
    * @return A map from variables to their values that minimizes the cost function subject to the constraints.
    */
  def solve(problem: MinimizationProblem, timeout: Duration): Option[Map[Var, Constant]]
}
