package eqsat.solver

import core.Expr
import eqsat.{ENodeT, OutliningExtractorCostModel}

/**
 * An e-node in an e-graph, along with all (recursive) argument e-classes that will always be extracted if the e-node
 * is selected for extraction.
 *
 * @param node The e-node at which this agglomerated node is rooted.
 * @param children The agglomerated node's children, represented as either smaller `AgglomeratedNode` instances (if there is no
 *                 possibility for choice) or `AgglomeratedClass` instances (if there can be choice).
 */
final case class AgglomeratedNode(node: ENodeT, children: Seq[Either[AgglomeratedNode, AgglomeratedClass]]) {
  /**
   * The set of agglomerated classes on which this agglomerated node depends.
   */
  val args: Seq[AgglomeratedClass] = {
    children.flatMap({
      case Left(n) => n.args
      case Right(c) => Seq(c)
    }).distinct
  }

  private def instantiate(argMap: Map[AgglomeratedClass, Expr]): Expr = {
    node.instantiate(children.map({
      case Left(n) => n.instantiate(argMap)
      case Right(c) => argMap(c)
    }))
  }

  def instantiate(args: Seq[Expr]): Expr = {
    instantiate(this.args.zip(args).toMap)
  }

  private def useCost(costModel: OutliningExtractorCostModel, dimension: Int, argCostMap: Map[AgglomeratedClass, Formula]): Formula = {
    costModel.useCost(dimension)(node, children.map({
      case Left(n) => n.useCost(costModel, dimension, argCostMap)
      case Right(c) => argCostMap(c)
    }))
  }

  def useCost(costModel: OutliningExtractorCostModel, dimension: Int, argCosts: Seq[Formula]): Formula = {
    useCost(costModel, dimension, args.zip(argCosts).toMap)
  }

  private def callCost(costModel: OutliningExtractorCostModel, dimension: Int, argCostMap: Map[AgglomeratedClass, Formula]): Formula = {
    costModel.callCost(dimension)(node, children.map({
      case Left(n) => n.useCost(costModel, dimension, argCostMap)
      case Right(c) => argCostMap(c)
    }))
  }

  def callCost(costModel: OutliningExtractorCostModel, dimension: Int, argCosts: Seq[Formula]): Formula = {
    callCost(costModel, dimension, args.zip(argCosts).toMap)
  }
}
