package eqsat.solver

import eqsat.{DeBruijnParamUse, EClassT}

import scala.collection.mutable

/**
 * An agglomerated e-class in an e-graph. This data structure is the e-class analogue to `AgglomeratedNode`.
 */
final class AgglomeratedClass(val eClass: EClassT) {
  /**
    * The set of agglomerated nodes in this e-class.
    */
  val nodes = new mutable.ArrayBuffer[AgglomeratedNode]()

  /**
    * Checks if this e-class contains a parameter.
    * @return `true` if this e-class contains a parameter.
    */
  def isParam: Boolean = nodes.exists(_.node.expr.isInstanceOf[DeBruijnParamUse])

  private def findReachable(results: mutable.Set[AgglomeratedClass]): Unit = {
    if (results.add(this)) {
      for (c <- directlyReachable) {
        c.findReachable(results)
      }
    }
  }

  /**
    * Find all e-classes strictly reachable from this e-class.
    * @return The set of all e-classes strictly reachable from this e-class.
    */
  def strictlyReachable: Seq[AgglomeratedClass] = {
    val results = mutable.LinkedHashSet[AgglomeratedClass]()
    for (c <- directlyReachable) {
      c.findReachable(results)
    }
    results.toSeq
  }

  /**
    * Find all e-classes reachable from this e-class.
    * @return The set of all e-classes reachable from this e-class.
    */
  def reachable: Seq[AgglomeratedClass] = {
    val results = mutable.LinkedHashSet[AgglomeratedClass]()
    findReachable(results)
    results.toSeq
  }

  /**
    * The set of e-classes that are directly reachable from this e-class through its outgoing edges.
    */
  def directlyReachable: Seq[AgglomeratedClass] = {
    val results = mutable.LinkedHashSet[AgglomeratedClass]()
    for (node <- nodes) {
      for (arg <- node.args) {
        results.add(arg)
      }
    }
    results.toSeq
  }

  /**
    * The set of strongly connected components in the graph defined by the classes reachable from this class.
    * @return The set of strongly connected components.
    */
  def stronglyConnectedComponents: Seq[Seq[AgglomeratedClass]] = {
    tarjan(reachable)
  }

  private def tarjan(classes: Seq[AgglomeratedClass]): Seq[Seq[AgglomeratedClass]] = {
    val stack = new mutable.Stack[AgglomeratedClass]()
    val index = mutable.Map[AgglomeratedClass, Int]()
    val lowLink = mutable.Map[AgglomeratedClass, Int]()
    val onStack = mutable.Set[AgglomeratedClass]()
    val sccs = new mutable.ArrayBuffer[Seq[AgglomeratedClass]]()

    var idx = 0

    def strongConnect(v: AgglomeratedClass): Unit = {
      index(v) = idx
      lowLink(v) = idx
      idx += 1
      stack.push(v)
      onStack.add(v)

      for (w <- v.directlyReachable) {
        if (!index.contains(w)) {
          strongConnect(w)
          lowLink(v) = math.min(lowLink(v), lowLink(w))
        } else if (onStack.contains(w)) {
          lowLink(v) = math.min(lowLink(v), index(w))
        }
      }

      if (lowLink(v) == index(v)) {
        val scc = new mutable.ArrayBuffer[AgglomeratedClass]()
        var w = stack.pop()
        onStack.remove(w)
        scc += w
        while (w != v) {
          w = stack.pop()
          onStack.remove(w)
          scc += w
        }
        sccs += scc
      }
    }

    for (v <- classes) {
      if (!index.contains(v)) {
        strongConnect(v)
      }
    }

    sccs
  }
}
