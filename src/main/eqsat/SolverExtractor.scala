package eqsat
import cGen.CLambda
import core.{Expr, FunctionCall, LambdaT, ParamDef}
import eqsat.solver._

import scala.collection.mutable

/**
  * A strategy for outlining.
  */
trait OutliningStrategy {
  /**
    * Determines the constraints to impose on the outlining variable.
    * @param outliningVar The outlining variable.
    * @param usersSelectionVars The selection variables of all users of the variable to outline.
    * @return The constraints to impose on the outlining variable.
    */
  def constraints(outliningVar: solver.BooleanVar,
                  usersSelectionVars: Seq[solver.Formula]): Seq[solver.Formula]

  /**
   * Determines the global constraints to impose on the outlining variables.
   * @param outliningVars The outlining variables.
   * @return The global constraints to impose on the outlining variables.
   */
  def globalConstraints(outliningVars: Seq[solver.BooleanVar]): Seq[solver.Formula] = Seq()
}

/**
 * An outlining strategy that never outlines any e-node.
 */
object NeverOutline extends OutliningStrategy {
  override def constraints(outliningVar: solver.BooleanVar,
                           usersSelectionVars: Seq[solver.Formula]): Seq[solver.Formula] = {
    Seq(outliningVar == solver.BooleanConstant(false))
  }
}

/**
 * An outlining strategy that outlines every e-node if and only if it is used more than once.
 */
object OutlineIfUsedMoreThanOnce extends OutliningStrategy {
  override def constraints(outliningVar: solver.BooleanVar,
                           usersSelectionVars: Seq[solver.Formula]): Seq[solver.Formula] = {
    Seq(outliningVar == solver.BooleanConstant(true))
  }
}

/**
 * An outlining strategy that leaves outlining as a variable for the solver to decide.
 */
object VariableOutlining extends OutliningStrategy {
  override def constraints(outliningVar: solver.BooleanVar,
                           usersSelectionVars: Seq[solver.Formula]): Seq[solver.Formula] = {
    Seq()
  }
}

/**
 * An outlining strategy that is like `VariableOutlining` but imposes a limit on the number of outlined functions.
 * @param maxOutlinedFunctions The maximum number of outlined functions.
 */
final case class LimitedOutlining(maxOutlinedFunctions: Int) extends OutliningStrategy {
  override def constraints(outliningVar: solver.BooleanVar,
                           usersSelectionVars: Seq[solver.Formula]): Seq[solver.Formula] = {
    Seq()
  }

  override def globalConstraints(outliningVars: Seq[BooleanVar]): Seq[Formula] = {
    Seq(AtMostN(maxOutlinedFunctions, outliningVars))
  }
}

/**
  * A description of the optimizations that a solver-based extractor can perform.
  * @param enableAgglomeration Tells the extractor whether it is allowed to agglomerate e-classes, thereby potentially
  *                            greatly reducing the number of variables and constraints fed to the solver.
  * @param includeAtMostOneENodeConstraint Tells the extractor whether it should include a constraint that ensures that
  *                                        at most one e-node is selected for extraction.
  * @param optimizeAcyclicConstraints Tells the extractor whether it should optimize the constraints that impose
  *                                   acyclicity on graphs. This optimization proceeds by partitioning the graph into
  *                                   strongly-connected components. Constraints are then imposed that ensure that the
  *                                   components are acyclic.
  */
final case class SolverExtractorOptimizations(enableAgglomeration: Boolean = true,
                                              includeAtMostOneENodeConstraint: Boolean = true,
                                              optimizeAcyclicConstraints: Boolean = true)

/**
  * An extractor implementation that relies on a solver to make a global selection of which e-nodes are optimal to
  * extract. This extractor will opportunistically perform outlining.
  *
  * Because of the global judgment this extractor makes, the extractor is particularly suitable for code size and
  * resource-usage optimization.
  *
  * @param costModel A cost model that determines the cost of each operation in the e-graph.
  * @param isEligible A predicate that determines if an e-node is an eligible candidate for extraction. Ineligible
  *                   e-nodes will never show up in the extractor's node selection and its extracted expressions.
  * @param solverInterface An interface to an external solver.
  * @param optimizations A description of the optimizations that the extractor can perform.
  * @param outliningStrategy The strategy to use for outlining.
  *
  */
final case class SolverExtractor(costModel: OutliningExtractorCostModel,
                                 isEligible: ENodeT => Boolean = _ => true,
                                 solverInterface: solver.Optimizer = solver.Z3Py(),
                                 optimizations: SolverExtractorOptimizations = SolverExtractorOptimizations(),
                                 outliningStrategy: OutliningStrategy = VariableOutlining)
                                (implicit override val buildLambda: (ParamDef, Expr) => LambdaT = (p, body) => CLambda(p, body),
                                 override val buildFunctionCall: (Expr, Expr) => Expr = FunctionCall(_, _)) extends OutliningExtractor {

  private def imposeAcyclicityConstraint(root: AgglomeratedClass,
                                         reachable: Seq[AgglomeratedClass],
                                         nodeSelectionVars: mutable.LinkedHashMap[(AgglomeratedClass, AgglomeratedNode), solver.BooleanVar]): Seq[solver.Formula] = {
    val components = if (optimizations.optimizeAcyclicConstraints) {
      root.stronglyConnectedComponents
    } else {
      Seq(reachable)
    }

    val classIndices = reachable.zipWithIndex.toMap
    val nodeSelectionVarsPerClass = nodeSelectionVars.groupBy(_._1._1).mapValues(m => m.map(p => (p._1._2, p._2)))

    val constraints = mutable.ArrayBuffer[solver.Formula]()
    for (component <- components) {
      if (component.length > 1) {
        val classLabelVars = mutable.Map[AgglomeratedClass, solver.Formula]()
        for (c <- component) {
          classLabelVars(c) = solver.IntVar(s"label_c${classIndices(c)}")
        }

        for (c <- component) {
          for ((node, selectionVar) <- nodeSelectionVarsPerClass(c)) {
            for (arg <- node.args) {
              if (component.contains(arg)) {
                val argLabel = classLabelVars(arg)
                val nodeLabel = classLabelVars(c)
                constraints.append(solver.Implies(selectionVar, nodeLabel < argLabel))
              }
            }
          }
        }
      }
    }
    constraints
  }

  /**
    * Formulates and solves the extraction/outlining optimization problem for a given agglomerated class.
    * @param root The root agglomerated class.
    * @return The solution to the optimization problem.
    */
  def solve(root: AgglomeratedClass): Option[AgglomeratedSolution] = {
    val reachableClasses = root.reachable

    val costDims = Seq.range(0, costModel.dimensions)

    // Associate each class with two to four variables
    //   * select_cN determines if the class is selected or not
    //
    //   * use_cost_cN_dK is the cost associated with a single use of the class. K represents the Kth cost dimension
    //
    //   * base_cost_cN_dK (optional) is the cost associated with selecting the class. This cost is nonzero only if the
    //     e-class is outlined or if it is the root e-class. This cost is only associated with a variable when there is
    //     a possibility of it being nonzero. K represents the Kth cost dimension
    //
    //   * outline_cN (optional) determines if the class is outlined or not. It is only generated for classes that have
    //     an in-degree greater than one (meaning multiple eligible nodes take the class as an argument)
    val classSelectionVars = mutable.Map[AgglomeratedClass, solver.BooleanVar]()
    val classUseCost = mutable.Map[(AgglomeratedClass, Int), solver.IntVar]()
    val classBaseCost = mutable.Map[(AgglomeratedClass, Int), solver.Formula]()
    val classOutliningVars = mutable.Map[AgglomeratedClass, solver.BooleanVar]()

    val inDegrees = countInDegrees(reachableClasses)
    for ((c, index) <- reachableClasses.zipWithIndex) {
      classSelectionVars(c) = solver.BooleanVar(s"select_c$index")

      val considerOutlining = inDegrees.getOrElse(c, 0) > 1 && c != root && !c.isParam
      if (considerOutlining) {
        classOutliningVars(c) = solver.BooleanVar(s"outline_c$index")
      }

      for (costDim <- costDims) {
        classUseCost((c, costDim)) = solver.IntVar(s"use_cost_c${index}_d$costDim")
        if (considerOutlining) {
          classBaseCost((c, costDim)) = solver.IntVar(s"base_cost_c${index}_d$costDim")
        }
      }
    }

    // Prepare an array buffer to store constraints in
    val constraints = mutable.ArrayBuffer[solver.Formula]()

    // Add global outlining constraints
    constraints.appendAll(outliningStrategy.globalConstraints(classOutliningVars.values.toSeq))

    // Associate each node with a Boolean variable that determines if the node is selected or not
    val nodeSelectionVars = mutable.LinkedHashMap[(AgglomeratedClass, AgglomeratedNode), solver.BooleanVar]()

    // Encode constraints for each class
    for (c <- reachableClasses) {
      val nodeSelectsAndCosts = mutable.ArrayBuffer[(solver.BooleanVar, Seq[solver.Formula], Seq[solver.Formula])]()
      for (node <- c.nodes) {
        val selectNode = solver.BooleanVar(s"select_n${nodeSelectionVars.size}")
        nodeSelectionVars((c, node)) = selectNode

        // Compute the use and call cost of the node
        val useCosts = costDims.map(costDim =>
          node.useCost(costModel, costDim, node.args.map(a => classUseCost((a, costDim)))))
        val callCosts = costDims.map(costDim =>
          node.callCost(costModel, costDim, node.args.map(a => classUseCost((a, costDim)))))
        nodeSelectsAndCosts.append((selectNode, useCosts, callCosts))

        if (node.args.nonEmpty) {
          // Add a constraint that specifies that if a node is selected, its arguments must also be selected
          constraints.append(solver.Implies(selectNode, node.args.map(classSelectionVars(_)).reduce[solver.Formula](_ && _)))
        }
      }

      // Create cost constraints for the total class
      for (costDim <- costDims) {
        // Set the total costs of the class to the sum of the costs of its selected nodes
        val cUseCost = nodeSelectsAndCosts.map(x => x._1 * x._2(costDim)).reduce(_ + _)
        val cCallCost = nodeSelectsAndCosts.map(x => x._1 * x._3(costDim)).reduce(_ + _)

        // If an e-class is being considered for outlining, then its use cost and base cost depend on whether it is
        // outlined or not
        if (classOutliningVars.contains(c)) {
          val outlineOrNot = classOutliningVars(c)
          constraints.append(If(
            outlineOrNot,
            classBaseCost(c, costDim) == cUseCost && classUseCost(c, costDim) == cCallCost,
            classUseCost(c, costDim) == cUseCost && classBaseCost(c, costDim) == solver.IntConstant(0)))
        } else {
          constraints.append(classUseCost(c, costDim) == cUseCost)
        }
      }

      // Add a constraint that requires any selected class to have at least one selected node
      constraints.append(classSelectionVars(c) == nodeSelectsAndCosts.map(_._1).reduce[solver.Formula](_ || _))

      if (optimizations.includeAtMostOneENodeConstraint && nodeSelectsAndCosts.length > 1) {
        // Add a constraint that requires at most one node to be selected
        constraints.append(AtMostOne(nodeSelectsAndCosts.map(_._1)))
      }
    }

    // Apply outlining strategy. First collect the selection variables of all users of each node, then apply the
    // outlining strategy to each class
    val classToUsers = nodeSelectionVars.flatMap(pair => pair._1._2.args.map(a => (a, pair._2)))
      .groupBy(_._1)
      .mapValues(_.values.toSeq.distinct)

    for (c <- reachableClasses) {
      classOutliningVars.get(c) match {
        case Some(outliningVar) =>
          constraints.appendAll(outliningStrategy.constraints(outliningVar, classToUsers(c)).map(Implies(classSelectionVars(c), _)))
        case None =>
      }
    }

    // Impose acyclicity constraints
    val acyclicityConstraints = imposeAcyclicityConstraint(root, reachableClasses, nodeSelectionVars)
    constraints.appendAll(acyclicityConstraints)

    // The optimization problem consists of the assertion that the root class is selected and the objective of
    // minimizing the total cost
    constraints.append(classSelectionVars(root) == solver.BooleanConstant(true))

    val totalCostVars = costDims.map(i => solver.IntVar(s"total_cost_d$i"))
    for (costDim <- costDims) {
      val totalCostForDim = costModel.totalCost(costDim)(
        classUseCost(root, costDim),
        classOutliningVars.keys.map(classBaseCost(_, costDim)).toSeq)

      constraints.append(totalCostVars(costDim) == totalCostForDim)
      costModel.budget(costDim) match {
        case Some(budget) => constraints.append(totalCostVars(costDim) <= budget)
        case None =>
      }
    }

    val minimizationTarget = costModel.minimizationTarget(totalCostVars)
    solverInterface.solve(MinimizationProblem(constraints, minimizationTarget)).map { solution =>
      // Use the optimization problem's solution to determine which nodes we pick
      val nodeSelections = nodeSelectionVars.collect({
        case ((c, n), v) if solution(v).asInstanceOf[solver.BooleanConstant].value => (c, n)
      }).toMap

      val classOutlining = reachableClasses
        .map(c => {
          val nominatedForOutlining = classOutliningVars.get(c) match {
            case Some(v) => solution(v).asInstanceOf[solver.BooleanConstant].value
            case None => false
          }
          (c, nominatedForOutlining)
        })
        .toMap

      // Collect the costs of the solution
      val costs = totalCostVars.map(v => solution(v).asInstanceOf[solver.IntConstant].value)

      AgglomeratedSolution(nodeSelections, classOutlining, costs)
    }
  }

  /**
   * Determines whether e-class agglomeration is enabled.
   *
   * @return `true` if e-class agglomeration is enabled; otherwise, `false`.
   */
  override protected def enableAgglomeration: Boolean = optimizations.enableAgglomeration
}

object SolverExtractor {
  /**
   * Creates a simple solver-based extractor.
   * @param costFunction A cost function that determines the cost of each operation in the e-graph.
   * @param isEligible A predicate that determines if an e-node is an eligible candidate for extraction. Ineligible
   *                   e-nodes will never show up in the extractor's node selection and its extracted expressions.
   */
  def simple(costFunction: Expr => BigInt,
             isEligible: ENodeT => Boolean = _ => true,
             callCost: BigInt = 5)
            (implicit buildLambda: (ParamDef, Expr) => LambdaT = (p, body) => CLambda(p, body),
             buildFunctionCall: (Expr, Expr) => Expr = FunctionCall(_, _)): SolverExtractor = {
    SolverExtractor(SimpleOutliningExtractorCostModel(costFunction, _ => callCost), isEligible)
  }
}
