package eqsat

/**
 * An observer for program equivalence graphs.
 */
trait EGraphObserver {
  /**
   * Notes that a rule has been applied to a graph.
   * @param graph The graph to which `rule` was applied.
   * @param rule The rule that was applied to `graph`.
   * @param duration A measurement of the wall-clock time that elapsed while `rule` was applied to `graph`.
   */
  def onApply[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                 (rule: Rewrite, duration: MeasuredActivity): Unit = ()

  /**
   * Notes that a rule has considered an e-class.
   * @param graph The graph that contains the e-class.
   * @param rule The rule that was considering `eClass`.
   * @param duration A measurement of the wall-clock time elapsed while `eClass` was being considered.
   */
  def onSearch[ENode <: ENodeT, EClass <: EClassT](graph: ReadOnlyEGraph[ENode, EClass])
                                                  (rule: Rewrite, duration: MeasuredActivity): Unit = ()

  /**
   * Notes that the graph is about to perform a step in the Equality Saturation algorithm.
   * @param graph The graph that is about to take a step
   */
  def beforeSaturationStep[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = ()

  /**
   * Notes that the graph has finished saturating.
   * @param graph The graph that has finished saturating.
   * @param fullIteration Whether the graph has completed a full iteration of the saturation algorithm.
   * @param appliedMatchCount The number of matches that were applied to the graph.
   */
  def afterSaturationStep[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass],
                                                              fullIteration: Boolean,
                                                              appliedMatchCount: Int): Unit = ()

  /**
   * Notes that the graph has finished saturating.
   * @param graph The graph that has finished saturating.
   */
  def onFinish[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = ()

  /**
   * Notes that the saturation algorithm has been terminated early.
   * @param graph The graph for which saturation has been terminated early.
   */
  def onAbort[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = ()
}

/**
 * A PEG observer that does nothing.
 */
object EGraphNullObserver extends EGraphObserver

/**
 * A PEG observer that is a composition of other observers.
 * @param observers A sequence of observers of which each element is notified whenever an event occurs.
 */
case class EGraphCompositeObserver(observers: EGraphObserver*) extends EGraphObserver {
  override def onApply[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass])
                                                          (rule: Rewrite, duration: MeasuredActivity): Unit = {
    for (obs <- observers) {
      obs.onApply(graph)(rule, duration)
    }
  }

  override def onSearch[ENode <: ENodeT, EClass <: EClassT](graph: ReadOnlyEGraph[ENode, EClass])
                                                           (rule: Rewrite, duration: MeasuredActivity): Unit = {
    for (obs <- observers) {
      obs.onSearch(graph)(rule, duration)
    }
  }

  override def beforeSaturationStep[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = {
    for (obs <- observers) {
      obs.beforeSaturationStep(graph)
    }
  }

  override def afterSaturationStep[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass],
                                                                       fullIteration: Boolean,
                                                                       appliedMatchCount: Int): Unit = {
    for (obs <- observers) {
      obs.afterSaturationStep(graph, fullIteration, appliedMatchCount)
    }
  }

  override def onFinish[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = {
    for (obs <- observers) {
      obs.onFinish(graph)
    }
  }

  override def onAbort[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = {
    for (obs <- observers) {
      obs.onAbort(graph)
    }
  }
}

/**
 * An applier that triggers a PEG observer.
 * @param observer The observer that is triggered upon rule activation.
 */
case class ObservedRuleApplier(observer: EGraphObserver) extends Applier {
  override def apply[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                        (rule: Rewrite,
                                                         matchInfo: Match[EClass],
                                                         stopwatch: HierarchicalStopwatch): Set[EClass] = {
    val childStopwatch = stopwatch.startChildOrNew(rule.name)

    // Apply the rule.
    val changed = rule.applier.apply(eGraph)(rule, matchInfo, childStopwatch)

    // Notify the observer.
    observer.onApply(eGraph)(rule, childStopwatch.complete())

    // Return the set of changed e-classes.
    changed
  }
}

/**
 * A searcher that triggers a PEG observer.
 * @param rule The rewrite rule that is used.
 */
case class ObservedRuleSearcher(rule: Rewrite, observer: EGraphObserver, batchSearch: Boolean = true) extends Searcher {
  override def searchEClass[ENode <: ENodeT, EClass <: EClassT](eGraph: ReadOnlyEGraph[ENode, EClass])
                                                               (eClass: EClass,
                                                                stopwatch: HierarchicalStopwatch): Seq[Match[EClass]] = {
    val childStopwatch = stopwatch.startChildOrNew(rule.name)

    // Apply the rule.
    val result = rule.searcher.searchEClass(eGraph)(eClass, childStopwatch)

    // Notify the observer.
    observer.onSearch(eGraph)(rule, childStopwatch.complete())

    // Return the set of changed e-classes.
    result
  }

  override def search[ENode <: ENodeT, EClass <: EClassT](eGraph: ReadOnlyEGraph[ENode, EClass],
                                                          createStopwatch: () => HierarchicalStopwatch): Seq[Match[EClass]] = {
    if (!batchSearch) {
      return super.search(eGraph, createStopwatch)
    }

    val childStopwatch = createStopwatch().startChildOrNew(rule.name)

    // Apply the rule.
    val result = rule.searcher.search(eGraph, childStopwatch)

    // Notify the observer.
    observer.onSearch(eGraph)(rule, childStopwatch.complete())

    // Return the set of changed e-classes.
    result
  }
}

/**
 * An observer that triggers a callback whenever a step is taken in the Equality Saturation algorithm.
 */
trait EGraphStepObserver extends EGraphObserver {
  def onStepOrFinish[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit

  override def beforeSaturationStep[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = {
    onStepOrFinish(graph)
  }

  override def onFinish[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = {
    onStepOrFinish(graph)
  }
}
