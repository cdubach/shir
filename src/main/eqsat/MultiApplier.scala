package eqsat

/**
 * An applier that invokes a sequence of other appliers. Works well with `GeneratedApplier` when multiple actions are
 * to be taken following a match.
 * @param appliers The sequence of other appliers to which each match is delegated.
 */
final case class MultiApplier(appliers: Seq[Applier]) extends Applier {

  /**
   * Rewrites an e-class based on an expression and type substitution map.
   *
   * @param eGraph    The graph that contains the e-class to rewrite.
   * @param rule      The rewrite rule that triggered this applier.
   * @param matchInfo The match to rewrite.
   * @param stopwatch A stopwatch that times the application.
   * @return A set of all e-classes that were _directly_ modified as a result of the application.
   */
  override def apply[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass])
                                                        (rule: Rewrite,
                                                         matchInfo: Match[EClass],
                                                         stopwatch: HierarchicalStopwatch): Set[EClass] = {
    appliers.foldLeft(Set[EClass]())((acc, applier) => acc.union(applier(eGraph)(rule, matchInfo, stopwatch)))
  }
}
