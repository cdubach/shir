package eqsat

/**
 * A trait that provides a method to check if a shutdown has been requested for a rewrite engine.
 */
trait EnginePreemptor {
  /**
   * Returns `true` if the engine should shut down, `false` otherwise.
   */
  def shutdownRequested: Boolean
}
