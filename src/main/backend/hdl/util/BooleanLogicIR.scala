package backend.hdl.util

import core.{ArithTypeT, TreeNode}

sealed trait BooleanLogicIR extends TreeNode[BooleanLogicIR] {
  def getInputs: Seq[BooleanInputT] = children.flatMap(_.getInputs)
}

final case class And(children: BooleanLogicIR*) extends BooleanLogicIR

final case class Or(children: BooleanLogicIR*) extends BooleanLogicIR

final case class Xor(children: BooleanLogicIR*) extends BooleanLogicIR

final case class Not(child: BooleanLogicIR) extends BooleanLogicIR {
  override def children: Seq[BooleanLogicIR] = Seq(child)
}

trait BooleanInputT extends BooleanLogicIR {
  override def children: Seq[BooleanLogicIR] = Seq()
  override def getInputs: Seq[BooleanInputT] = Seq(this)
}

final case class BooleanInput() extends BooleanInputT

final case class BooleanInputVector(inputs: ArithTypeT) extends BooleanInputT

final case class True() extends BooleanLogicIR {
  override def children: Seq[BooleanLogicIR] = Seq()
}
final case class False() extends BooleanLogicIR {
  override def children: Seq[BooleanLogicIR] = Seq()
}
