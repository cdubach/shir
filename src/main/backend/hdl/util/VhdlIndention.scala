package backend.hdl.util

object VhdlIndention {
  private final val INDENTION = "    "
  private final val openStartingKeywords: Seq[String] = Seq("package", "configuration", "entity", "architecture", "component", "procedure", "function", "port", "generic", "for", "while", "if", "case")
  private final val openEndingKeywords: Seq[String] = Seq("process", "loop", "is record", "port map", "generate")
  private final val intermediateKeywords: Seq[String] = Seq("begin", "else", "elsif") // "when" could be added here ...
  private final val closeKeywords: Seq[String] = Seq("end", "\\);")

  def indent(flatCode: String): String =  {
    var currentIndention: Int = 0
    val indentedCode: StringBuilder = new StringBuilder
    for (s <- flatCode.split('\n')) {
      val line = s.trim
      if ((closeKeywords ++ intermediateKeywords).exists(kw => line.toLowerCase.matches(kw + "(\\W.*)?")) && currentIndention > 0)
        currentIndention -= 1
      indentedCode.append(INDENTION * currentIndention).append(line.trim + "\n")
      if ((openStartingKeywords ++ intermediateKeywords).exists(kw => line.toLowerCase.matches(kw + "(\\W.*)?")) || (closeKeywords.forall(!line.toLowerCase.contains(_)) && openEndingKeywords.exists(kw => line.toLowerCase.contains(kw))))
        currentIndention += 1
    }
    indentedCode.mkString
  }
}
