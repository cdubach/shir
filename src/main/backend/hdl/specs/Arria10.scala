package backend.hdl.specs

import backend.hdl._
import backend.hdl.arch.Area
import core.ArithTypeT

object Arria10 extends FPGASpecs {
  override def name: String = "Arria 10 GX"

  override def cachelineType: BasicDataTypeT = VectorType(LogicType(), 512)

  override def onBoardRamDataType: BasicDataTypeT = VectorType(LogicType(), 512)
  override def onBoardRamSize: ArithTypeT = 67108864
  override def onBoardRamBurstWidth: ArithTypeT = 7
  override def onBoardRamReqIdSize: ArithTypeT = 16 //TODO: make it variable

  def addressType: BasicDataTypeT = IntType(42)
  def metaDataType: BasicDataTypeT = VectorType(LogicType(), 16)
  def readInputType: HWDataTypeT = RequestType(addressType, metaDataType)
  def readOutputType: HWDataTypeT = RequestType(cachelineType, metaDataType)
  def writeInputType: HWDataTypeT = RequestType(AddressedDataType(cachelineType, addressType), metaDataType)
  def writeOutputType: HWDataTypeT = RequestType(cachelineType, metaDataType)

  override def readReqBufferSize: ArithTypeT = 64
  override def writeReqBufferSize: ArithTypeT = 64

  override def hostRamReadInterface: HWFunTypeT =
    HWFunType(
      readInputType,
      readOutputType,
      AsyncCommunication
    )

  override def hostRamWriteInterface: HWFunTypeT =
    HWFunType(
      writeInputType,
      writeOutputType,
      AsyncCommunication
    )

  override def usedArea: Area = ???

  override def availableArea: Area = Area(
    427200,
    854400,
    55562240,
    2713, // M20K ram blocks
    1518 // DSPs; each DSP has 2 multipliers when using 19x18 bits
  )

  override def ramBlockBits: Int = 20000 // M20K

  override def calcRamBlocks(datawidth: ArithTypeT, depth: ArithTypeT): ArithTypeT = (datawidth.ae.evalInt, depth.ae.evalInt) match {
    case (_, 1) => 0 // if only one element is stored basic logic elements are used instead
    case (1, d) => d / 16000 + 1
    case (2, d) => d / 8000 + 1
    case (w, d) if w <= 5 => d / 4000 + 1
    case (w, d) if w <= 10 => d / 2000 + 1
    case (w, d) if w <= 20 => d / 1000 + 1
    case (w, d) if w <= 40 => d / 512 + 1
    case (w, d) =>
      val blocksWidth: Int = (w-1) / 40 + 1
      val blocksDepth: Int = (d-1) / 512 + 1
      blocksWidth * blocksDepth
  }
}
