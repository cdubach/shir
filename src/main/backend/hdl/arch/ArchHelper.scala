package backend.hdl.arch

import backend.hdl.HWFunTypeT
import core.{Expr, FunctionCall, Marker, ParamDef, ParamUse}

object ArchHelper {
  /**
   * Get Lambda Function's body.
   */
  def getLambdaBody(e: Expr): Expr = e match {
    case ArchLambda(p, body, _) => getLambdaBody(body)
    case e => e
  }

  /**
   * Get Lambda Params.
   */
  def getLambdaParams(e: Expr): Seq[ParamDef] = e match {
    case ArchLambda(p, body, _) => Seq(p) ++ getLambdaParams(body)
    case _ => Seq()
  }

  def getLambdaParamIds(e: Expr): Seq[Long] = e match {
    case ArchLambda(p, body, _) => Seq(p.id) ++ getLambdaParamIds(body)
    case _ => Seq()
  }

  /**
   * A helper that checks if an expression is calling a shared function.
   */
  def isSharedFunCall(e: Expr): Boolean = e match {
    case FunctionCall(f, _, _) => isSharedFunCall(f)
    case ParamUse(p) if p.t.isInstanceOf[HWFunTypeT] => true
    case Marker(ParamUse(p), _) if p.t.isInstanceOf[HWFunTypeT] => true
    case _ => false
  }

  /**
   * A helper that gets the shared function and inputs.
   */
  def getFunCall(e: Expr): Seq[Expr] = e match {
    case FunctionCall(f, input, _) => getFunCall(f) :+ input
    case pu @ ParamUse(p) if p.t.isInstanceOf[HWFunTypeT] => Seq(pu)
    case Marker(pu @ ParamUse(p), _) if p.t.isInstanceOf[HWFunTypeT] => Seq(pu)
    case _ => Seq()
  }
}
