package backend.hdl.arch

import backend.hdl.arch.mem.{MemFunctionsCompiler, MemLayoutCompiler, MemoryAllocation, WriteAsync, WriteAsyncOutOfBounds}
import backend.hdl.arch.rewrite.{MergeIntoCounterRules, MoveDownJoinStreamRules, MoveDownRepeatHiddenRules, MoveDownRepeatRules, RemoveSelectRules, RepeatBufferingRules}
import backend.hdl._
import backend.hdl.arch.ArchHelper.{getLambdaBody, getLambdaParams}
import backend.hdl.arch.MapCompilerHelper.{insertRepeatHiddenAndRepeatHiddenInverse, insertRepeatHiddenToHighOrderLambda}
import backend.hdl.arch.costModel.{AddressInterpreter, CostModel, ReadAnalyzer}
import backend.hdl.arch.sharedFunc.SharedFuncCompiler
import core._
import core.compile.{CompilerPass, CompilerPhase}
import core.rewrite.{RewriteAll, RewriteStep, Rule}
import core.util.IRDotGraph

object MapCompiler extends CompilerPass {

  override def phaseBefore: CompilerPhase = SharedFuncCompiler.phaseAfter

  override def run(expr: Expr): Expr = {
    val newExpr0 = TypeChecker.checkAssert(expr)
    val newExpr1 = TypeChecker.check(removeUnboundParams(newExpr0))
    val newExpr2 = RewriteStep(RewriteAll(), MoveDownJoinStreamRules.get() ++ MoveDownRepeatRules.get() ++ RemoveSelectRules.get()).apply(newExpr1)
    val newExpr3 = TypeChecker.check(MergeIntoCounterRules.reduce(
      RewriteStep(RewriteAll(), Seq(MergeIntoCounterRules.splitRepeatSplitStream)).apply(newExpr2)
    ))
    // RepeatBufferingRules now is an independent pass to avoid messing up (like inserting unwanted BRAMs) with write the other repeat-related rules.
    // This pass must happen after MergeIntoCounterRules to prevent extra buffers from being introduced.
    val newExpr4 = RewriteStep(RewriteAll(), Seq(RepeatBufferingRules.bufferRepeatParam, resolveHighLevelBRAMAccess)).apply(newExpr3)
    val newExpr5 = TypeChecker.check(removeLambdaRepeat(repeatFunctionLeaves(newExpr4)))
    val newExpr6 = RewriteStep(RewriteAll(), MoveDownRepeatHiddenRules.get()).apply(newExpr5)
    // assert that the RepeatHidden have been properly propagated
    newExpr6.visit({
      case InverseRepeatHidden(_, _, _) => throw MalformedExprException("temporary helper node found after rewriting! This should not happen - maybe a rewrite rule is missing?")
      case _ =>
    })
    print("Estimated run time (cycles): " +  CostModel.estimateRunCyclesWithFunctions(newExpr6) + "\n")
    val newExpr7 = TypeChecker.check(replaceMapStreams(newExpr6))
    //    IRDotGraphAnimation(Seq(newExpr0, newExpr5)).show()
    newExpr7
  }

  private def removeUnboundParams(expr: Expr): Expr = expr match {
    // For MapAsync, we have to increase the number of input ports
    case MapAsyncOrderedStream(function: LambdaT, inputs, t: OrderedStreamTypeT) =>
      val unboundParams = getUnboundParams(function).distinct
      if (unboundParams.isEmpty) {
        MapAsyncOrderedStream(removeUnboundParams(function), inputs.map(i => removeUnboundParams(i)): _*)
      } else {
        val newParams = unboundParams.map(p => ParamDef(p.t))
        val newFunc = ArchLambdas(
          newParams,
          removeUnboundParams(function).visitAndRebuild {
            case pu: ParamUse if unboundParams.contains(pu) =>
              ParamUse(newParams(unboundParams.indexOf(pu)))
            case e => e
          }.asInstanceOf[Expr]
        )
        val repeatUnboundParams = unboundParams.map(p => Repeat(p, t.len))
        val inputGroup = repeatUnboundParams ++ inputs
        val newInputGroup = inputGroup.map(i => removeUnboundParams(i))
        MapAsyncOrderedStream(newFunc, newInputGroup: _*)
      }
    case MapOrderedStream(function: LambdaT, input, t: OrderedStreamTypeT) =>
      val unboundParams = getUnboundParams(function).distinct

      if (unboundParams.isEmpty) {
        MapOrderedStream(
          removeUnboundParams(function),
          removeUnboundParams(input)
        )
      } else {
        val arity = unboundParams.size + 1
        val entries = input +: unboundParams.map(Repeat(_, t.len))
        MapOrderedStream({
          val newParam = ParamDef()
          ArchLambda(
            newParam,
            // Important: first treat children (here: body) and then the current node. Traverse from leaves to the top.
            // Otherwise a more complex nesting of Select primitives would be generated!
            removeUnboundParams(function.body).visitAndRebuild {
              case ParamUse(pd) if pd.id == function.param.id =>
                Select(ParamUse(newParam), arity, 0)
              case pu: ParamUse if unboundParams.contains(pu) =>
                Select(ParamUse(newParam), arity, unboundParams.indexOf(pu) + 1)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, removeUnboundParams(ZipOrderedStream(Tuple(entries: _*), arity)))
      }
    case MapUnorderedStream(function, input, t: UnorderedStreamTypeT) =>
      ???
    // Reduce -based primitives do not need this mechanism because there is no need to ever repeat their input stream
    case _ => expr.build(expr.children.map({
      case e: Expr => removeUnboundParams(e)
      case t: Type => t
    }))
  }

  /**
   * Inserting RepeatHidden and RepeatHiddenInverse to MapStm's body and Let-Lambda's body. Note that RepeatHidden
   * implies the information of how many times we should repeat. However, inserted RepeatHiddens must be moved down so
   * that they can cancel RepeatHiddenInverses. This process may take about 2/3 of time for a deep expression tree. To
   * solve this, we directly insert RepeatHiddens to data sources such as counter and const to avoid tedious rewriting.
   *
   * Note that RepeatHidden insertion of Let-Lambda's body requires us to traverse all the function calls to accumulate
   * the number of occurrences.
   *
   * @param boundedLetParams lists all params that are bounded by Let and the corresponding arg's real value is only
   *                         assigned once.
   */
  private def repeatFunctionLeaves(expr: Expr, boundedLetParams: Seq[Long] = Seq()): Expr = expr match {
    case MapOrderedStream(ArchLambda(p, body, _), input, t) =>
      val OrderedStreamType(_, inputLength) = t
      // insertRepeatHidden method directly add RepeatHidden to data sources. To preserve the order of RepeatHidden, we
      // must insert RepeatHidden for outer map and then the one of inner map.
      // This is what we want: RepeatHidden(RepeatHidden(_, innerMap.len), outerMap.len).
      MapOrderedStream(
        ArchLambda(
          p,
          repeatFunctionLeaves(insertRepeatHiddenAndRepeatHiddenInverse(body, inputLength, boundedLetParams), boundedLetParams)),
        repeatFunctionLeaves(input, boundedLetParams)
      )
    case MapUnorderedStream(ArchLambda(p, body, _), input, t) =>
      val UnorderedStreamType(_, inputLength) = t
      MapUnorderedStream(
        ArchLambda(p,
          repeatFunctionLeaves(insertRepeatHiddenAndRepeatHiddenInverse(body, inputLength, boundedLetParams), boundedLetParams)),
        repeatFunctionLeaves(input, boundedLetParams)
      )
    case MapAsyncOrderedStream(f @ ArchLambda(_, _, _), inputs, t) => // @ ArchLambda is necessary for avoiding Let-MapAsync-Fun
      val OrderedStreamType(_, inputLength) = t
      val body = getLambdaBody(f)
      val params = getLambdaParams(f).reverse
      MapAsyncOrderedStream(
        ArchLambdas(params,
          repeatFunctionLeaves(insertRepeatHiddenAndRepeatHiddenInverse(body, inputLength, boundedLetParams), boundedLetParams)),
        inputs.map(input => repeatFunctionLeaves(input, boundedLetParams)): _*
      )
    case FoldOrderedStream(ArchLambda(p1, ArchLambda(p2, body, _), _), input, _) =>
      val inputLength = TypeChecker.checkIfUnknown(input, input.t).t match {
        case UpperBoundedStreamType(_, length) => length
        case OrderedStreamType(_, length) => length
      }
      FoldOrderedStream(
        // TODO: Not sure if both params are required.
        ArchLambda(p1, ArchLambda(p2,
          repeatFunctionLeaves(insertRepeatHiddenAndRepeatHiddenInverse(body, inputLength, boundedLetParams), boundedLetParams))),
        repeatFunctionLeaves(input, boundedLetParams)
      )
    case ReduceOrderedStream(ArchLambda(p1, ArchLambda(p2, body, _), _), initialValue, input, _) =>
      val OrderedStreamType(_, inputLength) = TypeChecker.checkIfUnknown(input, input.t).t
      ReduceOrderedStream(
        // TODO: Not sure if both params are required.
        ArchLambda(p1, ArchLambda(p2,
          repeatFunctionLeaves(insertRepeatHiddenAndRepeatHiddenInverse(body, inputLength, boundedLetParams), boundedLetParams))),
        repeatFunctionLeaves(initialValue, boundedLetParams),
        repeatFunctionLeaves(input, boundedLetParams)
      )
    case Let(param: ParamDef, body, ArchLambda(inParam, fBody, _), _) if {
      // Important: Exclude Let-Lambda-MemoryController, which requires "Read" to access the function.
      // Here we assume the valid ways to access a shared function are FunCall, MapStm, MapAsyncStm.
      // If the counters below does not match, there might be some unexpected accesses to a function.
      var callOccurrences = 0
      var paramOccurrences = 0
      body.visit {
        case FunctionCall(ParamUse(p1), _, _) if param.id == p1.id =>
          callOccurrences += 1
        case MapOrderedStream(ParamUse(p1), _, _) if param.id == p1.id =>
          callOccurrences += 1
        case MapAsyncOrderedStream(ParamUse(p1), _, _) if param.id == p1.id =>
          callOccurrences += 1
        case ParamUse(p1) if param.id == p1.id =>
          paramOccurrences += 1
        case _ =>
      }
      callOccurrences >= 1 && paramOccurrences == callOccurrences
    } =>
      // We have to solve the number of occurrences inside body child so that it can be propagate to inner nested function.
      val newBody = repeatFunctionLeaves(body, boundedLetParams)

      // Reserve Marker(ParamUse(_), _) for this pass. Check if body expression contains this.
      newBody.visit {
        case Marker(ParamUse(p1), _) if param.id == p1.id =>
          throw MalformedExprException("Illegal Marker(ParamUse(_), _) found. This expression is reserved by MapCompiler.")
        case _ =>
      }

      // Give each call an Id and build occurrence table
      var idCount = 0
      var occurTable: Seq[Int] = Seq()
      val idBody = TypeChecker.check(newBody.visitAndRebuild {
        case ParamUse(p1) if param.id == p1.id =>
          val markPoint = Marker(ParamUse(p1), TextType(idCount.toString))
          idCount = idCount + 1
          occurTable = occurTable :+ 1
          markPoint
        case e => e
      }.asInstanceOf[Expr])

      // Increase values in the occurrence table based on MapStm
      idBody.visit {
        case Let(_, _, arg @ ArchLambda(_, _, _), _) if MapCompilerHelper.checkLambdaTopRepeatHidden(arg) && { // Consider nested functions
          var exprFound = false
          val rep = MapCompilerHelper.getLambdaTopRepeatHidden(arg) match {
            case Some(rep: ArithTypeT) => rep
            case _ => ArithType(1)
          }
          arg.visit {
            case Marker(ParamUse(p1), ts) if param.id == p1.id =>
              occurTable = occurTable.updated(ts.s.toInt, occurTable(ts.s.toInt) * rep.ae.evalInt)
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
        case MapOrderedStream(f, _, t: OrderedStreamTypeT) if { // Map's length should be reflected on "occurrences"
          var len = t.len.ae.evalInt
          var exprFound = false
          f.visit {
            case WriteAsync(_, _, _, _) => len = 1
            case WriteAsyncOutOfBounds(_, _, _, _, _) => len = 1
            case Marker(ParamUse(p1), ts) if param.id == p1.id =>
              occurTable = occurTable.updated(ts.s.toInt, occurTable(ts.s.toInt) * len)
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
        case MapAsyncOrderedStream(f, _, t: OrderedStreamTypeT) if { // Map's length should be reflected on "occurences"
          var len = t.len.ae.evalInt
          var exprFound = false
          f.visit {
            case WriteAsync(_, _, _, _) => len = 1
            case WriteAsyncOutOfBounds(_, _, _, _, _) => len = 1
            case Marker(ParamUse(p1), ts) if param.id == p1.id =>
              occurTable = occurTable.updated(ts.s.toInt, occurTable(ts.s.toInt) * len)
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
        case _ =>
      }
      // Get repetition
      val rep = occurTable.sum

      // Function's input should not be repeated
      val newFBody = insertRepeatHiddenAndRepeatHiddenInverse(fBody, rep, boundedLetParams)

      // Insert RepeatHidden to nested Lambda
      Let(param,
        newBody,
        ArchLambda(inParam,
          repeatFunctionLeaves(insertRepeatHiddenToHighOrderLambda(newFBody, rep), boundedLetParams)
        )
      )

    case Let(param: ParamDef, body, wr@Let(_, writePr, _, _), _) if {
      var exprFound = false
      writePr match {
        case WriteAsync(_, _, _, _) => exprFound = true
        case WriteAsyncOutOfBounds(_, _, _, _, _) => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      // We have to solve the number of occurrences inside body child so that it can be propagate to inner nested function.
      val newBody = repeatFunctionLeaves(body, boundedLetParams)
      // Give each call an Id and build occurrence table
      var idCount = 0
      var occurTable: Seq[Int] = Seq()
      val idBody = TypeChecker.check(newBody.visitAndRebuild {
        case ParamUse(p1) if param.id == p1.id =>
          val markPoint = Marker(ParamUse(p1), TextType(idCount.toString))
          idCount = idCount + 1
          occurTable = occurTable :+ 1
          markPoint
        case e => e
      }.asInstanceOf[Expr])

      // Increase values in the occurrence table based on MapStm
      idBody.visit {
        case Let(_, _, arg @ ArchLambda(_, _, _), _) if MapCompilerHelper.checkLambdaTopRepeatHidden(arg) && { // Consider nested functions
          var exprFound = false
          val rep = MapCompilerHelper.getLambdaTopRepeatHidden(arg) match {
            case Some(rep: ArithTypeT) => rep
            case _ => ArithType(1)
          }
          arg.visit {
            case Marker(ParamUse(p1), ts) if param.id == p1.id =>
              occurTable = occurTable.updated(ts.s.toInt, occurTable(ts.s.toInt) * rep.ae.evalInt)
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
        case MapOrderedStream(ArchLambda(_, mapBody, _), mapIn, _) if { // Map's length should be reflected on "occurrences"
          var exprFound = false
          mapBody.visit {
            case Marker(ParamUse(p1), ts) if param.id == p1.id =>
              occurTable = occurTable.updated(ts.s.toInt, occurTable(ts.s.toInt) * mapIn.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt)
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
        case MapAsyncOrderedStream(f, _, t: OrderedStreamTypeT) if { // Map's length should be reflected on "occurences"
          val mapBody = getLambdaBody(f)
          var exprFound = false
          mapBody.visit {
            case Marker(ParamUse(p1), ts) if param.id == p1.id =>
              occurTable = occurTable.updated(ts.s.toInt, occurTable(ts.s.toInt) * t.len.ae.evalInt)
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
        case _ =>
      }
      // Get repetition
      val rep = occurTable.sum

      Let(param,
        newBody,
        repeatFunctionLeaves(RepeatHidden(wr, rep), boundedLetParams)
      )
    case Let(p, body, arg, _) if getUnboundParams(arg, boundedLetParams).length == 0 =>
      Let(p, repeatFunctionLeaves(body, boundedLetParams :+ p.id), repeatFunctionLeaves(arg, boundedLetParams))
    case e: Expr => e.build(e.children.map({
      case ce: Expr => repeatFunctionLeaves(ce, boundedLetParams)
      case t => t
    }))
  }

  /**
   * Avoid moving RepeatHidden into Let-Lambda because RepeatHidden calculation is done by RepeatHiddenLeaves.
   */
  private def removeLambdaRepeat(expr: Expr): Expr = expr.visitAndRebuild {
    case Let(p0, body, f @ ArchLambda(_, _, _), _) if MapCompilerHelper.checkLambdaTopRepeatHidden(f) =>
      Let(p0, body, MapCompilerHelper.removeLambdaTopRepeatHidden(f))
    case e => e
  }.asInstanceOf[Expr]

  private def replaceMapStreams(expr: Expr): Expr = expr match {
    // maps within repeat or loop expressions may be repeated and cannot be replaced by simplified implementations
    case repeat @ Repeat(_, _, _) => skipMapStreams(repeat)
    case repeatHidden @ RepeatHidden(_, _, _) => skipMapStreams(repeatHidden)
    case MapOrderedStream(function: LambdaT, input, _) =>
      val unboundParams = getUnboundParams(function)
      (unboundParams.isEmpty, hasRepeatedStreamInput(function)) match {
        case (true, false) =>
          MapSimpleOrderedStream(
            replaceMapStreams(function),
            replaceMapStreams(input)
          )
        case (_, true) =>
          MapOrderedStream(
            replaceMapStreams(function),
            input
          )
        case _ =>
          MapOrderedStream(
            replaceMapStreams(function),
            replaceMapStreams(input)
          )
      }
    case MapUnorderedStream(function, input, t: UnorderedStreamTypeT) =>
      ???
      // TODO fix vhdl templates first!
    case _ => expr.build(expr.children.map({
      case e: Expr => replaceMapStreams(e)
      case t: Type => t
    }))
  }

  private def skipMapStreams(expr: Expr): Expr = expr match {
    /* reduction-like expressions (like fold, reduce, writeAsync (remember WriteAsync takes a stream of data and returns a base address))
    return a single element (and not a stream anymore); and single elements can be repeated without any problem.
    No need to 'skip' MapStreams any longer, go back to 'replaceMapStreams'. */
    case fold @ FoldOrderedStream(_, _, _) => replaceMapStreams(fold)
    case reduce @ ReduceOrderedStream(_, _, _, _) => replaceMapStreams(reduce)
    case writeAsync @ WriteAsync(_, _, _, _) => replaceMapStreams(writeAsync)
    case writeAsync @ WriteAsyncOutOfBounds(_, _, _, _, _) => replaceMapStreams(writeAsync)
    case stm2Vec @ OrderedStreamToVector(_) => replaceMapStreams(stm2Vec)
    case alterStm2Vec @ Alternate(ArchLambda(_, OrderedStreamToVector(_, _), _), ArchLambda(_, OrderedStreamToVector(_, _), _), _, _) => replaceMapStreams(alterStm2Vec)
    case _ => expr.build(expr.children.map({
      case e: Expr => skipMapStreams(e)
      case t: Type => t
    }))
  }

  def getUnboundParams(expr: Expr, paramIds: Seq[Long] = Seq()): Seq[ParamUse] = expr match {
    case l: LambdaT => getUnboundParams(l.body, paramIds :+ l.param.id)
    // do not collect shared functions! We do not want to 'repeat' anything in shared functions!
    case pu @ Marker(ParamUse(pd), ts) if ts.s.contains("repeatGuard") => Seq()
    case pu @ ParamUse(pd) if !pu.t.isInstanceOf[FunTypeT] && !paramIds.contains(pd.id) => Seq(pu)
    case _ => expr.children.flatMap{
      case e: Expr => getUnboundParams(e, paramIds)
      case _ => Seq()
    }
  }

  private def hasRepeatedStreamInput(lambda: LambdaT): Boolean = {
    lambda.visit{
      case Repeat(input, _, _) if {
        (input match {
          case Repeat(_, _, _) => false
          case RepeatHidden(_, _, _) => false
          case _ => true
        }) &&
        (input.t match {
          case _: UnorderedStreamTypeT => true
          case _ => false // 1D data can easily be repeated without any repetition related problems
        }) &&
        hasParamUse(input, lambda.param.id)
      } => return true
      case _ =>
    }
    false
  }

  private def hasParamUse(expr: Expr, paramId: Long): Boolean = {
    expr.visit{
      case ParamUse(pd) if pd.id == paramId => return true
      case _ =>
    }
    false
  }

  /**
   * This rule aims to replace high-level block rams that are inserted after memory function phase.
   */
  private def resolveHighLevelBRAMAccess: Rule = Rule("resolveHighLevelBRAMAccess", {
    case e @ (Let(_, _, _ @ MemoryAllocation(_, _, _, _, _), _) | Repeat(Let(_, _, MemoryAllocation(_), _), _, _)) =>
      val expr1 = MemLayoutCompiler.run(e)
      val expr2 = MemFunctionsCompiler.run(expr1)
      val expr3 = removeUnboundParams(expr2)
      val expr4 = RewriteStep(RewriteAll(), MoveDownRepeatRules.get() ++ RemoveSelectRules.get()).apply(expr3)
      val expr5 = TypeChecker.check(MergeIntoCounterRules.reduce(expr4))
      expr5
  })
}

case class InverseRepeatHiddenExpr private[arch](innerIR: Expr, types: Seq[Type]) extends BuiltinExpr {
  override def build(newInnerIR: Expr, newTypes: Seq[Type]): InverseRepeatHiddenExpr = InverseRepeatHiddenExpr(newInnerIR, newTypes)
}
object InverseRepeatHidden extends BuiltinExprFun {
  override def args: Int = 1
  private[arch] def apply(input: Expr, dimensions: Seq[ArithTypeT]): Expr = dimensions.size match {
    case 0 => input
    case _ => InverseRepeatHidden(apply(input, dimensions.tail), dimensions.head)
  }
  def apply(input: Expr, len: ArithTypeT): InverseRepeatHiddenExpr = InverseRepeatHiddenExpr({
    val stv = NonArrayTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(stv, HWFunType(stv, stv))), input.t), input)
  }, Seq(len))
  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): InverseRepeatHiddenExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])
  def unapply(expr: InverseRepeatHiddenExpr): Some[(Expr, ArithTypeT, Type)] =
    Some(expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t)
}
