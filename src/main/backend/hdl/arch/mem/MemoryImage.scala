package backend.hdl.arch.mem

import backend.hdl.arch.ConstantValue
import backend.hdl.specs.FPGA
import backend.hdl._
import core.util.{IRDotGraph, Log, ToFiles}
import core.{ArithTypeT, Expr}

import scala.collection.{AbstractIterator, mutable}

final case class MemoryImage private (binaryData: Map[String, Array[Array[Byte]]], baseAddrs: Map[String, Long]) extends ToFiles {

  val INITIAL_MEM_IMAGE: String = "mem_initial.dat"
  val FINAL_MEM_IMAGE: String = "mem_final.dat"

  override def toStringIterators: Map[String, Iterator[String]] = Map(INITIAL_MEM_IMAGE -> initialState, FINAL_MEM_IMAGE -> finalState)

  def initialState: Iterator[String] = generate(false)

  def finalState: Iterator[String] = generate(true)

  private def generate(includeResult: Boolean): Iterator[String] = {
    val cacheLineWidth = FPGA.current.cachelineType.bitWidth.ae.evalInt

    val filteredBaseAddrs = baseAddrs.filterNot(entry =>
      !includeResult && entry._1 == MemoryImage.RESULT_VAR_NAME
    ).toSeq.sortBy(_._2)

    new AbstractIterator[String] {
      private var currentLine: Int = 0
      private var currentBucket: Int = 0

      override val length: Int = filteredBaseAddrs.lastOption match {
        case None => 0
        case Some((id, baseAddr)) =>
          baseAddr.toInt + binaryData(id).length
      }

      override def hasNext: Boolean = currentLine < length

      override def next(): String = {
        val (id, baseAddr) = filteredBaseAddrs(currentBucket)
        if (currentLine < baseAddr) {
          currentLine += 1
          "".padTo(cacheLineWidth, '0')
        } else {
          val lines = binaryData(id)
          if (currentLine - baseAddr.toInt < lines.length) {
            currentLine += 1
            MemoryImage.bytesToCacheLine(lines(currentLine - 1 - baseAddr.toInt))
          } else {
            // The previous call to next() has just emitted the last line of
            // the previous data entry, so move to the next entry and retry.
            //
            // hasNext of true implies there has to be a next entry,
            // therefore this increment here is safe.
            // (hasNext of false is undefined, so we can crash in that case)
            currentBucket += 1
            next()
          }
        }
      }
    }
  }
}

object MemoryImage {

  final val RESULT_VAR_NAME: String = "result"

  def apply(expr: Expr, data: Map[String, Seq[Any]]): MemoryImage = apply(MemoryLayout(expr), data)

  def apply(layout: MemoryLayout, data: Map[String, Seq[Any]]): MemoryImage = {
    val cacheLineWidth = FPGA.current.cachelineType.bitWidth.ae.evalInt
    val binaryData: Map[String, Array[Array[Byte]]] = layout.metaData.map(entry => {
      val id = entry._1
      val (dims, origElementType) = entry._2
      data.get(id) match {
        case None =>
          throw MemoryException("data entry for '" + id + "' is missing")
        case Some(d: Seq[Any]) if d.isEmpty =>
          throw MemoryException("data must not be empty")
        case Some(d: Seq[Any]) =>
          Log.info(this, "starting memory image generation for '" + id + "'.")
          id -> generateMemDataEntry(d, dims, origElementType, cacheLineWidth)
      }
    }).toMap

    apply(binaryData, layout.baseAddrs)
  }

  def cacheLineToBytes(cl: String): Array[Byte] = {
    assert(cl.length % 8 == 0)
    cl.grouped(8).map(Integer.parseInt(_, 2).toByte).toArray
  }

  def bytesToCacheLine(bytes: Array[Byte]): String = {
    bytes.map(b => Integer.toBinaryString((b & 0xFF) + 0x100).substring(1)).mkString
  }

  def generateMemDataEntry(data: Any, dimensions: Seq[ArithTypeT], elementType: HWDataTypeT, cacheLineWidth: Int): Array[Array[Byte]] = (data, dimensions.size) match {
    case (seq: Seq[Any], s) if s > 1 =>
      if (seq.length != dimensions.last.ae.evalInt)
        throw MemoryException("length " + seq.length + " of provided data does not match length " + dimensions.last.ae.evalInt + " of data type!")
      seq.flatMap(generateMemDataEntry(_, dimensions.init, elementType, cacheLineWidth)).toArray
    case (seq: Seq[Any], 1) =>
      var lines: Array[Array[Byte]] = Array()
      var currentCL: String = ""

      seq.foreach(e => {
        val newData = generateScalarMemData(e, elementType)
        if (newData.length > cacheLineWidth) {
          assert(currentCL.isEmpty)
          val newDataPadded = newData.grouped(cacheLineWidth).map(s => s.reverse.padTo(cacheLineWidth, '0').reverse)
          lines ++= newDataPadded.map(cacheLineToBytes)
        } else if (currentCL.length + newData.length <= cacheLineWidth) {
          currentCL = newData + currentCL
        } else { // data normally fits into cacheline, but cacheline is too full
          lines :+= cacheLineToBytes(currentCL.reverse.padTo(cacheLineWidth, '0').reverse)
          currentCL = newData
        }
      })

      if (currentCL.nonEmpty) {
        lines :+= cacheLineToBytes(currentCL.reverse.padTo(cacheLineWidth, '0').reverse)
      }

      lines

    case (_, s) if s > 0 =>
      throw MemoryException("dimensions of provided data does not data type")
  }

  def generateScalarMemData(data: Any, elementType: HWDataTypeT): String = (data, elementType) match {
    case (i: Int, _: SignedIntTypeT) if i < 0 =>
      val binValue: String = i.toBinaryString
      val realBinLength: Int = (~i).toBinaryString.length + 1
      val width: Int = elementType.bitWidth.ae.evalInt
      if (realBinLength > width)
        throw MemoryException("Binary value for " + i + " exceeds precision of " + width + " bits!")
      binValue.reverse.substring(0, realBinLength).padTo(width, binValue.head).reverse
    case (i: Int, _: IntTypeT) =>
      val binValue: String = i.toBinaryString
      val width: Int = elementType.bitWidth.ae.evalInt
      if (binValue.length > width)
        throw MemoryException("Binary value for " + i + " exceeds precision of " + width + " bits!")
      binValue.reverse.padTo(width, '0').reverse
    case _ =>
      throw new Exception("implementations for other data types than integer missing here")
  }
}
