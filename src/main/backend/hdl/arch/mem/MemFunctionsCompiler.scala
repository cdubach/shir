package backend.hdl.arch.mem

import backend.hdl.arch._
import backend.hdl._
import backend.hdl.specs.FPGA
import core.util.{IRDotGraph, Log}
import core._
import core.compile.{CompilerPass, CompilerPhase}

import scala.collection.mutable

object MemFunctionsCompiler extends CompilerPass {

  override def phaseBefore: CompilerPhase = MemLayoutCompiler.phaseAfter

  override def run(expr: Expr): Expr = {
    val exprChecked = TypeChecker.checkAssert(expr)
    exprChecked.visit({
      case MemoryAllocation(_, _, _, _, _) => throw MemoryException("Memory must be allocated as regions first!")
      case _ =>
    })

    val baseAddrTypes: mutable.Map[IdTypeT, BaseAddrType] = mutable.Map()
    exprChecked.visit({
      case MemoryRegion(_, _, baseAddr, _, _, ramArrayType: RamArrayTypeT) =>
        val baseAddrBitWidth = baseAddr.ae.evalLong.toBinaryString.length
        // get maximum bitwidth
        baseAddrTypes.get(ramArrayType.memoryLocation.ramId) match {
          case Some(bat) if bat.bitWidth.ae.evalInt >= baseAddrBitWidth => Unit
          case _ => baseAddrTypes.put(ramArrayType.memoryLocation.ramId,
            BaseAddrType(ArithType(baseAddrBitWidth), ramArrayType.memoryLocation))
        }
      case _ => Unit
    })

    //    val hostRamReadCtrlFunc = ReadHostMemoryController.asFunction(Seq(FPGA.current.hostRamReadInterface, FPGA.current.readReqBufferSize))
    val hostRamReadCtrlFunc = ReadHostMemoryController.asFunction(FPGA.current)
    val hostRamReadCtrlParam = ParamDef(hostRamReadCtrlFunc.t)
    //    val hostRamWriteCtrlFunc = WriteHostMemoryController.asFunction(Seq(FPGA.current.hostRamWriteInterface, FPGA.current.writeReqBufferSize))
    val hostRamWriteCtrlFunc = WriteHostMemoryController.asFunction(FPGA.current)
    val hostRamWriteCtrlParam = ParamDef(hostRamWriteCtrlFunc.t)

    //TODO: Change made here
    val onBoardRamBankFunc = OnBoardRamBuffer.asFunction(FPGA.current)
    val onBoardRamBankParam = ParamDef(onBoardRamBankFunc.t)

    val onBoardRamBankReadCtrlFunc: Expr = {
            val inputParam = ParamDef()
            ArchLambda(inputParam, ReadLocalMemoryController(ParamUse(onBoardRamBankParam), ParamUse(inputParam)), AsyncCommunication)
          }

    val onBoardRamBankReadCtrlParam = ParamDef(onBoardRamBankReadCtrlFunc.t)

    val onBoardRamBankWriteCtrlFunc: Expr = {
            val inputParam = ParamDef()
            ArchLambda(inputParam, WriteLocalMemoryController(ParamUse(onBoardRamBankParam), ParamUse(inputParam)), AsyncCommunication)
          }
    val onBoardRamBankWriteCtrlParam = ParamDef(onBoardRamBankWriteCtrlFunc.t)

    /**
      * @param memCtrlFuncs maps id of rams to their read controller parameter _1 and write controller paramter _2
      */
    def _generate(expr: Expr, memCtrlFuncs: Map[IdTypeT, Seq[(ParamDef, ParamDef)]]): Expr = expr match {
      // catch Let with MemAllocation (arg) here and replace by Let with mem control functions!
      case Let(_, _, arg, _) if
      (arg.t match {
        case RamArrayType(_, _, ramType) if !memCtrlFuncs.isDefinedAt(ramType.ramId) => true
        case _ => false
      }) =>
        val at = arg.t.asInstanceOf[RamArrayTypeT]
        at.memoryLocation match {
          case HostRamType => throw MalformedExprException("Hostram controller must be created in advance!")
          case OnBoardRamType => throw MalformedExprException("On Board Ram controller must be created in advance!")
          case _: BlockRamTypeT =>
            // create block ram function and both ram controllers
            val ramFunc: Expr = BlockRamBuffer.asFunction(at)
            val ramFuncParam = ParamDef(ramFunc.t)

            val readMemCtrlFunc: Expr = {
              val inputParam = ParamDef()
              ArchLambda(inputParam, ReadSyncMemoryController(ParamUse(ramFuncParam), ParamUse(inputParam)))
            }
            val readMemCtrlFuncParam = ParamDef(readMemCtrlFunc.t)
            val writeMemCtrlFunc: Expr = {
              val inputParam = ParamDef()
              ArchLambda(inputParam, WriteSyncMemoryController(ParamUse(ramFuncParam), ParamUse(inputParam)))
            }
            val writeMemCtrlFuncParam = ParamDef(writeMemCtrlFunc.t)

            val newMemFuncParams = memCtrlFuncs ++ Map(at.memoryLocation.ramId -> Seq((readMemCtrlFuncParam, writeMemCtrlFuncParam)))

            Let(
              ramFuncParam,
              Let(
                Seq(readMemCtrlFuncParam, writeMemCtrlFuncParam),
                _generate(expr, newMemFuncParams),
                Seq(readMemCtrlFunc, writeMemCtrlFunc)
              ),
              ramFunc
            )
          case _: BankedBlockRamTypeT =>
            val ramFunc: Expr = SyncBankedBuffer.asFunction(at)
            val ramFuncParam = ParamDef(ramFunc.t)

            val readMemCtrlFunc: Expr = {
              val inputParam = ParamDef()
              ArchLambda(inputParam, ReadSyncBankedMemoryController(ParamUse(ramFuncParam), ParamUse(inputParam), at.memoryLocation.asInstanceOf[BankedBlockRamType].numBanks))
            }
            val readMemCtrlFuncParam = ParamDef(readMemCtrlFunc.t)
            val writeMemCtrlFunc: Expr = {
              val inputParam = ParamDef()
              ArchLambda(inputParam, WriteSyncBankedMemoryController(ParamUse(ramFuncParam), ParamUse(inputParam)))
            }
            val writeMemCtrlFuncParam = ParamDef(writeMemCtrlFunc.t)

            val newMemFuncParams = memCtrlFuncs ++ Map(at.memoryLocation.ramId -> Seq((readMemCtrlFuncParam, writeMemCtrlFuncParam)))

            Let(
              ramFuncParam,
              Let(
                Seq(readMemCtrlFuncParam, writeMemCtrlFuncParam),
                _generate(expr, newMemFuncParams),
                Seq(readMemCtrlFunc, writeMemCtrlFunc)
              ),
              ramFunc
            )
          case MemoryLocationType(_) | MemoryLocationTypeVar(_, _) => throw MalformedExprException("Cannot lower memory expression with abstract memory location type!")
        }
      // turn Map-Read on async memory into a ReadAsync
      // (the request buffer is as long as the input stream to the map)
      case MapUnorderedStream(ArchLambda(_, Read(memAllocation, _, _), _), input, _) if
      (memAllocation.t match {
        case RamArrayType(_, _, HostRamType) => true
        case RamArrayType(_, _, OnBoardRamType) => true //TODO: Change made here
        case _ => false
      }) =>
        val ramType = memAllocation.t.asInstanceOf[RamArrayTypeT].memoryLocation
        ReadAsync(
          ParamUse(memCtrlFuncs(ramType.ramId).head._1),
          _generate(memAllocation, memCtrlFuncs),
          _generate(input, memCtrlFuncs)
        )
//      case MapOrderedStream(f @ ArchLambda(_, MapOrderedStream(ArchLambda(_, Read(_, _, _), _), _, _), _), input, _) =>
      case MapOrderedStream(ArchLambda(_, Read(memAllocation, _, _), _), input, _) if
      (memAllocation.t match {
        case RamArrayType(_, _, HostRamType) => true
        case RamArrayType(_, _, OnBoardRamType) => true //TODO: Change made here
        case _ => false
      }) =>
        val ramType = memAllocation.t.asInstanceOf[RamArrayTypeT].memoryLocation
        ReadAsyncOrdered(
          ParamUse(memCtrlFuncs(ramType.ramId).head._1),
          _generate(memAllocation, memCtrlFuncs),
          _generate(input, memCtrlFuncs)
        )
      case MapOrderedStream(ArchLambda(_, ReadOutOfBounds(memAllocation, _, paddedValue, addrRule, _), _), input, _) if
        (memAllocation.t match {
          case RamArrayType(_, _, HostRamType) => true
          case RamArrayType(_, _, OnBoardRamType) => true //TODO: Change made here
          case _ => false
        }) =>
        val ramType = memAllocation.t.asInstanceOf[RamArrayTypeT].memoryLocation
        ReadAsyncOrderedOutOfBounds(
          ParamUse(memCtrlFuncs(ramType.ramId).head._1),
          _generate(memAllocation, memCtrlFuncs),
          _generate(input, memCtrlFuncs),
          paddedValue,
          addrRule.toArithLambda
        )
      // turn Reduce-Write on async memory into a WriteAsync
      // (the request buffer is as long as the input stream to the reduce)
      case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, Write(_, _, _), _), _), memAllocation, input, _) if
      (memAllocation.t match {
        case RamArrayType(_, _, HostRamType) => true
        case RamArrayType(_, _, OnBoardRamType) => true //TODO: Change made here
        case _ => false
      }) =>
        val ramType = memAllocation.t.asInstanceOf[RamArrayTypeT].memoryLocation
        WriteAsync(
          ParamUse(memCtrlFuncs(ramType.ramId).head._2),
          _generate(memAllocation, memCtrlFuncs),
          _generate(input, memCtrlFuncs)
        )
      case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, WriteOutOfBounds(_, _, addrRule, _), _), _), memAllocation, input, _) if
        (memAllocation.t match {
          case RamArrayType(_, _, HostRamType) => true
          case RamArrayType(_, _, OnBoardRamType) => true //TODO: Change made here
          case _ => false
        }) =>
        val ramType = memAllocation.t.asInstanceOf[RamArrayTypeT].memoryLocation
        WriteAsyncOutOfBounds(
          ParamUse(memCtrlFuncs(ramType.ramId).head._2),
          _generate(memAllocation, memCtrlFuncs),
          _generate(input, memCtrlFuncs),
          addrRule
        )
      case memoryExpr: MemExpr => memoryExpr match {
        case MemoryAllocation(_, _, _, _, _) =>
          throw MalformedExprException("Unknown memory region detected! Generate memory regions first!")
        case FakeMemoryAllocation(realMem, _, _) =>
          _generate(realMem, memCtrlFuncs)
        case MemoryRegion(identifier, description, baseAddr, origElementType, dimensions, ramArrayType: RamArrayTypeT) =>
          ConstantValue(baseAddr, Some(BaseAddrInitType(
            baseAddrTypes(ramArrayType.memoryLocation.ramId).bitWidth,
            identifier,
            description,
            ramArrayType.memoryLocation,
            origElementType,
            dimensions
          )))
        case Read(memAllocation, input, _) => memAllocation.t match {
          case RamArrayType(_, _, ramType) => ramType match {
            //TODO: Change made here
            case _: BlockRamTypeT =>
              ReadSync(
                ParamUse(memCtrlFuncs(ramType.ramId).head._1),
                _generate(memAllocation, memCtrlFuncs),
                _generate(input, memCtrlFuncs)
              )

            case _: BankedBlockRamTypeT =>
              ReadSyncBanked(
                ParamUse(memCtrlFuncs(ramType.ramId).head._1),
                _generate(memAllocation, memCtrlFuncs),
                _generate(input, memCtrlFuncs)
              )

            case HostRamType | OnBoardRamType =>
              // TODO: Is this correct?
              // This is highly inefficient (only 1 pending request) and should be optimised in rewriting
              // This case only occurs when the previous case MapOrderedStream(ArchLambda(Read ... did not fire
              JoinOrderedStream(
                ReadAsync(
                  ParamUse(memCtrlFuncs(ramType.ramId).head._1),
                  _generate(memAllocation, memCtrlFuncs),
                  Repeat(
                    _generate(input, memCtrlFuncs),
                    1
                  )
                )
              )
            case _ => ??? // never happens
          }
          case _ => throw MalformedExprException("Allocated memory must be of type ArrayType!")
        }
        case Write(memAllocation, input, _) => memAllocation.t match {
          case RamArrayType(_, _, ramType) => ramType match {
            // TODO: Change made here
            case _: BlockRamTypeT =>
              WriteSync(
                ParamUse(memCtrlFuncs(ramType.ramId).head._2),
                _generate(memAllocation, memCtrlFuncs),
                _generate(input, memCtrlFuncs)
              )

            case _: BankedBlockRamTypeT =>
              WriteSyncBanked(
                ParamUse(memCtrlFuncs(ramType.ramId).head._2),
                _generate(memAllocation, memCtrlFuncs),
                _generate(input, memCtrlFuncs)
              )

            case HostRamType | OnBoardRamType =>
              // This is highly inefficient (only 1 pending request) and should be optimised in rewriting
              // This case only occurs when the previous case ReduceOrderedStream(ArchLambda(Write ... did not fire
              WriteAsync(
                ParamUse(memCtrlFuncs(ramType.ramId).head._2),
                _generate(memAllocation, memCtrlFuncs),
                Repeat(
                  _generate(input, memCtrlFuncs),
                  1
                )
              )
            case _ => throw new RuntimeException("never happens")
          }
          case _ => throw MalformedExprException("Allocated memory must be of type ArrayType")
        }
        case _ => throw MalformedExprException("Cannot lower memory expressions if some memory expressions are already lowered!")
      }
      case expr: Expr => expr.build(expr.children.map({
        case e: Expr => _generate(e, memCtrlFuncs)
        case t: Type => t.visitAndRebuild({
          case at: RamArrayTypeT => baseAddrTypes.get(at.memoryLocation.ramId) match {
            case Some(bat) => bat
            case None => throw MalformedExprException("Unknown unallocated memory has been detected")
          }
          case t => t
        })
      }))
    }

    //TODO: Change made here
    val memExpr = _generate(exprChecked,
      Map(HostRamType.ramId -> Seq((hostRamReadCtrlParam, hostRamWriteCtrlParam)),
        OnBoardRamType.ramId -> Seq((onBoardRamBankReadCtrlParam, onBoardRamBankWriteCtrlParam))))

//    var usesHostRam: Boolean = false
//    var usesOnBoardRam: Boolean = false

    var usesHostRamRead: Boolean = false
    var usesHostRamWrite: Boolean = false

    var usesOnBoardRamRead: Boolean = false
    var usesOnBoardRamWrite: Boolean = false

    memExpr.visit({
//      case pd: ParamDef if pd.id == hostRamReadCtrlParam.id || pd.id == hostRamWriteCtrlParam.id =>
//        usesHostRam = true
//      case pd: ParamDef if pd.id == onBoardRamBankReadCtrlParam.id || pd.id == onBoardRamBankWriteCtrlParam.id =>
//        usesOnBoardRam = true
      case pd: ParamDef if pd.id == hostRamReadCtrlParam.id =>
        usesHostRamRead = true
      case pd: ParamDef if pd.id == hostRamWriteCtrlParam.id =>
        usesHostRamWrite = true
      case pd: ParamDef if pd.id == onBoardRamBankReadCtrlParam.id =>
        usesOnBoardRamRead = true
      case pd: ParamDef if pd.id == onBoardRamBankWriteCtrlParam.id =>
        usesOnBoardRamWrite = true
      case _ =>
    })

    if (usesHostRamRead) {
      if (usesHostRamWrite) {
        if (usesOnBoardRamRead) {
          if (usesOnBoardRamWrite) {
            TypeChecker.check(Let(Seq(hostRamReadCtrlParam, hostRamWriteCtrlParam, onBoardRamBankParam),
                              Let(Seq(onBoardRamBankReadCtrlParam, onBoardRamBankWriteCtrlParam), memExpr, Seq(onBoardRamBankReadCtrlFunc, onBoardRamBankWriteCtrlFunc)),
                              Seq(hostRamReadCtrlFunc, hostRamWriteCtrlFunc, onBoardRamBankFunc)))
          } else{
            TypeChecker.check(Let(Seq(hostRamReadCtrlParam, hostRamWriteCtrlParam, onBoardRamBankParam),
                              Let(Seq(onBoardRamBankReadCtrlParam), memExpr, Seq(onBoardRamBankReadCtrlFunc)),
                              Seq(hostRamReadCtrlFunc, hostRamWriteCtrlFunc, onBoardRamBankFunc)))
          }
        } else{
          if (usesOnBoardRamWrite) {
            TypeChecker.check(Let(Seq(hostRamReadCtrlParam, hostRamWriteCtrlParam, onBoardRamBankParam),
                              Let(Seq(onBoardRamBankWriteCtrlParam), memExpr, Seq(onBoardRamBankWriteCtrlFunc)),
                              Seq(hostRamReadCtrlFunc, hostRamWriteCtrlFunc, onBoardRamBankFunc)))
          } else{
            TypeChecker.check(Let(Seq(hostRamReadCtrlParam, hostRamWriteCtrlParam),
                              memExpr,
                              Seq(hostRamReadCtrlFunc, hostRamWriteCtrlFunc)))
          }
        }
      } else {
        if (usesOnBoardRamRead) {
          if (usesOnBoardRamWrite) {
            TypeChecker.check(Let(Seq(hostRamReadCtrlParam, onBoardRamBankParam),
                              Let(Seq(onBoardRamBankReadCtrlParam, onBoardRamBankWriteCtrlParam), memExpr, Seq(onBoardRamBankReadCtrlFunc, onBoardRamBankWriteCtrlFunc)),
                              Seq(hostRamReadCtrlFunc, onBoardRamBankFunc)))
          } else{
            TypeChecker.check(Let(Seq(hostRamReadCtrlParam, onBoardRamBankParam),
                              Let(Seq(onBoardRamBankReadCtrlParam), memExpr, Seq(onBoardRamBankReadCtrlFunc)),
                              Seq(hostRamReadCtrlFunc, onBoardRamBankFunc)))
          }
        } else{
          if (usesOnBoardRamWrite) {
            TypeChecker.check(Let(Seq(hostRamReadCtrlParam, onBoardRamBankParam),
                              Let(Seq(onBoardRamBankWriteCtrlParam), memExpr, Seq(onBoardRamBankWriteCtrlFunc)),
                              Seq(hostRamReadCtrlFunc, onBoardRamBankFunc)))
          } else{
            TypeChecker.check(Let(Seq(hostRamReadCtrlParam),
                              memExpr,
                              Seq(hostRamReadCtrlFunc)))
          }
        }
      }
    } else {
      if (usesHostRamWrite) {
        if (usesOnBoardRamRead) {
          if (usesOnBoardRamWrite) {
            TypeChecker.check(Let(Seq(hostRamWriteCtrlParam, onBoardRamBankParam),
                                  Let(Seq(onBoardRamBankReadCtrlParam, onBoardRamBankWriteCtrlParam), memExpr, Seq(onBoardRamBankReadCtrlFunc, onBoardRamBankWriteCtrlFunc)),
                                  Seq(hostRamWriteCtrlFunc, onBoardRamBankFunc)))
          } else{
            TypeChecker.check(Let(Seq(hostRamWriteCtrlParam, onBoardRamBankParam),
                              Let(Seq(onBoardRamBankReadCtrlParam), memExpr, Seq(onBoardRamBankReadCtrlFunc)),
                              Seq(hostRamWriteCtrlFunc, onBoardRamBankFunc)))
          }
        } else{
          if (usesOnBoardRamWrite) {
            TypeChecker.check(Let(Seq(hostRamWriteCtrlParam, onBoardRamBankParam),
                              Let(Seq(onBoardRamBankWriteCtrlParam), memExpr, Seq(onBoardRamBankWriteCtrlFunc)),
                              Seq(hostRamWriteCtrlFunc, onBoardRamBankFunc)))
          } else{
            TypeChecker.check(Let(Seq(hostRamWriteCtrlParam),
                              memExpr,
                              Seq(hostRamWriteCtrlFunc)))
          }
        }
      } else {
        if (usesOnBoardRamRead) {
          if (usesOnBoardRamWrite) {
            TypeChecker.check(Let(Seq(onBoardRamBankParam),
                              Let(Seq(onBoardRamBankReadCtrlParam, onBoardRamBankWriteCtrlParam), memExpr, Seq(onBoardRamBankReadCtrlFunc, onBoardRamBankWriteCtrlFunc)),
                              Seq(onBoardRamBankFunc)))
          } else{
            TypeChecker.check(Let(Seq(onBoardRamBankParam),
                              Let(Seq(onBoardRamBankReadCtrlParam), memExpr, Seq(onBoardRamBankReadCtrlFunc)),
                              Seq(onBoardRamBankFunc)))
          }
        } else{
          if (usesOnBoardRamWrite) {
            TypeChecker.check(Let(Seq(onBoardRamBankParam),
                              Let(Seq(onBoardRamBankWriteCtrlParam), memExpr, Seq(onBoardRamBankWriteCtrlFunc)),
                              Seq(onBoardRamBankFunc)))
          } else{
            TypeChecker.check(memExpr)
          }
        }
      }
    }

//    if (usesHostRam) {
//      // only add Let with HostRam functions if host ram is accessed at all
//      if (usesOnBoardRam) {
//        TypeChecker.check(
//          Let(
//            Seq(hostRamReadCtrlParam, hostRamWriteCtrlParam, onBoardRamBankParam),
//            Let(
//              Seq(onBoardRamBankReadCtrlParam, onBoardRamBankWriteCtrlParam),
//              memExpr,
//              Seq(onBoardRamBankReadCtrlFunc, onBoardRamBankWriteCtrlFunc)
//            ),
//            Seq(hostRamReadCtrlFunc, hostRamWriteCtrlFunc, onBoardRamBankFunc)))
//      } else
//        TypeChecker.check(Let(Seq(hostRamReadCtrlParam, hostRamWriteCtrlParam), memExpr, Seq(hostRamReadCtrlFunc, hostRamWriteCtrlFunc)))
//    } else
//
//      if (usesOnBoardRam)
//        TypeChecker.check(
//          Let(
//            Seq(onBoardRamBankParam),
//            Let(
//              Seq(onBoardRamBankReadCtrlParam, onBoardRamBankWriteCtrlParam),
//              memExpr,
//              Seq(onBoardRamBankReadCtrlFunc, onBoardRamBankWriteCtrlFunc)
//            ),
//            Seq(onBoardRamBankFunc)))
//      else
//        TypeChecker.check(memExpr)

  }
}
