package backend.hdl.arch.mem

import core._
import lift.arithmetic._

case object MemAddrStrategyKind extends Kind

sealed trait MemAddrStrategyT extends MetaTypeT {

  def apply(virtOffset: ArithExpr): ArithExpr

  def toArithLambda: ArithLambdaType = {
    val v = ArithTypeVar(StartFromRange(0))
    ArithLambdaType(v, apply(v))
  }

  override def kind: Kind = MemAddrStrategyKind

  override def superType: Type = AnyType()

  override def children: Seq[Type] = Seq()

  override def build(newChildren: Seq[ShirIR]): Type = this
}

final case class Padding(endPoint: ArithExpr) extends MemAddrStrategyT {
  override def apply(virtOffset: ArithExpr): ArithExpr =
    (virtOffset gt (endPoint - 1)) ?? -1 !! virtOffset
}

final case class Clipping(endPoint: ArithExpr) extends MemAddrStrategyT {
  override def apply(virtOffset: ArithExpr): ArithExpr = {
    val c = endPoint - 1
    (virtOffset gt c) ?? c !! virtOffset
  }
}

final case class Mirroring(endPoint: ArithExpr) extends MemAddrStrategyT {
  override def apply(virtOffset: ArithExpr): ArithExpr = {
    val c = endPoint - 1
    (virtOffset gt c) ?? (2 * c - virtOffset) !! virtOffset
  }
}

final case class ComplexAddressing(fun: ArithLambdaType) extends MemAddrStrategyT {
  override def apply(virtOffset: ArithExpr): ArithExpr =
    ArithExpr.substitute(this.fun.b.ae, Predef.Map(this.fun.v.ae -> virtOffset))

  override def toArithLambda: ArithLambdaType = this.fun
}
