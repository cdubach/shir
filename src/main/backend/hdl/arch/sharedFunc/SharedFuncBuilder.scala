package backend.hdl.arch.sharedFunc

import algo.{AlgoDataTypeT, AlgoFunTypeT, AlgoLambda}
import backend.hdl.HWDataTypeT
import backend.hdl.arch.ArchLambda
import core.commonSubtree.FunBuilder.insertSharedFunc
import core.commonSubtree.SubtreeChecker.{getSubtreeParams, selfScan}
import core.commonSubtree.SubtreeSelector.{rankByOverlapping, removeOverlapping}
import core.util.IRDotGraph
import core.{Expr, FunTypeT, FunctionCall, Lambda, Let, ParamDef, ParamUse, TypeChecker}

object SharedFuncBuilder {

  /** Find out which phase we are at (algo, arch, or unknown). */
  def determinePhase(expr: Expr): String = {
    var phase: String = "unknown"
    expr.visit {
      case _: AlgoDataTypeT => phase = "algo"
      case _: HWDataTypeT => phase = "arch"
      case _ =>
    }
    phase
  }

  /** Find out which phase we are at (algo, arch, or unknown). */
  def replaceLambda(expr: Expr, phase: String): Expr = expr match {
    case Lambda(p, body, _) if phase == "algo" => AlgoLambda(p, replaceLambda(body, phase))
    case Lambda(p, body, _) if phase == "arch" => ArchLambda(p, replaceLambda(body, phase))
    case _ => expr
  }

  /** Find out full function call. */
  def matchFunCall(expr: Expr, p: ParamDef): Boolean = expr match {
    case FunctionCall(fc @ FunctionCall(_, _, _) , _, _) => matchFunCall(fc, p)
    case FunctionCall(ParamUse(pd), _, _) if pd.id == p.id => true
    case _ => false
  }

  /** Find out full function call. */
  def replaceFunCall(expr: Expr, p: ParamDef, newP: ParamDef): Expr = expr match {
    case FunctionCall(fc@FunctionCall(_, _, _), input, _) =>
      val newFun = replaceFunCall(fc, p, newP)
      FunctionCall(newFun, input)
    case FunctionCall(ParamUse(pd), input, _) if pd.id == p.id =>
      FunctionCall(ParamUse(newP), input)
    case e => e
  }

  /** Replace shared Lambdas with CompilerPhase-specific one. */
  def specializeFunc(expr: Expr): Expr = {
    val phase = determinePhase(expr)
    expr.visitAndRebuild{
      case l @ Let(_, _, _ @ Lambda(_, _, _@(_: HWDataTypeT | _: AlgoFunTypeT)), _) => l
      case Let(p, body, arg @ Lambda(_, _, _: FunTypeT), _) =>
        val newArg = TypeChecker.check(replaceLambda(arg, phase))
        val newP = ParamDef(newArg.t)
        val newBody = body.visitAndRebuild{
          case e: Expr if matchFunCall(e, p) => replaceFunCall(e, p, newP)
          case e => e
        }.asInstanceOf[Expr]
        Let(newP, newBody, newArg)
      case e => e
    }.asInstanceOf[Expr]
  }

  /** Wrapper for detecting and inserting shared function. Just choice one function with max overlapping. */
  def createSharedFunc(expr: Expr): Expr = {
    val subtrees = selfScan(expr)
    val candidates = removeOverlapping(expr, subtrees)
    val params = getSubtreeParams(candidates.head)
    val sharedFuncExpr = insertSharedFunc(expr, candidates.head, params)
    specializeFunc(sharedFuncExpr)
  }

  /**
   * Wrapper for detecting and inserting shared function.
   *
   * @params expr: input expression.
   * @params numOfCandidates: how many function should be created.
   * @return expression with shared functions.
   */
  def createMultipleSharedFunc(expr: Expr, numOfCandidates: Int): Expr = {
    val subtrees = selfScan(expr)
    val allCandidates = rankByOverlapping(expr, subtrees)
    val chosenCandidates =
      if (allCandidates.length > numOfCandidates)
        allCandidates.dropRight(allCandidates.length - numOfCandidates)
      else
        allCandidates
    var resultExpr: Expr = expr
    chosenCandidates.foreach(e => {
      val params = getSubtreeParams(e)
      val sharedFuncExpr = insertSharedFunc(resultExpr, e, params)
      resultExpr = specializeFunc(sharedFuncExpr)
      IRDotGraph(resultExpr).show()
    })
    resultExpr
  }
}
