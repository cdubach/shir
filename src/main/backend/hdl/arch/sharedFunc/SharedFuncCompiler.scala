package backend.hdl.arch.sharedFunc

import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.rewrite.{PortOrderRules, WriteRules}
import core.{Expr, Marker, TypeChecker}
import core.compile.{CompilerPass, CompilerPhase}
import core.rewrite.{RewriteAll, RewriteStep}

object SharedFuncCompiler extends CompilerPass {
  override def phaseBefore: CompilerPhase = DeviceSpecificCompiler.phaseAfter

  override def run(expr: Expr): Expr = {
    val expr0 = TypeChecker.checkAssert(expr)
    //val expr1 = TypeChecker.check(createLetForWrite(expr0))
    val expr2 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(PortOrderRules.testRule)).apply(expr0))
    val expr3 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(WriteRules.removeLetWrite)).apply(expr2))
    removeGlobalTag(expr3)
  }

  private def removeGlobalTag(expr: Expr): Expr = expr.visitAndRebuild{
    case Marker(main, t) if t.s == "global" =>
      main
    case e => e
  }.asInstanceOf[Expr]
}