package backend.hdl.arch.sharedFunc

import backend.hdl.arch.device.DeviceSpecificCompiler
import backend.hdl.arch.rewrite.{MoveDownStreamToVectorRules, MoveUpVectorToStreamRules}
import backend.hdl.arch.rewrite.sharedFunc.{Map2InputRules, MergeFuncRules, MoveUpReadRules, PropagateFunRules, ReshapeFuncOutputRules}
import backend.hdl.arch.rewrite.transpose.MoveUpTransposeRules
import core.{Expr, TypeChecker}
import core.compile.{CompilerPass, CompilerPhase}
import core.rewrite.{RewriteAll, RewriteStep}

object MergeFuncCompiler extends CompilerPass {
  override def phaseBefore: CompilerPhase = GlobalFuncCompiler.phaseAfter

  override def run(expr: Expr): Expr = {
    val expr0 = TypeChecker.checkAssert(expr)
    // Handle vec2Stm
    val expr1 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(MoveUpVectorToStreamRules.mapFission) ++ ReshapeFuncOutputRules.get()).apply(expr0))
    val expr2 = TypeChecker.check(RewriteStep(RewriteAll(), MoveDownStreamToVectorRules.skipTupleStructure ++ Seq(MoveDownStreamToVectorRules.skipRepeat, MoveDownStreamToVectorRules.skipJoinStreamV2)).apply(expr1))
    val expr3 = TypeChecker.check(RewriteStep(RewriteAll(), Map2InputRules.get()).apply(expr2))
    val expr4 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(PropagateFunRules.mergeMapStm2Input, ReshapeFuncOutputRules.moveInStmToVec, ReshapeFuncOutputRules.moveOutVecToStm)).apply(expr3))
    val expr5 = TypeChecker.check(RewriteStep(RewriteAll(), MoveDownStreamToVectorRules.skipTupleStructure ++ Seq(MoveDownStreamToVectorRules.skipJoinStreamV2, MoveDownStreamToVectorRules.skipAlternate, MoveUpTransposeRules.skipMapNDFun)).apply(expr4))
    val expr6 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(MergeFuncRules.mergeNearbyCallsSingleInput)).apply(expr5))
    val expr7 = TypeChecker.check(RewriteStep(RewriteAll(), MoveUpReadRules.get() ++ PropagateFunRules.get()).apply(expr6))
    expr7
  }
}
