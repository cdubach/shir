package backend.hdl.arch

import algo.{SeqTypeT, StoreResult}
import backend.hdl._
import backend.hdl.arch.mem._
import backend.hdl.specs.FPGA
import core._
import core.compile.{CompilerPass, CompilerPhase}

import scala.collection.immutable.SortedMap

object ArchCompiler extends CompilerPass {

  override def phaseBefore: CompilerPhase = CompilerPhase.first()

  override def run(expr: Expr): Expr =
    TypeChecker.check(
      _generate(
        TypeChecker.checkAssert(
          expr match {
            case StoreResult(_, _) => expr
            // add parent 'StoreResult' expression, if not available and host ram is accessed
            case _ if hasHostRamInput(expr) => StoreResult(expr)
            case _ => expr
          }
        )
      )
    )

  private def hasHostRamInput(expr: Expr): Boolean = {
    expr.visit({
      case algo.Input(_, _, _, _) => return true
      case _ =>
    })
    false
  }

  private def _generate(expr: Expr): Expr = expr match {
    case archExpr: ArchExpr => archExpr
    case memExpr: MemExpr => memExpr
    case macroExpr: algo.AlgoMacroExpr => _generate(macroExpr.expand())
    case algoExpr: algo.AlgoExpr => algoExpr match {
      case algo.Input(identifier, _, desiredDimensions: Seq[ArithTypeT], t) => mem.Input(identifier, _generate(t).asInstanceOf[UnorderedStreamTypeT].leafType, desiredDimensions, TextType(t + ""))
      case algo.StoreResult(input, _) => mem.StoreResult(_generate(input))
      case algo.Buffer(input, t:SeqTypeT) =>
      if(t.dimensions.length == 2)
        BufferStream2DInBlockRam(_generate(input))
      else
        BufferStreamInBlockRam(_generate(input))
      case algo.BufferHost(input, identifier, _) => BufferStreamInHostRam(_generate(input), identifier)
      case algo.ConstantInteger(value, valueType) => ConstantValue(value, Some(_generate(valueType)))
      case algo.ConstantSeq(value, valueType) => VectorToOrderedStream(ConstantVector(value.init, Some(_generate(valueType.asInstanceOf[SeqTypeT].et))))
      case algo.CounterInteger(start, increment, loop, dimensions, repetitions, valueType) => CounterInteger(start, increment, loop, dimensions, repetitions, Some(_generate(valueType.leafType)))
      case algo.TruncInteger(input, targetBitWidth, _) => TruncInteger(_generate(input), targetBitWidth)
      case algo.ResizeInteger(input, targetBitWidth, _) => ResizeInteger(_generate(input), targetBitWidth)
      case algo.Item(input, _) => OrderedStreamToItem(_generate(input))
      case algo.Repeat(input, len, _) => Repeat(_generate(input), len)
      case algo.Sink(input, _) => Sink(_generate(input))
      case algo.Id(input, _) => Id(_generate(input))
      case algo.Signed(input, _) => Signed(_generate(input))
      case algo.Unsigned(input, _) => Unsigned(_generate(input))
      case algo.ClipBankersRound(input, lowElements, highElements, _) => ClipBankersRound(_generate(input), lowElements, highElements)
      case algo.ClipBankersRoundShift(input, _) => ClipBankersRoundShift(_generate(input))
      case algo.QScale(input, _) => QScale(_generate(input))
      case algo.Add(input, _: algo.IntTypeT) => AddInt(_generate(input))
      case algo.Add(input, _: algo.FloatTypeT) => AddFloat(_generate(input))
      case algo.Add(_, _) => throw MalformedExprException("Unknown Type for Add")
      case algo.Sub(input, _) => SubInt(_generate(input))
      case algo.Mul(input, _: algo.IntTypeT) => MulInt(_generate(input))
      case algo.Mul(input, _: algo.FloatTypeT) => MulFloat(_generate(input))
      case algo.Mul(_, _) => throw MalformedExprException("Unknown Type for Mul")
      case algo.Max(input, _: algo.IntTypeT) => MaxInt(_generate(input))
      case algo.Max(_, _) => throw MalformedExprException("Unknown Type for Max")
      case algo.Min(input, _: algo.IntTypeT) => MinInt(_generate(input))
      case algo.Min(_, _) => throw MalformedExprException("Unknown Type for Min")
      case algo.Select(input, n, selection, _) => Select(_generate(input), n, selection)
      case algo.Map(f, input, _) => MapOrderedStream(_generate(f), _generate(input))
      case algo.MapAsync(f, inputs, _) => MapAsyncOrderedStream(_generate(f), inputs.map(_generate): _*)
      case algo.Fold(f, input, _) => FoldOrderedStream(_generate(f), _generate(input))
      case algo.Reduce(f, initialValue, input, _) => ReduceOrderedStream(_generate(f), _generate(initialValue), _generate(input))
      case algo.Slide(input, windowWidth, stepWidth, _) => {
        val newInput = TypeChecker.check(_generate(input))
        val origDims = newInput.t.asInstanceOf[OrderedStreamTypeT].dimensions
        if(origDims.length == 1) {
          MapOrderedStream(VectorToOrderedStream.asFunction(), SlideOrderedStream(newInput, windowWidth, stepWidth))
        } else {
          val joinInner = TypeChecker.check(MapOrderedStream({
            val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(param, OrderedStreamToVector(JoinStreamAll(ParamUse(param))))
          }, newInput))
          val slidData = TypeChecker.check(MapOrderedStream(2, VectorToOrderedStream.asFunction(),
            MapOrderedStream(VectorToOrderedStream.asFunction(), SlideOrderedStream(joinInner, windowWidth, stepWidth))))
          val splitOut = TypeChecker.check(MapOrderedStream(2, {
            val param = ParamDef(slidData.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(param, SplitStreamAll(ParamUse(param), origDims.dropRight(2).reverse))
          }, slidData))
          splitOut
        }
      }
      case algo.SlideGeneral(input, windowWidth, stepSize, _) => SlideGeneralOrderedStream(_generate(input), windowWidth, stepSize)
      case algo.SeqToUpperBoundedSeq(input, maxLength, _) => OrderedStreamToUpperBoundedStream(_generate(input), maxLength)
      case algo.Tuple(inputs, _) => Tuple(inputs.map(_generate): _*)
      case algo.FlipTuple(input, n, _) => FlipTuple(_generate(input), n)
      case algo.Zip(input, n, _) => ZipOrderedStream(_generate(input), n)
      case algo.Split(input, chunkSize, _) => SplitOrderedStream(_generate(input), chunkSize)
      case algo.Join(input, _) => JoinStream(_generate(input))
      case algo.Drop(input, firstElements, lastElements, _) => DropOrderedStream(_generate(input), firstElements, lastElements)
      case algo.Pad(input, firstElements, lastElements, value, _) => PadOrderedStream(_generate(input), firstElements, lastElements, value)
      case algo.Concat(input1, input2, dimension, _) => ConcatOrderedStream(_generate(input1), _generate(input2), dimension)
      case algo.Permute(input, arith, _) => {
        val innerType = input.t.asInstanceOf[SeqTypeT].et
        if(innerType.isInstanceOf[SeqTypeT]) {
          val inputInnerVec = MapOrderedStream(OrderedStreamToVector.asFunction(), _generate(input))
          val permuted = VectorToOrderedStream(PermuteVector(OrderedStreamToVector(inputInnerVec), arith))
          MapOrderedStream(VectorToOrderedStream.asFunction(), permuted)
        } else {
          VectorToOrderedStream(PermuteVector(OrderedStreamToVector(_generate(input)), arith))
        }
      }
      case algo.TransposeND(input, dimOrder, _) => TypeChecker.check(TransposeNDOrderedStream(_generate(input), dimOrder))
      case algo.Ifelse(f1, f2, input, _) => Ifelse(_generate(f1), _generate(f2), _generate(input))
    }
    case algo.AlgoLambda(param, body, _) => ArchLambda(_generate(param).asInstanceOf[ParamDef], _generate(body))
    case coreExpr: CoreExpr => coreExpr.build(coreExpr.children.map({
      case e: Expr => _generate(e)
      case t: Type => _generate(t)
    }))
    case other => throw MalformedExprException("cannot generate Arch expression from " + other)
  }

  private def _generate(t: Type): Type = t match {
    case hwDataType: HWDataTypeT => hwDataType
    case algoType: algo.AlgoDataTypeT => _generateHWDataType(algoType)
    case algo.AlgoFunType(inType, outType) => HWFunType(_generate(inType), _generate(outType))
    case coreType: CoreType => coreType.build(coreType.children.map(_generate))
    case other => other //throw new MalformedExprException("cannot generate HW type from " + other)
  }

  private def _generateHWDataType(t: algo.AlgoDataTypeT): HWDataTypeT = t match {
    case _: TypeVarT => throw MalformedExprException("cannot generate IR due to remaining type var in expression: " + t)
      // TODO generate vector / or stream type here
    case algo.SeqType(et, len) => OrderedStreamType(_generateHWDataType(et).asInstanceOf[NonArrayTypeT], len)
    case algo.UpperBoundedSeqType(et, len) => UpperBoundedStreamType(_generateHWDataType(et).asInstanceOf[NonArrayTypeT], len)
    case algo.NamedTupleType(namedTypes) => NamedTupleType(namedTypes.map(p => (p._1, _generateHWDataType(p._2))))//asInstanceOf[BasicDataTypeT])))
    case algo.SignedIntType(width) => SignedIntType(width)
    case algo.IntType(width) => IntType(width)
    case algo.FloatHalfType() => FloatHalfType()
    case algo.FloatSingleType() => FloatSingleType()
    case algo.FloatDoubleType() => FloatDoubleType()
    case algo.AlgoDataType() | algo.AbstractScalarType() | algo.FloatType() | algo.AbstractCollectionType() =>
      throw MalformedExprException("Cannot generate Arch type from abstract type")
  }
}
