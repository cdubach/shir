package backend.hdl.arch.tiling

import backend.hdl.arch.rewrite.{CleanupRules, MoveDownSplitStreamRules, MoveDownStreamToVectorRules, MoveDownTruncIntegerRules, MoveUpDropStreamRules, MoveUpJoinStreamRules, MoveUpJoinVectorRules, MoveUpOrderedToUnorderedStreamRules, MoveUpVectorToStreamRules, RemoveStreamVectorConversionRules}
import backend.hdl.arch.rewrite.conv.{MoveDownSlideGeneralStreamRules, MoveDownSlideVectorRules}
import backend.hdl.arch.rewrite.tiling.SlideTilingRules
import backend.hdl.arch.rewrite.transpose.{MoveDownTransposeRules, MoveDownTransposeWithVecToStmRules, MoveUpTransposeRules, TransposeConversionRules}
import backend.hdl.arch.sharedFunc.{GlobalFuncCompiler, MergeFuncCompiler}
import core.compile.{CompilerPass, CompilerPhase}
import core.rewrite.{RewriteAll, RewriteStep}
import core.util.IRDotGraph
import core.{Expr, TypeChecker}

object TranspositionCompiler extends CompilerPass {
  override def phaseBefore: CompilerPhase = MergeFuncCompiler.phaseAfter

  override def run(expr: Expr): Expr = {
    val expr0 = TypeChecker.checkAssert(expr)
    // Clear Split and Slide before moving down transpositions
    val expr1 = TypeChecker.check(RewriteStep(RewriteAll(), MoveDownSplitStreamRules.mergeIntoReadAddress ++ TransposeConversionRules.get() ++
      Seq(MoveUpTransposeRules.skipMapNDSplit, MoveDownStreamToVectorRules.skipJoinStream, MoveUpVectorToStreamRules.skipSplitStream, RemoveStreamVectorConversionRules.removeConversion4) ++ MoveUpOrderedToUnorderedStreamRules.get()).apply(expr0))
    val expr21 = TypeChecker.check(RewriteStep(RewriteAll(), MoveDownSlideGeneralStreamRules.get() ++ MoveDownSlideVectorRules.get() ++ SlideTilingRules.get()).apply(expr1))
    val expr2 = TypeChecker.check(RewriteStep(RewriteAll(), MoveDownSplitStreamRules.mergeIntoReadAddress ++ MoveDownSplitStreamRules.skipTupleStructure).apply(expr21))
    // Clean Vec-Stm pairs
    val expr3 = TypeChecker.check(RewriteStep(RewriteAll(), MoveUpJoinStreamRules.all(None) ++ MoveUpVectorToStreamRules.solveTranspose ++ RemoveStreamVectorConversionRules.all(None) ++
      Seq(MoveUpJoinStreamRules.mapFusionMapParam, MoveUpDropStreamRules.mapFusionMapParam, MoveDownStreamToVectorRules.skipDropStream, MoveDownStreamToVectorRules.skipRepeat, CleanupRules.removeEmptyMap, MoveUpJoinVectorRules.reorderJoins)).apply(expr2))
    // Move down transpositions
    val expr4 = TypeChecker.check(RewriteStep(RewriteAll(), MoveDownTransposeRules.get() ++ MoveDownTransposeRules.mergeIntoReadAddress ++ MoveDownTransposeWithVecToStmRules.get()).apply(expr3))
    // Optimize AB Map Order and move up transpositions
    val expr5 = TypeChecker.check(RewriteStep(RewriteAll(), MoveUpTransposeRules.get() ++ MoveUpTransposeRules.mergeIntoWriteAddress).apply(expr4))
    expr5
  }
}
