package backend.hdl.arch

import backend.hdl.arch.device.{DeviceSpecificExpr, Mul2AddInt}
import backend.hdl.arch.mem._
import backend.hdl.specs.FPGA
import backend.hdl.{HWDataTypeT, OrderedStreamTypeT, VectorTypeT, arch}
import core._
import core.util.Log

object PerformanceModel {

  private final val COUNTER_PERFORMANCE = PerformanceEstimate(area = Area(alms = 1))
  private final val ADDER_PERFORMANCE = PerformanceEstimate(area = Area(alms = 1))

  def analyse(expr: Expr): PerformanceEstimate = expr match {
      // TODO TODO arbitration is not considered
    case ae: ArchExpr => ae match {
      case ConstantValue(_, _) => PerformanceEstimate()
      case CounterInteger(_, _, _, _, _, _) => PerformanceEstimate(area = Area(alms = 53.5, dedicatedLogicRegisters = 74)) // TODO scale with dimensions
      case Repeat(input, _, _) => analyse(input) -> COUNTER_PERFORMANCE
      case RepeatHidden(input, _, _) => analyse(input) -> COUNTER_PERFORMANCE
      case VectorGenerator(input, _, _) => analyse(input)
      case Sink(input, _) => analyse(input)
      case Registered(input, _) => analyse(input) -> PerformanceEstimate() // TODO
      case Delay(input, cycles, _) => analyse(input) -> PerformanceEstimate() // TODO
      case TestBench(input, _) => analyse(input)
      case Id(input, _) => analyse(input)
      case AddInt(input, _) => analyse(input) -> ADDER_PERFORMANCE
      case AddFloat(input, _) => analyse(input) -> ADDER_PERFORMANCE // TODO more complex!
      case MulInt(input, _) => analyse(input) ->
        PerformanceEstimate(area = Area(dsps = 1)) // TODO
      case MulFloat(input, _) => analyse(input) ->
        PerformanceEstimate(area = Area(dsps = 1)) // TODO
      case Select2(input, _, _) => analyse(input) ->
        PerformanceEstimate(area = Area(alms = 1, dedicatedLogicRegisters = 1))
      case Select3(input, _, _) => analyse(input) ->
        PerformanceEstimate(area = Area(alms = 1, dedicatedLogicRegisters = 1))
      case MapOrderedStream(function, input, _) => analyse(input) -> (analyse(function) || COUNTER_PERFORMANCE || COUNTER_PERFORMANCE)
      case MapSimpleOrderedStream(function, input, _) => analyse(input) -> (analyse(function) || COUNTER_PERFORMANCE || COUNTER_PERFORMANCE)
      case MapUnorderedStream(function, input, _) => analyse(input) -> (analyse(function) || COUNTER_PERFORMANCE || COUNTER_PERFORMANCE)
      case MapVector(function, input, _) =>
        val inputType = input.t.asInstanceOf[VectorTypeT]
        analyse(input) -> Seq.fill(inputType.len.ae.evalInt)(analyse(function)).reduce((ar1, ar2) => ar1 || ar2)
      case FoldOrderedStream(function, input, _) =>
        val inputType = input.t.asInstanceOf[OrderedStreamTypeT]
        analyse(input) -> analyse(function) -> PerformanceEstimate(consumeRate = inputType.len.ae.evalInt, innerLatency = 1)
      case ReduceOrderedStream(function, _, input, _) =>
        val inputType = input.t.asInstanceOf[OrderedStreamTypeT]
        analyse(input) -> analyse(function) -> PerformanceEstimate(consumeRate = inputType.len.ae.evalInt, innerLatency = 1)
      case SlideOrderedStream(input, windowWidth, stepSize, _) => analyse(input) ->
        ??? // TODO PerformanceEstimate(area = Area(alms = (62, 62), dedicatedLogicRegisters = (8, 8)), innerLatency = windowWidth.ae.evalInt)
      case SlideGeneralOrderedStream(_, _, _, _) => ???
      case SlideVector(input, windowWidth, stepSize, _) => analyse(input) -> ???
      case Alternate(f1, f2, input, _) => analyse(input) -> (analyse(f1) || analyse(f2)) ->
        PerformanceEstimate(area = Area(alms = 5.5))
      case VectorToTuple2(input, _) => analyse(input)
      case VectorToOrderedStream(input, _) =>
        val inputType = input.t.asInstanceOf[VectorTypeT]
        analyse(input) -> PerformanceEstimate(consumeRate = 1/inputType.len.ae.evalInt)
      case OrderedStreamToVector(input, _) =>
        val inputType = input.t.asInstanceOf[OrderedStreamTypeT]
        analyse(input) -> PerformanceEstimate(consumeRate = inputType.len.ae.evalInt) // all elements of the ingoing stream produce 1 output vector
      case IndexUnorderedStream(input, _) => analyse(input) ->
        PerformanceEstimate(area = Area(alms = 1))
//      case UnorderedStreamToOrderedStream(input, _) =>
//        val inputType = input.t.asInstanceOf[UnorderedStreamTypeT]
//        analyse(input) ->
//          PerformanceEstimate(
//            1,
//            RegisteredCriticalPath(2, 2, 2),
//            inputType.len.ae.evalInt, // worst case latency! arises when the first element comes in last
//            inputType.bitWidth.ae.evalInt + 100 // the entire stream has to be buffered
//          )
      case OrderedStreamToUnorderedStream(input, _) => analyse(input) -> COUNTER_PERFORMANCE
      case UnorderedStreamToOrderedStream(input, _) => analyse(input) -> ???
      case Tuple2(input1, input2, _) => (analyse(input1) || analyse(input2)) ->
        PerformanceEstimate(area = Area(alms = 0.5))
      case Tuple3(input1, input2, input3, _) => (analyse(input1) || analyse(input2) || analyse(input3)) ->
        PerformanceEstimate(area = Area(alms = 0.5))
      case FlipTuple2(input, _) => analyse(input)
      case Zip2OrderedStream(input, _) => analyse(input)
      case Zip3OrderedStream(input, _) => analyse(input)
      case ZipVector(input, _) => analyse(input)
      case SplitOrderedStream(input, _, _) => analyse(input) ->
        PerformanceEstimate(area = Area(alms = 62, dedicatedLogicRegisters = 8))
      case SplitVector(input, _, _) => analyse(input)
      case JoinOrderedStream(input, _) => analyse(input)
      case JoinUnorderedStream(input, _) => analyse(input) // TODO also contains * and +
      case JoinVector(input, _) => analyse(input)
      case TransposeNDOrderedStream(input, _, _) => analyse(input)
      case DropOrderedStream(input, firstElements, _, t) =>
        val inputLength = input.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt
        val outputLength = t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt
        analyse(input) -> PerformanceEstimate(consumeRate = inputLength / outputLength, innerLatency = firstElements.ae.evalInt)
      case DropVector(input, _, _, _) => analyse(input)
      case ConcatOrderedStream(input1, input2, _, _) => analyse(input1) || analyse(input2)
      case ConcatVector(input, _) => analyse(input)
      case RotateVector(input, _, _) => analyse(input) // TODO
      case PermuteVector(input, _, _) => analyse(input) // TODO
    }
    /* memory related expressions */
    case MemoryAllocation(_, _, _, _, _) => PerformanceEstimate()
    case MemoryRegion(_, _, _, _, _, _) => PerformanceEstimate()
    case Read(memAllocation, input, _) => analyse(memAllocation) || analyse(input)
    case Write(memAllocation, input, _) => analyse(memAllocation) || analyse(input)
    case ReadSync(memoryController, baseAddr, input, _) => analyse(memoryController) || analyse(baseAddr) || analyse(input) // TODO
    case WriteSync(memoryController, baseAddr, input, _) => analyse(memoryController) || analyse(baseAddr) || analyse(input) // TODO
    case ReadSyncMemoryController(ram, input, _) => analyse(ram) || analyse(input) // TODO
    case WriteSyncMemoryController(ram, input, _) => analyse(ram) || analyse(input) // TODO
    case BlockRamBuffer(input, _, len, t: HWDataTypeT) =>
      val ramBlocks = FPGA.current.calcRamBlocks(t.bitWidth, len).ae.evalInt
      Log.info(this, "using " + ramBlocks + " ram blocks for " + len.ae.evalInt + " times " + t)

      val alms = t.bitWidth.ae.evalInt * 7 / 4 + // minimum control logic for ram
        (if (ramBlocks > 0) 0 else // if no blockram is used, put memory into ALMs!
          t.bitWidth.ae.evalInt * len.ae.evalInt * .75)

      val blockMemoryBits: Int = if (ramBlocks == 0) 0 else t.bitWidth.ae.evalInt * len.ae.evalInt

      analyse(input) ->
      PerformanceEstimate(area = Area(alms = alms, blockMemoryBits = blockMemoryBits, ramBlocks = ramBlocks))
    case ReadSyncBanked(_, _, _, _) => ???
    case WriteSyncBanked(_, _, _, _) => ???
    case ReadSyncBankedMemoryController(_, _, _, _) => ???
    case WriteSyncBankedMemoryController(_, _, _) => ???
    case SyncBankedBuffer(_, _, _, _, _) => ???
    case ReadAsync(memoryController, baseAddr, input, _) => (analyse(memoryController) || analyse(baseAddr) || analyse(input)) ->
      ???
    case ReadAsyncOrdered(memoryController, baseAddr, input, t: OrderedStreamTypeT) =>
      val ramBlocks = FPGA.current.calcRamBlocks(t.et.bitWidth, t.len).ae.evalInt
      val blockMemoryBits: Int = if (ramBlocks == 0) 0 else t.et.bitWidth.ae.evalInt * t.len.ae.evalInt
      (analyse(memoryController) || analyse(baseAddr) || analyse(input)) ->
        PerformanceEstimate(area = Area(alms = 740, blockMemoryBits = blockMemoryBits, ramBlocks = ramBlocks))
    case WriteAsync(memoryController, baseAddr, input, _) => (analyse(memoryController) || analyse(baseAddr) || analyse(input)) ->
      PerformanceEstimate(area = Area(alms = 45.5, dedicatedLogicRegisters = 40))
    case ReadHostMemoryController(input, _, _, _) => analyse(input) ->
      PerformanceEstimate(area = Area(alms = 29.9, dedicatedLogicRegisters = 48))
    case WriteHostMemoryController(input, _, _, _) => analyse(input) ->
      PerformanceEstimate(area = Area(alms = 30.5, dedicatedLogicRegisters = 51))
    case ReadLocalMemoryController(_, _, _) => ???
    case WriteLocalMemoryController(_, _, _) => ???
    case OnBoardRamBuffer(_, _, _, _, _, _) => ???
    /* memory related expressions */
    case Mul2AddInt(input, _) => analyse(input) ->
      PerformanceEstimate(area = Area(alms = 11, dsps = 1))
    case TypeLambda(body, _) => analyse(body)
    case TypeFunctionCall(tf, _, _) => analyse(tf)
    case l: LambdaT => analyse(l.param) -> analyse(l.body) // TODO add Arbiter
    case FunctionCall(f, arg, _) => analyse(arg) -> analyse(f)
    case ParamDef(_, _) => PerformanceEstimate()
    case ParamUse(p) => analyse(p)
    case Conversion(input, _) => analyse(input)
    case Marker(input, _) => analyse(input)
  }

}

/**
  *
  * @param consumeRate represents 1/throughput = consumed input words per output word
  * @param area
  * @param innerLatency additional inner delay (in cycles) ignoring the delay caused by the (different) consume rates!
  *     e.g. "reduce stream" (of length n) may have an inner latency of 0 although its output is provided n cycles after the input has arrived
  * @param criticalPath the critical path information
  */
case class PerformanceEstimate(
    consumeRate: Double = 1,
//    hostRamReads: Int = 0,
//    hostRamWrites: Int = 0,
    area: Area = Area(),
    private val innerLatency: Double = 0,
    criticalPath: CriticalPath = UnregisteredCriticalPath()) {

  final def latency: Double = consumeRate + innerLatency

  def get(property: PerformanceEstimate.Value): Double = property match {
    case PerformanceEstimate.CONSUME_RATE => consumeRate
    case PerformanceEstimate.AREA => area.dsps // TODO
    case PerformanceEstimate.LATENCY => latency
    case PerformanceEstimate.CRITICAL_PATH => criticalPath.length
  }

  /**
    * sequential combination of blocks (for example, latency is added here)
   */
  def ->(that: PerformanceEstimate): PerformanceEstimate =
    PerformanceEstimate(
      this.consumeRate * that.consumeRate,
      this.area.add(that.area),
      this.innerLatency + that.innerLatency,
      this.criticalPath.add(that.criticalPath)
    )

  /**
    * parallel combination of blocks (for example, the max latency is taken here)
    */
  def ||(that: PerformanceEstimate): PerformanceEstimate =
    PerformanceEstimate(
      Math.max(this.consumeRate, that.consumeRate),
      this.area.add(that.area),
      Math.max(this.innerLatency, that.innerLatency),
      this.criticalPath.max(that.criticalPath)
    )

  override def toString: String = "Throughput: " + 1.0/consumeRate + " words/cycle; Area: " + area + "; Latency: " + latency + "; Critical Path: " + criticalPath.length
}

object PerformanceEstimate extends Enumeration {
  type Margin = Value
  val CONSUME_RATE, CRITICAL_PATH, LATENCY, AREA = Value
}

case class PerformanceEstimateOrdering(property: PerformanceEstimate.Value) extends Ordering[PerformanceEstimate] {
  override def compare(a: PerformanceEstimate, b: PerformanceEstimate): Int = {
    val primaryPropertyDiff = b.get(property).compare(a.get(property))
    val consumeRateDiff = b.consumeRate.compare(a.consumeRate)
    val critPathDiff = b.criticalPath.length.compare(a.criticalPath.length)
    val latencyDiff = b.latency.compare(a.latency)
    val areaDiff = b.area.compare(a.area)

    // this ordering determines the priority of each property
    // e.g. throughput (aka cycles per word) is second and therefore has the highest priority when comparing (after the selected primary property)
    if (primaryPropertyDiff != 0) primaryPropertyDiff
    else if (consumeRateDiff != 0) consumeRateDiff
    else if (latencyDiff != 0) latencyDiff
    else if (areaDiff != 0) areaDiff
    else critPathDiff
  }
}

final case class Area(alms: Double = 0, dedicatedLogicRegisters: Int = 0, blockMemoryBits: Int = 0, ramBlocks: Int = 0, dsps: Int = 0) extends Ordered[Area] {
  override def compare(that: Area): Int = this.dsps.compare(that.dsps) // TODO add other values to this comparison

  def add(that: Area): Area = {
    Area(
      this.alms + that.alms,
      this.dedicatedLogicRegisters + that.dedicatedLogicRegisters,
      this.blockMemoryBits + that.blockMemoryBits,
      this.ramBlocks + that.ramBlocks,
      this.dsps + that.dsps
    )
  }

  override def toString: String = this.getClass.getSimpleName + "(Alms=" + alms + "; Registers=" + dedicatedLogicRegisters + "; BlockMemoryBits=" + blockMemoryBits + "; RamBlocks=" + ramBlocks + "; DSPs=" + dsps + ")"

}

sealed trait CriticalPath {
  def length: Double
  def add(that: CriticalPath): CriticalPath = (this, that) match {
    case (UnregisteredCriticalPath(length1), UnregisteredCriticalPath(length2)) =>
      UnregisteredCriticalPath(length1 + length2)
    case (RegisteredCriticalPath(pathBefore1, pathAfter1, innerCriticalPath1), RegisteredCriticalPath(pathBefore2, pathAfter2, innerCriticalPath2)) =>
      val newConnectingPath = pathAfter1 + pathBefore2
      val newInnerCriticalPath = Math.max(innerCriticalPath1, innerCriticalPath2)
      RegisteredCriticalPath(pathBefore1, pathAfter2, Math.max(newConnectingPath, newInnerCriticalPath))
    case (UnregisteredCriticalPath(length), RegisteredCriticalPath(pathBefore, pathAfter, innerCriticalPath)) =>
      RegisteredCriticalPath(length + pathBefore, pathAfter, innerCriticalPath)
    case (RegisteredCriticalPath(pathBefore, pathAfter, innerCriticalPath), UnregisteredCriticalPath(length)) =>
      RegisteredCriticalPath(pathBefore, pathAfter + length, innerCriticalPath)
  }
  def max(that: CriticalPath): CriticalPath = (this, that) match {
    case (UnregisteredCriticalPath(length1), UnregisteredCriticalPath(length2)) =>
      UnregisteredCriticalPath(Math.max(length1, length2))
    case (RegisteredCriticalPath(pathBefore1, pathAfter1, innerCriticalPath1), RegisteredCriticalPath(pathBefore2, pathAfter2, innerCriticalPath2)) =>
      RegisteredCriticalPath(
      Math.max(pathBefore1, pathBefore2),
      Math.max(pathAfter1, pathAfter2),
      Math.max(innerCriticalPath1, innerCriticalPath2)
      )
    case _ => ??? // TODO it is impossible to model is case correctly in the current representation
  }
}

final case class RegisteredCriticalPath(pathBefore: Double, pathAfter: Double, innerCriticalPath: Double) extends CriticalPath {
  override def length: Double = Math.max(Math.max(pathBefore, pathAfter), innerCriticalPath)
}

final case class UnregisteredCriticalPath(length: Double = 0) extends CriticalPath
