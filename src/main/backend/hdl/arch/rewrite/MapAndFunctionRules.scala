package backend.hdl.arch.rewrite


import backend.hdl.{BaseAddrTypeT, IntType, IntTypeT, LogicType, LogicTypeT, OrderedStreamTypeT, VectorType, VectorTypeT}
import backend.hdl.arch.{AddInt, Alternate, ArchLambda, ConstantValue, CounterInteger, CounterIntegerND, DropVector, JoinOrderedStream, JoinVector, MapOrderedStream, MapOrderedStream2Input, MapVector, Registered, Repeat, VectorToTuple2}
import core.{ArithType, ArithTypeT, Conversion, Expr, FunctionCall, LambdaT, Let, Marker, ParamDef, ParamUse, TextType, TypeChecker}
import core.rewrite.Rule
import core.util.IRDotGraph


object MapAndFunctionRules {
  def moveOutCommonAlterMap: Rule = Rule("moveOutCommonAlterMap", {
    case Alternate(ArchLambda(p1, MapOrderedStream(f1, body1, _), _), ArchLambda(p2, MapOrderedStream(f2, body2, _), _), input, _) =>
      MapOrderedStream(f1, Alternate(ArchLambda(p1, body1), ArchLambda(p2, body2), input))
  })

  def moveOutCommonAlterMap2: Rule = Rule("moveOutCommonAlterMap2", {
    case Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _) if {
      var exprFound = false
      body1.visit{
        case MapOrderedStream(f, ParamUse(pd), _) if pd.id == p1.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      body2.visit{
        case MapOrderedStream(f, ParamUse(pd), _) if pd.id == p2.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      var fun: Expr = null
      body1.visit{
        case MapOrderedStream(f, ParamUse(pd), _) if pd.id == p1.id =>
          fun = f
        case _ =>
      }
      val newInput = TypeChecker.check(MapOrderedStream(fun, input))
      val np1 = ParamDef(newInput.t)
      val np2 = ParamDef(newInput.t)
      Alternate(ArchLambda(np1,
        body1.visitAndRebuild{
          case MapOrderedStream(f, ParamUse(pd), _) if pd.id == p1.id =>
            ParamUse(np1)
          case e => e
        }.asInstanceOf[Expr]
      ), ArchLambda(np2,
        body2.visitAndRebuild{
          case MapOrderedStream(f, ParamUse(pd), _) if pd.id == p2.id =>
            ParamUse(np2)
          case e => e
        }.asInstanceOf[Expr]
      ), newInput)


    case Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _) if {
      var exprFound = false
      body1.visit{
        case JoinOrderedStream(ParamUse(pd), _) if pd.id == p1.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      body2.visit{
        case JoinOrderedStream(ParamUse(pd), _) if pd.id == p2.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(JoinOrderedStream(input))
      val np1 = ParamDef(newInput.t)
      val np2 = ParamDef(newInput.t)
      Alternate(ArchLambda(np1,
        body1.visitAndRebuild{
          case JoinOrderedStream(ParamUse(pd), _) if pd.id == p1.id =>
            ParamUse(np1)
          case e => e
        }.asInstanceOf[Expr]
      ), ArchLambda(np2,
        body2.visitAndRebuild{
          case JoinOrderedStream(ParamUse(pd), _) if pd.id == p2.id =>
            ParamUse(np2)
          case e => e
        }.asInstanceOf[Expr]
      ), newInput)
  })

  def moveDownMemAllocFunCalls: Seq[Rule] = Seq(
    skipMapStm2D,
    skipMapStm2DWithExtraMap,
  )

  private def skipMapStm2D: Rule = Rule("moveDownMemAllocFunCallWorkaround_skipMapStm2D", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p, body @
      FunctionCall(FunctionCall(FunctionCall(FunctionCall(fp1 @ ParamUse(_), in1, _), ParamUse(pd), _),
    fCall @ FunctionCall(FunctionCall(_ @ ParamUse(_), _, _), _, _: BaseAddrTypeT), _), in4, _),
     _), inBody, _), _), input, _) if
      RulesHelper.containsParam(inBody, p1) && !RulesHelper.containsParam(body, p1) && p.id == pd.id &&
        !RulesHelper.containsParam(fCall, p) // Ensure params are not used by targeted funCall.
    =>
      val ilen1 = input.t.asInstanceOf[OrderedStreamTypeT].len
      val ilen2 = inBody.t.asInstanceOf[OrderedStreamTypeT].len
      val repFunCall = TypeChecker.check(Repeat(Repeat(fCall, ilen2),ilen1))

      MapOrderedStream2Input(
        {
          val param1 = ParamDef(repFunCall.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param1,
            ArchLambda(
              p1,
              MapOrderedStream2Input(
                {
                  val param = ParamDef(param1.t.asInstanceOf[OrderedStreamTypeT].et)
                  ArchLambda(
                    p,
                    ArchLambda(
                      param,
                      FunctionCall(FunctionCall(FunctionCall(FunctionCall(fp1, in1), ParamUse(pd)),
                        ParamUse(param)), in4)
                    )
                  )
                }, inBody, ParamUse(param1)
              )
            )
          )
        }, repFunCall, input
      )
  })

  private def skipMapStm2DWithExtraMap: Rule = Rule("moveDownMemAllocFunCallWorkaround_skipMapStm2DWithExtraMap", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p, body @ MapOrderedStream(f,
      FunctionCall(FunctionCall(FunctionCall(FunctionCall(fp1 @ ParamUse(_), in1, _), ParamUse(pd), _),
      fCall @ FunctionCall(FunctionCall(_ @ ParamUse(_), _, _), _, _: BaseAddrTypeT), _), in4, _),
    _), _), inBody, _), _), input, _) if
      RulesHelper.containsParam(inBody, p1) && !RulesHelper.containsParam(body, p1) && p.id == pd.id &&
        !RulesHelper.containsParam(fCall, p) // Ensure params are not used by targeted funCall.
    =>
      val ilen1 = input.t.asInstanceOf[OrderedStreamTypeT].len
      val ilen2 = inBody.t.asInstanceOf[OrderedStreamTypeT].len
      val repFunCall = TypeChecker.check(Repeat(Repeat(fCall, ilen2),ilen1))

      MapOrderedStream2Input(
        {
          val param1 = ParamDef(repFunCall.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param1,
            ArchLambda(
              p1,
              MapOrderedStream2Input(
                {
                  val param = ParamDef(param1.t.asInstanceOf[OrderedStreamTypeT].et)
                  ArchLambda(
                    p,
                    ArchLambda(
                      param,
                      MapOrderedStream(f,
                        FunctionCall(FunctionCall(FunctionCall(FunctionCall(fp1, in1), ParamUse(pd)),
                          ParamUse(param)), in4))
                    )
                  )
                }, inBody, ParamUse(param1)
              )
            )
          )
        }, repFunCall, input
      )
  })
}
