package backend.hdl.arch.rewrite

import backend.hdl.{BlockRamTypeT, HostRamTypeT, LogicType, OrderedStreamType, OrderedStreamTypeT, RamArrayTypeT, VectorType}
import backend.hdl.arch.mem._
import backend.hdl.arch._
import core.rewrite.Rule
import core._

object InputBufferingRules {

  def readDoubleBuffering: Seq[Rule] =
    Seq(
      mapFissionReadAsync,
      doubleBufferRead
    )

  def fissionReadOOB: Seq[Rule] = Seq(
    InputBufferingRules.mapFissionReadOOB,
    InputBufferingRules.mapFissionReadOOBOuter,
    InputBufferingRules.mapFissionReadOOBOuter4
  )

  def bufferRepeatedStream: Rule = Rule("bufferRepeatedStream", {
    case l: LambdaT if
    {
      RulesHelper.hasRepeatedStream(l) && RulesHelper.isComputation(l) &&
        !RulesHelper.exprContains(l.body, { // prevent infinite rewrites
          case MemoryAllocation(_, _, _, _, _) => true
        })
    } =>
      // TODO fix before using
      ???
      // this rule inserts a buffer next to a Tuple, which results in a deadlock!
      // buffer must be inserted at input source
      val param = ParamDef(l.param.t)
      ArchLambda(param, FunctionCall(l, Marker(BufferStreamInBlockRam(ParamUse(param)), TextType("buffer_repeated_stream"))))
  })

  def bufferInputRow(inputLabel: String, scale: Int = 1, useOnBoardRam: Boolean = false): Rule = bufferInput(inputLabel, 1, scale, useOnBoardRam)

  def bufferInputMatrix(inputLabel: String, scale: Int = 1, useOnBoardRam: Boolean = false): Rule = bufferInput(inputLabel, 2, scale, useOnBoardRam)

  def bufferInput(inputLabel: String, dimensionsToBuffer: Int = 1, scale: Int = 1, useOnBoardRam: Boolean = false): Rule = Rule("bufferInput", {
    case Marker(input, text)
      if text.s.contains(inputLabel) &&
        !RulesHelper.exprContains(input, {
          // Avoid inserting buffer (BlockRAM) multiple times. If there is a host ram write, we can still insert a buffer.
          case Write(_, _, t: RamArrayTypeT) if t.memoryLocation.isInstanceOf[BlockRamTypeT] => true
        }) =>
      Marker(_bufferInput(input, dimensionsToBuffer, scale, useOnBoardRam), text)
  })

  private def _bufferInput(input: Expr, dimensionsToBuffer: Int = 1, scale: Int, useOnBoardRam: Boolean): Expr = {
    val inputType: OrderedStreamTypeT = input.t match {
      case s: OrderedStreamTypeT => s
      case _ => throw MalformedExprException("Cannot buffer anything other than streams.")
    }
    val inputDimensions = inputType.dimensions

    if (scale > 1 && inputDimensions.head.ae.evalInt % scale != 0)
      throw new Exception("Cannot scale memory input and output, because the innermost stream does not contain a multiple of " + scale + " elements (" + inputDimensions.head.ae.evalInt + ") !")

    val scaledLeafType = VectorType(LogicType(), inputType.leafType.bitWidth.ae.evalInt * scale)
    val scaledType = OrderedStreamType(ArithType(inputDimensions.head.ae.evalInt / scale) +: inputDimensions.tail, scaledLeafType)

    val bufferedType = OrderedStreamType(scaledType.dimensions.slice(0, dimensionsToBuffer), scaledLeafType)

    val inputConversion = ArchConversion(input, inputType, scaledType)

    val buffer =
      MapOrderedStream(
        inputType.dimensions.length - dimensionsToBuffer,
        {
          val p = ParamDef(bufferedType)
          ArchLambda(p, {
            if (useOnBoardRam)
              BufferStreamInOnBoardRam(ParamUse(p))
            else
              BufferStreamInBlockRam(ParamUse(p))
          })
        },
        inputConversion
      )

    ArchConversion(buffer, scaledType, inputType)
  }

  def doubleBufferRead: Rule = Rule("doubleBufferRead", {
    case ArchLambda(p1, ReadAsyncOrdered(readMemoryController, baseAddr, ParamUse(p2), _), _) if p1.id == p2.id =>

      val f1 = {
        val p = ParamDef()
        ArchLambda(p, ReadAsyncOrdered(readMemoryController, Select2(ParamUse(p), 1), Select2(ParamUse(p), 0)))
      }
      val f2 = {
        val p = ParamDef()
        ArchLambda(p, ReadAsyncOrdered(readMemoryController, Select2(ParamUse(p), 1), Select2(ParamUse(p), 0)))
      }
      ArchLambda(p1, Alternate(f1, f2, Tuple2(ParamUse(p1), baseAddr)))
  })

  def doubleBufferReadLimited: Rule = Rule("doubleBufferReadLimited", {
    case ArchLambda(p1, ReadAsyncOrdered(readMemoryController, baseAddr, ParamUse(p2), t: OrderedStreamTypeT), _) if p1.id == p2.id && t.len.ae.evalInt > 32 =>

      val f1 = {
        val p = ParamDef()
        ArchLambda(p, ReadAsyncOrdered(readMemoryController, Select2(ParamUse(p), 1), Select2(ParamUse(p), 0)))
      }
      val f2 = {
        val p = ParamDef()
        ArchLambda(p, ReadAsyncOrdered(readMemoryController, Select2(ParamUse(p), 1), Select2(ParamUse(p), 0)))
      }
      ArchLambda(p1, Alternate(f1, f2, Tuple(ParamUse(p1), baseAddr)))

    case ArchLambda(p1, ReadAsyncOrderedOutOfBounds(readMemoryController, baseAddr, ParamUse(p2), paddedValue, fun, t: OrderedStreamTypeT), _) if p1.id == p2.id && t.len.ae.evalInt > 32=>

      val f1 = {
        val p = ParamDef()
        ArchLambda(p, ReadAsyncOrderedOutOfBounds(readMemoryController, Select2(ParamUse(p), 1), Select2(ParamUse(p), 0), paddedValue, fun))
      }
      val f2 = {
        val p = ParamDef()
        ArchLambda(p, ReadAsyncOrderedOutOfBounds(readMemoryController, Select2(ParamUse(p), 1), Select2(ParamUse(p), 0), paddedValue, fun))
      }
      ArchLambda(p1, Alternate(f1, f2, Tuple(ParamUse(p1), baseAddr)))
  })

  def limitParallelHostRamReadRequests(maxParReq: Int = 2): Rule = Rule("limitParallelHostRamReadRequests", {
    case Let(param, body, ArchLambda(p1, memController@ReadHostMemoryController(ParamUse(p2), _, _, _), _), _) if p1.id == p2.id && {
      RulesHelper.exprContains(body, {
        case ReadAsyncOrdered(ParamUse(p), _, _, t: OrderedStreamTypeT)
          if param.id == p.id && t.len.ae.evalInt > maxParReq && t.len.ae.evalInt % maxParReq == 0 => true
      })
    } =>
      Let(
        param,
        body.visitAndRebuild({
          case ReadAsyncOrdered(ParamUse(p), baseAddr, input, t: OrderedStreamTypeT)
            if param.id == p.id && t.len.ae.evalInt > maxParReq && t.len.ae.evalInt % maxParReq == 0 =>
            val splitInput = TypeChecker.check(SplitOrderedStream(input, maxParReq))
            JoinOrderedStream(
              MapOrderedStream(
                {
                  val pIn = ParamDef()
                  ArchLambda(
                    pIn,
                    ReadAsyncOrdered(ParamUse(p), baseAddr, ParamUse(pIn))
                  )
                },
                splitInput
              )
            )
          case e => e
        }).asInstanceOf[Expr],
        memController
      )
  })

  private def increaseReqCount(maxReqs: Int, basep: ParamDef, base: MemoryAllocationExpr, addrp: ParamDef, addrs: Expr, inner: Int, outer: Int): Expr = {
    var newInner = inner
    var newOuter = outer
    while (newOuter > 1 && newInner < maxReqs && newInner * 2L <= maxReqs) {
      newInner = newInner << 1
      newOuter = (newOuter + 1) >>> 1
    }

    val padding = newInner / inner * newOuter - outer
    val newAddrs = SplitOrderedStream(JoinOrderedStream(PadOrderedStream(addrs, 0, padding, 0)), newInner)
    val newShape = Let(basep, MapOrderedStream({
      val p = ParamDef()
      ArchLambda(p,
        MapOrderedStream(
          ArchLambda(addrp, Read(ParamUse(basep), ParamUse(addrp))), ParamUse(p)))
    }, newAddrs), base)
    DropOrderedStream(SplitOrderedStream(JoinOrderedStream(newShape), inner), 0, padding)
  }

  def increaseParallelReadRequests(maxReqs: Int = 64): Rule = Rule("increaseParallelReadRequests", {
    // instead of reading [<bit x 512> x M x kN], read [<bit x 512> x kM x N]
    case Let(basep, MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    Read(ParamUse(baseu), ParamUse(u2), _), _), ParamUse(u1), t2: OrderedStreamTypeT), _), addrs,
    t1: OrderedStreamTypeT), base@MemoryAllocation(_), _)
      if t2.len.ae.evalInt < maxReqs && t2.len.ae.evalInt * 2 <= maxReqs && t1.len.ae.evalInt > 1 &&
        basep.id == baseu.id && p1.id == u1.id && p2.id == u2.id =>
      increaseReqCount(maxReqs, basep, base, p2, addrs, t2.len.ae.evalInt, t1.len.ae.evalInt)

    case Let(basep, MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3,
    Read(ParamUse(baseu), ParamUse(u3), _), _), ParamUse(u2), t3: OrderedStreamTypeT), _), ParamUse(u1),
    t2: OrderedStreamTypeT), _), addrs, t1: OrderedStreamTypeT), base@MemoryAllocation(_), _)
      if t3.len.ae.evalInt < maxReqs && t3.len.ae.evalInt * 2 <= maxReqs && (t1.len.ae * t2.len.ae).evalInt > 1 &&
        basep.id == baseu.id && p1.id == u1.id && p2.id == u2.id && p3.id == u3.id =>
      SplitOrderedStream(increaseReqCount(maxReqs, basep, base, p3, JoinOrderedStream(addrs), t3.len.ae.evalInt, (t2.len.ae * t1.len.ae).evalInt), t2.len.ae)
  })

  def limitParallelReadRequests(maxParReq: Int = 2): Rule = Rule("limitParallelReadRequests", {
    case MapOrderedStream(ArchLambda(p1, Read(memAllocation, ParamUse(p2), _), _), input, t: OrderedStreamTypeT)
      if t.len.ae.evalInt > maxParReq && t.len.ae.evalInt % maxParReq == 0 && p1.id == p2.id && memAllocation.t.asInstanceOf[RamArrayTypeT].memoryLocation.isInstanceOf[HostRamTypeT] =>
      JoinOrderedStream(
        MapOrderedStream(
          {
            val p = ParamDef()
            ArchLambda(p,
              MapOrderedStream(
                {
                  val pp = ParamDef()
                  ArchLambda(pp, Read(memAllocation, ParamUse(pp)))
                },
                ParamUse(p)
              )
            )
          },
          SplitOrderedStream(input, maxParReq)
        )
      )

    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, Read(memAllocation, ParamUse(p3), _),
    _), ParamUse(p4), _), _), input, t: OrderedStreamTypeT) if p1.id == p4.id && p2.id == p3.id
      && /*t.len.ae.evalInt * t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt > maxParReq &&*/
      t.len.ae.evalInt * t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt % maxParReq == 0 &&
      t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt != maxParReq &&
      memAllocation.t.asInstanceOf[RamArrayTypeT].memoryLocation.isInstanceOf[HostRamTypeT] =>
      val reshapedInput = TypeChecker.check(SplitOrderedStream(JoinOrderedStream(input), maxParReq))
      val newRead = JoinOrderedStream(MapOrderedStream(
        {
          val p = ParamDef(reshapedInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            p,
            MapOrderedStream(
              {
                val pp = ParamDef(reshapedInput.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
                ArchLambda(pp, Read(memAllocation, ParamUse(pp)))
              }, ParamUse(p)
            )
          )
        }, reshapedInput
      ))
      SplitOrderedStream(newRead, t.et.asInstanceOf[OrderedStreamTypeT].len)


    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, ReadOutOfBounds(memAllocation, ParamUse(p3), pv, fun, _),
    _), ParamUse(p4), _), _), input, t: OrderedStreamTypeT) if p1.id == p4.id && p2.id == p3.id
      && /*t.len.ae.evalInt * t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt > maxParReq &&*/
      t.len.ae.evalInt * t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt % maxParReq == 0 &&
      t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt != maxParReq &&
      memAllocation.t.asInstanceOf[RamArrayTypeT].memoryLocation.isInstanceOf[HostRamTypeT] =>
      val reshapedInput = TypeChecker.check(SplitOrderedStream(JoinOrderedStream(input), maxParReq))
      val newRead = JoinOrderedStream(MapOrderedStream(
        {
          val p = ParamDef(reshapedInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            p,
            MapOrderedStream(
              {
                val pp = ParamDef(reshapedInput.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
                ArchLambda(pp, ReadOutOfBounds(memAllocation, ParamUse(pp), Some(pv), fun))
              }, ParamUse(p)
            )
          )
        }, reshapedInput
      ))
      SplitOrderedStream(newRead, t.et.asInstanceOf[OrderedStreamTypeT].len)

    case MapOrderedStream(ArchLambda(p1, ReadOutOfBounds(memAllocation, ParamUse(p2), pv, fun, _), _), input, t: OrderedStreamTypeT)
      if t.len.ae.evalInt > maxParReq && t.len.ae.evalInt % maxParReq == 0 && p1.id == p2.id =>
      JoinOrderedStream(
        MapOrderedStream(
          {
            val p = ParamDef()
            ArchLambda(p,
              MapOrderedStream(
                {
                  val pp = ParamDef()
                  ArchLambda(pp, ReadOutOfBounds(memAllocation, ParamUse(pp), Some(pv), fun))
                },
                ParamUse(p)
              )
            )
          },
          SplitOrderedStream(input, maxParReq)
        )
      )

    case ReadAsyncOrdered(readMemoryController, baseAddr, input, t: OrderedStreamTypeT)
      if t.len.ae.evalInt > maxParReq && t.len.ae.evalInt % maxParReq == 0 =>
      val splitInput = TypeChecker.check(SplitOrderedStream(input, maxParReq))
      JoinOrderedStream(
        MapOrderedStream(
          {
            val p = ParamDef()
            ArchLambda(
              p,
              ReadAsyncOrdered(readMemoryController, baseAddr, ParamUse(p))
            )
          },
          splitInput
        )
      )

    case ReadAsync(readMemoryController, baseAddr, input, t: OrderedStreamTypeT) => ???
  })

  def limitParallelReadRequestsLevel3(maxParReq: Int = 2): Rule = Rule("limitParallelReadRequestsLevel3", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3, ReadOutOfBounds(memAllocation, ParamUse(p4), pv, fun, _), _),
    ParamUse(p5), _), _), ParamUse(p6), _), _), input, t: OrderedStreamTypeT) if p1.id == p6.id && p2.id == p5.id && p3.id == p4.id
      && /*t.len.ae.evalInt * t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt *
      t.et.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt > maxParReq &&*/
      t.len.ae.evalInt * t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt *
        t.et.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt % maxParReq == 0 &&
      t.et.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt != maxParReq &&
      memAllocation.t.asInstanceOf[RamArrayTypeT].memoryLocation.isInstanceOf[HostRamTypeT] =>
      val reshapedInput = TypeChecker.check(SplitOrderedStream(JoinOrderedStream(JoinOrderedStream(input)), maxParReq))
      val newRead = JoinOrderedStream(MapOrderedStream(
        {
          val p = ParamDef(reshapedInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            p,
            MapOrderedStream(
              {
                val pp = ParamDef(reshapedInput.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
                ArchLambda(pp, ReadOutOfBounds(memAllocation, ParamUse(pp), Some(pv), fun))
              }, ParamUse(p)
            )
          )
        }, reshapedInput
      ))
      SplitOrderedStream(
        SplitOrderedStream(newRead, t.et.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len),
        t.et.asInstanceOf[OrderedStreamTypeT].len
      )
  })

  def mapFissionReadAsync: Rule = Rule("mapFissionReadAsync", {
    // map fission
    // prevent infinite rewrites with this rule by checking that fun is not a ParamUse
    case MapOrderedStream(ArchLambda(origParam, ReadAsyncOrdered(mc, ba, body, _), _), input, _) if !RulesHelper.isParamUse(body) =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            ReadAsyncOrdered(mc, ba, ParamUse(param))
          )
        }, MapOrderedStream(ArchLambda(origParam, body), input)
      )
  })

  def mapFissionReadOOB: Rule = Rule("mapFissionReadOOB", {
    case MapOrderedStream(ArchLambda(p1, ReadOutOfBounds(pm, body, pv, fun, _), _), input, _) if RulesHelper.containsParam(body, p1) && !RulesHelper.isParamUse(body) =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            ReadOutOfBounds(pm, ParamUse(param), Some(pv), fun)
          )
        }, MapOrderedStream(ArchLambda(p1, body), input)
      )
    case MapOrderedStream(ArchLambda(origParam, MapOrderedStream(readFun @ ArchLambda(_, ReadOutOfBounds(ParamUse(_), ParamUse(_), _, _, _), _), body, _), _), input, _) if !RulesHelper.isParamUse(body) =>
      MapOrderedStream(2,
        readFun,
        MapOrderedStream(ArchLambda(origParam, body), input)
      )
  })


  def mapFissionReadOOBOuter: Rule = Rule("mapFissionReadOOBOuter", {
    case MapOrderedStream(ArchLambda(origParam, MapOrderedStream(ArchLambda(_,
    MapOrderedStream(readFun @ ArchLambda(_, ReadOutOfBounds(ParamUse(_), ParamUse(_), _, _, _), _), ParamUse(_), _), _), body, _), _), input, _) if
      !RulesHelper.isParamUse(body) =>
      MapOrderedStream(3,
        readFun,
        MapOrderedStream(ArchLambda(origParam, body), input)
      )
  })

  def mapFissionReadOOBOuter4: Rule = Rule("mapFissionReadOOBOuter4", {
    case MapOrderedStream(ArchLambda(origParam, MapOrderedStream(ArchLambda(_, MapOrderedStream(ArchLambda(_,
    MapOrderedStream(readFun @ ArchLambda(_, ReadOutOfBounds(ParamUse(_), ParamUse(_), _, _, _), _), ParamUse(_), _), _), ParamUse(_), _), _), body, _), _), input, _) if
      !RulesHelper.isParamUse(body) =>
      MapOrderedStream(4,
        readFun,
        MapOrderedStream(ArchLambda(origParam, body), input)
      )
    case MapOrderedStream(ArchLambda(origParam, MapOrderedStream(ArchLambda(_, MapOrderedStream(ArchLambda(_, MapOrderedStream(ArchLambda(_,
    MapOrderedStream(readFun @ ArchLambda(_, ReadOutOfBounds(ParamUse(_), ParamUse(_), _, _, _), _), ParamUse(_), _), _), ParamUse(_), _), _), ParamUse(_), _), _), body, _), _), input, _) if
      !RulesHelper.isParamUse(body) =>
      MapOrderedStream(5,
        readFun,
        MapOrderedStream(ArchLambda(origParam, body), input)
      )
  })

  def removeDoubleBuffer: Rule = Rule("removeDoubleBuffer", {
    case MapOrderedStream(ArchLambda(p1, Alternate(f1: LambdaT, f2: LambdaT, inner, _), _), in @ CounterInteger(_), _) if
      RulesHelper.containsParam(inner, p1) && in.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt == 1=>
      MapOrderedStream(ArchLambda(p1, f1.body.visitAndRebuild{
        case ParamUse(pd) if pd.id == f1.param.id => inner
        case e => e
      }.asInstanceOf[Expr]), in)
  })
}
