package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.arch.{ArchLambda, MapOrderedStream, Repeat}
import core.{Expr, FunctionCall, Let, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object MergeFuncRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] = Seq(

  )

  def mergeNearbyCallsSingleInput: Rule = Rule("mergeNearbyCallsSingleInput", {
    case Let(pf1, Let(pf2, body, ArchLambda(pi2, fBody2, _), _),
    ArchLambda(pi11, ArchLambda(pi12, fBody1, _), _), _) if {
      var param1Uses = 0
      var param2Uses = 0
      var matchParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p1), FunctionCall(ParamUse(p2), input0, _), _), input1, _) if
          p1.id == pf1.id && p2.id == pf2.id =>
          matchParamUses = matchParamUses + 1
        case ParamUse(p) if p.id == pf1.id => param1Uses = param1Uses + 1
        case ParamUse(p) if p.id == pf2.id => param2Uses = param2Uses + 1
        case _ =>
      }
      matchParamUses == param1Uses && matchParamUses == param2Uses && matchParamUses > 0
    } =>
      val newParam1 = ParamDef(pi2.t)
      val newParam2 = ParamDef(pi12.t)

      val newFunBody2 = fBody2.visitAndRebuild{
        case ParamUse(p) if p.id == pi2.id => ParamUse(newParam1)
        case e => e
      }.asInstanceOf[Expr]

      val newFunBody1 = fBody1.visitAndRebuild{
        case ParamUse(p) if p.id == pi11.id => newFunBody2
        case ParamUse(p) if p.id == pi12.id => ParamUse(newParam2)
        case e => e
      }.asInstanceOf[Expr]

      val newFun = TypeChecker.check(ArchLambda(newParam1, ArchLambda(newParam2, newFunBody1)))
      val newFunParam = ParamDef(newFun.t)

      Let(newFunParam,
        body.visitAndRebuild{
          case FunctionCall(FunctionCall(ParamUse(p1), FunctionCall(ParamUse(p2), input0, _), _), input1, _) if
            p1.id == pf1.id && p2.id == pf2.id =>
            FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
          case e => e
        }.asInstanceOf[Expr],
        newFun
      )

    case Let(pf1, Let(pf2, body, ArchLambda(pi2, fBody2, _), _),
    ArchLambda(pi11, ArchLambda(pi12, fBody1, _), _), _) if {
      var param1Uses = 0
      var param2Uses = 0
      var matchParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p1), input0, _), FunctionCall(ParamUse(p2), input1, _), _) if
          p1.id == pf1.id && p2.id == pf2.id =>
          matchParamUses = matchParamUses + 1
        case ParamUse(p) if p.id == pf1.id => param1Uses = param1Uses + 1
        case ParamUse(p) if p.id == pf2.id => param2Uses = param2Uses + 1
        case _ =>
      }
      matchParamUses == param1Uses && matchParamUses == param2Uses && matchParamUses > 0
    } =>
      val newParam1 = ParamDef(pi11.t)
      val newParam2 = ParamDef(pi2.t)

      val newFunBody2 = fBody2.visitAndRebuild {
        case ParamUse(p) if p.id == pi2.id => ParamUse(newParam2)
        case e => e
      }.asInstanceOf[Expr]

      val newFunBody1 = fBody1.visitAndRebuild {
        case ParamUse(p) if p.id == pi12.id => newFunBody2
        case ParamUse(p) if p.id == pi11.id => ParamUse(newParam1)
        case e => e
      }.asInstanceOf[Expr]

      val newFun = TypeChecker.check(ArchLambda(newParam1, ArchLambda(newParam2, newFunBody1)))
      val newFunParam = ParamDef(newFun.t)

      Let(newFunParam,
        body.visitAndRebuild {
          case FunctionCall(FunctionCall(ParamUse(p1), input0, _), FunctionCall(ParamUse(p2), input1, _), _) if
            p1.id == pf1.id && p2.id == pf2.id =>
            FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
          case e => e
        }.asInstanceOf[Expr],
        newFun
      )
  })
}