package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.mem.BufferStream2DInBlockRam
import backend.hdl.arch.rewrite.RemoveSelectRules
import backend.hdl.arch.{Alternate, ArchLambda, JoinStreamAll, Repeat, SlideGeneralOrderedStream, SplitOrderedStream, SplitStreamAll}
import core.{Expr, LambdaT, Let, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object FuncBufferingRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] = Seq(
    bufferRepeatData,
    bufferSlideData
  )  ++ PropagateFunRules.get() ++ Seq(RemoveSelectRules.removeSelect)

  def bufferRepeatData: Rule = Rule("FuncBufferingRules_bufferRepeatData", {
    case Let(p, body, ArchLambda(pf, fBody, _), _) if !fBody.isInstanceOf[LambdaT] && {
      var exprFound = false
      fBody.visit {
        case Repeat(ParamUse(pi), times, _) if pi.id == pf.id && pf.t.isInstanceOf[OrderedStreamTypeT] =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newParam = ParamDef(pf.t)
      val inputDims = pf.t.asInstanceOf[OrderedStreamTypeT].dimensions

      // Double buffer 0
      val iBufParam1 = ParamDef(newParam.t)
      val iBufFun1 = ArchLambda(
        iBufParam1,
        {
          val joinedInput = if (inputDims.length == 1) {
            ParamUse(iBufParam1)
          } else {
            TypeChecker.check(SplitOrderedStream(JoinStreamAll(ParamUse(iBufParam1)), inputDims.head))
          }
          val bufData = TypeChecker.check(BufferStream2DInBlockRam(joinedInput))
          val splitBufOut = TypeChecker.check(SplitStreamAll(bufData, inputDims.drop(1).dropRight(1).reverse))
          splitBufOut
        }
      )

      // Double buffer 0
      val iBufParam2 = ParamDef(newParam.t)
      val iBufFun2 = ArchLambda(
        iBufParam2,
        {
          val joinedInput = if (inputDims.length == 1) {
            ParamUse(iBufParam2)
          } else {
            TypeChecker.check(SplitOrderedStream(JoinStreamAll(ParamUse(iBufParam2)), inputDims.head))
          }
          val bufData = TypeChecker.check(BufferStream2DInBlockRam(joinedInput))
          val splitBufOut = TypeChecker.check(SplitStreamAll(bufData, inputDims.drop(1).dropRight(1).reverse))
          splitBufOut
        }
      )

      val doubleBufInput = TypeChecker.check(Alternate(iBufFun1, iBufFun2, ParamUse(newParam)))

      // Rebuild function
      val newFBody = fBody.visitAndRebuild{
        case Repeat(ParamUse(pi), times, _) if pi.id == pf.id && pf.t.isInstanceOf[OrderedStreamTypeT] =>
          Repeat(doubleBufInput, times)
        case e => e
      }.asInstanceOf[Expr]

      val newFun = TypeChecker.check(ArchLambda(newParam, newFBody))
      Let(p, body, newFun)
  })

  def bufferSlideData: Rule = Rule("FuncBufferingRules_bufferSlideData", {
    case Let(p, body, ArchLambda(pf, fBody, _), _) if !fBody.isInstanceOf[LambdaT] && {
      var exprFound = false
      fBody.visit {
        case SlideGeneralOrderedStream(ParamUse(pi), windowSize, stepSize, _) if pi.id == pf.id && pf.t.isInstanceOf[OrderedStreamTypeT] =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newParam = ParamDef(pf.t)
      val inputDims = pf.t.asInstanceOf[OrderedStreamTypeT].dimensions

      // Double buffer 0
      val iBufParam1 = ParamDef(newParam.t)
      val iBufFun1 = ArchLambda(
        iBufParam1,
        {
          val joinedInput = if (inputDims.length == 1) {
            ParamUse(iBufParam1)
          } else {
            TypeChecker.check(SplitOrderedStream(JoinStreamAll(ParamUse(iBufParam1)), inputDims.head))
          }
          val bufData = TypeChecker.check(BufferStream2DInBlockRam(joinedInput))
          val splitBufOut = TypeChecker.check(SplitStreamAll(bufData, inputDims.drop(1).dropRight(1).reverse))
          splitBufOut
        }
      )

      // Double buffer 0
      val iBufParam2 = ParamDef(newParam.t)
      val iBufFun2 = ArchLambda(
        iBufParam2,
        {
          val joinedInput = if (inputDims.length == 1) {
            ParamUse(iBufParam2)
          } else {
            TypeChecker.check(SplitOrderedStream(JoinStreamAll(ParamUse(iBufParam2)), inputDims.head))
          }
          val bufData = TypeChecker.check(BufferStream2DInBlockRam(joinedInput))
          val splitBufOut = TypeChecker.check(SplitStreamAll(bufData, inputDims.drop(1).dropRight(1).reverse))
          splitBufOut
        }
      )

      val doubleBufInput = TypeChecker.check(Alternate(iBufFun1, iBufFun2, ParamUse(newParam)))

      // Rebuild function
      val newFBody = fBody.visitAndRebuild {
        case SlideGeneralOrderedStream(ParamUse(pi), windowSize, stepSize, _) if pi.id == pf.id && pf.t.isInstanceOf[OrderedStreamTypeT] =>
          SlideGeneralOrderedStream(doubleBufInput, windowSize, stepSize)
        case e => e
      }.asInstanceOf[Expr]

      val newFun = TypeChecker.check(ArchLambda(newParam, newFBody))
      Let(p, body, newFun)
  })
}