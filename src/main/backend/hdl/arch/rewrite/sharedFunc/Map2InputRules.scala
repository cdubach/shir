package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.arch.{ArchLambda, MapOrderedStream, MapOrderedStream2Input}
import core.{Expr, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object Map2InputRules extends RulesT {

  /**
   * This rule set deals with map fission and MapOrderedStream2Input. We are trying to move out MapOrderedStreams from
   * MapOrderedStream2Input so that they can be rewritten by the other rules.
   */

  override def all(config: Option[Int]): Seq[Rule] = Seq(
    mapFissionDown,
    mapFissionUp
  )

  def mapFissionDown: Rule = Rule("Map2Input_fissionDown", {
    case MapOrderedStream2Input(ArchLambda(p0, ArchLambda(p1, body, _), _), input0, input1, _) if
      RulesHelper.containsParam(body, p0) && RulesHelper.containsParam(body, p1) && {
        var exprFound = false
        body.visit{
          case MapOrderedStream(fun, ParamUse(p2), _) if p0.id == p2.id && !RulesHelper.containsParam(fun, p1) => exprFound = true
          case _ =>
        }
        exprFound} =>
      var fun: Expr = null
      body.visit {
        case MapOrderedStream(f, ParamUse(p2), _) if p0.id == p2.id => fun = f
        case _ =>
      }
      val newInput0 = TypeChecker.check(MapOrderedStream(2, fun, input0))
      val inputParam0 = ParamDef(newInput0.t.asInstanceOf[OrderedStreamTypeT].et)
      val inputParam1 = ParamDef(input1.t.asInstanceOf[OrderedStreamTypeT].et)

      val newBody = body.visitAndRebuild{
        case MapOrderedStream(fun, ParamUse(p2), _) if p0.id == p2.id => ParamUse(inputParam0)
        case ParamUse(p2) if p1.id == p2.id => ParamUse(inputParam1)
        case e => e
      }.asInstanceOf[Expr]
      MapOrderedStream2Input(ArchLambda(inputParam0, ArchLambda(inputParam1, newBody)), newInput0, input1)

    case MapOrderedStream2Input(ArchLambda(p0, ArchLambda(p1, body, _), _), input0, input1, _) if
      RulesHelper.containsParam(body, p0) && RulesHelper.containsParam(body, p1) && {
        var exprFound = false
        body.visit {
          case MapOrderedStream(fun, ParamUse(p2), _) if p1.id == p2.id && !RulesHelper.containsParam(fun, p0) => exprFound = true
          case _ =>
        }
        exprFound
      } =>
      var fun: Expr = null
      body.visit {
        case MapOrderedStream(f, ParamUse(p2), _) if p1.id == p2.id => fun = f
        case _ =>
      }
      val newInput1 = TypeChecker.check(MapOrderedStream(2, fun, input1))
      val inputParam1 = ParamDef(newInput1.t.asInstanceOf[OrderedStreamTypeT].et)
      val inputParam0 = ParamDef(input0.t.asInstanceOf[OrderedStreamTypeT].et)

      val newBody = body.visitAndRebuild {
        case MapOrderedStream(fun, ParamUse(p2), _) if p1.id == p2.id => ParamUse(inputParam1)
        case ParamUse(p2) if p0.id == p2.id => ParamUse(inputParam0)
        case e => e
      }.asInstanceOf[Expr]
      MapOrderedStream2Input(ArchLambda(inputParam0, ArchLambda(inputParam1, newBody)), input0, newInput1)
  })

  def mapFissionUp: Rule = Rule("Map2Input_mapFissionUp", {
    case MapOrderedStream2Input(ArchLambda(p0, ArchLambda(p1, MapOrderedStream(fun, body ,_), _), _), input0, input1, _) if
      !RulesHelper.containsParam(fun, p0) && !RulesHelper.containsParam(fun, p1) =>
      MapOrderedStream(2, fun, MapOrderedStream2Input(ArchLambda(p0, ArchLambda(p1, body)), input0, input1))
  })
}