package backend.hdl.arch.rewrite.sharedFunc

import core._
import core.rewrite.{Rule, RulesT}
import backend.hdl._
import backend.hdl.arch.mem.{FakeMemoryAllocation, MemoryAllocation, Padding, Read, ReadOutOfBounds, ReadOutOfBoundsExpr}
import backend.hdl.arch.rewrite.{MergeIntoCounterRules, MoveUpJoinStreamRules, MoveUpVectorToStreamRules, RemoveJoinSplitRules, RulesHelper}
import backend.hdl.arch._
import backend.hdl.arch.rewrite.conv.MoveDownPadRules

object MoveUpReadRules extends RulesT {

  /**
   * Since the algo level has no info about memory, it is prohibited to create memory-related functions at that level.
   * However, the memory controllers inserted by Arch-level Compiler will not be in the best positions.
   * These rules serve as the workarounds for moving host RAM read closer to function calls.
   * With these, we are able to merge read access into a function.
   */
  override def all(config: Option[Int]): Seq[Rule] = Seq(
    skipSplitStream,
    skipPadStream,
    skipTiling,
    mapFusionMapParam,
    moveIntoMap,
    mapFusionMapParamMap2Input
  ) ++
    Seq(MoveUpJoinStreamRules.skipPad, MoveUpJoinStreamRules.skipSlideGeneral, MoveUpJoinStreamRules.skipTranspose, MoveUpJoinStreamRules.mapFusionMapParam, MoveUpJoinStreamRules.mapFusionMapParamMap2Input) ++
    Seq(MoveUpVectorToStreamRules.skipPad, MoveUpVectorToStreamRules.skipSlideGeneral, MoveUpVectorToStreamRules.skipTranspose, MoveUpVectorToStreamRules.mapFusionMapParam, MoveUpVectorToStreamRules.mapFusionMapParamMap2Input) ++
    Seq(MergeIntoCounterRules.mergeSplitStream, RemoveJoinSplitRules.removeSplitJoin, MoveUpJoinStreamRules.skipMapNDStm2VecSize1Fix, MoveDownPadRules.skipMap)

  def skipSplitStream: Rule = Rule("MoveUpRead_skipSplitStream", {
    case SplitOrderedStream(Marker(MapOrderedStream(fun, Marker(Let(p, MapOrderedStream(readFun: LambdaT, input, _),
    arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), chunkSize, _) if
      ts0.s.contains("map_fusion_guard") && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
        innerMostFun.body match {
          case Read(ParamUse(p1), ParamUse(p2) ,_) if p1.id == p.id && p2.id == innerMostFun.param.id => exprFound = true
          case _ =>
        }
        exprFound
      } =>
      val splitInput = TypeChecker.check(SplitOrderedStream(input, chunkSize))
      val newRead = TypeChecker.check(Marker(Let(p, MapOrderedStream(2, readFun, splitInput), arg), ts1))
      val newReshape = TypeChecker.check(Marker(MapOrderedStream(2, fun, newRead), ts0))
      newReshape
  })

  def consPaddedValue(v: Long, sclTy: HWDataTypeT, vecWidth: Int): BitVectorType = {
    val sclWidth = sclTy.bitWidth.ae.evalInt
    val asSigned = sclTy.kind == SignedIntTypeKind
    BitVectorType(Seq.range(0, vecWidth).view
      .filter { u =>
        val i = u % sclWidth
        if (i < 64)
          (v & (1L << i)) != 0
        else
          asSigned && v < 0
      }
      .foldLeft(scala.collection.immutable.BitSet.empty)(_ + _), vecWidth)
  }

  def skipPadStream: Rule = Rule("MoveUpRead_skipPadStream", {
    case MapOrderedStream(ArchLambda(p1, PadOrderedStream(ParamUse(p2), fesW, lesW, valueW, _), _),
    Marker(MapOrderedStream(fun: LambdaT, Marker(Let(p, MapOrderedStream(readFun: LambdaT,
    Marker(pctr@ PadOrderedStream(_: CounterIntegerExpr, _, _, ArithType(lift.arithmetic.Cst(iv)), _), TextType("guard")), _),
    arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), tt: OrderedStreamTypeT) if
      p1.id == p2.id && ts0.s.contains("map_fusion_guard") && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
        innerMostFun.body match {
          case ReadOutOfBounds(ParamUse(p3), ParamUse(p4), pv, Padding(limit), r: HWDataTypeT)
            if p3.id == p.id && p4.id == innerMostFun.param.id &&
              iv >= limit.evalInt &&
              consPaddedValue(valueW.ae.evalInt, tt.leafType, r.bitWidth.ae.evalInt) == pv =>
            exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.getMapNDLevels(fun) >= 2 &&  RulesHelper.getMapNDLevels(readFun) >= 2
      } =>
      val paddedInput = TypeChecker.check(MapOrderedStream(PadOrderedStream.asFunction(Seq(None), Seq(fesW, lesW, lift.arithmetic.Cst(iv))), pctr))
      val readLevels = RulesHelper.getMapNDLevels(readFun)
      val readInnerFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
      val convertLevels = RulesHelper.getMapNDLevels(fun)
      val convertInnerFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val newRead = TypeChecker.check(MapOrderedStream(
        readLevels,
        readInnerFun, Marker(paddedInput, TextType("guard"))
      ))

      val newReshape = TypeChecker.check(Marker(MapOrderedStream(convertLevels, convertInnerFun, Marker(Let(p, newRead, arg), ts1)), ts0))
      newReshape

    case MapOrderedStream(ArchLambda(p1, PadOrderedStream(ParamUse(p2), fesW, lesW, valueW, _), _),
    Marker(MapOrderedStream(fun: LambdaT, Marker(Let(p, MapOrderedStream(readFun: LambdaT,
    cnt @ CounterInteger(start, inc, loop, dims, reps, t: OrderedStreamTypeT), _),
    arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), tt: OrderedStreamTypeT) if
      p1.id == p2.id && ts0.s.contains("map_fusion_guard") && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
        innerMostFun.body match {
          case Read(ParamUse(p3), ParamUse(p4), _) if p3.id == p.id && p4.id == innerMostFun.param.id => exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.isInputConversionFunction(fun) && RulesHelper.getMapNDLevels(readFun) >= 2
      } =>
      val newBitWidth = t.leafType.bitWidth.ae.evalInt + 1 // Reserve an extra bit
      val invalidValue = ((1L << newBitWidth) - 1).toInt
      val oldAddrLimit = RulesHelper.getLastValue(cnt) + 1
      val newInput = CounterInteger(start, inc, loop, dims, reps, Some(IntType(newBitWidth)))
      val paddedInput = TypeChecker.check(MapOrderedStream(PadOrderedStream.asFunction(Seq(None), Seq(fesW, lesW, invalidValue)), newInput))
      val readLevels = RulesHelper.getMapNDLevels(readFun)
      val convertLevels = RulesHelper.getMapNDLevels(fun)
      val convertInnerFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val paddedValue = consPaddedValue(valueW.ae.evalInt,
        tt.leafType,
        arg.t.asInstanceOf[RamArrayTypeT].et.bitWidth.ae.evalInt)
      val newRead = TypeChecker.check(MapOrderedStream(
        readLevels,
        {
          val param = ParamDef(paddedInput.t.asInstanceOf[OrderedStreamTypeT].leafType)
          ArchLambda(param, ReadOutOfBounds(ParamUse(p), ParamUse(param), Some(paddedValue), Padding(oldAddrLimit)))
        }, Marker(paddedInput, TextType("guard"))
      ))

      val newReshape = TypeChecker.check(Marker(MapOrderedStream(convertLevels, convertInnerFun, Marker(Let(p, newRead, arg), ts1)), ts0))
      newReshape

    case PadOrderedStream(Marker(MapOrderedStream(fun: LambdaT, Marker(Let(p, MapOrderedStream(readFun: LambdaT,
    cnt @ CounterInteger(start, inc, loop, dims, reps, t: OrderedStreamTypeT), _),
    arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), fesH, lesH, valueH, tt: OrderedStreamTypeT)
      if ts0.s.contains("map_fusion_guard") && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
        innerMostFun.body match {
          case Read(ParamUse(p3), ParamUse(p4), _) if p3.id == p.id && p4.id == innerMostFun.param.id => exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.isInputConversionFunction(fun) && RulesHelper.getMapNDLevels(readFun) >= 2
      } =>
      val newBitWidth = t.leafType.bitWidth.ae.evalInt + 1 // Reserve an extra bit
      val invalidValue = ((1L << newBitWidth) - 1).toInt
      val oldAddrLimit = RulesHelper.getLastValue(cnt) + 1
      val newInput = CounterInteger(start, inc, loop, dims, reps, Some(IntType(newBitWidth)))
      val paddedInput = TypeChecker.check(PadOrderedStream(newInput, fesH, lesH, invalidValue))
      val readLevels = RulesHelper.getMapNDLevels(readFun)
      val convertLevels = RulesHelper.getMapNDLevels(fun)
      val convertInnerFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val paddedValue = consPaddedValue(valueH.ae.evalInt,
        tt.leafType,
        arg.t.asInstanceOf[RamArrayTypeT].et.bitWidth.ae.evalInt)
      val newRead = TypeChecker.check(MapOrderedStream(
        readLevels,
        {
          val param = ParamDef(paddedInput.t.asInstanceOf[OrderedStreamTypeT].leafType)
          ArchLambda(param, ReadOutOfBounds(ParamUse(p), ParamUse(param), Some(paddedValue), Padding(oldAddrLimit)))
        }, Marker(paddedInput, TextType("guard"))
      ))

      val newReshape = TypeChecker.check(Marker(MapOrderedStream(convertLevels, convertInnerFun, Marker(Let(p, newRead, arg), ts1)), ts0))
      newReshape
  })

  def skipTiling: Rule = Rule("MoveUpRead_skipTiling", {
    case TransposeNDOrderedStream(SplitOrderedStream(MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), cs1, _), _),
    Marker(MapOrderedStream(fun: LambdaT, Marker(Let(p, MapOrderedStream(readFun: LambdaT, input, _),
    arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), _), cs2, _), dimOrder, _)
      if p1.id == p2.id && ts0.s.contains("map_fusion_guard") && RulesHelper.getMapNDLevels(fun) == 1 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case MapOrderedStream(innerFun1, MapOrderedStream(innerFun2, ParamUse(p3), _), _) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      var innerFun1: Expr = null
      var innerFun2: Expr = null
      val newInput = TypeChecker.check(TransposeNDOrderedStream(TypeChecker.check(SplitOrderedStream(TypeChecker.check(
        MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(cs1)), input)), cs2)), dimOrder))
      val funLevels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)

      innerMostFun.body match {
        case MapOrderedStream(if1, MapOrderedStream(if2, ParamUse(p3), _), _) if p3.id == innerMostFun.param.id =>
          innerFun1 = if1
          innerFun2 = if2
        case _ =>
      }

      val readFunLevels = RulesHelper.getMapNDLevels(readFun)
      val innerMostReadFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
      val readData = TypeChecker.check(MapOrderedStream(
        2,
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            {
              val ramInput = TypeChecker.check(Marker(Let(p, MapOrderedStream(readFunLevels, innerMostReadFun, ParamUse(param)), arg), ts1))
              val convertedInput = MapOrderedStream(funLevels,
                {
                  val param1 = ParamDef(ramInput.t.asInstanceOf[OrderedStreamTypeT].et)
                  ArchLambda(
                    param1,
                    MapOrderedStream(innerFun1, MapOrderedStream(innerFun2, ParamUse(param1)))
                  )
                }, ramInput
              )
              Marker(convertedInput, ts0)
            }
          )
        }, newInput
      ))
      readData

    case TransposeNDOrderedStream(SlideGeneralOrderedStream(MapOrderedStream(ArchLambda(p1, SlideGeneralOrderedStream(ParamUse(p2), ws1, ss1, _), _),
    Marker(MapOrderedStream(fun: LambdaT, Marker(Let(p, MapOrderedStream(readFun: LambdaT, input, _),
    arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), _), ws2, ss2, _), dimOrder, _)
      if p1.id == p2.id && ts0.s.contains("map_fusion_guard") && dimOrder.last.ae.evalInt == 0 && RulesHelper.getMapNDLevels(fun) == 2 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case MapOrderedStream(innerFun1, MapOrderedStream(innerFun2, ParamUse(p3), _) , _) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      var innerFun1: Expr = null
      var innerFun2: Expr = null
      val splitInput = TypeChecker.check(MapOrderedStream(2, SplitOrderedStream.asFunction(Seq(None), Seq(1)), input))
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, 0, 0)
      val newInput = TypeChecker.check(TransposeNDOrderedStream(TypeChecker.check(SlideGeneralOrderedStream(TypeChecker.check(
        MapOrderedStream(SlideGeneralOrderedStream.asFunction(Seq(None), Seq(ws1, ss1)), splitInput)), ws2, ss2)), newDimOrder))
      val funLevels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)

      innerMostFun.body match {
        case MapOrderedStream(if1, MapOrderedStream(if2, ParamUse(p3), _), _) if p3.id == innerMostFun.param.id =>
          innerFun1 = if1
          innerFun2 = if2
        case _ =>
      }

      val readFunLevels = RulesHelper.getMapNDLevels(readFun)
      val innerMostReadFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
      val readData = TypeChecker.check(MapOrderedStream(
        3,
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            {
              val ramInput = TypeChecker.check(Marker(Let(p, MapOrderedStream(readFunLevels, innerMostReadFun, ParamUse(param)), arg), ts1))
              val convertedInput = MapOrderedStream(funLevels,
                {
                  val param1 = ParamDef(ramInput.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
                  ArchLambda(
                    param1,
                    MapOrderedStream(innerFun1, MapOrderedStream(innerFun2, ParamUse(param1)))
                  )
                }, ramInput
              )
              Marker(convertedInput, ts0)
            }
          )
        }, newInput
      ))
      MapOrderedStream(4, JoinOrderedStream.asFunction(), readData)
    case TransposeNDOrderedStream(SlideGeneralOrderedStream(MapOrderedStream(ArchLambda(p1, SlideGeneralOrderedStream(ParamUse(p2), ws1, ss1, _), _),
    Marker(MapOrderedStream(fun: LambdaT, Marker(Let(p, MapOrderedStream(readFun: LambdaT, input, _),
    arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), _), ws2, ss2, _), dimOrder, _)
      if p1.id == p2.id && ts0.s.contains("map_fusion_guard") =>
      val newInput = TypeChecker.check(TransposeNDOrderedStream(TypeChecker.check(SlideGeneralOrderedStream(TypeChecker.check(
        MapOrderedStream(SlideGeneralOrderedStream.asFunction(Seq(None), Seq(ws1, ss1)), input)), ws2, ss2)), dimOrder))
      val funLevels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val readFunLevels = RulesHelper.getMapNDLevels(readFun)
      val innerMostReadFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
      MapOrderedStream(
        2,
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            Marker(MapOrderedStream(funLevels, innerMostFun, Marker(Let(p, MapOrderedStream(readFunLevels, innerMostReadFun, ParamUse(param)), arg), ts1)), ts0)
          )
        }, newInput
      )
  })

  def mapFusionMapParam: Rule = Rule("MoveUpRead_mapFusionMapParam", {
    case MapOrderedStream(ArchLambda(p1, body1, _), MapOrderedStream(ArchLambda(p2, body2, _),
    input, _), _)
      if {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        innerMostFun.body match {
          case Marker(MapOrderedStream(fun: LambdaT, Marker(Let(p, MapOrderedStream(readFun: LambdaT, ParamUse(p3), _),
          arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit {
          case ParamUse(p4) if p1.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body1))
        innerMostFun.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case JoinOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild {
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild {
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def moveIntoMap: Rule = Rule("MoveUpRead_moveIntoMap", {
    case MapOrderedStream(ArchLambda(p1, body, _),
    Marker(MapOrderedStream(fun: LambdaT, Marker(Let(p, MapOrderedStream(readFun: LambdaT,
    input @ CounterInteger(_, _, _, _, _, _), _),
    arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), _) if
      ts0.s.contains("map_fusion_guard") && RulesHelper.containsParam(body, p1) && RulesHelper.getMapNDLevels(readFun) > 2 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
        innerMostFun.body match {
          case Read(ParamUse(p1), ParamUse(p2), _) if p1.id == p.id && p2.id == innerMostFun.param.id => exprFound = true
          case _ =>
        }
        exprFound
      } =>
      val newParam = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
      val funLevels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val readFunLevels = RulesHelper.getMapNDLevels(readFun)
      val innerMostReadFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
      MapOrderedStream(
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case ParamUse(p2) if p1.id == p2.id =>
              Marker(MapOrderedStream(funLevels-1, innerMostFun, Marker(Let(p, MapOrderedStream(readFunLevels-1, innerMostReadFun,
                ParamUse(newParam)), arg), ts1)), ts0)
            case e => e
          }.asInstanceOf[Expr]
        ),
        input
      )
  })

  def mapFusionMapParamMap2Input: Rule = Rule("MoveUpRead_mapFusionMapParamMap2Input", {
    case MapOrderedStream2Input(ArchLambda(p11, ArchLambda(p12, body1, _), _), MapOrderedStream(ArchLambda(p2, body2, _), input1, _), input2, _)
      if {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        innerMostFun.body match {
          case Marker(MapOrderedStream(fun: LambdaT, Marker(Let(p, MapOrderedStream(readFun: LambdaT, ParamUse(p3), _),
          arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit {
          case ParamUse(p4) if p11.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream2Input({
        val newParam1 = ParamDef()
        val newParam2 = ParamDef()
        ArchLambda(
          newParam1,
          ArchLambda(
            newParam2,
            body1.visitAndRebuild {
              case ParamUse(usedParam1) if usedParam1.id == p11.id =>
                body2.visitAndRebuild {
                  case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                    ParamUse(newParam1)
                  case e => e
                }.asInstanceOf[Expr]
              case ParamUse(usedParam1) if usedParam1.id == p12.id =>
                ParamUse(newParam2)
              case e => e
            }.asInstanceOf[Expr]
          )
        )
      }, input1, input2
      )
    case MapOrderedStream2Input(ArchLambda(p11, ArchLambda(p12, body1, _), _), input1, MapOrderedStream(ArchLambda(p2, body2, _), input2, _), _)
      if {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        innerMostFun.body match {
          case Marker(MapOrderedStream(fun: LambdaT, Marker(Let(p, MapOrderedStream(readFun: LambdaT, ParamUse(p3), _),
          arg @ (MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit {
          case ParamUse(p4) if p12.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream2Input({
        val newParam1 = ParamDef()
        val newParam2 = ParamDef()
        ArchLambda(
          newParam1,
          ArchLambda(
            newParam2,
            body1.visitAndRebuild {
              case ParamUse(usedParam1) if usedParam1.id == p12.id =>
                body2.visitAndRebuild {
                  case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                    ParamUse(newParam2)
                  case e => e
                }.asInstanceOf[Expr]
              case ParamUse(usedParam1) if usedParam1.id == p11.id =>
                ParamUse(newParam1)
              case e => e
            }.asInstanceOf[Expr]
          )
        )
      }, input1, input2
      )
  })
}