package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.RamArrayTypeT
import backend.hdl.arch.{IndexStream, IndexUnorderedStream, JoinUnorderedStream, MapOrderedStream, OrderedStreamToUnorderedStream, ReduceOrderedStream, SplitOrderedStream}
import core.Let
import core.rewrite.{Rule, RulesT}

object MoveDownWriteRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] = Seq(
    skipLet
  )

  def skipLet: Rule = Rule("MoveDownWrite_skipLet", {
    case Let(pmem, ReduceOrderedStream(fun0, mem, SplitOrderedStream(IndexUnorderedStream(JoinUnorderedStream(
    OrderedStreamToUnorderedStream(MapOrderedStream(fun1, Let(p, body, arg, _), _), _), _), _), chunkSize, _), _), argMem, _) if
      mem.t.isInstanceOf[RamArrayTypeT] =>
      Let(p, Let(pmem, ReduceOrderedStream(fun0, mem, SplitOrderedStream(IndexStream(JoinUnorderedStream(
        OrderedStreamToUnorderedStream(MapOrderedStream(fun1, body)))), chunkSize)), argMem), arg)
  })
}