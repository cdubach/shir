package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.arch.{ArchLambda, MapOrderedStream, OrderedStreamToVector, Repeat, SplitOrderedStream}
import core.{ArithTypeT, Expr, FunctionCall, LambdaT, Let, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object ReshapeFuncRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] = Seq(
    moveOutMapStmToVec
  )

  def moveOutMapSplitStm: Rule = Rule("reshapeFunc_moveOutMapSplitStm", {
    case Let(pf, body, ArchLambda(pi1, ArchLambda(pi2, fBody, _), _), _) if {
      var exprFound = false
      fBody.visit{
        case MapOrderedStream(fun: LambdaT, ParamUse(pd), _) if pd.id == pi1.id && {
          var exprFound = false
          val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostBody.body match {
            case SplitOrderedStream(ParamUse(p), _, _) if p.id == innerMostBody.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      var chunkSize: ArithTypeT = 0
      var npi1: ParamDef = null
      var levels: Int = 0
      val nFBody = fBody.visitAndRebuild{
        case MapOrderedStream(fun: LambdaT, ParamUse(pd), t) if pd.id == pi1.id && {
          var exprFound = false
          val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostBody.body match {
            case SplitOrderedStream(ParamUse(p), cs, _) if p.id == innerMostBody.param.id =>
              chunkSize = cs
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          levels = RulesHelper.getMapNDLevels(fun)
          npi1 = ParamDef(t)
          ParamUse(npi1)
        case e => e
      }.asInstanceOf[Expr]

      val newFun = TypeChecker.check(ArchLambda(npi1, ArchLambda(pi2, nFBody)))
      val newFunParam = ParamDef(newFun.t)
      Let(
        newFunParam,
        body.visitAndRebuild{
          case FunctionCall(FunctionCall(ParamUse(pfd), in1, _), in2, _) if pfd.id == pf.id =>
            FunctionCall(FunctionCall(ParamUse(newFunParam), MapOrderedStream(levels, SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), in1)), in2)
          case e => e
        }.asInstanceOf[Expr],
        newFun
      )

    case Let(pf, body, ArchLambda(pi1, ArchLambda(pi2, fBody, _), _), _) if {
      var exprFound = false
      fBody.visit{
        case MapOrderedStream(fun: LambdaT, ParamUse(pd), _) if pd.id == pi2.id && {
          var exprFound = false
          val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostBody.body match {
            case SplitOrderedStream(ParamUse(p), _, _) if p.id == innerMostBody.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      var chunkSize: ArithTypeT = 0
      var npi2: ParamDef = null
      var levels: Int = 0
      val nFBody = fBody.visitAndRebuild{
        case MapOrderedStream(fun: LambdaT, ParamUse(pd), t) if pd.id == pi2.id && {
          var exprFound = false
          val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostBody.body match {
            case SplitOrderedStream(ParamUse(p), cs, _) if p.id == innerMostBody.param.id =>
              chunkSize = cs
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          levels = RulesHelper.getMapNDLevels(fun)
          npi2 = ParamDef(t)
          ParamUse(npi2)
        case e => e
      }.asInstanceOf[Expr]

      val newFun = TypeChecker.check(ArchLambda(pi1, ArchLambda(npi2, nFBody)))
      val newFunParam = ParamDef(newFun.t)
      Let(
        newFunParam,
        body.visitAndRebuild{
          case FunctionCall(FunctionCall(ParamUse(pfd), in1, _), in2, _) if pfd.id == pf.id =>
            FunctionCall(FunctionCall(ParamUse(newFunParam), in1), MapOrderedStream(levels, SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), in2))
          case e => e
        }.asInstanceOf[Expr],
        newFun
      )
  })

  def moveOutMapStmToVec: Rule = Rule("reshapeFunc_moveOutMapStmToVec", {
    case Let(pf, body, ArchLambda(pi1, ArchLambda(pi2, fBody, _), _), _) if {
      var exprFound = false
      fBody.visit{
        case MapOrderedStream(fun: LambdaT, ParamUse(pd), _) if pd.id == pi1.id && {
          var exprFound = false
          val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostBody.body match {
            case OrderedStreamToVector(ParamUse(p), _) if p.id == innerMostBody.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      var npi1: ParamDef = null
      var levels: Int = 0
      val nFBody = fBody.visitAndRebuild{
        case MapOrderedStream(fun: LambdaT, ParamUse(pd), t) if pd.id == pi1.id && {
          var exprFound = false
          val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostBody.body match {
            case OrderedStreamToVector(ParamUse(p), _) if p.id == innerMostBody.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          levels = RulesHelper.getMapNDLevels(fun)
          npi1 = ParamDef(t)
          ParamUse(npi1)
        case e => e
      }.asInstanceOf[Expr]

      val newFun = TypeChecker.check(ArchLambda(npi1, ArchLambda(pi2, nFBody)))
      val newFunParam = ParamDef(newFun.t)
      Let(
        newFunParam,
        body.visitAndRebuild{
          case FunctionCall(FunctionCall(ParamUse(pfd), in1, _), in2, _) if pfd.id == pf.id =>
            FunctionCall(FunctionCall(ParamUse(newFunParam), MapOrderedStream(levels, OrderedStreamToVector.asFunction(), in1)), in2)
          case e => e
        }.asInstanceOf[Expr],
        newFun
      )

    case Let(pf, body, ArchLambda(pi1, ArchLambda(pi2, fBody, _), _), _) if {
      var exprFound = false
      fBody.visit{
        case MapOrderedStream(fun: LambdaT, ParamUse(pd), _) if pd.id == pi2.id && {
          var exprFound = false
          val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostBody.body match {
            case OrderedStreamToVector(ParamUse(p), _) if p.id == innerMostBody.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      var npi2: ParamDef = null
      var levels: Int = 0
      val nFBody = fBody.visitAndRebuild{
        case MapOrderedStream(fun: LambdaT, ParamUse(pd), t) if pd.id == pi2.id && {
          var exprFound = false
          val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostBody.body match {
            case OrderedStreamToVector(ParamUse(p), _) if p.id == innerMostBody.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          levels = RulesHelper.getMapNDLevels(fun)
          npi2 = ParamDef(t)
          ParamUse(npi2)
        case e => e
      }.asInstanceOf[Expr]

      val newFun = TypeChecker.check(ArchLambda(pi1, ArchLambda(npi2, nFBody)))
      val newFunParam = ParamDef(newFun.t)
      Let(
        newFunParam,
        body.visitAndRebuild{
          case FunctionCall(FunctionCall(ParamUse(pfd), in1, _), in2, _) if pfd.id == pf.id =>
            FunctionCall(FunctionCall(ParamUse(newFunParam), in1), MapOrderedStream(levels, OrderedStreamToVector.asFunction(), in2))
          case e => e
        }.asInstanceOf[Expr],
        newFun
      )
  })
}