package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.{IntType, OrderedStreamTypeT, RamArrayTypeT}
import backend.hdl.arch.mem.{FakeMemoryAllocation, Read}
import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.arch.{ArchLambda, CounterInteger, MapOrderedStream}
import core.{Expr, LambdaT, Let, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object SynchronizeBitWidthRules extends RulesT {

  /**
   * The Rules aim at forcing memory read addresses are the same because we need to apply them to shared functions.
   * They also pretends that memory allocations are in the same size.
   */

  override def all(config: Option[Int]): Seq[Rule] = Seq(
    updateReadAddressBitWidth(config.getOrElse(32)),
    addFakeMemlloc(576)
  )

  def updateReadAddressBitWidth(bitWidth: Int = 32): Rule = Rule("updateReadAddressBits", {
    case MapOrderedStream(readFun: LambdaT, CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if
      t.leafType.bitWidth.ae.evalInt < bitWidth && {
        var exprFound = false
        val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
        innermostFun.body match {
          case Read(memAlloc, ParamUse(p), _) if p.id == innermostFun.param.id => exprFound = true
          case _ =>
        }
        exprFound
      } =>
      val newCounter = CounterInteger(start, increment, loop, dimensions, repetitions, Some(IntType(bitWidth)))
      val levels = RulesHelper.getMapNDLevels(readFun)
      val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
      var memAlloc: Expr = null
      innermostFun.body match {
        case Read(ma, ParamUse(p), _) if p.id == innermostFun.param.id => memAlloc = ma
        case _ =>
      }
      val newParam = ParamDef(IntType(bitWidth))
      val newFun = ArchLambda(newParam, Read(memAlloc, ParamUse(newParam)))
      MapOrderedStream(levels, newFun, newCounter)
  })

  def addFakeMemlloc(size: Int): Rule = Rule("addFakeMemlloc", {
    case Let(pm, MapOrderedStream(readFun: LambdaT, input, _), arg, _) if {
      var exprFound = false
      val innermostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
      innermostFun.body match {
        case Read(ParamUse(p1), ParamUse(p2), _) if p1.id == pm.id && p2.id == innermostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var found = false
      arg.t match {
        case rt: RamArrayTypeT if rt.len.ae.evalInt != size => found = true
        case _ =>
      }
      found
    } =>
      val newArg = TypeChecker.check(FakeMemoryAllocation(arg, size))
      val newParam = ParamDef(newArg.t)
      val levels = RulesHelper.getMapNDLevels(readFun)
      val newParam2 = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].leafType)
      val newBody = MapOrderedStream(levels, ArchLambda(newParam2, Read(ParamUse(newParam), ParamUse(newParam2))), input)
      Let(newParam, newBody, newArg)
  })
}