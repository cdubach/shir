package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.arch.mem.{BufferStreamInBlockRam, BufferStreamInHostRam}
import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.{BasicDataType, OrderedStreamTypeT, VectorTypeT}
import backend.hdl.arch.{ArchLambda, ConcatOrderedStream, JoinOrderedStream, JoinStreamAll, MapOrderedStream, OrderedStreamToVector, PadOrderedStream, Registered, Repeat, SlideGeneralOrderedStream, SplitOrderedStream, SplitStreamAll, TransposeNDOrderedStream, VectorToOrderedStream}
import core.{ArithTypeT, Expr, LambdaT, Marker, ParamDef, ParamUse, TextType, TextTypeT, TypeChecker}
import core.rewrite.{Rule, RulesT}

object BufferInsertionHelperRules extends RulesT {

  /**
   * Rules that adjust buffer's position. (Related to Tiling and Padding.)
   */

  override def all(config: Option[Int]): Seq[Rule] = Seq(
    skipTransposeND,
    skipSlide,
    skipPad,
    skipConcat,
    skipSplit,
    skipRepeat,
  )

  var identifierCount: Int = 0

  def skipTransposeND: Rule = Rule("BufferInsertionHelper_skipTransposeND", {
    case Marker(TransposeNDOrderedStream(input, dimOrder, _), ts: TextTypeT) if ts.s.contains("buffer") =>
      TransposeNDOrderedStream(Marker(input, ts), dimOrder)
  })

  def skipSlide: Rule = Rule("BufferInsertionHelper_skipSlideND", {
    case Marker(SlideGeneralOrderedStream(input, ws, ss, _), ts: TextTypeT) if ts.s.contains("buffer") =>
      SlideGeneralOrderedStream(Marker(input, ts), ws, ss)
    case Marker(MapOrderedStream(ArchLambda(p1, SlideGeneralOrderedStream(ParamUse(p2), ws, ss, _), _), input, _), ts: TextTypeT) if
      ts.s.contains("buffer") && p1.id == p2.id =>
      MapOrderedStream(ArchLambda(p1, SlideGeneralOrderedStream(ParamUse(p2), ws, ss)), Marker(input, ts))
  })

  def skipPad: Rule = Rule("BufferInsertionHelper_skipPad", {
    case Marker(PadOrderedStream(input, fs, ls, v,_), ts: TextTypeT) if ts.s.contains("buffer") =>
      PadOrderedStream(Marker(input, ts), fs, ls, v)
    case Marker(MapOrderedStream(ArchLambda(p1, PadOrderedStream(ParamUse(p2), fs, ls, v, _), _), input, _), ts: TextTypeT) if
      ts.s.contains("buffer") && p1.id == p2.id =>
      MapOrderedStream(ArchLambda(p1, PadOrderedStream(ParamUse(p2), fs, ls, v)), Marker(input, ts))
  })

  def skipConcat: Rule = Rule("BufferInsertionHelper_skipConcat", {
    case Marker(ConcatOrderedStream(input, concatIn, dim, _), ts: TextTypeT) if ts.s.contains("buffer") =>
      ConcatOrderedStream(Marker(input, ts), concatIn, dim)
  })

  def skipSplit: Rule = Rule("BufferInsertionHelper_skipSplit", {
    case Marker(SplitOrderedStream(input, cs, _), ts: TextTypeT) if ts.s.contains("buffer") =>
      SplitOrderedStream(Marker(input, ts), cs)
    case Marker(MapOrderedStream(fun: LambdaT, input, _), ts: TextTypeT) if ts.s.contains("buffer") && {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case SplitOrderedStream(ParamUse(p), _, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      MapOrderedStream(fun, Marker(input, ts))
  })

  def skipRepeat: Rule = Rule("BufferInsertionHelper_skipRepeat", {
    case Marker(Repeat(input, times, _), ts: TextTypeT) if ts.s.contains("buffer") =>
      Repeat(Marker(input, ts), times)
    case Marker(MapOrderedStream(fun: LambdaT, input, _), ts: TextTypeT) if ts.s.contains("buffer") && {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case Repeat(ParamUse(p), _, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      MapOrderedStream(fun, Marker(input, ts))
  })

  def replaceWithRealBuffer: Rule = Rule("BufferInsertionHelper_replaceWithRealBuffer", {
    case Marker(input, ts: TextTypeT) if ts.s.contains("buffer") && input.t.isInstanceOf[OrderedStreamTypeT] && {
      /** Do not insert buffer if there is any another buffer in "input".
       * This trick is to force hardware memory tag follow datagen memory tag.
       */
      var exprFound = false
      input.visit{
        case Marker(_, ts: TextTypeT) if ts.s.contains("buffer") =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      val dims = input.t.asInstanceOf[OrderedStreamTypeT].dimensions
      var newInput: Expr = input

      // Convert inner VectorType into StreamType because Buffer primitive only accepts StreamTypes.
      var innerDims: Seq[ArithTypeT] = Seq()
      input.t.asInstanceOf[OrderedStreamTypeT].leafType match {
        case vt: VectorTypeT =>
          innerDims = vt.dimensions
          for(i <- 0 to innerDims.length - 1) {
            newInput = TypeChecker.check(MapOrderedStream(dims.length + i, VectorToOrderedStream.asFunction(), newInput))
          }
          val param = ParamDef()
          newInput = TypeChecker.check(MapOrderedStream(dims.length, ArchLambda(param, JoinStreamAll(ParamUse(param))), newInput))
        case _ =>
      }

      val newDims = newInput.t.asInstanceOf[OrderedStreamTypeT].dimensions

      /*
       * TODO: need a better way to determine if we want to use BlockRAM or HostRAM.
       *  Right now we assume that high-dimensional data is stored in BlockRAM..
       */
      val bufferData = newDims.length match {
        case 1 =>
          identifierCount = identifierCount + 1
          JoinOrderedStream(BufferStreamInHostRam(TypeChecker.check(
            SplitOrderedStream(newInput, newInput.t.asInstanceOf[OrderedStreamTypeT].len)), TextType("Inserted_" + identifierCount)))
        case 2 =>
          identifierCount = identifierCount + 1
          BufferStreamInHostRam(newInput, TextType("Inserted_" + identifierCount))
        case _ =>
          val joinAll = TypeChecker.check(JoinStreamAll(newInput))
          val bufferData = TypeChecker.check(BufferStreamInBlockRam(joinAll))
          SplitStreamAll(bufferData, newDims.dropRight(1).reverse)
      }
      val bufferDatatc = TypeChecker.check(bufferData)

      // Convert inner VectorType back
      var newOutput: Expr = bufferDatatc
      input.t.asInstanceOf[OrderedStreamTypeT].leafType match {
        case _: VectorTypeT =>
          val param = ParamDef()
          newOutput = TypeChecker.check(MapOrderedStream(dims.length, ArchLambda(param, SplitStreamAll(ParamUse(param), innerDims.dropRight(1).reverse)), newOutput))
          for(i <- innerDims.length - 1 to 0 by -1) {
            newOutput = TypeChecker.check(MapOrderedStream(dims.length + i, OrderedStreamToVector.asFunction(), newOutput))
          }
        case _ =>
      }
      newOutput
    case Marker(input, ts: TextTypeT) if ts.s.contains("buffer") && input.t.isInstanceOf[VectorTypeT] =>
      ???
    case Marker(input, ts: TextTypeT) if ts.s.contains("buffer") && input.t.isInstanceOf[BasicDataType] =>
      Registered(input)
  })
}
