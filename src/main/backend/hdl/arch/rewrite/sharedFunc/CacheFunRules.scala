package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.{HostRamType, HostRamTypeT, IntType, LogicType, OrderedStreamType, OrderedStreamTypeT, RamArrayType, RamArrayTypeT, SignedIntType, VectorType, VectorTypeT}
import backend.hdl.arch.{ArchLambda, CounterInteger, MapOrderedStream, OrderedStreamToVector, SplitOrderedStream}
import backend.hdl.arch.mem.{BMemAlloc, BMemRead, BMemWrite, WrappedMemRead}
import core.{Expr, FunctionCall, LambdaT, Let, Marker, ParamDef, ParamUse, TextType, TypeChecker}
import core.rewrite.{Rule, RulesT}

object CacheFunRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] = Seq(
    createBufferForOneInput(widthModifier = config.getOrElse(1))
  )

  def createBufferForOneInput(widthModifier: Int = 1, innerDim: Int = 576, outerDim: Int = 64, bitWidth: Int = 8): Rule = Rule("CacheFun_createBufferForOneInput", {
    /**
     * This Rule Add Caffe buffer to weight ports mainly.
     * Not sure if there is a better way to do this.
     */
    case e @ Let(pf, body, ArchLambda(pIAlloc, ArchLambda(pIAddr, ArchLambda(pWAlloc, ArchLambda(pWAddr, fBody, _), _), _), _), _) if {
      var exprFound = false
      pWAlloc.t match {
        case rt: RamArrayTypeT if rt.memoryLocation.isInstanceOf[HostRamTypeT] => exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      fBody.visit{
        case Marker(MapOrderedStream(fun1: LambdaT, Marker(Let(pm1, MapOrderedStream(readFun1: LambdaT, ParamUse(input1Param), _), ParamUse(arg1Param), _), ts1), _), ts0) if
          ts0.s.contains("map_fusion_guard") && input1Param.id == pWAddr.id && arg1Param.id == pWAlloc.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val bAlloc = BMemAlloc(OrderedStreamType(SignedIntType(innerDim * bitWidth), outerDim))
      val bAllocParam = ParamDef(bAlloc.t)
      val addrBitWidth = pWAddr.t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth

      // Calculate Weight Info
      val cacheLineSize = 512
      val wAllocParam = ParamDef(RamArrayType(VectorType(LogicType(), cacheLineSize),
        widthModifier * innerDim * bitWidth / cacheLineSize * outerDim, HostRamType))
      val wAddrParam = ParamDef(OrderedStreamType(OrderedStreamType(IntType(addrBitWidth),
        innerDim * bitWidth / cacheLineSize), outerDim))
      val weightAllocParam = ParamDef(wAllocParam.t)
      val weight = TypeChecker.check(Let(
        weightAllocParam,
        WrappedMemRead(ParamUse(weightAllocParam), ParamUse(wAddrParam), TextType("weight"), SignedIntType(bitWidth), Seq(innerDim, outerDim)),
        ParamUse(wAllocParam)
      ))
      val storeWeight = TypeChecker.check(BMemWrite(bAllocParam, weight))
      val writeWeightfun = TypeChecker.check(ArchLambda(wAllocParam, ArchLambda(wAddrParam, storeWeight)))
      val writeWeightfunParam = ParamDef(writeWeightfun.t)
      val weightCacheAddr = CounterInteger(0, 1, Seq(bAllocParam.t.asInstanceOf[RamArrayTypeT].len))

      // Rebuild function param
      val newPIAlloc = ParamDef(pIAlloc.t)
      val newPIAddr = ParamDef(pIAddr.t)
      val newPWAlloc = ParamDef(bAllocParam.t)
      val newPWAddr = ParamDef(weightCacheAddr.t)

      // Rebuild original function
      val newFBody = fBody.visitAndRebuild{
        case Marker(MapOrderedStream(fun1: LambdaT, Marker(Let(pm1, MapOrderedStream(readFun1: LambdaT, ParamUse(input1Param), _), ParamUse(arg1Param), _), ts1), wT), ts0) if
          ts0.s.contains("map_fusion_guard") && input1Param.id == pWAddr.id && arg1Param.id == pWAlloc.id =>
          val weight = TypeChecker.check(BMemRead(newPWAlloc, ParamUse(newPWAddr), bitWidth))
          val reshapedWeight = TypeChecker.check(MapOrderedStream(2, OrderedStreamToVector.asFunction(),
            MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(wT.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[VectorTypeT].len)), weight)))
          reshapedWeight
        case ParamUse(px) if px.id == pIAlloc.id => ParamUse(newPIAlloc)
        case ParamUse(px) if px.id == pIAddr.id => ParamUse(newPIAddr)
        case e => e
      }.asInstanceOf[Expr]

      val newFun = TypeChecker.check(ArchLambda(newPIAlloc, ArchLambda(newPIAddr, ArchLambda(newPWAlloc, ArchLambda(newPWAddr, newFBody)))))
      val newFunParam = ParamDef(newFun.t)
      val newFunAccess = TypeChecker.check(
        Let(
          newFunParam,
          body.visitAndRebuild{
            case FunctionCall(FunctionCall(FunctionCall(FunctionCall(ParamUse(pCall), iAllocIn, _), iAddrIn, _), wAllocIn, _), wAddrIn, _) if pCall.id == pf.id =>
              val weightCacheCall = TypeChecker.check(FunctionCall(FunctionCall(ParamUse(writeWeightfunParam), wAllocIn), wAddrIn))
              val newWeightCacheAddr = CounterInteger(0, 1, Seq(bAllocParam.t.asInstanceOf[RamArrayTypeT].len))
              val newFunCall = TypeChecker.check(FunctionCall(FunctionCall(FunctionCall(FunctionCall(ParamUse(newFunParam), iAllocIn), iAddrIn), weightCacheCall), newWeightCacheAddr))
              newFunCall
            case e => e
          }.asInstanceOf[Expr],
          newFun
        )
      )
      Let(
        bAllocParam,
        Let(
          writeWeightfunParam,
          newFunAccess,
          writeWeightfun
        ),
        bAlloc
      )
  })
}