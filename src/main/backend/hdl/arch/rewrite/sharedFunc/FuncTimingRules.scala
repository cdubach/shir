package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.VectorTypeT
import backend.hdl.arch.{Alternate, ArchLambda, MapOrderedStream, MapVector, OrderedStreamToVector, Registered, VectorGenerator}
import core.{Expr, Let, ParamDef, ParamUse}
import core.rewrite.{Rule, RulesT}

object FuncTimingRules extends RulesT {
  override def all(config: Option[Int]): Seq[Rule] = Seq(
    alternateStreamToVector,
    registerMapVecGen
  )

  def alternateStreamToVector: Rule = Rule("funcTimingRules_alternateStreamToVector", {
    case Let(p, body, ArchLambda(pf0, ArchLambda(pf1, fBody, _), _), _) if {
      var exprFound = false
      fBody.visit{
        case OrderedStreamToVector(ParamUse(pi0), _) if pi0.id == pf0.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val param0 = ParamDef(pf0.t)
      val param1 = ParamDef(pf1.t)
      val newFBody = fBody.visitAndRebuild{
        case OrderedStreamToVector(ParamUse(pi0), _) if pi0.id == pf0.id =>
          Alternate(OrderedStreamToVector.asFunction(), OrderedStreamToVector.asFunction(), ParamUse(param0))
        case ParamUse(pi1) if pi1.id == pf1.id => ParamUse(param1)
        case e => e
      }.asInstanceOf[Expr]
      Let(p, body, ArchLambda(param0, ArchLambda(param1, newFBody)))

    case Let(p, body, ArchLambda(pf0, ArchLambda(pf1, fBody, _), _), _) if {
      var exprFound = false
      fBody.visit {
        case OrderedStreamToVector(ParamUse(pi1), _) if pi1.id == pf1.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val param0 = ParamDef(pf0.t)
      val param1 = ParamDef(pf1.t)
      val newFBody = fBody.visitAndRebuild {
        case OrderedStreamToVector(ParamUse(pi1), _) if pi1.id == pf1.id =>
          Alternate(OrderedStreamToVector.asFunction(), OrderedStreamToVector.asFunction(), ParamUse(param1))
        case ParamUse(pi0) if pi0.id == pf0.id => ParamUse(param0)
        case e => e
      }.asInstanceOf[Expr]
      Let(p, body, ArchLambda(param0, ArchLambda(param1, newFBody)))
  })

  def registerMapVecGen: Rule = Rule("funcTimingRules_registerMapVecGen", {
    case Let(p, body, ArchLambda(pf0, ArchLambda(pf1, fBody, _), _), _) if {
      var exprFound = false
      fBody.visit {
        case MapOrderedStream(ArchLambda(p1, VectorGenerator(ParamUse(p2), times, _), _), ParamUse(pi0), _) if
          pi0.id == pf0.id && p1.id == p2.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val param0 = ParamDef(pf0.t)
      val param1 = ParamDef(pf1.t)
      val newFBody = fBody.visitAndRebuild {
        case MapOrderedStream(ArchLambda(p1, VectorGenerator(ParamUse(p2), times, _), _), ParamUse(pi0), _) if
          pi0.id == pf0.id && p1.id == p2.id =>
          val param = ParamDef(p1.t)
          MapOrderedStream(ArchLambda(param, VectorGenerator(MapVector(p1.t.asInstanceOf[VectorTypeT].dimensions.length,
            Registered.asFunction(), ParamUse(param)), times)), ParamUse(param0))
        case ParamUse(pi1) if pi1.id == pf1.id => ParamUse(param1)
        case e => e
      }.asInstanceOf[Expr]
      Let(p, body, ArchLambda(param0, ArchLambda(param1, newFBody)))

    case Let(p, body, ArchLambda(pf0, ArchLambda(pf1, fBody, _), _), _) if {
      var exprFound = false
      fBody.visit {
        case MapOrderedStream(ArchLambda(p1, VectorGenerator(ParamUse(p2), times, _), _), ParamUse(pi1), _) if
          pi1.id == pf1.id && p1.id == p2.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val param0 = ParamDef(pf0.t)
      val param1 = ParamDef(pf1.t)
      val newFBody = fBody.visitAndRebuild {
        case MapOrderedStream(ArchLambda(p1, VectorGenerator(ParamUse(p2), times, _), _), ParamUse(pi1), _) if
          pi1.id == pf1.id && p1.id == p2.id =>
          val param = ParamDef(p1.t)
          MapOrderedStream(ArchLambda(param, VectorGenerator(MapVector(p1.t.asInstanceOf[VectorTypeT].dimensions.length,
            Registered.asFunction(), ParamUse(param)), times)), ParamUse(param1))
        case ParamUse(pi0) if pi0.id == pf0.id => ParamUse(param0)
        case e => e
      }.asInstanceOf[Expr]
      Let(p, body, ArchLambda(param0, ArchLambda(param1, newFBody)))
  })
}