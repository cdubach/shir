package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.mem.{FakeMemoryAllocation, MemoryAllocation, Read, ReadOutOfBounds}
import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.arch.{ArchLambda, JoinOrderedStream, MapOrderedStream, MapOrderedStream2Input, Repeat, Select2, Tuple2, VectorToOrderedStream, Zip2OrderedStream}
import core.{ArithTypeT, Expr, FunctionCall, LambdaT, Let, Marker, ParamDef, ParamUse, TextType, Type, TypeChecker}
import core.rewrite.{Rule, RulesT}

object PropagateFunRules extends RulesT {
  override def all(config: Option[Int]): Seq[Rule] = Seq(
    mergeMapStm,
    mergeRepeat,
    mergeMapRepeat,
    mergeMapJoin,
    mergeMapVec2Stm,
    mergeRead
  )

  def mergeMapStm: Rule = Rule("propagateFun_mergeMapStm", {
    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var matchParamUses = 0
      var otherParamUses = 0
      body.visit{
        case MapOrderedStream(ArchLambda(p1, FunctionCall(FunctionCall(ParamUse(p),
        Select2(ParamUse(p2), sel0, _), _), Select2(ParamUse(p3), sel1, _), _), _),
        Zip2OrderedStream(Tuple2(input0, input1, _), _), _) if
          p.id == pf.id && p1.id == p2.id && p1.id == p3.id &&
            sel0.ae.evalInt == 0 && sel1.ae.evalInt == 1 =>
          matchParamUses = matchParamUses + 1
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      matchParamUses == otherParamUses && matchParamUses > 0
    } =>
      // Get input information
      var input0Type: Type = null
      var input1Type: Type = null
      body.visit {
        case MapOrderedStream(ArchLambda(p1, FunctionCall(FunctionCall(ParamUse(p),
        Select2(ParamUse(p2), sel0, _), _), Select2(ParamUse(p3), sel1, _), _), _),
        Zip2OrderedStream(Tuple2(input0, input1, _), _), _) if
          p.id == pf.id && p1.id == p2.id && p1.id == p3.id &&
            sel0.ae.evalInt == 0 && sel1.ae.evalInt == 1 =>
          input0Type = input0.t
          input1Type = input1.t
        case _ =>
      }

      // Rebuild function parameters
      val fParam0 = ParamDef(input0Type)
      val fParam1 = ParamDef(input1Type)

      // Rebuild function body
      val zipNewInputs = TypeChecker.check(Zip2OrderedStream(Tuple2(ParamUse(fParam0), ParamUse(fParam1))))
      val zipParam = ParamDef(zipNewInputs.t.asInstanceOf[OrderedStreamTypeT].et)
      val newFBodyInner = fBody.visitAndRebuild{
        case ParamUse(pi1) if pi1.id == pf1.id => Select2(ParamUse(zipParam), 0)
        case ParamUse(pi2) if pi2.id == pf2.id => Select2(ParamUse(zipParam), 1)
        case e => e
      }.asInstanceOf[Expr]
      val newFBody = TypeChecker.check(MapOrderedStream(ArchLambda(zipParam, newFBodyInner), zipNewInputs))

      // Rebuild function
      val newFun = TypeChecker.check(ArchLambda(fParam0, ArchLambda(fParam1, newFBody)))
      val newFunParam = ParamDef(newFun.t)

      // Rebuild Let body
      val newBody = body.visitAndRebuild{
        case MapOrderedStream(ArchLambda(p1, FunctionCall(FunctionCall(ParamUse(p),
        Select2(ParamUse(p2), sel0, _), _), Select2(ParamUse(p3), sel1, _), _), _),
        Zip2OrderedStream(Tuple2(input0, input1, _), _), _) if
          p.id == pf.id && p1.id == p2.id && p1.id == p3.id &&
            sel0.ae.evalInt == 0 && sel1.ae.evalInt == 1 =>
          FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild Let
      Let(newFunParam, newBody, newFun)
  })

  def mergeMapStm2Input: Rule = Rule("propagateFun_mergeMapStm2Input", {
    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var matchParamUses = 0
      var otherParamUses = 0
      body.visit {
        case MapOrderedStream2Input(ArchLambda(p0, ArchLambda(p1,
        FunctionCall(FunctionCall(ParamUse(p), ParamUse(p2), _), ParamUse(p3), _), _), _), input0, input1, _) if
          p.id == pf.id && p0.id == p2.id && p1.id == p3.id =>
          matchParamUses = matchParamUses + 1
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      matchParamUses == otherParamUses && matchParamUses > 0
    } =>
      // Get input information
      var input0Type: Type = null
      var input1Type: Type = null
      body.visit {
        case MapOrderedStream2Input(ArchLambda(p0, ArchLambda(p1,
        FunctionCall(FunctionCall(ParamUse(p), ParamUse(p2), _), ParamUse(p3), _), _), _), input0, input1, _) if
          p.id == pf.id && p0.id == p2.id && p1.id == p3.id =>
          input0Type = input0.t
          input1Type = input1.t
        case _ =>
      }

      // Rebuild function parameters
      val fParam0 = ParamDef(input0Type)
      val fParam1 = ParamDef(input1Type)
      val fParam0Inner = ParamDef(fParam0.t.asInstanceOf[OrderedStreamTypeT].et)
      val fParam1Inner = ParamDef(fParam1.t.asInstanceOf[OrderedStreamTypeT].et)

      // Rebuild function body
      val newFBody = fBody.visitAndRebuild {
        case ParamUse(pi1) if pi1.id == pf1.id => ParamUse(fParam0Inner)
        case ParamUse(pi2) if pi2.id == pf2.id => ParamUse(fParam1Inner)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild function
      val newFun = TypeChecker.check(ArchLambda(fParam0, ArchLambda(fParam1,
        MapOrderedStream2Input(ArchLambda(fParam0Inner, ArchLambda(fParam1Inner, newFBody)), ParamUse(fParam0), ParamUse(fParam1)))))
      val newFunParam = ParamDef(newFun.t)

      // Rebuild Let body
      val newBody = body.visitAndRebuild {
        case MapOrderedStream2Input(ArchLambda(p0, ArchLambda(p1,
        FunctionCall(FunctionCall(ParamUse(p), ParamUse(p2), _), ParamUse(p3), _), _), _), input0, input1, _) if
          p.id == pf.id && p0.id == p2.id && p1.id == p3.id =>
          FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild Let
      Let(newFunParam, newBody, newFun)
  })

  def mergeRepeat: Rule = Rule("propagateFun_mergeRepeat", {
    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var reps: Seq[ArithTypeT] = Seq()
      var otherParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), Repeat(input0, repetitions, _), _), input1, _) if p.id == pf.id =>
          reps = reps :+ repetitions
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      reps.length == otherParamUses && reps.forall(_ == reps.head) && !reps.isEmpty
    } =>
      // Get input information
      var input0Type: Type = null
      var input1Type: Type = null
      var times: ArithTypeT = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), Repeat(input0, repetitions, _), _), input1, _) if p.id == pf.id =>
          input0Type = input0.t
          input1Type = input1.t
          times = repetitions
        case _ =>
      }

      // Rebuild function parameters
      val fParam0 = ParamDef(input0Type)
      val fParam1 = ParamDef(input1Type)

      // Rebuild function body
      val newFBody = fBody.visitAndRebuild{
        case ParamUse(pi1) if pi1.id == pf1.id => Repeat(ParamUse(fParam0), times)
        case ParamUse(pi2) if pi2.id == pf2.id => ParamUse(fParam1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild function
      val newFun = TypeChecker.check(ArchLambda(fParam0, ArchLambda(fParam1, newFBody)))
      val newFunParam = ParamDef(newFun.t)

      // Rebuild Let body
      val newBody = body.visitAndRebuild {
        case FunctionCall(FunctionCall(ParamUse(p), Repeat(input0, repetitions, _), _), input1, _) if p.id == pf.id =>
          FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild Let
      Let(newFunParam, newBody, newFun)

    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var reps: Seq[ArithTypeT] = Seq()
      var otherParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), Repeat(input1, repetitions, _), _) if p.id == pf.id =>
          reps = reps :+ repetitions
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      reps.length == otherParamUses && reps.forall(_ == reps.head) && !reps.isEmpty
    } =>
      // Get input information
      var input0Type: Type = null
      var input1Type: Type = null
      var times: ArithTypeT = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), Repeat(input1, repetitions, _), _) if p.id == pf.id =>
          input0Type = input0.t
          input1Type = input1.t
          times = repetitions
        case _ =>
      }

      // Rebuild function parameters
      val fParam0 = ParamDef(input0Type)
      val fParam1 = ParamDef(input1Type)

      // Rebuild function body
      val newFBody = fBody.visitAndRebuild {
        case ParamUse(pi1) if pi1.id == pf1.id => ParamUse(fParam0)
        case ParamUse(pi2) if pi2.id == pf2.id => Repeat(ParamUse(fParam1), times)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild function
      val newFun = TypeChecker.check(ArchLambda(fParam0, ArchLambda(fParam1, newFBody)))
      val newFunParam = ParamDef(newFun.t)

      // Rebuild Let body
      val newBody = body.visitAndRebuild {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), Repeat(input1, repetitions, _), _) if p.id == pf.id =>
          FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild Let
      Let(newFunParam, newBody, newFun)
  })

  def mergeMapRepeat: Rule = Rule("propagateFun_mergeMapRepeat", {
    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var reps: Seq[ArithTypeT] = Seq()
      var otherParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), MapOrderedStream(ArchLambda(p1,
        Repeat(ParamUse(p2), repetitions, _), _), input0, _), _), input1, _) if p.id == pf.id && p1.id == p2.id =>
          reps = reps :+ repetitions
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      reps.length == otherParamUses && reps.forall(_ == reps.head) && !reps.isEmpty
    } =>
      // Get input information
      var input0Type: Type = null
      var input1Type: Type = null
      var times: ArithTypeT = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), MapOrderedStream(ArchLambda(p1,
        Repeat(ParamUse(p2), repetitions, _), _), input0, _), _), input1, _) if p.id == pf.id && p1.id == p2.id =>
          input0Type = input0.t
          input1Type = input1.t
          times = repetitions
        case _ =>
      }

      // Rebuild function parameters
      val fParam0 = ParamDef(input0Type)
      val fParam1 = ParamDef(input1Type)

      // Rebuild function body
      val newFBody = fBody.visitAndRebuild {
        case ParamUse(pi1) if pi1.id == pf1.id => MapOrderedStream(Repeat.asFunction(Seq(None), Seq(times)), ParamUse(fParam0))
        case ParamUse(pi2) if pi2.id == pf2.id => ParamUse(fParam1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild function
      val newFun = TypeChecker.check(ArchLambda(fParam0, ArchLambda(fParam1, newFBody)))
      val newFunParam = ParamDef(newFun.t)

      // Rebuild Let body
      val newBody = body.visitAndRebuild {
        case FunctionCall(FunctionCall(ParamUse(p), MapOrderedStream(ArchLambda(p1,
        Repeat(ParamUse(p2), repetitions, _), _), input0, _), _), input1, _) if p.id == pf.id && p1.id == p2.id =>
          FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild Let
      Let(newFunParam, newBody, newFun)

    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var reps: Seq[ArithTypeT] = Seq()
      var otherParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), MapOrderedStream(ArchLambda(p1,
        Repeat(ParamUse(p2), repetitions, _), _), input1, _), _) if p.id == pf.id && p1.id == p2.id =>
          reps = reps :+ repetitions
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      reps.length == otherParamUses && reps.forall(_ == reps.head) && !reps.isEmpty
    } =>
      // Get input information
      var input0Type: Type = null
      var input1Type: Type = null
      var times: ArithTypeT = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), MapOrderedStream(ArchLambda(p1,
        Repeat(ParamUse(p2), repetitions, _), _), input1, _), _) if p.id == pf.id && p1.id == p2.id =>
          input0Type = input0.t
          input1Type = input1.t
          times = repetitions
        case _ =>
      }

      // Rebuild function parameters
      val fParam0 = ParamDef(input0Type)
      val fParam1 = ParamDef(input1Type)

      // Rebuild function body
      val newFBody = fBody.visitAndRebuild {
        case ParamUse(pi1) if pi1.id == pf1.id => ParamUse(fParam0)
        case ParamUse(pi2) if pi2.id == pf2.id => MapOrderedStream(Repeat.asFunction(Seq(None), Seq(times)), ParamUse(fParam1))
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild function
      val newFun = TypeChecker.check(ArchLambda(fParam0, ArchLambda(fParam1, newFBody)))
      val newFunParam = ParamDef(newFun.t)

      // Rebuild Let body
      val newBody = body.visitAndRebuild {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), MapOrderedStream(ArchLambda(p1,
        Repeat(ParamUse(p2), repetitions, _), _), input1, _), _) if p.id == pf.id && p1.id == p2.id =>
          FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild Let
      Let(newFunParam, newBody, newFun)
  })

  def mergeMapJoin: Rule = Rule("propagateFun_mergeMapJoin", {
    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var allLevels: Seq[Int] = Seq()
      var otherParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), MapOrderedStream(fun: LambdaT, input0, _), _), input1, _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case JoinOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          allLevels = allLevels :+ RulesHelper.getMapNDLevels(fun)
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      allLevels.length == otherParamUses  && allLevels.forall(_ == allLevels.head) && !allLevels.isEmpty
    } =>
      // Get input information
      var input0Type: Type = null
      var input1Type: Type = null
      var levels: Int = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), MapOrderedStream(fun: LambdaT, input0, _), _), input1, _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case JoinOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          input0Type = input0.t
          input1Type = input1.t
          levels = RulesHelper.getMapNDLevels(fun)
        case _ =>
      }

      // Rebuild function parameters
      val fParam0 = ParamDef(input0Type)
      val fParam1 = ParamDef(input1Type)

      // Rebuild function body
      val newFBody = fBody.visitAndRebuild {
        case ParamUse(pi1) if pi1.id == pf1.id => MapOrderedStream(levels, JoinOrderedStream.asFunction(), ParamUse(fParam0))
        case ParamUse(pi2) if pi2.id == pf2.id => ParamUse(fParam1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild function
      val newFun = TypeChecker.check(ArchLambda(fParam0, ArchLambda(fParam1, newFBody)))
      val newFunParam = ParamDef(newFun.t)

      // Rebuild Let body
      val newBody = body.visitAndRebuild {
        case FunctionCall(FunctionCall(ParamUse(p), MapOrderedStream(fun: LambdaT, input0, _), _), input1, _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case JoinOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild Let
      Let(newFunParam, newBody, newFun)

    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var allLevels: Seq[Int] = Seq()
      var otherParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), MapOrderedStream(fun: LambdaT, input1, _), _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case JoinOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          allLevels = allLevels :+ RulesHelper.getMapNDLevels(fun)
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      allLevels.length == otherParamUses  && allLevels.forall(_ == allLevels.head) && !allLevels.isEmpty
    } =>
      // Get input information
      var input0Type: Type = null
      var input1Type: Type = null
      var levels: Int = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), MapOrderedStream(fun: LambdaT, input1, _), _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case JoinOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          input0Type = input0.t
          input1Type = input1.t
          levels = RulesHelper.getMapNDLevels(fun)
        case _ =>
      }

      // Rebuild function parameters
      val fParam0 = ParamDef(input0Type)
      val fParam1 = ParamDef(input1Type)

      // Rebuild function body
      val newFBody = fBody.visitAndRebuild {
        case ParamUse(pi1) if pi1.id == pf1.id => ParamUse(fParam0)
        case ParamUse(pi2) if pi2.id == pf2.id => MapOrderedStream(levels, JoinOrderedStream.asFunction(), ParamUse(fParam1))
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild function
      val newFun = TypeChecker.check(ArchLambda(fParam0, ArchLambda(fParam1, newFBody)))
      val newFunParam = ParamDef(newFun.t)

      // Rebuild Let body
      val newBody = body.visitAndRebuild {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), MapOrderedStream(fun: LambdaT, input1, _), _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case JoinOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild Let
      Let(newFunParam, newBody, newFun)
  })

  def mergeMapVec2Stm: Rule = Rule("propagateFun_mergeMapVec2Stm", {
    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var allLevels: Seq[Int] = Seq()
      var otherParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), MapOrderedStream(fun: LambdaT, input0, _), _), input1, _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case VectorToOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          allLevels = allLevels :+ RulesHelper.getMapNDLevels(fun)
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      allLevels.length == otherParamUses && allLevels.forall(_ == allLevels.head) && !allLevels.isEmpty
    } =>
      // Get input information
      var input0Type: Type = null
      var input1Type: Type = null
      var levels: Int = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), MapOrderedStream(fun: LambdaT, input0, _), _), input1, _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case VectorToOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          input0Type = input0.t
          input1Type = input1.t
          levels = RulesHelper.getMapNDLevels(fun)
        case _ =>
      }

      // Rebuild function parameters
      val fParam0 = ParamDef(input0Type)
      val fParam1 = ParamDef(input1Type)

      // Rebuild function body
      val newFBody = fBody.visitAndRebuild {
        case ParamUse(pi1) if pi1.id == pf1.id => MapOrderedStream(levels, VectorToOrderedStream.asFunction(), ParamUse(fParam0))
        case ParamUse(pi2) if pi2.id == pf2.id => ParamUse(fParam1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild function
      val newFun = TypeChecker.check(ArchLambda(fParam0, ArchLambda(fParam1, newFBody)))
      val newFunParam = ParamDef(newFun.t)

      // Rebuild Let body
      val newBody = body.visitAndRebuild {
        case FunctionCall(FunctionCall(ParamUse(p), MapOrderedStream(fun: LambdaT, input0, _), _), input1, _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case VectorToOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild Let
      Let(newFunParam, newBody, newFun)

    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var allLevels: Seq[Int] = Seq()
      var otherParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), MapOrderedStream(fun: LambdaT, input1, _), _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case VectorToOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          allLevels = allLevels :+ RulesHelper.getMapNDLevels(fun)
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      allLevels.length == otherParamUses && allLevels.forall(_ == allLevels.head) && !allLevels.isEmpty
    } =>
      // Get input information
      var input0Type: Type = null
      var input1Type: Type = null
      var levels: Int = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), MapOrderedStream(fun: LambdaT, input1, _), _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case VectorToOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          input0Type = input0.t
          input1Type = input1.t
          levels = RulesHelper.getMapNDLevels(fun)
        case _ =>
      }

      // Rebuild function parameters
      val fParam0 = ParamDef(input0Type)
      val fParam1 = ParamDef(input1Type)

      // Rebuild function body
      val newFBody = fBody.visitAndRebuild {
        case ParamUse(pi1) if pi1.id == pf1.id => ParamUse(fParam0)
        case ParamUse(pi2) if pi2.id == pf2.id => MapOrderedStream(levels, VectorToOrderedStream.asFunction(), ParamUse(fParam1))
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild function
      val newFun = TypeChecker.check(ArchLambda(fParam0, ArchLambda(fParam1, newFBody)))
      val newFunParam = ParamDef(newFun.t)

      // Rebuild Let body
      val newBody = body.visitAndRebuild {
        case FunctionCall(FunctionCall(ParamUse(p), input0, _), MapOrderedStream(fun: LambdaT, input1, _), _) if p.id == pf.id && {
          var exprFound = false
          val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
          joinInnerMost.body match {
            case VectorToOrderedStream(ParamUse(p3), _) if p3.id == joinInnerMost.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1)
        case e => e
      }.asInstanceOf[Expr]

      // Rebuild Let
      Let(newFunParam, newBody, newFun)
  })

  def mergeRead: Rule = Rule("propagateFun_mergeRead", {
    /**
     * This merges two reads into a function at the same time. We need this hack for now because it is a bit complex
     * to merge them separately. After merging, the number of input ports increases, it is very tricky to write cases
     * for different ports.
     * TODO: Make it more general.
     */
    case Let(pf, body, ArchLambda(pf1, ArchLambda(pf2, fBody, _), _), _) if {
      var fun0Levels: Seq[Int] = Seq()
      var fun1Levels: Seq[Int] = Seq()
      var readFun0Levels: Seq[Int] = Seq()
      var readFun1Levels: Seq[Int] = Seq()
      var otherParamUses = 0
      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p),
        Marker(MapOrderedStream(fun0: LambdaT, Marker(Let(pm0, MapOrderedStream(readFun0: LambdaT, input0, _), arg0@(MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), _),
        Marker(MapOrderedStream(fun1: LambdaT, Marker(Let(pm1, MapOrderedStream(readFun1: LambdaT, input1, _), arg1@(MemoryAllocation(_, _, _, _, _) | FakeMemoryAllocation(_, _, _)), _), ts3), _), ts2), _)
          if p.id == pf.id && ts0.s.contains("map_fusion_guard") && ts2.s.contains("map_fusion_guard") && {
            var exprFound = false
            val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun0)
            innerMostFun.body match {
              case Read(ParamUse(p3), ParamUse(p4), _) if p3.id == pm0.id && p4.id == innerMostFun.param.id => exprFound = true
              case ReadOutOfBounds(ParamUse(p3), ParamUse(p4), _, _, _) if p3.id == pm0.id && p4.id == innerMostFun.param.id => exprFound = true
              case _ =>
            }
            exprFound
          } && {
            var exprFound = false
            val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun1)
            innerMostFun.body match {
              case Read(ParamUse(p3), ParamUse(p4), _) if p3.id == pm1.id && p4.id == innerMostFun.param.id => exprFound = true
              case ReadOutOfBounds(ParamUse(p3), ParamUse(p4), _, _, _) if p3.id == pm1.id && p4.id == innerMostFun.param.id => exprFound = true
              case _ =>
            }
            exprFound
          } =>
          fun0Levels = fun0Levels :+ RulesHelper.getMapNDLevels(fun0)
          fun1Levels = fun1Levels :+ RulesHelper.getMapNDLevels(fun1)
          readFun0Levels = readFun0Levels :+ RulesHelper.getMapNDLevels(readFun0)
          readFun1Levels = readFun1Levels :+ RulesHelper.getMapNDLevels(readFun1)
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      fun0Levels.length == otherParamUses  && !fun0Levels.isEmpty &&
        fun0Levels.forall(_ == fun0Levels.head) && fun1Levels.forall(_ == fun1Levels.head) &&
        readFun0Levels.forall(_ == readFun0Levels.head) && readFun1Levels.forall(_ == readFun1Levels.head)
    } =>
      // Collect infos
      var input0Type: Type = null
      var input1Type: Type = null
      var arg0Type: Type = null
      var arg1Type: Type = null
      var ffun0: LambdaT = null
      var ffun1: LambdaT = null
      var freadFun0: LambdaT = null
      var freadFun1: LambdaT = null
      var fpm0: ParamDef = null
      var fpm1: ParamDef = null

      body.visit {
        case FunctionCall(FunctionCall(ParamUse(p),
        Marker(MapOrderedStream(fun0: LambdaT, Marker(Let(pm0, MapOrderedStream(readFun0: LambdaT, input0, _), arg0@(MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), _),
        Marker(MapOrderedStream(fun1: LambdaT, Marker(Let(pm1, MapOrderedStream(readFun1: LambdaT, input1, _), arg1@(MemoryAllocation(_, _, _, _, _) | FakeMemoryAllocation(_, _, _)), _), ts3), _), ts2), _)
          if p.id == pf.id && ts0.s.contains("map_fusion_guard") && ts2.s.contains("map_fusion_guard") && {
            var exprFound = false
            val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun0)
            innerMostFun.body match {
              case Read(ParamUse(p3), ParamUse(p4), _) if p3.id == pm0.id && p4.id == innerMostFun.param.id => exprFound = true
              case ReadOutOfBounds(ParamUse(p3), ParamUse(p4), _, _, _) if p3.id == pm0.id && p4.id == innerMostFun.param.id => exprFound = true
              case _ =>
            }
            exprFound
          } && {
            var exprFound = false
            val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun1)
            innerMostFun.body match {
              case Read(ParamUse(p3), ParamUse(p4), _) if p3.id == pm1.id && p4.id == innerMostFun.param.id => exprFound = true
              case ReadOutOfBounds(ParamUse(p3), ParamUse(p4), _, _, _) if p3.id == pm1.id && p4.id == innerMostFun.param.id => exprFound = true
              case _ =>
            }
            exprFound
          } =>
          input0Type = input0.t
          input1Type = input1.t
          arg0Type = arg0.t
          arg1Type = arg1.t
          ffun0 = fun0
          ffun1 = fun1
          freadFun0 = readFun0
          freadFun1 = readFun1
          fpm0 = pm0
          fpm1 = pm1
        case _ =>
      }

      val arg0Param = ParamDef(arg0Type)
      val input0Param = ParamDef(input0Type)
      val arg1Param = ParamDef(arg1Type)
      val input1Param = ParamDef(input1Type)

      // Rebuild function
      val newFBody = fBody.visitAndRebuild{
        case ParamUse(px1) if px1.id == pf1.id =>
          Marker(MapOrderedStream(ffun0, Marker(Let(fpm0, MapOrderedStream(freadFun0, ParamUse(input0Param)), ParamUse(arg0Param)), TextType("input_image"))), TextType("map_fusion_guard"))
        case ParamUse(px2) if px2.id == pf2.id =>
          Marker(MapOrderedStream(ffun1, Marker(Let(fpm1, MapOrderedStream(freadFun1, ParamUse(input1Param)), ParamUse(arg1Param)), TextType("input_weight"))), TextType("map_fusion_guard"))
        case e => e
      }.asInstanceOf[Expr]

      val newFun = TypeChecker.check(ArchLambda(arg0Param, ArchLambda(input0Param, ArchLambda(arg1Param, ArchLambda(input1Param, newFBody)))))
      val newFunParam = ParamDef(newFun.t)
      Let(
        newFunParam,
        body.visitAndRebuild{
          case FunctionCall(FunctionCall(ParamUse(p),
          Marker(MapOrderedStream(fun0: LambdaT, Marker(Let(pm0, MapOrderedStream(readFun0: LambdaT, input0, _), arg0@(MemoryAllocation(_, _, _, _, _) | Marker(ParamUse(_), _) | FakeMemoryAllocation(_, _, _)), _), ts1), _), ts0), _),
          Marker(MapOrderedStream(fun1: LambdaT, Marker(Let(pm1, MapOrderedStream(readFun1: LambdaT, input1, _), arg1@(MemoryAllocation(_, _, _, _, _) | FakeMemoryAllocation(_, _, _)), _), ts3), _), ts2), _)
            if p.id == pf.id && ts0.s.contains("map_fusion_guard") && ts2.s.contains("map_fusion_guard") && {
              var exprFound = false
              val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun0)
              innerMostFun.body match {
                case Read(ParamUse(p3), ParamUse(p4), _) if p3.id == pm0.id && p4.id == innerMostFun.param.id => exprFound = true
                case ReadOutOfBounds(ParamUse(p3), ParamUse(p4), _, _, _) if p3.id == pm0.id && p4.id == innerMostFun.param.id => exprFound = true
                case _ =>
              }
              exprFound
            } && {
              var exprFound = false
              val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun1)
              innerMostFun.body match {
                case Read(ParamUse(p3), ParamUse(p4), _) if p3.id == pm1.id && p4.id == innerMostFun.param.id => exprFound = true
                case ReadOutOfBounds(ParamUse(p3), ParamUse(p4), _, _, _) if p3.id == pm1.id && p4.id == innerMostFun.param.id => exprFound = true
                case _ =>
              }
              exprFound
            } =>
            FunctionCall(FunctionCall(FunctionCall(FunctionCall(ParamUse(newFunParam), arg0), input0), arg1), input1)
          case e => e
        }.asInstanceOf[Expr],
        newFun
      )
  })
}