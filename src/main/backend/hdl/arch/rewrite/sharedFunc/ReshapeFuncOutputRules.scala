package backend.hdl.arch.rewrite.sharedFunc

import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.arch.{ArchLambda, MapOrderedStream, VectorToOrderedStream}
import core.{Expr, FunctionCall, LambdaT, Let, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object ReshapeFuncOutputRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] = Seq(
    moveOutVecToStm
  )

  def moveOutVecToStm: Rule = Rule("reshapeFuncOutput_moveOutVecToStm", {
    case Let(pf, body, ArchLambda(pf0, ArchLambda(pf1, MapOrderedStream(fun: LambdaT, fBody, _), _), _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case VectorToOrderedStream(ParamUse(p1), _) if p1.id == innerMostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val newFun = TypeChecker.check(ArchLambda(pf0, ArchLambda(pf1, fBody)))
      val newFunParam = ParamDef(newFun.t)
      Let(
        newFunParam,
        body.visitAndRebuild{
          case FunctionCall(FunctionCall(ParamUse(pd), input0, _), input1, _) if pd.id == pf.id =>
            MapOrderedStream(levels, innerMostFun,
              FunctionCall(FunctionCall(ParamUse(newFunParam), input0), input1))
          case e => e
        }.asInstanceOf[Expr],
        newFun
      )
  })

  def moveInStmToVec: Rule = Rule("reshapeFuncOutput_moveInStmToVec", {
    case Let(pf, body, ArchLambda(pf0, fBody, _), _) if {
      var levels: Seq[Int] = Seq()
      var otherParamUses = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, FunctionCall(ParamUse(p), input, _), _) if p.id == pf.id =>
          levels = levels :+ RulesHelper.getMapNDLevels(fun)
        case ParamUse(p) if p.id == pf.id =>
          otherParamUses = otherParamUses + 1
        case _ =>
      }
      levels.length == otherParamUses && levels.forall(_ == levels.head) && !levels.isEmpty
    } =>
      var level: Int = 0
      var fun: LambdaT = null
      body.visit {
        case MapOrderedStream(f: LambdaT, FunctionCall(ParamUse(p), input, _), _) if p.id == pf.id =>
          level = RulesHelper.getMapNDLevels(f)
          fun = f
        case _ =>
      }
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val newFun = TypeChecker.check(ArchLambda(pf0, MapOrderedStream(level, innerMostFun, fBody)))
      val newFunParam = ParamDef(newFun.t)
      Let(
        newFunParam,
        body.visitAndRebuild {
          case MapOrderedStream(fun: LambdaT, FunctionCall(ParamUse(p), input, _), _) if p.id == pf.id =>
            FunctionCall(ParamUse(newFunParam), input)
          case e => e
        }.asInstanceOf[Expr],
        newFun
      )
  })
}