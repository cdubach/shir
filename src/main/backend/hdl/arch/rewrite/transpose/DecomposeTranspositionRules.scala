package backend.hdl.arch.rewrite.transpose

import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.arch.{ArchLambda, MapOrderedStream, TransposeNDOrderedStream}
import core.{ArithType, Expr, LambdaT, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}
import core.util.IRDotGraph

object DecomposeTranspositionRules extends RulesT{

  /**
   * Handle Transposition of multiple complex inputs (2 for now). These two inputs comes MapStms which mix together.
   * It makes direct map-swapping difficult. To solve the issues, we first separate one complicated Transposition into
   * several simple ones. This requires us to re-calculate dimension orders of top level (output) and two inputs.
   */

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      transposeND2InputMapNDs
    )

  def transposeND2InputMapNDs: Rule = Rule("DecomposeTransposition_transposeND2InputMapNDs", {
    case TransposeNDOrderedStream(MapOrderedStream(fun1: LambdaT, input1, _), dimOrder, _) if {
      var exprFound = false
      var fun1Levels = 0
      var fun2Levels = 0
      val innerMostFun1 = RulesHelper.getCurrentOrInnerMostLambda(fun1)
      innerMostFun1.body match {
        case MapOrderedStream(fun2: LambdaT, input2, _) if !RulesHelper.containsParam(input2, innerMostFun1.param) && {
          val innerMostFun2 = RulesHelper.getCurrentOrInnerMostLambda(fun2)
          RulesHelper.containsParam(innerMostFun2.body, innerMostFun1.param)
        } =>
          fun2Levels = RulesHelper.getMapNDLevels(fun2)
          exprFound = true
        case _ =>
      }
      fun1Levels = RulesHelper.getMapNDLevels(fun1)
      exprFound && dimOrder.length == (fun1Levels + fun2Levels) && dimOrder.last.ae.evalInt < fun1Levels
    } =>
      // Collect info
      var fun2: LambdaT = null
      var input2: Expr = null
      val innerMostFun1 = RulesHelper.getCurrentOrInnerMostLambda(fun1)
      innerMostFun1.body match {
        case MapOrderedStream(f2: LambdaT, i2, _) if !RulesHelper.containsParam(i2, innerMostFun1.param) && {
          val innerMostFun2 = RulesHelper.getCurrentOrInnerMostLambda(f2)
          RulesHelper.containsParam(innerMostFun2.body, innerMostFun1.param)
        } =>
          fun2 = f2
          input2 = i2
        case _ =>
      }
      val fun1Levels = RulesHelper.getMapNDLevels(fun1)
      val fun2Levels = RulesHelper.getMapNDLevels(fun2)
      val innerMostFun2 = RulesHelper.getCurrentOrInnerMostLambda(fun2)

      // Calculate transpositions for each input after decomposition
      val input1DimOrder = RulesHelper.getDimOrderLargerThanEqual(dimOrder, fun2Levels)
      val input2DimOrder = RulesHelper.getDimOrderSmallerThan(dimOrder, fun2Levels)
      // Calculate output transposition if we apply above input transpositions
      val input1DimOrderInverted = RulesHelper.invertDimOrder(input1DimOrder)
      val input2DimOrderInverted = RulesHelper.invertDimOrder(input2DimOrder)
      val outputDimOrderFromInputs = input2DimOrderInverted ++ input1DimOrderInverted.map(e => ArithType(e.ae.evalInt + fun2Levels))
      // Calculate output transposition if we swap input1 and input 2
      val swapDimOrder = RulesHelper.generateCountSeq(fun2Levels, fun1Levels) ++ RulesHelper.generateCountSeq(0, fun2Levels)
      val swapDimOrderInverted = RulesHelper.invertDimOrder(swapDimOrder)
      // Combine output transpositions
      val newDimOrder = dimOrder.map(e => outputDimOrderFromInputs(e.ae.evalInt)).map(e => swapDimOrderInverted(e.ae.evalInt))

      // rebuild expression
      val newInput1 = TypeChecker.check(TransposeNDOrderedStream(input1, input1DimOrder))
      val newInput2 = TypeChecker.check(TransposeNDOrderedStream(input2, input2DimOrder))
      TypeChecker.check(TransposeNDOrderedStream(
        MapOrderedStream(fun2Levels, ArchLambda(innerMostFun2.param,
          MapOrderedStream(fun1Levels, ArchLambda(innerMostFun1.param, innerMostFun2.body), newInput1),
        ), newInput2), newDimOrder
      ))
  })
}