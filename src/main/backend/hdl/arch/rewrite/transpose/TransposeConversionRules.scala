package backend.hdl.arch.rewrite.transpose

import core._
import core.rewrite.{Rule, RulesT}
import backend.hdl._
import backend.hdl.arch._
import backend.hdl.arch.rewrite.RulesHelper
import lift.arithmetic.{ArithExpr, Cst, IntDiv, Mod, Prod, Sum}

import scala.annotation.tailrec

object TransposeConversionRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      cleanTranspose,
      mergeTransposes,
      replacePermuteVecWithTransposeStm,
    )

  def cleanTranspose: Rule = Rule("cleanTranspose", {
    case TransposeNDOrderedStream(input, dimOrder, _) if RulesHelper.checkDimOrder(dimOrder) =>
      input
  })

  def mergeTransposes: Rule = Rule("mergeTransposes", {
    case TransposeNDOrderedStream(TransposeNDOrderedStream(input, dimOrder1, _), dimOrder2, _) if
      dimOrder1.length == dimOrder2.length =>
      val newDimOrder = RulesHelper.mergeDimOrders(dimOrder2, dimOrder1)
      TransposeNDOrderedStream(input, newDimOrder)

    case TransposeNDOrderedStream(JoinOrderedStream(TransposeNDOrderedStream(input, dimOrder1, t: OrderedStreamTypeT), _), dimOrder2, _)
    if t.len.ae.evalInt == 1 && dimOrder1.length == dimOrder2.length + 1 =>
      val newDimOrder = RulesHelper.mergeDimOrders(dimOrder2 :+ ArithType(dimOrder2.length), dimOrder1)
      JoinOrderedStream(TransposeNDOrderedStream(input, newDimOrder))
  })

  def convertTranspose2DIntoPermuteVec: Rule = Rule("convertTranspose2DIntoPermuteVec", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(JoinVector(OrderedStreamToVector(
    MapOrderedStream(ArchLambda(p2, body, _), input2, _), _), _), _), _), input1, _), dimOrder, t: OrderedStreamTypeT) if dimOrder.length == 2 &&
      dimOrder.head.ae.evalInt == 1 && dimOrder.last.ae.evalInt == 0 &&
      t.len.ae.evalInt % t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt== 0 &&
      t.len.ae.evalInt > t.et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt =>
      JoinOrderedStream(
        MapOrderedStream(
          ArchLambda(
            p2,
            TransposeNDOrderedStream(MapOrderedStream(
              ArchLambda(
                p1,
                VectorToOrderedStream(body)
              ),
              input1
            ), Seq(1, 0))
          ), input2
        )
      )

    case TransposeNDOrderedStream(input, d @ Seq(d0, d1), _)
      if d0.ae.evalInt == 1 && d1.ae.evalInt == 0 &&
        !hasEquivalentReshape(input.t.asInstanceOf[OrderedStreamTypeT].dimensions, d) =>
      val inputVec = TypeChecker.check(OrderedStreamToVector(MapOrderedStream(OrderedStreamToVector.asFunction(), input)))
      val transposedInput = Transpose(inputVec)
      MapOrderedStream(VectorToOrderedStream.asFunction(), VectorToOrderedStream(transposedInput))

    case TransposeNDOrderedStream(input, d @ Seq(d0, d1, d2), _)
      if d0.ae.evalInt == 0 && d1.ae.evalInt == 2 && d2.ae.evalInt == 1 &&
        !hasEquivalentReshape(input.t.asInstanceOf[OrderedStreamTypeT].dimensions, d) =>
      val inputVec = TypeChecker.check(OrderedStreamToVector(MapOrderedStream(OrderedStreamToVector.asFunction(), MapOrderedStream(2, OrderedStreamToVector.asFunction(), input))))
      val transposedInput = Transpose(inputVec)
      MapOrderedStream(2, VectorToOrderedStream.asFunction(), MapOrderedStream(VectorToOrderedStream.asFunction(), VectorToOrderedStream(transposedInput)))

    case TransposeNDOrderedStream(input, d @ Seq(d0, d1, d2), _)
      if d0.ae.evalInt == 1 && d1.ae.evalInt == 2 && d2.ae.evalInt == 0 &&
        !hasEquivalentReshape(input.t.asInstanceOf[OrderedStreamTypeT].dimensions, d) =>
      val chunkSize = input.t.asInstanceOf[OrderedStreamTypeT].dimensions(1).ae.evalInt
      val inputVec = TypeChecker.check(OrderedStreamToVector(MapOrderedStream(OrderedStreamToVector.asFunction(),  JoinOrderedStream(input))))
      val transposedInput = Transpose(inputVec)
      MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), MapOrderedStream(VectorToOrderedStream.asFunction(), VectorToOrderedStream(transposedInput)))
  })

  def decomposeCounterND(start: ArithTypeT, loop: ArithTypeT, increments: Seq[ArithTypeT], dimensions: Seq[ArithTypeT],
                         bitWidth: ArithTypeT, fuseMarker: Boolean = false): Expr = {
    def mkCounter(start: ArithTypeT, incr: ArithTypeT, dim: ArithTypeT, valueType: Option[Type]): Expr = {
      // increment of zero means repetition.
      // also note that CounterInteger disallows 0 increments.
      if (incr.ae.evalInt == 0)
        CounterInteger(start, 1, loop, Seq(dim), Seq(1), valueType)
      else
        CounterInteger(start, incr, loop, Seq(dim), Seq(0), valueType)
    }

    @tailrec
    def go(prev: Expr, level: Int, increments: Seq[ArithTypeT], dimensions: Seq[ArithTypeT]): Expr = (increments, dimensions) match {
      case (Seq(), Seq()) =>
        prev

      case (x +: xs, y +: ys) =>
        val curr = mkCounter(0, x, y, None)
        val next = MapOrderedStream(
          {
            val param1 = ParamDef(curr.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              param1,
              MapOrderedStream(
                level,
                {
                  val param2 = ParamDef(IntType(bitWidth))
                  ArchLambda(
                    param2,
                    ResizeInteger(AddInt2(ParamUse(param1), ParamUse(param2)), bitWidth)
                  )
                }, prev
              )
            )
          }, curr
        )
        go(if (fuseMarker) Marker(next, TextType("FusionGuard")) else next, level + 1, xs, ys)
    }

    val (x +: xs, y +: ys) = (increments, dimensions)
    go(mkCounter(start, x, y, Some(IntType(bitWidth))), 1, xs, ys)
  }

  def mergeIntoCounter: Rule = Rule("mergeIntoCounter", {
    case TransposeNDOrderedStream(CounterInteger(start, increment, loop, dims, reps, _), dimOrder, t: OrderedStreamTypeT)
    if hasEquivalentReshape(dims, dimOrder) =>
      // The usual JoinAll + SplitAll might not be able to fully simplify if
      // the counter has a mixture of repeating and non-repeating dimensions.
      // hence, we manually reconstruct the counter here.
      @tailrec
      def go(acc: Seq[ArithTypeT], newDims: Seq[ArithTypeT], oldDims: Seq[ArithTypeT], oldReps: Seq[ArithTypeT]): Seq[ArithTypeT] = (newDims, oldDims, oldReps) match {
        case (Seq(), Seq(), Seq()) =>
          // since Seq's are (by default) Lists, it is better to prepend +
          // reverse (linear cost) instead of append (quadratic cost)
          acc.reverse

        case (ArithType(Cst(1)) +: newDims, oldDims, oldReps) =>
          // here we assume the length 1 dimensions never repeat. it might
          // affect rewrites, but functionally, it does not matter,
          go(ArithType(0) +: acc, newDims, oldDims, oldReps)

        case (newDims, ArithType(Cst(1)) +: oldDims, _ +: oldReps) =>
          // ignore the length 1 dimensions since they are reconstructed when
          // we see it on the newDims side.
          go(acc, newDims, oldDims, oldReps)

        case (d1 +: newDims, d2 +: oldDims, rep +: oldReps) =>
          // note that oldDims and oldReps must have the same length by
          // construction, so this is the final case we need to handle.
          assert(d1 == d2, "Not a reshape")
          go(rep +: acc, newDims, oldDims, oldReps)
      }

      val leafType = t.leafType
      val newDims = t.dimensions
      val newReps = go(Seq(), newDims, dims, reps)
      CounterInteger(start, increment, loop, newDims, newReps, Some(leafType))

    case TransposeNDOrderedStream(CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), dimOrder, _)  =>
      val accumInc = RulesHelper.expandAsNDIncr(increment, dimensions, repetitions)
      val reAccumInc = RulesHelper.mergeDimOrders(dimOrder, accumInc)
      val reDim =  RulesHelper.mergeDimOrders(dimOrder, dimensions)
      val bitWidth = t.leafType.bitWidth
      decomposeCounterND(start, loop, reAccumInc, reDim, bitWidth)
  })

  def mergeIntoCounterV2: Rule = Rule("mergeIntoCounterV2", {
    case TransposeNDOrderedStream(CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), dimOrder, _)  =>
      val accumInc = RulesHelper.expandAsNDIncr(increment, dimensions, repetitions)
      val reAccumInc = RulesHelper.mergeDimOrders(dimOrder, accumInc)
      val reDim =  RulesHelper.mergeDimOrders(dimOrder, dimensions)
      val bitWidth = t.leafType.bitWidth
      CounterIntegerND(start, reAccumInc, reDim, Some(IntType(bitWidth)))
  })

  def mergeIntoMap: Rule = Rule("mergeIntoMap", {
    case TransposeNDOrderedStream(maps @ MapOrderedStream(_, _, _), order, _) if (
      // the check to make sure maps is a MapOrderedStream is for visualization
      canExchangeMap(maps, order)
    ) =>
      @tailrec
      def loop(expr: Expr, order: Seq[ArithTypeT], xchg: Seq[(Int, ParamDef, Expr)], min: Option[Int]): Expr = (order, expr) match {
        case (order :+ x, MapOrderedStream(ArchLambda(p1, expr, _), counter, _)) =>
          val index = x.ae.evalInt
          loop(expr, order, (index, p1, counter) +: xchg, Some(min.getOrElse(index).min(index)))
        case (Seq(), expr) =>
          xchg.map(p => xchg(p._1 - min.get)).foldLeft(expr)({ case (body, (_, param, stream)) =>
            MapOrderedStream(ArchLambda(param, body), stream)
          })
      }
      loop(maps, dropInnerIdentityOrder(order), Seq(), None)
  })

  def dropInnerIdentityOrder(order: Seq[ArithTypeT]): Seq[ArithTypeT] =
    order.zipWithIndex.dropWhile(p => p._1.ae.evalInt == p._2).map(_._1)

  def canExchangeMap(expr: Expr, order: Seq[ArithTypeT]): Boolean = {
    @tailrec
    def loop(expr: Expr, order: Seq[_], ivs: Set[Long]): Boolean = (order, expr) match {
      case (Seq(), _) => true
      case (order :+ _, MapOrderedStream(ArchLambda(p1, expr, _), counter, _)) if {
        // disallow map exchange if the counter refers to any of the induction
        // variables introduced in higher levels (which is sufficient).
        var usesIV = false
        counter.visit({
          case ParamUse(id) => usesIV |= ivs.contains(id.id)
          case _ =>
        })
        !usesIV
      } =>
        loop(expr, order, ivs + p1.id)

      case _ => false
    }

    // identity transpose should be dealt with by other rewrites, so make sure
    // we are transposing something before checking for the nested maps.
    val leftovers = dropInnerIdentityOrder(order)
    leftovers.nonEmpty && loop(expr, leftovers, Set.empty)
  }

  def replacePermuteVecWithTransposeStm = Rule("replacePermuteVecWithTransposeStm", {
    case SplitOrderedStream(VectorToOrderedStream(PermuteVector(
    OrderedStreamToVector(JoinOrderedStream(input, _), _), arithFun, _), _), chunkSize, _) if {
      var exprFound = false
      arithFun.b.ae match {
        case Sum(IntDiv(v1, c1)::Prod(Cst(_)::Mod(v2, c2)::Nil)::Nil) if v1 == v2 && c1 == c2 =>
          exprFound = true
        case _=>
      }
      exprFound
    } =>
      TransposeNDOrderedStream(input, Seq(1,0))
  })

  def mergeIntoIndexStm: Rule = Rule("mergeIntoIndexStm", {
    case IndexUnorderedStream(OrderedStreamToUnorderedStream(joinTransposeInput, _), t) if {
      var exprFound = false
      val innerMostInput = RulesHelper.getCurrentOrJoinNDInput(joinTransposeInput)
      innerMostInput match {
        case TransposeNDOrderedStream(input, dimOrder, transposedType: OrderedStreamTypeT) if
          dimOrder.length - 1 == RulesHelper.getJoinNDLevels(joinTransposeInput) =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      // Get transposition's info
      var transposedDims: Seq[ArithTypeT] = Seq()
      var dimOrder: Seq[ArithTypeT] = Seq()
      var input: Expr = joinTransposeInput

      val innerMostInput = RulesHelper.getCurrentOrJoinNDInput(joinTransposeInput)
      innerMostInput match {
        case TransposeNDOrderedStream(i, order, tT: OrderedStreamTypeT) if
          order.length - 1 == RulesHelper.getJoinNDLevels(joinTransposeInput) =>
          transposedDims = tT.dimensions
          dimOrder = order
          input = i
        case _ =>
      }

      if (hasEquivalentReshape(input.t.asInstanceOf[OrderedStreamTypeT].dimensions, dimOrder))
        // we were just reshaping the input, hence the relative ordering of the elements remains the same
        // in other words, the transpose wasn't needed in the first place.
        IndexUnorderedStream(OrderedStreamToUnorderedStream(JoinStreamAll(input)))
      else {
        // transpose the address counter instead of the input
        val addrType = t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[NamedTupleTypeT].namedTypes.last._2
        val inverseDimOrder = RulesHelper.invertDimOrder(dimOrder)
        val addrCounter = TransposeNDOrderedStream(CounterInteger(0, 1, transposedDims, Some(addrType)), inverseDimOrder)

        Zip2OrderedStream(Tuple2(JoinStreamAll(input), JoinStreamAll(addrCounter)))
      }
  })

  def limitedConvertTransposeNDIntoPermute: Rule = Rule("limitedConvertTransposeNDIntoPermute", {
    case TransposeNDOrderedStream(input, dimOrder, _) if dimOrder.length == 4 && dimOrder(1).ae.evalInt == 2 && dimOrder(2).ae.evalInt == 1 && dimOrder(3).ae.evalInt == 3 && dimOrder(0).ae.evalInt == 0 =>
      val inputAsVec =  TypeChecker.check(MapOrderedStream(OrderedStreamToVector.asFunction(), MapOrderedStream(2, OrderedStreamToVector.asFunction(), MapOrderedStream(3, OrderedStreamToVector.asFunction(), input))))
      val transposed = MapOrderedStream({
        val param = ParamDef(inputAsVec.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(
          param,
          Transpose(ParamUse(param))
        )
      }, inputAsVec)
      MapOrderedStream(3, VectorToOrderedStream.asFunction(), MapOrderedStream(2, VectorToOrderedStream.asFunction(), MapOrderedStream(VectorToOrderedStream.asFunction(), transposed)))

    case TransposeNDOrderedStream(input, dimOrder, t) if dimOrder.length == 3 && dimOrder(1).ae.evalInt == 0 && dimOrder(0).ae.evalInt == 1 =>
      val inputAsVec =  TypeChecker.check(MapOrderedStream(OrderedStreamToVector.asFunction(), MapOrderedStream(2, OrderedStreamToVector.asFunction(), input)))
      val transposed = MapOrderedStream({
        val param = ParamDef(inputAsVec.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(
          param,
          Transpose(ParamUse(param))
        )
      }, inputAsVec)
      MapOrderedStream(2, VectorToOrderedStream.asFunction(), MapOrderedStream(VectorToOrderedStream.asFunction(), transposed))
  })

  def hasEquivalentReshape(dims: Seq[ArithTypeT], dimOrder: Seq[ArithTypeT]): Boolean =
    dimOrder
      .map(_.ae.evalInt)
      .filter(dims(_).ae.evalInt != 1)      // allow transpose to only move dimensions of length 1
      .sliding(2)
      .forall {
        case Seq() | Seq(_) => true         // less than two dimensions have length of not 1, clearly a reshape
        case Seq(x, y) => x < y             // all other dimensions must stay relative to each other
      }

  def expandPseudoTransposeND: Rule = Rule("expandPseudoTransposeND", {
    case TransposeNDOrderedStream(input, dimOrder, t: OrderedStreamTypeT) if (
      hasEquivalentReshape(input.t.asInstanceOf[OrderedStreamTypeT].dimensions, dimOrder)
    ) =>
      t.dimensions.dropRight(1).foldLeft(JoinStreamAll(input))((e, s) => SplitOrderedStream(e, s))

    // Convert TransposeND into PermuteVec because there is no hardware template for it.
    case TransposeNDOrderedStream(input, dimOrder, t: OrderedStreamTypeT) =>
      val inputDims = input.t.asInstanceOf[OrderedStreamTypeT].dimensions
      val outputDims = t.dimensions
      val permf = TransposeND.mkPermuteFunction(inputDims, dimOrder)

      val vecJoinAll = TypeChecker.check(OrderedStreamToVector(JoinStreamAll(input)))
      val permuteAll = TypeChecker.check(VectorToOrderedStream(PermuteVector(vecJoinAll, permf)))
      SplitStreamAll(permuteAll, outputDims.dropRight(1).reverse)
  })
}
