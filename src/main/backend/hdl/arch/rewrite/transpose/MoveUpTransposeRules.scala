package backend.hdl.arch.rewrite.transpose

import backend.hdl.arch.mem.{HostRam, MemoryAllocationExpr, Write}
import backend.hdl.{BasicDataTypeT, IntType, LogicType, LogicTypeT, OrderedStreamTypeT, UnorderedStreamTypeT}
import backend.hdl.arch.rewrite.{MoveUpDropStreamRules, MoveUpJoinStreamRules, MoveUpOrderedToUnorderedStreamRules, RemoveJoinSplitRules, RulesHelper}
import backend.hdl.arch.{ArchLambda, ConcatOrderedStream, ConcatVector, ConstantValue, CounterInteger, DropOrderedStream, IndexUnorderedStream, JoinOrderedStream, JoinVector, MapOrderedStream, OrderedStreamToUnorderedStream, OrderedStreamToVector, ReduceOrderedStream, Repeat, SplitOrderedStream, TransposeNDOrderedStream, Tuple2, VectorGenerator, VectorToOrderedStream}
import backend.hdl.specs.FPGA
import core.{ArithType, Conversion, Expr, LambdaT, Let, ParamDef, ParamUse, TextTypeT, TypeChecker}
import core.rewrite.{Rule, RulesT}
import core.util.{IRDotGraph, Log}

object MoveUpTransposeRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      realignOutputShape,
    ) ++ Seq(TransposeConversionRules.mergeIntoIndexStm)

  def mergeIntoWriteAddress: Seq[Rule] =
    Seq(
      skipMapNDSplit,
      skipMapNDFun,
      moveIntoMap,
      moveOutFromMapFun,
      skipMapNDFunPartition,
      mergeDropIntoWrite2D,
      skipLetBody,
      skipMapNDVec2Stm,
      skipMapNDJoin,
    ) ++ MoveUpJoinStreamRules.all(None) ++ MoveUpOrderedToUnorderedStreamRules.all(None) ++
      MoveUpDropStreamRules.all(None) ++
      Seq(TransposeConversionRules.mergeTransposes, TransposeConversionRules.mergeIntoCounterV2) ++
      Seq(RemoveJoinSplitRules.simplifyJoinJoinMapSplit) ++ Seq(MoveDownTransposeRules.skipMapJoinStream)

  def skipMapNDSplit: Rule = Rule("moveUpTranspose_skipMapNDSplit", {
    case MapOrderedStream(splitFun: LambdaT, TransposeNDOrderedStream(input, dimOrder, _), _) if {
      var exprFound = false
      val innermostSplitFun = RulesHelper.getCurrentOrInnerMostLambda(splitFun)
      innermostSplitFun.body match {
        case SplitOrderedStream(ParamUse(p1), chunkSize, _) if p1.id == innermostSplitFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innermostSplitFun = RulesHelper.getCurrentOrInnerMostLambda(splitFun)
      val chunkSize = innermostSplitFun.body match {
        case SplitOrderedStream(ParamUse(p1), chunkSize, _) if p1.id == innermostSplitFun.param.id =>
          chunkSize
        case _ => ArithType(0)
      }
      val splitFunLevels = RulesHelper.getMapNDLevels(splitFun)
      val axisLoc = dimOrder.length - 1 - splitFunLevels
      val selectedAxis = dimOrder(axisLoc).ae.evalInt
      val levelsBeforeTranspose = dimOrder.length - 1 - selectedAxis
      // SplitFun must be rebuilt
      val newSplitInput = MapOrderedStream(levelsBeforeTranspose, SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), input)
      val newSplitInputtc = TypeChecker.check(newSplitInput)
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, selectedAxis, axisLoc)
      TransposeNDOrderedStream(newSplitInputtc, newDimOrder)
  })

  def skipMapNDFun: Rule = Rule("moveUpTranspose_skipMapNDFun", {
    case MapOrderedStream(fun: LambdaT, TransposeNDOrderedStream(input, dimOrder, _), _) if
      !RulesHelper.isComputation(fun) &&
        RulesHelper.checkDimOrder(dimOrder.dropRight(RulesHelper.getMapNDLevels(fun))) &&
        !(RulesHelper.getCurrentOrInnerMostLambda(fun).body.t.isInstanceOf[UnorderedStreamTypeT] &&
          !RulesHelper.getCurrentOrInnerMostLambda(fun).body.t.isInstanceOf[OrderedStreamTypeT])&& {
        var exprFound = false
        input match {
          case CounterInteger(_, _, _, _, _, _) =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } && {
        var exprFound = false
        fun.visit {
          case OrderedStreamToUnorderedStream(_, _) =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      val level = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val innerMostFunStreamLevel = innerMostFun.body.t match {
        case t: OrderedStreamTypeT => t.dimensions.length
        case _ => 0
      }
      val padDimOrder = if(innerMostFunStreamLevel > 0)
        RulesHelper.generateCountSeq(0, innerMostFunStreamLevel)
      else
        Seq()
      val offset = dimOrder.length - level
      val newDimOrder = padDimOrder ++ dimOrder.drop(offset).map(x => ArithType(x.ae.evalInt - offset + innerMostFunStreamLevel))
      TypeChecker.check(TransposeNDOrderedStream(TypeChecker.check(MapOrderedStream(level, innerMostFun, input)), newDimOrder))
  })

  def skipMapNDFunPartition: Rule = Rule("moveUpTranspose_skipMapNDFunPartition", {
    case MapOrderedStream(fun: LambdaT, TransposeNDOrderedStream(input, dimOrder, t:OrderedStreamTypeT), _) if
      !RulesHelper.isComputation(fun) &&
        RulesHelper.getMapNDLevels(fun) > 2 &&
        dimOrder.head.ae.evalInt == 1 && dimOrder(1).ae.evalInt == 0 &&
        !RulesHelper.getCurrentOrInnerMostLambda(fun).body.t.isInstanceOf[OrderedStreamTypeT] &&
        {
          var exprFound = false
          input match{
            case CounterInteger(_, _, _, _, _,_) =>
              exprFound = true
            case _ =>
          }
          !exprFound
        }=>
      val level = RulesHelper.getMapNDLevels(fun)
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder.drop(2).map(x => ArithType(x.ae.evalInt - 2)), 0, 0)
      val innerDimOrder = Seq(dimOrder(0), dimOrder(1))
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      TransposeNDOrderedStream(MapOrderedStream(level, innerMostFun, MapOrderedStream(dimOrder.length - 2, TransposeNDOrderedStream.asFunction(Seq(None), innerDimOrder), input)), newDimOrder)

    case MapOrderedStream(fun: LambdaT, TransposeNDOrderedStream(input, dimOrder, t:OrderedStreamTypeT), _) if
      !RulesHelper.isComputation(fun) &&
        RulesHelper.getMapNDLevels(fun) > 2 &&
        dimOrder.head.ae.evalInt == 2 && dimOrder(1).ae.evalInt == 3 && dimOrder(2).ae.evalInt == 0 && dimOrder(3).ae.evalInt == 1 && dimOrder.length == 4 &&
        !RulesHelper.getCurrentOrInnerMostLambda(fun).body.t.isInstanceOf[OrderedStreamTypeT] &&
        {
          var exprFound = false
          input match{
            case CounterInteger(_, _, _, _, _,_) =>
              exprFound = true
            case _ =>
          }
          !exprFound
        }=>
      val level = RulesHelper.getMapNDLevels(fun)
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder.drop(2).map(x => ArithType(x.ae.evalInt - 2)), 0, 0)
      val innerDimOrder = Seq(dimOrder(0), dimOrder(1))
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      TransposeNDOrderedStream(MapOrderedStream(level, innerMostFun, MapOrderedStream(1, TransposeNDOrderedStream.asFunction(Seq(None), Seq(2, 0, 1)), input)), Seq(2, 0, 1))
  })

  def moveIntoMap: Rule = Rule("moveUpTranspose_moveIntoMap", {
    case MapOrderedStream(fun: LambdaT, TransposeNDOrderedStream(input, dimOrder, _), _) if dimOrder.last.ae.evalInt == dimOrder.length - 1 && {
      var exprFound = false
      fun.body match{
        case JoinOrderedStream(ParamUse(p), _) if p.id == fun.param.id =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } &&  {
      var exprFound = false
      val joinInnerMost = RulesHelper.getCurrentOrInnerMostLambda(fun)
      joinInnerMost.body match {
        case JoinOrderedStream(ParamUse(p), _) if p.id == joinInnerMost.param.id =>
          exprFound = true
        case OrderedStreamToUnorderedStream(ParamUse(p), _) if p.id == joinInnerMost.param.id =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } && {
      var exprFound = false
      input match{
        case CounterInteger(_, _, _, _, _,_) =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            fun.body.visitAndRebuild{
              case ParamUse(usedParam) if usedParam.id == fun.param.id =>
                TransposeNDOrderedStream(ParamUse(param), dimOrder.dropRight(1))
              case e => e
            }.asInstanceOf[Expr]
          )
        }, input
      )
  })

  def moveOutFromMapFun: Rule = Rule("moveUpTranspose_moveOutFromMapFun", {
    case MapOrderedStream(ArchLambda(p1, TransposeNDOrderedStream(body, dimOrder, _), _), input, _) if RulesHelper.containsParam(body, p1) =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length, dimOrder.length)
      TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, body), input), newDimOrder)
  })

  private def realignOutputShape = Rule("moveUpTranspose_realignOutputShape", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, ConcatVector(Tuple2(JoinVector(OrderedStreamToVector(ParamUse(p3), _), _), VectorGenerator(ConstantValue(v1, t1: LogicTypeT), padVecSize, _), _), _), _),
    SplitOrderedStream(ConcatOrderedStream(JoinOrderedStream(ParamUse(p4), origJoinT: OrderedStreamTypeT), Repeat(VectorGenerator(ConstantValue(v2, t2: LogicTypeT), fullVecSize, _), times, _), dim, _), chunkSize, _), _), _),
    TransposeNDOrderedStream(input, dimOrder, _), t) if p1.id == p4.id && p2.id == p3.id &&
      dimOrder.last.ae.evalInt == dimOrder.length - 2 &&
      dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 1 &&
      RulesHelper.checkDimOrder(dimOrder.dropRight(2)) &&
      origJoinT.len.ae.evalInt % input.t.asInstanceOf[OrderedStreamTypeT].dimensions.head.ae.evalInt == 0 =>

      Log.warn(this, "Rewrite rule realignOutputShape changes the layout of data! Be careful using this rule.")
      // Calculate Padding information
      val cacheLineBitWidth = FPGA.current.cachelineType.bitWidth.ae.evalInt
      val inputLeafSize = input.t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth.ae.evalInt
      val origInnerDim = origJoinT.len.ae.evalInt
      val newElementsPerCacheLine = cacheLineBitWidth / inputLeafSize
      val paddingBitWidth = cacheLineBitWidth % inputLeafSize
      val lastRowElements = origInnerDim % newElementsPerCacheLine
      val lastRowPaddingElements = newElementsPerCacheLine - lastRowElements

      // Reshape input
      val newInput = TypeChecker.check(MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(origInnerDim)), MapOrderedStream(JoinOrderedStream.asFunction(), input)))

      // Add Padding
      val paddingElements = Repeat(VectorGenerator(ConstantValue(0, Some(LogicType())), inputLeafSize), lastRowPaddingElements)
      val paddingBits = VectorGenerator(ConstantValue(0, Some(LogicType())), paddingBitWidth)
      val paddedInput = MapOrderedStream(
        {
          val param1 = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param1,
            MapOrderedStream(
              {
                val param2 = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
                ArchLambda(
                  param2,
                  MapOrderedStream(
                    {
                      val param3 = ParamDef()
                      ArchLambda(
                        param3,
                        ConcatVector(Tuple2(JoinVector(OrderedStreamToVector(ParamUse(param3))), paddingBits))
                      )
                    }, SplitOrderedStream(ConcatOrderedStream(ParamUse(param2), paddingElements), newElementsPerCacheLine)
                  )
                )
              }, ParamUse(param1)
            )
          )
        }, newInput
      )

      // calculate output length fix
      val paddedInputtc = TypeChecker.check(paddedInput)
      val joinedOutput = JoinOrderedStream(JoinOrderedStream(paddedInputtc))
      val joinedOutputtc = TypeChecker.check(joinedOutput)
      val newOutputLen = joinedOutputtc.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt
      val origOutputLen = t.asInstanceOf[OrderedStreamTypeT].dimensions.map(_.ae.evalInt).product
      val dropElements = newOutputLen - origOutputLen
      SplitOrderedStream(DropOrderedStream(joinedOutputtc, 0, dropElements), t.asInstanceOf[OrderedStreamTypeT].dimensions.head)
  })

  private def mergeDropIntoWrite2D: Rule = Rule("moveUpTranspose_mergeDropIntoWrite2D", {
    case Let(p1, ReduceOrderedStream(ArchLambda(p2, ArchLambda(p3, ReduceOrderedStream(ArchLambda(p4,
    ArchLambda(p5, Write(ParamUse(p6), ParamUse(p7), _), _), _), ParamUse(p8), ParamUse(p9), _), _), _), ParamUse(p10),
    SplitOrderedStream(IndexUnorderedStream(OrderedStreamToUnorderedStream(JoinOrderedStream(
    MapOrderedStream(ArchLambda(p11, DropOrderedStream(ParamUse(p12), _, _, _), _), input, _), _), _), _), chunkSize, _), _), memAlloc: MemoryAllocationExpr, _)
      if p1.id == p10.id && p2.id == p9.id && p3.id == p8.id && p4.id == p7.id && p5.id == p6.id && p11.id == p12.id &&
        input.t.asInstanceOf[OrderedStreamTypeT].dimensions.map(_.ae.evalInt).product % chunkSize.ae.evalInt == 0 // check if reshaping is feasible
    =>
      // Calculate new memory allocation
      val newOuterDim = input.t.asInstanceOf[OrderedStreamTypeT].dimensions.map(_.ae.evalInt).product / chunkSize.ae.evalInt
      val newDims = Seq(ArithType(newOuterDim),  chunkSize)
      val newMemAlloc = HostRam(memAlloc.types.head.asInstanceOf[TextTypeT], memAlloc.types(1).asInstanceOf[TextTypeT], memAlloc.types(2).asInstanceOf[BasicDataTypeT], newDims)

      // Build new input
      val newInput = SplitOrderedStream(IndexUnorderedStream(TypeChecker.check(OrderedStreamToUnorderedStream(JoinOrderedStream(input)))), chunkSize)
      val newInputtc = TypeChecker.check(newInput)

      // Build write access
      val newMemAllocParam = ParamDef(newMemAlloc.t)
      val newHostWrite = Let(
        newMemAllocParam,
        ReduceOrderedStream(
          {
            val param1 = ParamDef(newInputtc.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              param1,
              ArchLambda(
                newMemAllocParam,
                ReduceOrderedStream(
                  {
                    val param2 = ParamDef(newInputtc.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
                    ArchLambda(
                      param2,
                      TypeChecker.check(ArchLambda(
                        newMemAllocParam,
                        TypeChecker.check(Write(ParamUse(newMemAllocParam), ParamUse(param2)))
                      ))
                    )
                  },
                  ParamUse(newMemAllocParam),
                  ParamUse(param1)
                )
              )
            )
          },
          ParamUse(newMemAllocParam),
          newInputtc,
        ),
        newMemAlloc
      )
      TypeChecker.check(newHostWrite)
  })

  def skipLetBody: Rule = Rule("moveUpTranspose_skipLetBody", {
    case Let(p, TransposeNDOrderedStream(input, dimOrder, _), arg, _) =>
      TransposeNDOrderedStream(Let(p, input, arg), dimOrder)
  })

  def skipMapNDVec2Stm: Rule = Rule("moveUpTranspose_skipMapNDVec2Stm", {
    case MapOrderedStream(fun: LambdaT, TransposeNDOrderedStream(input, dimOrder, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case VectorToOrderedStream(ParamUse(pd), _) if pd.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val level = RulesHelper.getMapNDLevels(fun)
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, 0, 0)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      TransposeNDOrderedStream(MapOrderedStream(level, innerMostFun, input), newDimOrder)
  })

  def skipMapNDJoin: Rule = Rule("moveUpTranspose_skipMapNDJoin", {
    case MapOrderedStream(fun: LambdaT, TransposeNDOrderedStream(input, dimOrder, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case JoinOrderedStream(ParamUse(pd), _) if pd.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } && dimOrder.head.ae.evalInt == 0 && dimOrder(1).ae.evalInt == 1 =>
      val level = RulesHelper.getMapNDLevels(fun)
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, 0)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      TransposeNDOrderedStream(MapOrderedStream(level, innerMostFun, input), newDimOrder)
  })
}