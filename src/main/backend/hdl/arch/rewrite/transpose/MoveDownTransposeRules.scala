package backend.hdl.arch.rewrite.transpose

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.rewrite.{CleanupRules, MergeIntoCounterRules, RulesHelper}
import backend.hdl.arch.{Alternate, ArchLambda, ConcatOrderedStream, CounterInteger, FoldOrderedStream, JoinOrderedStream, JoinVector, MapOrderedStream, OrderedStreamToVector, PermuteVector, Repeat, SplitOrderedStream, SplitVector, TransposeNDOrderedStream, VectorToOrderedStream}
import core.{ArithType, ArithTypeT, Expr, FunctionCall, LambdaT, Let, Marker, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}
import core.util.IRDotGraph

object MoveDownTransposeRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      skipMarker,
    )

  def mergeIntoReadAddress: Seq[Rule] = Seq(
    moveIntoMapNonCompute,
    skipMapJoinStream,
    mapFission,
    skipMapParam2D,
    moveIntoLet,
    skipMapWithCounter,
    swapABCounter,
    mapFission2,
    skipVecToStm2D,
    skipVecToStmND, // TODO this 2 need to be refactored
    skipJoinStmND,
    skipStmToVecND,
    skipFunND,
    skipConcat,
    skipMapSplitStream,
    moveIntoAlternate
  ) ++ Seq(CleanupRules.mapFissionMapStmFun) ++
    Seq(TransposeConversionRules.mergeIntoCounter, TransposeConversionRules.mergeIntoMap, MergeIntoCounterRules.mergeTransposeND) ++
    DecomposeTranspositionRules.get()

  def moveIntoMapNonCompute: Rule = Rule("moveDownTranspose_moveIntoMapNonCompute", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, body ,_), input, _), dimOrder, _) if
      !RulesHelper.isComputation(body) && dimOrder.last.ae.evalInt == dimOrder.length - 1 && {
        var exprFound = false
        body.visit{
          case FunctionCall(ParamUse(_), _, _) => exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, dimOrder.length - 1)
      MapOrderedStream(
        ArchLambda(p1, TransposeNDOrderedStream(body, newDimOrder)), input
      )
  })

  def moveIntoMapNonComputeMul: Rule = Rule("moveDownTranspose_moveIntoMapNonComputeMul", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, body ,_), input, _), dimOrder, _) if
      !RulesHelper.isMulComputation(body) && dimOrder.last.ae.evalInt == dimOrder.length - 1 && {
        var exprFound = false
        body.visit{
          case FunctionCall(ParamUse(_), _, _) => exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, dimOrder.length - 1)
      MapOrderedStream(
        ArchLambda(p1, TransposeNDOrderedStream(body, newDimOrder)), input
      )
  })

  def moveIntoMapNonCompute2: Rule = Rule("moveDownTranspose_moveIntoMapNonCompute2", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun: LambdaT, input2, _) ,_), input1, _), dimOrder, _) if
      RulesHelper.containsParam(fun.body, p1) &&
        !RulesHelper.isComputation(input2) && dimOrder.last.ae.evalInt == dimOrder.length - 1 =>
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, dimOrder.length - 1)
      MapOrderedStream(
        ArchLambda(p1, TransposeNDOrderedStream(MapOrderedStream(fun: LambdaT, input2), newDimOrder)), input1
      )
  })
  def skipMapJoinStream: Rule = Rule("moveDownTranspose_skipMapJoinStream", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _) ,_), input, _), dimOrder, _) if
      p1.id == p2.id && dimOrder.last.ae.evalInt == dimOrder.length - 2 =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length - 2, dimOrder.length-1)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      JoinOrderedStream(transposedInput)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, JoinOrderedStream(ParamUse(p3), _) ,_), ParamUse(p4), _), _), input, _), dimOrder, _) if
      p1.id == p4.id && p2.id == p3.id && dimOrder.last.ae.evalInt == dimOrder.length - 2 && RulesHelper.checkDimOrder(dimOrder.drop(1)) =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length - 2, dimOrder.length-1)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      MapOrderedStream(JoinOrderedStream.asFunction(), transposedInput)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, JoinOrderedStream(ParamUse(p3), _) ,_), ParamUse(p4), _), _), input, _), dimOrder, _) if
      p1.id == p4.id && p2.id == p3.id && dimOrder.last.ae.evalInt == dimOrder.length - 3 =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length - 3, dimOrder.length-1)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      JoinOrderedStream(transposedInput)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3, JoinOrderedStream(ParamUse(p4), _) ,_), ParamUse(p5), _), _), ParamUse(p6), _), _), input, _), dimOrder, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && dimOrder.last.ae.evalInt == dimOrder.length - 2 && RulesHelper.checkDimOrder(dimOrder.drop(1)) =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length - 2, dimOrder.length-1)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      MapOrderedStream(2, JoinOrderedStream.asFunction(), transposedInput)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, JoinOrderedStream(ParamUse(p3), _) ,_), ParamUse(p4), _), _), input, _), dimOrder, _) if
      p1.id == p4.id && p2.id == p3.id && dimOrder.last.ae.evalInt == dimOrder.length - 1 &&
        dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 3 &&
        dimOrder(dimOrder.length - 3).ae.evalInt == dimOrder.length - 2 &&
        RulesHelper.checkDimOrder(dimOrder.dropRight(3)) =>
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length - 3, dimOrder.length - 2)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      MapOrderedStream(JoinOrderedStream.asFunction(), transposedInput)
  })

  def mapFission: Rule = Rule("moveDownTranspose_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      body.visit{
        case TransposeNDOrderedStream(ParamUse(p2), dimOrder, _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val dimOrder = {
        var exprFound = Seq().asInstanceOf[Seq[ArithTypeT]]
        body.visit{
          case TransposeNDOrderedStream(ParamUse(p2), dimOrder, _) if p1.id == p2.id =>
            exprFound = dimOrder
          case _ =>
        }
        exprFound
      }
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, dimOrder.length, dimOrder.length)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      val transposedInputtc = TypeChecker.checkIfUnknown(transposedInput, transposedInput.t)
      MapOrderedStream(
        {
          val param = ParamDef(transposedInputtc.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            body.visitAndRebuild{
              case TransposeNDOrderedStream(ParamUse(p2), dimOrder, _) if p1.id == p2.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, transposedInputtc
      )
  })

  def skipMapParam2D: Rule = Rule("moveDownTranspose_skipMapParam2D", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun, ParamUse(p2), _) ,_), input, _), dimOrder, _) if
      p1.id == p2.id && dimOrder.last.ae.evalInt == dimOrder.length - 2 && dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 1  &&
        RulesHelper.checkDimOrder(dimOrder.dropRight(2)) && {
        var exprFound = false
        fun.visit{
          case FoldOrderedStream(_, _, _) => exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      val inputDims = input.t.asInstanceOf[OrderedStreamTypeT].dimensions
      val newDimOrder = if(inputDims.length <= dimOrder.length)
        dimOrder.drop(dimOrder.length - inputDims.length).map(_.ae.evalInt - (dimOrder.length - inputDims.length)).map(ArithType(_))
      else
        RulesHelper.generateCountSeq(0, inputDims.length - dimOrder.length) ++ dimOrder.map(_.ae.evalInt + (inputDims.length - dimOrder.length)).map(ArithType(_))
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      val transposedInputtc = TypeChecker.check(transposedInput)
      MapOrderedStream(2, fun, transposedInputtc)
  })

  private def skipMarker: Rule = Rule("moveDownTranspose_skipMarker", {
    case TransposeNDOrderedStream(Marker(input, text), dimOrder, _) if !text.s.contains("transpose_guard")=>
      Marker(TransposeNDOrderedStream(input, dimOrder), text)
  })

  def moveIntoLet: Rule = Rule("moveDownTranspose_moveIntoLet", {
    case TransposeNDOrderedStream(Let(arg, body, param, _), dimOrder, _)  =>
      Let(arg, TransposeNDOrderedStream(body, dimOrder), param)
  })

  def swapABMap: Rule = Rule("moveDownTranspose_swapABMap", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, body, _), input2, _) ,_), input1, _), dimOrder, _) if
      RulesHelper.containsParam(body, p1) && RulesHelper.containsParam(body, p2) && dimOrder.last.ae.evalInt == dimOrder.length - 2 && dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 1  &&
        RulesHelper.checkDimOrder(dimOrder.dropRight(2)) && dimOrder.length == 2=>
      MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p1, body), input1)), input2)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, body, _), inner2, _), _), input2, _) ,_), input1, _), dimOrder, _) if
      RulesHelper.containsParam(body, p1) && RulesHelper.containsParam(body, p4) && RulesHelper.containsParam(inner2, p3) && dimOrder.length == 3 &&
        dimOrder(2).ae.evalInt == 1 && dimOrder(1).ae.evalInt == 0 && dimOrder.head.ae.evalInt == 2  =>
      MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, MapOrderedStream(ArchLambda(p1, body), input1)), inner2)), input2)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, body, _), inner2, _), _), input2, _), _), inner1, _) ,_), input1, _), dimOrder, _) if
      RulesHelper.containsParam(body, p2) && RulesHelper.containsParam(body, p4) && RulesHelper.containsParam(inner2, p3) && RulesHelper.containsParam(inner1, p1) && dimOrder.length == 4 &&
        dimOrder.last.ae.evalInt == 3 && dimOrder(2).ae.evalInt == 1 && dimOrder(1).ae.evalInt == 0 && dimOrder.head.ae.evalInt == 2  =>
      MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, MapOrderedStream(ArchLambda(p2, body), inner1)), inner2)), input2)), input1)

    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, body, _), inner1, _), _), inner2, _), _), input2, _) ,_), input1, _), dimOrder, _) if
      RulesHelper.containsParam(body, p3) && RulesHelper.containsParam(body, p4) && RulesHelper.containsParam(inner2, p2) && RulesHelper.containsParam(inner1, p1) && dimOrder.length == 4 &&
        dimOrder.last.ae.evalInt == 2 && dimOrder(2).ae.evalInt == 3 && dimOrder(1).ae.evalInt == 0 && dimOrder.head.ae.evalInt == 1  =>
      MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p4, MapOrderedStream(ArchLambda(p3, body), inner2)), inner1)), input1)), input2)
  })

  def swapABCounter: Rule = Rule("moveDownTranspose_swapABCounter", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, body, _), input2, _) ,_), input1, _), dimOrder, _) if
      RulesHelper.containsParam(body, p1) && RulesHelper.containsParam(body, p2) && dimOrder.last.ae.evalInt == dimOrder.length - 2 && dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 1  &&
        RulesHelper.checkDimOrder(dimOrder.dropRight(2)) && {
        var exprFound = false
        input1 match {
          case CounterInteger(_, _, _, _, _, _) =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        input2 match {
          case CounterInteger(_, _, _, _, _, _) =>
            exprFound = true
          case _ =>
        }
        exprFound
      }=>
      MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p1, body), input1)), input2)
  })

  def skipMapWithCounter: Rule = Rule("moveDownTranspose_skipMapWithCounter", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, body ,_), input, _), dimOrder, _) if {
      var exprFound = false
      input match {
        case CounterInteger(_, _, _, _, _, _) =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && dimOrder.last.ae.evalInt == dimOrder.length - 1 =>
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, dimOrder.length - 1)
      MapOrderedStream(
        ArchLambda(p1, TransposeNDOrderedStream(body, newDimOrder)), input
      )
  })

  def skipVecToStm2D: Rule = Rule("moveDownTranspose_skipVecToStm2D", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _),
    VectorToOrderedStream(ParamUse(p4), _), _), _), input, _), dimOrder, _) if
      p1.id == p4.id && p2.id ==p3.id && dimOrder.last.ae.evalInt == dimOrder.length - 2 && dimOrder(dimOrder.length - 2).ae.evalInt == dimOrder.length - 1  &&
        RulesHelper.checkDimOrder(dimOrder.dropRight(2)) =>
      MapOrderedStream(2, VectorToOrderedStream.asFunction(), TransposeNDOrderedStream(MapOrderedStream(VectorToOrderedStream.asFunction(), input), Seq(1, 0)))
  })

  def skipVecToStmND: Rule = Rule("moveDownTranspose_skipVecToStmND", {
    case TransposeNDOrderedStream(MapOrderedStream(fun: LambdaT, input, _), dimOrder, _) if
      dimOrder.head.ae.evalInt == 0 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case VectorToOrderedStream(ParamUse(p2), _) if p2.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.getMapNDLevels(fun) == dimOrder.length - 1
      } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, 0)
      MapOrderedStream(levels, VectorToOrderedStream.asFunction(), TransposeNDOrderedStream(input, newDimOrder))
  })

  def skipStmToVecND: Rule = Rule("moveDownTranspose_skipStmToVecND", {
    case TransposeNDOrderedStream(MapOrderedStream(fun: LambdaT, input, _), dimOrder, _) if
    {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case OrderedStreamToVector(ParamUse(p2), _) if p2.id == innerMostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound && RulesHelper.getMapNDLevels(fun) == dimOrder.length
    } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, 0, 0)
      MapOrderedStream(levels, OrderedStreamToVector.asFunction(), TransposeNDOrderedStream(input, newDimOrder))
  })

  def skipJoinStmND: Rule = Rule("moveDownTranspose_skipJoinStmND", {
    case TransposeNDOrderedStream(MapOrderedStream(fun: LambdaT, input, _), dimOrder, _) if
      dimOrder.head.ae.evalInt == 0 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case JoinOrderedStream(ParamUse(p2), _) if p2.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.getMapNDLevels(fun) == dimOrder.length - 1
      } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      val newDimOrder = RulesHelper.addDimToDimOrder(dimOrder, 0, 0)
      MapOrderedStream(levels, JoinOrderedStream.asFunction(), TransposeNDOrderedStream(input, newDimOrder))
  })

  def skipFunND: Rule = Rule("moveDownTranspose_skipFunND", {
    case TransposeNDOrderedStream(MapOrderedStream(fun: LambdaT, input, _), dimOrder, _) if
      dimOrder.length == RulesHelper.getMapNDLevels(fun) && dimOrder.length == input.t.asInstanceOf[OrderedStreamTypeT].dimensions.length && {
        var exprFound = false
        fun.body.visit{
          case FoldOrderedStream(_, _, _) => exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      MapOrderedStream(levels, innerMostFun, TransposeNDOrderedStream(input, dimOrder))
  })

  def mapFission2: Rule = Rule("moveDownTranspose_mapFission2", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case SplitVector(SplitVector(PermuteVector(JoinVector(JoinVector(ParamUse(p2), _), _), fun, _), cs2, _), cs1, _) if p1.id == p2.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val alreadyApplied: Boolean = body match {
        case SplitVector(SplitVector(PermuteVector(JoinVector(JoinVector(ParamUse(p2), _), _), fun, _), cs2, _), cs1, _) if p1.id == p2.id => true
        case _ => false
      }
      !alreadyApplied && exprFound && paramUses == 1
    } =>
      val param = ParamDef()
      var tranposeFun: Expr = null
      val newFun = {
        val newParam = ParamDef()
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case SplitVector(SplitVector(PermuteVector(JoinVector(JoinVector(ParamUse(p2), _), _), fun, _), cs2, _), cs1, _) if p1.id == p2.id =>
              tranposeFun = SplitVector(SplitVector(PermuteVector(JoinVector(JoinVector(ParamUse(param))), fun), cs2), cs1)
              ParamUse(newParam)
            case ParamUse(pd) if pd.id == newParam.id =>
              ParamUse(newParam)
            case e => e
          }.asInstanceOf[Expr]
        )
      }
      MapOrderedStream(newFun, MapOrderedStream(ArchLambda(param, tranposeFun), input))
  })


  def skipConcat: Rule = Rule("moveDownTranspose_skipConcat", {
    case TransposeNDOrderedStream(ConcatOrderedStream(input, Repeat(Repeat(Repeat(pad, rep1, _), rep2, _), rep3, _), dim, _), dimOrder, _) if
      dimOrder.length == 3 && dimOrder(0).ae.evalInt == 1 && dimOrder(1).ae.evalInt == 2 && dimOrder(2).ae.evalInt == 0 && dim.ae.evalInt < 2 =>
      ConcatOrderedStream(TransposeNDOrderedStream(input, dimOrder), Repeat(Repeat(Repeat(pad, rep2), rep3), rep1), dim.ae.evalInt + 1)

    case TransposeNDOrderedStream(ConcatOrderedStream(Repeat(Repeat(Repeat(pad, rep1, _), rep2, _), rep3, _), input, dim, _), dimOrder, _) if
      dimOrder.length == 3 && dimOrder(0).ae.evalInt == 1 && dimOrder(1).ae.evalInt == 2 && dimOrder(2).ae.evalInt == 0 && dim.ae.evalInt < 2 =>
      ConcatOrderedStream(Repeat(Repeat(Repeat(pad, rep2), rep3), rep1), TransposeNDOrderedStream(input, dimOrder), dim.ae.evalInt + 1)

    case TransposeNDOrderedStream(ConcatOrderedStream(input, Repeat(Repeat(Repeat(Repeat(pad, rep1, _), rep2, _), rep3, _), rep4, _), dim, _), dimOrder, _) if
      dimOrder.length == 4 && dimOrder(0).ae.evalInt == 1 && dimOrder(1).ae.evalInt == 2 && dimOrder(2).ae.evalInt == 3 && dimOrder(3).ae.evalInt == 0 && dim.ae.evalInt < 2 =>
      ConcatOrderedStream(TransposeNDOrderedStream(input, dimOrder), Repeat(Repeat(Repeat(Repeat(pad, rep2), rep3), rep4), rep1), dim.ae.evalInt + 1)

    case TransposeNDOrderedStream(ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(pad, rep1, _), rep2, _), rep3, _), rep4, _), input, dim, _), dimOrder, _) if
      dimOrder.length == 4 && dimOrder(0).ae.evalInt == 1 && dimOrder(1).ae.evalInt == 2 && dimOrder(2).ae.evalInt == 3 && dimOrder(3).ae.evalInt == 0 && dim.ae.evalInt < 2 =>
      ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(pad, rep2), rep3), rep4), rep1), TransposeNDOrderedStream(input, dimOrder), dim.ae.evalInt + 1)
  })

  def skipMapSplitStream: Rule = Rule("moveDownTranspose_skipMapSplitStream", {
    case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), cs, _), _), input, _), dimOrder, _) if
      p1.id == p2.id  =>
      val targetDim = dimOrder.length - 2
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, targetDim)
      val transposedInput = TransposeNDOrderedStream(input, newDimOrder)
      SplitOrderedStream(transposedInput, cs)
  })

  def moveIntoAlternate: Rule = Rule("moveDownTranspose_skipAlternate", {
    case TransposeNDOrderedStream(Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _),
    dimOrder, _) if RulesHelper.containsParam(body1, p1) && RulesHelper.containsParam(body2, p2) =>
      Alternate(ArchLambda(p1, TransposeNDOrderedStream(body1, dimOrder)),
        ArchLambda(p2, TransposeNDOrderedStream(body2, dimOrder)), input)
  })
}