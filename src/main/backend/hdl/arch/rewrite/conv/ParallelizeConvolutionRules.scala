package backend.hdl.arch.rewrite.conv

import backend.hdl.arch.rewrite.transpose.SwapMapsRules
import backend.hdl.arch.rewrite.{CleanupRules, MoveDownTruncIntegerRules, MoveUpVectorToStreamRules}
import backend.hdl.{LogicTypeT, OrderedStreamTypeT, ScalarTypeT, VectorTypeT}
import backend.hdl.arch.{ArchLambda, JoinVector, MapOrderedStream, MapVector, OrderedStreamToVector, SlideOrderedStream, SlideVector, SplitVector, VectorToOrderedStream}
import core.{Expr, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object ParallelizeConvolutionRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      parallelizeMapFunWithSlideStm,
      decreaseParallelismForMapVecSlideVec(config.getOrElse(2))
    ) ++ Seq(MoveUpVectorToStreamRules.mapFusionMapParam)++
      Seq(CleanupRules.mapFusionMapParam) ++ Seq(MoveDownTruncIntegerRules.mapFusion) ++
      Seq(SwapMapsRules.exchangeABMapSlide)

  def parallelizeMapFunWithSlideStm: Rule = Rule("parallelizeMapFunWithSlideStm", {
    case MapOrderedStream(ArchLambda(p1, fun, _), SlideOrderedStream(input, windowSize, stepSize, _), _)
      if {
        var exprFound = false
        fun.visit{
          case ParamUse(p2) if p1.id == p2.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        val funtc = TypeChecker.checkIfUnknown(fun, fun.t)
        var scalarFound = false
        funtc.t match {
          case st: ScalarTypeT =>
            scalarFound = true
          case vt: VectorTypeT if vt.et.isInstanceOf[LogicTypeT] =>
            scalarFound = true
          case _ =>
        }
        scalarFound
      } =>
      val inputVec = OrderedStreamToVector(input)
      val inputVecSlide = SlideVector(inputVec, windowSize.ae.evalInt, stepSize)
      val mapSlideToFun = MapVector(
        {
          val newParam = ParamDef()
          ArchLambda(newParam,
            fun.visitAndRebuild{
              case ParamUse(usedParam) if usedParam.id == p1.id =>
                ParamUse(newParam)
              case e => e
            }.asInstanceOf[Expr]
          )
        },inputVecSlide
      )
      VectorToOrderedStream(mapSlideToFun)
  })

  def decreaseParallelismForMapVecSlideVec(parMuls: Int = 2): Rule = Rule("decreaseParallelismForMapVecSlideVec", {
    case MapVector(ArchLambda(p1, fun, _), SlideVector(input, windowSize, stepSize, _), _)
      if {
        var exprFound = false
        fun.visit{
          case ParamUse(p2) if p1.id == p2.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        val inputtc = TypeChecker.checkIfUnknown(input, input.t)
        val windowElementSize =
          inputtc.t.asInstanceOf[VectorTypeT].et match {
            case vt: VectorTypeT => vt.len.ae.evalInt
            case _ => 1
          }
        val inputLen = inputtc.t.asInstanceOf[VectorTypeT].len.ae.evalInt
        val outputLen = inputLen - windowSize.ae.evalInt + stepSize.ae.evalInt
        outputLen * windowSize.ae.evalInt * windowElementSize > parMuls
      }
    =>
      val inputtc = TypeChecker.checkIfUnknown(input, input.t)
      val windowElementSize =
        inputtc.t.asInstanceOf[VectorTypeT].et match {
          case vt: VectorTypeT => vt.len.ae.evalInt
          case _ => 1
        }
      val chunkVecSize = parMuls / (windowSize.ae.evalInt * windowElementSize)
      val slideVecStm = VectorToOrderedStream(SplitVector(SlideVector(input, windowSize, stepSize), chunkVecSize))
      val slideVecStmtc = TypeChecker.checkIfUnknown(slideVecStm, slideVecStm.t)

      val splitSlide = MapOrderedStream({
        val param1 = ParamDef(slideVecStmtc.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(
          param1,
          MapVector(
            {
              val param2 = ParamDef()
              ArchLambda(
              param2,
              fun.visitAndRebuild{
                case ParamUse(usedParam) if usedParam.id == p1.id =>
                  ParamUse(param2)
                case e => e
              }.asInstanceOf[Expr]
            )
          }, ParamUse(param1))
        )
      }, slideVecStmtc)
      JoinVector(OrderedStreamToVector(splitSlide))
  })
}
