package backend.hdl.arch.rewrite.conv

import backend.hdl.arch.mem.Read
import backend.hdl.arch.rewrite.{MergeIntoCounterRules, RulesHelper}
import backend.hdl.arch._
import backend.hdl.{IntType, IntTypeT, OrderedStreamTypeT, VectorTypeT}
import core.rewrite.{Rule, RulesT}
import core._
import lift.arithmetic._

object MoveDownSlideGeneralStreamRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      skipSplitStream,
      skipVectorToStream,
      skipDropStream,
      skipJoinStream,
      skipMapFun,
      skipMarker,
      moveIntoLet,
      skipMapVecJoinVec,
      skipTransposeVec,
      mapFission,
      mapFissionMapNDSlide,
      mapFusionMapNDSlide,
      mapFusion,
      moveIntoAlternate,
      mergeCounterND
    ) ++ Seq(MergeIntoCounterRules.mergeSlideGeneral)


  def skipSplitStream: Rule = Rule("moveDownSlideGeneralStream_skipSplitStream", {
    case SlideGeneralOrderedStream(SplitOrderedStream(input, chunkSize, _), windowSize, stepSize, _) =>
      val newWindowSize = windowSize.ae.evalInt * chunkSize.ae.evalInt
      val newStepSize = stepSize.ae.evalInt * chunkSize.ae.evalInt
      val slidedInput = SlideGeneralOrderedStream(input, newWindowSize, newStepSize)
      MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), slidedInput)

    case MapOrderedStream(fun: LambdaT, SplitOrderedStream(input, chunkSize, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case SlideGeneralOrderedStream(ParamUse(pd), _, _, _) if pd.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound && RulesHelper.getMapNDLevels(fun) > 1
    } =>
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val levels = RulesHelper.getMapNDLevels(fun)
      SplitOrderedStream(MapOrderedStream(levels - 1, innerMostFun, input), chunkSize)
  })

  def skipVectorToStream: Rule = Rule("moveDownSlideGeneralStream_skipVectorToStream", {
    case SlideGeneralOrderedStream(VectorToOrderedStream(input, _), windowSize, stepSize, _) =>
      MapOrderedStream(VectorToOrderedStream.asFunction(), VectorToOrderedStream(SlideVector(input, windowSize, stepSize)))
  })

  def skipDropStream: Rule = Rule("moveDownSlideGeneralStream_skipDropStream", {
    case SlideGeneralOrderedStream(DropOrderedStream(input, firstElements, lastElements, _), windowSize, stepSize, _) =>
      val dropMargin = firstElements.ae.evalInt + lastElements.ae.evalInt
      val newWindowSize = windowSize.ae.evalInt + dropMargin
      val slidedInput = SlideGeneralOrderedStream(input, newWindowSize, stepSize)
      MapOrderedStream(DropOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements)), slidedInput)
  })

  def skipJoinStream: Rule = Rule("moveDownSlideGeneralStream_skipJoinStream", {
    case SlideGeneralOrderedStream(JoinOrderedStream(input, _), windowSize, stepSize, _) if {
      val innerLen = input.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      windowSize.ae.evalInt % innerLen.ae.evalInt == 0 && stepSize.ae.evalInt % innerLen.ae.evalInt == 0
    } =>
      val innerLen = input.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      val newWindowSize = windowSize.ae.evalInt / innerLen.ae.evalInt
      val newStepSize = stepSize.ae.evalInt / innerLen.ae.evalInt
      val slidedInput = SlideGeneralOrderedStream(input, newWindowSize, newStepSize)
      MapOrderedStream(JoinOrderedStream.asFunction(), slidedInput)
  })

  def skipMapFun: Rule = Rule("moveDownSlideGeneralStream_skipMapFun", {
    case SlideGeneralOrderedStream(MapOrderedStream(fun, input, _), windowSize, stepSize, _) if {
      var exprFound = false
      fun.visit{
        case ConcatOrderedStream(_, _, _, _) => exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      MapOrderedStream(2, fun, SlideGeneralOrderedStream(input, windowSize, stepSize))
  })

  def skipMarker: Rule = Rule("moveDownSlideGeneralStream_skipMarker", {
    case SlideGeneralOrderedStream(Marker(input, text), windowSize, stepSize, _) =>
      Marker(SlideGeneralOrderedStream(input, windowSize, stepSize), text)
    case MapOrderedStream(body: LambdaT, Marker(input, text), _) if {
      var exprFound = false
      val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(body)
      innerMostBody.body match {
        case SlideGeneralOrderedStream(ParamUse(p3), windowSize, stepSize, _) if p3.id == innerMostBody.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(body)
      val mapLevels = RulesHelper.getMapNDLevels(body)
      Marker(MapOrderedStream(mapLevels, innerMostBody, input), text)
  })

  def moveIntoLet: Rule = Rule("moveDownSlideGeneralStream_moveIntoLet", {
    case SlideGeneralOrderedStream(Let(arg, body, param, _), windowSize, stepSize, _) =>
      Let(arg, SlideGeneralOrderedStream(body, windowSize, stepSize), param)
    case MapOrderedStream(body: LambdaT, Let(arg, letBody, param, _), _) if {
      var exprFound = false
      val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(body)
      innerMostBody.body match {
        case SlideGeneralOrderedStream(ParamUse(p3), windowSize, stepSize, _) if p3.id == innerMostBody.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(body)
      val mapLevels = RulesHelper.getMapNDLevels(body)
      Let(arg, MapOrderedStream(mapLevels, innerMostBody, letBody), param)
  })

  def skipMapVecJoinVec: Rule = Rule("moveDownSlideGeneralStream_skipMapVecJoinVec", {
    case SlideGeneralOrderedStream(VectorToOrderedStream(MapVector(ArchLambda(p1, JoinVector(ParamUse(p2), _), _), input, _), _), windowSize, stepSize, _) if p1.id == p2.id =>
      val slidedInput = SlideGeneralOrderedStream(VectorToOrderedStream(input), windowSize, stepSize)
      val slidedInputtc = TypeChecker.check(slidedInput)
      MapOrderedStream(
        {
          val param1 = ParamDef(slidedInputtc.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param1,
            MapOrderedStream(JoinVector.asFunction(), ParamUse(param1)
            )
          )
        }, slidedInputtc
      )
  })

  def mapFission: Rule = Rule("moveDownSlideGeneralStream_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case SlideGeneralOrderedStream(ParamUse(p2), windowSize, stepSize, _) if p1.id == p2.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isSlideStreamOnly: Boolean = body match {
        case SlideGeneralOrderedStream(ParamUse(p2), windowSize, stepSize, _) if p1.id == p2.id => true
        case _ => false
      }
      !isSlideStreamOnly && exprFound && paramUses == 1
    } =>
      var windowSize: ArithTypeT = 0
      var stepSize: ArithTypeT = 0
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case SlideGeneralOrderedStream(ParamUse(p2), ws, ss, _) if p1.id == p2.id =>
                windowSize = ws
                stepSize = ss
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(SlideGeneralOrderedStream.asFunction(Seq(None), Seq(windowSize, stepSize)), input)
      )
  })

  def mapFissionMapNDSlide: Rule = Rule("moveDownSlideGeneralStream_mapFissionMapNDSlide", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
          p1.id == p2.id && {
            val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
            var innerExprFound = false
            innerMostBody.body match {
              case SlideGeneralOrderedStream(ParamUse(p3), windowSize, stepSize, _) if p3.id == innerMostBody.param.id =>
                innerExprFound = true
              case _ =>
            }
            innerExprFound
          } =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapNDSlideStmOnly: Boolean = body match {
        case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
          p1.id == p2.id && {
            val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
            var innerExprFound = false
            innerMostBody.body match {
              case SlideGeneralOrderedStream(ParamUse(p3), windowSize, stepSize, _) if p3.id == innerMostBody.param.id =>
                innerExprFound = true
              case _ =>
            }
            innerExprFound
          } => true
        case _ => false
      }
      !isMapNDSlideStmOnly && exprFound && paramUses == 1
    } =>
      var windowSize: ArithTypeT = 0
      var stepSize: ArithTypeT = 0
      var innerLevels: Int = 0
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
                p1.id == p2.id && {
                  val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
                  var innerExprFound = false
                  innerLevels = RulesHelper.getMapNDLevels(innerBody)
                  innerMostBody.body match {
                    case SlideGeneralOrderedStream(ParamUse(p3), ws, ss, _) if p3.id == innerMostBody.param.id =>
                      windowSize = ws
                      stepSize = ss
                      innerExprFound = true
                    case _ =>
                  }
                  innerExprFound
                } =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(innerLevels + 1, SlideGeneralOrderedStream.asFunction(Seq(None), Seq(windowSize, stepSize)), input)
      )
  })

  def mapFusion: Rule = Rule("moveDownSlideGeneralStream_mapFusion", {
    case MapOrderedStream(ArchLambda(p1, body1, _), MapOrderedStream(fun2: LambdaT, input, _), _)
      if {
        var exprFound = false
        body1.visit {
          case SlideGeneralOrderedStream(ParamUse(p3), windowSize, stepSize, _) if p1.id == p3.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        // Avoid fusion with MapND-slide, or there will be a rewriting loop.
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun2)
        innerMostBody.body match {
          case SlideGeneralOrderedStream(ParamUse(p3), windowSize, stepSize, _) if p3.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild {
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              fun2.body.visitAndRebuild {
                case ParamUse(usedParam2) if usedParam2.id == fun2.param.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def mapFusionMapNDSlide: Rule = Rule("moveDownSlideGeneralStream_mapFusionMapNDSlide", {
    case MapOrderedStream(ArchLambda(p1, body1, _), MapOrderedStream(fun2: LambdaT, input, _), _)
      if {
        var exprFound = false
        body1.visit {
          case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
            p1.id == p2.id && {
              val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
              var innerExprFound = false
              innerMostBody.body match {
                case SlideGeneralOrderedStream(ParamUse(p3), windowSize, stepSize, _) if p3.id == innerMostBody.param.id =>
                  innerExprFound = true
                case _ =>
              }
              innerExprFound
            } =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        // Avoid fusion with MapND-slide, or there will be a rewriting loop.
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun2)
        innerMostBody.body match {
          case SlideGeneralOrderedStream(ParamUse(p3), windowSize, stepSize, _) if p3.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild {
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              fun2.body.visitAndRebuild {
                case ParamUse(usedParam2) if usedParam2.id == fun2.param.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def moveIntoAlternate: Rule = Rule("moveDownSlideGeneralStream_skipAlternate", {
    case SlideGeneralOrderedStream(Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _),
    windowSize, stepSize, _) if RulesHelper.containsParam(body1, p1) && RulesHelper.containsParam(body2, p2)=>
      Alternate(ArchLambda(p1, SlideGeneralOrderedStream(body1, windowSize, stepSize)),
        ArchLambda(p2, SlideGeneralOrderedStream(body2, windowSize, stepSize)), input)

    case MapOrderedStream(fun: LambdaT, Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _), _) if
      RulesHelper.containsParam(body1, p1) && RulesHelper.containsParam(body2, p2) && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case SlideGeneralOrderedStream(ParamUse(p), windowSize, stepSize, _) if p.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      var windowSize: ArithTypeT = 0
      var stepSize: ArithTypeT = 0

      innerMostFun.body match {
        case SlideGeneralOrderedStream(ParamUse(p), ws, ss, _) if p.id == innerMostFun.param.id =>
          windowSize = ws
          stepSize = ss
        case _ =>
      }

      Alternate(ArchLambda(p1, MapOrderedStream(levels, SlideGeneralOrderedStream.asFunction(Seq(None), Seq(windowSize, stepSize)), body1)),
        ArchLambda(p2, MapOrderedStream(levels, SlideGeneralOrderedStream.asFunction(Seq(None), Seq(windowSize, stepSize)), body2)), input)
  })

  def insertSlideBetweenCounterDim(start: ArithTypeT, inc: ArithTypeT, dims: Seq[ArithTypeT], bitWidth: ArithTypeT,
                                   depth: Int, windowSize: ArithTypeT, stepSize: ArithTypeT): Expr = {
    dims.splitAt(dims.size - depth) match {
      case (Seq(), _) =>
        scala.sys.error("Cannot insert slide on an integer")

      case (_, Seq()) =>
        // leave mergeCounterND rewrite to deal with this
        SlideGeneralOrderedStream(CounterInteger(start, inc, dims, Some(IntType(bitWidth))), windowSize, stepSize)

      case (inner, outer) =>
        val newStepSize = inner.foldLeft(inc.ae)(_ * _.ae)
        val outerCounter = CounterInteger(start, newStepSize, outer, Some(IntType(bitWidth)))
        val innerCounter = CounterInteger(0, inc, inner, None)

        MapOrderedStream(outer.size, {
          val p1 = ParamDef(IntType(bitWidth))
          ArchLambda(p1, {
            // this form triggers outer rewrites, namely:
            // 1.  counter |> map f |> slide --> counter |> slide |> map (map f)
            // 2.  counter |> slide --> mergeCounterND
            SlideGeneralOrderedStream(
              MapOrderedStream(inner.size, {
                val p2 = ParamDef(innerCounter.t.asInstanceOf[OrderedStreamTypeT].leafType)
                ArchLambda(p2,
                  ResizeInteger(AddInt2(ParamUse(p1), ParamUse(p2)), bitWidth)
                )
              }, innerCounter),
              windowSize, stepSize
            )
          })
        }, outerCounter)
    }
  }

  def mergeCounterND: Rule = Rule("moveDownSlideGeneralStream_mergeCounterND", {
    case SlideGeneralOrderedStream(CounterInteger(start, inc, loop, dim, rep, _), windowSize, stepSize, t: OrderedStreamTypeT) if (
      loop.ae.evalInt == 0 && rep.forall(_.ae.evalInt == 0)
    ) =>
      val bitWidth = t.leafType.bitWidth
      val newStepSize = dim.init.foldLeft(stepSize.ae * inc.ae)(_ * _.ae)
      val stepCounter = CounterInteger(start, newStepSize, Seq(t.len), Some(IntType(bitWidth)))
      val windowCounter = CounterInteger(0, inc, dim.init :+ windowSize)
      MapOrderedStream({
        val p1 = ParamDef(stepCounter.t.asInstanceOf[OrderedStreamTypeT].et)
        ArchLambda(p1, {
          MapOrderedStream(dim.size, {
            val p2 = ParamDef(windowCounter.t.asInstanceOf[OrderedStreamTypeT].leafType)
            ArchLambda(p2,
              // Must type check or there will be an overflow error!
              TypeChecker.check(ResizeInteger(AddInt2(ParamUse(p1), ParamUse(p2)), bitWidth))
            )
          }, windowCounter)
        })
      }, stepCounter)

    case MapOrderedStream(ArchLambda(d1, SlideGeneralOrderedStream(ParamUse(u1), windowSize, stepSize, _), _),
      CounterInteger(start, inc, loop, dim, rep, _), t: OrderedStreamTypeT) if (
      d1.id == u1.id && loop.ae.evalInt == 0 && rep.forall(_.ae.evalInt == 0)
    ) =>
      insertSlideBetweenCounterDim(start, inc, dim, t.leafType.bitWidth, 1, windowSize, stepSize)

    case MapOrderedStream(ArchLambda(d1, MapOrderedStream(ArchLambda(d2, SlideGeneralOrderedStream(ParamUse(u2), windowSize, stepSize, _), _), ParamUse(u1), _), _),
      CounterInteger(start, inc, loop, dim, rep, _), t: OrderedStreamTypeT) if (
      d1.id == u1.id && d2.id == u2.id && loop.ae.evalInt == 0 && rep.forall(_.ae.evalInt == 0)
    ) =>
      insertSlideBetweenCounterDim(start, inc, dim, t.leafType.bitWidth, 2, windowSize, stepSize)
  })

  /*
   *********************************************************************************************************************
    SlideGeneralOrderedStream to SlideVector Rules
   *********************************************************************************************************************
   */
  def skipTransposeVec: Rule = Rule("moveDownSlideGeneralStream_skipTransposeVec", {
    case SlideGeneralOrderedStream(VectorToOrderedStream(SplitVector(PermuteVector(JoinVector(input, _), permuteFun, _), chunkSize, _), _), windowSize, stepSize, _) if {
      var exprFound = false
      permuteFun.b.ae match {
        case Sum(IntDiv(v1, c1) :: Prod(Cst(c3) :: Mod(v2, c2) :: Nil) :: Nil) if v1 == v2 && c1 == c2 && c3.toInt == chunkSize.ae.evalInt =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val slidedVecInput = MapVector(SlideVector.asFunction(Seq(None), Seq(windowSize, stepSize)), input)
      val slidedVecInputtc = TypeChecker.check(slidedVecInput)
      val joinIntermediate = MapVector(JoinVector.asFunction(), slidedVecInputtc)
      val transposedIntermediate = Transpose(joinIntermediate)
      val transposedIntermediatetc = TypeChecker.checkIfUnknown(transposedIntermediate, transposedIntermediate.t)
      MapOrderedStream(VectorToOrderedStream.asFunction(), VectorToOrderedStream(SplitVector(transposedIntermediatetc, windowSize)))
  })

  def moveUpMapConversion: Rule = Rule("moveUpMapConversion", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(convFun: LambdaT, body, _), _), SlideOrderedStream(input, windowSize, stepSize, _), _) if
      RulesHelper.containsParam(body, p1) && {
        var exprFound = false
        convFun.body match {
          case Conversion(ParamUse(p1), t) if p1.id == convFun.param.id =>
            exprFound = true
          case Conversion(DropVector(Conversion(ParamUse(p1), _), _, _, _), _) if p1.id == convFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream(2, convFun, MapOrderedStream(ArchLambda(p1, body), SlideOrderedStream(input, windowSize, stepSize)))
  })

  def skipStmToVec: Rule = Rule("MoveDownSlideStm_skipStmToVec", {
    case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(body, _), _), SlideOrderedStream(input, winSize, stepSize, _), _) if RulesHelper.containsParam(body, p1) =>
      MapOrderedStream(OrderedStreamToVector.asFunction(), MapOrderedStream(ArchLambda(p1, body), SlideOrderedStream(input, winSize, stepSize)))
  })
}
