package backend.hdl.arch.rewrite.conv

import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.arch.{ArchLambda, MapOrderedStream, SlideOrderedStream}
import core.rewrite.Rule
import core.{LambdaT, ParamDef, ParamUse}

object MoveUpSlideStreamRules {
  def mapFission: Rule = Rule("MoveUpSlideStreamRules_mapFission", {
    case MapOrderedStream(ArchLambda(p1, SlideOrderedStream(body, windowSize, stepSize, _), _), input, _) if
      RulesHelper.containsParam(body, p1) && !RulesHelper.isParamUse(body) =>
      MapOrderedStream(SlideOrderedStream.asFunction(Seq(None), Seq(windowSize, stepSize)), MapOrderedStream(ArchLambda(p1, body), input))

    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun: LambdaT, inputInner, _), _), inputOuter, _)
      if {
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostBody.body match {
          case SlideOrderedStream(ParamUse(p2), windowSize, stepSize, _) if p2.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } &&
        // prevent infinite rewrites with this rule:
        (inputInner match {
          case ParamUse(p3) if p1.id == p3.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(
            pOuter,
            MapOrderedStream(
              fun,
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          ArchLambda(p1, inputInner),
          inputOuter
        )
      )
  })
}
