package backend.hdl.arch.rewrite.conv

import core._
import core.rewrite.{Rule, RulesT}
import backend.hdl._
import backend.hdl.arch.mem.{Padding, Read, ReadOutOfBounds}
import backend.hdl.arch.rewrite.RulesHelper
import backend.hdl.arch._

object MoveDownPadRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] = Seq(
    skipMarker,
    mapFission,
    skipSlide,
    mapNDFusion,
    skipLet,
    skipMapRead
  )

  def skipMap: Rule = Rule("MoveDownPad_skipMap", {
    case MapOrderedStream(ArchLambda(p1, PadOrderedStream(ParamUse(u1), firstElements, lastElements, value, _), _),
    MapOrderedStream(ArchLambda(p2, MapOrderedStream(fun: LambdaT, ParamUse(u2), _), _), input, _), _)
      if RulesHelper.isStreamReshapeFunction(fun) && p1.id == u1.id && p2.id == u2.id =>
      MapOrderedStream({
        val p = ParamDef()
        ArchLambda(p, MapOrderedStream(fun, ParamUse(p)))
      }, MapOrderedStream(PadOrderedStream.asFunction(types = Seq(firstElements, lastElements, value)), input))

    case PadOrderedStream(MapOrderedStream(fun: LambdaT, input, _), firstElements, lastElements, value, _)
      if RulesHelper.isStreamReshapeFunction(fun) =>
      MapOrderedStream(fun, PadOrderedStream(input, firstElements, lastElements, value))
  })

  def skipMarker: Rule = Rule("MoveDownPad_skipMarker", {
    case PadOrderedStream(Marker(input, ts), firstElements, lastElements, value, _) =>
      Marker(PadOrderedStream(input, firstElements, lastElements, value), ts)

    case MapOrderedStream(fun: LambdaT, Marker(input, ts), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), firstElements, lastElements, value, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val levels = RulesHelper.getMapNDLevels(fun)
      var firstElements: ArithTypeT = 0
      var lastElements: ArithTypeT = 0
      var value: ArithTypeT = 0
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), fes, les, v, _) if p.id == innerMostFun.param.id =>
          firstElements = fes
          lastElements = les
          value = v
        case _ =>
      }
      Marker(MapOrderedStream(levels, PadOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements, value)), input), ts)
  })

  def mapFission: Rule = Rule("MoveDownPad_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      body.visit {
        case PadOrderedStream(ParamUse(p2), firstElements, lastElements, value, _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      body match {
        case PadOrderedStream(ParamUse(p2), firstElements, lastElements, value, _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      var newInput: Expr = null
      body.visit {
        case PadOrderedStream(ParamUse(p2), firstElements, lastElements, value, _) if p1.id == p2.id =>

          val param = ParamDef()
          newInput = MapOrderedStream(ArchLambda(param, PadOrderedStream(ParamUse(param), firstElements, lastElements, value)), input)
        case _ =>
      }
      MapOrderedStream(
        {
          val param1 = ParamDef()
          ArchLambda(
            param1,
            body.visitAndRebuild {
              case PadOrderedStream(ParamUse(p2), firstElements, lastElements, value, _) if p1.id == p2.id =>
                ParamUse(param1)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )

    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case PadOrderedStream(ParamUse(p3), firstElements, lastElements, value, _) if p3.id == innerMostFun.param.id => exprFound1 = true
            case _ =>
          }
          exprFound1
        } =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case PadOrderedStream(ParamUse(p3), firstElements, lastElements, value, _) if p3.id == innerMostFun.param.id => exprFound1 = true
            case _ =>
          }
          exprFound1
        } => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      var level: Int = 0
      var firstElements: ArithTypeT = 0
      var lastElements: ArithTypeT = 0
      var value: ArithTypeT = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case PadOrderedStream(ParamUse(p3), fes, les, v, _) if p3.id == innerMostFun.param.id =>
              level = RulesHelper.getMapNDLevels(fun)
              firstElements = fes
              lastElements = les
              value = v
              exprFound1 = true
            case _ =>
          }
          exprFound1
        } =>
        case _ =>
      }

      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
                var exprFound1 = false
                val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
                innerMostFun.body match {
                  case PadOrderedStream(ParamUse(p3), firstElements, lastElements, value, _) if p3.id == innerMostFun.param.id =>
                    level = RulesHelper.getMapNDLevels(fun)
                    exprFound1 = true
                  case _ =>
                }
                exprFound1
              } =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(level + 1, PadOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements, value)), input)
      )
  })

  def skipSlide: Rule = Rule("MoveDownPad_skipSlide", {
    case MapOrderedStream(fun: LambdaT, SlideGeneralOrderedStream(input, windowSize, stepSize, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), firstElements, lastElements, value, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound && RulesHelper.getMapNDLevels(fun) >= 2 // Avoid accessing the dimension controlled by slide.
    } =>
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val levels = RulesHelper.getMapNDLevels(fun)
      var firstElements: ArithTypeT = 0
      var lastElements: ArithTypeT = 0
      var value: ArithTypeT = 0
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), fes, les, v, _) if p.id == innerMostFun.param.id =>
          firstElements = fes
          lastElements = les
          value = v
        case _ =>
      }
      SlideGeneralOrderedStream(TypeChecker.check(MapOrderedStream(levels - 1, PadOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements, value)), input)), windowSize, stepSize)
  })

  private def mapNDFusion: Rule = Rule("MoveDownPad_mapNDFusion", {
    // map ND fusion
    case MapOrderedStream(repFun: LambdaT, MapOrderedStream(ArchLambda(param, body, _), input, _), _) if { //ArchLambda(p1, Repeat(ParamUse(p2), repetitions, _), _)
      // Avoid looping with multiple-repeat-removal guard in moveDownMapRepeatParamUse
      val isMapRepeatOnly: Boolean = body match {
        case PadOrderedStream(ParamUse(_), _, _, _, _) => true
        case MapOrderedStream(innerRepFun: LambdaT, _, _) if {
          var exprFound = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(innerRepFun)
          innerMostFun.body match {
            case PadOrderedStream(ParamUse(p), _, _, _, _) if p.id == innerMostFun.param.id => exprFound = true
            case _ =>
          }
          exprFound
        } => true
        case _ => false
      }
      !isMapRepeatOnly
    } && {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), firstElements, lastElements, value, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val funLevels = RulesHelper.getMapNDLevels(repFun)
      var firstElements: ArithTypeT = 0
      var lastElements: ArithTypeT = 0
      var value: ArithTypeT = 0
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), fes, les, v, _) if p.id == innerMostFun.param.id =>
          firstElements = fes
          lastElements = les
          value = v
        case _ =>
      }
      MapOrderedStream(
        {
          ArchLambda(
            param,
            if(funLevels == 1)
              PadOrderedStream(body, firstElements, lastElements, value)
            else
              MapOrderedStream(funLevels - 1, PadOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements, value)), body)
          )
        },
        input
      )
  })

  def skipLet: Rule = Rule("MoveDownPad_skipLet", {
    case PadOrderedStream(Let(p, body, arg, _), firstElements, lastElements, value, _) =>
      Let(p, PadOrderedStream(body, firstElements, lastElements, value), arg)

    case MapOrderedStream(fun: LambdaT, Let(p, body, arg, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), firstElements, lastElements, value, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val levels = RulesHelper.getMapNDLevels(fun)
      var firstElements: ArithTypeT = 0
      var lastElements: ArithTypeT = 0
      var value: ArithTypeT = 0
      innerMostFun.body match {
        case PadOrderedStream(ParamUse(p), fes, les, v, _) if p.id == innerMostFun.param.id =>
          firstElements = fes
          lastElements = les
          value = v
        case _ =>
      }
      Let(p, MapOrderedStream(levels, PadOrderedStream.asFunction(Seq(None), Seq(firstElements, lastElements, value)), body), arg)
  })

  def skipMapRead: Rule = Rule("MoveDownPad_skipMapRead", {
    // Right now we only handle one case.
    // TODO: Make it more general.
    case MapOrderedStream(ArchLambda(p1, PadOrderedStream(ParamUse(p2), firstElementsW, lastElementsW, valueW, _), _),
    PadOrderedStream(MapOrderedStream(fun: LambdaT, CounterInteger(start, inc, loop, reps, dims, t: OrderedStreamTypeT), _), firstElementsH, lastElementsH, valueH, _), _) if
      p1.id == p2.id && valueW.ae.evalInt == valueH.ae.evalInt && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case Read(mem, ParamUse(p), _) if p.id == innerMostFun.param.id => exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.getMapNDLevels(fun) == 3
      } =>
      val newBitWidth = t.leafType.bitWidth.ae.evalInt + 1 // Reserve an extra bit
      val invalidValue = ((1L << newBitWidth) - 1).toInt
      val levels = RulesHelper.getMapNDLevels(fun)
      var mem: Expr = null

      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      var hwt: HWDataTypeT = null
      innerMostFun.body match {
        case Read(m, ParamUse(p), t) if p.id == innerMostFun.param.id =>
          mem = m
          hwt = t.asInstanceOf[HWDataTypeT]
        case _ =>
      }

      // Since the "fun" node is a MapND of a singular Read, the result of the Mapped Counter
      // must be N dimensions of T --- whatever Read returns, which means each of the values
      // padded must be coerced into a singular instance of the same type T.
      val defaultValue = BitVectorType(
        scala.collection.immutable.BitSet.fromBitMask(Array(valueW.ae.evalInt)),
        hwt.bitWidth.ae.evalInt)

      // rebuild counter
      val newCounter = CounterInteger(start, inc, loop, reps, dims, Some(IntType(newBitWidth)))
      val paddedCounter = TypeChecker.check(MapOrderedStream(PadOrderedStream.asFunction(Seq(None), Seq(firstElementsW, lastElementsW, invalidValue)),
        PadOrderedStream(newCounter, firstElementsH, lastElementsH, invalidValue)))

      // rebuild read
      MapOrderedStream(
        levels,
        {
          val param = ParamDef(paddedCounter.t.asInstanceOf[OrderedStreamTypeT].leafType)
          ArchLambda(param, ReadOutOfBounds(mem, ParamUse(param), Some(defaultValue), Padding(invalidValue)))
        }, paddedCounter
      )
  })
}
