package backend.hdl.arch.rewrite

import backend.hdl.{IntType, IntTypeT, NamedTupleTypeT, OrderedStreamTypeT}
import backend.hdl.arch.mem.{Write, WriteOutOfBounds}
import backend.hdl.arch.rewrite.Address2DRules.consCntrMaxWidth
import backend.hdl.arch.{ArchLambda, ConstrainedSum, CounterInteger, DropOrderedStream, IndexUnorderedStream, JoinOrderedStream, JoinUnorderedStream, MapOrderedStream, OrderedStreamToUnorderedStream, ReduceOrderedStream, ResizeInteger, SplitOrderedStream, Tuple2, VectorToOrderedStream, Zip2OrderedStream, ZipOrderedStream}
import core.{ArithType, ArithTypeT, Expr, LambdaT, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}
import core.util.IRDotGraph

object MoveUpDropStreamRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      mapFission
    )

  def mergeIntoWriteAddress(): Seq[Rule] = Seq(
    mapFissionSimple,
    mapFission,
    mapFusionMapParam,
    skipMapStm,
    MoveUpOrderedToUnorderedStreamRules.fissionND,
    MoveUpOrderedToUnorderedStreamRules.fission1D,
    MoveUpOrderedToUnorderedStreamRules.fission,
    mergeIntoWrite2D
  )

  def mapFissionSimple: Rule = Rule("moveUpDropStreamRules_mapFissionSimple", {
    case MapOrderedStream(ArchLambda(origParam, DropOrderedStream(fun, hi, lo, _), _), input, _) if !RulesHelper.isParamUse(fun)  =>
      // IRDotGraph(e).show()
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(param, DropOrderedStream(ParamUse(param), hi, lo))
        },
        MapOrderedStream(ArchLambda(origParam, fun), input)
      )
  })

  def mapFission: Rule = Rule("moveUpDropStreamRules_mapFission", {
    // map fission
    // prevent infinite rewrites with this rule by checking that fun is not a ParamUse
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(fun: LambdaT, innerInput, _), _), input, _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case DropOrderedStream(ParamUse(pd), fe, le, _) if pd.id == innerMostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && RulesHelper.containsParam(innerInput, p1) && !RulesHelper.isParamUse(innerInput) &&
      RulesHelper.getMapNDLevels(fun) >= 1 =>
      val levels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      MapOrderedStream(levels + 1, innerMostFun, MapOrderedStream(ArchLambda(p1, innerInput), input))
  })

  def mapFusionMapParam: Rule = Rule("moveUpDropStreamRules_mapFusionMapParam", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        val dropInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        dropInnerMost.body match {
          case DropOrderedStream(ParamUse(p3), _, _, _) if p3.id == dropInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit{
          case ParamUse(p4) if p1.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        // Do not fuse if first map contains vec2Stm only
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body1) )
        innerMostFun.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def skipMapStm: Rule = Rule("moveUpDropStreamRules_skipMapStm", {
    case MapOrderedStream(f, DropOrderedStream(input, hi, lo, _), _) =>
      DropOrderedStream(MapOrderedStream(f, input), hi, lo)
  })

  def mergeIntoWrite2D: Rule = Rule("moveUpDropStreamRules_mergeIntoWrite2D", {
    case ReduceOrderedStream(wrFun @ ArchLambda(_, ArchLambda(_, ReduceOrderedStream(ArchLambda(_, ArchLambda(_, Write(_, _, _),
      _), _), _, _, _), _), _), paramIn @ ParamUse(_), SplitOrderedStream(IndexUnorderedStream(
      JoinUnorderedStream(OrderedStreamToUnorderedStream(JoinOrderedStream(DropOrderedStream(
      MapOrderedStream(ArchLambda(p1, DropOrderedStream(ParamUse(p2), wLow, wHigh, _), _),
      MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, OrderedStreamToUnorderedStream(ParamUse(p5), _), _),
      ParamUse(p6), _), _), input, _), _),
      hLow, hHigh, _), jt: OrderedStreamTypeT), _), _), iut), cs, st: OrderedStreamTypeT), _) if
      p1.id == p2.id && p3.id == p6.id && p4.id == p5.id && jt.len == st.len
    =>
      val sizeLimit = st.len.ae * cs.ae
      val maxW = input.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      val maxH = input.t.asInstanceOf[OrderedStreamTypeT].len
      val addrWidth = iut.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[NamedTupleTypeT].namedTypes.last._2.asInstanceOf[IntTypeT].bitWidth
      val newAddrWidth = if(math.pow(2, addrWidth.ae.evalInt).toInt == sizeLimit.evalInt) addrWidth.ae.evalInt + 1 else addrWidth.ae.evalInt

      // Calculate dimensions
      val newHCounter = CounterInteger(0, 1, Seq(maxH), Some(IntType(newAddrWidth)))
      val newWCounter = CounterInteger(0, 1, Seq(cs, maxW), Some(IntType(newAddrWidth)))

      val lowerBoundW = wLow.ae * cs.ae
      val upperBoundW = (maxW.ae - wHigh.ae) * cs.ae
      val lowerBoundH = hLow.ae
      val upperBoundH = maxH.ae - hHigh.ae

      val newCounters = TypeChecker.check(MapOrderedStream(
        {
          val paramH = ParamDef(newHCounter.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            paramH,
            MapOrderedStream(
              2,
              {
                val paramW = ParamDef(newWCounter.t.asInstanceOf[OrderedStreamTypeT].leafType)
                ArchLambda(
                  paramW,
                  ResizeInteger(
                    ConstrainedSum(Tuple2(ParamUse(paramW), ParamUse(paramH)), lowerBoundW, upperBoundW, lowerBoundH, upperBoundH, maxW, maxH),
                    newAddrWidth)
                )
              },
              newWCounter
            )
          )
        },
        newHCounter
      ))

      val zippedInput = TypeChecker.check(MapOrderedStream(Zip2OrderedStream.asFunction(), Zip2OrderedStream(
        Tuple2(JoinOrderedStream(input), JoinOrderedStream(newCounters)))))
      val np1 = ParamDef(zippedInput.t.asInstanceOf[OrderedStreamTypeT].et)
      val np2 = ParamDef(paramIn.t)
      val np3 = ParamDef(zippedInput.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].et)
      val np4 = ParamDef(paramIn.t)
      ReduceOrderedStream(ArchLambda(np1, ArchLambda(np2, ReduceOrderedStream(
        ArchLambda(np3, ArchLambda(np4, WriteOutOfBounds(ParamUse(np4), ParamUse(np3), sizeLimit))),
        ParamUse(np2), ParamUse(np1)))), paramIn, zippedInput)
  })
}