package backend.hdl.arch.rewrite

import backend.hdl.{IntType, OrderedStreamTypeT}
import backend.hdl.arch._
import core.rewrite.{Rule, RulesT}
import core._
import lift.arithmetic.Cst

object MergeIntoCounterRules extends RulesT {

  def reduce(expr: Expr): Expr = partialEval(Map(), expr)._1

  // Basically a CounterIntegerExpr, but with the following differences
  // *  start may be non constant (e.g. ArithTypeVar)
  // *  dims and reps may be both empty (must be the same length)
  private final case class SymbolicCounter(start: ArithTypeT, incr: ArithTypeT, loop: ArithTypeT, dims: Seq[ArithTypeT], reps: Seq[ArithTypeT])

  private def partialEval(env: Map[Long, SymbolicCounter], expr: Expr): (Expr, Option[SymbolicCounter]) = expr match {
    case CounterInteger(start, incr, loop, dims, reps, _) =>
      // propagate the equivalent symbolic value
      (expr, Some(SymbolicCounter(start, incr, loop, dims, reps)))

    case ParamUse(u) =>
      // do NOT rewrite the expression because:
      // *  the counter may be incomplete / depends on iteration and other things
      // *  SHIR does not like unused parameters
      //
      // (but do propagate the symbolic value)
      (expr, env.get(u.id))

    case MapOrderedStream(ArchLambda(param, body, _), e, t: OrderedStreamTypeT) =>
      partialEval(env, e) match {
        case (e, Some(SymbolicCounter(start, outer_incr, loop, outer_dims :+ d1, outer_reps :+ r1))) =>
          // if it's a symbolic counter, then we know param must be an subcounter.
          //
          // here, we assume the subcounter depends on the iteration count of the map.
          // this is necessary for correctness.
          val p1 = ArithTypeVar()
          val newEnv = env + (param.id -> SymbolicCounter(p1, outer_incr, loop, outer_dims, outer_reps))
          partialEval(newEnv, body) match {
            case (body, Some(SymbolicCounter(u1: ArithTypeVar, incr, _, dims, reps))) if u1.id == p1.id =>
              // in the simplest case, the map function reshapes the subcounter.
              start.ae match {
                case Cst(_) =>
                  (CounterInteger(start, incr, loop, dims :+ d1, reps :+ r1, Some(t.leafType)),
                    Some(SymbolicCounter(start, incr, loop, dims :+ d1, reps :+ r1)))
                case _ =>
                  (MapOrderedStream(ArchLambda(param, body), e),
                    Some(SymbolicCounter(start, incr, loop, dims :+ d1, reps :+ r1)))
              }

            case (body, _) => (MapOrderedStream(ArchLambda(param, body), e), None)
          }

        case (e, _) =>
          // otherwise, assume we don't know enough about the stream,
          // therefore, we also don't know anything about the result of the mapped stream.
          //
          // since all params have unique id, we don't have to update the env.
          partialEval(env, body) match {
            case (body, _) => (TypeChecker.check(MapOrderedStream(ArchLambda(param, body), e)), None)
          }
      }

    case Repeat(e, rep, _) =>
      partialEval(env, e) match {
        case (CounterInteger(start, incr, loop, dims, reps, t: OrderedStreamTypeT), _) =>
          (CounterInteger(start, incr, loop, dims :+ rep, reps :+ ArithType(1), Some(t.leafType)),
            Some(SymbolicCounter(start, incr, loop, dims :+ rep, reps :+ ArithType(1))))
        case (e, Some(SymbolicCounter(start, incr, loop, dims, reps))) =>
          (Repeat(e, rep),
            Some(SymbolicCounter(start, incr, loop, dims :+ rep, reps :+ ArithType(1))))
        case (e, _) =>
          (Repeat(e, rep), None)
      }

    case SplitOrderedStream(e, chunk, _) =>
      partialEval(env, e) match {
        case (CounterInteger(start, incr, loop, dims :+ d1, reps :+ r1, t: OrderedStreamTypeT), _) =>
          (CounterInteger(start, incr, loop, dims :+ chunk :+ ArithType(d1.ae / chunk.ae), reps :+ r1 :+ r1, Some(t.leafType)),
            Some(SymbolicCounter(start, incr, loop, dims :+ chunk :+ ArithType(d1.ae / chunk.ae), reps :+ r1 :+ r1)))
        case (e, Some(SymbolicCounter(start, incr, loop, dims :+ d1, reps :+ r1))) =>
          (SplitOrderedStream(e, chunk),
            Some(SymbolicCounter(start, incr, loop, dims :+ chunk :+ ArithType(d1.ae / chunk.ae), reps :+ r1 :+ r1)))
        case (e, _) =>
          (SplitOrderedStream(e, chunk), None)
      }

    case JoinOrderedStream(e, _) =>
      partialEval(env, e) match {
        case (CounterInteger(start, incr, loop, dims :+ d2 :+ d1, reps :+ r2 :+ r1, t: OrderedStreamTypeT), _) if (
          r2.ae.evalInt == r1.ae.evalInt
          ) =>
          (CounterInteger(start, incr, loop, dims :+ ArithType(d2.ae * d1.ae), reps :+ r2, Some(t.leafType)),
            Some(SymbolicCounter(start, incr, loop, dims :+ ArithType(d2.ae * d1.ae), reps :+ r2)))
        case (e, Some(SymbolicCounter(start, incr, loop, dims :+ d2 :+ d1, reps :+ r2 :+ r1))) if (
          r2.ae.evalInt == r1.ae.evalInt
          ) =>
          (JoinOrderedStream(e),
            Some(SymbolicCounter(start, incr, loop, dims :+ ArithType(d2.ae * d1.ae), reps :+ r1)))
        case (e, _) =>
          (JoinOrderedStream(e), None)
      }

    case _ =>
      // partial evaluate the children, no symbolic value to propagate
      (expr.build(expr.children.map({
        case e: Expr => partialEval(env, e)._1
        case t: Type => t
      })), None)
  }

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(mergeRepeat, mergeMapRepeat, mergeMapMapRepeat, mergeMapMapMapRepeat, mergeMapMapMapMapRepeat,
      mergeSplitStream, mergeMapSplitStream, splitRepeatSplitStream, nDmergeRepeat)

  def mergeRepeat: Rule = Rule("mergeRepeat", {
    case Repeat(CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), outerRep, _) =>
      CounterInteger(start, increment, loop, dimensions :+ outerRep, repetitions :+ ArithType(1), Some(t.leafType))
  })

  def mergeMapRepeat: Rule = Rule("mergeMapRepeat", {
    case MapOrderedStream(ArchLambda(p1, Repeat(ParamUse(p2), mapRep, _), _), CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if p1.id == p2.id =>
      CounterInteger(start, increment, loop, dimensions.init :+ mapRep :+ dimensions.last, repetitions.init :+ ArithType(1) :+ repetitions.last, Some(t.leafType))
    case MapOrderedStream(ArchLambda(p1, Repeat(ParamUse(p2), mapRep, _), _), RepeatHidden(CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), rep, _), _) if p1.id == p2.id =>
      RepeatHidden(CounterInteger(start, increment, loop, dimensions.init :+ mapRep :+ dimensions.last, repetitions.init :+ ArithType(1) :+ repetitions.last, Some(t.leafType)), rep)
  })

  def mergeMapMapRepeat: Rule = Rule("mergeMapMapRepeat", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, Repeat(ParamUse(p3), mapRep, _), _), ParamUse(p4), _), _), CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if p1.id == p4.id && p2.id == p3.id =>
      CounterInteger(start, increment, loop, dimensions.init.init ++ Seq(mapRep) ++ dimensions.takeRight(2), repetitions.init.init ++ Seq(ArithType(1)) ++ repetitions.takeRight(2), Some(t.leafType))
  })

  def mergeMapMapMapRepeat: Rule = Rule("mergeMapMapMapRepeat", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, Repeat(ParamUse(p4), mapRep, _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if p1.id == p6.id && p2.id == p5.id && p3.id == p4.id =>
      CounterInteger(start, increment, loop, dimensions.init.init.init ++ Seq(mapRep) ++ dimensions.takeRight(3), repetitions.init.init.init ++ Seq(ArithType(1)) ++ repetitions.takeRight(3), Some(t.leafType))
  })

  def mergeMapMapMapMapRepeat: Rule = Rule("mergeMapMapMapMapRepeat", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, Repeat(ParamUse(p5), mapRep, _), _), ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id =>
      CounterInteger(start, increment, loop, dimensions.init.init.init.init ++ Seq(mapRep) ++ dimensions.takeRight(4), repetitions.init.init.init.init ++ Seq(ArithType(1)) ++ repetitions.takeRight(4), Some(t.leafType))
  })

  def mergeSplitStream: Rule = Rule("mergeSplitStream", {
    case SplitOrderedStream(CounterInteger(start, increment, loop, dimensions, repetitions, _), chunkSize, t: OrderedStreamTypeT) =>
      CounterInteger(start, increment, loop, dimensions.init :+ chunkSize :+ ArithType(dimensions.last.ae / chunkSize.ae), repetitions :+ repetitions.last, Some(t.leafType))
  })

  def mergeMapSplitStream: Rule = Rule("mergeMapSplitStream", {
    case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), chunkSize, _), _), CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if p1.id == p2.id =>
      CounterInteger(start, increment, loop, dimensions.init.init :+ chunkSize :+ ArithType(dimensions.init.last.ae / chunkSize.ae) :+ dimensions.last, repetitions.init :+ repetitions.init.last :+ repetitions.last, Some(t.leafType))

    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, SplitOrderedStream(ParamUse(p3), chunkSize, _), _), ParamUse(p4), _), _), CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _) if p1.id == p4.id && p2.id == p3.id =>
      CounterInteger(start, increment, loop, dimensions.init.init.init :+ chunkSize :+ ArithType(dimensions.init.init.last.ae / chunkSize.ae) :+ dimensions.init.last :+ dimensions.last, repetitions.init.init :+ repetitions.init.init.last :+ repetitions.init.last :+ repetitions.last, Some(t.leafType))
  })

  def nDmergeSplitStream: Rule = Rule("mergeIntoCounter_nDmergeSplitStream", {
    case SplitOrderedStream(CounterIntegerND(start, loop, increments, dimensions, _), chunkSize, t: OrderedStreamTypeT) =>
      CounterIntegerND(start, loop, increments :+ ArithType(increments.last.ae * chunkSize.ae),
        dimensions.init :+ chunkSize :+ ArithType(dimensions.last.ae / chunkSize.ae),
        Some(t.leafType))

    case MapOrderedStream(fun: LambdaT, CounterIntegerND(start, loop, incs, dims, t), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val bitWidth = t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth
      var chunkSize: ArithTypeT = 0
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case SplitOrderedStream(ParamUse(p), cs, _) if p.id == innerMostFun.param.id =>
          chunkSize = cs
        case _ =>
      }
      val level = RulesHelper.getMapNDLevels(fun)
      val index = dims.length - 1 - level
      val numOfChunks = ArithType(dims(index).ae.evalInt / chunkSize.ae.evalInt)
      val newDims: Seq[ArithTypeT] = dims.take(index) ++ Seq(chunkSize, numOfChunks) ++ dims.takeRight(level)
      val newIncs: Seq[ArithTypeT] = incs.take(index + 1) ++ Seq(ArithType(incs(index).ae * chunkSize.ae)) ++ incs.takeRight(level)
      CounterIntegerND(start, newIncs, newDims, Some(IntType(bitWidth)))
  })

  def nDmergeRepeat: Rule = Rule("mergeIntoCounter_nDmergeRepeat", {
    case Repeat(CounterIntegerND(start, loop, incs, dims, _), times, t: OrderedStreamTypeT) =>
      val bitWidth = t.leafType.bitWidth
      val newDims: Seq[ArithTypeT] = dims :+ times
      val newIncs: Seq[ArithTypeT] = incs :+ ArithType(0)
      CounterIntegerND(start, loop, newIncs, newDims, Some(IntType(bitWidth)))

    case MapOrderedStream(fun: LambdaT, CounterIntegerND(start, loop, incs, dims, t), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case Repeat(ParamUse(p), times, _) if p.id == innerMostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val bitWidth = t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth
      var times: ArithTypeT = 0
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case Repeat(ParamUse(p), tms, _) if p.id == innerMostFun.param.id =>
          times = tms
        case _ =>
      }
      val level = RulesHelper.getMapNDLevels(fun)
      val index = dims.length - 1 - level
      val newInc = ArithType(0)
      val newDims: Seq[ArithTypeT] = dims.take(index + 1) ++ Seq(times) ++ dims.takeRight(level)
      val newIncs: Seq[ArithTypeT] = incs.take(index + 1) ++ Seq(newInc) ++ incs.takeRight(level)
      CounterIntegerND(start, loop, newIncs, newDims, Some(IntType(bitWidth)))
  })

  def splitRepeatSplitStream: Rule = Rule("splitRepeatSplitStream", {
    case MapOrderedStream(ArchLambda(param, body, _), input, _) if {
      // if all the ParamUses of the function's parameter are directly wrapped in a SplitOrderedStream
      var splits: Int = 0
      var paramUses: Int = 0
      var chunkSize: Option[Int] = None
      body.visit{
        case SplitOrderedStream(ParamUse(pd), cs, _) if pd.id == param.id && chunkSize.isEmpty =>
          splits = splits + 1
          chunkSize = Some(cs.ae.evalInt)
        case SplitOrderedStream(ParamUse(pd), cs, _) if pd.id == param.id && chunkSize.get == cs.ae.evalInt =>
          splits = splits + 1
        case ParamUse(pd) if pd.id == param.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapSplitOnly: Boolean = body match {
        case SplitOrderedStream(ParamUse(_), _, _) => true
        case _ => false
      }
      !isMapSplitOnly && splits > 0 && splits == paramUses
    } =>
      var chunkSize: ArithTypeT = 0
      val newParam = ParamDef()
      val newBody = {
        body.visitAndRebuild{
          case SplitOrderedStream(ParamUse(pd), cs, _) if pd.id == param.id =>
            chunkSize = cs
            ParamUse(newParam)
          case e => e
        }.asInstanceOf[Expr]
      }
      MapOrderedStream(
        ArchLambda(
          newParam,
          newBody
        ),
        MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), input)
      )
  })

  def mergeSlideGeneral: Rule = Rule("mergeIntoCounter_mergeSlideGeneral", {
    case SlideGeneralOrderedStream(CounterInteger(start, inc, loop, dim, rep, t), windowSize, stepSize, _) if
      rep.forall(_ == rep.head) && rep.head.ae.evalInt == 0 =>
      val bitWidth = t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth
      val incs = dim.map(_.ae.evalInt).scanLeft(inc.ae.evalInt)(_*_).map(e => ArithType(e)).init
      val numOfStep = ArithType((dim.last.ae.evalInt - windowSize.ae.evalInt) / stepSize.ae.evalInt + 1)
      val newStepSize = ArithType(stepSize.ae.evalInt * incs.last.ae.evalInt)
      val newDims: Seq[ArithTypeT] = dim.init ++ Seq(windowSize, numOfStep)
      val newIncs: Seq[ArithTypeT] = incs ++ Seq(newStepSize)
      CounterIntegerND(start, newIncs, newDims, Some(IntType(bitWidth)))

    case MapOrderedStream(fun: LambdaT, CounterIntegerND(start, loop, incs, dims, t), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case SlideGeneralOrderedStream(ParamUse(p), windowSize, stepSize, _) if p.id == innerMostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    }=>
      val bitWidth = t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth
      var windowSize: ArithTypeT = 0
      var stepSize: ArithTypeT = 0
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case SlideGeneralOrderedStream(ParamUse(p), ws, ss, _) if p.id == innerMostFun.param.id =>
          windowSize = ws
          stepSize = ss
        case _ =>
      }
      val level = RulesHelper.getMapNDLevels(fun)
      val index = dims.length - 1 - level
      val numOfStep = ArithType((dims(index).ae.evalInt - windowSize.ae.evalInt) / stepSize.ae.evalInt + 1)
      val newStepSize = ArithType(stepSize.ae.evalInt * incs(index).ae.evalInt)
      val newDims: Seq[ArithTypeT] = dims.take(index) ++ Seq(windowSize, numOfStep) ++ dims.takeRight(level)
      val newIncs: Seq[ArithTypeT] = incs.take(index + 1) ++ Seq(newStepSize) ++ incs.takeRight(level)
      CounterIntegerND(start, newIncs, newDims, Some(IntType(bitWidth)))
  })

  def mergeTransposeND: Rule = Rule("mergeIntoCounter_mergeTransposeND", {
    case TransposeNDOrderedStream(CounterIntegerND(start, loop, incs , dims, t), dimOrder, _) =>
      val bitWidth = t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth
      val newIncs = dimOrder.map(e => incs(e.ae.evalInt))
      val newDims = dimOrder.map(e => dims(e.ae.evalInt))
      CounterIntegerND(start, newIncs, newDims, Some(IntType(bitWidth)))
  })
}