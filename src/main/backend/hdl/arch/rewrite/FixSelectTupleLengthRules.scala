package backend.hdl.arch.rewrite

import backend.hdl.{IntType, OrderedStreamTypeT, VectorTypeT}
import backend.hdl.arch.{ArchLambda, ConcatOrderedStream, ConstantValue, DropOrderedStream, MapOrderedStream, Repeat, Select2, Tuple2, VectorGenerator, Zip2OrderedStream}
import core.{Expr, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

/**
 * Rule set that aims at fixing Tuple of streams with different lengths.
 */
object FixSelectTupleLengthRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] = Seq(
    padSelectTupleInputs
  )

  def padSelectTupleInputs: Rule = Rule("fixSelect", {
    /**
     * Fix the problem with Map(Lambda(p, sth ... Select2(param, sel)), ZipStm(Tuple2(in0, in1))).
     * If the inner stream sizes of in0 and in1 are different, the hardware might get stuck or generate incorrect rules.
     * This rule pads the short one and then drop the padding elements after Select.
     * We have 2 cases for both in0.t.len.et.len > in1.t.len.et.len and in0.t.len.et.len < in1.t.len.et.len.
     */
    case MapOrderedStream(ArchLambda(p1, body, _), Zip2OrderedStream(Tuple2(in0, in1, _), _), _) if {
      var exprFound = false
      var in0Size: Int = 0
      var in1Size: Int = 0
      body.visit{
        case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 0 =>
          in0Size = t.len.ae.evalInt
          exprFound = true
        case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 1 =>
          in1Size = t.len.ae.evalInt
          exprFound = true
        case _ =>
      }
      exprFound && in0Size > in1Size && in1.t.asInstanceOf[OrderedStreamTypeT].leafType.isInstanceOf[VectorTypeT]
    } =>
      var in0Size: Int = 0
      var in1Size: Int = 0
      body.visit{
        case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 0 =>
          in0Size = t.len.ae.evalInt
        case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 1 =>
          in1Size = t.len.ae.evalInt
        case _ =>
      }
      val in1Bits = in1.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].et.bitWidth
      val in1Const = VectorGenerator(ConstantValue(0, Some(IntType(in1Bits))), in1.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].len)
      val in1rep = TypeChecker.check(Repeat(in1Const, in0Size - in1Size))
      val newIn1 = MapOrderedStream(
        {
          val param = ParamDef(in1.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            ConcatOrderedStream(ParamUse(param), in1rep)
          )
        }, in1
      )
      val newZip = TypeChecker.check(Zip2OrderedStream(Tuple2(in0, newIn1)))
      MapOrderedStream(
        {
          val param = ParamDef(newZip.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            body.visitAndRebuild{
              case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 0 =>
                Select2(ParamUse(param), sel)
              case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 1 =>
                DropOrderedStream(Select2(ParamUse(param), sel),  0, in0Size - in1Size)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newZip
      )

    case MapOrderedStream(ArchLambda(p1, body, _), Zip2OrderedStream(Tuple2(in0, in1, _), _), _) if {
      var exprFound = false
      var in0Size: Int = 0
      var in1Size: Int = 0
      body.visit{
        case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 0 =>
          in0Size = t.len.ae.evalInt
          exprFound = true
        case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 1 =>
          in1Size = t.len.ae.evalInt
          exprFound = true
        case _ =>
      }
      exprFound && in0Size < in1Size && in0.t.asInstanceOf[OrderedStreamTypeT].leafType.isInstanceOf[VectorTypeT]
    } =>
      var in0Size: Int = 0
      var in1Size: Int = 0
      body.visit{
        case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 0 =>
          in0Size = t.len.ae.evalInt
        case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 1 =>
          in1Size = t.len.ae.evalInt
        case _ =>
      }
      val in0Bits = in1.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].et.bitWidth
      val in0Const = VectorGenerator(ConstantValue(0, Some(IntType(in0Bits))), in0.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].len)
      val in0rep = TypeChecker.check(Repeat(in0Const, in1Size - in0Size))
      val newIn0 = MapOrderedStream(
        {
          val param = ParamDef(in0.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            ConcatOrderedStream(ParamUse(param), in0rep)
          )
        }, in0
      )
      val newZip = TypeChecker.check(Zip2OrderedStream(Tuple2(newIn0, in1)))
      MapOrderedStream(
        {
          val param = ParamDef(newZip.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            body.visitAndRebuild{
              case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 0 =>
                DropOrderedStream(Select2(ParamUse(param), sel),  0, in1Size - in0Size)
              case Select2(ParamUse(p2), sel, t:OrderedStreamTypeT) if p1.id == p2.id && sel.ae.evalInt == 1 =>
                Select2(ParamUse(param), sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newZip
      )
  })
}