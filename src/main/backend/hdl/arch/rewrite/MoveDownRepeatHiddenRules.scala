package backend.hdl.arch.rewrite

import backend.hdl.arch.ArchHelper.{getLambdaBody, getLambdaParams}
import backend.hdl.arch._
import backend.hdl.arch.mem.{BlockRamBuffer, WriteAsync, WriteAsyncOutOfBounds}
import core._
import core.rewrite.{Rule, RulesT}

object MoveDownRepeatHiddenRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(skipExprs, moveIntoHigherOrderBuiltins, cancelInverseRepeatHidden, removeRepeatHiddenOnFunctionParam, moveRepeatHiddenIntoLet)

  def skipExprs: Rule = Rule("moveDownRepeatHidden_skipExprs", {
    case RepeatHidden(Marker(input, text), repetitions, _) =>
      Marker(RepeatHidden(input, repetitions), text)
    case RepeatHidden(Conversion(input, t), repetitions, _) =>
      Conversion(RepeatHidden(input, repetitions), t)
    case RepeatHidden(Let(param, body, arg, _), repetitions, _) =>
      Let(param, RepeatHidden(body, repetitions), arg)
    case RepeatHidden(FunctionCall(ParamUse(pf), input, _), repetitions, _) =>
      FunctionCall(ParamUse(pf), RepeatHidden(input, repetitions))
    case RepeatHidden(FunctionCall(FunctionCall(ParamUse(pf), input1, _), input2, _), repetitions, _) =>
      FunctionCall(FunctionCall(ParamUse(pf), RepeatHidden(input1, repetitions)), RepeatHidden(input2, repetitions))
    case RepeatHidden(FunctionCall(FunctionCall(FunctionCall(ParamUse(pf), input1, _), input2, _), input3, _), repetitions, _) =>
      FunctionCall(FunctionCall(FunctionCall(ParamUse(pf), RepeatHidden(input1, repetitions)), RepeatHidden(input2, repetitions)), RepeatHidden(input3, repetitions))
    case RepeatHidden(FunctionCall(FunctionCall(FunctionCall(FunctionCall(ParamUse(pf), input1, _), input2, _), input3, _), input4, _), repetitions, _) =>
      FunctionCall(FunctionCall(FunctionCall(FunctionCall(ParamUse(pf), RepeatHidden(input1, repetitions)), RepeatHidden(input2, repetitions)), RepeatHidden(input3, repetitions)), RepeatHidden(input4, repetitions))
    case RepeatHidden(be: BuiltinExpr, repetitions, _) if be.args.nonEmpty && (be match {
      // prevent loops
      case RepeatHidden(_, _, _) => false
      // do not skip InverseRepeatHidden
      case InverseRepeatHidden(_, _, _) => false
      // do not process higher order builtins
      case Ifelse(_, _, _, _) => false
      case Alternate(_, _, _, _) => false
      case MapVector(_, _, _) => false
      case MapOrderedStream(_, _, _) => false
      case MapUnorderedStream(_, _, _) => false
      case MapAsyncOrderedStream(_, _, _) => false
      case FoldOrderedStream(_, _, _) => false
      case ReduceOrderedStream(_, _, _, _) => false
      case WriteAsync(_, _, _, _) => false
      case WriteAsyncOutOfBounds(_, _, _, _, _) => false
      case _ => true
    }) =>
      be.replaceArgs(be.args.map(RepeatHidden(_, repetitions)))
  })

  def moveIntoHigherOrderBuiltins: Rule = Rule("moveDownRepeatHidden_moveIntoHigherOrderBuiltins", {
    case RepeatHidden(Alternate(f1, f2, input, _), repetitions, _) if {
      // RepeatHidden should go to double-buffer's address instead of its input.
      // This avoids redundant write accesses to the buffers.
      var f1Found = false
      var f2Found = false
      f1.visit { case BlockRamBuffer(_, _, _, _) => f1Found = true case _ => }
      f2.visit { case BlockRamBuffer(_, _, _, _) => f2Found = true case _ => }
      !(f1Found && f2Found)
    } =>
      Alternate(f1, f2, RepeatHidden(input, repetitions))
    case RepeatHidden(Ifelse(f1, f2, input, _), repetitions, _) =>
      Ifelse(f1, f2, RepeatHidden(input, repetitions))
    case RepeatHidden(MapVector(ArchLambda(p, body, _), input, _), repetitions, _) =>
      MapVector(
        ArchLambda(p,
          RepeatHidden(
            body.visitAndRebuild{
              case ParamUse(pd) if pd.id == p.id => InverseRepeatHidden(ParamUse(pd), repetitions)
              case e => e
            }.asInstanceOf[Expr], repetitions)
        ),
        RepeatHidden(input, repetitions)
      )
    case RepeatHidden(MapOrderedStream(ArchLambda(p, body, _), input, _), repetitions, _) =>
      MapOrderedStream(
        ArchLambda(p,
          RepeatHidden(
            body.visitAndRebuild{
              case ParamUse(pd) if pd.id == p.id => InverseRepeatHidden(ParamUse(pd), repetitions)
              case e => e
            }.asInstanceOf[Expr], repetitions)
        ),
        RepeatHidden(input, repetitions)
      )
    case RepeatHidden(MapUnorderedStream(ArchLambda(p, body, _), input, _), repetitions, _) =>
      MapUnorderedStream(
        ArchLambda(p,
          RepeatHidden(
            body.visitAndRebuild{
              case ParamUse(pd) if pd.id == p.id => InverseRepeatHidden(ParamUse(pd), repetitions)
              case e => e
            }.asInstanceOf[Expr], repetitions)
        ),
        RepeatHidden(input, repetitions)
      )
    case RepeatHidden(MapAsyncOrderedStream(f, inputs, _), repetitions, _) =>
      val params = getLambdaParams(f)
      val body = getLambdaBody(f)
      val newInputs = inputs.map(i => RepeatHidden(i, repetitions))
      val newBody = body.visitAndRebuild {
        case ParamUse(pd) if params.contains(pd.id) => InverseRepeatHidden(ParamUse(pd), repetitions)
        case e => e
      }.asInstanceOf[Expr]
      MapAsyncOrderedStream(ArchLambdas(params, RepeatHidden(newBody, repetitions)), newInputs: _*)

    case RepeatHidden(fold @ FoldOrderedStream(ArchLambda(p1, ArchLambda(p2, body, _), _), input, _), repetitions, _) if {
      var invRepFound = false
      var repFound = false
      fold.visit {
        case InverseRepeatHidden(_, reps, _) if reps == repetitions => invRepFound = true
        case RepeatHidden(_, _, _) => repFound = true // must not rewrite if there is another RepeatHidden present inside of the Fold
        case _ =>
      }
      invRepFound // && !repFound
    } =>
      FoldOrderedStream(
        ArchLambda(p1, ArchLambda(p2,
          RepeatHidden(
            body.visitAndRebuild{
              case ParamUse(pd) if pd.id == p1.id || pd.id == p2.id => InverseRepeatHidden(ParamUse(pd), repetitions)
              case e => e
            }.asInstanceOf[Expr], repetitions)
        )),
        RepeatHidden(input, repetitions)
      )
    case RepeatHidden(reduce @ ReduceOrderedStream(ArchLambda(p1, ArchLambda(p2, body, _), _), initialValue, input, _), repetitions, _) if {
      var invRepFound = false
      var repFound = false
      reduce.visit {
        case InverseRepeatHidden(_, reps, _) if reps == repetitions => invRepFound = true
        case RepeatHidden(_, _, _) => repFound = true // must not rewrite if there is another RepeatHidden present inside of the Reduce
        case _ =>
      }
      invRepFound && !repFound
    } =>
      ReduceOrderedStream(
        ArchLambda(p1, ArchLambda(p2,
          RepeatHidden(
            body.visitAndRebuild{
              case ParamUse(pd) if pd.id == p1.id || pd.id == p2.id => InverseRepeatHidden(ParamUse(pd), repetitions)
              case e => e
            }.asInstanceOf[Expr], repetitions)
        )),
        RepeatHidden(initialValue, repetitions),
        RepeatHidden(input, repetitions)
      )
  })

  def cancelInverseRepeatHidden: Rule = Rule("moveDownRepeatHidden_cancelInverseRepeatHidden", {
    case RepeatHidden(InverseRepeatHidden(input, reps1, _), resp2, _) if reps1 == resp2 =>
      input
  })

  def removeRepeatHiddenOnFunctionParam: Rule = Rule("moveDownRepeatHidden_removeRepeatHiddenOnFunctionParam", {
    case RepeatHidden(pu @ ParamUse(_), _, _) if (pu.t match {
      case _: FunTypeT => true
      case _ => false
    }) =>
      pu
  })

  def moveRepeatHiddenIntoLet: Rule = Rule("moveDownRepeatHiddenIntoLet", {
    case Let(param, body, arg, _) if {
      // if all the ParamUses of the function's parameter are directly wrapped in a RepeatHidden
      var repeatHiddens: Int = 0
      var paramUses: Int = 0
      body.visit{
        case RepeatHidden(ParamUse(pd), _, _) if pd.id == param.id && (pd.t match {
          case _: FunTypeT => false
          case _ => true
        }) =>
          repeatHiddens = repeatHiddens + 1
        case ParamUse(pd) if pd.id == param.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      repeatHiddens > 0 && repeatHiddens == paramUses
    } =>
      var repetitions: ArithTypeT = ArithType(1)
      val newParam = ParamDef()
      val newBody =
        body.visitAndRebuild{
          case RepeatHidden(ParamUse(pd), reps, _) if pd.id == param.id =>
            repetitions = reps
            ParamUse(newParam)
          case e => e
        }.asInstanceOf[Expr]
      Let(
        newParam,
        newBody,
        RepeatHidden(arg, repetitions)
      )
  })
}
