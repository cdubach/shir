package backend.hdl.arch.rewrite

import backend.hdl.NamedTupleTypeT
import backend.hdl.arch.mem.{BlockRamBuffer, ReadAsyncOrdered, ReadSync}
import backend.hdl.arch._
import core.rewrite.{Rule, RulesT}
import core._
import core.util.IRDotGraph

object MoveDownRepeatRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(moveDownMapRepeatParamUse, moveDownMapNDRepeatParamUse, moveRepeatIntoLet, moveMapRepeatIntoLet, moveMapNDRepeatIntoLet, skipExpr,
      mapNDFusion, moveDownRepeat4, skipMarker, reorderRepeats, removeRepeatTupleType,
      moveDownRepeatIntoReadAddress, skipSelectMapZip, untupleMapRepeatZipInput, skipSelectMapZipRepeatND, skipSplit) ++ RepeatBufferingRules.get() ++
      MoveUpZipStreamRules.get()//, removeRepeat)

  private def moveDownMapRepeatParamUse: Rule = Rule("moveDownMapRepeatParamUse", {
    case MapOrderedStream(ArchLambda(param, body, _), input, _) if {
      // if all the ParamUses of the function's parameter are directly wrapped in a Repeat with the same N
      var reps: Seq[ArithTypeT] = Seq()
      var paramUses: Int = 0
      body.visit{
        case Repeat(ParamUse(pd), repetitions, _) if pd.id == param.id =>
          reps = reps :+ repetitions
        case ParamUse(pd) if pd.id == param.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapRepeatOnly: Boolean = body match {
        case Repeat(ParamUse(_), _, _) => true
        case _ => false
      }
      !isMapRepeatOnly && reps.nonEmpty && reps.forall(_ == reps.head) && paramUses == reps.size
    } =>
      var reps: ArithTypeT = 0
      val newFun = {
        val newParam = ParamDef()
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case Repeat(ParamUse(pd), repetitions, _) if pd.id == param.id => // Avoid removing multiple repeats
              reps = repetitions
              ParamUse(newParam)
            case e => e
          }.asInstanceOf[Expr]
        )
      }

      MapOrderedStream(
        newFun,
        MapOrderedStream(Repeat.asFunction(Seq(None), Seq(reps)), input)
      )
  })

  private def moveDownMapNDRepeatParamUse: Rule = Rule("moveDownMapNDRepeatParamUse", {
    case MapOrderedStream(ArchLambda(param, body, _), input, _) if {
      // if all the ParamUses of the function's parameter are directly wrapped in a Repeat with the same N
      var reps: Seq[ArithTypeT] = Seq()
      var levels: Seq[Int] = Seq()
      var paramUses: Int = 0
      body.visit{
        //case MapOrderedStream(ArchLambda(p1, Repeat(ParamUse(pd), repetitions, _), _), ParamUse(p2), _) if p1.id ==pd.id && p2.id == param.id =>
        case MapOrderedStream(repFun: LambdaT, ParamUse(p1), _) if p1.id == param.id && {
          var exprFound = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
          innerMostFun.body match{
            case Repeat(ParamUse(p2), repetitions, _) if p2.id == innerMostFun.param.id =>
              levels = levels :+ RulesHelper.getMapNDLevels(repFun)
              reps = reps :+ repetitions
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
        case ParamUse(pd) if pd.id == param.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapRepeatOnly: Boolean = body match {
        case MapOrderedStream(repFun: LambdaT, ParamUse(p1), _) if p1.id == param.id && {
          var exprFound = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
          innerMostFun.body match {
            case Repeat(ParamUse(p2), _, _) if p2.id == innerMostFun.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          true
        case _ => false
      }
      !isMapRepeatOnly && reps.nonEmpty && reps.forall(_ == reps.head) && levels.forall(_ == levels.head) && paramUses == reps.size
    } =>
      var reps: ArithTypeT = 0
      var levels: Int = 0
      val newFun = {
        val newParam = ParamDef()
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case MapOrderedStream(repFun: LambdaT, ParamUse(p1), _) if p1.id == param.id && {
              var exprFound = false
              val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
              innerMostFun.body match {
                case Repeat(ParamUse(p2), repetitions, _) if p2.id == innerMostFun.param.id =>
                  reps = repetitions
                  levels = RulesHelper.getMapNDLevels(repFun)
                  exprFound = true
                case _ =>
              }
              exprFound
            } =>
              // Avoid removing multiple repeats
              ParamUse(newParam)
            case e => e
          }.asInstanceOf[Expr]
        )
      }

      MapOrderedStream(
        newFun,
        MapOrderedStream(levels+1, Repeat.asFunction(Seq(None), Seq(reps)), input)
      )
  })

  private def moveRepeatIntoLet: Rule = Rule("moveRepeatIntoLet", {
    case Let(param, body, arg, _) if {
      // if all the ParamUses of the function's parameter are directly wrapped in a Repeat with the same N
      var reps: Seq[ArithTypeT] = Seq()
      var paramUses: Int = 0
      body.visit{
        case Repeat(ParamUse(pd), repetitions, _) if pd.id == param.id =>
          reps = reps :+ repetitions
        case ParamUse(pd) if pd.id == param.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      reps.nonEmpty && reps.forall(_ == reps.head) && paramUses == reps.size
    } =>
      var reps: ArithTypeT = 0
      val newParam = ParamDef()
      val newBody = {
        body.visitAndRebuild{
          case Repeat(ParamUse(pd), repetitions, _) if pd.id == param.id || pd.id == newParam.id =>
            reps = repetitions
            ParamUse(newParam)
          case e => e
        }.asInstanceOf[Expr]
      }
      val newArg = Repeat(arg, reps)
      Let(newParam, newBody, newArg)
  })

  private def moveMapRepeatIntoLet: Rule = Rule("moveMapRepeatIntoLet", {
    case Let(param, body, arg, _) if {
      // if all the ParamUses of the function's parameter are directly wrapped in a Map-Repeat with the same N
      var reps: Seq[ArithTypeT] = Seq()
      var paramUses: Int = 0
      body.visit{
        case MapOrderedStream(ArchLambda(p1, Repeat(ParamUse(p2), repetitions, _), _), ParamUse(pd), _) if pd.id == param.id && p1.id == p2.id =>
          reps = reps :+ repetitions
        case ParamUse(pd) if pd.id == param.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      reps.nonEmpty && reps.forall(_ == reps.head) && paramUses == reps.size
    } =>
      var reps: ArithTypeT = 0
      val newParam = ParamDef()
      val newBody = {
        body.visitAndRebuild{
          case MapOrderedStream(ArchLambda(p1, Repeat(ParamUse(p2), repetitions, _), _), ParamUse(pd), _) if pd.id == param.id && p1.id == p2.id =>
            reps = repetitions
            ParamUse(newParam)
          case e => e
        }.asInstanceOf[Expr]
      }
      val newArg = MapOrderedStream(Repeat.asFunction(Seq(None), Seq(reps)), arg)
      Let(newParam, newBody, newArg)
  })

  private def moveMapNDRepeatIntoLet: Rule = Rule("moveMapNDRepeatIntoLet", {
    case Let(param, body, arg, _) if {
      // if all the ParamUses of the function's parameter are directly wrapped in a Map-Repeat with the same N
      var reps: Seq[ArithTypeT] = Seq()
      var levels: Seq[Int] = Seq()
      var paramUses: Int = 0
      body.visit{
        case MapOrderedStream(repFun: LambdaT, ParamUse(pd), _) if pd.id == param.id && {
          var exprFound = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
          innerMostFun.body match {
            case Repeat(ParamUse(p2), repetitions, _) if p2.id == innerMostFun.param.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
          innerMostFun.body match {
            case Repeat(ParamUse(p2), repetitions, _) if p2.id == innerMostFun.param.id =>
              reps = reps :+ repetitions
            case _ =>
          }
          levels = levels :+ RulesHelper.getMapNDLevels(repFun)
        case ParamUse(pd) if pd.id == param.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      reps.nonEmpty && reps.forall(_ == reps.head) && levels.forall(_ == levels.head) && paramUses == reps.size
    } =>
      var reps: ArithTypeT = 0
      var levels: Int = 0
      val newParam = ParamDef()
      val newBody = {
        body.visitAndRebuild{
          case MapOrderedStream(repFun: LambdaT, ParamUse(pd), _) if pd.id == param.id && {
            var exprFound = false
            val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
            innerMostFun.body match {
              case Repeat(ParamUse(p2), repetitions, _) if p2.id == innerMostFun.param.id =>
                exprFound = true
              case _ =>
            }
            exprFound
          } =>
            val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
            innerMostFun.body match {
              case Repeat(ParamUse(p2), repetitions, _) if p2.id == innerMostFun.param.id =>
                reps = repetitions
              case _ =>
            }
            levels = RulesHelper.getMapNDLevels(repFun)
            ParamUse(newParam)
          case e => e
        }.asInstanceOf[Expr]
      }
      val newArg = MapOrderedStream(levels, Repeat.asFunction(Seq(None), Seq(reps)), arg)
      Let(newParam, newBody, newArg)
  })

  private def skipExpr: Rule = Rule("skipExpr", {
    case Repeat(Unsigned(input, _), repetitions, _) =>
      MapOrderedStream(Unsigned.asFunction(), Repeat(input, repetitions))
    case Repeat(DropVector(input, lowElements, highElements, _), repetitions, _) =>
      MapOrderedStream(DropVector.asFunction(Seq(None), Seq(lowElements, highElements)), Repeat(input, repetitions))
    case Repeat(AddInt(input, _), repetitions, _) =>
      MapOrderedStream(AddInt.asFunction(), Repeat(input, repetitions))
    case Repeat(SubInt(input, _), repetitions, _) =>
      MapOrderedStream(SubInt.asFunction(), Repeat(input, repetitions))
    case Repeat(VectorToTuple2(input, _), repetitions, _) =>
      MapOrderedStream(VectorToTuple2.asFunction(), Repeat(input, repetitions))
    case Repeat(OrderedStreamToVector(input, _), repetitions, _) =>
      MapOrderedStream(OrderedStreamToVector.asFunction(), Repeat(input, repetitions))
    case Repeat(VectorToOrderedStream(input, _), repetitions, _) =>
      MapOrderedStream(VectorToOrderedStream.asFunction(), Repeat(input, repetitions))
    case MapOrderedStream(fun @ ArchLambda(p1, Repeat(ParamUse(p2), _, _), _), RepeatHidden(input, repetitions, _), _) if p1.id == p2.id =>
      RepeatHidden(MapOrderedStream(fun, input), repetitions)
    case MapOrderedStream(fun @ ArchLambda(p1, Repeat(ParamUse(p2), _, _), _), Marker(input, text), _) if p1.id == p2.id =>
      Marker(MapOrderedStream(fun, input), text)
    case Repeat(RepeatHidden(input, hiddenReps, _), reps, _) =>
      RepeatHidden(Repeat(input, reps), hiddenReps)
    case Repeat(MapOrderedStream(fun, input, _), repetitions, _) =>
      MapOrderedStream(MapOrderedStream.asFunction(Seq(Some(fun), None)), Repeat(input, repetitions))
    case Repeat(JoinOrderedStream(input, _), repetitions, _) =>
      MapOrderedStream(JoinOrderedStream.asFunction(), Repeat(input, repetitions))
    case Repeat(JoinVector(input, _), repetitions, _) =>
      MapOrderedStream(JoinVector.asFunction(), Repeat(input, repetitions))
    case Repeat(SplitOrderedStream(input, chunkSize, _), repetitions, _) =>
      MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), Repeat(input, repetitions))
    case Repeat(SplitVector(input, chunkSize, _), repetitions, _) =>
      MapOrderedStream(SplitVector.asFunction(Seq(None), Seq(chunkSize)), Repeat(input, repetitions))
    case Repeat(PermuteVector(input, rule, _), repetitions, _) =>
      MapOrderedStream(PermuteVector.asFunction(Seq(None), Seq(rule)), Repeat(input, repetitions))
    case Repeat(VectorGenerator(input, len, _), repetitions, _) =>
      MapOrderedStream(VectorGenerator.asFunction(Seq(None), Seq(len)), Repeat(input, repetitions))
    case Repeat(ConcatOrderedStream(input1, input2, dim, _), times, _) =>
      ConcatOrderedStream(Repeat(input1, times), Repeat(input2, times), dim.ae + 1)
    case Repeat(Let(param, body, arg, _), repetitions, _) =>
      Let(param, Repeat(body, repetitions), arg)
    case MapOrderedStream(fun @ ArchLambda(p1, Repeat(ParamUse(p2), _, _), _), Let(param, body, arg, _), _) if p1.id == p2.id =>
      Let(param, MapOrderedStream(fun, body), arg)
    case MapOrderedStream(ArchLambda(p1, Repeat(ParamUse(p2), repetitions, _), _), ZipOrderedStream(Tuple(inputs, _), n, _), _) if p1.id == p2.id =>
      MapOrderedStream(
        ZipOrderedStream.asFunction(Seq(None), Seq(n)),
        ZipOrderedStream(Tuple(inputs.map(
          MapOrderedStream(Repeat.asFunction(Seq(None), Seq(repetitions)), _)) : _*), n)
      )
    case Repeat(Alternate(f1, f2, input, _), repetitions, _) if {
      var exprFound = false
      f1 match {
        case ArchLambda(_, OrderedStreamToVector(_, _), _) => exprFound = true
        case _ =>
      }
      !exprFound
    } && {
      var f1Found = false
      var f2Found = false
      f1.visit{case BlockRamBuffer(_, _, _, _) => f1Found = true case _ => }
      f2.visit{case BlockRamBuffer(_, _, _, _) => f2Found = true case _ => }
      !(f1Found && f2Found)
    } =>
      MapOrderedStream(Alternate.asFunction(Seq(Some(f1), Some(f2), None)), Repeat(input, repetitions))
    case Repeat(Tuple(inputs, _), repetitions, _) =>
      ZipOrderedStream(Tuple(inputs.map(Repeat(_, repetitions)): _*), inputs.size)
    case Repeat(ZipOrderedStream(input, n, _), repetitions, _) =>
      MapOrderedStream(ZipOrderedStream.asFunction(Seq(None), Seq(n)), Repeat(input, repetitions))
    //case Repeat(Alternate(f1, f2, input, _), repetitions, _) =>
    //  MapOrderedStream(Alternate.asFunction(Seq(Some(f1), Some(f2), None)), Repeat(input, repetitions))
    case Repeat(ConcatVector(input, _), repetitions, _) =>
      MapOrderedStream(ConcatVector.asFunction(), Repeat(input, repetitions))
    case Repeat(Conversion(input, t), repetitions, _) =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(param, Conversion(ParamUse(param), t))
        },
        Repeat(input, repetitions)
      )
    case MapOrderedStream(ArchLambda(p1, Repeat(ParamUse(p2), times, _), _), Conversion(input, t), _) if p1.id == p2.id =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(param, Conversion(ParamUse(param), t))
        },
        MapOrderedStream(Repeat.asFunction(Seq(None), Seq(times)), input)
      )
  })

  private def moveDownRepeat3: Rule = Rule("moveDownRepeat3", {
    // map fusion
    case MapOrderedStream(ArchLambda(p1, Repeat(ParamUse(p2), repetitions, _), _), MapOrderedStream(ArchLambda(param, body, _), input, _), _) if {
      // Avoid looping with multiple-repeat-removal guard in moveDownMapRepeatParamUse
      val isMapRepeatOnly: Boolean = body match {
        case Repeat(ParamUse(_), _, _) => true
        case _ => false
      }
      !isMapRepeatOnly
    } && p1.id == p2.id =>
      MapOrderedStream(
        {
          ArchLambda(
            param,
            Repeat(body, repetitions)
          )
        },
        input
      )
  })

  private def mapNDFusion: Rule = Rule("mapNDFusion", {
    // map ND fusion
    case MapOrderedStream(repFun: LambdaT, MapOrderedStream(ArchLambda(param, body, _), input, _), _) if {//ArchLambda(p1, Repeat(ParamUse(p2), repetitions, _), _)
      // Avoid looping with multiple-repeat-removal guard in moveDownMapRepeatParamUse
      val isMapRepeatOnly: Boolean = body match {
        case Repeat(ParamUse(_), _, _) => true
        case MapOrderedStream(innerRepFun: LambdaT, _,  _) if {
          var exprFound = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(innerRepFun)
          innerMostFun.body match {
            case Repeat(ParamUse(p), _, _) if p.id == innerMostFun.param.id => exprFound = true
            case _ =>
          }
          exprFound
        } => true
        case _ => false
      }
      !isMapRepeatOnly
    } && {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
      innerMostFun.body match{
        case Repeat(ParamUse(p), _, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val funLevels = RulesHelper.getMapNDLevels(repFun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
      MapOrderedStream(
        {
          ArchLambda(
            param,
            MapOrderedStream(funLevels - 1, innerMostFun, body)
          )
        },
        input
      )
  })

  private def moveDownRepeat4: Rule = Rule("moveDownRepeat4", {
    case ArchLambda(p1: ParamDef, Repeat(ReadAsyncOrdered(readMemoryController, Select2(ParamUse(p2), sel1, _), Select2(ParamUse(p3), sel2, _), _), repetitions, _), _) if p1.id == p2.id  && p1.id == p3.id =>
      ArchLambda(
        p1,
        MapOrderedStream(
          {
            val param = ParamDef()
            ArchLambda(
              param,
              ReadAsyncOrdered(
                readMemoryController,
                Select2(ParamUse(param), sel1),
                Select2(ParamUse(param), sel2)
              )
            )
          },
          Repeat(ParamUse(p1), repetitions)
        )
      )
  })

  private def skipMarker: Rule = Rule("skipMarker", {
    case Repeat(Marker(input, ts), times, _) => Marker(Repeat(input, times), ts)
    case MapOrderedStream(repFun: LambdaT, Marker(input, text), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
      innerMostFun.body match{
        case Repeat(ParamUse(p), _, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      Marker(MapOrderedStream(repFun, input), text)

    case MapOrderedStream(repFun: LambdaT, Let(param, body, arg, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
      innerMostFun.body match{
        case Repeat(ParamUse(p), _, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      Let(param, MapOrderedStream(repFun, body), arg)
  })

  private def getConsecutiveRepeats(expr: Expr, count: Int): Seq[ArithTypeT] = expr match {
    case Repeat(input, rep, _) if count > 0 => getConsecutiveRepeats(input, count - 1) :+ rep
    case _ => Seq()
  }

  private def getConsecutiveRepeatsInput(expr: Expr, count: Int): Expr = expr match {
    case Repeat(input, _, _) if count > 0 => getConsecutiveRepeatsInput(input, count - 1)
    case e => e
  }

  private def rebuildRepeat(input: Expr, repSeq: Seq[ArithTypeT]): Expr =
    if(repSeq.isEmpty)
      input
    else
      rebuildRepeat(Repeat(input, repSeq.head), repSeq.tail)


  private def reorderRepeats: Rule = Rule("reorderRepeats", {
    case MapOrderedStream(ArchLambda(p1: ParamDef, Repeat(ParamUse(p2), reps1, _), _), Repeat(input, reps2, _), _) if p1.id == p2.id =>
      Repeat(Repeat(input, reps1), reps2)
    case MapOrderedStream(ArchLambda(p1: ParamDef, MapOrderedStream(ArchLambda(p2, Repeat(ParamUse(p3), reps1, _), _), ParamUse(p4), _), _), Repeat(Repeat(input, reps2, _), reps3, _), _) if p1.id == p4.id && p2.id == p3.id =>
      Repeat(Repeat(Repeat(input, reps1), reps2), reps3)
    case MapOrderedStream(ArchLambda(p1: ParamDef, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, Repeat(ParamUse(p4), reps1, _), _), ParamUse(p5), _), _), ParamUse(p6), _), _),
    Repeat(Repeat(Repeat(input, reps2, _), reps3, _), reps4, _), _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id  =>
      Repeat(Repeat(Repeat(Repeat(input, reps1), reps2), reps3), reps4)
    case MapOrderedStream(ArchLambda(p1: ParamDef, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p7, Repeat(ParamUse(p8), reps1, _), _), ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _),
    Repeat(Repeat(Repeat(Repeat(input, reps2, _), reps3, _), reps4, _), reps5, _), _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && p7.id == p8.id  =>
      Repeat(Repeat(Repeat(Repeat(Repeat(input, reps1), reps2), reps3), reps4), reps5)
    case MapOrderedStream(repeatFun: LambdaT, input @ Repeat(_, _, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repeatFun)
      innerMostFun.body match {
        case Repeat(ParamUse(p), rep2, _) if innerMostFun.param.id == p.id => exprFound = true
        case _ =>
      }
      if(exprFound) {
        val levels = RulesHelper.getMapNDLevels(repeatFun)
        val repLen = getConsecutiveRepeats(input, levels).length
        exprFound = repLen == levels
      }
      exprFound
    } =>
      val levels = RulesHelper.getMapNDLevels(repeatFun)
      var repeatSeq = getConsecutiveRepeats(input, levels)
      var rep: ArithTypeT = 0
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repeatFun)
      innerMostFun.body match {
        case Repeat(ParamUse(p), rep2, _) if innerMostFun.param.id == p.id => rep = rep2
        case _ =>
      }
      repeatSeq = rep +: repeatSeq
      val origInput = getConsecutiveRepeatsInput(input, levels)
      rebuildRepeat(origInput, repeatSeq)
  })

  private def removeRepeatTupleType: Rule = Rule("removeRepeatTupleType", {
    case MapOrderedStream(ArchLambda(p1, body, _), Repeat(input, times, _), _) if input.t.isInstanceOf[NamedTupleTypeT] && {
      var exprFound = false
      body.visit{
        case Select2(ParamUse(p2), sel, _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      body match {
        case ReadAsyncOrdered(_, _, _, _) => exprFound = true
        case ReadSync(_, _, _, _) => exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      Repeat(
        body.visitAndRebuild{
          case Select2(ParamUse(p2), sel, _) if p1.id == p2.id =>
            Select2(input, sel)
          case e => e
        }.asInstanceOf[Expr],
        times
      )
  })

  private def moveDownRepeatIntoReadAddress: Rule = Rule("moveDownRepeatIntoReadAddress", {
    case Repeat(SplitOrderedStream(Let(p1, Let(p2, Let(p3, Let(p4, Let(p5,
    MapOrderedStream(ArchLambda(p6, ReadSync(ParamUse(p7), Select2(ParamUse(p8), sel1, _), Select2(ParamUse(p9), sel2, _), _), _), Zip2OrderedStream(Tuple2(rAddr,
    ParamUse(p10), _), _), _) , a5, _), a4, _), a3, _), a2, _), a1, _), chunkSize, _), times, _)  if p5.id == p10.id && p2.id == p7.id && p6.id == p8.id && p6.id == p9.id
    =>
      SplitOrderedStream(SplitOrderedStream(
        Let(p1, Let(p2, Let(p3, Let(p4, Let(p5, MapOrderedStream(ArchLambda(p6,
          ReadSync(ParamUse(p7), Select2(ParamUse(p8), sel1), Select2(ParamUse(p9), sel2))),
          Zip2OrderedStream(Tuple2(JoinOrderedStream(Repeat(rAddr, times)), JoinOrderedStream(Repeat(ParamUse(p10), times))))
        ), a5), a4), a3), a2), a1),
        chunkSize), times)

    case MapOrderedStream(ArchLambda(pa, Repeat(ParamUse(pb), times, _), _), SplitOrderedStream(Let(p1, Let(p2, Let(p3, Let(p4, Let(p5,
    MapOrderedStream(ArchLambda(p6, ReadSync(ParamUse(p7), Select2(ParamUse(p8), sel1, _), Select2(ParamUse(p9), sel2, _), _), _), Zip2OrderedStream(Tuple2(rAddr,
    ParamUse(p10), _), _), _) , a5, _), a4, _), a3, _), a2, _), a1, _), chunkSize, _), _) if
      p5.id == p10.id && p2.id == p7.id && p6.id == p8.id && p6.id == p9.id && pa.id == pb.id
    =>
      SplitOrderedStream(SplitOrderedStream(
        Let(p1, Let(p2, Let(p3, Let(p4, Let(p5, MapOrderedStream(ArchLambda(p6,
          ReadSync(ParamUse(p7), Select2(ParamUse(p8), sel1), Select2(ParamUse(p9), sel2))),
          Zip2OrderedStream(Tuple2(JoinOrderedStream(MapOrderedStream(Repeat.asFunction(Seq(None), Seq(times)), rAddr)),
            JoinOrderedStream(MapOrderedStream(Repeat.asFunction(Seq(None), Seq(times)), ParamUse(p10)))))
        ), a5), a4), a3), a2), a1),
        chunkSize), times)
  })

  def skipSelectMapZip: Rule = Rule("skipSelectMapZip", {
    case MapOrderedStream(ArchLambda(p1, body, _), ZipOrderedStream(in, n, _), _) if {
      findSkipSelectMapZipCandidates(p1, body).nonEmpty
    } =>
      val candidates = findSkipSelectMapZipCandidates(p1, body)
      val newInput = ZipOrderedStream(Tuple(Seq
        .range(0, n.ae.evalInt)
        .map(i =>
          candidates.get(i) match {
            case Some(reps) => MapOrderedStream(Repeat.asFunction(Seq(None), Seq(reps)), Select(in, n, i))
            case None => Select(in, n, i)
          }
        ): _*), n)
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(param,
            body.visitAndRebuild {
              case Repeat(Select(ParamUse(p2), _, sel, _), _, _) if p1.id == p2.id && candidates.contains(sel.ae.evalInt) =>
                Select(ParamUse(param), n, sel)
              case Select(ParamUse(p2), _, sel, _) if p1.id == p2.id && !candidates.contains(sel.ae.evalInt) =>
                Select(ParamUse(param), n, sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )
  })

  private def findSkipSelectMapZipCandidates(p1: ParamDef, body: Expr): Map[Int, Int] = {
    val buckets = collection.mutable.HashMap[Int, (Int, Seq[Int])]()
    var numOfSelectParams = 0
    var numOfParams = 0
    body.visit {
      case Repeat(Select(ParamUse(p2), _, sel, _), repetitions, _) if p1.id == p2.id =>
        // update the repetitions here
        val i = sel.ae.evalInt
        val (occ, reps) = buckets.getOrElse(i, (0, Seq()))
        buckets(i) = (occ - 1, repetitions.ae.evalInt +: reps)
      case Select(ParamUse(p2), _, sel, _) if p1.id == p2.id =>
        // increase the non-repeated occurrences here
        val i = sel.ae.evalInt
        val (occ, reps) = buckets.getOrElse(i, (0, Seq()))
        buckets(i) = (occ + 1, reps)
        numOfSelectParams = numOfSelectParams + 1
      case ParamUse(p2) if p1.id == p2.id =>
        numOfParams = numOfParams + 1
      case _ =>
    }

    if (numOfParams != numOfSelectParams)
      // there are occurrences of param that did not go through a Select node
      Map.empty
    else
      buckets.view.collect {
        // it is always repeated by the same amount
        case (sel, (0, x +: xs)) if xs.forall(_ == x) => (sel, x)
      }.toMap
  }

  def skipSelectMapZipRepeatND: Rule = Rule("skipSelectMapZipRepeatND", {
    case MapOrderedStream(ArchLambda(p1, body, _), ZipOrderedStream(in, n, _), _) if {
      findSkipSelectMapZipRepeatNDCandidates(p1, body).nonEmpty
    } =>
      val candidates = findSkipSelectMapZipRepeatNDCandidates(p1, body)
      val newInput = ZipOrderedStream(Tuple(Seq
        .range(0, n.ae.evalInt)
        .map(i =>
          candidates.get(i) match {
            case Some((reps, levels)) =>
              MapOrderedStream(levels + 1, Repeat.asFunction(Seq(None), Seq(reps)), Select(in, n, i))
            case None => Select(in, n, i)
          }
        ): _*), n)
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(param,
            body.visitAndRebuild {
              case MapOrderedStream(_: LambdaT, Select(ParamUse(p2), _, sel, _), _)
                if p1.id == p2.id && candidates.contains(sel.ae.evalInt) =>
                // since we also count occurrences, this one had to be one of the RepeatND terms.
                Select(ParamUse(param), n, sel)
              case Select(ParamUse(p2), _, sel, _) if p1.id == p2.id && !candidates.contains(sel.ae.evalInt) =>
                Select(ParamUse(param), n, sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )
  })

  private def findSkipSelectMapZipRepeatNDCandidates(p1: ParamDef, body: Expr): Map[Int, (Int, Int)] = {
    val buckets = collection.mutable.HashMap[Int, (Int, Seq[(Int, Int)])]()
    var numOfSelectParams = 0
    var numOfParams = 0
    body.visit {
      case MapOrderedStream(repFun: LambdaT, Select(ParamUse(p2), _, sel, _), _) if p1.id == p2.id =>
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
        innerMostFun.body match {
          case Repeat(ParamUse(p2), repetitions, _) if p2.id == innerMostFun.param.id =>
            // update the repetitions and its corresponding level here
            val i = sel.ae.evalInt
            val (occ, reps) = buckets.getOrElse(i, (0, Seq()))
            buckets(i) = (occ - 1, (repetitions.ae.evalInt, RulesHelper.getMapNDLevels(repFun)) +: reps)
          case _ =>
        }
      case Select(ParamUse(p2), _, sel, _) if p1.id == p2.id =>
        // increase the non-repeated occurrences here
        val i = sel.ae.evalInt
        val (occ, reps) = buckets.getOrElse(i, (0, Seq()))
        buckets(i) = (occ + 1, reps)
        numOfSelectParams = numOfSelectParams + 1
      case ParamUse(p2) if p1.id == p2.id =>
        numOfParams = numOfParams + 1
      case _ =>
    }

    if (numOfParams != numOfSelectParams)
      // there are occurrences of param that did not go through a Select node
      Map.empty
    else
      buckets.view.collect {
        // it is always repeated at the same level by the same amount
        case (sel, (0, x +: xs)) if xs.forall(_ == x) => (sel, x)
      }.toMap
  }

  def untupleMapRepeatZipInput: Rule = Rule("untupleMapRepeatZipInput", {
    case MapOrderedStream(fun: LambdaT, Zip2OrderedStream(in, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case Repeat(ParamUse(p2), _, _) if p2.id == innerMostFun.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      MapOrderedStream(fun, Zip2OrderedStream(Tuple2(Select2(in, 0), Select2(in, 1))))
  })

  private def skipSplit: Rule = Rule("skipSplit", {
    case MapOrderedStream(repFun: LambdaT, SplitOrderedStream(input, chunkSize, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
      innerMostFun.body match {
        case Repeat(ParamUse(p), _, _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound && RulesHelper.getMapNDLevels(repFun) > 1
    } =>
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(repFun)
      val levels = RulesHelper.getMapNDLevels(repFun)
      SplitOrderedStream(MapOrderedStream(levels - 1, innerMostFun, input), chunkSize)
  })
}
