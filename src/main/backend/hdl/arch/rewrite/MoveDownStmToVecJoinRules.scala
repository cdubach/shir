package backend.hdl.arch.rewrite

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.{ArchLambda, JoinOrderedStream, JoinVector, MapOrderedStream, MapVector, OrderedStreamToVector, Select2, SplitVector, Tuple2, VectorToOrderedStream, Zip2OrderedStream, ZipVector}
import core.{Expr, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object MoveDownStmToVecJoinRules extends RulesT {
  override protected def all(config: Option[Int]): Seq[Rule] = Seq(
    mapFission,
    mapFusion,
    skipZipStream2D,
    skipZipStream,
    skipVec2Stm,
    skipMap2D,
    mergeMap1,
  ) ++ Seq(MoveUpZipStreamRules.mapFusion)

  def mapFission: Rule = Rule("moveDownStmToVecJoin_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case OrderedStreamToVector(JoinOrderedStream(ParamUse(p2), _), _) if p1.id == p2.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case OrderedStreamToVector(JoinOrderedStream(ParamUse(p2), _), _) if p1.id == p2.id => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case OrderedStreamToVector(JoinOrderedStream(ParamUse(p2), _), _) if p1.id == p2.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream({
          val param = ParamDef()
          ArchLambda(
            param,
            OrderedStreamToVector(JoinOrderedStream(ParamUse(param)))
          )
        }, input)
      )

    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case OrderedStreamToVector(JoinOrderedStream(JoinOrderedStream(ParamUse(p2), _), _), _) if p1.id == p2.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case OrderedStreamToVector(JoinOrderedStream(JoinOrderedStream(ParamUse(p2), _), _), _) if p1.id == p2.id => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case OrderedStreamToVector(JoinOrderedStream(JoinOrderedStream(ParamUse(p2), _), _), _) if p1.id == p2.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream({
          val param = ParamDef()
          ArchLambda(
            param,
            OrderedStreamToVector(JoinOrderedStream(JoinOrderedStream(ParamUse(param))))
          )
        }, input)
      )

    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(JoinOrderedStream(JoinOrderedStream(ParamUse(p3), _), _), _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(JoinOrderedStream(JoinOrderedStream(ParamUse(p3), _), _), _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(JoinOrderedStream(JoinOrderedStream(ParamUse(p3), _), _), _), _), ParamUse(p4), _) if p1.id == p4.id && p2.id == p3.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(2, {
          val param = ParamDef()
          ArchLambda(
            param,
            OrderedStreamToVector(JoinOrderedStream(JoinOrderedStream(ParamUse(param))))
          )
        }, input)
      )
  })

  def mapFusion: Rule = Rule("moveDownStmToVecJoin_mapFusion", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        body1.visit{
          case OrderedStreamToVector(JoinOrderedStream(ParamUse(p3), _), _) if p1.id == p3.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val stm2vecInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        stm2vecInnerMost.body match {
          case OrderedStreamToVector(ParamUse(p3), _) if p3.id == stm2vecInnerMost.param.id =>
            exprFound = true
          case OrderedStreamToVector(JoinOrderedStream(ParamUse(p3), _), _) if p3.id == stm2vecInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def skipZipStream: Rule = Rule("moveDownStmToVecJoin_skipZipStream", {
    case MapOrderedStream(ArchLambda(p1, body, _), Zip2OrderedStream(Tuple2(in0, in1, _), _), _) if {
      var exprFound = false
      var numOfselectParams: Int = 0
      var numOfParams: Int = 0
      body.visit{
        case OrderedStreamToVector(JoinOrderedStream(Select2(ParamUse(p2), sel, _), _), _) if p1.id == p2.id =>
          numOfselectParams = numOfselectParams + 1
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          numOfParams = numOfParams + 1
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(Tuple2(
        MapOrderedStream(
          {
            val param = ParamDef()
            ArchLambda(
              param,
              OrderedStreamToVector(JoinOrderedStream(ParamUse(param)))
            )
          }, in0
        ),  MapOrderedStream(
          {
            val param = ParamDef()
            ArchLambda(
              param,
              OrderedStreamToVector(JoinOrderedStream(ParamUse(param)))
            )
          }, in1
        )
      )))
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case OrderedStreamToVector(JoinOrderedStream(Select2(ParamUse(p2), sel, _), _), _) if p1.id == p2.id =>
                Select2(ParamUse(param), sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )
  })

  def skipZipStream2D: Rule = Rule("moveDownStmToVecJoin_skipZipStream2D", {
    case OrderedStreamToVector(JoinOrderedStream(MapOrderedStream(ArchLambda(p1, Zip2OrderedStream(ParamUse(p2), _), _), Zip2OrderedStream(input, _), _), _), _) if p1.id == p2.id =>
      ZipVector(Tuple2(OrderedStreamToVector(JoinOrderedStream(Select2(input, 0))), OrderedStreamToVector(JoinOrderedStream(Select2(input, 1)))))
  })

  def skipVec2Stm: Rule = Rule("moveDownStmToVecJoin_skipVec2Stm", {
    case OrderedStreamToVector(JoinOrderedStream(JoinOrderedStream(MapOrderedStream(
    ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), input,  _), _), _), _) if
      p1.id == p4.id && p2.id == p3.id =>
      JoinVector(OrderedStreamToVector(JoinOrderedStream(input)))
  })

  def skipMap2D: Rule = Rule("moveDownStmToVecJoin_skipMap2D", {
    case OrderedStreamToVector(JoinOrderedStream(MapOrderedStream(
    ArchLambda(p1, MapOrderedStream(fun, ParamUse(p2), _), _), input,  _), _), _) if
      p1.id == p2.id && input.t.asInstanceOf[OrderedStreamTypeT].dimensions == 2 =>
      MapVector(fun, OrderedStreamToVector(JoinOrderedStream(input)))
  })

  def mergeMap1: Rule = Rule("moveDownStmToVecJoin_mergeMap1", {
    case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(JoinOrderedStream(ParamUse(p2), joint: OrderedStreamTypeT), _), _), input, _) if
      p1.id == p2.id && input.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt == 1 =>
      VectorToOrderedStream(SplitVector(OrderedStreamToVector(JoinOrderedStream(JoinOrderedStream(input))), joint.len))
  })
}