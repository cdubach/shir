package backend.hdl.arch.rewrite

import backend.hdl.arch.mem.WriteAsync
import core.{Expr, Let, ParamUse}
import core.rewrite.Rule

object WriteRules {
  def removeLetWrite: Rule = Rule("removeLetWrite", {
    case Let(p, body, l@Let(_, WriteAsync(_, _, _, _), _, _), _) if {
      var count = 0
      body.visit {
        case ParamUse(pd) if pd.id == p.id => count += 1
        case _ =>
      }
      count == 1
    } =>
      body.visitAndRebuild {
        case ParamUse(pd) if pd.id == p.id => l
        case e => e
      }.asInstanceOf[Expr]
  })
}