package backend.hdl.arch.rewrite
import backend.hdl.{BaseAddrTypeT, HWDataTypeT}
import core.{FunctionCall, LambdaT, Marker, ParamUse, TextType}
import core.rewrite.Rule

object PortOrderRules {
  var count: Int = 0

  def testRule: Rule = Rule("testRule", {
    case FunctionCall(FunctionCall(ParamUse(p), input1, _), input2, _: BaseAddrTypeT) =>
      val res = FunctionCall(FunctionCall(Marker(ParamUse(p), TextType(count.toString)), input1), input2)
      count = count + 1
      res
  })
}