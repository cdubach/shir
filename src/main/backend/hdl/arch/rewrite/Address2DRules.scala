package backend.hdl.arch.rewrite

import core._
import core.rewrite.{Rule, RulesT}
import backend.hdl._
import backend.hdl.arch._
import backend.hdl.arch.mem.ReadOutOfBounds

object Address2DRules extends RulesT {
  override def all(config: Option[Int]): Seq[Rule] = Seq(
    convertPaddingToFakeStream,
    convertPaddingToFakeStream4D
  )

  private def consCntrMaxWidth(start: ArithTypeT, incr: ArithTypeT, loop: ArithTypeT, dims: Seq[ArithTypeT], reps: Seq[ArithTypeT], width: ArithTypeT): CounterIntegerExpr = {
    val e1 = CounterInteger(start, incr, loop, dims, reps, None)
    val w1 = e1.t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth.ae.evalInt
    if (w1 >= width.ae.evalInt)
      e1
    else
      CounterInteger(start, incr, loop, dims, reps, Some(IntType(width)))
  }

  def convertPaddingToFakeStream: Rule = Rule("convertPaddingToFakeStream", {
    case MapOrderedStream(readFun: LambdaT, PaddedAddr2D(loop, earlyReps, padLeft, padRight, padTop, padBot, t), _)
      if t.dimensions.length == 3 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
        innerMostFun.body match {
          case ReadOutOfBounds(_, ParamUse(p), _, _, _) if p.id == innerMostFun.param.id => exprFound = true
          case _ =>
        }
        exprFound
      } =>
      // get memory
      var memAlloc: ParamDef = null
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
      innerMostFun.body match {
        case ReadOutOfBounds(ParamUse(pm), ParamUse(p), _, _, _) if p.id == innerMostFun.param.id => memAlloc = pm
        case _ =>
      }
      // Calculate dimensions
      val Seq(ichDim, wDim, hDim) = t.dimensions
      val newHDims = Seq(hDim)
      val newWDims = Seq(ichDim, wDim)
      val elementType = t.leafType
      val newHCounter = consCntrMaxWidth(0, 1, loop, newHDims, Seq(0), elementType.bitWidth)
      val newWCounter = consCntrMaxWidth(0, 1, loop, newWDims, earlyReps :+ ArithType(0), elementType.bitWidth)

      val factor = ichDim
      val lowerBoundW = padLeft.ae * factor.ae
      val upperBoundW = (wDim.ae - padRight.ae) * factor.ae
      val lowerBoundH = padTop.ae
      val upperBoundH = hDim.ae - padBot.ae

      val maxW = wDim.ae * factor.ae
      val maxH = hDim.ae
      // Original bound for pad1
      //val innerOffset = ichDim.evalInt
      //val lowerBoundW = ((wDim.ae - t.dimensions.init.last.ae) /2) * innerOffset
      //val upperBoundW = (wDim.ae - (wDim.ae - t.dimensions.init.last.ae) /2) * innerOffset
      //val lowerBoundH = (hDim.ae - t.dimensions.last.ae) /2
      //val upperBoundH = hDim.ae - (hDim.ae - t.dimensions.last.ae) /2
      val newCounters = TypeChecker.check(MapOrderedStream(
        {
          val paramH = ParamDef(newHCounter.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            paramH,
            MapOrderedStream(
              newWDims.length,
              {
                val paramW = ParamDef(newWCounter.t.asInstanceOf[OrderedStreamTypeT].leafType)
                ArchLambda(
                  paramW,
                  ResizeInteger(
                    ConstrainedSum(Tuple(ParamUse(paramW), ParamUse(paramH)), lowerBoundW, upperBoundW, lowerBoundH, upperBoundH, maxW, maxH),
                    elementType.bitWidth)
                )
              },
              newWCounter
            )
          )
        },
        newHCounter
      ))
      MapOrderedStream(readFun, newCounters)
  })

  def convertPaddingToFakeStreamAddrOnly: Rule = Rule("convertPaddingToFakeStreamAddrOnly", {
    case PaddedAddr2D(loop, earlyReps, padLeft, padRight, padTop, padBot, t) if t.dimensions.length == 3 =>
      // calculate dimensions
      val Seq(ichDim, wDim, hDim) = t.dimensions
      val newHDims = Seq(hDim)
      val newWDims = Seq(ichDim, wDim)
      val elementType = t.leafType
      val newHCounter = consCntrMaxWidth(0, 1, loop, newHDims, Seq(0), elementType.bitWidth)
      val newWCounter = consCntrMaxWidth(0, 1, loop, newWDims, earlyReps :+ ArithType(0), elementType.bitWidth)

      val factor = ichDim
      val lowerBoundW = padLeft.ae * factor.ae
      val upperBoundW = (wDim.ae - padRight.ae) * factor.ae
      val lowerBoundH = padTop.ae
      val upperBoundH = hDim.ae - padBot.ae

      val maxW = wDim.ae * factor.ae
      val maxH = hDim.ae
      // Original bound for pad1
      //val innerOffset = ichDim.evalInt
      //val lowerBoundW = ((wDim.ae - t.dimensions.init.last.ae) /2) * innerOffset
      //val upperBoundW = (wDim.ae - (wDim.ae - t.dimensions.init.last.ae) /2) * innerOffset
      //val lowerBoundH = (hDim.ae - t.dimensions.last.ae) /2
      //val upperBoundH = hDim.ae - (hDim.ae - t.dimensions.last.ae) /2
      val newCounters = TypeChecker.check(MapOrderedStream(
        {
          val paramH = ParamDef(newHCounter.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            paramH,
            MapOrderedStream(
              newWDims.length,
              {
                val paramW = ParamDef(newWCounter.t.asInstanceOf[OrderedStreamTypeT].leafType)
                ArchLambda(
                  paramW,
                  ResizeInteger(
                    ConstrainedSum(Tuple(ParamUse(paramW), ParamUse(paramH)), lowerBoundW, upperBoundW, lowerBoundH, upperBoundH, maxW, maxH),
                    elementType.bitWidth)
                )
              },
              newWCounter
            )
          )
        },
        newHCounter
      ))
      newCounters
  })

  private object PaddedAddr2D {
    def unapply(e: Expr): Option[(ArithTypeT, Seq[ArithTypeT], ArithTypeT, ArithTypeT, ArithTypeT, ArithTypeT, OrderedStreamTypeT)] = e match {
      case MapOrderedStream(ArchLambda(p1, ConcatOrderedStream(MapOrderedStream(f: LambdaT, CounterInteger(wstart, wincr, wloop, wDims, wreps, _), _),
      RepeatN(pad0 +: _, ConstantValue(iv, eltTy: IntTypeT)), cDim, _), _), hctr: CounterIntegerExpr, t: OrderedStreamTypeT)
        if {
          // check if concat is done on the correct dimensions
          cDim.ae.evalInt == 0
        } && {
          // check if the counters are well-behaved (TODO: is start and incr check necessary?)
          val CounterInteger(hstart, hincr, hloop, _, hreps, _) = hctr
          wreps.last.ae.evalInt == 0 && hreps.last.ae.evalInt == 0 &&
            wstart.ae.evalInt == 0 && hstart.ae.evalInt == 0 &&
            wincr.ae.evalInt == 1 && hincr.ae.evalInt == 1 &&
            wloop.ae.evalInt == hloop.ae.evalInt
        } && {
          // check if the padded invalid values are well-behaved
          Integer.bitCount(iv.ae.evalInt) == eltTy.bitWidth.ae.evalInt
        } =>
        // extract the inner ConstrainedSum to determine the range
        val caddf = RulesHelper.getCurrentOrInnerMostLambda(f)
        caddf.body match {
          case ResizeInteger(ConstrainedSum(Tuple(Seq(ParamUse(u2), ParamUse(u1)), _),
          loW, hiW, loH, hiH, maxW, maxH, _), _, _)
            if u2.id == caddf.param.id && u1.id == p1.id =>
            // Dropping Height and width.
            val innerDimProd = wDims.dropRight(1).map(_.ae).foldLeft(ArithType(1).ae)(_*_)
            Some((wloop, wreps.dropRight(1),
              ArithType(loW.ae / innerDimProd),
              ArithType((maxW.ae - hiW.ae) / innerDimProd + pad0.ae),
              ArithType(loH.ae),
              ArithType(maxH.ae - hiH.ae),
              t))
          case _ => None
        }

      case ConcatOrderedStream(
      MapOrderedStream(ArchLambda(p1, MapOrderedStream(f: LambdaT, CounterInteger(wstart, wincr, wloop, wDims, wreps, _), _),
      _), hctr: CounterIntegerExpr, _),
      RepeatN(pad0 +: _, ConstantValue(iv, eltTy: IntTypeT)), cDim, t: OrderedStreamTypeT)
        if {
          // check if concat is done on the correct dimensions
          cDim.ae.evalInt == 0
        } && {
          // check if the counters are well-behaved (TODO: is start and incr check necessary?)
          val CounterInteger(hstart, hincr, hloop, _, hreps, _) = hctr
          wreps.last.ae.evalInt == 0 && hreps.last.ae.evalInt == 0 &&
            wstart.ae.evalInt == 0 && hstart.ae.evalInt == 0 &&
            wincr.ae.evalInt == 1 && hincr.ae.evalInt == 1 &&
            wloop.ae.evalInt == hloop.ae.evalInt
        } && {
          // check if the padded invalid values are well-behaved
          Integer.bitCount(iv.ae.evalInt) == eltTy.bitWidth.ae.evalInt
        } =>
        // extract the inner ConstrainedSum to determine the range
        val caddf = RulesHelper.getCurrentOrInnerMostLambda(f)
        caddf.body match {
          case ResizeInteger(ConstrainedSum(Tuple(Seq(ParamUse(u2), ParamUse(u1)), _),
          loW, hiW, loH, hiH, maxW, maxH, _), _, _)
            if u2.id == caddf.param.id && u1.id == p1.id =>
            val innerDimProd = wDims.dropRight(1).map(_.ae).foldLeft(ArithType(1).ae)(_*_)
            Some((wloop, wreps.dropRight(1),
              ArithType(loW.ae / innerDimProd),
              ArithType((maxW.ae - hiW.ae) / innerDimProd),
              ArithType(loH.ae),
              ArithType(maxH.ae - hiH.ae + pad0.ae),
              t))
          case _ => None
        }

      case ConcatOrderedStream(
      ctr@CounterInteger(start, incr, loop, _, reps, _),
      RepeatN(pad0 +: pad1 +: _, ConstantValue(iv, eltTy: IntTypeT)), cDim, t: OrderedStreamTypeT)
        if {
          // check if concat is done on the correct dimensions
          cDim.ae.evalInt == 1 || cDim.ae.evalInt == 0
        } && {
          // check if counter is well-behaved (TODO: is start and incr check necessary?)
          reps.takeRight(2).forall(_.ae.evalInt == 0) && start.ae.evalInt == 0 && incr.ae.evalInt == 1
        } && {
          // check if the padded invalid values are well-behaved and don't overlap with the counter's range
          val bw = eltTy.bitWidth.ae.evalInt
          Integer.bitCount(iv.ae.evalInt) == bw &&
            Integer.bitCount(RulesHelper.getLastValue(ctr).evalInt) != bw
        } =>
        if (cDim.ae.evalInt == 1)
          Some((loop, reps.dropRight(2), 0, pad1, 0, 0, t))
        else
          Some((loop, reps.dropRight(2), 0, 0, 0, pad0, t))

      case MapOrderedStream(ArchLambda(p1, ConcatOrderedStream(RepeatN(pad0 +: _, ConstantValue(iv, eltTy: IntTypeT)),
      MapOrderedStream(f: LambdaT, CounterInteger(wstart, wincr, wloop, wDims, wreps, _), _), cDim, _), _), hctr: CounterIntegerExpr, t: OrderedStreamTypeT)
        if {
          // check if concat is done on the correct dimensions
          cDim.ae.evalInt == 0
        } && {
          // check if the counters are well-behaved (TODO: is start and incr check necessary?)
          val CounterInteger(hstart, hincr, hloop, _, hreps, _) = hctr
          wreps.last.ae.evalInt == 0 && hreps.last.ae.evalInt == 0 &&
            wstart.ae.evalInt == 0 && hstart.ae.evalInt == 0 &&
            wincr.ae.evalInt == 1 && hincr.ae.evalInt == 1 &&
            wloop.ae.evalInt == hloop.ae.evalInt
        } && {
          // check if the padded invalid values are well-behaved
          Integer.bitCount(iv.ae.evalInt) == eltTy.bitWidth.ae.evalInt
        } =>
        // extract the inner ConstrainedSum to determine the range
        val caddf = RulesHelper.getCurrentOrInnerMostLambda(f)
        caddf.body match {
          case ResizeInteger(ConstrainedSum(Tuple(Seq(ParamUse(u2), ParamUse(u1)), _),
          loW, hiW, loH, hiH, maxW, maxH, _), _, _)
            if u2.id == caddf.param.id && u1.id == p1.id =>
            val innerDimProd = wDims.dropRight(1).map(_.ae).foldLeft(ArithType(1).ae)(_*_)
            Some((wloop, wreps.dropRight(1),
              ArithType(loW.ae / innerDimProd + pad0.ae),
              ArithType((maxW.ae - hiW.ae) / innerDimProd),
              ArithType(loH.ae),
              ArithType(maxH.ae - hiH.ae),
              t))
          case _ => None
        }

      case ConcatOrderedStream(
      RepeatN(pad0 +: _, ConstantValue(iv, eltTy: IntTypeT)),
      MapOrderedStream(ArchLambda(p1, MapOrderedStream(f: LambdaT, CounterInteger(wstart, wincr, wloop, wDims, wreps, _), _),
      _), hctr: CounterIntegerExpr, _), cDim, t: OrderedStreamTypeT)
        if {
          // check if concat is done on the correct dimensions
          cDim.ae.evalInt == 0
        } && {
          // check if the counters are well-behaved (TODO: is start and incr check necessary?)
          val CounterInteger(hstart, hincr, hloop, _, hreps, _) = hctr
          wreps.last.ae.evalInt == 0 && hreps.last.ae.evalInt == 0 &&
            wstart.ae.evalInt == 0 && hstart.ae.evalInt == 0 &&
            wincr.ae.evalInt == 1 && hincr.ae.evalInt == 1 &&
            wloop.ae.evalInt == hloop.ae.evalInt
        } && {
          // check if the padded invalid values are well-behaved
          Integer.bitCount(iv.ae.evalInt) == eltTy.bitWidth.ae.evalInt
        } =>
        // extract the inner ConstrainedSum to determine the range
        val caddf = RulesHelper.getCurrentOrInnerMostLambda(f)
        caddf.body match {
          case ResizeInteger(ConstrainedSum(Tuple(Seq(ParamUse(u2), ParamUse(u1)), _),
          loW, hiW, loH, hiH, maxW, maxH, _), _, _)
            if u2.id == caddf.param.id && u1.id == p1.id =>
            val innerDimProd = wDims.dropRight(1).map(_.ae).foldLeft(ArithType(1).ae)(_*_)
            Some((wloop, wreps.dropRight(1),
              ArithType(loW.ae / innerDimProd),
              ArithType((maxW.ae - hiW.ae) / innerDimProd),
              ArithType(loH.ae + pad0.ae),
              ArithType(maxH.ae - hiH.ae),
              t))
          case _ => None
        }

      case ConcatOrderedStream(
      RepeatN(pad0 +: pad1 +: _, ConstantValue(iv, eltTy: IntTypeT)),
      ctr@CounterInteger(start, incr, loop, _, reps, _), cDim, t: OrderedStreamTypeT)
        if {
          // check if concat is done on the correct dimensions
          cDim.ae.evalInt == 1 || cDim.ae.evalInt == 0
        } && {
          // check if counter is well-behaved (TODO: is start and incr check necessary?)
          reps.takeRight(2).forall(_.ae.evalInt == 0) && start.ae.evalInt == 0 && incr.ae.evalInt == 1
        } && {
          // check if the padded invalid values are well-behaved and don't overlap with the counter's range
          val bw = eltTy.bitWidth.ae.evalInt
          Integer.bitCount(iv.ae.evalInt) == bw &&
            Integer.bitCount(RulesHelper.getLastValue(ctr).evalInt) != bw
        } =>
        if (cDim.ae.evalInt == 1)
          Some((loop, reps.dropRight(2), pad1, 0, 0, 0, t))
        else
          Some((loop, reps.dropRight(2), 0, 0, pad0, 0, t))

      case _ => None
    }
  }

  private object RepeatN {
    def unapply(e: Expr): Option[(Seq[ArithTypeT], ConstantValueExpr)] =
      unwrap(Seq.empty, e) match {
        case (reps, v: ConstantValueExpr) => Some(reps.reverse, v)
        case _ => None
      }

    @scala.annotation.tailrec
    private def unwrap(acc: Seq[ArithTypeT], e: Expr): (Seq[ArithTypeT], Expr) = e match {
      case Repeat(e, r, _) => unwrap(r +: acc, e)
      case e => (acc, e)
    }
  }

  def convertPaddingToFakeStream4D: Rule = Rule("convertPaddingToFakeStream4D", {
    case MapOrderedStream(readFun: LambdaT, PaddedAddr2D(loop, earlyReps, padLeft, padRight, padTop, padBot, t), _)
      if t.dimensions.length == 4 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
        innerMostFun.body match {
          case ReadOutOfBounds(_, ParamUse(p), _, _, _) if p.id == innerMostFun.param.id => exprFound = true
          case _ =>
        }
        exprFound
      } =>
      // get memory
      var memAlloc: ParamDef = null
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(readFun)
      innerMostFun.body match {
        case ReadOutOfBounds(ParamUse(pm), ParamUse(p), _, _, _) if p.id == innerMostFun.param.id => memAlloc = pm
        case _ =>
      }
      // Calculate dimensions
      val Seq(ich0Dim, ich1Dim, wDim, hDim) = t.dimensions
      val remainDims = Seq(ich0Dim, ich1Dim)
      val newHDims = Seq(hDim)
      val newWDims = remainDims :+ wDim
      val elementType = t.leafType
      val newHCounter = consCntrMaxWidth(0, 1, loop, newHDims, Seq(0), elementType.bitWidth)
      val newWCounter = consCntrMaxWidth(0, 1, loop, newWDims, earlyReps :+ ArithType(0), elementType.bitWidth)

      val factor = t.dimensions.dropRight(2).map(_.ae.evalInt).product // ich offset
      val lowerBoundW = padLeft.ae * factor
      val upperBoundW = (wDim.ae - padRight.ae) * factor
      val lowerBoundH = padTop.ae
      val upperBoundH = hDim.ae - padBot.ae

      val maxW = wDim.ae * factor
      val maxH = hDim.ae
      // Original bound for pad1
      //val innerOffset = remainDims.map(_.ae.evalInt).product
      //val lowerBoundW = ((wDim.ae - t.dimensions.init.last.ae) /2) * innerOffset
      //val upperBoundW = (wDim.ae - (wDim.ae - t.dimensions.init.last.ae) /2) * innerOffset
      //val lowerBoundH = (hDim.ae - t.dimensions.last.ae) /2
      //val upperBoundH = hDim.ae - (hDim.ae - t.dimensions.last.ae) /2
      val newCounters = TypeChecker.check(MapOrderedStream(
        {
          val paramH = ParamDef(newHCounter.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            paramH,
            MapOrderedStream(
              newWDims.length,
              {
                val paramW = ParamDef(newWCounter.t.asInstanceOf[OrderedStreamTypeT].leafType)
                ArchLambda(
                  paramW,
                  ResizeInteger(
                    ConstrainedSum(Tuple(ParamUse(paramW), ParamUse(paramH)), lowerBoundW, upperBoundW, lowerBoundH, upperBoundH, maxW, maxH),
                    elementType.bitWidth)
                )
              },
              newWCounter
            )
          )
        },
        newHCounter
      ))
      MapOrderedStream(readFun, newCounters)
  })
}