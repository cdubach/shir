package backend.hdl.arch.rewrite

import algo.SeqTypeVar
import backend.hdl.{OrderedStreamTypeT, ScalarTypeT, VectorTypeT}
import backend.hdl.arch.{AddInt, ArchLambda, JoinOrderedStream, MapAsyncOrderedStream, MapOrderedStream, MapVector, Registered, Repeat, ResizeInteger, Select2, SplitOrderedStream, SplitVector, Tuple2, VectorToTuple2}
import core.{Expr, Let, Marker, ParamDef, ParamUse, TextTypeT}
import core.rewrite.Rule

object WorkAroundTimingRules {

  def regAdd: Rule = Rule("regAdd", {
    case MapVector(ArchLambda(p1, AddInt(VectorToTuple2(body, _), _), _), input, _) if RulesHelper.containsParam(body, p1) =>
      MapVector(ArchLambda(p1, Registered(AddInt(VectorToTuple2(body)))), input)
  })

  def reg4Inputs: Rule = Rule("reg4Inputs", {
    case Let(p, body, arg @ ArchLambda(p1, ArchLambda(p2, ArchLambda(p3, ArchLambda(p4, argb, _), _), _), _), _) =>
      val newArg = arg.visitAndRebuild{
        case ParamUse(p) if p.t.isInstanceOf[OrderedStreamTypeT] && (p.id == p1.id || p.id == p2.id || p.id == p3.id || p.id == p4.id) =>
          val levels = p.t.asInstanceOf[OrderedStreamTypeT].dimensions.length
          MapOrderedStream(levels, Registered.asFunction(), ParamUse(p))
        case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p3, SplitVector(ParamUse(p4), svc, _), _), JoinOrderedStream(ParamUse(p2), _), _), _), Marker(input, ts), _) if
          ts.s == "map_fusion_guard" && p1.id == p2.id && p3.id == p4.id =>
          MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p3, MapVector(2, Registered.asFunction(), SplitVector(ParamUse(p4), svc))), JoinOrderedStream(ParamUse(p2)))), Marker(input, ts))
        case e => e
      }.asInstanceOf[Expr]
      Let(p, body, newArg)
  })

  def regRepeatSplit: Rule = Rule("regRepeatSplit", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3,
    SplitVector(ParamUse(p4), sv, _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), Repeat(Repeat(input, rep1, _), rep2, _), _) =>
      MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3,
        MapVector(2, Registered.asFunction(), SplitVector(ParamUse(p4), sv))), ParamUse(p5))), ParamUse(p6))), Repeat(Repeat(input, rep1), rep2))
  })

  def regJoins: Rule = Rule("regJoins", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, JoinOrderedStream(JoinOrderedStream(JoinOrderedStream(ParamUse(p3), _), _), _), _), ParamUse(p4), _), _), input, _) =>
      val param = ParamDef()
      MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
        MapOrderedStream(ArchLambda(param, MapVector(Registered.asFunction(), ParamUse(param))),
          JoinOrderedStream(JoinOrderedStream(JoinOrderedStream(ParamUse(p3)))))), ParamUse(p4))), input)
  })

  def regMapResize: Rule = Rule("regMapResize", {
    case MapOrderedStream(ArchLambda(p1, ResizeInteger(ParamUse(p2), c1, _), _), input, _) if p1.id == p2.id =>
      MapOrderedStream(ArchLambda(p1, Registered(ResizeInteger(ParamUse(p2), c1))), input)
  })

  def regMapVecAdd: Rule = Rule("regMapVecAdd", {
    case Tuple2(in1, in2, _) if {
      var exprFound = false
      in1 match{
        case MapAsyncOrderedStream(_, _, _) => exprFound = true
        case MapOrderedStream(ArchLambda(_, MapAsyncOrderedStream(_, _, _), _), _, _) => exprFound = true
        case MapOrderedStream(_, MapOrderedStream(_, Marker(_, ts: TextTypeT), _), _) if ts.s.contains("FusionGuard")=> exprFound = true
        case MapOrderedStream(_, Marker(_, ts: TextTypeT), _) if ts.s.contains("guard")=> exprFound = true
        case MapOrderedStream(_, SplitOrderedStream(Marker(_, ts: TextTypeT), _, _), _) if ts.s.contains("guard")=> exprFound = true
        case MapOrderedStream(_, SplitOrderedStream(Marker(_, ts: TextTypeT), _, _), _) if ts.s.contains("FusionGuard")=> exprFound = true
        case _ =>
      }
      exprFound &&
        in1.t.asInstanceOf[OrderedStreamTypeT].leafType.isInstanceOf[VectorTypeT] &&
        in1.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].et.isInstanceOf[ScalarTypeT]
    } =>
      val len1 = in1.t.asInstanceOf[OrderedStreamTypeT].dimensions.length
      val inner1 = in1.t.asInstanceOf[OrderedStreamTypeT].leafType
      val newIn1 = MapOrderedStream(len1, {
        val param = ParamDef(inner1)
        ArchLambda(param, MapVector(Registered.asFunction(), ParamUse(param)))
      }, in1)
      val len2 = in2.t.asInstanceOf[OrderedStreamTypeT].dimensions.length
      val inner2 = in2.t.asInstanceOf[OrderedStreamTypeT].leafType
      val newIn2 = MapOrderedStream(len2, {
        val param = ParamDef(inner2)
        ArchLambda(param, MapVector(Registered.asFunction(), ParamUse(param)))
      }, in2)
      Tuple2(newIn1, newIn2)
  })
}