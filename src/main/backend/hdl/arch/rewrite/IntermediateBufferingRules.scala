package backend.hdl.arch.rewrite

import backend.hdl.arch.mem.{BufferStreamInBlockRam, ReadSync}
import backend.hdl.{IntTypeT, LogicType, OrderedStreamTypeT, VectorType, VectorTypeT}
import backend.hdl.arch.{Alternate, ArchLambda, FoldOrderedStream, JoinOrderedStream, JoinVector, MapOrderedStream, MapVector, MulInt, OrderedStreamToUnorderedStream, OrderedStreamToVector, Repeat, Select2, SplitOrderedStream, SplitVector, Tuple2, Zip2OrderedStream}
import core.{Conversion, Expr, LambdaT, Let, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object IntermediateBufferingRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      insertBufferToMapSelect,
      doubleBufferOutputMapStmToVec,
    )

  def insertBufferToMapSelect: Rule = Rule("insertBufferToMapSelect", {
    case MapOrderedStream(selectFun: LambdaT, input, _) if {
      var exprFound = false
      selectFun.body.visit{
        case Select2(ParamUse(p), sel, t: OrderedStreamTypeT) if selectFun.param.id == p.id && t.dimensions.length == 2 && {
          var typeFound = false
          if(t.leafType.isInstanceOf[VectorTypeT])
            if(t.leafType.asInstanceOf[VectorTypeT].et.isInstanceOf[IntTypeT])
              typeFound = true
          typeFound
        } && {
          var loopFound = false
          selectFun.body.visit{
            case OrderedStreamToUnorderedStream(JoinOrderedStream(MapOrderedStream(_, Select2(ParamUse(pd), sel,_), _), _), _) if pd.id == p.id && sel.ae.evalInt == sel.ae.evalInt =>
              loopFound = true
            case _ =>
          }
          !loopFound
        } =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      MapOrderedStream(
        selectFun.visitAndRebuild{
          case Select2(ParamUse(p), sel, t: OrderedStreamTypeT) if selectFun.param.id == p.id && t.dimensions.length == 2 && {
            var typeFound = false
            if(t.leafType.isInstanceOf[VectorTypeT])
              if(t.leafType.asInstanceOf[VectorTypeT].et.isInstanceOf[IntTypeT])
                typeFound = true
            typeFound
          } =>
            val intType = t.leafType.asInstanceOf[VectorTypeT].et.asInstanceOf[IntTypeT]
            val chunkSize = t.et.asInstanceOf[OrderedStreamTypeT].len
            val paramInput = TypeChecker.check(Select2(ParamUse(p), sel))
            val convertedInput = TypeChecker.check(MapOrderedStream(2,
              {
                val param1 = ParamDef(t.leafType)
                ArchLambda(
                  param1,
                  JoinVector(MapVector(
                    {
                      val param2 = ParamDef(t.leafType.asInstanceOf[VectorTypeT].et)
                      ArchLambda(
                        param2,
                        Conversion(ParamUse(param2), VectorType(LogicType(), intType.bitWidth))
                      )
                    }, ParamUse(param1)
                  ))
                )
              }, paramInput
            ))
            val joinedInput = TypeChecker.check(JoinOrderedStream(convertedInput))
            val bufferedData = TypeChecker.check(BufferStreamInBlockRam(joinedInput))
            val splitOutput = TypeChecker.check(SplitOrderedStream(bufferedData, chunkSize))
            val convertedOutput = TypeChecker.check(MapOrderedStream(2,
              {
                val param1 = ParamDef(splitOutput.t.asInstanceOf[OrderedStreamTypeT].leafType)
                ArchLambda(
                  param1,
                  MapVector(
                    {
                      val param2 = ParamDef()
                      ArchLambda(
                        param2,
                        Conversion(ParamUse(param2), intType)
                      )
                    }, SplitVector(ParamUse(param1), intType.bitWidth)
                  )
                )
              }, splitOutput
            ))
            convertedOutput
          case e => e
        }.asInstanceOf[Expr],
        input
      )
  })

  def doubleBufferOutputMapStmToVec: Rule = Rule("doubleBufferOutputMapStmToVec", {
    case OrderedStreamToVector(input, _) if {
      var exprFound = false
      input.visit{
        case FoldOrderedStream(_, _, _) =>
          exprFound = true
        case MulInt(_, _) =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val f1 = {
        val p = ParamDef(input.t)
        ArchLambda(p, OrderedStreamToVector(ParamUse(p)))
      }
      val f2 = {
        val p = ParamDef(input.t)
        ArchLambda(p, OrderedStreamToVector(ParamUse(p)))
      }
      Alternate(f1, f2, input)

    case MapOrderedStream(fun: LambdaT, input, _) if {
      var exprFound = false
      input.visit{
        case FoldOrderedStream(_, _, _) =>
          exprFound = true
        case MulInt(_, _) =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _), input, _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      MapOrderedStream(
        fun.visitAndRebuild{
          case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _), input, _) if p1.id == p2.id =>
            MapOrderedStream(
              {
                val param = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
                ArchLambda(
                  param,
                  {
                    val f1 = {
                      val p = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
                      ArchLambda(p, OrderedStreamToVector(ParamUse(p)))
                    }
                    val f2 = {
                      val p = ParamDef(input.t.asInstanceOf[OrderedStreamTypeT].et)
                      ArchLambda(p, OrderedStreamToVector(ParamUse(p)))
                    }
                    Alternate(f1, f2, ParamUse(param))
                  }
                )
              }, input
            )
          case e => e
        }.asInstanceOf[Expr],
        input
      )
  })
}
