package backend.hdl.arch.rewrite

import algo.{Input, SeqTypeT}
import backend.hdl._
import backend.hdl.arch._
import backend.hdl.arch.device.Mul2AddInt
import core._
import lift.arithmetic.ArithExpr

import scala.annotation.tailrec

object RulesHelper {

  def isParamUse(expr: Expr): Boolean = expr match {
    case ParamUse(_) => true
    case _ => false
  }

  def isSelectParamUse(expr: Expr): Boolean = expr match {
    case algo.Select2(ParamUse(_), _, _) => true
    case arch.Select2(ParamUse(_), _, _) => true
    case _ => false
  }

  def exprContains(expr: Expr, check: PartialFunction[Expr, Boolean]): Boolean = {
    expr.visit({
      case e: Expr if check.isDefinedAt(e) && check(e) => return check(e)
      case _ =>
    })
    false
  }

  def isComputation(expr: Expr): Boolean = {
    RulesHelper.exprContains(expr, {
      case MulInt(_, _) => true
      case MulFloat(_, _) => true
      case AddInt(_, _) => true
      case AddFloat(_, _) => true
      case Mul2AddInt(_, _) => true
    })
  }

  def isMulComputation(expr: Expr): Boolean = {
    RulesHelper.exprContains(expr, {
      case MulInt(_, _) => true
      case MulFloat(_, _) => true
      case Mul2AddInt(_, _) => true
    })
  }

  def isStreamReshapeFunction(fun: LambdaT): Boolean = {
    @tailrec
    def go(xs: Seq[(ParamDef, Expr)]): Boolean = xs match {
      case Seq() => true
      case (p, ParamUse(u)) +: xs if u.id == p.id => go(xs)
      case (p, JoinOrderedStream(e, _)) +: xs => go((p, e) +: xs)
      case (p, SplitOrderedStream(e, _, _)) +: xs => go((p, e) +: xs)
      case (p, Marker(e, _)) +: xs => go((p, e) +: xs)
      case (p, MapOrderedStream(fun: LambdaT, e, _)) +: xs => go((p, e) +: (fun.param, fun.body) +: xs)
      case _ => false
    }

    go(Seq((fun.param, fun.body)))
  }

  def isInputConversionFunction(f: LambdaT): Boolean = {
    val fun = RulesHelper.getCurrentOrInnerMostLambda(f)

    // the last operation is very likely a (Map Conversion)
    // If MapVec is detected, it implies the "Join . (Map VecToStream)" pattern might be cleared.
    var isStmToVecMovedIn = false
    val e1 = fun.body match {
      case MapOrderedStream(ArchLambda(p, Conversion(ParamUse(u), _), _), e, _)
        if p.id == u.id => e
      case MapOrderedStream(ArchLambda(p1, MapVector(ArchLambda(p2, Conversion(ParamUse(p3), _), _),
        ParamUse(p4), _), _), e, _) if p1.id == p4.id && p2.id == p3.id =>
        isStmToVecMovedIn = true
        e
      case e => e
    }

    // then there might be a Drop
    // (if the inner data does not perfectly span multiple lines)
    val e2 = e1 match {
      case DropOrderedStream(e, _, _, _) => e
      case e => e
    }

    // then there is a Join . (Map VecToStream)
    val e3 = e2 match {
      case JoinOrderedStream(MapOrderedStream(ArchLambda(p, VectorToOrderedStream(ParamUse(u), _), _), e, _), _)
        if p.id == u.id => e
      case e if isStmToVecMovedIn => e
      case _ => return false
    }

    // then there might be a DropVec
    // (if the inner data does not perfectly span a single line)
    val e4 = e3 match {
      case MapOrderedStream(ArchLambda(p1, DropVector(ParamUse(u1), _, _, _), _), e, _)
        if p1.id == u1.id => e
      case e => e
    }

    // then, finally, there is a Map (SplitVec w . DropVec 0 ?) input
    e4 match {
      case MapOrderedStream(ArchLambda(p1, SplitVector(ParamUse(u1), _, _), _), ParamUse(uf), _)
        if fun.param.id == uf.id && p1.id == u1.id => true
      case MapOrderedStream(ArchLambda(p1, SplitVector(DropVector(ParamUse(u1), z, _, _), _, _), _), ParamUse(uf), _)
        if z.ae.evalInt == 0 && fun.param.id == uf.id && p1.id == u1.id => true
      case _ => false
    }
  }

  def hasRepeatedStream(l: LambdaT): Boolean = l.param.t match {
    case _: UnorderedStreamTypeT =>
      // check if there is a Map somewhere in the lambda's body, which uses the lambda's parameter somewhere (also if the usage is further down in the children)
      findMapStreamChildren(l.body).foreach(mapExpr => mapExpr.visit({
        case ParamUse(pd) if pd.id == l.param.id => return true
        case _ =>
      }))
      false
    case _ => false
  }

  def findMapStreamChildren(e: Expr): Seq[Expr] = e match {
    case MapOrderedStream(_, _, _) => Seq(e)
    case MapUnorderedStream(_, _, _) => Seq(e)
    case _ => e.children.flatMap{
      case innerExpr: Expr => findMapStreamChildren(innerExpr)
      case _ => Seq()
    }
  }

  def exprMatches(expr: Expr, check: PartialFunction[Expr, Boolean]): Boolean = {
    expr match{
      case e: Expr if check.isDefinedAt(e) && check(e) => return check(e)
      case _ =>
    }
    false
  }

  def containsParam(expr: Expr, param: ParamDef): Boolean = {
    RulesHelper.exprContains(expr, {
      case ParamUse(usedParam) if usedParam.id == param.id => true
    })
  }

  def matchesParam(expr: Expr, param: ParamDef): Boolean = {
    exprMatches(expr, {
      case ParamUse(usedParam) if usedParam.id == param.id => true
    })
  }

  def findInnerMostLambda(l: LambdaT): Option[LambdaT] = l.body match{
    case algo.Map(nl: LambdaT, ParamUse(pd), _) if pd.id == l.param.id =>
      nl.body match {
        case algo.Map(nnl: LambdaT, ParamUse(pd), _) if pd.id == nl.param.id  =>
          findInnerMostLambda(nl)
        case _ => Some(nl)
      }
    case MapOrderedStream(nl: LambdaT, ParamUse(pd), _) if pd.id == l.param.id =>
      nl.body match {
        case MapOrderedStream(nnl: LambdaT, ParamUse(pd), _) if pd.id == nl.param.id  =>
          findInnerMostLambda(nl)
        case _ => Some(nl)
      }
    case _ => None
  }

  def getCurrentOrInnerMostLambda(l: LambdaT): LambdaT = findInnerMostLambda(l) match{
    case Some(nl) => nl
    case None => l
  }

  def findLambdaWithGivenLevel(l: LambdaT, targetLevel: Int): Option[LambdaT] = l.body match{
    case algo.Map(nl: LambdaT, ParamUse(pd), _) if pd.id == l.param.id && targetLevel > 0 =>
      findLambdaWithGivenLevel(nl, targetLevel - 1)
    case MapOrderedStream(nl: LambdaT, ParamUse(pd), _) if pd.id == l.param.id && targetLevel > 0 =>
      findLambdaWithGivenLevel(nl, targetLevel - 1)
    case _ => if(targetLevel <= 0) Some(l) else None
  }

  def getMapNDLevels(l: LambdaT): Int = l.body match{
    case algo.Map(nl: LambdaT, ParamUse(pd), _) if pd.id == l.param.id => getMapNDLevels(nl) + 1
    case MapOrderedStream(nl: LambdaT, ParamUse(pd), _) if pd.id == l.param.id => getMapNDLevels(nl) + 1
    case _ => 1
  }

  def getInnerMostStreamType(inputType: Type): Type = inputType match {
    case ot: OrderedStreamTypeT => ot.et match {
      case _: OrderedStreamTypeT => getInnerMostStreamType(ot.et)
      case _ => inputType
    }
    case _ => inputType
  }

  def isCounter(expr: Expr): Boolean = expr match {
    case CounterInteger(_, _, _, _, _, _) => true
    case _ => false
  }

  def containsCounter(expr: Expr): Boolean = {
    RulesHelper.exprContains(expr, {
      case CounterInteger(_, _, _, _, _, _) => true
    })
  }

  def findJoinNDInput(expr: Expr): Option[Expr] = expr match{
    case algo.Join(ne: Expr, _) =>
      ne match {
        case algo.Join(nne: Expr, _)  =>
          findJoinNDInput(ne)
        case _ => Some(ne)
      }
    case JoinOrderedStream(ne: Expr, _) =>
      ne match {
        case JoinOrderedStream(nne: Expr, _)  =>
          findJoinNDInput(ne)
        case _ => Some(ne)
      }
    case _ => None
  }

  def getCurrentOrJoinNDInput(expr: Expr): Expr = findJoinNDInput(expr) match {
    case Some(ne) => ne
    case None => expr
  }

  def getJoinNDLevels(expr: Expr): Int = expr match{
    case algo.Join(ne: Expr, _) => getJoinNDLevels(ne) + 1
    case JoinOrderedStream(ne: Expr, _) => getJoinNDLevels(ne) + 1
    case _ => 0
  }

  /*
   *********************************************************************************************************************
    Algo level only helpers
   *********************************************************************************************************************
   */
  def containsInput(expr: Expr): Boolean = {
    RulesHelper.exprContains(expr, {
      case Input(_, _, _, _) => true
    })
  }

  @tailrec
  def getInnerMostSeqType(inputType: Type): Type = inputType match {
    case st: SeqTypeT => st.et match {
      case _: SeqTypeT => getInnerMostSeqType(st.et)
      case _ => inputType
    }
    case _ => inputType
  }

  def getInnerSeqTypeWithGivenLevel(inputType: Type, targetLevel: Int): Type = inputType match {
    case st: SeqTypeT => st.et match {
      case _: SeqTypeT if targetLevel > 0 => getInnerMostSeqType(st.et)
      case _ => inputType
    }
    case _ => inputType
  }

  /*
   *********************************************************************************************************************
    Transpose related helpers
   *********************************************************************************************************************
   */
  def generateCountSeq(start: Int, length: Int): Seq[ArithTypeT] = {
    List.range(start, start + length).map(ArithType(_))
  }

  def checkDimOrder(dimOrder: Seq[ArithTypeT]): Boolean =
    dimOrder.zipWithIndex.forall(p => p._1.ae.evalInt == p._2)

  def invertDimOrder(dimOrder: Seq[ArithTypeT]): Seq[ArithTypeT] =
    dimOrder.map(_.ae.evalInt).zipWithIndex.sortBy(_._1).map(_._2).map(ArithType(_))

  def removeDimFromDimOrder(dimOrder: Seq[ArithTypeT], target: Int): Seq[ArithTypeT] = {
    dimOrder.filter(_.ae.evalInt != target).map(x => ArithType(if(x.ae.evalInt > target) x.ae.evalInt - 1 else x.ae.evalInt))
  }

  def addDimToDimOrder(dimOrder: Seq[ArithTypeT], target: Int, location: Int): Seq[ArithTypeT] = {
    dimOrder.map(x => ArithType(if(x.ae.evalInt >= target) x.ae.evalInt + 1 else x.ae.evalInt)).patch(location, Seq(ArithType(target)), 0)
  }

  def moveDimInDimOrder(dimOrder: Seq[ArithTypeT], target: Int, location: Int): Seq[ArithTypeT] = {
    dimOrder.patch(target, Nil, 1).patch(location, Seq(ArithType(target)), 0)
  }

  def mergeDimOrders(dimOrderTop: Seq[ArithTypeT], dimOrderBot: Seq[ArithTypeT]): Seq[ArithTypeT] =
    dimOrderTop.map(_.ae.evalInt).map(dimOrderBot(_))

  // Remove Dimensions with Larger Ids
  def getDimOrderLargerThanEqual(dimOrder: Seq[ArithTypeT], target: Int): Seq[ArithTypeT] = {
    dimOrder.filter(_.ae.evalInt >= target).map(x => ArithType(x.ae.evalInt - target))
  }

  // Remove Dimensions with Smaller Ids
  def getDimOrderSmallerThan(dimOrder: Seq[ArithTypeT], target: Int): Seq[ArithTypeT] = {
    dimOrder.filter(_.ae.evalInt < target)
  }

  def expandAsNDIncr(increment: ArithTypeT, dimensions: Seq[ArithTypeT], repetitions: Seq[ArithTypeT]): Seq[ArithTypeT] = {
    val increments = new collection.mutable.ListBuffer[ArithTypeT]
    var carryover = increment.ae
    for ((dim, reps) <- dimensions.zip(repetitions)) {
      if (reps.ae.evalInt != 0  /* it's a repeating dimension */)
        increments += ArithType(0)
      else {
        increments += carryover
        carryover *= dim.ae
      }
    }

    increments.toList
  }

  def getLastValue(counter: CounterIntegerExpr): ArithExpr = {
    val CounterInteger(start, incr, _, dims, reps, _) = counter
    val limit = dims.zip(reps)
      .filter(_._2.ae.evalInt == 0)
      .map(_._1.ae)
      .fold(ArithType(1).ae)(_ * _)
    start.ae + incr.ae * (limit - 1)
  }

  def getLastValue(counter: CounterIntegerNDExpr): ArithExpr = {
    val CounterIntegerND(start, _, incrs, dims, _) = counter
    start.ae + incrs.zip(dims)
      .map(p => p._1.ae * (p._2.ae - 1))
      .fold(ArithType(0).ae)(_ + _)
  }
}
