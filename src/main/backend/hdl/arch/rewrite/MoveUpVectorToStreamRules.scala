package backend.hdl.arch.rewrite

import backend.hdl.{BasicDataTypeT, OrderedStreamTypeT}
import backend.hdl.arch._
import core.rewrite.{Rule, RulesT}
import core.util.IRDotGraph
import core.{Expr, LambdaT, Marker, ParamDef, ParamUse, TypeChecker}

object MoveUpVectorToStreamRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      mapFission,
      mapFusion,
      mapFusionMapParam,
      convertMapStreamToMapVector,
      skipExprs,
      skipTuple,
      skipSplitStream,
      mergeWithStreamToVector,
      skipDropJoinStream,
      skipAlternate,
      skipRepeat,
      map2InputFissionND,
      mapNDSkipZipTuple,
      skipZipTuple,
      skipMapAdd
    )

  def solveTranspose: Seq[Rule] =
    Seq(
      mapFission,
      mapFusion,
      convertMapStreamToMapVector,
      skipExprs,
      skipTuple,
      skipSplitStream,
      mergeWithStreamToVector,
      mapFusionMapParam,
      skipRepeat,
      map2InputFissionND,
      skipDropStream
    )

  def outputPadding(clWidth: Int = 512): Rule = Rule("moveUpVectorToStream_outputPadding", {
    case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(u1), _), _),
      SplitOrderedStream(ConcatOrderedStream(
        input,
        Repeat(pad @ VectorGenerator(ConstantValue(_), _, _), k, _), dim, _), s, t: OrderedStreamTypeT), _)
      if dim.ae.evalInt == 0 && t.len.ae.evalInt == 1 && t.bitWidth.ae.evalInt <= clWidth &&
        p1.id == u1.id =>
      VectorToOrderedStream(SplitVector(
        ConcatVector(Tuple(OrderedStreamToVector(input), VectorGenerator(pad, k))), s))

    case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(u1), t: BasicDataTypeT), _),
      ConcatOrderedStream(
        input,
        Repeat(Repeat(pad @ VectorGenerator(ConstantValue(_), _, _), k, _), _, _), dim, _), _)
    if dim.ae.evalInt == 1 && t.bitWidth.ae.evalInt <= clWidth && p1.id == u1.id =>
      MapOrderedStream({
        val p1 = ParamDef()
        ArchLambda(p1,
          ConcatVector(Tuple(OrderedStreamToVector(ParamUse(p1)), VectorGenerator(pad, k))))
      }, input)

    case MapOrderedStream(ArchLambda(p2, MapOrderedStream(f,
      MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(u1), _), _),
        SplitOrderedStream(ParamUse(u2), s, t: OrderedStreamTypeT), _), _), _),
      ConcatOrderedStream(
        input,
        Repeat(Repeat(pad @ VectorGenerator(ConstantValue(_), _, _), k, _), _, _), dim, _), _)
    if dim.ae.evalInt == 1 && t.len.ae.evalInt == 1 && t.bitWidth.ae.evalInt <= clWidth &&
      p1.id == u1.id && p2.id == u2.id && !RulesHelper.containsParam(f, p2) =>
      MapOrderedStream({
        val p1 = ParamDef()
        ArchLambda(p1,
          MapOrderedStream(f,
            VectorToOrderedStream(SplitVector(
              ConcatVector(Tuple(OrderedStreamToVector(ParamUse(p1)), VectorGenerator(pad, k))), s))))
      }, input)
  })

  private def mergeWithStreamToVector: Rule = Rule("moveUpVectorToStream_mergeWithStreamToVector", {
    case
      MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _),
      MapOrderedStream(ArchLambda(p3, JoinOrderedStream(ParamUse(p4), _), _),
      MapOrderedStream(ArchLambda(p5, MapOrderedStream(ArchLambda(p6, VectorToOrderedStream(ParamUse(p7), _), _), ParamUse(p8), _), _), input, _), _), _)
      if p1.id == p2.id && p3.id == p4.id && p5.id == p8.id && p6.id == p7.id =>
      MapOrderedStream(
        {
          val p = ParamDef()
          ArchLambda(p,
            JoinVector(OrderedStreamToVector(ParamUse(p)))
          )
        },
        input
      )
  })

  def mapFission: Rule = Rule("moveUpVectorToStream_mapFission", {
    // map fission
    case MapOrderedStream(ArchLambda(origParam, VectorToOrderedStream(fun, _), _), input, _)
      // prevent infinite rewrites with this rule by checking that fun is not a ParamUse
      if !RulesHelper.isParamUse(fun) =>
      MapOrderedStream(
        VectorToOrderedStream.asFunction(),
        MapOrderedStream(ArchLambda(origParam, fun), input)
      )
    // map map fission
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), inputInner, _), _), inputOuter, _)
      if p2.id == p3.id &&
        // prevent infinite rewrites with this rule:
        (inputInner match {
          case ParamUse(p4) if p1.id == p4.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(
            pOuter,
            MapOrderedStream(
              VectorToOrderedStream.asFunction(),
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          ArchLambda(p1, inputInner),
          inputOuter
        )
      )
    // map map map fission
    case MapOrderedStream(ArchLambda(p1,
          MapOrderedStream(ArchLambda(p2,
            MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), _),
            inputInner, _), _),
          inputOuter, _)
      if p3.id == p4.id && p2.id == p5.id &&
        // prevent infinite rewrites with this rule:
        (inputInner match {
          case ParamUse(p6) if p1.id == p6.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(pOuter,
            MapOrderedStream(
              {
                val pInner = ParamDef()
                ArchLambda(pInner,
                  MapOrderedStream(
                    VectorToOrderedStream.asFunction(),
                    ParamUse(pInner)
                  )
                )
              },
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          ArchLambda(p1, inputInner),
          inputOuter
        )
      )
    // MapNDFission
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(vecToStmFun: LambdaT, inputInner, _), _), inputOuter, _)
      if {
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(vecToStmFun)
        innerMostBody.body match {
          case VectorToOrderedStream(ParamUse(p2), _) if p2.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } &&
        // prevent infinite rewrites with this rule:
        (inputInner match {
          case ParamUse(p2) if p1.id == p2.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(
            pOuter,
            MapOrderedStream(
              vecToStmFun,
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          ArchLambda(p1, inputInner),
          inputOuter
        )
      )
  })

  private def mapFusion: Rule = Rule("moveUpVectorToStream_mapFusion", {
    // map fusion
    case MapOrderedStream(ArchLambda(pd, body, _), MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), input, _), _)
      if p1.id == p2.id &&
        (body match {
          // prevent loops with map map fission rule
          case MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), ParamUse(p3), _) if pd.id == p3.id && p1.id == p2.id => false
          case JoinOrderedStream(ParamUse(p), _) if pd.id == p.id => false
          case MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), ParamUse(p3), _) if pd.id == p3.id && p1.id == p2.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val newParam = ParamDef()
          ArchLambda(
            newParam,
            body.visitAndRebuild(
              {
                case ParamUse(usedParam) if usedParam.id == pd.id => VectorToOrderedStream(ParamUse(newParam))
                case e => e
              }
            ).asInstanceOf[Expr]
          )
        },
        input
      )
    // map map fusion
    case MapOrderedStream(ArchLambda(pd, body, _), MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), input, _), _)
      if p2.id == p3.id && p1.id == p4.id &&
        (body match {
          // prevent loops with map map fission rule
          case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), ParamUse(p5), _)
            if pd.id == p5.id && p1.id == p4.id && p2.id == p3.id => false
          case JoinOrderedStream(ParamUse(p), _) if pd.id == p.id => false // prevent loops with join stream rule
          case MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), ParamUse(p3), _) if pd.id == p3.id && p1.id == p2.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val newParam = ParamDef()
          ArchLambda(
            newParam,
            body.visitAndRebuild(
              {
                case ParamUse(usedParam) if usedParam.id == pd.id =>
                  MapOrderedStream(
                    VectorToOrderedStream.asFunction(),
                    ParamUse(newParam)
                  )
                case e => e
              }
            ).asInstanceOf[Expr]
          )
        },
        input
      )
    // map map map fusion
    case MapOrderedStream(ArchLambda(pd, body, _),
    MapOrderedStream(ArchLambda(p1,
    MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(
    ParamUse(p4), _), _),
    ParamUse(p5), _), _),
    ParamUse(p6), _), _), input, _), _)
      if p3.id == p4.id && p2.id == p5.id && p1.id == p6.id &&
        (body match {
          // prevent loops with map map map fission rule
          case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _)
            if pd.id == p7.id && p1.id == p6.id && p2.id == p5.id && p3.id == p4.id => false
          case JoinOrderedStream(ParamUse(p), _) if pd.id == p.id => false // prevent loops with join stream rule
          case MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), ParamUse(p3), _) if pd.id == p3.id && p1.id == p2.id => false
          case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, JoinOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _)
            if pd.id == p7.id && p1.id == p6.id && p2.id == p5.id && p3.id == p4.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val newParam = ParamDef()
          ArchLambda(newParam,
            body.visitAndRebuild(
              {
                case ParamUse(usedParam) if usedParam.id == pd.id =>
                  MapOrderedStream(
                    {
                      val newInnerParam = ParamDef()
                      ArchLambda(newInnerParam,
                        MapOrderedStream(
                          VectorToOrderedStream.asFunction(),
                          ParamUse(newInnerParam)
                        )
                      )
                    },
                    ParamUse(newParam)
                  )
                case e => e
              }
            ).asInstanceOf[Expr]
          )
        },
        input
      )
  })

  // TODO make private
  def mapFusionMapParam: Rule = Rule("moveUpVectorToStream_mapFusionMapParam", {
    // This rule conflicts with MoveUpJoinStream(MapFission)!
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        val vec2stmInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        vec2stmInnerMost.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == vec2stmInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit{
          case ParamUse(p4) if p1.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val vec2stmInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body1))
        vec2stmInnerMost.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == vec2stmInnerMost.param.id =>
            exprFound = true
          case JoinOrderedStream(ParamUse(p3), _) if p3.id == vec2stmInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      }  =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  private def convertMapStreamToMapVector: Rule = Rule("moveUpVectorToStream_convertMapStreamToMapVector", {
    // if fun is too generic, this statement may parallelize even further by replacing more mapstreams with mapvectors
    //    case MapOrderedStream(fun @ ArchLambda(p1, Conversion(ParamUse(p2, _), _), _), VectorToOrderedStream(input, _), _) if p1.id == p2.id =>
    //      VectorToOrderedStream(MapVector(fun, input))
    case MapOrderedStream(fun: LambdaT, VectorToOrderedStream(input, _), _) if
      fun.t.outType.isInstanceOf[BasicDataTypeT] &&
      !RulesHelper.isComputation(fun) &&
      !RulesHelper.exprContains(fun, {
        case VectorToOrderedStream(_, _) => true // do not rewrite if there is an inner stream creation (may temporary create a vector of stream, which is forbidden!)
      }) =>
      VectorToOrderedStream(MapVector(fun, input))
  })

  private def skipExprs: Rule = Rule("moveUpVectorToStream_skipExprs", {
    case RepeatHidden(MapOrderedStream(fun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), input, _), repetitions, _)
      if p2.id == p3.id && p1.id == p4.id =>
      MapOrderedStream(fun, RepeatHidden(input, repetitions))
    case Marker(MapOrderedStream(fun @ ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), input, _), text)
      if p1.id == p2.id && !text.s.contains("vec2stm_guard") =>
      MapOrderedStream(fun, Marker(input, text))
    case Marker(MapOrderedStream(fun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), input, _), text)
      if p2.id == p3.id && p1.id == p4.id && !text.s.contains("vec2stm_guard") =>
      MapOrderedStream(fun, Marker(input, text))
    case Marker(
    MapOrderedStream(fun @ ArchLambda(p1,
    MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3,
    VectorToOrderedStream(
    ParamUse(p4), _), _),
    ParamUse(p5), _), _),
    ParamUse(p6), _), _),
    input, _), text)
      if p3.id == p4.id && p2.id == p5.id && p1.id == p6.id =>
      MapOrderedStream(fun, Marker(input, text))
    case Marker(MapOrderedStream(vecToStmFun: LambdaT, input, _), text)
      if {
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(vecToStmFun)
        innerMostBody.body match {
          case VectorToOrderedStream(ParamUse(p), _) if p.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && !text.s.contains("vec2stm_guard") =>
      MapOrderedStream(vecToStmFun, Marker(input,  text))
  })

  private def skipTuple: Rule = Rule("moveUpVectorToStream_skipTuple", {
    case
      MapOrderedStream(
      ArchLambda(p1, Zip2OrderedStream(ParamUse(p2), _), _),
      Zip2OrderedStream(Tuple2(
      MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), input1, _),
      MapOrderedStream(ArchLambda(p5, VectorToOrderedStream(ParamUse(p6), _), _), input2, _), _), _), _)
      if p1.id == p2.id && p3.id == p4.id && p5.id == p6.id =>
      MapOrderedStream(
        {
          val p = ParamDef()
          ArchLambda(p, VectorToOrderedStream(ZipVector(ParamUse(p))))
        },
        Zip2OrderedStream(Tuple2(input1, input2))
      )
    case
      MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, Zip2OrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _),
      MapOrderedStream(ArchLambda(p5, Zip2OrderedStream(ParamUse(p6), _), _),
      Zip2OrderedStream(Tuple2(
      MapOrderedStream(ArchLambda(p7, MapOrderedStream(ArchLambda(p8, VectorToOrderedStream(ParamUse(p9), _), _), ParamUse(p10), _), _), input1, _),
      MapOrderedStream(ArchLambda(p11, MapOrderedStream(ArchLambda(p12, VectorToOrderedStream(ParamUse(p13), _), _), ParamUse(p14), _), _), input2, _), _), _), _), _)
      if p1.id == p4.id && p2.id == p3.id && p5.id == p6.id && p7.id == p10.id && p8.id == p9.id && p11.id == p14.id && p12.id == p13.id =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(pOuter,
            MapOrderedStream(
              {
                val pInner = ParamDef()
                ArchLambda(pInner,
                  VectorToOrderedStream(ZipVector(ParamUse(pInner)))
                )
              },
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          Zip2OrderedStream.asFunction(),
          Zip2OrderedStream(Tuple2(input1, input2))
        )
      )
    case MapOrderedStream(ArchLambda(p1, Zip2OrderedStream(ParamUse(p2), _), _), Zip2OrderedStream(Tuple2(MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), input1, _), input2, _), _), _)
      if p1.id == p2.id && p3.id == p4.id =>
      MapOrderedStream(
        {
          val p = ParamDef()
          ArchLambda(p, VectorToOrderedStream(ZipVector(ParamUse(p))))
        },
        Zip2OrderedStream(Tuple2(
          input1,
          MapOrderedStream(
            OrderedStreamToVector.asFunction(),
            input2,
          )
        ))
      )
  })

  // TODO make private
  def skipSplitStream: Rule = Rule("moveUpVectorToStream_skipSplitStream", {
    case SplitOrderedStream(VectorToOrderedStream(input, _), chunkSize, _) =>
      MapOrderedStream(
        VectorToOrderedStream.asFunction(),
        VectorToOrderedStream(SplitVector(input, chunkSize))
      )
    case SplitOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), input, _), chunkSize, _) if p1.id == p2.id =>
      MapOrderedStream(
        {
          val p = ParamDef()
          ArchLambda(p,
            MapOrderedStream(
              VectorToOrderedStream.asFunction(),
              ParamUse(p)
            )
          )
        },
        SplitOrderedStream(input, chunkSize)
      )
  })

  private def skipJoinStream: Rule = Rule("moveUpVectorToStream_skipJoinStream", {
    case JoinOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), input, _), _)
      if p1.id == p4.id && p2.id == p3.id =>
      MapOrderedStream(
        VectorToOrderedStream.asFunction(),
        JoinOrderedStream(input)
      )
    //    case JoinOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), input, _), _)
    //      if p1.id == p6.id && p2.id == p5.id && p3.id == p4.id =>
    //      MapOrderedStream(
    //        {
    //          val p = ParamDef()
    //          ArchLambda(p, MapOrderedStream(VectorToOrderedStream.asFunction(), ParamUse(p)))
    //        },
    //        JoinOrderedStream(input)
    //      )
  })

  private def skipDropStream: Rule = Rule("moveUpVectorToStream_skipDropStream", {
    case DropOrderedStream(VectorToOrderedStream(input, _), firstElements, lastElements, _) =>
      VectorToOrderedStream(DropVector(input, firstElements, lastElements))
    case DropOrderedStream(MapOrderedStream(fun: LambdaT, input, _), firstElements, lastElements, _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      MapOrderedStream(levels, VectorToOrderedStream.asFunction(), DropOrderedStream(input, firstElements, lastElements))
  })

  private def skipDropJoinStream: Rule = Rule("moveUpVectorToStream_skipDropJoinStream", {
    case DropOrderedStream(JoinOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), input, _), _),
    firstElements, lastElements, _) if p1.id == p2.id =>
      VectorToOrderedStream(DropVector(JoinVector(OrderedStreamToVector(input)), firstElements, lastElements))
  })

  // Extra rules for move up Read beacuse Read always come with JoinStm and Stm2Vec!
  def skipPad: Rule = Rule("moveUpVectorToStream_skipPad", {
    case PadOrderedStream(MapOrderedStream(fun: LambdaT, input, _), fes, les, value, _)
      if {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.getMapNDLevels(fun) > 0
      } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      MapOrderedStream(levels, VectorToOrderedStream.asFunction(), PadOrderedStream(input, fes, les, value))

    case MapOrderedStream(ArchLambda(p1, PadOrderedStream(ParamUse(p2), fes, les, value, _), _),
    MapOrderedStream(fun: LambdaT, input, _), _)
      if p1.id == p2.id && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.getMapNDLevels(fun) > 1
      } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      MapOrderedStream(levels, VectorToOrderedStream.asFunction(), MapOrderedStream(PadOrderedStream.asFunction(Seq(None), Seq(fes, les, value)), input))
  })

  def skipSlideGeneral: Rule = Rule("moveUpVectorToStream_skipSlideGeneral", {
    case SlideGeneralOrderedStream(MapOrderedStream(fun: LambdaT, input, _), ws, ss, _)
      if {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.getMapNDLevels(fun) > 0
      } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      MapOrderedStream(levels + 1, VectorToOrderedStream.asFunction(), SlideGeneralOrderedStream(input, ws, ss))

    case MapOrderedStream(ArchLambda(p1, SlideGeneralOrderedStream(ParamUse(p2), ws, ss, _), _),
    MapOrderedStream(fun: LambdaT, input, _), _)
      if p1.id == p2.id && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.getMapNDLevels(fun) > 1
      } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      MapOrderedStream(levels + 1, VectorToOrderedStream.asFunction(), MapOrderedStream(SlideGeneralOrderedStream.asFunction(Seq(None), Seq(ws, ss)), input))
  })

  def skipTranspose: Rule = Rule("moveUpVectorToStream_skipTranspose", {
    case TransposeNDOrderedStream(MapOrderedStream(fun: LambdaT, input, _), dimOrder, _)
      if {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound && RulesHelper.getMapNDLevels(fun) == dimOrder.length - 1 && dimOrder.head.ae.evalInt == 0
      } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      val newDimOrder = RulesHelper.removeDimFromDimOrder(dimOrder, 0)
      MapOrderedStream(levels, VectorToOrderedStream.asFunction(), TransposeNDOrderedStream(input, newDimOrder))
  })

  def skipAlternate: Rule = Rule("moveUpVectorToStream_skipAlternate", {
    case Alternate(ArchLambda(p1, MapOrderedStream(fun1: LambdaT, body1, _), _), ArchLambda(p2, MapOrderedStream(fun2: LambdaT, body2, _), _), input, _)if {
      var exprFound1 = false
      var exprFound2 = false
      val innerMostFun1 = RulesHelper.getCurrentOrInnerMostLambda(fun1)
      innerMostFun1.body match {
        case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun1.param.id => exprFound1 = true
        case _ =>
      }
      val innerMostFun2 = RulesHelper.getCurrentOrInnerMostLambda(fun2)
      innerMostFun2.body match {
        case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun2.param.id => exprFound2 = true
        case _ =>
      }
      exprFound2 && exprFound1 && RulesHelper.getMapNDLevels(fun1) == RulesHelper.getMapNDLevels(fun2)
    } =>
      val levels = RulesHelper.getMapNDLevels(fun1)
      MapOrderedStream(levels, VectorToOrderedStream.asFunction(), Alternate(ArchLambda(p1, body1), ArchLambda(p2, body2), input))
  })

  def skipRepeat: Rule = Rule("moveUpVectorToStream_skipRepeat", {
    case Repeat(VectorToOrderedStream(input, _), times, _) =>
      MapOrderedStream(VectorToOrderedStream.asFunction(), Repeat(input, times))
    case Repeat(MapOrderedStream(fun: LambdaT, input, _), times, _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      MapOrderedStream(levels + 1, VectorToOrderedStream.asFunction(), Repeat(input, times))
  })

  def parallelizeFoldMax: Rule = Rule("moveUpVectorToStream_parallelizeFoldMax", {
    case FoldOrderedStream(ArchLambda(p1, ArchLambda(p2, MaxInt(Tuple2(ParamUse(p3), ParamUse(p4), _), _), _), _), VectorToOrderedStream(input, _), _) if p1.id == p4.id && p2.id == p3.id =>
      FoldVector.maximum(input)
  })

  def map2InputFissionND: Rule = Rule("moveUpVectorToStream_map2InputFissionND", {
    case MapOrderedStream2Input(ArchLambda(p1, ArchLambda(p2, MapOrderedStream(vecToStmFun: LambdaT, body, _), _), _), input1, input2, _)
      if {
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(vecToStmFun)
        innerMostBody.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      val levels = RulesHelper.getMapNDLevels(vecToStmFun)
      MapOrderedStream(levels + 1, VectorToOrderedStream.asFunction(),
        MapOrderedStream2Input(ArchLambda(p1, ArchLambda(p2, body)), input1, input2)
      )
  })

  def mapFusionMapParamMap2Input: Rule = Rule("moveUpVectorToStream_mapFusionMapParamMap2Input", {
    // This rule conflicts with MoveUpJoinStream(MapFission)!
    case MapOrderedStream2Input(ArchLambda(p11, ArchLambda(p12, body1, _), _), MapOrderedStream(ArchLambda(p2, body2, _),
    input1, _), input2, _)
      if {
        var exprFound = false
        val vec2stmInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        vec2stmInnerMost.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == vec2stmInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit {
          case ParamUse(p4) if p11.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream2Input({
        val newParam1 = ParamDef()
        val newParam2 = ParamDef()
        ArchLambda(
          newParam1,
          ArchLambda(
            newParam2,
            body1.visitAndRebuild {
              case ParamUse(usedParam1) if usedParam1.id == p11.id =>
                body2.visitAndRebuild {
                  case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                    ParamUse(newParam1)
                  case e => e
                }.asInstanceOf[Expr]
              case ParamUse(usedParam1) if usedParam1.id == p12.id =>
                ParamUse(newParam2)
              case e => e
            }.asInstanceOf[Expr]
          )
        )
      }, input1, input2
      )

    case MapOrderedStream2Input(ArchLambda(p11, ArchLambda(p12, body1, _), _), input1, MapOrderedStream(ArchLambda(p2, body2, _),
    input2, _),  _)
      if {
        var exprFound = false
        val vec2stmInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        vec2stmInnerMost.body match {
          case VectorToOrderedStream(ParamUse(p3), _) if p3.id == vec2stmInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body1.visit {
          case ParamUse(p4) if p12.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      MapOrderedStream2Input({
        val newParam1 = ParamDef()
        val newParam2 = ParamDef()
        ArchLambda(
          newParam1,
          ArchLambda(
            newParam2,
            body1.visitAndRebuild {
              case ParamUse(usedParam1) if usedParam1.id == p12.id =>
                body2.visitAndRebuild {
                  case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                    ParamUse(newParam2)
                  case e => e
                }.asInstanceOf[Expr]
              case ParamUse(usedParam1) if usedParam1.id == p11.id =>
                ParamUse(newParam1)
              case e => e
            }.asInstanceOf[Expr]
          )
        )
      }, input1, input2
      )
  })

  def mapNDSkipZipTuple: Rule = Rule("moveUpVectorToStream_mapNDSkipZipTuple", {
    case MapOrderedStream(ArchLambda(p, body, _), Zip2OrderedStream(Tuple2(
    MapOrderedStream(f1: LambdaT, in1, t1), MapOrderedStream(f2: LambdaT, in2, t2), _), _), _) if {
      var exprFound1 = false
      var exprFound2 = false
      val vec2stmInnerMost1 = RulesHelper.getCurrentOrInnerMostLambda(f1)
      vec2stmInnerMost1.body match {
        case VectorToOrderedStream(ParamUse(p1), _) if p1.id == vec2stmInnerMost1.param.id =>
          exprFound1 = true
        case _ =>
      }
      val vec2stmInnerMost2 = RulesHelper.getCurrentOrInnerMostLambda(f2)
      vec2stmInnerMost2.body match {
        case VectorToOrderedStream(ParamUse(p1), _) if p1.id == vec2stmInnerMost2.param.id =>
          exprFound2 = true
        case _ =>
      }
      exprFound1 && exprFound2 && RulesHelper.getMapNDLevels(f1) == RulesHelper.getMapNDLevels(f2)
    } && t1.asInstanceOf[OrderedStreamTypeT].dimensions == t2.asInstanceOf[OrderedStreamTypeT].dimensions =>
      val level = RulesHelper.getMapNDLevels(f1)
      val newTuple = TypeChecker.check(Zip2OrderedStream(Tuple2(in1, in2)))
      val newParam = ParamDef(newTuple.t.asInstanceOf[OrderedStreamTypeT].et)
      MapOrderedStream(
        ArchLambda(
          newParam,
          body.visitAndRebuild {
            case ParamUse(pd) if pd.id == p.id =>
              Tuple2(
                MapOrderedStream(level-1, VectorToOrderedStream.asFunction(), Select2(ParamUse(newParam), 0)),
                MapOrderedStream(level-1, VectorToOrderedStream.asFunction(), Select2(ParamUse(newParam), 1)))
            case e => e
          }.asInstanceOf[Expr]
        ),
        newTuple
      )

    case e @ MapOrderedStream(ArchLambda(p, body, _), Zip2OrderedStream(Tuple2(MapOrderedStream(f: LambdaT, in1, t), in2, _), _), _) if {
      var exprFound = false
      val vec2stmInnerMost = RulesHelper.getCurrentOrInnerMostLambda(f)
      vec2stmInnerMost.body match {
        case VectorToOrderedStream(ParamUse(p1), _) if p1.id == vec2stmInnerMost.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && t.asInstanceOf[OrderedStreamTypeT].dimensions == in2.t.asInstanceOf[OrderedStreamTypeT].dimensions && {
      var exprFound = false
      in2 match {
        case CounterIntegerND(_) => exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      val level = RulesHelper.getMapNDLevels(f)
      val newInput2 = MapOrderedStream(level, OrderedStreamToVector.asFunction(), in2)
      val newTuple = TypeChecker.check(Zip2OrderedStream(Tuple2(in1, newInput2)))
      val newParam = ParamDef(newTuple.t.asInstanceOf[OrderedStreamTypeT].et)
      MapOrderedStream(
        ArchLambda(
          newParam,
          body.visitAndRebuild {
            case ParamUse(pd) if pd.id == p.id =>
              Tuple2(
                MapOrderedStream(level-1, VectorToOrderedStream.asFunction(), Select2(ParamUse(newParam), 0)),
                MapOrderedStream(level-1, VectorToOrderedStream.asFunction(), Select2(ParamUse(newParam), 1)))
            case e => e
          }.asInstanceOf[Expr]
        ),
        newTuple
      )
  })

  def skipZipTuple: Rule = Rule("skipZipTuple", {
    case Zip2OrderedStream(Tuple2(VectorToOrderedStream(in1, _), VectorToOrderedStream(in2, _), _), _) =>
      VectorToOrderedStream(ZipVector(Tuple2(in1, in2)))
  })

  def skipMapAdd: Rule = Rule("skipMapAdd", {
    case MapOrderedStream(ArchLambda(p1, AddInt(ParamUse(p2), _), _), VectorToOrderedStream(in, _), _) if p1.id == p2.id =>
      VectorToOrderedStream(MapVector(AddInt.asFunction(), in))
  })
}
