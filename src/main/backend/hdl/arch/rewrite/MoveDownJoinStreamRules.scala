package backend.hdl.arch.rewrite

import backend.hdl.arch._
import core.rewrite.{Rule, RulesT}
import core._

object MoveDownJoinStreamRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(skipSelectMapZip)

  def skipSelectMapZip: Rule = Rule("MoveDownJoinStreamRules_skipSelectMapZip", {
    case MapOrderedStream(ArchLambda(p1, body, _), ZipOrderedStream(in, n, _), _) if {
      findSkipSelectMapZipCandidates(p1, body).nonEmpty
    } =>
      val candidates = findSkipSelectMapZipCandidates(p1, body)
      val newInput = ZipOrderedStream(Tuple(Seq
        .range(0, n.ae.evalInt)
        .map(i =>
          if (candidates(i))
            MapOrderedStream(JoinOrderedStream.asFunction(), Select(in, n, i))
          else
            Select(in, n, i)
        ): _*), n)
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(param,
            body.visitAndRebuild {
              case JoinOrderedStream(Select(ParamUse(p2), _, sel, _), _) if p1.id == p2.id && candidates(sel.ae.evalInt) =>
                Select(ParamUse(param), n, sel)
              case Select(ParamUse(p2), _, sel, _) if p1.id == p2.id && !candidates(sel.ae.evalInt) =>
                Select(ParamUse(param), n, sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )
  })

  private def findSkipSelectMapZipCandidates(p1: ParamDef, body: Expr): Set[Int] = {
    val buckets = collection.mutable.HashMap[Int, Int]()
    var numOfSelectParams = 0
    var numOfParams = 0
    body.visit {
      case JoinOrderedStream(Select(ParamUse(p2), _, sel, _), _) if p1.id == p2.id =>
        // decrease the joined occurrences here
        val i = sel.ae.evalInt
        val occ = buckets.getOrElse(i, 0)
        buckets(i) = occ - 1
      case Select(ParamUse(p2), _, sel, _) if p1.id == p2.id =>
        // increase the non-joined occurrences here
        val i = sel.ae.evalInt
        val occ = buckets.getOrElse(i, 0)
        buckets(i) = occ + 1
        numOfSelectParams = numOfSelectParams + 1
      case ParamUse(p2) if p1.id == p2.id =>
        numOfParams = numOfParams + 1
      case _ =>
    }

    if (numOfParams != numOfSelectParams)
      // there are occurrences of param that did not go through a Select node
      Set.empty
    else
      buckets.view.collect {
        // it always go through a join node
        case (sel, 0) => sel
      }.toSet
  }
}
