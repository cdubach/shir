package backend.hdl.arch.rewrite

import backend.hdl.{HWDataTypeT, OrderedStreamTypeT, VectorTypeT}
import backend.hdl.arch.device.Mul2AddInt
import backend.hdl.arch.{AddInt, ArchLambda, ClipBankersRound, ConstrainedSum, JoinUnorderedStream, MapOrderedStream, MapVector, MaxInt, MulInt, Registered, SplitVector, Tuple2, VectorToTuple, VectorToTuple2}
import core.{Conversion, Expr, FunctionCall, ParamDef, ParamUse, TypeChecker}
import backend.hdl.VectorTypeT
import core.{Expr, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}
import core.util.IRDotGraph

object FixTimingRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      pipelineReduceVector,
      balanceTuplePipelineReduceVector,
      //      bufferJoinUnorderedStream, // use rewrites instead to get rid of JoinUnorderedStream
      //      bufferMul, // is now obsolete (the registers are now fixed in the template)
    )

  def otherFixes: Seq[Rule] = Seq(
    regMax,
    regConstrainedSum,
    regConvAdd2,
    regCall,
    regClipBankersRound
  )

  def bufferJoinUnorderedStream: Rule = Rule("bufferJoinUnorderedStream", {
    case JoinUnorderedStream(input, _)
      if (input match {
        case Registered(_, _) => false
        case _ => true
      }) => JoinUnorderedStream(Registered(input))
  })

  def pipelineReduceVector: Rule = Rule("pipelineReduceVector", {
    case MapVector(ArchLambda(p, AddInt(addIn, _), _), SplitVector(splitIn, chunkSize, _), _) =>
      MapVector(ArchLambda(p, Registered(AddInt(addIn))), SplitVector(splitIn, chunkSize))
    //    case SplitVector(MapVector(ArchLambda(p, AddInt(addIn, _), _), mapIn, _), chunkSize, _) =>
    //      SplitVector(MapVector(ArchLambda(p, Registered(AddInt(addIn))), mapIn), chunkSize)
  })

  def balanceTuplePipelineReduceVector: Rule = Rule("balancePipelineReduceVector", {
    case MapVector(ArchLambda(p1, fun, _), input, _) if {
      var expr0Found = false
      fun.visit{
        case Tuple2(pow2, rest, _) if  {
          var expr1Found = false
          pow2.visit{
            case ParamUse(p2) if p1.id == p2.id =>
              expr1Found = true
            case _ =>
          }
          expr1Found
        } && {
          var expr1Found = false
          rest.visit{
            case ParamUse(p2) if p1.id == p2.id =>
              expr1Found = true
            case _ =>
          }
          expr1Found
        } && {
          var expr1Found = false
          val inputtc = TypeChecker.checkIfUnknown(input, input.t)
          if(inputtc.t.asInstanceOf[VectorTypeT].len.ae.evalInt == 1 &&
            inputtc.t.asInstanceOf[VectorTypeT].et.isInstanceOf[VectorTypeT]) {
            val len = inputtc.t.asInstanceOf[VectorTypeT].et.asInstanceOf[VectorTypeT].len
            val pow2Pow = math.floor(math.log(len.ae.evalInt)/math.log(2)).toInt
            val pow2Len = math.pow(2, pow2Pow).toInt
            val restLen = len.ae.evalInt - pow2Len
            val restLenPow = math.floor(math.log(restLen)/math.log(2)).toInt
            val regCountLeft = if(pow2Pow > 2) pow2Pow - 2 else 0
            val regCountRight = if(restLenPow > 2) restLenPow - 2 else 0
            if (regCountLeft > regCountRight)
              expr1Found = true
          }
          expr1Found
        } && {
          var expr1Found = false
          rest.visit{
            case Registered(nonRegIn, _) if {
              var expr2Found = false
              nonRegIn.visit{
                case ParamUse(p2) if p1.id == p2.id =>
                  expr2Found = true
                case _ =>
              }
              expr2Found
            } =>
              expr1Found = true
            case _ =>
          }
          !expr1Found
        } =>
          expr0Found = true
        case _ =>
      }
      expr0Found
    } =>
      val inputtc = TypeChecker.checkIfUnknown(input, input.t)
      val len = inputtc.t.asInstanceOf[VectorTypeT].et.asInstanceOf[VectorTypeT].len
      val pow2Pow = math.floor(math.log(len.ae.evalInt)/math.log(2)).toInt
      val pow2Len = math.pow(2, pow2Pow).toInt
      val restLen = len.ae.evalInt - pow2Len
      val restLenPow = math.floor(math.log(restLen)/math.log(2)).toInt
      val regCountLeft = if(pow2Pow > 2) pow2Pow - 2 else 0
      val regCountRight = if(restLenPow > 2) restLenPow - 2 else 0
      val diff = regCountLeft - regCountRight

      def nestedReg(input: Expr, idx: Int): Expr = {
        if(idx >= diff)
          input
        else
          nestedReg(Registered(input), idx + 1)
      }

      MapVector(ArchLambda(p1, fun.visitAndRebuild{
        case AddInt(Tuple2(pow2, rest, _), _) if {
          var exprFound = false
          rest.visit{
            case ParamUse(p2) if p1.id == p2.id =>
              exprFound = true
            case _ =>
          }
          exprFound
        } =>
          AddInt(Tuple2(pow2, nestedReg(rest, 0)))
        case e => e
      }.asInstanceOf[Expr]), input)
  })

  def regMul2Add: Rule = Rule("regMul2Add", {
    case MapVector(ArchLambda(p, Mul2AddInt(addIn, _), _), input, _) =>
      MapVector(ArchLambda(p, Registered(Mul2AddInt(addIn))), input)
  })

  def regMax: Rule = Rule("regMax", {
    case Conversion(MaxInt(VectorToTuple2(input, _), _), t) =>
      Conversion(Registered(MaxInt(VectorToTuple2(input))), t)
    case ArchLambda(p1, MaxInt(Tuple2(in1, in2, _), _), _)  =>
      ArchLambda(p1, Registered(MaxInt(Tuple2(in1, in2))))
    case MapVector(ArchLambda(p1, MaxInt(VectorToTuple2(ParamUse(p2), _), _), _), input, _) if p1.id == p2.id =>
      MapVector(ArchLambda(p1, Registered(MaxInt(VectorToTuple2(ParamUse(p2))))), input)
  })

  def regConstrainedSum: Rule = Rule("regConstrainedSum", {
    case ArchLambda(p, ConstrainedSum(Tuple2(in1, in2, _), b1, b2, b3, b4, b5, b6, _), _) =>
      ArchLambda(p, Registered(ConstrainedSum(Tuple2(Registered(in1), Registered(in2)), b1, b2, b3, b4, b5, b6)))
  })

  def regConvAdd2: Rule = Rule("regConvAdd2", {
    case Conversion(AddInt(VectorToTuple2(input, _), _), t) =>
      Conversion(Registered(AddInt(VectorToTuple2(input))), t)
  })

  def regCall: Rule = Rule("regCall", {
    case FunctionCall(FunctionCall(ParamUse(pf), input1 @ ParamUse(_), _), input2, t: HWDataTypeT) if {
      var found = false
      input1.t match {
        case ot: OrderedStreamTypeT =>
          ot.leafType match {
            case vt: VectorTypeT if vt.dimensions.length == 1 =>
              found = true
            case _ =>
          }
        case _ =>
      }
      found
    } =>
      val newInput1 = TypeChecker.check(MapOrderedStream(
        input1.t.asInstanceOf[OrderedStreamTypeT].dimensions.length,
        {
          val param = ParamDef(input1.t.asInstanceOf[OrderedStreamTypeT].leafType)
          ArchLambda(param, MapVector(Registered.asFunction(), ParamUse(param)))
        }, input1
      ))
      FunctionCall(FunctionCall(ParamUse(pf), newInput1), input2)

    case FunctionCall(FunctionCall(ParamUse(pf), input1, _), input2 @ParamUse(_), t: HWDataTypeT) if {
      var found = false
      input2.t match {
        case ot: OrderedStreamTypeT =>
          ot.leafType match {
            case vt: VectorTypeT if vt.dimensions.length == 2 =>
              found = true
            case _ =>
          }
        case _ =>
      }
      found
    } =>
      val newInput2 = TypeChecker.check(MapOrderedStream(
        input2.t.asInstanceOf[OrderedStreamTypeT].dimensions.length,
        {
          val param = ParamDef(input2.t.asInstanceOf[OrderedStreamTypeT].leafType)
          ArchLambda(param, MapVector(2, Registered.asFunction(), ParamUse(param)))
        }, input2
      ))
      FunctionCall(FunctionCall(ParamUse(pf), input1), newInput2)
  })

  def regClipBankersRound: Rule = Rule("regClipBankersRound", {
    case ClipBankersRound(input @ ParamUse(_), le, he, _) =>
      ClipBankersRound(Registered(input), le, he)
  })
}