package backend.hdl.arch.rewrite

import backend.hdl.{OrderedStreamTypeT, VectorTypeT}
import backend.hdl.arch.{ArchLambda, JoinOrderedStream, JoinVector, MapOrderedStream, SplitOrderedStream, SplitVector}
import core.rewrite.{Rule, RulesT}

object RemoveJoinSplitRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      removeJoinSplit
      // see skipSplit in MoveUpJoinStreamRules as well
    )

  def removeJoinSplit: Rule = Rule("removeJoinSplit", {
    case JoinOrderedStream(SplitOrderedStream(input, _, _), _) =>
      input
    case JoinVector(SplitVector(input, _, _), _) =>
      input

    case SplitVector(JoinVector(input, _), _, t: VectorTypeT)
      if input.t.asInstanceOf[VectorTypeT].len.ae.evalInt == t.len.ae.evalInt =>
      input
  })

  def simplifyJoinJoinMapSplit: Rule = Rule("simplifyJoinJoinMapSplit", {
    case JoinOrderedStream(JoinOrderedStream(MapOrderedStream(ArchLambda(p1, SplitOrderedStream(body, _, _), _), input, _), _), _) if RulesHelper.containsParam(body, p1)=>
      JoinOrderedStream(MapOrderedStream(ArchLambda(p1, body), input))
  })

  def removeSplitJoin: Rule = Rule("removeSplitJoin", {
    case SplitOrderedStream(JoinOrderedStream(input, _), chunkSize, _) if chunkSize.ae.evalInt == input.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt =>
      input
  })
}
