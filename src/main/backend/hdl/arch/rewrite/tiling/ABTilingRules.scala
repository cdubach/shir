package backend.hdl.arch.rewrite.tiling

import algo.{AlgoLambda, Join, SeqTypeT, Split, TransposeND}
import backend.hdl.arch.rewrite.RulesHelper
import core.{Expr, LambdaT, ParamDef, ParamUse, TypeChecker}
import core.rewrite.Rule

object ABTilingRules {

  /**
    * Given expression MapND(a -> MapND(b -> f(a, b)) o B) o A, the rule tiles input B.
    * @param selectedLevel The Map level that is going to be tiled in MapND(..., B). 0 means the outermost one.
    * @param tileSize The number of elements in a tile.
    * @return MapND(Join) o TransposeND o MapND(a -> Map(bTile -> MapND(b -> f(a, b)) o bTile) o TransposeND o MapND(Split) o B) o A.
    */
  def tilingInnerMap(selectedLevel: Int, tileSize: Int = 128): Rule = Rule("tilingInnerMap", {
    case algo.Map(fun1: LambdaT, input1, _) if
      RulesHelper.containsInput(input1) &&
        RulesHelper.containsInput(fun1) && {
        var exprFound = false
        val fun1InnerMostLambda = RulesHelper.getCurrentOrInnerMostLambda(fun1)
        fun1InnerMostLambda.visit{
          case algo.Map(fun2: LambdaT, input2, _) if
            RulesHelper.containsInput(input2) &&
              RulesHelper.containsParam(fun2.body, fun1InnerMostLambda.param) =>
            // Check if selected level can be tiled
            val dims = input2.t.asInstanceOf[SeqTypeT].dimensions
            val totalLevels = RulesHelper.getMapNDLevels(fun2)
            if(selectedLevel < totalLevels) {
              val selectedAxis = dims.length - 1 - selectedLevel
              exprFound = dims(selectedAxis).ae.evalInt > tileSize && dims(selectedAxis).ae.evalInt % tileSize == 0
            }
          case _ =>
        }
        exprFound
      }
    =>
      // Get number of levels for MapND(fun1, input1)
      val fun1Levels = RulesHelper.getMapNDLevels(fun1)

      // Rebuild input2 with tiling
      val mapTiledInput = algo.Map(
        {
          val param1 = ParamDef()
          AlgoLambda(
            param1,
            fun1.body.visitAndRebuild{
              case algo.Map(fun2: LambdaT, input2, _) if RulesHelper.containsInput(input2) &&
                RulesHelper.containsParam(fun2.body, fun1.param) =>
                val dims = input2.t.asInstanceOf[SeqTypeT].dimensions
                val newAxis = dims.length - selectedLevel

                // Split targeted level
                val splitInput2 = algo.Map(selectedLevel, Split.asFunction(Seq(None), Seq(tileSize)), input2)
                val splitInput2tc = TypeChecker.check(splitInput2)

                // Calculate transposition for split input2
                val splitInputDimOrder = RulesHelper.generateCountSeq(0, splitInput2tc.t.asInstanceOf[SeqTypeT].dimensions.length)
                val newSplitInputDimOrder = RulesHelper.moveDimInDimOrder(splitInputDimOrder, newAxis, splitInputDimOrder.length - 1)
                val transposeInput = TransposeND(splitInput2tc, newSplitInputDimOrder)
                val transposeInputtc = TypeChecker.check(transposeInput)

                // Extract innermost Lambda in MapND(fun2, input2) and rebuild it with TransposeND o MapND(Split) o input2
                val innerLambda = RulesHelper.getCurrentOrInnerMostLambda(fun2)
                val innerLevels = RulesHelper.getMapNDLevels(fun2)
                val splitInnerLevels = innerLevels + 1
                val rebuildInnerFun = algo.Map(splitInnerLevels, innerLambda, transposeInputtc)
                val rebuildInnerFunWithOuter =  rebuildInnerFun.visitAndRebuild{
                  case ParamUse(usedParam) if  usedParam.id == fun1.param.id =>
                    ParamUse(param1)
                  case e => e
                }.asInstanceOf[Expr]
                val rebuildInnerFunWithOutertc = TypeChecker.check(rebuildInnerFunWithOuter)
                rebuildInnerFunWithOutertc
              case e => e
            }.asInstanceOf[Expr]
          )
        }, input1
      )
      val mapTiledInputtc = TypeChecker.check(mapTiledInput)

      // Transpose back the output to maintain the original dimension order
      val mapTiledInputDimOrder = RulesHelper.generateCountSeq(0, mapTiledInputtc.t.asInstanceOf[SeqTypeT].dimensions.length)
      val movedAxis = mapTiledInputDimOrder.length - 1 - (selectedLevel + fun1Levels)
      val recoveredAxis = mapTiledInputDimOrder.length - 1 - fun1Levels
      val recoverDimOrder = RulesHelper.moveDimInDimOrder(mapTiledInputDimOrder, movedAxis, recoveredAxis)
      val recoveredOutput = TransposeND(mapTiledInputtc, recoverDimOrder)
      val recoveredOutputtc = TypeChecker.check(recoveredOutput)

      // Shape the dimension back
      val recoverLevels = selectedLevel + fun1Levels
      TypeChecker.check(algo.Map(recoverLevels, Join.asFunction(), recoveredOutputtc))
  })

  /**
    * Given expression MapND(a -> MapND(b -> f(a, b)) o B) o A, the rule swaps A and B.
    * Use this rule with TilingInnerMap to fulfill TilingOuterMap.
    * @param reservedInnerLevels Number of inner Map levels in B which will not be swapped.
    * @return TransposeND o MapND(b' -> MapND(a -> MapND(b -> f(a, b)) o b') o A) o B.
    */
  def exchangeABMapNoCondition(reservedInnerLevels: Int): Rule = Rule("exchangeABMapNoCondition", {
    case algo.Map(fun1: LambdaT, input1, t: SeqTypeT) if
      RulesHelper.containsInput(input1) &&
        RulesHelper.containsInput(fun1) && {
        var exprFound = false
        val fun1InnerMostLambda = RulesHelper.getCurrentOrInnerMostLambda(fun1)
        fun1InnerMostLambda.visit{
          case algo.Map(fun2: LambdaT, input2, _) if
            RulesHelper.containsInput(input2) &&
              RulesHelper.containsParam(fun2.body, fun1InnerMostLambda.param) =>
            exprFound = true
          case _ =>
        }
        exprFound
      }
    =>
      // Get fun2 and input2
      val fun1InnerMostLambda = RulesHelper.getCurrentOrInnerMostLambda(fun1)
      var input2 = input1
      var fun2 = fun1
      fun1InnerMostLambda.visit{
        case algo.Map(f2: LambdaT, i2, _) if
          RulesHelper.containsInput(i2) &&
            RulesHelper.containsParam(f2.body, fun1InnerMostLambda.param) =>
          input2 = i2
          fun2 = f2
        case _ =>
      }

      // Get targeted function with given level
      val fun1Levels = RulesHelper.getMapNDLevels(fun1)
      val fun2Levels = RulesHelper.getMapNDLevels(fun2)
      val nonReservedFun2Levels = fun2Levels - reservedInnerLevels
      val origFun = RulesHelper.findLambdaWithGivenLevel(fun2, nonReservedFun2Levels - 1).get
      val origFuntc = TypeChecker.check(origFun)

      // Reorder Maps
      val newFun1 = AlgoLambda(fun1InnerMostLambda.param, origFuntc.asInstanceOf[LambdaT].body)
      val map1NewFun = algo.Map(fun1Levels, newFun1, input1)
      val map1NewFuntc = TypeChecker.check(map1NewFun)
      val newFun2 = AlgoLambda(origFuntc.asInstanceOf[LambdaT].param, map1NewFuntc)
      val map2NewFun = algo.Map(nonReservedFun2Levels, newFun2, input2)
      val map2NewFuntc = TypeChecker.check(map2NewFun)

      // Calculate transposition
      val numOfDims = t.dimensions.length
      val numOfRestDims = numOfDims - fun1Levels - fun2Levels
      val newFun1DimOrder = RulesHelper.generateCountSeq(numOfRestDims + reservedInnerLevels, fun1Levels)
      val newFun2DimOrderHead = RulesHelper.generateCountSeq(numOfRestDims, reservedInnerLevels)
      val newFun2DimOrderTail = RulesHelper.generateCountSeq(numOfRestDims + reservedInnerLevels + fun1Levels, nonReservedFun2Levels)
      val newDimOrder = newFun2DimOrderHead  ++ newFun2DimOrderTail ++ newFun1DimOrder
      val newFunDimOrderHead = RulesHelper.generateCountSeq(0, numOfRestDims)
      TransposeND(map2NewFuntc, newFunDimOrderHead ++ newDimOrder)
  })
}
