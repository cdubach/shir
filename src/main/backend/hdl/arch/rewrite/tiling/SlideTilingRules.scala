package backend.hdl.arch.rewrite.tiling

import backend.hdl.{OrderedStreamTypeT, VectorTypeT}
import backend.hdl.arch.{MapOrderedStream, SlideGeneralOrderedStream, SlideOrderedStream, SplitOrderedStream, VectorToOrderedStream}
import core._
import core.rewrite.{Rule, RulesT}

object SlideTilingRules extends RulesT {
  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      tilingSlide,
    )

  def tilingSlide: Rule = Rule("tilingSlide", {
    case SplitOrderedStream(SlideOrderedStream(input, windowSize, stepSize, _), chunkSize, _)
      if input.t.asInstanceOf[OrderedStreamTypeT].et.isInstanceOf[VectorTypeT] =>
      val tileWindowSize = (chunkSize.ae - 1) * stepSize.ae + windowSize.ae
      val tileStepSize = chunkSize.ae * stepSize.ae
      val slideTiles = SlideGeneralOrderedStream(input, tileWindowSize, tileStepSize)
      MapOrderedStream(SlideOrderedStream.asFunction(Seq(None), Seq(windowSize, stepSize)), slideTiles)
  })
}
