package backend.hdl.arch.rewrite

import backend.hdl.arch.device.Mul2AddInt
import backend.hdl.{IntTypeT, OrderedStreamTypeT, ScalarTypeT}
import backend.hdl.arch.{AddInt, AddInt2, ArchLambda, ClipBankersRound, FoldOrderedStream, FoldVector, MapOrderedStream, MapVector, MulInt, OrderedStreamToVector, ResizeInteger, SplitOrderedStream, SplitVector, Tuple2, VectorToOrderedStream, VectorToTuple2, ZipVector}
import core.{Conversion, Expr, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object ParallelizeDotProductRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(parallelizeDotProduct(config.getOrElse(2))) ++
    Seq(CleanupRules.joinWrites) ++
    RemoveStreamVectorConversionRules.get() ++
    MoveUpVectorToStreamRules.get() ++
    MoveUpJoinStreamRules.all(None) ++
    Seq(MoveUpJoinStreamRules.moveIntoFold) ++
    //      MoveDownStreamToVectorRules.get ++ // try to avoid
    MoveDownSplitStreamRules.get() // try to keep set minimal (use MoveUpJoinStream more)

  def createMultipleDotProd: Seq[Rule] =
    Seq(multipleDotProd) ++ Seq(MoveDownStreamToVectorRules.skipZipStream3DTuple, MoveDownStreamToVectorRules.mapFission)

  def parallelizeDotProduct(parMuls: Int = 2): Rule = Rule("parallelizeDotProduct", {
    case FoldOrderedStream(
    ArchLambda(d0, ArchLambda(d1, AddInt(Tuple2(ParamUse(u0), ParamUse(u1), _), _), _), _),
    MapOrderedStream(mulfunc @ ArchLambda(d3, MulInt(ParamUse(u3), _), _), input, mapt: OrderedStreamTypeT), resTy)
      if (
        ((d0.id == u0.id && d1.id == u1.id) || (d0.id == u1.id && d1.id == u0.id))
          && d3.id == u3.id
          && parMuls > 1 && mapt.len.ae.evalInt > 1 && mapt.len.ae.evalInt == parMuls
        ) =>
      // Do not introduce Fold if lengths match.
      val reduction = TypeChecker.check(FoldVector.sum(
        MapVector(
          mulfunc,
          OrderedStreamToVector(input)
        )
      ))
      (resTy, reduction.t) match {
        case (a: IntTypeT, b: IntTypeT) if a.bitWidth.ae.evalInt != b.bitWidth.ae.evalInt =>
          ResizeInteger(reduction, a.bitWidth.ae.evalInt)
        case _ => reduction
      }
    case FoldOrderedStream(
      ArchLambda(d0, ArchLambda(d1, AddInt(Tuple2(ParamUse(u0), ParamUse(u1), _), _), _), _),
      MapOrderedStream(mulfunc @ ArchLambda(d3, MulInt(ParamUse(u3), _), _), input, mapt: OrderedStreamTypeT), resTy)
    if (
      ((d0.id == u0.id && d1.id == u1.id) || (d0.id == u1.id && d1.id == u0.id))
      && d3.id == u3.id
      && parMuls > 1 && mapt.len.ae.evalInt > 1 && mapt.len.ae.evalInt % parMuls == 0
    ) =>
      val newInput = TypeChecker.check(SplitOrderedStream(input, parMuls))
      val reduction = TypeChecker.check(FoldOrderedStream(
        // do not reuse the add function here, because input precision will be different
        AddInt2.asFunction(),
        MapOrderedStream(
          {
            val p = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              p,
              FoldVector.sum(
                MapVector(
                  mulfunc,
                  OrderedStreamToVector(ParamUse(p))
                )
              )
            )
          },
          newInput
        )
      ))

      // Consider this: parallelize input of Seq[Tuple[Int(8);Int(9)]; 25] by 5.
      // the original reduction gives Int(8+9 + ceil(log2(25))) = Int(22)
      // the parallel reduction gives Int(8+9 + ceil(log2(5)) + ceil(log2(5))) = Int(23)
      //
      // in a case like this one, resize the output to match the original.
      (resTy, reduction.t) match {
        case (a: IntTypeT, b: IntTypeT) if a.bitWidth.ae.evalInt != b.bitWidth.ae.evalInt =>
          ResizeInteger(reduction, a.bitWidth.ae.evalInt)
        case _ => reduction
      }
  })

  def isParallelDotProd(expr: Expr, param: ParamDef): Boolean = expr match {
    case MapVector(ArchLambda(_, AddInt(VectorToTuple2(_, _), _), _), SplitVector(input, _, _), _) => isParallelDotProd(input, param)
    case MapVector(ArchLambda(_, Mul2AddInt(VectorToTuple2(ParamUse(_), _), _), _),
    SplitVector(ZipVector(ParamUse(pd),  _), _, _), _) if pd.id == param.id => true
    case _ => false
  }

  def multipleDotProd: Rule = Rule("multipleDotProd", {
    case MapOrderedStream(dotProdFun @ ArchLambda(p, AddInt(VectorToTuple2(_, _), _), _), input, _) if
      !p.t.isInstanceOf[OrderedStreamTypeT] =>
      VectorToOrderedStream(MapVector(dotProdFun, OrderedStreamToVector(input)))
    case MapOrderedStream(dotProdFun @ ArchLambda(p, AddInt(VectorToTuple2(innerIn, _), _), _), input, _) if
      isParallelDotProd(innerIn, p) && !p.t.isInstanceOf[OrderedStreamTypeT] =>
      VectorToOrderedStream(MapVector(dotProdFun, OrderedStreamToVector(input)))
    case MapOrderedStream(dotProdFun @ ArchLambda(p, Conversion(ClipBankersRound(AddInt(VectorToTuple2(innerIn, _), _), cp1, cp2, _), _), _), input, _) if
      isParallelDotProd(innerIn, p) && !p.t.isInstanceOf[OrderedStreamTypeT] =>
      VectorToOrderedStream(MapVector(dotProdFun, OrderedStreamToVector(input)))
    case MapOrderedStream(dotProdFun @ ArchLambda(p, ClipBankersRound(AddInt(VectorToTuple2(innerIn, _), _), cp1, cp2, _), _), input, _) if
      isParallelDotProd(innerIn, p) &&  !p.t.isInstanceOf[OrderedStreamTypeT] =>
      VectorToOrderedStream(MapVector(dotProdFun, OrderedStreamToVector(input)))
  })

  def parallelizeSum: Rule = Rule("ParallelizeSum", {
    case FoldOrderedStream(ArchLambda(_, ArchLambda(_, AddInt(Tuple2(_, _, _), _), _), _), input, foldT) if foldT.isInstanceOf[ScalarTypeT]=>
      FoldVector.sum(
        TypeChecker.check(OrderedStreamToVector(input))
      )
  })
}
