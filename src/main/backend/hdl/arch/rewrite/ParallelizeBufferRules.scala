package backend.hdl.arch.rewrite

import backend.hdl.{LogicType, OrderedStreamType, OrderedStreamTypeT, SignedIntType, VectorType, VectorTypeT}
import backend.hdl.arch.{AddInt, ArchLambda, ArchLambdas, FoldOrderedStream, JoinOrderedStream, JoinVector, MapOrderedStream, MapVector, OrderedStreamToVector, SplitOrderedStream, SplitVector, Tuple2, VectorToOrderedStream, Zip2OrderedStream, ZipVector}
import core.{Conversion, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}

object ParallelizeBufferRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] = Seq(
    parallelizeFold2DBuffer(config.getOrElse(4))
  )

  def parallelizeFold2DBuffer(parElements: Int = 4): Rule = Rule("parallelizeFold2DBuffer", {
    case FoldOrderedStream(ArchLambda(p1, ArchLambda(p2, MapOrderedStream(ArchLambda(p3, AddInt(ParamUse(p4), _), _),
    Zip2OrderedStream(Tuple2(ParamUse(p5), ParamUse(p6), _), _), _), _), _), input, t: OrderedStreamTypeT) if
      t.dimensions.length == 1 && p3.id == p4.id && p1.id == p6.id && p2.id == p5.id =>

      // Vectorize input
      val splitInput = TypeChecker.check(MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(parElements)), input))
      val vecInput = TypeChecker.check(MapOrderedStream(2, OrderedStreamToVector.asFunction(), splitInput))

      // Calculate bit width
      val intBitWidth = vecInput.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].et.bitWidth
      val vecBitWidth = vecInput.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].bitWidth

      val innerLen = vecInput.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt
      val outputIntBitWidth = t.leafType.bitWidth.ae.evalInt
      val outputVecBitWidth = outputIntBitWidth * parElements
      val bufferType = OrderedStreamType(SignedIntType(outputVecBitWidth), innerLen)

      // convert into large int
      val convertedInput = TypeChecker.check(MapOrderedStream(
        2,
        {
          val param1 = ParamDef(vecInput.t.asInstanceOf[OrderedStreamTypeT].leafType)
          ArchLambda(
            param1,
            Conversion(JoinVector(MapVector(
              {
                val param2 = ParamDef(param1.t.asInstanceOf[VectorTypeT].et)
                ArchLambda(
                  param2,
                  Conversion(ParamUse(param2), VectorType(LogicType(), intBitWidth))
                )
              }, ParamUse(param1)
            )), SignedIntType(vecBitWidth))
          )
        }, vecInput
      ))

      // Buffer Sum Calulation
      val tmpSum = TypeChecker.check(FoldOrderedStream(
        {
          val param1 = ParamDef(bufferType)
          val param2 = ParamDef(convertedInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambdas(Seq(param1, param2),
            {
              // data convert in
              val newParam1 = TypeChecker.check(MapOrderedStream(
                {
                  val param3 = ParamDef(param1.t.asInstanceOf[OrderedStreamTypeT].leafType)
                  ArchLambda(
                    param3,
                    MapVector(
                      {
                        val param4 = ParamDef()
                        ArchLambda(
                          param4,
                          Conversion(ParamUse(param4), SignedIntType(outputIntBitWidth))
                        )
                      }, SplitVector(Conversion(ParamUse(param3), VectorType(LogicType(), outputVecBitWidth)), outputIntBitWidth)
                    )
                  )
                }, ParamUse(param1)
              ))
              val newParam2 = TypeChecker.check(MapOrderedStream(
                {
                  val param3 = ParamDef(param2.t.asInstanceOf[OrderedStreamTypeT].leafType)
                  ArchLambda(
                    param3,
                    MapVector(
                      {
                        val param4 = ParamDef()
                        ArchLambda(
                          param4,
                          Conversion(ParamUse(param4), SignedIntType(intBitWidth))
                        )
                      }, SplitVector(Conversion(ParamUse(param3), VectorType(LogicType(), vecBitWidth)), intBitWidth)
                    )
                  )
                }, ParamUse(param2)
              ))

              // Apply sum
              val newTup = TypeChecker.check(MapOrderedStream(ZipVector.asFunction(), Zip2OrderedStream(Tuple2(newParam1, newParam2))))
              val newSum = TypeChecker.check(
                MapOrderedStream(
                  {
                    val param3 = ParamDef(newTup.t.asInstanceOf[OrderedStreamTypeT].et)
                    ArchLambda(
                      param3,
                      MapVector(AddInt.asFunction(), ParamUse(param3))
                    )
                  }, newTup
                )
              )
              MapOrderedStream(
                {
                  val param3 = ParamDef(newSum.t.asInstanceOf[OrderedStreamTypeT].leafType)
                  ArchLambda(
                    param3,
                    Conversion(JoinVector(MapVector(
                      {
                        val param4 = ParamDef(newSum.t.asInstanceOf[OrderedStreamTypeT].leafType.asInstanceOf[VectorTypeT].et)
                        ArchLambda(
                          param4,
                          Conversion(ParamUse(param4), VectorType(LogicType(), outputIntBitWidth))
                        )
                      }, ParamUse(param3)
                    )), SignedIntType(outputVecBitWidth))
                  )
                }, newSum
              )
            }
          )
        },
        convertedInput
      ))

      // Convert Buffered Large Int back to vector of Ints
      val newOutput = TypeChecker.check(MapOrderedStream(
        {
          val param3 = ParamDef(tmpSum.t.asInstanceOf[OrderedStreamTypeT].leafType)
          ArchLambda(
            param3,
            MapVector(
              {
                val param4 = ParamDef()
                ArchLambda(
                  param4,
                  Conversion(ParamUse(param4), SignedIntType(outputIntBitWidth))
                )
              }, SplitVector(Conversion(ParamUse(param3), VectorType(LogicType(), outputVecBitWidth)), outputIntBitWidth)
            )
          )
        }, tmpSum
      ))

      val reshapeBack = TypeChecker.check(JoinOrderedStream(MapOrderedStream(VectorToOrderedStream.asFunction(), newOutput)))

      reshapeBack
  })

}