package backend.hdl.arch.rewrite

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.rewrite.sharedFunc.ReshapeFuncRules
import backend.hdl.arch.{Alternate, ArchLambda, ConcatOrderedStream, FlipTuple2, MapOrderedStream, Repeat, RepeatHidden, Select2, SplitOrderedStream, Tuple2, Zip2OrderedStream}
import core.{ArithType, ArithTypeT, Conversion, Expr, LambdaT, Let, Marker, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}
import core.util.IRDotGraph

object MoveDownSplitStreamRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      moveBelowMap,
      moveInTuple

      //      moveDownSplitStream1,
      //      moveDownMapSplitStream1,
      //      moveDownMapSplitStream2,
      //      moveDownMapSplitStream3,
    )

  def mergeIntoReadAddress: Seq[Rule] =
    Seq(
      mapFusion,
      mapFissionMapSplitStream,
      reorderSplits,
      moveDownMapSplitStream1,
      moveDownMapSplitStream2,
      moveDownMapSplitStream4,
      moveDownMapSplitStream5,
      skipConcat,
      moveInAlternate,
      mergeRepeat,
    ) ++ all(None) ++ Seq(MoveUpJoinStreamRules.skipSplit) ++
      Seq(MergeIntoCounterRules.mergeMapSplitStream, MergeIntoCounterRules.mergeSplitStream,
        MergeIntoCounterRules.nDmergeSplitStream, ReshapeFuncRules.moveOutMapSplitStm)

  def skipTupleStructure: Seq[Rule] =
    Seq(
      mapFusion,
      moveDownMapSplitStream1,
      skipSelectTuple,
      mapFissionMapSplitStream,
      mapFusionMapParam,
      skipZips,
      skipZipTuple
    ) ++ all(None) ++ Seq(ReshapeFuncRules.moveOutMapSplitStm, RemoveSelectRules.removeSelect)

  def moveBelowMap: Rule = Rule("moveDownSplitStream_moveBelowMap", {
    case SplitOrderedStream(MapOrderedStream(f: LambdaT, input, _), chunkSize, _) if {
      var exprFound = false
      f.body match{
        case SplitOrderedStream(ParamUse(p), _, _) if p.id == f.param.id =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      MapOrderedStream(
        {
          val p = ParamDef()
          ArchLambda(p,
            MapOrderedStream(f, ParamUse(p))
          )
        },
        SplitOrderedStream(input, chunkSize)
      )
  })

  def moveInTuple: Rule = Rule("moveDownSplitStream_moveInTuple", {
    case SplitOrderedStream(Zip2OrderedStream(Tuple2(input1, input2, _), _), chunkSize, _) =>
      MapOrderedStream(
        Zip2OrderedStream.asFunction(),
        Zip2OrderedStream(
          Tuple2(
            SplitOrderedStream(input1, chunkSize),
            SplitOrderedStream(input2, chunkSize)
          )
        )
      )
  })

  def moveDownMapSplitStream1: Rule = Rule("moveDownMapSplitStream1", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case SplitOrderedStream(ParamUse(p2), _, _) if p1.id == p2.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val alreadyApplied: Boolean = body match {
        case SplitOrderedStream(ParamUse(p2), _, _) if p1.id == p2.id => true
        case _ => false
      }
      !alreadyApplied && exprFound && paramUses == 1
    } =>
      var chunkSize: ArithTypeT = 0
      val newFun = {
        val newParam = ParamDef()
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case SplitOrderedStream(ParamUse(p2), cs, _) if p1.id == p2.id =>
              chunkSize = cs
              ParamUse(newParam)
            case ParamUse(pd) if pd.id == newParam.id =>
              ParamUse(newParam)
            case e => e
          }.asInstanceOf[Expr]
        )
      }

      MapOrderedStream(
        newFun,
        MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), input)
      )
  })

  def moveDownMapSplitStream2: Rule = Rule("moveDownMapSplitStream2", {
    case MapOrderedStream(fun@ArchLambda(p1, SplitOrderedStream(ParamUse(p2), _, _), _), Marker(input, text), _) if p1.id == p2.id =>
      Marker(MapOrderedStream(fun, input), text)
    case MapOrderedStream(ArchLambda(p3, MapOrderedStream(fun@ArchLambda(p1, SplitOrderedStream(ParamUse(p2), _, _), _), ParamUse(p4), _), _), Marker(input, text), _) if p1.id == p2.id && p3.id == p4.id =>
      Marker(MapOrderedStream(2, fun, input), text)
    case MapOrderedStream(fun@ArchLambda(p1, SplitOrderedStream(ParamUse(p2), _, _), _), RepeatHidden(input, repetitions, _), _) if p1.id == p2.id =>
      RepeatHidden(MapOrderedStream(fun, input), repetitions)
    case SplitOrderedStream(Let(param, body, arg, _), chunkSize, _) =>
      Let(param, SplitOrderedStream(body, chunkSize), arg)
    case SplitOrderedStream(Conversion(input, _), chunkSize, t) =>
      Conversion(SplitOrderedStream(input, chunkSize), t)
  })

  def moveDownMapSplitStream3: Rule = Rule("moveDownMapSplitStream3", {
    // map fusion
    case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), chunkSize, _), _), MapOrderedStream(ArchLambda(param, body, _), input, _), _) if p1.id == p2.id =>
      MapOrderedStream(
        {
          ArchLambda(
            param,
            SplitOrderedStream(body, chunkSize)
          )
        },
        input
      )
  })

  def moveDownMapSplitStream4: Rule = Rule("moveDownMapSplitStream4", {
    case MapOrderedStream(splitFun: LambdaT, Let(param, body, arg, _), _) if {
      var exprFound = false
      val splitFunInnerMost = RulesHelper.getCurrentOrInnerMostLambda(splitFun)
      splitFunInnerMost.body match {
        case SplitOrderedStream(ParamUse(p), _, _) if p.id == splitFunInnerMost.param.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      Let(param, MapOrderedStream(splitFun, body), arg)
  })

  def moveDownMapSplitStream5: Rule = Rule("moveDownMapSplitStream5", {
    case SplitOrderedStream(Marker(input, text), chunkSize, _) =>
      Marker(SplitOrderedStream(input, chunkSize), text)
    case MapOrderedStream(splitFun: LambdaT, Marker(input, text), _)
      if {
        var exprFound = false
        val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(splitFun)
        innerMostBody.body match {
          case SplitOrderedStream(ParamUse(p), _, _) if p.id == innerMostBody.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      Marker(MapOrderedStream(splitFun, input), text)
  })

  def mapFusion: Rule = Rule("moveDownMapSplitStream_mapFusion", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        body1.visit{
          case SplitOrderedStream(ParamUse(p3), chunkSize, _) if p1.id == p3.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val splitInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        splitInnerMost.body match {
          case SplitOrderedStream(ParamUse(p3), _, _) if p3.id == splitInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )

    // map map fusion
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        body1.visit{
          case MapOrderedStream(ArchLambda(p3, SplitOrderedStream(ParamUse(p4), chunkSize, _), _), ParamUse(p5), _) if
            p1.id == p5.id && p3.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val splitInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        splitInnerMost.body match {
          case SplitOrderedStream(ParamUse(p3), _, _) if p3.id == splitInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def mapFusionMapParam: Rule = Rule("moveDownMapSplitStream_mapFusionMapParam", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        val splitInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body1))
        splitInnerMost.body match {
          case SplitOrderedStream(ParamUse(p3), _, _) if p3.id == splitInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val splitInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        splitInnerMost.body match {
          case SplitOrderedStream(ParamUse(p3), _, _) if p3.id == splitInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def mapFissionMapSplitStream: Rule = Rule("moveDownMapSplitStream_mapFissionMapSplitStream", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case MapOrderedStream(ArchLambda(p3, SplitOrderedStream(ParamUse(p4), _, _), _), ParamUse(p2), _) if p1.id == p2.id && p3.id == p4.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val alreadyApplied: Boolean = body match {
        case MapOrderedStream(ArchLambda(p3, SplitOrderedStream(ParamUse(p4), _, _), _), ParamUse(p2), _) if p1.id == p2.id && p3.id == p4.id => true
        case _ => false
      }
      !alreadyApplied && exprFound && paramUses == 1
    } =>
      var chunkSize: ArithTypeT = 0
      val newFun = {
        val newParam = ParamDef()
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case MapOrderedStream(ArchLambda(p3, SplitOrderedStream(ParamUse(p4), cs, _), _), ParamUse(p2), _) if (p1.id == p2.id || p2.id == newParam.id) && p3.id == p4.id =>
              chunkSize = cs
              ParamUse(newParam)
            case ParamUse(pd) if pd.id == newParam.id =>
              ParamUse(newParam)
            case e => e
          }.asInstanceOf[Expr]
        )
      }

      MapOrderedStream(
        newFun,
        MapOrderedStream(2, SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), input)
      )

    // map map fission
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, SplitOrderedStream(ParamUse(p5), _, _), _), ParamUse(p6), _), _), ParamUse(p2), _)
          if p1.id == p2.id && p3.id == p6.id && p4.id == p5.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val alreadyApplied: Boolean = body match {
        case MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, SplitOrderedStream(ParamUse(p5), _, _), _), ParamUse(p6), _), _), ParamUse(p2), _)
          if p1.id == p2.id && p3.id == p6.id && p4.id == p5.id => true
        case _ => false
      }
      !alreadyApplied && exprFound && paramUses == 1
    } =>
      var chunkSize: ArithTypeT = 0
      val newFun = {
        val newParam = ParamDef()
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, SplitOrderedStream(ParamUse(p5), cs, _), _), ParamUse(p6), _), _), ParamUse(p2), _) if
              (p1.id == p2.id || p2.id == newParam.id) && p3.id == p6.id && p4.id == p5.id =>
              chunkSize = cs
              ParamUse(newParam)
            case ParamUse(pd) if pd.id == newParam.id =>
              ParamUse(newParam)
            case e => e
          }.asInstanceOf[Expr]
        )
      }

      MapOrderedStream(
        newFun,
        MapOrderedStream(3, SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), input)
      )
  })

  def reorderSplits: Rule = Rule("moveUpSplitStream_reorderSplits", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, SplitOrderedStream(ParamUse(p3), chunkSize2, _), _), ParamUse(p4),  _), _), SplitOrderedStream(input, chunkSize1, _),  _) if p1.id == p4.id && p2.id == p3.id =>
      SplitOrderedStream(MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize2)), input), chunkSize1)
  })

  def skipSelectTuple: Rule = Rule("moveDownSplitStream_skipSelectTuple", {
    case MapOrderedStream(fun: LambdaT, Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if {
      var exprFound = false
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), _, _), _), Select2(ParamUse(p), sel, _), _)
          if p1.id == p2.id && fun.param.id == p.id && sel.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      var chunkSize: ArithTypeT = ArithType(0)
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), cs, _), _), Select2(ParamUse(p), sel, _), _)
          if p1.id == p2.id && fun.param.id == p.id && sel.ae.evalInt == 0 =>
          chunkSize = cs
        case _ =>
      }

      val newInput = TypeChecker.check(Zip2OrderedStream(
        Tuple2(MapOrderedStream(2, SplitOrderedStream.asFunction(Seq(None),  Seq(chunkSize)), input1), input2)
      ))

      MapOrderedStream(
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            fun.body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), _, _), _), Select2(ParamUse(p), sel, _), _)
                if p1.id == p2.id && fun.param.id == p.id && sel.ae.evalInt == 0 =>
                Select2(ParamUse(param), sel)
              case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt != 0 =>
                Select2(ParamUse(param), sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        newInput
      )

    case MapOrderedStream(fun: LambdaT, Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if {
      var exprFound = false
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), _, _), _), Select2(ParamUse(p), sel, _), _)
          if p1.id == p2.id && fun.param.id == p.id && sel.ae.evalInt == 1 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      var chunkSize: ArithTypeT = ArithType(0)
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), cs, _), _), Select2(ParamUse(p), sel, _), _)
          if p1.id == p2.id && fun.param.id == p.id && sel.ae.evalInt == 1 =>
          chunkSize = cs
        case _ =>
      }

      val newInput = TypeChecker.check(Zip2OrderedStream(
        Tuple2(input1, MapOrderedStream(2, SplitOrderedStream.asFunction(Seq(None),  Seq(chunkSize)), input2))
      ))

      MapOrderedStream(
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            fun.body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), _, _), _), Select2(ParamUse(p), sel, _), _)
                if p1.id == p2.id && fun.param.id == p.id && sel.ae.evalInt == 1 =>
                Select2(ParamUse(param), sel)
              case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt != 1 =>
                Select2(ParamUse(param), sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        newInput
      )
  })

  def skipConcat: Rule = Rule("moveDownSplitStream_skipConcat", {
    case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), cs, _), _), ConcatOrderedStream(in0, Repeat(Repeat(in1, rep1, _), rep2, _), dim, _), _) if p1.id == p2.id && dim.ae.evalInt == 0=>
      ConcatOrderedStream(MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(cs)), in0), Repeat(Repeat(Repeat(in1, cs), rep1.ae / cs.ae), rep2), dim)
    case MapOrderedStream(ArchLambda(p1, SplitOrderedStream(ParamUse(p2), cs, _), _), ConcatOrderedStream(Repeat(Repeat(in0, rep1, _), rep2, _), in1, dim, _), _) if p1.id == p2.id && dim.ae.evalInt == 0=>
      ConcatOrderedStream(Repeat(Repeat(Repeat(in0, cs), rep1.ae / cs.ae), rep2), MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(cs)), in1), dim)

    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, SplitOrderedStream(ParamUse(p3), cs, _), _), ParamUse(p4), _), _), ConcatOrderedStream(in0, Repeat(Repeat(Repeat(in1, rep1, _), rep2, _), rep3, _), dim, _), _) if p1.id == p4.id && p2.id == p3.id && dim.ae.evalInt == 0=>
      ConcatOrderedStream(MapOrderedStream(2, SplitOrderedStream.asFunction(Seq(None), Seq(cs)), in0), Repeat(Repeat(Repeat(Repeat(in1, cs), rep1.ae / cs.ae), rep2), rep3), dim)
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, SplitOrderedStream(ParamUse(p3), cs, _), _), ParamUse(p4), _), _), ConcatOrderedStream(Repeat(Repeat(Repeat(in0, rep1, _), rep2, _), rep3, _), in1, dim, _), _) if p1.id == p4.id && p2.id == p3.id && dim.ae.evalInt == 0=>
      ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(in0, cs), rep1.ae / cs.ae), rep2), rep3), MapOrderedStream(2, SplitOrderedStream.asFunction(Seq(None), Seq(cs)), in1),  dim)
  })

  def moveInAlternate: Rule = Rule("moveDownSplitStream_moveInAlternate", {
    case SplitOrderedStream(Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _),
    chunkSize, _) if RulesHelper.containsParam(body1, p1) && RulesHelper.containsParam(body2, p2) =>
      Alternate(ArchLambda(p1, SplitOrderedStream(body1, chunkSize)),
        ArchLambda(p2, SplitOrderedStream(body2, chunkSize)), input)

    case MapOrderedStream(fun: LambdaT, Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _), _) if
      RulesHelper.containsParam(body1, p1) && RulesHelper.containsParam(body2, p2) && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
        innerMostFun.body match {
          case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } =>
      val levels = RulesHelper.getMapNDLevels(fun)
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      var chunkSize: ArithTypeT = 0

      innerMostFun.body match {
        case SplitOrderedStream(ParamUse(p), cs, _) if p.id == innerMostFun.param.id =>
          chunkSize = cs
        case _ =>
      }

      Alternate(ArchLambda(p1, MapOrderedStream(levels, SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), body1)),
        ArchLambda(p2, MapOrderedStream(levels, SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), body2)), input)
  })

  def mergeRepeat: Rule = Rule("moveDownSplitStream_mergeRepeat", {
    case SplitOrderedStream(Repeat(input, times, _), chunkSize, _) =>
      Repeat(Repeat(input, chunkSize), times.ae / chunkSize.ae)
  })

  def skipZips: Rule = Rule("moveDownSplitStream_skipZips", {
    case MapOrderedStream(ArchLambda(p1, body, _), Zip2OrderedStream(in, _), _) if {
      var exprFound = false
      body.visit{
        case SplitOrderedStream(Zip2OrderedStream(ParamUse(p2), _), _, _) if p1.id == p2.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      var chunkSize: ArithTypeT = 0
      body.visit{
        case SplitOrderedStream(Zip2OrderedStream(ParamUse(p2), _), cs, _) if p1.id == p2.id => chunkSize = cs
        case _ =>
      }
      val newInput = TypeChecker.check(Zip2OrderedStream(Tuple2(MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), Select2(in, 0)),
        MapOrderedStream(SplitOrderedStream.asFunction(Seq(None), Seq(chunkSize)), Select2(in, 1)))))
      val newParam = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
      MapOrderedStream(
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case SplitOrderedStream(Zip2OrderedStream(ParamUse(p2), _), _, _) if p1.id == p2.id =>
              MapOrderedStream(Zip2OrderedStream.asFunction(), Zip2OrderedStream(ParamUse(newParam)))
            case e => e
          }.asInstanceOf[Expr]
        ),
        newInput
      )
  })

  def skipZipTuple: Rule = Rule("moveDownSplitStream_skipZipTuple", {
    case MapOrderedStream(ArchLambda(p1, body, _), Zip2OrderedStream(Tuple2(in1, in2, _), _), _) if {
      var exprFound = false
      body.visit{
        case Zip2OrderedStream(Tuple2(MapOrderedStream(f1: LambdaT, Select2(ParamUse(p2), sel1, _), _),
        MapOrderedStream(f2: LambdaT, Select2(ParamUse(p3), sel2, _), _), _), _) if
          p1.id == p2.id && p3.id == p1.id && sel1.ae.evalInt == 0 && sel2.ae.evalInt == 1 && {
            var ch1: ArithTypeT = 0
            var exprFound1 = false
            var ch2: ArithTypeT = 0
            var exprFound2 = false
            val innerMostFun1 = RulesHelper.getCurrentOrInnerMostLambda(f1)
            innerMostFun1.body match {
              case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun1.param.id =>
                ch1 = chunkSize
                exprFound1 = true
              case _ =>
            }
            val innerMostFun2 = RulesHelper.getCurrentOrInnerMostLambda(f2)
            innerMostFun2.body match {
              case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun2.param.id =>
                ch2 = chunkSize
                exprFound2 = true
              case _ =>
            }
            exprFound1 && exprFound2 && ch1 == ch2 && ch1.ae.evalInt > 0 &&
              RulesHelper.getMapNDLevels(f1) == RulesHelper.getMapNDLevels(f2)
          }=> exprFound = true
        case _ =>
      }
      exprFound
    } =>
      var ch: ArithTypeT = 0
      var level: Int = 0
      body.visit{
        case Zip2OrderedStream(Tuple2(MapOrderedStream(f1: LambdaT, Select2(ParamUse(p2), sel1, _), _),
        MapOrderedStream(f2: LambdaT, Select2(ParamUse(p3), sel2, _), _), _), _) if
          p1.id == p2.id && p3.id == p1.id && sel1.ae.evalInt == 0 && sel2.ae.evalInt == 1 && {
            var ch1: ArithTypeT = 0
            var exprFound1 = false
            var ch2: ArithTypeT = 0
            var exprFound2 = false
            val innerMostFun1 = RulesHelper.getCurrentOrInnerMostLambda(f1)
            innerMostFun1.body match {
              case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun1.param.id =>
                ch1 = chunkSize
                exprFound1 = true
              case _ =>
            }
            val innerMostFun2 = RulesHelper.getCurrentOrInnerMostLambda(f2)
            innerMostFun2.body match {
              case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun2.param.id =>
                ch2 = chunkSize
                exprFound2 = true
              case _ =>
            }
            val res =exprFound1 && exprFound2 && ch1 == ch2 && ch1.ae.evalInt > 0 &&
              RulesHelper.getMapNDLevels(f1) == RulesHelper.getMapNDLevels(f2)
            if(res) {
              ch = ch1
              level = RulesHelper.getMapNDLevels(f1)
            }
            res
          }=>
        case _ =>
      }

      val newIn = TypeChecker.check(Zip2OrderedStream(Tuple2(
        MapOrderedStream(level+1, SplitOrderedStream.asFunction(Seq(None), Seq(ch)), in1),
        MapOrderedStream(level+1, SplitOrderedStream.asFunction(Seq(None), Seq(ch)), in2))))
      val newParam = ParamDef(newIn.t.asInstanceOf[OrderedStreamTypeT].et)
      MapOrderedStream(
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case Zip2OrderedStream(Tuple2(MapOrderedStream(f1: LambdaT, Select2(ParamUse(p2), sel1, _), _),
            MapOrderedStream(f2: LambdaT, Select2(ParamUse(p3), sel2, _), _), _), _) if
              p1.id == p2.id && p3.id == p1.id && sel1.ae.evalInt == 0 && sel2.ae.evalInt == 1 && {
                var ch1: ArithTypeT = 0
                var exprFound1 = false
                var ch2: ArithTypeT = 0
                var exprFound2 = false
                val innerMostFun1 = RulesHelper.getCurrentOrInnerMostLambda(f1)
                innerMostFun1.body match {
                  case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun1.param.id =>
                    ch1 = chunkSize
                    exprFound1 = true
                  case _ =>
                }
                val innerMostFun2 = RulesHelper.getCurrentOrInnerMostLambda(f2)
                innerMostFun2.body match {
                  case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun2.param.id =>
                    ch2 = chunkSize
                    exprFound2 = true
                  case _ =>
                }
                exprFound1 && exprFound2 && ch1 == ch2 && ch1.ae.evalInt > 0 &&
                  RulesHelper.getMapNDLevels(f1) == RulesHelper.getMapNDLevels(f2)
              }=>
              Zip2OrderedStream(ParamUse(newParam))
            case e => e
          }.asInstanceOf[Expr]
        ),
        newIn
      )

    case MapOrderedStream(ArchLambda(p1, body, _), Zip2OrderedStream(in @ ParamUse(_), _), _) if {
      var exprFound = false
      body.visit{
        case Zip2OrderedStream(Tuple2(MapOrderedStream(f1: LambdaT, Select2(ParamUse(p2), sel1, _), _),
        MapOrderedStream(f2: LambdaT, Select2(ParamUse(p3), sel2, _), _), _), _) if
          p1.id == p2.id && p3.id == p1.id && sel1.ae.evalInt == 0 && sel2.ae.evalInt == 1 && {
            var ch1: ArithTypeT = 0
            var exprFound1 = false
            var ch2: ArithTypeT = 0
            var exprFound2 = false
            val innerMostFun1 = RulesHelper.getCurrentOrInnerMostLambda(f1)
            innerMostFun1.body match {
              case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun1.param.id =>
                ch1 = chunkSize
                exprFound1 = true
              case _ =>
            }
            val innerMostFun2 = RulesHelper.getCurrentOrInnerMostLambda(f2)
            innerMostFun2.body match {
              case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun2.param.id =>
                ch2 = chunkSize
                exprFound2 = true
              case _ =>
            }
            exprFound1 && exprFound2 && ch1 == ch2 && ch1.ae.evalInt > 0 &&
              RulesHelper.getMapNDLevels(f1) == RulesHelper.getMapNDLevels(f2)
          }=> exprFound = true
        case _ =>
      }
      exprFound
    } =>
      var ch: ArithTypeT = 0
      var level: Int = 0
      body.visit{
        case Zip2OrderedStream(Tuple2(MapOrderedStream(f1: LambdaT, Select2(ParamUse(p2), sel1, _), _),
        MapOrderedStream(f2: LambdaT, Select2(ParamUse(p3), sel2, _), _), _), _) if
          p1.id == p2.id && p3.id == p1.id && sel1.ae.evalInt == 0 && sel2.ae.evalInt == 1 && {
            var ch1: ArithTypeT = 0
            var exprFound1 = false
            var ch2: ArithTypeT = 0
            var exprFound2 = false
            val innerMostFun1 = RulesHelper.getCurrentOrInnerMostLambda(f1)
            innerMostFun1.body match {
              case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun1.param.id =>
                ch1 = chunkSize
                exprFound1 = true
              case _ =>
            }
            val innerMostFun2 = RulesHelper.getCurrentOrInnerMostLambda(f2)
            innerMostFun2.body match {
              case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun2.param.id =>
                ch2 = chunkSize
                exprFound2 = true
              case _ =>
            }
            val res =exprFound1 && exprFound2 && ch1 == ch2 && ch1.ae.evalInt > 0 &&
              RulesHelper.getMapNDLevels(f1) == RulesHelper.getMapNDLevels(f2)
            if(res) {
              ch = ch1
              level = RulesHelper.getMapNDLevels(f1)
            }
            res
          }=>
        case _ =>
      }

      val newIn = TypeChecker.check(Zip2OrderedStream(Tuple2(
        MapOrderedStream(level+1, SplitOrderedStream.asFunction(Seq(None), Seq(ch)), Select2(in, 0)),
        MapOrderedStream(level+1, SplitOrderedStream.asFunction(Seq(None), Seq(ch)), Select2(in, 1)))))
      val newParam = ParamDef(newIn.t.asInstanceOf[OrderedStreamTypeT].et)
      MapOrderedStream(
        ArchLambda(
          newParam,
          body.visitAndRebuild{
            case Zip2OrderedStream(Tuple2(MapOrderedStream(f1: LambdaT, Select2(ParamUse(p2), sel1, _), _),
            MapOrderedStream(f2: LambdaT, Select2(ParamUse(p3), sel2, _), _), _), _) if
              p1.id == p2.id && p3.id == p1.id && sel1.ae.evalInt == 0 && sel2.ae.evalInt == 1 && {
                var ch1: ArithTypeT = 0
                var exprFound1 = false
                var ch2: ArithTypeT = 0
                var exprFound2 = false
                val innerMostFun1 = RulesHelper.getCurrentOrInnerMostLambda(f1)
                innerMostFun1.body match {
                  case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun1.param.id =>
                    ch1 = chunkSize
                    exprFound1 = true
                  case _ =>
                }
                val innerMostFun2 = RulesHelper.getCurrentOrInnerMostLambda(f2)
                innerMostFun2.body match {
                  case SplitOrderedStream(ParamUse(p), chunkSize, _) if p.id == innerMostFun2.param.id =>
                    ch2 = chunkSize
                    exprFound2 = true
                  case _ =>
                }
                exprFound1 && exprFound2 && ch1 == ch2 && ch1.ae.evalInt > 0 &&
                  RulesHelper.getMapNDLevels(f1) == RulesHelper.getMapNDLevels(f2)
              }=>
              Zip2OrderedStream(ParamUse(newParam))
            case e => e
          }.asInstanceOf[Expr]
        ),
        newIn
      )
  })
}