package backend.hdl.arch.rewrite

import backend.hdl._
import backend.hdl.arch._
import backend.hdl.arch.device.Mul2AddInt
import core._
import core.rewrite.{Rule, RulesT}

object ExtractMul2AddRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(extractMul2Add)

  def extractMul2Add: Rule = Rule("extractMul2Add", {
    case MapVector(ArchLambda(p1, AddInt(VectorToTuple2(ParamUse(p2), _), _), _), SplitVector(MapVector(ArchLambda(p3, MulInt(ParamUse(p4), _), _), input, _), at, _), _)
      if p1.id == p2.id && p3.id == p4.id && at.ae.evalInt == 2 && isSupportedInputType(p3.t) =>
      MapVector(
        {
          val p = ParamDef()
          ArchLambda(p,
            Mul2AddInt(VectorToTuple2(ParamUse(p)))
          )
        },
        SplitVector(input, 2)
      )

    case AddInt(VectorToTuple(MapVector(ArchLambda(p1, MulInt(ParamUse(u1), _), _), e, at: VectorTypeT), _, _), _)
      if p1.id == u1.id && at.len.ae.evalInt == 2 && isSupportedInputType(p1.t) =>
      Mul2AddInt(VectorToTuple2(e))
  })

  def isSupportedInputType(ty: Type): Boolean = ty match {
    case TupleType(Seq(lhs: IntTypeT, rhs: IntTypeT)) =>
      lhs.kind match {
        case _ if lhs.kind != rhs.kind => false // the IR node doesn't allow this (assume FPGA doesn't either)
        case IntTypeKind => lhs.bitWidth.ae.evalInt <= 18 && rhs.bitWidth.ae.evalInt <= 18
        case SignedIntTypeKind =>
          val lhsw = lhs.bitWidth.ae.evalInt
          val rhsw = rhs.bitWidth.ae.evalInt
          (lhsw <= 18 && rhsw <= 19) || (lhsw <= 19 && rhsw <= 18)
        case _ => false
      }
    case _ => false
  }
}
