package backend.hdl.arch.rewrite

import backend.hdl.arch.mem.{Padding, Read, ReadOutOfBounds}
import backend.hdl._
import backend.hdl.arch._
import core._
import core.rewrite.{Rule, RulesT}

object MoveDownConcatRules extends RulesT {
  override def all(config: Option[Int]): Seq[Rule] = Seq(
    cancelSplitJoin,
    skipMarker,
    moveIntoMap,
    skipMapConvert,
    skipMapMapMapConvert,
    skipMapMapMapMapConvert,
    skipMapMapMapMapMapConvert,
    skipDropJoinStm,
    skipMapJoinStm,
    skipMapMapJoinStm,
    skipMapMapMapJoinStm,
    skipMapVecToStm,
    skipVecToStm,
    skipMapMapVecToStm,
    skipMapMapMapVecToStm,
    skipMapMapMapMapVecToStm,
    skipMapSplitVec,
    skipMapMapSplitVec,
    skipMapMapMapSplitVec,
    skipMapMapMapMapSplitVec,
    mapFission,
    leaveMapFun,
    skipLet,
    mergeIntoReadCounter,
    mergeIntoMapMapMapReadCounter,
    mergeIntoMapMapMapMapReadCounter
  )

  def cancelSplitJoin: Rule = Rule("moveDownConcat_cancelSplitJoin", {
    case SplitOrderedStream(ConcatOrderedStream(JoinOrderedStream(in1, _), in2, dim, _), cs, _)
      if {
        val OrderedStreamType(OrderedStreamType(_, d1), d0) = in1.t
        d0.ae.evalInt == 1 && cs.ae.evalInt == (d1.ae.evalInt + in2.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt)
      } =>
      ConcatOrderedStream(in1, Repeat(in2, 1), dim.ae.evalInt + 1)
    case SplitOrderedStream(ConcatOrderedStream(in1, JoinOrderedStream(in2, _), dim, _), cs, _)
      if {
        val OrderedStreamType(OrderedStreamType(_, d1), d0) = in2.t
        d0.ae.evalInt == 1 && cs.ae.evalInt == (d1.ae.evalInt + in1.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalInt)
      } =>
      ConcatOrderedStream(Repeat(in1, 1), in2, dim.ae.evalInt + 1)
  })

  def skipMarker: Rule = Rule("moveDownConcat_skipMarker", {
    case ConcatOrderedStream(Marker(in1, ts), in2, dim, _) => Marker(ConcatOrderedStream(in1, in2, dim), ts)
    case ConcatOrderedStream(in1, Marker(in2, ts), dim, _) => Marker(ConcatOrderedStream(in1, in2, dim), ts)
  })

  def moveIntoMap: Rule = Rule("moveDownConcat_moveIntoMap", {
    case ConcatOrderedStream(MapOrderedStream(ArchLambda(p1, body, _), in1, mapt: OrderedStreamTypeT), Repeat(in2, rep, _), dim, _) if
      RulesHelper.containsParam(body, p1) && mapt.len.ae.evalInt == rep.ae.evalInt && dim.ae.evalInt != 0 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body))
        innerMostFun.body match {
          case Read(_, ParamUse(p), _) if p.id == innerMostFun.param.id => exprFound = true
          case ReadOutOfBounds(_, ParamUse(p), _, _, _) if p.id == innerMostFun.param.id => exprFound = true
          case ConcatOrderedStream(_, _, _, _) => exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream(ArchLambda(p1, ConcatOrderedStream(body, in2, dim.ae.evalInt - 1)), in1)
    case ConcatOrderedStream(Repeat(in1, rep, _), MapOrderedStream(ArchLambda(p1, body, _), in2, mapt: OrderedStreamTypeT), dim, _) if
      RulesHelper.containsParam(body, p1) && mapt.len.ae.evalInt == rep.ae.evalInt && dim.ae.evalInt != 0 && {
        var exprFound = false
        val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body))
        innerMostFun.body match {
          case Read(_, ParamUse(p), _) if p.id == innerMostFun.param.id => exprFound = true
          case ReadOutOfBounds(_, ParamUse(p), _, _, _) if p.id == innerMostFun.param.id => exprFound = true
          case ConcatOrderedStream(_, _, _, _) => exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream(ArchLambda(p1, ConcatOrderedStream(in1, body, dim.ae.evalInt - 1)), in2)
  })

  def skipMapConvert: Rule = Rule("moveDownConcat_skipMapConvert", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, Conversion(pt @ ParamUse(p2), _), _), in, _), Repeat(ConstantValue(intValue, _), rep, _), dim, _) if
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(ConstantValue(intValue, Some(pt.t)), rep), dim))
    case ConcatOrderedStream(Repeat(ConstantValue(intValue, _), rep, _), MapOrderedStream(cnvFun @ ArchLambda(p1, Conversion(pt @ ParamUse(p2), _), _), in, _), dim, _) if
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(ConstantValue(intValue, Some(pt.t)), rep), in, dim))
  })

  def skipMapMapMapConvert: Rule = Rule("moveDownConcat_skipMapMapMapConvert", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapVector(ArchLambda(p3, Conversion(pt @
      ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), in, _),
    Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), dim, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t)), rep1), rep2), rep3), dim))
    case ConcatOrderedStream(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapVector(ArchLambda(p3, Conversion(pt @
      ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), in, _), dim, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t)), rep1), rep2), rep3), in, dim))
  })

  def skipMapMapMapMapConvert: Rule = Rule("moveDownConcat_skipMapMapMapMapConvert", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapVector(ArchLambda(p4, Conversion(pt @
      ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), in, _),
    Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _), dim, _) if
      p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t)), rep1), rep2), rep3), rep4), dim))
    case ConcatOrderedStream(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapVector(ArchLambda(p4, Conversion(pt @
      ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), in, _), dim, _) if
      p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t)), rep1), rep2), rep3), rep4), in, dim))
  })

  def skipMapMapMapMapMapConvert: Rule = Rule("moveDownConcat_skipMapMapMapMapMapConvert", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4,
    MapVector(ArchLambda(p5, Conversion(pt @ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), ParamUse(p9), _), _), ParamUse(p10), _), _), in, _),
    Repeat(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _), rep5, _),  dim, _) if
      p1.id == p10.id && p2.id == p9.id && p3.id == p8.id && p4.id == p7.id && p5.id == p6.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t)), rep1), rep2), rep3), rep4), rep5), dim))
    case ConcatOrderedStream( Repeat(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _), rep5, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4,
    MapVector(ArchLambda(p5, Conversion(pt @ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), ParamUse(p9), _), _), ParamUse(p10), _), _), in, _), dim, _) if
      p1.id == p10.id && p2.id == p9.id && p3.id == p8.id && p4.id == p7.id && p5.id == p6.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t)), rep1), rep2), rep3), rep4), rep5), in, dim))
  })

  def skipDropJoinStm: Rule = Rule("moveDownConcat_skipDropJoinStm", {
    // Only works for Drop with zero
    case ConcatOrderedStream(Repeat(in1, rep, _), DropOrderedStream(JoinOrderedStream(in2, _), fstElements, lstElements, _), dim, _) if
      fstElements.ae.evalInt == 0 && dim.ae.evalInt == 0 =>
      val innerLen = in2.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      val diff = innerLen.ae.evalInt - (rep.ae.evalInt + 1) % innerLen.ae.evalInt + 1
      val newOuterRep = math.ceil(rep.ae.evalInt.toFloat / innerLen.ae.evalInt).toInt
      val newIn1 = TypeChecker.check(Repeat(Repeat(in1, innerLen.ae.evalInt), newOuterRep))
      DropOrderedStream(JoinOrderedStream(ConcatOrderedStream(newIn1, in2, dim)), diff, lstElements)
    case ConcatOrderedStream(DropOrderedStream(JoinOrderedStream(in1, _), fstElements, lstElements, _), Repeat(in2, rep, _),  dim, _) if
      lstElements.ae.evalInt == 0 && dim.ae.evalInt == 0 =>
      val innerLen = in1.t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      val diff = innerLen.ae.evalInt - (rep.ae.evalInt + 1) % innerLen.ae.evalInt + 1
      val newOuterRep = math.ceil(rep.ae.evalInt.toFloat / innerLen.ae.evalInt).toInt
      val newIn2 = TypeChecker.check(Repeat(Repeat(in2, innerLen.ae.evalInt), newOuterRep))
      DropOrderedStream(JoinOrderedStream(ConcatOrderedStream(in1, newIn2, dim)), fstElements, diff)
  })

  def skipMapJoinStm: Rule = Rule("moveDownConcat_skipMapJoinStm", {
    case ConcatOrderedStream(Repeat(Repeat(in1, rep2, _), rep1, _), MapOrderedStream(ArchLambda(p1,
    JoinOrderedStream(ParamUse(p2), _), _), in2, _), dim, _) if
      p1.id == p2.id  && dim.ae.evalInt < 1 =>
      val newRep1 = ParamUse(p2).t.asInstanceOf[OrderedStreamTypeT].len
      val newRep2 = ParamUse(p2).t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      MapOrderedStream(JoinOrderedStream.asFunction(), ConcatOrderedStream(Repeat(Repeat(Repeat(in1, newRep2), newRep1), rep1), in2, dim))

    case ConcatOrderedStream(MapOrderedStream(ArchLambda(p1, JoinOrderedStream(ParamUse(p2), _), _), in1, _),
    Repeat(Repeat(in2, rep2, _), rep1, _),  dim, _) if
      p1.id == p2.id  && dim.ae.evalInt < 1 =>
      val newRep1 = ParamUse(p2).t.asInstanceOf[OrderedStreamTypeT].len
      val newRep2 = ParamUse(p2).t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      MapOrderedStream(JoinOrderedStream.asFunction(), ConcatOrderedStream(in1, Repeat(Repeat(Repeat(in2, newRep2), newRep1), rep1), dim))
  })

  def skipMapMapJoinStm: Rule = Rule("moveDownConcat_skipMapMapJoinStm", {
    case ConcatOrderedStream(Repeat(Repeat(Repeat(in1, rep3, _), rep2, _), rep1, _), MapOrderedStream(ArchLambda(p1,
    MapOrderedStream(ArchLambda(p2, JoinOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), in2, _), dim, _) if
      p1.id == p4.id && p2.id == p3.id && dim.ae.evalInt < 2 =>
      val newRep1 = ParamUse(p3).t.asInstanceOf[OrderedStreamTypeT].len
      val newRep2 = ParamUse(p3).t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      MapOrderedStream(2, JoinOrderedStream.asFunction(), ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(in1, newRep2), newRep1), rep2), rep1), in2, dim))
    case ConcatOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, JoinOrderedStream(ParamUse(p3), _), _), ParamUse(p4), _), _), in1, _),
    Repeat(Repeat(Repeat(in2, rep3, _), rep2, _), rep1, _), dim, _) if
      p1.id == p4.id && p2.id == p3.id && dim.ae.evalInt < 2 =>
      val newRep1 = ParamUse(p3).t.asInstanceOf[OrderedStreamTypeT].len
      val newRep2 = ParamUse(p3).t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      MapOrderedStream(2, JoinOrderedStream.asFunction(), ConcatOrderedStream(in1, Repeat(Repeat(Repeat(Repeat(in2, newRep2), newRep1), rep2), rep1), dim))
  })

  def skipMapMapMapJoinStm: Rule = Rule("moveDownConcat_skipMapMapMapJoinStm", {
    case ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(in1, rep4, _), rep3, _), rep2, _), rep1, _), MapOrderedStream(ArchLambda(p1,
    MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, JoinOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), _),
    ParamUse(p6), _), _), in2, _), dim, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && dim.ae.evalInt < 2 =>
      val newRep1 = ParamUse(p4).t.asInstanceOf[OrderedStreamTypeT].len
      val newRep2 = ParamUse(p4).t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      MapOrderedStream(3, JoinOrderedStream.asFunction(), ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(Repeat(in1, newRep2), newRep1), rep3), rep2), rep1), in2, dim))
    case ConcatOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3, JoinOrderedStream(ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), in1, _),
    Repeat(Repeat(Repeat(Repeat(in2, rep4, _), rep3, _), rep2, _), rep1, _), dim, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && dim.ae.evalInt < 2 =>
      val newRep1 = ParamUse(p4).t.asInstanceOf[OrderedStreamTypeT].len
      val newRep2 = ParamUse(p4).t.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[OrderedStreamTypeT].len
      MapOrderedStream(3, JoinOrderedStream.asFunction(), ConcatOrderedStream(in1, Repeat(Repeat(Repeat(Repeat(Repeat(in2, newRep2), newRep1), rep3), rep2), rep1), dim))
  })

  def skipVecToStm: Rule = Rule("moveDownConcat_skipVecToStm", {
    case ConcatOrderedStream(VectorToOrderedStream(input, _), Repeat(const, rep1, _), dim, _) if
      dim.ae.evalInt == 0  =>
      VectorToOrderedStream(ConcatVector(Tuple(input, VectorGenerator(const, rep1))))
    case ConcatOrderedStream(Repeat(const, rep1, _), VectorToOrderedStream(input, _), dim, _) if
      dim.ae.evalInt == 0  =>
      VectorToOrderedStream(ConcatVector(Tuple(VectorGenerator(const, rep1), input)))
  })

  def skipMapVecToStm: Rule = Rule("moveDownConcat_skipMapVecToStm", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, VectorToOrderedStream(pt @ ParamUse(p2), _), _), in, _), Repeat(Repeat(const, rep1, _), rep2, _), dim, _) if
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t.asInstanceOf[VectorTypeT].et match {
          case VectorType(_, _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      }  =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(VectorGenerator(const, rep1), rep2), dim))
    case ConcatOrderedStream(Repeat(Repeat(const, rep1, _), rep2, _), MapOrderedStream(cnvFun @ ArchLambda(p1, VectorToOrderedStream(pt @ ParamUse(p2), _), _), in, _), dim, _) if
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t.asInstanceOf[VectorTypeT].et match {
          case VectorType(_, _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(VectorGenerator(const, rep1), rep2), in, dim))
  })

  def skipMapMapVecToStm: Rule = Rule("moveDownConcat_skipMapMapVecToStm", {
    case ConcatOrderedStream(Repeat(Repeat(Repeat(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(pt @ ParamUse(p3), _), _), ParamUse(p4), _), _), in, _), dim, _) if
      p1.id == p4.id && p2.id == p3.id && dim.ae.evalInt < 2 && {
        var typeFound = false
        pt.t.asInstanceOf[VectorTypeT].et match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t.asInstanceOf[VectorTypeT].et)), rep1), rep2), rep3), in, dim))
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, VectorToOrderedStream(pt @ ParamUse(p3), _), _), ParamUse(p4), _), _), in, _),
    Repeat(Repeat(Repeat(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), dim, _) if
      p1.id == p4.id && p2.id == p3.id && dim.ae.evalInt < 2 && {
        var typeFound = false
        pt.t.asInstanceOf[VectorTypeT].et match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t.asInstanceOf[VectorTypeT].et)), rep1), rep2), rep3), dim))
  })

  def skipMapMapMapVecToStm: Rule = Rule("moveDownConcat_skipMapMapMapVecToStm", {
    case ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3,
    VectorToOrderedStream(pt @ ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), in, _), dim, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && dim.ae.evalInt < 3 && {
        var typeFound = false
        pt.t.asInstanceOf[VectorTypeT].et match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t.asInstanceOf[VectorTypeT].et)), rep1), rep2), rep3), rep4), in, dim))
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3,
    VectorToOrderedStream(pt @ ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), in, _),
    Repeat(Repeat(Repeat(Repeat(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _), dim, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && dim.ae.evalInt < 3 && {
        var typeFound = false
        pt.t.asInstanceOf[VectorTypeT].et match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t.asInstanceOf[VectorTypeT].et)), rep1), rep2), rep3), rep4), dim))
  })

  def skipMapMapMapMapVecToStm: Rule = Rule("moveDownConcat_skipMapMapMapMapVecToStm", {
    case ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(Repeat(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _), rep5, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3,
    MapOrderedStream(ArchLambda(p4, VectorToOrderedStream(pt @ ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), in, _), dim, _) if
      p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id  && dim.ae.evalInt < 4 && {
        var typeFound = false
        pt.t.asInstanceOf[VectorTypeT].et match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t.asInstanceOf[VectorTypeT].et)), rep1), rep2), rep3), rep4), rep5), in, dim))
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3,
    MapOrderedStream(ArchLambda(p4, VectorToOrderedStream(pt @ ParamUse(p5), _), _), ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), in, _),
    Repeat(Repeat(Repeat(Repeat(Repeat(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _), rep5, _),dim, _) if
      p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id  && dim.ae.evalInt < 4 && {
        var typeFound = false
        pt.t.asInstanceOf[VectorTypeT].et match {
          case VectorType(LogicType(), _) => typeFound = true
          case _: ScalarTypeT => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, Some(pt.t.asInstanceOf[VectorTypeT].et)), rep1), rep2), rep3), rep4), rep5), dim))
  })

  def skipMapSplitVec: Rule = Rule("moveDownConcat_skipMapSplitVec", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, SplitVector(pt @ ParamUse(p2), cs, _), _), in, _),
    Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), dim, _) if intValue.ae.evalInt == 0 &&
      p1.id == p2.id && dim.ae.evalInt == 0 && {
      var typeFound = false
      pt.t match {
        case VectorType(LogicType(), _) => typeFound = true
      }
      typeFound
    } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(ConstantValue(intValue, Some(pt.t)), rep2), dim))
    case ConcatOrderedStream(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, SplitVector(pt @ ParamUse(p2), cs, _), _), in, _), dim, _) if
      p1.id == p2.id && dim.ae.evalInt == 0 && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(ConstantValue(intValue, Some(pt.t)), rep2), in, dim))
  })

  def skipMapMapSplitVec: Rule = Rule("moveDownConcat_skipMapMapSplitVec", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    SplitVector(pt @ ParamUse(p3), cs, _), _), ParamUse(p4), _), _), in, _),
    Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _),  dim, _) if
      p1.id == p4.id && p2.id == p3.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(Repeat(ConstantValue(intValue, Some(pt.t)), rep2), rep3), dim))
    case ConcatOrderedStream(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    SplitVector(pt @ ParamUse(p3), cs, _), _), ParamUse(p4), _), _), in, _), dim, _) if
      p1.id == p4.id && p2.id == p3.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(Repeat(ConstantValue(intValue, Some(pt.t)), rep2), rep3), in, dim))
  })

  def skipMapMapMapSplitVec: Rule = Rule("moveDownConcat_skipMapMapMapSplitVec", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3, SplitVector(pt @ ParamUse(p4), cs, _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), in, _),
    Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _), dim, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(Repeat(Repeat(ConstantValue(intValue, Some(pt.t)), rep2), rep3), rep4), dim))
    case ConcatOrderedStream(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3, SplitVector(pt @ ParamUse(p4), cs, _), _), ParamUse(p5), _), _), ParamUse(p6), _), _), in, _), dim, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(Repeat(Repeat(ConstantValue(intValue, Some(pt.t)), rep2), rep3), rep4), in, dim))
  })

  def skipMapMapMapMapSplitVec: Rule = Rule("moveDownConcat_skipMapMapMapMapSplitVec", {
    case ConcatOrderedStream(MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, SplitVector(pt @ ParamUse(p5), cs, _), _), ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), in, _),
    Repeat(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _), rep5, _), dim, _) if
      p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id && {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(in, Repeat(Repeat(Repeat(Repeat(ConstantValue(intValue, Some(pt.t)), rep2), rep3), rep4), rep5), dim))
    case ConcatOrderedStream(
    Repeat(Repeat(Repeat(Repeat(VectorGenerator(ConstantValue(intValue, _), rep1, _), rep2, _), rep3, _), rep4, _), rep5, _),
    MapOrderedStream(cnvFun @ ArchLambda(p1, MapOrderedStream(ArchLambda(p2,
    MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, SplitVector(pt @ ParamUse(p5), cs, _), _), ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), in, _), dim, _) if
      p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id &&  {
        var typeFound = false
        pt.t match {
          case VectorType(LogicType(), _) => typeFound = true
        }
        typeFound
      } =>
      MapOrderedStream(cnvFun, ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(ConstantValue(intValue, Some(pt.t)), rep2), rep3), rep4), rep5), in, dim))
  })

  def mapFission: Rule = Rule("moveDownConcat_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      body.visit{
        case ConcatOrderedStream(padInput, ParamUse(p2), dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      body match{
        case ConcatOrderedStream(padInput, ParamUse(p2), dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      var newInput: Expr = null
      body.visit{
        case ConcatOrderedStream(padInput, ParamUse(p2), dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          val param = ParamDef()
          newInput = MapOrderedStream(ArchLambda(param, ConcatOrderedStream(padInput, ParamUse(param))), input)
        case _ =>
      }
      MapOrderedStream(
        {
          val param1 = ParamDef()
          ArchLambda(
            param1,
            body.visitAndRebuild{
              case ConcatOrderedStream(padInput, ParamUse(p2), dim, _) if
                p1.id == p2.id && dim.ae.evalInt == 0 =>
                ParamUse(param1)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )

    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      body.visit{
        case ConcatOrderedStream(ParamUse(p2), padInput, dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      body match{
        case ConcatOrderedStream(ParamUse(p2), padInput, dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      var newInput: Expr = null
      body.visit{
        case ConcatOrderedStream(ParamUse(p2), padInput, dim, _) if
          p1.id == p2.id && dim.ae.evalInt == 0 =>
          val param = ParamDef()
          newInput = MapOrderedStream(ArchLambda(param, ConcatOrderedStream(ParamUse(param), padInput)), input)
        case _ =>
      }
      MapOrderedStream(
        {
          val param1 = ParamDef()
          ArchLambda(
            param1,
            body.visitAndRebuild{
              case ConcatOrderedStream(ParamUse(p2), padInput, dim, _) if
                p1.id == p2.id && dim.ae.evalInt == 0 =>
                ParamUse(param1)
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )
  })

  def leaveMapFun: Rule = Rule("moveDownConcat_leaveMapFun", {
    case MapOrderedStream(ArchLambda(p1, ConcatOrderedStream(ParamUse(p2), in2, dim, _), _), in1, _) if p1.id == p2.id =>
      ConcatOrderedStream(in1, Repeat(in2, in1.t.asInstanceOf[OrderedStreamTypeT].len), dim.ae.evalInt + 1)
    case MapOrderedStream(ArchLambda(p1, ConcatOrderedStream(in1, ParamUse(p2), dim, _), _), in2, _) if p1.id == p2.id =>
      ConcatOrderedStream(Repeat(in1, in2.t.asInstanceOf[OrderedStreamTypeT].len), in2, dim.ae.evalInt + 1)
  })

  def skipLet: Rule = Rule("moveDownPaddingRules_skipLet", {
    case ConcatOrderedStream(in1, Let(p, in2, arg, _), dim, _) =>
      Let(p, ConcatOrderedStream(in1, in2, dim), arg)
    case ConcatOrderedStream(Let(p, in1, arg, _), in2, dim, _) =>
      Let(p, ConcatOrderedStream(in1, in2, dim), arg)
  })

  def mergeIntoReadCounter: Rule = Rule("moveDownPaddingRules_mergeIntoReadCounter", {
    case ConcatOrderedStream(Repeat(Repeat(iv: ConstantValueExpr, rep1, _), rep2, _),
    MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, Read(mem, ParamUse(p3), _), _), ParamUse(p4), _), _),
    cnt@ CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _), dim, _) if
      p1.id == p4.id && p2.id == p3.id =>
      val newBitWidth = t.leafType.bitWidth.ae.evalInt + 1
      val newElementType = IntType(newBitWidth)
      val newCounter = CounterInteger(start, increment, loop, dimensions, repetitions, Some(newElementType))
      val oldAddrLimit = RulesHelper.getLastValue(cnt) + 1
      val invalidAddr = ((1L << newBitWidth) - 1).toInt
      val newConst = ConstantValue(invalidAddr, Some(newElementType))
      val newAddr = TypeChecker.check(ConcatOrderedStream(Repeat(Repeat(newConst, rep1), rep2), newCounter, dim))
      val param = ParamDef(newAddr.t.asInstanceOf[OrderedStreamTypeT].leafType)
      MapOrderedStream(2, ArchLambda(param, ReadOutOfBounds(mem, ParamUse(param), Some(iv.toBitVector), Padding(oldAddrLimit))), newAddr)
    case ConcatOrderedStream(
    MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, Read(mem, ParamUse(p3), _), _), ParamUse(p4), _), _),
    cnt@ CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _),
    Repeat(Repeat(iv: ConstantValueExpr, rep1, _), rep2, _), dim, _) if
      p1.id == p4.id && p2.id == p3.id =>
      val newBitWidth = t.leafType.bitWidth.ae.evalInt + 1
      val newElementType = IntType(newBitWidth)
      val newCounter = CounterInteger(start, increment, loop, dimensions, repetitions, Some(newElementType))
      val oldAddrLimit = RulesHelper.getLastValue(cnt) + 1
      val invalidAddr = ((1L << newBitWidth) - 1).toInt
      val newConst = ConstantValue(invalidAddr, Some(newElementType))
      val newAddr = TypeChecker.check(ConcatOrderedStream(newCounter, Repeat(Repeat(newConst, rep1), rep2), dim))
      val param = ParamDef(newAddr.t.asInstanceOf[OrderedStreamTypeT].leafType)
      MapOrderedStream(2, ArchLambda(param, ReadOutOfBounds(mem, ParamUse(param), Some(iv.toBitVector), Padding(oldAddrLimit))), newAddr)
  })

  def mergeIntoMapMapMapReadCounter: Rule = Rule("moveDownPaddingRules_mergeIntoMapMapMapReadCounter", {
    case ConcatOrderedStream(Repeat(Repeat(Repeat(iv: ConstantValueExpr, rep1, _), rep2, _), rep3, _),
    MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, Read(mem, ParamUse(p4), _), _), ParamUse(p5), _), _), ParamUse(p6), _), _),
    cnt@ CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _), dim, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id =>
      val newBitWidth = t.leafType.bitWidth.ae.evalInt + 1
      val newElementType = IntType(newBitWidth)
      val newCounter = CounterInteger(start, increment, loop, dimensions, repetitions, Some(newElementType))
      val oldAddrLimit = RulesHelper.getLastValue(cnt) + 1
      val invalidAddr = ((1L << newBitWidth) - 1).toInt
      val newConst = ConstantValue(invalidAddr, Some(newElementType))
      val newAddr = TypeChecker.check(ConcatOrderedStream(Repeat(Repeat(Repeat(newConst, rep1), rep2), rep3), newCounter, dim))
      val param = ParamDef(newAddr.t.asInstanceOf[OrderedStreamTypeT].leafType)
      MapOrderedStream(3, ArchLambda(param, ReadOutOfBounds(mem, ParamUse(param), Some(iv.toBitVector), Padding(oldAddrLimit))), newAddr)

    case ConcatOrderedStream(Repeat(Repeat(Repeat(iv: ConstantValueExpr, rep1, _), rep2, _), rep3, _),
    MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, ReadOutOfBounds(mem, ParamUse(p4), pv, readFun: Padding, _), _), ParamUse(p5), _), _), ParamUse(p6), _), _),
    counter, _), dim, _) if
      p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && iv.toBitVector == pv =>
      val newBitWidth = counter.t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth.ae.evalInt
      val newElementType = IntType(newBitWidth)
      val invalidAddr = ((1L << newBitWidth) - 1).toInt
      val newConst = ConstantValue(invalidAddr, Some(newElementType))
      val newAddr = TypeChecker.check(ConcatOrderedStream(Repeat(Repeat(Repeat(newConst, rep1), rep2), rep3), counter, dim))
      val param = ParamDef(newAddr.t.asInstanceOf[OrderedStreamTypeT].leafType)
      MapOrderedStream(3, ArchLambda(param, ReadOutOfBounds(mem, ParamUse(param), Some(pv), readFun)), newAddr)

  case ConcatOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, ReadOutOfBounds(mem, ParamUse(p4), pv, readFun: Padding, _), _), ParamUse(p5), _), _), ParamUse(p6), _), _),
  counter, _), Repeat(Repeat(Repeat(iv: ConstantValueExpr, rep1, _), rep2, _), rep3, _), dim, _) if
    p1.id == p6.id && p2.id == p5.id && p3.id == p4.id && iv.toBitVector == pv =>
    val newBitWidth = counter.t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth.ae.evalInt
    val newElementType = IntType(newBitWidth)
    val invalidAddr = ((1L << newBitWidth) - 1).toInt
    val newConst = ConstantValue(invalidAddr, Some(newElementType))
    val newAddr = TypeChecker.check(ConcatOrderedStream(counter, Repeat(Repeat(Repeat(newConst, rep1), rep2), rep3), dim))
    val param = ParamDef(newAddr.t.asInstanceOf[OrderedStreamTypeT].leafType)
    MapOrderedStream(3, ArchLambda(param, ReadOutOfBounds(mem, ParamUse(param), Some(pv), readFun)), newAddr)
  })

  def mergeIntoMapMapMapMapReadCounter: Rule = Rule("moveDownPaddingRules_mergeIntoMapMapMapMapReadCounter", {
    case ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(iv: ConstantValueExpr, rep1, _), rep2, _), rep3, _), rep4, _),
    MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, Read(mem, ParamUse(p5), _), _),
    ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _),
    cnt@ CounterInteger(start, increment, loop, dimensions, repetitions, t: OrderedStreamTypeT), _), dim, _) if
      p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id =>
      val newBitWidth = t.leafType.bitWidth.ae.evalInt + 1
      val newElementType = IntType(newBitWidth)
      val newCounter = CounterInteger(start, increment, loop, dimensions, repetitions, Some(newElementType))
      val oldAddrLimit = RulesHelper.getLastValue(cnt) + 1
      val invalidAddr = ((1L << newBitWidth) - 1).toInt
      val newConst = ConstantValue(invalidAddr, Some(newElementType))
      val newAddr = TypeChecker.check(ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(newConst, rep1), rep2), rep3), rep4), newCounter, dim))
      val param = ParamDef(newAddr.t.asInstanceOf[OrderedStreamTypeT].leafType)
      MapOrderedStream(4, ArchLambda(param, ReadOutOfBounds(mem, ParamUse(param), Some(iv.toBitVector), Padding(oldAddrLimit))), newAddr)

    case ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(iv: ConstantValueExpr, rep1, _), rep2, _), rep3, _), rep4, _),
    MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, ReadOutOfBounds(mem, ParamUse(p5), pv, readFun: Padding, _), _),
    ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), counter, _), dim, _) if
      p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id && iv.toBitVector == pv =>
      val newBitWidth = counter.t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth.ae.evalInt
      val newElementType = IntType(newBitWidth)
      val invalidAddr = ((1L << newBitWidth) - 1).toInt
      val newConst = ConstantValue(invalidAddr, Some(newElementType))
      val newAddr = TypeChecker.check(ConcatOrderedStream(Repeat(Repeat(Repeat(Repeat(newConst, rep1), rep2), rep3), rep4), counter, dim))
      val param = ParamDef(newAddr.t.asInstanceOf[OrderedStreamTypeT].leafType)
      MapOrderedStream(4, ArchLambda(param, ReadOutOfBounds(mem, ParamUse(param), Some(pv), readFun)), newAddr)

    case ConcatOrderedStream(MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, ReadOutOfBounds(mem, ParamUse(p5), pv, readFun: Padding, _), _),
    ParamUse(p6), _), _), ParamUse(p7), _), _), ParamUse(p8), _), _), counter, _), Repeat(Repeat(Repeat(Repeat(iv: ConstantValueExpr, rep1, _), rep2, _), rep3, _), rep4, _), dim, _) if
      p1.id == p8.id && p2.id == p7.id && p3.id == p6.id && p4.id == p5.id && iv.toBitVector == pv =>
      val newBitWidth = counter.t.asInstanceOf[OrderedStreamTypeT].leafType.bitWidth.ae.evalInt
      val newElementType = IntType(newBitWidth)
      val invalidAddr = ((1L << newBitWidth) - 1).toInt
      val newConst = ConstantValue(invalidAddr, Some(newElementType))
      val newAddr = TypeChecker.check(ConcatOrderedStream(counter, Repeat(Repeat(Repeat(Repeat(newConst, rep1), rep2), rep3), rep4), dim))
      val param = ParamDef(newAddr.t.asInstanceOf[OrderedStreamTypeT].leafType)
      MapOrderedStream(4, ArchLambda(param, ReadOutOfBounds(mem, ParamUse(param), Some(pv), readFun)), newAddr)
  })
}