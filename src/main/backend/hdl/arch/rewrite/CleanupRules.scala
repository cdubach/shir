package backend.hdl.arch.rewrite

import core._
import core.rewrite.{Rule, RulesT}
import backend.hdl._
import backend.hdl.arch._
import backend.hdl.arch.MapCompiler.getUnboundParams
import backend.hdl.arch.mem.{Write, WriteAsync, WriteAsyncOutOfBounds, WriteOutOfBounds}

object CleanupRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      flattenConversion,
      mergeRepeatHiddenConstant,
      joinWrites,
      moveUpMapOrderedToUnorderedStream,
      replaceJoinUnorderedStream,
      removeIdentityResizes,
      removeIdentityArith,
      //      mapStreamFusion
    ) ++
      RemoveJoinSplitRules.get() ++
      RemoveStreamVectorConversionRules.all(None)

  def removeIdentityArith: Rule = Rule("removeIdentityArith", {
    case MulInt(Tuple(Seq(x, ConstantValue(ArithType(lift.arithmetic.Cst(1)), _)), _), t: IntTypeT) =>
      ResizeInteger(x, t.bitWidth)
    case MulInt(Tuple(Seq(ConstantValue(ArithType(lift.arithmetic.Cst(1)), _), x), _), t: IntTypeT) =>
      ResizeInteger(x, t.bitWidth)

    case ClipBankersRound(ResizeInteger(x, w, _), lo, hi, _)
      if {
        // check if extended bits are completely clipped away
        val xw = x.t.asInstanceOf[IntTypeT].bitWidth.ae.evalInt
        val ww = w.ae.evalInt
        val tw = hi.ae.evalInt
        xw <= ww && (ww - tw) <= xw
      } =>
      // meaning the extension was redundant
      ClipBankersRound(x, lo, hi.ae - (w.ae - x.t.asInstanceOf[IntTypeT].bitWidth.ae))
  })

  def removeIdentityResizes: Rule = Rule("removeIdentityResizes", {
    case DropVector(e, a, b, _) if a.ae.evalInt == 0 && b.ae.evalInt == 0 => e
    case DropOrderedStream(e, a, b, _) if a.ae.evalInt == 0 && b.ae.evalInt == 0 => e
    case ResizeInteger(e, w, _) if w.ae.evalInt == e.t.asInstanceOf[IntTypeT].bitWidth.ae.evalInt => e
  })

  def flattenConversion: Rule = Rule("flattenConversion", {
    case MapOrderedStream(ArchLambda(p1, Conversion(ParamUse(p2), _), _), input, t) if p1.id == p2.id =>
      Conversion(input, t)
    //    does not work in VHDL ...
    //    case MapVector(ArchLambda(p1, Conversion(ParamUse(p2, _), _), _), input, t) if p1.id == p2.id =>
    //      Conversion(input, t)
  })

  // constants are always repeated indefinitely
  def mergeRepeatHiddenConstant: Rule = Rule("mergeRepeatHiddenConstant", {
    case RepeatHidden(c @ ConstantValue(_, _), _, _) => c
  })

  def joinWrites: Rule = Rule("joinWrites", {
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_,
    ReduceOrderedStream(ArchLambda(_, ArchLambda(_, Write(ParamUse(_), ParamUse(_), _), _), _), ParamUse(_), ParamUse(_), _), _), _),
    memAllocation: ParamUse, input, _) =>
      Write(memAllocation, JoinOrderedStream(input))
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, WriteAsync(writeMemoryController, _, _, _), _), _), baseAddr, input, _) =>
      WriteAsync(writeMemoryController, baseAddr, JoinOrderedStream(input))
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_,
    ReduceOrderedStream(ArchLambda(_, ArchLambda(_, WriteOutOfBounds(ParamUse(_), ParamUse(_), rule, _), _), _), ParamUse(_), ParamUse(_), _), _), _),
    memAllocation: ParamUse, input, _) =>
      WriteOutOfBounds(memAllocation, JoinOrderedStream(input), rule)
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, WriteAsyncOutOfBounds(writeMemoryController, _, _, rule, _), _), _), baseAddr, input, _) =>
      WriteAsyncOutOfBounds(writeMemoryController, baseAddr, JoinOrderedStream(input), rule)
  })

  def moveUpMapOrderedToUnorderedStream: Rule = Rule("moveUpMapOrderedToUnorderedStream", {
    // map fission
    // prevent infinite rewrites with this rule by checking that fun is not a ParamUse
    case MapOrderedStream(ArchLambda(origParam, OrderedStreamToUnorderedStream(fun, _), _), input, _) if !RulesHelper.isParamUse(fun) =>
      MapOrderedStream(
        OrderedStreamToUnorderedStream.asFunction(),
        MapOrderedStream(ArchLambda(origParam, fun), input)
      )

    // map map fission
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, OrderedStreamToUnorderedStream(ParamUse(p3), _), _), inputInner, _), _), inputOuter, _)
      if p2.id == p3.id &&
        // prevent infinite rewrites with this rule:
        (inputInner match {
          case ParamUse(p4) if p1.id == p4.id => false
          case _ => true
        }) =>
      MapOrderedStream(
        {
          val pOuter = ParamDef()
          ArchLambda(
            pOuter,
            MapOrderedStream(
              OrderedStreamToUnorderedStream.asFunction(),
              ParamUse(pOuter)
            )
          )
        },
        MapOrderedStream(
          ArchLambda(p1, inputInner),
          inputOuter
        )
      )
  })

  def replaceJoinUnorderedStream: Rule = Rule("replaceJoinUnorderedStream", {
    case JoinUnorderedStream(OrderedStreamToUnorderedStream(MapOrderedStream(ArchLambda(p1, OrderedStreamToUnorderedStream(ParamUse(p2), _), _), input, _), _), _) if p1.id == p2.id =>
      OrderedStreamToUnorderedStream(JoinOrderedStream(input))
  })

  def mapStreamFusion: Rule = Rule("mapStreamFusion", {
    case MapOrderedStream(f2, MapOrderedStream(f1, input, _), _) =>
      MapOrderedStream(
        {
          val param = ParamDef(f1.t.asInstanceOf[FunTypeT].inType)
          ArchLambda(
            param,
            FunctionCall(f2, FunctionCall(f1, ParamUse(param)))
          )
        },
        input
      )
  })

  def removeEmptyMap: Rule = Rule("removeEmptyMap", {
    case MapOrderedStream(ArchLambda(p1, ParamUse(p2), _), input, _)
      if p1.id == p2.id =>
      input
    case MapVector(ArchLambda(p1, ParamUse(p2), _), input, _)
      if p1.id == p2.id =>
      input
  })

  def mapFusionMapParam: Rule = Rule("mapFusionMapParam", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        body1.visit{
          case MapOrderedStream(f, ParamUse(p3), _) if p1.id == p3.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      }  =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def mapFissionMapVecFun: Rule = Rule("mapFissionMapVecFun", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case MapVector(fun, ParamUse(p2), _) if p1.id == p2.id && getUnboundParams(fun).isEmpty =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapVecFunOnly: Boolean = body match {
        case MapVector(fun, ParamUse(p2), _) if p1.id == p2.id => true
        case _ => false
      }
      !isMapVecFunOnly && exprFound && paramUses == 1
    } && {
      var exprFound = false
      body match{
        case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), _, _), _, _) if  p3.id == p4.id =>
          exprFound = true
        case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, VectorToOrderedStream(ParamUse(p5), _), _),
        VectorToOrderedStream(ParamUse(p6), _), _), _), _, _), _, _) if  p3.id == p6.id && p4.id == p5.id =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      var fun: LambdaT = ArchLambda(p1, body)
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case MapVector(f: LambdaT, ParamUse(p2), _) if p1.id == p2.id =>
                fun = f
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(
          {
            val param = ParamDef()
            ArchLambda(
              param,
              MapVector(fun, ParamUse(param))
            )
          },
          input
        )
      )

    // MapND MapVecFun
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
          p1.id == p2.id && {
            val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
            var innerExprFound = false
            innerMostBody.body match {
              case MapVector(fun, ParamUse(p3),_) if p3.id == innerBody.param.id =>
                innerExprFound = true
              case _ =>
            }
            innerExprFound
          } =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapVecFunOnly: Boolean = body match {
        case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
          p1.id == p2.id && {
            val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
            var innerExprFound = false
            innerMostBody.body match {
              case MapVector(fun, ParamUse(p3),_) if p3.id == innerBody.param.id =>
                innerExprFound = true
              case _ =>
            }
            innerExprFound
          } => true
        case _ => false
      }
      !isMapVecFunOnly && exprFound && paramUses == 1
    } && {
      var exprFound = false
      body match{
        case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p3, VectorToOrderedStream(ParamUse(p4), _), _), _, _), _, _) if  p3.id == p4.id =>
          exprFound = true
        case TransposeNDOrderedStream(MapOrderedStream(ArchLambda(p3, MapOrderedStream(ArchLambda(p4, VectorToOrderedStream(ParamUse(p5), _), _),
        VectorToOrderedStream(ParamUse(p6), _), _), _), _, _), _, _) if  p3.id == p6.id && p4.id == p5.id =>
          exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      var fun: LambdaT = ArchLambda(p1, body)
      var innerLevels: Int = 0
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case MapOrderedStream(innerBody: LambdaT, ParamUse(p2), _) if
                p1.id == p2.id && {
                  val innerMostBody = RulesHelper.getCurrentOrInnerMostLambda(innerBody)
                  var innerExprFound = false
                  innerLevels = RulesHelper.getMapNDLevels(innerBody)
                  innerMostBody.body match {
                    case MapVector(f: LambdaT, ParamUse(p3),_) if p3.id == innerBody.param.id =>
                      fun = f
                      innerExprFound = true
                    case _ =>
                  }
                  innerExprFound
                } =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(
          innerLevels + 1,
          {
            val param = ParamDef(RulesHelper.getInnerMostStreamType(input.t).asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              param,
              MapVector(fun, ParamUse(param))
            )
          }, input
        )
      )
  })

  def mapFissionMapStmFun: Rule = Rule("mapFissionMapStmFun", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case MapOrderedStream(fun, ParamUse(p2), _) if p1.id == p2.id && getUnboundParams(fun).isEmpty =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isMapFunOnly: Boolean = body match {
        case MapOrderedStream(fun, ParamUse(p2), _) if p1.id == p2.id => true
        case TransposeNDOrderedStream(_, _, _) => true  // avoid looping with moveDownTransposeRules
        case _ => false
      }
      !isMapFunOnly && exprFound && paramUses == 1
    } && { // avoid unbound map
      var exprFound = false
      body match {
        case MapOrderedStream(_, innerIn, _) if !RulesHelper.containsParam(innerIn, p1) => exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      var fun: Expr = ArchLambda(p1, body)
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case MapOrderedStream(f, ParamUse(p2), _) if p1.id == p2.id =>
                fun = f
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(
          {
            val param = ParamDef()
            ArchLambda(
              param,
              MapOrderedStream(fun, ParamUse(param))
            )
          },
          input
        )
      )
  })
}
