package backend.hdl.arch.rewrite

import backend.hdl.OrderedStreamTypeT
import backend.hdl.arch.mem.{BlockRamBuffer, BufferStreamInBlockRam}
import backend.hdl.arch.{Alternate, ArchLambda, InverseRepeatHidden, JoinStreamAll, MapOrderedStream, OrderedStreamToVector, ReduceOrderedStream, Repeat, RepeatHidden, SplitStreamAll, VectorToOrderedStream}
import core.{Expr, Let, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}
import core.util.IRDotGraph

object RepeatBufferingRules extends RulesT {

  /** Buffer stream data if any Repeat is not correctly moved down to counter. Because this stage (MapCompiler) happens
    * after MemoryLayout, it is impossible to insert "BRAM" or host buffer. To circumvent this problem, we insert a reg-
    * ister array to store the values in a stream. The register array is created from OrderedStreamToVector.
    */

  override protected def all(config: Option[Int]): Seq[Rule] = Seq(
    fixRepeatBuffer, repeatAlterBuffer, repeatHiddenAlterBuffer
  ) ++ MoveUpVectorToStreamRules.get() ++ RemoveStreamVectorConversionRules.get() ++ Seq(CleanupRules.removeEmptyMap)
  // MoveDownStreamToVectorRules is not included because we want to put Repeat and StreamToVector together!

  /**
   * Inserting block ram instead of stm2Vec-vec2Stm pair. stm2Vec-vec2Stm pair will create lots of registers which is
   * not practical for hardware design.
   */
  def bufferRepeatParam: Rule = Rule("bufferRepeatParam", {
    case Repeat(input@ParamUse(_), times, t: OrderedStreamTypeT) if t.et.isInstanceOf[OrderedStreamTypeT] =>
      Repeat(BufferStreamInBlockRam(input), times)
  })

  def fixRepeatBuffer: Rule = Rule("fixRepeatBuffer", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p3, body, _), ParamUse(p2), _), _), Repeat(Repeat(input, rep1, _), rep2, _), _) if
      p1.id == p2.id && {
        var paramCount: Int = 0
        body.visit {
          case BlockRamBuffer(_, _, _, _) => paramCount = paramCount + 1
          case _ =>
        }
        paramCount == 1
      } || {
        var paramCount: Int = 0
        body.visit {
          case Alternate(f1, f2, _, _) if {
            var f1Found = false
            var f2Found = false
            f1.visit { case BlockRamBuffer(_, _, _, _) => f1Found = true case _ => }
            f2.visit { case BlockRamBuffer(_, _, _, _) => f2Found = true case _ => }
            f1Found && f2Found
          } => paramCount = paramCount + 1
          case _ =>
        }
        paramCount == 1
      } =>
      Repeat(Repeat(body.visitAndRebuild {
        case ParamUse(p4) if p3.id == p4.id =>
          input
        case e => e
      }.asInstanceOf[Expr], rep1), rep2)


    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p3, body, _), ParamUse(p2), _), _), Repeat(input, rep1, _), _) if
      p1.id == p2.id && {
        var paramCount: Int = 0
        body.visit {
          case BlockRamBuffer(_, _, _, _) => paramCount = paramCount + 1
          case _ =>
        }
        paramCount == 1
      } || {
        var paramCount: Int = 0
        body.visit {
          case Alternate(f1, f2, _, _) if {
            var f1Found = false
            var f2Found = false
            f1.visit { case BlockRamBuffer(_, _, _, _) => f1Found = true case _ => }
            f2.visit { case BlockRamBuffer(_, _, _, _) => f2Found = true case _ => }
            f1Found && f2Found
          } => paramCount = paramCount + 1
          case _ =>
        }
        paramCount == 1
      } =>
      Repeat(body.visitAndRebuild {
        case ParamUse(p4) if p3.id == p4.id =>
          input
        case e => e
      }.asInstanceOf[Expr], rep1)
  })

  def repeatAlterBuffer: Rule = Rule("repeatAlterBuffer", {
    case Repeat(Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _), times, _) if {
      var f1Found = false
      var f2Found = false
      body1.visit { case BlockRamBuffer(_, _, _, _) => f1Found = true case _ => }
      body2.visit { case BlockRamBuffer(_, _, _, _) => f2Found = true case _ => }
      f1Found && f2Found
    } =>
      Alternate(ArchLambda(p1, Repeat(body1, times)), ArchLambda(p2, Repeat(body2, times)), input)
  })

  def repeatHiddenAlterBuffer: Rule = Rule("repeatHiddenAlterBuffer", {
    case RepeatHidden(Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _), times, _) if {
      var f1Found = false
      var f2Found = false
      body1.visit { case BlockRamBuffer(_, _, _, _) => f1Found = true case _ => }
      body2.visit { case BlockRamBuffer(_, _, _, _) => f2Found = true case _ => }
      f1Found && f2Found
    } && {
      var exprFound = false
      body1.visit {
        case Let(_, _, ReduceOrderedStream(_, _, _, _), _) => exprFound = true
        case Repeat(ReduceOrderedStream(_, _, _, _), _, _) => exprFound = true
        case _ =>
      }
      exprFound
    } && {
      var exprFound = false
      body2.visit {
        case Let(_, _, ReduceOrderedStream(_, _, _, _), _) => exprFound = true
        case Repeat(ReduceOrderedStream(_, _, _, _), _, _) => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newBody1 = body1.visitAndRebuild {
        case Let(p, body, reduceExpr@ReduceOrderedStream(_, _, _, _), _) => Let(p, body, InverseRepeatHidden(reduceExpr, times))
        case Repeat(reduceExpr@ReduceOrderedStream(_, _, _, _), repTimes, _) => Repeat(InverseRepeatHidden(reduceExpr, times), repTimes)
        case e => e
      }.asInstanceOf[Expr]
      val newBody2 = body2.visitAndRebuild {
        case Let(p, body, reduceExpr@ReduceOrderedStream(_, _, _, _), _) => Let(p, body, InverseRepeatHidden(reduceExpr, times))
        case Repeat(reduceExpr@ReduceOrderedStream(_, _, _, _), repTimes, _) => Repeat(InverseRepeatHidden(reduceExpr, times), repTimes)
        case e => e
      }.asInstanceOf[Expr]
      Alternate(ArchLambda(p1, RepeatHidden(newBody1, times)), ArchLambda(p2, RepeatHidden(newBody2, times)), RepeatHidden(input, times))
  })
}
