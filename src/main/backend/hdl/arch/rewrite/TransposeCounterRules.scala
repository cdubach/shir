package backend.hdl.arch.rewrite

import backend.hdl.{IntType, OrderedStreamTypeT, VectorTypeT}
import backend.hdl.arch.{AddInt, ArchLambda, CounterInteger, JoinOrderedStream, MapOrderedStream, OrderedStreamToVector, ResizeInteger, SplitOrderedStream, SplitVector, TransposeNDOrderedStream, Tuple2, VectorToOrderedStream}
import core.{?, ArithType, ArithTypeT, Expr, Marker, ParamDef, ParamUse, TextType, TypeChecker}
import core.rewrite.{Rule, RulesT}

object TransposeCounterRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      splitCounterUnderTranspose,
      mapFusionUnderTranspose,
      transposeFlipMapCounters,
    ) ++ Seq(
      MoveDownSplitStreamRules.moveDownMapSplitStream1,
      MergeIntoCounterRules.mergeMapSplitStream
    )

  def mapFusionUnderTranspose: Rule = Rule("mapFusionUnderTranspose", {
    case TransposeNDOrderedStream(body, dimOrder, _) if {
      var exprFound = false
      body.visit{
        case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
        input, _), _)
          if {
            var exprFound = false
            body1.visit{
              case ParamUse(p3) if p1.id == p3.id =>
                exprFound = true
              case _ =>
            }
            exprFound
          } && {
            var exprFound = false
            body2.visit{
              case ParamUse(p4) if p2.id == p4.id =>
                exprFound = true
              case _ =>
            }
            exprFound
          }  => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newBody = body.visitAndRebuild{
        case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
        input, _), _)
          if {
            var exprFound = false
            body1.visit{
              case ParamUse(p3) if p1.id == p3.id =>
                exprFound = true
              case _ =>
            }
            exprFound
          } && {
            var exprFound = false
            body2.visit{
              case ParamUse(p4) if p2.id == p4.id =>
                exprFound = true
              case _ =>
            }
            exprFound
          }  =>
          MapOrderedStream({
            val newParam1 = ParamDef()
            ArchLambda(
              newParam1,
              body1.visitAndRebuild{
                case ParamUse(usedParam1) if usedParam1.id == p1.id =>
                  body2.visitAndRebuild{
                    case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                      ParamUse(newParam1)
                    case e => e
                  }.asInstanceOf[Expr]
                case e => e
              }.asInstanceOf[Expr]
            )
          }, input
          )
        case e => e
      }.asInstanceOf[Expr]
      TransposeNDOrderedStream(newBody, dimOrder)
  })

  def splitCounterUnderTranspose: Rule = Rule("splitCounterUnderTranspose", {
    case TransposeNDOrderedStream(body, dimOrder, _) if {
      var exprFound = false
      body.visit{
        case CounterInteger(_, _, _, _, _, t: OrderedStreamTypeT) if t.dimensions.length > 1 => exprFound = true
        case _ =>
      }
      body match {
        case CounterInteger(_) => exprFound = false
        case _ =>
      }
      exprFound
    } && {
      // All Split must be merged.
      var exprFound = false
      body.visit{
        case SplitOrderedStream(_, _, _) => exprFound = true
        case _ =>
      }
      !exprFound
    } =>
      val newBody = body.visitAndRebuild{
        case CounterInteger(start, inc, loop, dim, rep, t: OrderedStreamTypeT) if t.dimensions.length > 1 =>
          val bitWidth = t.leafType.bitWidth
          val dimOrder = Seq.range(0, dim.length).map(i => ArithType(i))
          generateSumOfCounters(start, inc, loop, dim, rep, dimOrder, bitWidth)._1
        case e => e
      }.asInstanceOf[Expr]
      TransposeNDOrderedStream(newBody, dimOrder)
  })


  def generateSumOfCounters(start: ArithTypeT, increment: ArithTypeT, loop: ArithTypeT, dimensions: Seq[ArithTypeT],
    repetitions: Seq[ArithTypeT], dimOrder: Seq[ArithTypeT], bitWidth: ArithTypeT): (Expr, Long) = {
    // dimOrder shouldn't be empty
    // (if it does happen, the match would throw an error)
    dimOrder match {
      case Seq(x) =>
        val order = x.ae.evalInt
        val dim = dimensions(order)
        val rep = repetitions(order)
        val incr = dimensions.slice(0, order).foldLeft(increment.ae)(_ * _.ae)
        val last = start.ae + (if (rep.ae.evalInt == 1) 0 else incr * (dim.ae - 1))
        (CounterInteger(start, incr, loop, Seq(dim), Seq(rep), Some(IntType(bitWidth))), last.evalInt)
      case xs :+ x =>
        val (prevSum, prevLast) = generateSumOfCounters(start, increment, loop, dimensions, repetitions, xs, bitWidth)
        val prevSumtc = TypeChecker.check(prevSum)
        val prevSumLevel = prevSumtc.t.asInstanceOf[OrderedStreamTypeT].dimensions.length

        val order = x.ae.evalInt
        val dim = dimensions(order)
        val rep = repetitions(order)
        val incr = dimensions.slice(0, order).foldLeft(increment.ae)(_ * _.ae)
        val last = prevLast + (if (rep.ae.evalInt == 1) 0 else (incr * (dim.ae - 1)).evalInt)

        val counter = CounterInteger(
          ArithType(0), incr, loop, Seq(dim), Seq(rep),
          Some(IntType(last.toBinaryString.length)))

        (MapOrderedStream(
          {
            val param1 = ParamDef(counter.t.asInstanceOf[OrderedStreamTypeT].et)
            ArchLambda(
              param1,
              MapOrderedStream(
                prevSumLevel,
                {
                  val param2 = ParamDef(prevSumtc.t.asInstanceOf[OrderedStreamTypeT].leafType)
                  ArchLambda(
                    param2,
                    TypeChecker.check(ResizeInteger(AddInt(Tuple2(ParamUse(param1), ParamUse(param2))), bitWidth))
                  )
                }, prevSumtc
              )
            )
          }, counter
        ),last)
    }
  }

  private def getMapCounterSequence(expr: Expr): Seq[(ParamDef, Expr)] = expr match {
    case MapOrderedStream(ArchLambda(p, body, _), counter @ CounterInteger(_, _, _, _, _, t: OrderedStreamTypeT), _) if
      t.dimensions.length == 1 =>
      getMapCounterSequence(body) :+ (p, counter)
    case MapOrderedStream(ArchLambda(p, body, _), counter @ ParamUse(pu), _) if
      pu.t.asInstanceOf[OrderedStreamTypeT].dimensions.length == 1 =>
      getMapCounterSequence(body) :+ (p, counter)
    case _ => Seq()
  }

  private def getMapCounterInnermostBody(expr: Expr): Expr = expr match {
    case MapOrderedStream(ArchLambda(_, body, _), CounterInteger(_, _, _, _, _, t: OrderedStreamTypeT), _) if
      t.dimensions.length == 1 =>
      getMapCounterInnermostBody(body)
    case MapOrderedStream(ArchLambda(_, body, _), ParamUse(pu), _) if
      pu.t.asInstanceOf[OrderedStreamTypeT].dimensions.length == 1 =>
      getMapCounterInnermostBody(body)
    case e => e
  }

  private def buildMapCounter(body: Expr, mapCounterSeq: Seq[(ParamDef, Expr)]): Expr = {
    if(mapCounterSeq.length == 0) {
      body
    } else {
      val (p, counter) = mapCounterSeq.last
      MapOrderedStream(
        ArchLambda(
          p, buildMapCounter(body, mapCounterSeq.init)
        ),
        counter
      )
    }
  }

  def transposeFlipMapCounters: Rule = Rule("transposeFlipMapCounters", {
    case TransposeNDOrderedStream(input, dimOrder, _) if getMapCounterSequence(input).length == dimOrder.length =>
      val counterSeq = getMapCounterSequence(input)
      val innermostBody = getMapCounterInnermostBody(input)
      val newCounterSeq = dimOrder.map(_.ae.evalInt).map(i => counterSeq(i))
      buildMapCounter(innermostBody, newCounterSeq)
  })
}