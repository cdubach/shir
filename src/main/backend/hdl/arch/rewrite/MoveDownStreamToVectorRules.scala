package backend.hdl.arch.rewrite

import backend.hdl.arch.mem.MemoryAllocationExpr
import backend.hdl.{OrderedStreamTypeT, VectorTypeT}
import backend.hdl.arch.rewrite.sharedFunc.ReshapeFuncRules
import backend.hdl.arch.{AddInt, Alternate, ArchLambda, ClipBankersRound, ConcatOrderedStream, ConcatVector, ConstantValue, DropOrderedStream, DropVector, JoinOrderedStream, JoinVector, MapOrderedStream, MapOrderedStream2Input, MapVector, MaxInt, OrderedStreamToVector, Repeat, Select2, SplitOrderedStream, SplitVector, Tuple2, VectorGenerator, Zip2OrderedStream, ZipVector}
import core.{Conversion, Expr, LambdaT, Let, ParamDef, ParamUse, TypeChecker}
import core.rewrite.{Rule, RulesT}
import core.util.IRDotGraph

object MoveDownStreamToVectorRules extends RulesT {

  override protected def all(config: Option[Int]): Seq[Rule] =
    Seq(
      mapFission,
      //      moveDownMapStreamToVector2,
      //      moveDownMapStreamToVector3
    )

  def skipTupleStructure: Seq[Rule] =
    Seq(
      mapFusion,
      skipZipStream2DTuple,
      mapFissionND,
      skipSelectTuple,
      mapFusionMapParam,
      skipAlternate,
      leaveAlternate,
      skipLet,
      map2InputFissionND,
      skipConcatStm,
      skipSplitStm,
      skipRepeat,
      skipMapJoinVec,
      skipMapConversion,
      skipMapReluRound,
      skipSplit,
      skipDrop,
      skipZips,
      skipMapAdd,
    ) ++ all(None) ++ Seq(ReshapeFuncRules.moveOutMapStmToVec)

  def mapFission: Rule = Rule("moveDownMapStreamToVector_mapFission", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit{
        case OrderedStreamToVector(ParamUse(p2), _) if p1.id == p2.id =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case OrderedStreamToVector(ParamUse(p2), _) if p1.id == p2.id => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case OrderedStreamToVector(ParamUse(p2), _) if p1.id == p2.id =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(OrderedStreamToVector.asFunction(), input)
      )
  })

  def mapFissionND: Rule = Rule("moveDownMapStreamToVector_mapFissionND", {
    case MapOrderedStream(ArchLambda(p1, body, _), input, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case OrderedStreamToVector(ParamUse(p3), _) if p3.id == innerMostFun.param.id => exprFound1 = true
            case _ =>
          }
          exprFound1
        } =>
          exprFound = true
        case ParamUse(p2) if p1.id == p2.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      // prevent loops; check if this rewrite rule has already been applied
      val isStmToVecOnly: Boolean = body match {
        case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case OrderedStreamToVector(ParamUse(p3), _) if p3.id == innerMostFun.param.id => exprFound1 = true
            case _ =>
          }
          exprFound1
        } => true
        case _ => false
      }
      !isStmToVecOnly && exprFound && paramUses == 1
    } =>
      var level: Int = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case OrderedStreamToVector(ParamUse(p3), _) if p3.id == innerMostFun.param.id =>
              level = RulesHelper.getMapNDLevels(fun)
              exprFound1 = true
            case _ =>
          }
          exprFound1
        } =>
        case _ =>
      }

      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild {
              case MapOrderedStream(fun: LambdaT, ParamUse(p2), _) if p1.id == p2.id && {
                var exprFound1 = false
                val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
                innerMostFun.body match {
                  case OrderedStreamToVector(ParamUse(p3), _) if p3.id == innerMostFun.param.id =>
                    level = RulesHelper.getMapNDLevels(fun)
                    exprFound1 = true
                  case _ =>
                }
                exprFound1
              } =>
                ParamUse(param)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        MapOrderedStream(level + 1, OrderedStreamToVector.asFunction(), input)
      )
  })

  //  def moveDownMapStreamToVector2: Rule = Rule("moveDownMapStreamToVector2", {
  //    case MapOrderedStream(fun @ ArchLambda(p1, OrderedStreamToVector(ParamUse(p2, _), _), _), Marker(input, text), _) if p1.id == p2.id =>
  //      Marker(MapOrderedStream(fun, input), text)
  //    case OrderedStreamToVector(Conversion(input, _), t) =>
  //      ???
  //      Conversion(SplitOrderedStream(input, chunkSize), t)
  //  })

  //  def moveDownMapStreamToVector3: Rule = Rule("moveDownMapStreamToVector3", {
  //    // map fusion
  //    case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2, _), _), _), MapOrderedStream(ArchLambda(param, body, _), input, _), _) if p1.id == p2.id =>
  //      MapOrderedStream(
  //        {
  //          ArchLambda(
  //            param,
  //            OrderedStreamToVector(body)
  //          )
  //        },
  //        input
  //      )
  //  })

  def skipDropStream: Rule = Rule("moveDownMapStreamToVector_skipDropStream", {
    case OrderedStreamToVector(DropOrderedStream(input, firstElements, lastElements, _), _) =>
      DropVector(OrderedStreamToVector(input), firstElements, lastElements)
  })

  def mapFusion: Rule = Rule("moveDownMapStreamToVector_mapFusion", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        body1.visit{
          case OrderedStreamToVector(ParamUse(p3), _) if p1.id == p3.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val stm2vecInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        stm2vecInnerMost.body match {
          case OrderedStreamToVector(ParamUse(p3), _) if p3.id == stm2vecInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def mapFusionMapParam: Rule = Rule("moveDownMapStreamToVector_mapFusionMapParam", {
    case MapOrderedStream(ArchLambda(p1, body1, _) , MapOrderedStream(ArchLambda(p2, body2,_),
    input, _), _)
      if {
        var exprFound = false
        val stm2vecInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p1, body1))
        stm2vecInnerMost.body match {
          case OrderedStreamToVector(ParamUse(p3), _) if p3.id == stm2vecInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        body2.visit{
          case ParamUse(p4) if p2.id == p4.id =>
            exprFound = true
          case _ =>
        }
        exprFound
      } && {
        var exprFound = false
        val stm2vecInnerMost = RulesHelper.getCurrentOrInnerMostLambda(ArchLambda(p2, body2))
        stm2vecInnerMost.body match {
          case OrderedStreamToVector(ParamUse(p3), _) if p3.id == stm2vecInnerMost.param.id =>
            exprFound = true
          case _ =>
        }
        !exprFound
      } =>
      MapOrderedStream({
        val newParam1 = ParamDef()
        ArchLambda(
          newParam1,
          body1.visitAndRebuild{
            case ParamUse(usedParam1) if usedParam1.id == p1.id =>
              body2.visitAndRebuild{
                case ParamUse(usedParam2) if usedParam2.id == p2.id =>
                  ParamUse(newParam1)
                case e => e
              }.asInstanceOf[Expr]
            case e => e
          }.asInstanceOf[Expr]
        )
      }, input
      )
  })

  def skipZipStream2DTuple: Rule = Rule("moveDownMapStreamToVector_skipZipStream2DTuple", {
    case MapOrderedStream(ArchLambda(p1, body, _),
    Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if {
      var exprFound = false
      body.visit{
        case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p2), _), _) if p1.id == p2.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = Zip2OrderedStream(Tuple2(
        MapOrderedStream(OrderedStreamToVector.asFunction(), input1),
        MapOrderedStream(OrderedStreamToVector.asFunction(), input2)
      ))
      MapOrderedStream(
        {
          val param = ParamDef()
          ArchLambda(
            param,
            body.visitAndRebuild{
              case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p2), _), _) if p1.id == p2.id =>
                ZipVector(ParamUse(param))
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )
  })

  def skipZipStream3DTuple: Rule = Rule("moveDownMapStreamToVector_skipZipStream3DTuple", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, body, _), Zip2OrderedStream(ParamUse(p3), _), _), _),
    Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if p1.id == p3.id &&{
      var exprFound = false
      body.visit {
        case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p4), _), _) if p2.id == p4.id =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(Tuple2(
        MapOrderedStream(2, OrderedStreamToVector.asFunction(), input1),
        MapOrderedStream(2, OrderedStreamToVector.asFunction(), input2)
      )))
      MapOrderedStream(
        {
          val param1 = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param1,
            MapOrderedStream(
              {
                val param2 = ParamDef()
                ArchLambda(
                  param2,
                  body.visitAndRebuild {
                    case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p4), _), _) if p2.id == p4.id =>
                      ZipVector(ParamUse(param2))
                    case e => e
                  }.asInstanceOf[Expr]
                )
              }, Zip2OrderedStream(ParamUse(param1))
            )
          )
        }, newInput
      )
  })

  def skipSelectTuple: Rule = Rule("moveDownMapStreamToVector_skipSelectTuple", {
    case MapOrderedStream(fun: LambdaT, Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if {
      var exprFound = false
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _), Select2(ParamUse(p), sel, _), _)
          if p1.id == p2.id  && fun.param.id == p.id && sel.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(
        Tuple2(MapOrderedStream(2, OrderedStreamToVector.asFunction(), input1), input2)
      ))

      MapOrderedStream(
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            fun.body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _), Select2(ParamUse(p), sel, _), _)
                if p1.id == p2.id  && fun.param.id == p.id && sel.ae.evalInt == 0 =>
                Select2(ParamUse(param), sel)
              case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt != 0 =>
                Select2(ParamUse(param), sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        newInput
      )

    case MapOrderedStream(fun: LambdaT, Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if {
      var exprFound = false
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _), Select2(ParamUse(p), sel, _), _)
          if p1.id == p2.id  && fun.param.id == p.id &&  sel.ae.evalInt == 1 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(
        Tuple2(input1, MapOrderedStream(2, OrderedStreamToVector.asFunction(), input2))
      ))

      MapOrderedStream(
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            fun.body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _), Select2(ParamUse(p), sel, _), _)
                if p1.id == p2.id  && fun.param.id == p.id &&  sel.ae.evalInt == 1 =>
                Select2(ParamUse(param), sel)
              case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt != 1 =>
                Select2(ParamUse(param), sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        newInput
      )

    case MapOrderedStream(fun: LambdaT, Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if {
      var exprFound = false
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _), _), Select2(ParamUse(p), sel, _), _)
          if p1.id == p4.id && p2.id == p3.id && fun.param.id == p.id && sel.ae.evalInt == 0 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(
        Tuple2(MapOrderedStream(3, OrderedStreamToVector.asFunction(), input1), input2)
      ))

      MapOrderedStream(
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            fun.body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _), _), Select2(ParamUse(p), sel, _), _)
                if p1.id == p4.id && p2.id == p3.id && fun.param.id == p.id && sel.ae.evalInt == 0 =>
                Select2(ParamUse(param), sel)
              case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt != 0 =>
                Select2(ParamUse(param), sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        newInput
      )

    case MapOrderedStream(fun: LambdaT, Zip2OrderedStream(Tuple2(input1, input2, _), _), _) if {
      var exprFound = false
      fun.body.visit{
        case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _), _), Select2(ParamUse(p), sel, _), _)
          if p1.id == p4.id && p2.id == p3.id && fun.param.id == p.id && sel.ae.evalInt == 1 =>
          exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(
        Tuple2(input1, MapOrderedStream(3, OrderedStreamToVector.asFunction(), input2))
      ))

      MapOrderedStream(
        {
          val param = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            param,
            fun.body.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _), _), Select2(ParamUse(p), sel, _), _)
                if p1.id == p4.id && p2.id == p3.id && fun.param.id == p.id && sel.ae.evalInt == 1 =>
                Select2(ParamUse(param), sel)
              case Select2(ParamUse(p), sel, _) if fun.param.id == p.id && sel.ae.evalInt != 1 =>
                Select2(ParamUse(param), sel)
              case e => e
            }.asInstanceOf[Expr]
          )
        },
        newInput
      )
  })

  def skipJoinStream: Rule = Rule("moveDownMapStreamToVector_skipJoinStream", {
    //case OrderedStreamToVector(JoinOrderedStream(input, _), _) =>
    //  JoinVector(OrderedStreamToVector(MapOrderedStream(OrderedStreamToVector.asFunction(), input)))
    case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _),
    JoinOrderedStream(input, _), _) if p1.id == p2.id =>
      JoinOrderedStream(MapOrderedStream(2, OrderedStreamToVector.asFunction(), input))
  })

  def skipJoinStreamV2: Rule = Rule("moveDownMapStreamToVector_skipJoinStreamV2", {
    case OrderedStreamToVector(JoinOrderedStream(input, _), _) =>
      JoinVector(OrderedStreamToVector(TypeChecker.check(MapOrderedStream(OrderedStreamToVector.asFunction(), input))))
    case MapOrderedStream(fun: LambdaT, JoinOrderedStream(input, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case OrderedStreamToVector(ParamUse(p1), _) if p1.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val level = RulesHelper.getMapNDLevels(fun)
      JoinOrderedStream(TypeChecker.check(MapOrderedStream(level+1, OrderedStreamToVector.asFunction(), input)))
  })

  def skipRepeat: Rule = Rule("moveDownMapStreamToVector_skipRepeat", {
    case OrderedStreamToVector(Repeat(input, times, _), _) =>
      VectorGenerator(input, times)
    case MapOrderedStream(fun: LambdaT, Repeat(input, times, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case OrderedStreamToVector(ParamUse(p3), _) if p3.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val level = RulesHelper.getMapNDLevels(fun)
      Repeat(MapOrderedStream(level - 1, OrderedStreamToVector.asFunction(), input), times)
  })

  def skipAlternate: Rule = Rule("moveDownMapStreamToVector_skipAlternate", {
    case MapOrderedStream(fun: LambdaT, Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case OrderedStreamToVector(ParamUse(p3), _) if p3.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val level = RulesHelper.getMapNDLevels(fun)
      Alternate(ArchLambda(p1, MapOrderedStream(level, OrderedStreamToVector.asFunction(), body1)), ArchLambda(p2, MapOrderedStream(level, OrderedStreamToVector.asFunction(), body2)), input)
  })

  def leaveAlternate: Rule = Rule("moveDownMapStreamToVector_leaveAlternate", {
    case Alternate(ArchLambda(p1, body1, _), ArchLambda(p2, body2, _), input, _)if {
      var p1Levels: Int = -1
      var p2Levels: Int = -1
      var param1Count: Int = 0
      var param2Count: Int = 0
      var mapParam1Count: Int = 0
      var mapParam2Count: Int = 0
      body1.visit{
        case MapOrderedStream(fun: LambdaT, ParamUse(p3), _) if p1.id == p3.id && {
          var exprFound = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case OrderedStreamToVector(ParamUse(p), _) if p.id == innerMostFun.param.id => exprFound = true
            case _ =>
          }
          exprFound
        } =>
          mapParam1Count = mapParam1Count + 1
          p1Levels = RulesHelper.getMapNDLevels(fun)
        case ParamUse(p3) if p1.id == p3.id => param1Count = param1Count + 1
        case _ =>
      }
      body2.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p4), _) if p2.id == p4.id && {
          var exprFound = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case OrderedStreamToVector(ParamUse(p), _) if p.id == innerMostFun.param.id => exprFound = true
            case _ =>
          }
          exprFound
        } =>
          mapParam2Count = mapParam2Count + 1
          p2Levels = RulesHelper.getMapNDLevels(fun)
        case ParamUse(p4) if p2.id == p4.id => param2Count = param2Count + 1
        case _ =>
      }
      p1Levels == p2Levels && p1Levels > 0 && mapParam1Count == param1Count && mapParam2Count == param2Count
    } =>
      var levels: Int = 0
      body1.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p3), _) if p1.id == p3.id && {
          var exprFound = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case OrderedStreamToVector(ParamUse(p), _) if p.id == innerMostFun.param.id => exprFound = true
            case _ =>
          }
          exprFound
        } =>
          levels = RulesHelper.getMapNDLevels(fun)
        case _ =>
      }

      val newInput = TypeChecker.check(MapOrderedStream(levels, OrderedStreamToVector.asFunction(), input))
      val newParam1 = ParamDef(newInput.t)
      val newParam2 = ParamDef(newInput.t)

      val newBody1 = body1.visitAndRebuild{
        case MapOrderedStream(fun: LambdaT, ParamUse(p3), _) if p1.id == p3.id => ParamUse(newParam1)
        case e => e
      }.asInstanceOf[Expr]

      val newBody2 = body2.visitAndRebuild {
        case MapOrderedStream(fun: LambdaT, ParamUse(p4), _) if p2.id == p4.id => ParamUse(newParam2)
        case e => e
      }.asInstanceOf[Expr]

      Alternate(ArchLambda(newParam1, newBody1), ArchLambda(newParam2, newBody2), newInput)
  })

  def skipLet: Rule = Rule("moveDownMapStreamToVector_skipLet", {
    case MapOrderedStream(fun: LambdaT, Let(p, body, arg, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case OrderedStreamToVector(ParamUse(p), _) if p.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound && !arg.isInstanceOf[MemoryAllocationExpr]
    } =>
      Let(p, MapOrderedStream(fun, body), arg)
  })

  /**
   * Remove MapND-StmToVec out of Map2Input.
   */

  def map2InputFissionND: Rule = Rule("moveDownMapStreamToVector_map2InputFissionND", {
    case MapOrderedStream2Input(ArchLambda(p1, ArchLambda(p2, body, _), _), input1, input2, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p3), _) if p1.id == p3.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case OrderedStreamToVector(ParamUse(p4), _) if p4.id == innerMostFun.param.id => exprFound1 = true
            case _ =>
          }
          exprFound1
        } =>
          exprFound = true
        case ParamUse(p3) if p1.id == p3.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      exprFound && paramUses == 1
    } =>
      var level: Int = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p3), _) if p1.id == p3.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case OrderedStreamToVector(ParamUse(p4), _) if p4.id == innerMostFun.param.id =>
              level = RulesHelper.getMapNDLevels(fun)
              exprFound1 = true
            case _ =>
          }
          exprFound1
        } =>
        case _ =>
      }

      MapOrderedStream2Input(
        {
          val param1 = ParamDef()
          val param2 = ParamDef()
          ArchLambda(
            param1,
            ArchLambda(
              param2,
              body.visitAndRebuild {
                case MapOrderedStream(fun: LambdaT, ParamUse(p3), _) if p1.id == p3.id && {
                  var exprFound1 = false
                  val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
                  innerMostFun.body match {
                    case OrderedStreamToVector(ParamUse(p4), _) if p4.id == innerMostFun.param.id =>
                      level = RulesHelper.getMapNDLevels(fun)
                      exprFound1 = true
                    case _ =>
                  }
                  exprFound1
                } =>
                  ParamUse(param1)
                case ParamUse(p3) if p2.id == p3.id => ParamUse(param2)
                case e => e
              }.asInstanceOf[Expr]
            )
          )
        },
        MapOrderedStream(level + 1, OrderedStreamToVector.asFunction(), input1),
        input2
      )

    case MapOrderedStream2Input(ArchLambda(p1, ArchLambda(p2, body, _), _), input1, input2, _) if {
      var exprFound = false
      var paramUses = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p3), _) if p2.id == p3.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case OrderedStreamToVector(ParamUse(p4), _) if p4.id == innerMostFun.param.id => exprFound1 = true
            case _ =>
          }
          exprFound1
        } =>
          exprFound = true
        case ParamUse(p3) if p2.id == p3.id =>
          paramUses = paramUses + 1
        case _ =>
      }
      exprFound && paramUses == 1
    } =>
      var level: Int = 0
      body.visit {
        case MapOrderedStream(fun: LambdaT, ParamUse(p3), _) if p2.id == p3.id && {
          var exprFound1 = false
          val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
          innerMostFun.body match {
            case OrderedStreamToVector(ParamUse(p4), _) if p4.id == innerMostFun.param.id =>
              level = RulesHelper.getMapNDLevels(fun)
              exprFound1 = true
            case _ =>
          }
          exprFound1
        } =>
        case _ =>
      }

      MapOrderedStream2Input(
        {
          val param1 = ParamDef()
          val param2 = ParamDef()
          ArchLambda(
            param1,
            ArchLambda(
              param2,
              body.visitAndRebuild {
                case MapOrderedStream(fun: LambdaT, ParamUse(p3), _) if p2.id == p3.id && {
                  var exprFound1 = false
                  val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
                  innerMostFun.body match {
                    case OrderedStreamToVector(ParamUse(p4), _) if p4.id == innerMostFun.param.id =>
                      level = RulesHelper.getMapNDLevels(fun)
                      exprFound1 = true
                    case _ =>
                  }
                  exprFound1
                } =>
                  ParamUse(param2)
                case ParamUse(p3) if p1.id == p3.id => ParamUse(param1)
                case e => e
              }.asInstanceOf[Expr]
            )
          )
        },
        input1,
        MapOrderedStream(level + 1, OrderedStreamToVector.asFunction(), input2)
      )
  })

  def skipConcatStm: Rule = Rule("moveDownMapStreamToVector_skipConcatStm", {
    case OrderedStreamToVector(ConcatOrderedStream(input1, input2, dim, _), _) if dim.ae.evalInt == 0 =>
      ConcatVector(Tuple2(OrderedStreamToVector(input1), OrderedStreamToVector(input2)))
    case MapOrderedStream(fun: LambdaT, ConcatOrderedStream(input1, input2, dim, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val level = RulesHelper.getMapNDLevels(fun)
      innerMostFun.body match {
        case OrderedStreamToVector(ParamUse(p3), _) if p3.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound && level > dim.ae.evalInt
    } =>
      val level = RulesHelper.getMapNDLevels(fun)
      ConcatOrderedStream(MapOrderedStream(level, OrderedStreamToVector.asFunction(), input1), MapOrderedStream(level, OrderedStreamToVector.asFunction(), input2), dim)
  })

  def skipSplitStm: Rule = Rule("moveDownMapStreamToVector_skipSplitStm", {
    case MapOrderedStream(fun: LambdaT, SplitOrderedStream(input, chunkSize, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      val level = RulesHelper.getMapNDLevels(fun)
      innerMostFun.body match {
        case OrderedStreamToVector(ParamUse(p3), _) if p3.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound && level > 1
    } =>
      val level = RulesHelper.getMapNDLevels(fun)
      SplitOrderedStream(MapOrderedStream(level - 1, OrderedStreamToVector.asFunction(), input), chunkSize)
  })

  def skipMapJoinVec: Rule = Rule("moveDownMapStreamToVector_skipMapJoinVec", {
    case OrderedStreamToVector(MapOrderedStream(ArchLambda(p1, JoinVector(ParamUse(p2), _), _), input, _), _) if
      p1.id == p2.id =>
      MapVector(JoinVector.asFunction(), OrderedStreamToVector(input))
  })

  def skipMapConversion: Rule = Rule("moveDownMapStreamToVector_skipMapConversion", {
    case OrderedStreamToVector(MapOrderedStream(ArchLambda(p1, Conversion(ParamUse(p2), t), _), input, _), _) if
      p1.id == p2.id =>
      val vecInput = TypeChecker.check(OrderedStreamToVector(input))
      MapVector({
        val param = ParamDef(vecInput.t.asInstanceOf[VectorTypeT].et)
        ArchLambda(param, Conversion(ParamUse(param), t))
      }, vecInput
      )
  })

  def skipMapReluRound: Rule = Rule("moveDownMapStreamToVector_skipMapReluRound", {
    case OrderedStreamToVector(MapOrderedStream(ArchLambda(p1, MaxInt(Tuple2(ClipBankersRound(ParamUse(p2), le, he, _),
    constantV @ ConstantValue(_, _), _), _), _), input, _), _) if
      p1.id == p2.id =>
      val vecInput = TypeChecker.check(OrderedStreamToVector(input))
      MapVector({
        val param = ParamDef(vecInput.t.asInstanceOf[VectorTypeT].et)
        ArchLambda(param, MaxInt(Tuple2(ClipBankersRound(ParamUse(param), le, he), constantV)))
      }, vecInput
      )
  })

  def skipMapAdd: Rule = Rule("moveDownMapStreamToVector_skipMapAdd", {
    case OrderedStreamToVector(MapOrderedStream(ArchLambda(p1, AddInt(ParamUse(p2), _), _), input, _), _) if
      p1.id == p2.id =>
      val vecInput = TypeChecker.check(OrderedStreamToVector(input))
      MapVector({
        val param = ParamDef(vecInput.t.asInstanceOf[VectorTypeT].et)
        ArchLambda(param, AddInt(ParamUse(param)))
      }, vecInput
      )
  })

  def skipSplit: Rule = Rule("moveDownMapStreamToVector_skipSplit", {
    case OrderedStreamToVector(MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), _), _),
    SplitOrderedStream(input, chunkSize, _), _), _) if p1.id == p2.id =>
      SplitVector(OrderedStreamToVector(input), chunkSize)
  })

  def skipDrop: Rule = Rule("moveDownMapStreamToVector_skipDrop", {
    case MapOrderedStream(fun: LambdaT, DropOrderedStream(input, fe, le, _), _) if {
      var exprFound = false
      val innerMostFun = RulesHelper.getCurrentOrInnerMostLambda(fun)
      innerMostFun.body match {
        case OrderedStreamToVector(ParamUse(p3), _) if p3.id == innerMostFun.param.id => exprFound = true
        case _ =>
      }
      exprFound
    } =>
      val level = RulesHelper.getMapNDLevels(fun)
      DropOrderedStream(MapOrderedStream(level, OrderedStreamToVector.asFunction(), input), fe, le)
  })

  def skipZips: Rule = Rule("moveDownMapStreamToVector_skipZips", {
    case MapOrderedStream(ArchLambda(p1, body1, _), Zip2OrderedStream(Tuple2(in1, in2, _), _), _) if {
      var exprFound = false
      body1.visit{
        case MapOrderedStream(ArchLambda(p2, body2, _), Zip2OrderedStream(ParamUse(p1u), _), _) if p1u.id == p1.id =>
          body2.visit{
            case MapOrderedStream(ArchLambda(p3, body3, _), Zip2OrderedStream(ParamUse(p2u), _), _) if p2u.id == p2.id =>
              body3.visit{
                case MapOrderedStream(ArchLambda(p4, body4, _), Zip2OrderedStream(ParamUse(p3u), _), _) if p3u.id == p3.id =>
                  body4.visit{
                    case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p4u), _), _) if p4u.id == p4.id =>
                      exprFound = true
                    case _ =>
                  }
                case _ =>
              }
            case _ =>
          }
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(Tuple2(MapOrderedStream(4, OrderedStreamToVector.asFunction(), in1), MapOrderedStream(4, OrderedStreamToVector.asFunction(), in2))))
      MapOrderedStream(
        {
          val newParam1 = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            newParam1,
            body1.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p2, body2, _), Zip2OrderedStream(ParamUse(p1u), _), _) if p1u.id == p1.id =>
                //===
                val newInput1 = TypeChecker.check(Zip2OrderedStream(ParamUse(newParam1)))
                MapOrderedStream(
                  {
                    val newParam2 = ParamDef(newInput1.t.asInstanceOf[OrderedStreamTypeT].et)
                    ArchLambda(
                      newParam2,
                      body2.visitAndRebuild{
                        case MapOrderedStream(ArchLambda(p3, body3, _), Zip2OrderedStream(ParamUse(p2u), _), _) if p2u.id == p2.id =>
                          //===
                          val newInput2 = TypeChecker.check(Zip2OrderedStream(ParamUse(newParam2)))
                          MapOrderedStream(
                            {
                              val newParam3 = ParamDef(newInput2.t.asInstanceOf[OrderedStreamTypeT].et)
                              ArchLambda(
                                newParam3,
                                body3.visitAndRebuild{
                                  case MapOrderedStream(ArchLambda(p4, body4, _), Zip2OrderedStream(ParamUse(p3u), _), _) if p3u.id == p3.id =>
                                    //===
                                    val newInput3 = TypeChecker.check(Zip2OrderedStream(ParamUse(newParam3)))
                                    MapOrderedStream(
                                      {
                                        val newParam4 = ParamDef(newInput3.t.asInstanceOf[OrderedStreamTypeT].et)
                                        ArchLambda(
                                          newParam4,
                                          body4.visitAndRebuild{
                                            case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p4u), _), _) if p4u.id == p4.id =>
                                              ZipVector(ParamUse(newParam4))
                                            case e => e
                                          }.asInstanceOf[Expr]
                                        )
                                      }, newInput3
                                    )
                                  //===
                                  case e => e
                                }.asInstanceOf[Expr]
                              )
                            }, newInput2
                          )
                        //===
                        case e => e
                      }.asInstanceOf[Expr]
                    )
                  }, newInput1
                )
              //===
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )
    case MapOrderedStream(ArchLambda(p1, body1, _), Zip2OrderedStream(Tuple2(in1, in2, _), _), _) if {
      var exprFound = false
      body1.visit{
        case MapOrderedStream(ArchLambda(p2, body2, _), Zip2OrderedStream(ParamUse(p1u), _), _) if p1u.id == p1.id =>
          body2.visit{
            case MapOrderedStream(ArchLambda(p3, body3, _), Zip2OrderedStream(ParamUse(p2u), _), _) if p2u.id == p2.id =>
              body3.visit{
                case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p3u), _), _) if p3u.id == p3.id =>
                  exprFound = true
                case _ =>
              }
            case _ =>
          }
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(Tuple2(MapOrderedStream(3, OrderedStreamToVector.asFunction(), in1), MapOrderedStream(3, OrderedStreamToVector.asFunction(), in2))))
      MapOrderedStream(
        {
          val newParam1 = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            newParam1,
            body1.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p2, body2, _), Zip2OrderedStream(ParamUse(p1u), _), _) if p1u.id == p1.id =>
                //===
                val newInput1 = TypeChecker.check(Zip2OrderedStream(ParamUse(newParam1)))
                MapOrderedStream(
                  {
                    val newParam2 = ParamDef(newInput1.t.asInstanceOf[OrderedStreamTypeT].et)
                    ArchLambda(
                      newParam2,
                      body2.visitAndRebuild{
                        case MapOrderedStream(ArchLambda(p3, body3, _), Zip2OrderedStream(ParamUse(p2u), _), _) if p2u.id == p2.id =>
                          //===
                          val newInput2 = TypeChecker.check(Zip2OrderedStream(ParamUse(newParam2)))
                          MapOrderedStream(
                            {
                              val newParam3 = ParamDef(newInput2.t.asInstanceOf[OrderedStreamTypeT].et)
                              ArchLambda(
                                newParam3,
                                body3.visitAndRebuild{
                                  case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p3u), _), _) if p3u.id == p3.id =>
                                    ZipVector(ParamUse(newParam3))
                                  case e => e
                                }.asInstanceOf[Expr]
                              )
                            }, newInput2
                          )
                        //===
                        case e => e
                      }.asInstanceOf[Expr]
                    )
                  }, newInput1
                )
              //===
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )

    case MapOrderedStream(ArchLambda(p1, body1, _), Zip2OrderedStream(Tuple2(in1, in2, _), _), _) if {
      var exprFound = false
      body1.visit{
        case MapOrderedStream(ArchLambda(p2, body2, _), Zip2OrderedStream(ParamUse(p1u), _), _) if p1u.id == p1.id =>
          body2.visit{
            case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p2u), _), _) if p2u.id == p2.id =>
              exprFound = true
            case _ =>
          }
        case _ =>
      }
      exprFound
    } =>
      val newInput = TypeChecker.check(Zip2OrderedStream(Tuple2(MapOrderedStream(2, OrderedStreamToVector.asFunction(), in1), MapOrderedStream(2, OrderedStreamToVector.asFunction(), in2))))
      MapOrderedStream(
        {
          val newParam1 = ParamDef(newInput.t.asInstanceOf[OrderedStreamTypeT].et)
          ArchLambda(
            newParam1,
            body1.visitAndRebuild{
              case MapOrderedStream(ArchLambda(p2, body2, _), Zip2OrderedStream(ParamUse(p1u), _), _) if p1u.id == p1.id =>
                //===
                val newInput1 = TypeChecker.check(Zip2OrderedStream(ParamUse(newParam1)))
                MapOrderedStream(
                  {
                    val newParam2 = ParamDef(newInput1.t.asInstanceOf[OrderedStreamTypeT].et)
                    ArchLambda(
                      newParam2,
                      body2.visitAndRebuild{
                        case OrderedStreamToVector(Zip2OrderedStream(ParamUse(p2u), _), _) if p2u.id == p2.id =>
                          ZipVector(ParamUse(newParam2))
                        //===
                        case e => e
                      }.asInstanceOf[Expr]
                    )
                  }, newInput1
                )
              //===
              case e => e
            }.asInstanceOf[Expr]
          )
        }, newInput
      )
  })
}