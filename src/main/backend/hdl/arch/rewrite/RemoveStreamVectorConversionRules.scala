package backend.hdl.arch.rewrite

import backend.hdl.VectorTypeT
import backend.hdl.arch.{ArchLambda, JoinOrderedStream, JoinVector, MapOrderedStream, OrderedStreamToVector, PermuteVector, SplitVector, Tuple2, VectorToOrderedStream, Zip2OrderedStream, ZipVector}
import core.{ArithLambdaType, ArithType, ArithTypeT, ArithTypeVar, ParamUse}
import core.rewrite.{Rule, RulesT}

object RemoveStreamVectorConversionRules extends RulesT {

  override def all(config: Option[Int]): Seq[Rule] =
    Seq(
      removeConversion1,
      removeConversion2,
      removeDeadPermute,
    ) ++ Seq(CleanupRules.removeEmptyMap, RemoveSelectRules.removeSelect)

  private def removeDeadPermute: Rule = Rule("removeDeadPermute", {
    case PermuteVector(PermuteVector(input, f1, _), f2, _) =>
      import lift.arithmetic._
      val compose = ArithExpr.substitute(f2.b.ae, Predef.Map(f2.v.ae -> f1.b.ae))
      PermuteVector(input, ArithLambdaType(f1.v, compose))

    case PermuteVector(input, f, t: VectorTypeT) if {
      import lift.arithmetic._
      val v = ArithTypeVar(ContinuousRange(0, t.len.ae))
      v === ArithExpr.substitute(f.b.ae, Predef.Map(f.v.ae -> v))
    } =>
      input
  })

  private def removeConversion1: Rule = Rule("removeConversion1", {
    case OrderedStreamToVector(VectorToOrderedStream(input, _), _) => input
    case VectorToOrderedStream(OrderedStreamToVector(input, _), _) => input
  })

  private def removeConversion2: Rule = Rule("removeConversion2", {
    case OrderedStreamToVector(JoinOrderedStream(MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), input, _), _), _)
      if p1.id == p2.id =>
      JoinVector(OrderedStreamToVector(input))
  })

  // TODO make private
  def removeConversion3: Rule = Rule("removeConversion3", {
    case MapOrderedStream(ArchLambda(p1, VectorToOrderedStream(ParamUse(p2), _), _), MapOrderedStream(ArchLambda(p3, OrderedStreamToVector(ParamUse(p4), _), _), input, _), _) if
      p1.id == p2.id && p3.id == p4.id => input
  })

  def removeConversion4: Rule = Rule("removeConversion4", {
    case MapOrderedStream(ArchLambda(p1, MapOrderedStream(ArchLambda(p2, OrderedStreamToVector(ParamUse(p3), _), _), ParamUse(p4), _), _),
    MapOrderedStream(ArchLambda(p5, MapOrderedStream(ArchLambda(p6, VectorToOrderedStream(ParamUse(p7), _), _), ParamUse(p8), _), _), input, _), _) if
      p1.id == p4.id && p2.id == p3.id && p5.id == p8.id && p6.id == p7.id  => input
  })

  // TODO make private and move to CleanupRules
  def removeStm2VecSize1: Rule = Rule("removeStm2VecSize1", {
    case MapOrderedStream(ArchLambda(p1, OrderedStreamToVector(ParamUse(p2), t: VectorTypeT), _), input, _) if
      p1.id == p2.id && t.len.ae.evalInt == 1 && t.et.isInstanceOf[VectorTypeT] =>
      MapOrderedStream(SplitVector.asFunction(Seq(None), Seq(t.et.asInstanceOf[VectorTypeT].len)), JoinOrderedStream(input))
  })
}
