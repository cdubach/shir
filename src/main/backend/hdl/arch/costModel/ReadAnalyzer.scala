package backend.hdl.arch.costModel
import backend.hdl.arch.MapCompilerHelper.isRepeatReduce
import backend.hdl.arch.{ArchHelper, MapAsyncOrderedStream, MapOrderedStream}
import backend.hdl.arch.costModel.AddressInterpreter.FunParamMap
import backend.hdl.arch.mem.{ReadAsyncOrdered, ReadAsyncOrderedOutOfBounds}
import core.{Expr, FunctionCall, LambdaT, Let, ParamDef, ParamUse}

object ReadAnalyzer {

  /**
   * Analyze if two read requests conflict with each other. Some primitives like Tuple in CostModel can overlap runtime
   * from its children. However, if there is a conflict about Read, overlapping cannot happen.
   */

  private var conflictSites: Seq[Expr] = Seq()
  private var ParamMap: Map[ParamDef, Boolean] = Map()
  private var FunMap: Map[ParamDef, Expr] = Map()

  def getAllConflicts(expr: Expr): Seq[Expr] = {
    conflictSites = Seq()
    ParamMap = Map()
    FunMap = Map()
    getConflicts(expr)
    conflictSites
  }

  private def getConflicts(expr: Expr): Boolean = expr match {
    case fCall @ FunctionCall(_, _, _) if ArchHelper.isSharedFunCall(fCall) =>
      val funSeq = ArchHelper.getFunCall(fCall)
      val fp = funSeq.head.asInstanceOf[ParamUse].p
      val inputs = funSeq.tail
      val fun = FunMap(fp)
      val funInputParams = ArchHelper.getLambdaParams(fun)
      val newInputMap = funInputParams.zip(inputs).map(e => (e._1, getConflicts(e._2)))
      if(newInputMap.map(_._2).foldLeft(true)(_&&_)) {
        conflictSites = conflictSites :+ fCall
        false
      } else {
        newInputMap.foreach(e => ParamMap = ParamMap + (e._1 -> e._2))
        val funBody = ArchHelper.getLambdaBody(fun)
        val res = getConflicts(funBody)
        newInputMap.foreach(e => ParamMap = ParamMap.-(e._1))
        res
      }
    case Let(p, body, arg: LambdaT, _) =>
      FunMap = FunMap + (p -> arg)
      getConflicts(body)
    // Hack the case that Read only happen once.
    case Let(p, body, arg, _) if isRepeatReduce(arg) =>
      getConflicts(body)
    case Let(p, body, arg, _) =>
      ParamMap = ParamMap + (p -> getConflicts(arg))
      val res = getConflicts(body)
      ParamMap = ParamMap.-(p)
      res
    case MapAsyncOrderedStream(f: LambdaT, inputs, _) =>
      val fParamList = ArchHelper.getLambdaParams(f)
      val fBody = ArchHelper.getLambdaBody(f)
      fParamList.zip(inputs).foreach(e => ParamMap = ParamMap + (e._1 -> getConflicts(e._2)))
      val res = getConflicts(fBody)
      fParamList.foreach(e => ParamMap.-(e))
      res
    case MapOrderedStream(f: LambdaT, input, _) =>
      ParamMap = ParamMap + (f.param -> getConflicts(input))
      val res = getConflicts(f.body)
      ParamMap = ParamMap.-(f.param)
      res
    case ReadAsyncOrdered(_, _, _, _) => true
    case ReadAsyncOrderedOutOfBounds(_, _, _, _, _, _) => true
    case ParamUse(p) if ParamMap.contains(p) => ParamMap(p)
    case e => e.children.map(c => c match {
      case ce: Expr => getConflicts(ce)
      case _ => false
    }).foldLeft(false)(_||_)
  }
}
