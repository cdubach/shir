package backend.hdl.arch.costModel

import algo.Mul
import backend.hdl.arch.ArchHelper.{getLambdaBody, getLambdaParamIds, getLambdaParams}
import backend.hdl.arch.device.Mul2AddInt
import backend.hdl.arch.mem._
import backend.hdl.arch._
import backend.hdl._
import backend.hdl.arch.costModel.ReadAnalyzer.{FunMap, ParamMap}
import core._

object CostModel {

  /**
   * Calculate required run cycles. The idea is that every is driven by counters. A counter(N) spend N cycles on
   * generating a stream. When the stream passes a primitive, the run time might be influenced. For example, when a
   * stream encounter Map(Read(_)), the time spent on read access should be considered.
   */

  private val readHostPenalty = 2.57
  private val writeHostPenalty = 3
  private val readBRAMPenalty = 0
  private val writeBRAMPenalty = 1
  private val readHostLatency = 100

  // We partition the expression based on WriteAsync.
  // Assume the expression under each write async is independent and can be summed up directly.
  private var previousWrite: Double = 0

  // Handle functions and make param memorize local runtime.
  // Note that for each function call, we have to re-visit function since each input can have different run time.
  private var funMap: Map[ParamDef, Expr] = Map()
  private var paramMap: Map[ParamDef, Double] = Map()

  // Handle ratio of valid read requests.
  private var siteReadRate: Map[Expr, Double] = Map()
  private var selectReadRate: Seq[Double] = Seq()

  // Handle read request conflicts.
  private var conflictSites: Seq[Expr] = Seq()

  def estimateRunCyclesWithFunctions(expr: Expr): Double = {
    previousWrite = 0
    funMap = Map()
    paramMap = Map()
    siteReadRate = AddressInterpreter.interpretReadOOB(expr)
    conflictSites = ReadAnalyzer.getAllConflicts(expr)
    selectReadRate = Seq()
    val latency = LatencyModel.estimateOverallLatency(expr)
    val remainingTimeWithoutWrite = estimateRunCycles(TypeChecker.check(expr))
    remainingTimeWithoutWrite + previousWrite + latency
  }

  private def estimateRunCycles(expr: Expr): Double = expr match {
    case Let(p, body, arg: LambdaT, _) =>
      funMap = funMap + (p -> arg)
      val res = estimateRunCycles(body)
      funMap = funMap.-(p)
      res
    case Let(p, body, arg, _) =>
      paramMap = paramMap + (p -> estimateRunCycles(arg))
      val res = estimateRunCycles(body)
      paramMap = paramMap.-(p)
      res
    // FunctionCall workaround for conflicting
    case fCall @ FunctionCall(_, _, _) if ArchHelper.isSharedFunCall(fCall) && conflictSites.contains(fCall) =>
      val funSeq = ArchHelper.getFunCall(fCall)
      val fp = funSeq.head.asInstanceOf[ParamUse].p
      val inputs = funSeq.tail
      val fun = funMap(fp)
      val funInputParams = ArchHelper.getLambdaParams(fun)
      val newInputMap = funInputParams.zip(inputs).map(e => (e._1, estimateRunCycles(e._2)))
      newInputMap.foreach(e => paramMap = paramMap + (e._1 -> e._2))
      val funBody = ArchHelper.getLambdaBody(fun)
      val res = estimateRunCycles(funBody)
      newInputMap.foreach(e => paramMap = paramMap.-(e._1))
      val newInputMapMin = newInputMap.head._2
      res + newInputMapMin
    // Workaround for alternate read. We need to know the total number of read so that the latency can be added.
    case MapOrderedStream(ArchLambda(p1, Alternate(ArchLambda(_, ReadAsyncOrderedOutOfBounds(_, _, _, _, _, _), _), ArchLambda(_, ReadAsyncOrderedOutOfBounds(_, _, _, _, _, _), _), ParamUse(p2), _), _), input, t: OrderedStreamTypeT) if p1.id == p2.id =>
      val readHostCycles = (t.dimensions.map(_.ae.evalDouble).foldLeft(1.0)(_*_) * readHostPenalty) * selectReadRate.last + readHostLatency
      selectReadRate = selectReadRate.init
      val inputTime = estimateRunCycles(input)
      readHostCycles max inputTime
    case MapOrderedStream(ArchLambda(p1, Alternate(ArchLambda(_, ReadAsyncOrdered(_, _, _, _), _), ArchLambda(_, ReadAsyncOrdered(_, _, _, _), _), ParamUse(p2), _), _), input, t: OrderedStreamTypeT) if p1.id == p2.id =>
      val readHostCycles = (t.dimensions.map(_.ae.evalDouble).foldLeft(1.0)(_*_) * readHostPenalty) + readHostLatency
      val inputTime = estimateRunCycles(input)
      readHostCycles max inputTime
    case e @ ConstrainedSum(input, _, _, _, _, _, _, _) if siteReadRate.contains(e)=>
      selectReadRate = selectReadRate :+ siteReadRate(e)
      val inputTime = estimateRunCycles(input)
      inputTime //* newRate
    // FunctionCall
    case fCall @ FunctionCall(_, _, _) if ArchHelper.isSharedFunCall(fCall) =>
      val funSeq = ArchHelper.getFunCall(fCall)
      val fp = funSeq.head.asInstanceOf[ParamUse].p
      val inputs = funSeq.tail
      val fun = funMap(fp)
      val funInputParams = ArchHelper.getLambdaParams(fun)
      val newInputMap = funInputParams.zip(inputs).map(e => (e._1, estimateRunCycles(e._2)))
      newInputMap.foreach(e => paramMap = paramMap + (e._1 -> e._2))
      val funBody = ArchHelper.getLambdaBody(fun)
      val res = estimateRunCycles(funBody)
      newInputMap.foreach(e => paramMap = paramMap.-(e._1))
      res
    // Map-Reduce
    case MapAsyncOrderedStream(f: LambdaT, inputs, t: OrderedStreamTypeT) =>
      val fParamList = ArchHelper.getLambdaParams(f)
      val fBody = ArchHelper.getLambdaBody(f)
      val inputLen = t.len.ae.evalDouble
      val inputsTime = inputs.map(e => (estimateRunCycles(e) / inputLen))
      fParamList.zip(inputsTime).foreach(e => paramMap = paramMap + (e._1 -> e._2))
      val fTime = estimateRunCycles(fBody)
      fParamList.foreach(e => paramMap = paramMap.-(e))
      val res = fTime * inputLen
      res
    case MapOrderedStream(f: LambdaT, input, t: OrderedStreamTypeT) =>
      val inputTime = estimateRunCycles(input)
      val inputLen = t.len.ae.evalDouble
      paramMap = paramMap + (f.param -> (inputTime / inputLen))
      val fTime = estimateRunCycles(f.body)
      fTime * inputLen
    case FoldOrderedStream(f: LambdaT, input, _) =>
      val inputTime = estimateRunCycles(input)
      val inputLen = input.t.asInstanceOf[UpperBoundedStreamTypeT].len.ae.evalDouble
      paramMap = paramMap + (f.param -> (inputTime / inputLen))
      val fTime = estimateRunCycles(f.body)
      fTime * inputLen
    case ReduceOrderedStream(f: LambdaT, initVal, input, _) =>
      val fParamList = ArchHelper.getLambdaParams(f)
      val fBody = ArchHelper.getLambdaBody(f)
      val inputLen = input.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalDouble
      val inputsTime = Seq(estimateRunCycles(initVal), (estimateRunCycles(input)/ inputLen))
      fParamList.zip(inputsTime).foreach(e => paramMap = paramMap + (e._1 -> e._2))
      val fTime = estimateRunCycles(fBody)
      fParamList.foreach(e => paramMap = paramMap.-(e))
      fTime * inputLen
    // Memory Access Primitives
    case ReadAsyncOrderedOutOfBounds(_, baseAddr, input, _, _, t: OrderedStreamTypeT) =>
      val readHostCycles = (t.len.ae.evalDouble * (readHostPenalty)) * selectReadRate.last
      selectReadRate = selectReadRate.init
      val maxInput = estimateRunCycles(baseAddr) max estimateRunCycles(input)
      readHostCycles max maxInput
    case ReadAsyncOrdered(_, baseAddr, input, t: OrderedStreamTypeT) =>
      val readHostCycles = (t.len.ae.evalDouble * readHostPenalty)
      val maxInput = estimateRunCycles(baseAddr) max estimateRunCycles(input)
      readHostCycles max maxInput
    case ReadSync(_, baseAddr, input, t: BasicDataTypeT) =>
      readBRAMPenalty + estimateRunCycles(baseAddr) max estimateRunCycles(input)
    case WriteAsync(_, _, input, _) =>
      /**
       * We assume Write and anything before it form a pipeline. The time should depend on the slower one.
       */
      val writeHostCycles = (input.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalDouble * writeHostPenalty).toDouble
      val maxTimeBeforeWrite = estimateRunCycles(input) max writeHostCycles
      previousWrite = previousWrite + maxTimeBeforeWrite
      0
    case WriteAsyncOutOfBounds(_, _, input, _, _) =>
      /**
       * We assume Write and anything before it form a pipeline. The time should depend on the slower one.
       */
      val writeHostCycles = (input.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalDouble * writeHostPenalty).toDouble
      val maxTimeBeforeWrite = estimateRunCycles(input) max writeHostCycles
      previousWrite = previousWrite + maxTimeBeforeWrite
      0
    case ReduceOrderedStream(ArchLambda(p1, ArchLambda(p2, WriteSync(_, _, _, _), _), _), initVal, input, _) =>
      val writePenalty = writeBRAMPenalty *
        input.t.asInstanceOf[OrderedStreamTypeT].dimensions.map(_.ae.evalDouble).foldLeft(1.0)(_ * _)
      val inputTime = estimateRunCycles(initVal) max estimateRunCycles(input)
      writePenalty max inputTime
    // Alternate
    case Alternate(f1: LambdaT, f2: LambdaT, input, t) =>
      /**
       * Alternate makes calculation more complex. In general, we want the time for function overlaps anything after
       * the Alternate primitive. Thus, we store the time for max(f1, f2) for later use.
       * Here we discard the initialization time. Assume the output can be overlapped with the input.
       */
      val inputTime = estimateRunCycles(input)
      paramMap = paramMap + (f1.param -> inputTime)+ (f2.param -> inputTime)
      val f1Time = estimateRunCycles(f1.body)
      val f2Time = estimateRunCycles(f2.body)
      paramMap = paramMap.-(f1.param)
      paramMap = paramMap.-(f2.param)
      f1Time max f2Time
    // Leaf Primitives
    case CounterInteger(_, _, _, _, _, t: OrderedStreamTypeT) => t.dimensions.map(_.ae.evalDouble).foldLeft(1.0)(_*_)
    case ParamUse(p) if paramMap.contains(p) => paramMap(p)
    case ParamUse(p) if p.t.isInstanceOf[OrderedStreamTypeT] => p.t.asInstanceOf[OrderedStreamTypeT].dimensions.map(_.ae.evalDouble).foldLeft(1.0)(_*_)
    case ParamUse(_) => 0.0
    // Other Primitives
    case Repeat(input, _, t: OrderedStreamTypeT) if t.leafType.isInstanceOf[BaseAddrTypeT] => estimateRunCycles(input)
    case Repeat(input, _, t: OrderedStreamTypeT) if t.et.isInstanceOf[BasicDataTypeT] =>
      estimateRunCycles(input) max t.len.ae.evalDouble
    case Repeat(input, rep, _) => estimateRunCycles(input) * rep.ae.evalDouble
    case Tuple(inputs, _) => inputs.map(i => estimateRunCycles(i)).foldLeft(0.0)(_ max _)
    case VectorToOrderedStream(input, t: OrderedStreamTypeT) =>
      val inputTime = estimateRunCycles(input)
      val genLength = t.len.ae.evalDouble
      // If the input time is too long, the time spent on pushing data out can be hidden.
      if(inputTime > genLength)
        inputTime
      else
        inputTime + genLength
    // Default
    case e => e.children.map(c => c match {
      case ce: Expr => estimateRunCycles(ce)
      case _ => 0
    }).foldLeft(0.0)(_+_)
  }

  /** Doubleel Arria 10 bit width constraDoubles for small Doubleeger multipliers.*/
  def arria10MulBitWidthCheck(bitWidth1: Double, bitWidth2: Double): Boolean =
    (bitWidth1 <= 18 && bitWidth2 <= 19) || (bitWidth1 <= 19 && bitWidth2 <= 18)

  /**
   * Calculate the number of DSP blocks.
   * Note that the unit here is DSP block, not DSP. A DSP supports 2 blocks.
   */
  def estimateDSPBlocks(expr: Expr): Double = expr match {
    case Mul(i, _) =>
      val numOfDSPs =
        i.t match {
          case algo.TupleType(Seq(sit1: algo.SignedIntTypeT, sit2: algo.SignedIntTypeT)) if
            arria10MulBitWidthCheck(sit1.width.ae.evalDouble, sit2.width.ae.evalDouble) => 1
          case algo.TupleType(Seq(it1: algo.IntTypeT, it2: algo.IntTypeT)) if
            arria10MulBitWidthCheck(it1.width.ae.evalDouble, it2.width.ae.evalDouble) => 1
          case algo.TupleType(Seq(_: algo.FloatTypeT, _: algo.FloatTypeT)) => 2
          case _ => 2
        }
      numOfDSPs + estimateDSPBlocks(i)
    case MulInt(i, _) => val numOfDSPs =
      i.t match {
        case TupleType(Seq(sit1: SignedIntTypeT, sit2: SignedIntTypeT)) if
          arria10MulBitWidthCheck(sit1.bitWidth.ae.evalDouble, sit2.bitWidth.ae.evalDouble) => 1
        case TupleType(Seq(it1: IntTypeT, it2: IntTypeT)) if
          arria10MulBitWidthCheck(it1.bitWidth.ae.evalDouble, it2.bitWidth.ae.evalDouble) => 1
        case _ => 2
      }
      numOfDSPs + estimateDSPBlocks(i)
    case MulFloat(i, _) => 2 + estimateDSPBlocks(i)
    case Mul2AddInt(i, _) => 2 + estimateDSPBlocks(i)
    case MapVector(f, i, t: VectorTypeT) => t.len.ae.evalDouble * estimateDSPBlocks(f) + estimateDSPBlocks(i)
    case e => e.children.map(c => c match {
      case ce: Expr => estimateDSPBlocks(ce)
      case _ => 0
    }).foldLeft(0.0)(_+_)
  }

  /**
   * Calculate the number of wires to a function.
   * Note that MapVec(x -> FunCall(x), ...) is treated as an invalid case and is not considered.
   * Only support Arch level since Algo level lacks of bit width information of SeqTypeT.
   */
  def estimateInputWiring(expr: Expr): Map[ParamDef, Double] = {
    var res: Map[ParamDef, Double] = Map()
    expr.visit {
      case Let(p, body, arg@ArchLambda(_), t) =>
        val funParams = getLambdaParams(arg)
        var numberOfCalls = 0
        // Get input bit width
        val portBitWidth = funParams.map(p => p.t match {
          case t: UnorderedStreamTypeT => t.leafType.bitWidth.ae.evalDouble // TopType for stream-based data is UnorderedStreamTypeT
          case t: BasicDataTypeT => t.bitWidth.ae.evalDouble
          case _ => ???
        }).foldLeft(0.0)(_+_)
        // Get number of calls
        body.visit{
          case ParamUse(pd) if pd.id == p.id => numberOfCalls = numberOfCalls + 1
          case _ =>
        }
        res = res + (p -> portBitWidth * numberOfCalls)
      case _ =>
    }
    res
  }

  def estimateOutputWiring(expr: Expr): Map[ParamDef, Double] = {
    var res: Map[ParamDef, Double] = Map()
    expr.visit {
      case Let(p, body, arg@ArchLambda(_), t) =>
        val funBody = getLambdaBody(arg)
        var numberOfCalls = 0
        // Get input bit width
        val portBitWidth = funBody.t match {
          case t: UnorderedStreamTypeT => t.leafType.bitWidth.ae.evalDouble // TopType for stream-based data is UnorderedStreamTypeT
          case t: BasicDataTypeT => t.bitWidth.ae.evalDouble
          case _ => ???
        }
        // Get number of calls
        body.visit {
          case ParamUse(pd) if pd.id == p.id => numberOfCalls = numberOfCalls + 1
          case _ =>
        }
        res = res + (p -> portBitWidth * numberOfCalls)
      case _ =>
    }
    res
  }

  /**
   * Calculate the number of data read (in bit), with the consideration of repeating read.
   * Only consider read access for now since NN usually requires a lots of memory reads.
   *
   * @param expr input expression.
   * @param mapRead number of read accesses should be handled by Map-Repeat.
   * @param repeatOptimization Consider optimization related to Repeat.
   * @return a tuple of repeatable read, non-repeatable read, and map-read
   */
  def estimateHostReadAccess(expr: Expr, mapRead: Map[Long, Double] = Map(), repeatOptimization: Boolean = false): (Double, Double) = expr match {
    // Handle Reads
    case Read(mem, addr, t: VectorTypeT) =>
      var numOfReads: Double = 0
      mem.t match {
        case rt: RamArrayTypeT if rt.memoryLocation.isInstanceOf[HostRamTypeT] => numOfReads = t.bitWidth.ae.evalDouble
        case _ =>
      }
      val prevNumOfReads = estimateHostReadAccess(addr, mapRead, repeatOptimization)
      (prevNumOfReads._1 + numOfReads, prevNumOfReads._2)
    case ReadOutOfBounds(mem, addr, _, _, t: VectorTypeT) =>
      var numOfReads: Double = 0
      mem.t match {
        case rt: RamArrayTypeT if rt.memoryLocation.isInstanceOf[HostRamTypeT] => numOfReads = t.bitWidth.ae.evalDouble
        case _ =>
      }
      val prevNumOfReads = estimateHostReadAccess(addr, mapRead, repeatOptimization)
      (prevNumOfReads._1 + numOfReads, prevNumOfReads._2)
    case ReadAsyncOrdered(_, _, addr, t: OrderedStreamTypeT) =>
      val numOfReads = t.et.bitWidth.ae.evalDouble * t.len.ae.evalDouble
      val prevNumOfReads = estimateHostReadAccess(addr, mapRead, repeatOptimization)
      (prevNumOfReads._1 + numOfReads, prevNumOfReads._2)
    case ReadAsyncOrderedOutOfBounds(_, _, addr, _, _, t: OrderedStreamTypeT) =>
      val numOfReads = t.et.bitWidth.ae.evalDouble * t.len.ae.evalDouble
      val prevNumOfReads = estimateHostReadAccess(addr, mapRead, repeatOptimization)
      (prevNumOfReads._1 + numOfReads, prevNumOfReads._2)

    // Handle Writes
    // Note that if there is a buffer, a pair of read and write will be Doubleroduced.
    // Repeat Optimization will move Repeats down to the address of the buffer's Read primitive.
    case WriteAsync(_, _, input, _) if repeatOptimization =>
      val prevNumOfReads = estimateHostReadAccess(input, mapRead, repeatOptimization)
      (0, prevNumOfReads._1 + prevNumOfReads._2)
    case Write(_, input, _) if repeatOptimization  =>
      val prevNumOfReads = estimateHostReadAccess(input, mapRead, repeatOptimization)
      (0, prevNumOfReads._1 + prevNumOfReads._2)
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, Write(_, _, _), _), _), _, input, _) if repeatOptimization  =>
      val prevNumOfReads = estimateHostReadAccess(input, mapRead, repeatOptimization)
      (0, prevNumOfReads._1 + prevNumOfReads._2)
    case ReduceOrderedStream(ArchLambda(_, ArchLambda(_, ReduceOrderedStream(ArchLambda(_, ArchLambda(_,
    Write(_, _, _), _), _), _, _, _), _), _), _, input, _) if repeatOptimization =>
      val prevNumOfReads = estimateHostReadAccess(input, mapRead, repeatOptimization)
      (0, prevNumOfReads._1 + prevNumOfReads._2)

    // Handle Maps
    case MapVector(f, i, t: VectorTypeT) =>
      val funNumOfReads = estimateHostReadAccess(f, mapRead, repeatOptimization)
      val prevNumOfReads = estimateHostReadAccess(i, mapRead, repeatOptimization)
      (prevNumOfReads._1 + t.len.ae.evalDouble * funNumOfReads._1, prevNumOfReads._2 + t.len.ae.evalDouble * funNumOfReads._2)
    case MapOrderedStream(f: LambdaT, i, t: OrderedStreamTypeT) =>
      // Estimate Repeated Reads
      val prevNumOfReads = estimateHostReadAccess(i, mapRead, repeatOptimization)
      val innerRead = prevNumOfReads._1 / t.len.ae.evalDouble
      val newMapRead = Map(f.param.id -> innerRead) ++ mapRead
      // Estimate Reads happening inside Lambda
      val funNumOfReads = estimateHostReadAccess(f, newMapRead, repeatOptimization)
      (t.len.ae.evalDouble * (funNumOfReads._1 + funNumOfReads._2), prevNumOfReads._2)
    case MapAsyncOrderedStream(f: LambdaT, ins, t: OrderedStreamTypeT) =>
      // Estimate Repeated Reads
      val prevNumsOfReads = ins.map(i => estimateHostReadAccess(i, mapRead, repeatOptimization))
      val prevNumOfReads = prevNumsOfReads.foldLeft((0.0, 0.0))((p, r) => (p._1 + r._1, p._2 + r._2))
      val params = getLambdaParamIds(f.asInstanceOf[Expr])
      val innerReads = prevNumsOfReads.map(p => p._1 / t.len.ae.evalDouble)
      val newMapRead = params.zip(innerReads).toMap ++ mapRead
      // Estimate Reads happening inside Lambda
      val funNumOfReads = estimateHostReadAccess(f, newMapRead, repeatOptimization)
      (t.len.ae.evalDouble * (funNumOfReads._1 + funNumOfReads._2) , prevNumOfReads._2)

    // Handle Repeat and the rest
    case Repeat(i, times, _) =>
      val prevNumOfReads = estimateHostReadAccess(i, mapRead, repeatOptimization)
      (prevNumOfReads._1 * times.ae.evalDouble, prevNumOfReads._2)
    case ParamUse(pd) =>
      (mapRead.getOrElse(pd.id, 0.0), 0.0)
    case e => e.children.map(c => c match {
      case ce: Expr => estimateHostReadAccess(ce, mapRead, repeatOptimization)
      case _ => (0.0, 0.0)
    }).foldLeft((0.0, 0.0))((p, r) => (p._1 + r._1, p._2 + r._2))
  }

  def estimateHostReadCacheLines(expr: Expr, repeatOptimization: Boolean = false): Double = {
    val reads = estimateHostReadAccess(expr, repeatOptimization = repeatOptimization)
    (reads._1 + reads._2) / 512
  }
}
