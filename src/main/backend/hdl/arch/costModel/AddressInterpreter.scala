package backend.hdl.arch.costModel

import backend.hdl.arch.{Alternate, ArchHelper, ArchLambda, ConstrainedSum, FoldOrderedStream, MapAsyncOrderedStream, MapOrderedStream, ReduceOrderedStream}
import backend.hdl.arch.mem.ReadAsyncOrderedOutOfBounds
import core.{ArithTypeT, Expr, FunctionCall, LambdaT, Let, ParamDef, ParamUse}

object AddressInterpreter {

  /**
   * This is a workaround interpreter that tries to catch how many "valid" read requests are sent from the smart memory
   * controller (ReadAsyncOutOfBounds). Right now our use cases only have "ConstrainedSum" primitives that helps to find
   * out if an address is valid. This interpret only aims to find out such primitives that are linked to Read controller
   * and then calculate the read rate.
   *
   * TODO: Make it general by passing variables with range to find out the equation.
   */

  private var siteReadRate: Map[Expr, Double] = Map()
  private var ParamMap: Map[ParamDef, Boolean] = Map()
  private var FunParamMap: Map[ParamDef, Seq[Boolean]] = Map()

  def interpretReadOOB(expr: Expr): Map[Expr, Double] = {
    ParamMap = Map()
    FunParamMap = Map()
    siteReadRate = Map()
    getReadOOBPath(expr)
    siteReadRate
  }

  private def getReadOOBPath(expr: Expr, searchFlag: Boolean = false): Unit = expr match {
    case e @ ConstrainedSum(input, lowerBoundW: ArithTypeT, upperBoundW: ArithTypeT, lowerBoundH: ArithTypeT, upperBoundH: ArithTypeT, maxW: ArithTypeT, maxH: ArithTypeT, _) =>
      if(searchFlag) {
        val wRange = (upperBoundW.ae.evalDouble - lowerBoundW.ae.evalDouble) / maxW.ae.evalDouble
        val HRange = (upperBoundH.ae.evalDouble - lowerBoundH.ae.evalDouble) / maxH.ae.evalDouble
        siteReadRate = siteReadRate + (e -> wRange * HRange)
      }
    case fCall @ FunctionCall(_, _, _) if ArchHelper.isSharedFunCall(fCall) =>
      val funSeq = ArchHelper.getFunCall(fCall)
      val fp = funSeq.head.asInstanceOf[ParamUse].p
      val inputs = funSeq.tail
      val passOrder = FunParamMap.getOrElse(fp, inputs.map(_ => false))
      inputs.zip(passOrder).map(e => getReadOOBPath(e._1, e._2))
    case Let(fp, body, arg: LambdaT, _) =>
      val fParamList = ArchHelper.getLambdaParams(arg)
      val fBody = ArchHelper.getLambdaBody(arg)
      fParamList.foreach(p => ParamMap = ParamMap + (p -> false))
      getReadOOBPath(fBody, searchFlag)
      FunParamMap = FunParamMap + (fp -> fParamList.map(p => ParamMap.getOrElse(p, false)))
      fParamList.foreach(p => ParamMap = ParamMap.-(p))
      getReadOOBPath(body, searchFlag)
    case Let(p, body, arg, _) =>
      ParamMap = ParamMap + (p -> false)
      getReadOOBPath(body, searchFlag)
      val foundFlag = ParamMap.getOrElse(p, false)
      ParamMap = ParamMap.-(p)
      getReadOOBPath(arg, searchFlag || foundFlag)
    case MapAsyncOrderedStream(f: LambdaT, inputs, _) =>
      val fParamList = ArchHelper.getLambdaParams(f)
      val fBody = ArchHelper.getLambdaBody(f)
      fParamList.foreach(p => ParamMap = ParamMap + (p -> false))
      getReadOOBPath(fBody, searchFlag)
      val foundFlags = fParamList.map(p => ParamMap.getOrElse(p, false))
      fParamList.foreach(p => ParamMap = ParamMap.-(p))
      inputs.zip(foundFlags).map(e => getReadOOBPath(e._1, searchFlag || e._2))
    case FoldOrderedStream(f: LambdaT, input, _) =>
      ParamMap = ParamMap + (f.param -> false)
      getReadOOBPath(f.body, searchFlag)
      val foundFlag = ParamMap.getOrElse(f.param, false)
      ParamMap = ParamMap.-(f.param)
      getReadOOBPath(input, searchFlag || foundFlag)
    case ReduceOrderedStream(f: LambdaT, initVal, input, _) =>
      ParamMap = ParamMap + (f.param -> false)
      getReadOOBPath(f.body, searchFlag)
      val foundFlag = ParamMap.getOrElse(f.param, false)
      ParamMap = ParamMap.-(f.param)
      getReadOOBPath(initVal, searchFlag || foundFlag)
      getReadOOBPath(input, searchFlag || foundFlag)
    case MapOrderedStream(f: LambdaT, input, _) =>
      ParamMap = ParamMap + (f.param -> false)
      getReadOOBPath(f.body, searchFlag)
      val foundFlag = ParamMap.getOrElse(f.param, false)
      ParamMap = ParamMap.-(f.param)
      getReadOOBPath(input, searchFlag || foundFlag)
    case Alternate(f1: LambdaT, f2: LambdaT, input, _) =>
      ParamMap = ParamMap + (f1.param -> false)
      getReadOOBPath(f1.body, searchFlag)
      val foundFlag1 = ParamMap.getOrElse(f1.param, false)
      ParamMap = ParamMap.-(f1.param)
      ParamMap = ParamMap + (f2.param -> false)
      getReadOOBPath(f2.body, searchFlag)
      val foundFlag2 = ParamMap.getOrElse(f2.param, false)
      ParamMap = ParamMap.-(f2.param)
      getReadOOBPath(input, searchFlag || foundFlag1 || foundFlag2)
    case ReadAsyncOrderedOutOfBounds(_, _, input, _, _, _) => getReadOOBPath(input, true)
    case ParamUse(p) if ParamMap.contains(p) && searchFlag => ParamMap = ParamMap + (p -> true)
    case e => e.children.map(c => c match {
      case ce: Expr => getReadOOBPath(ce, searchFlag)
      case _ =>
    })
  }
}
