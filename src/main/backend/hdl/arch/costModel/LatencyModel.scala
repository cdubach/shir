package backend.hdl.arch.costModel

import backend.hdl.{BasicDataTypeT, OrderedStreamTypeT}
import backend.hdl.arch.{Alternate, FoldOrderedStream, OrderedStreamToVector, ReduceOrderedStream, Registered, Tuple}
import backend.hdl.arch.device.Mul2AddInt
import backend.hdl.arch.mem.{ReadAsyncOrdered, ReadAsyncOrderedOutOfBounds, ReadSync, WriteAsync, WriteAsyncOutOfBounds, WriteSync}
import core.{Expr, LambdaT, Let, ParamDef, ParamUse}

object LatencyModel {

  /**
   * Measure the latency of first output.
   */

  private val readHostLatency = 100
  private val writeHostLatency = 10
  private val readBRAMLatency = 3
  private val writeBRAMLatency = 3
  private var funLatencyMap: Map[ParamDef, Double] = Map()

  def estimateOverallLatency(expr: Expr): Double = {
    estimateLatencyCycles(expr)
  }

  private def estimateLatencyCycles(expr: Expr): Double = expr match {
    case Let(p, body, arg: LambdaT, _) =>
      funLatencyMap = funLatencyMap + (p -> estimateLatencyCycles(arg))
      val res = estimateLatencyCycles(body)
      funLatencyMap = funLatencyMap.-(p)
      res
    case Let(_, body, arg, _) =>
      estimateLatencyCycles(body) + estimateLatencyCycles(arg)
    case Tuple(inputs, _) => inputs.map(i => estimateLatencyCycles(i)).foldLeft(0.0)(_ max _)
    case Registered(input, _) => estimateLatencyCycles(input) + 1.0
    case ReadAsyncOrderedOutOfBounds(_, baseAddr, input, _, _, _) =>
      (estimateLatencyCycles(baseAddr) max estimateLatencyCycles(input)) + readHostLatency
    case ReadAsyncOrdered(_, baseAddr, input,  _) =>
      (estimateLatencyCycles(baseAddr) max estimateLatencyCycles(input)) + readHostLatency
    case ReadSync(_, baseAddr, input, _: BasicDataTypeT) =>
      readBRAMLatency + (estimateLatencyCycles(baseAddr) max estimateLatencyCycles(input))
    case WriteAsync(_, _, input, _) => estimateLatencyCycles(input) + writeHostLatency
    case WriteAsyncOutOfBounds(_, _, input, _, _) => estimateLatencyCycles(input) + writeHostLatency
    case WriteSync(_, _, input, _) => estimateLatencyCycles(input) + writeBRAMLatency
    case FoldOrderedStream(f, input, _: BasicDataTypeT) => estimateLatencyCycles(f) +  estimateLatencyCycles(input) + 1
    case FoldOrderedStream(f, input, _: OrderedStreamTypeT) =>
      estimateLatencyCycles(f) + estimateLatencyCycles(input) + readBRAMLatency + writeBRAMLatency
    case ReduceOrderedStream(f, initVal, input, _: BasicDataTypeT) =>
      estimateLatencyCycles(f) + (estimateLatencyCycles(initVal) max estimateLatencyCycles(input)) + 1
    case Alternate(f1, f2, input, _) =>
      (estimateLatencyCycles(f1) max estimateLatencyCycles(f2)) + estimateLatencyCycles(input)
    case Mul2AddInt(input, _) => estimateLatencyCycles(input) + 2
    case OrderedStreamToVector(input, _) =>
      estimateLatencyCycles(input) + input.t.asInstanceOf[OrderedStreamTypeT].len.ae.evalDouble
    case ParamUse(p) if funLatencyMap.contains(p) => funLatencyMap(p)
    case e => e.children.map(c => c match {
      case ce: Expr => estimateLatencyCycles(ce)
      case _ => 0
    }).foldLeft(0.0)(_+_)
  }
}
