package backend.hdl.arch.device

import backend.hdl.arch.ConcatOrderedStream
import backend.hdl.arch.mem.MemFunctionsCompiler
import backend.hdl.arch.rewrite.{ExtractMul2AddRules, MoveUpVectorToStreamRules}
import core._
import core.compile.{CompilerPass, CompilerPhase}
import core.rewrite.{RewriteAll, RewriteStep}
import core.util.IRDotGraph

object DeviceSpecificCompiler extends CompilerPass {

  override def phaseBefore: CompilerPhase = MemFunctionsCompiler.phaseAfter

  override def run(expr: Expr): Expr = {
    val expr0 = TypeChecker.checkAssert(expr)
    val expr1 = TypeChecker.check(RewriteStep(RewriteAll(), Seq(MoveUpVectorToStreamRules.outputPadding(512)) ++ ExtractMul2AddRules.get()).apply(expr0))

    var found = false
    expr1.visit({
      case ConcatOrderedStream(_) => found = true
      case _ =>
    })
    if (found)
      core.util.Log.warn(this, "found ConcatOrderedStream, this may be a performance bottleneck")
    expr1
  }
}
