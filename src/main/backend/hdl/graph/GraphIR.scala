package backend.hdl.graph

import core.{Counter, TreeNode}

import scala.collection._

final case class MalformedGraphException(private val message: String, private val cause: Throwable = None.orNull) extends Exception(message, cause)

sealed trait GraphIR extends TreeNode[GraphIR] {
  def ports: Seq[PortT]
  // final def findPort[T <: PortT]: T = ports.find(_.isInstanceOf[T]).get.asInstanceOf[T] // does not work due to type erasure
  final def portsSimple: Seq[PortSimpleT] = ports.flatMap(_.flatten)
  final private[graph] def portsInHandshake: Seq[PortInHandshake] = ports.filter(_.param.isEmpty).filter(_.isInstanceOf[PortInHandshake]).map(_.asInstanceOf[PortInHandshake])
  final private[graph] def portsOutHandshake: Seq[PortOutHandshake] = ports.filter(_.param.isEmpty).filter(_.isInstanceOf[PortOutHandshake]).map(_.asInstanceOf[PortOutHandshake])
  final private[graph] def portsInRequest: Seq[PortInRequest] = ports.filter(_.param.isEmpty).filter(_.isInstanceOf[PortInRequest]).map(_.asInstanceOf[PortInRequest])
  final private[graph] def portsOutRequest: Seq[PortOutRequest] = ports.filter(_.param.isEmpty).filter(_.isInstanceOf[PortOutRequest]).map(_.asInstanceOf[PortOutRequest])
  final private[graph] def portsInResponse: Seq[PortInResponse] = ports.filter(_.param.isEmpty).filter(_.isInstanceOf[PortInResponse]).map(_.asInstanceOf[PortInResponse])
  final private[graph] def portsOutResponse: Seq[PortOutResponse] = ports.filter(_.param.isEmpty).filter(_.isInstanceOf[PortOutResponse]).map(_.asInstanceOf[PortOutResponse])
  final private[graph] def paramPorts: Seq[PortT] = ports.filter(_.param.isDefined)
  final private[graph] def paramPortsInHandshake: Seq[PortInHandshake] = paramPorts.filter(_.isInstanceOf[PortInHandshake]).map(_.asInstanceOf[PortInHandshake])
  final private[graph] def paramPortsOutHandshake: Seq[PortOutHandshake] = paramPorts.filter(_.isInstanceOf[PortOutHandshake]).map(_.asInstanceOf[PortOutHandshake])

  /**
    * @return true, if each ingoing port has exactly one (writing) connection.
    *         outgoing ports can have arbitrary number of connections (0 - n).
    */
  def checkConnections: Boolean
}

sealed trait NodeT extends GraphIR {
  def name: String
  override def children: Seq[GraphT] = subGraph.toSeq
  override def checkConnections: Boolean = children.forall(_.checkConnections) && checkAllowedConnections {
    children.foldLeft(Set.empty[PortSimpleT])((m, c) => c.nodes.foldLeft(m)(_ ++ _.portsSimple)) ++ portsSimple
  }
  protected final def checkAllowedConnections(allowedPorts: Set[PortSimpleT]): Boolean = {
    val offenders = children.flatMap(_.connections.flatMap(_.ports.find(p => !allowedPorts.contains(p))))
    if (offenders.nonEmpty)
      throw MalformedGraphException("port " + offenders.head + " is neither in node " + this + " not in its subgraph " + children.mkString("") + ". This kind of connection is forbidden!")
    true
  }

  def subGraph: Option[GraphT]
  def id: Long
}

final case class Node(name: String, ports: Seq[PortT], subGraph: Option[GraphT], id: Long = Counter(Node.getClass).next()) extends NodeT

/**
  * builds a top node from a graph. The node gets explicit in and out ports that are then connected to the nodes is the subgraph
  */
final case class TopNode(graph: GraphT) extends NodeT {
  private val newStructure: Seq[(PortT, Seq[Connection])] = graph.ports.map(_.forwardToParent)

  override def name: String = "top"
  override def id: Long = Counter(Node.getClass).next()

  override def subGraph: Option[GraphT] = Some(Graph(graph.nodes, graph.connections ++ newStructure.flatMap(_._2)))

  override def ports: Seq[PortT] = newStructure.map(_._1)
}

trait BehaviourNodeT extends NodeT {
  final override def name: String = getClass.getSimpleName.replace(".", "_").toLowerCase.replace("behaviournode", "")
  def behaviourPorts: Seq[PortT]
  final def allPorts: Seq[PortT] = ports ++ behaviourPorts
  final def allPortsSimple: Seq[PortSimpleT] = allPorts.flatMap(_.flatten)
  override def checkConnections: Boolean = children.forall(_.checkConnections) && checkAllowedConnections {
    val s0 = children.foldLeft(Set.empty[PortSimpleT])((m, c) => c.nodes.foldLeft(m)(_ ++ _.portsSimple))
    val s1 = behaviourPorts.foldLeft(s0)(_ ++ _.flatten)
    s1 ++ portsSimple
  }
}

sealed trait GraphT extends GraphIR {
  override def ports: Seq[PortT] = Graph.getFreePorts(nodes, connections)
  override def children: Seq[GraphIR] = nodes
  override def checkConnections: Boolean = {
    if (!children.forall(_.checkConnections))
      return false

    // check all ingoing simple ports of any subnode
    val portset = connections.foldLeft(Set.empty[PortSimpleT])((m, c) => m + c.pout + c.pin)
    val offenders = nodes.flatMap(_.portsSimple.find(p => p.isInstanceOf[PortInT] && !portset.contains(p)))
    if (offenders.nonEmpty)
      throw MalformedGraphException("port " + offenders.head + " is not connected!")

    val iterator = connections.groupBy(_.pin).values.map(_.groupBy(_.pinPart)).flatMap(_.values.find(c => c.isEmpty || c.tail.nonEmpty)).iterator
    if (iterator.hasNext) {
      val c = iterator.next()
      throw MalformedGraphException("port " + c.head.pin + " (part " + c.head.pinPart + ") is written by multiple ports: " + c.map(_.pout))
    }

    true
  }

  def nodes: Seq[NodeT]
  def connections: Seq[Connection]
  final def getFreePorts: Seq[PortT] = Graph.getFreePorts(nodes, connections)
}
final private[graph] case class Graph private(nodes: Seq[NodeT], connections: Seq[Connection]) extends GraphT
object Graph {
  private[graph] def apply(nodes: Seq[NodeT], connections: Seq[Connection]): Graph = new Graph(nodes, connections)
  def getFreePorts(nodes: Seq[NodeT], connections: Seq[Connection]): Seq[PortT] = {
    val portset = connections.foldLeft(Set.empty[PortSimpleT])((m, c) => m + c.pout + c.pin)
    nodes.flatMap(_.ports).filterNot(_.flatten.exists(portset.contains))
  }
}

final class Connection private[graph](val pout: PortSimpleT, val pin: PortSimpleT, val poutPart: Option[Int] = None, val pinPart: Option[Int] = None) {
  if (pout.param != pin.param) {
//    throw MalformedGraphException("are you sure to connect ports with different parameters (" + pout.param + " and " + pin.param + ")?")
  }
  if (poutPart.isEmpty && pinPart.isEmpty && (pout.dataType.kind != pin.dataType.kind || pout.dataType.bitWidth != pin.dataType.bitWidth)) {
    throw MalformedGraphException("unable to connect ports with different types: " + pout + " and " + pin)
  }
  // TODO check if VectorType.. only vectortypes are allowed to have parts?!

  def ports: Seq[PortSimpleT] = Seq(pout, pin)
  def contains(p: PortSimpleT): Boolean = pout == p || pin == p
  override def toString: String = "Connection(" + pout + "->" + pin + "," + poutPart + "," + pinPart + ")"
}
