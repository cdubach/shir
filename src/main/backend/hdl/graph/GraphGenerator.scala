package backend.hdl.graph

import backend.hdl._
import backend.hdl.arch.ArchHelper.getLambdaParamIds
import backend.hdl.arch._
import backend.hdl.arch.device.Mul2AddInt
import backend.hdl.arch.mem._
import backend.hdl.graph.util.DataFlowDotGraph
import backend.hdl.specs.FPGA
import core._
import core.util.Log

object GraphGenerator {

  // Memorize the output ports that do not connect to functions.
  // This helps the compiler to avoid connecting function ports to non function ports by mistake/
  private var nonOutputsToFun: List[Seq[PortOutHandshake]] = Nil

  /**
    * generate data flow graph from low level expression and wrap it in a 'top node'
    * @return top level dataflow node for the given IR
    */
  def generate(expr: Expr): NodeT = expr match {
    case TestBench(input, _) =>
      BehaviourNodeTestbench(generate(input))
    case _ =>
      nonOutputsToFun = Nil
      val graph = TopNode(
        _generate(TypeChecker.checkAssert(expr), Map())
      )
      // DataFlowDotGraph(graph).show()
      graph
  }

  private def _generate(expr: Expr, lambdaFunTypes: Map[Long, FunTypeT], cleanIdNodes: Boolean = true): Graph = cleanup(expr match {
    /*
     *******************************************************************************************************************
     value/input generators
     *******************************************************************************************************************
     */
    case ConstantValue(value: ArithTypeT, t: HWDataTypeT) =>
      Graph(Seq(BehaviourNodeConstantValue(PortOutHandshake(t), value, t)), Seq())
    case ConstantBitVector(values: Seq[ArithTypeT], t: HWDataTypeT) =>
      Graph(Seq(BehaviourNodeConstantBitVector(PortOutHandshake(t), values, t)), Seq())
    case CounterInteger(start: ArithTypeT, increment: ArithTypeT, loop: ArithTypeT, dimensions: Seq[ArithTypeT], repetitions: Seq[ArithTypeT], t: HWDataTypeT) =>
      Graph(Seq(BehaviourNodeCounterInteger(PortOutHandshake(t), start, increment, loop, dimensions, repetitions)), Seq())
    case CounterIntegerND(start: ArithTypeT, loop: ArithTypeT, increments: Seq[ArithTypeT],  dimensions: Seq[ArithTypeT], t: HWDataTypeT) =>
      Graph(Seq(BehaviourNodeCounterIntegerND(PortOutHandshake(t), start, loop, increments, dimensions)), Seq())
    case Repeat(input: Expr, len: ArithTypeT, t: HWDataTypeT) =>
      Graph(Seq(BehaviourNodeRepeat(BehaviourOutFunction(_generate(input, lambdaFunTypes)), PortOutHandshake(t), len)), Seq())
    case RepeatHidden(input: Expr, len: ArithTypeT, t: HWDataTypeT) =>
      Graph(Seq(BehaviourNodeRepeatHidden(BehaviourOutFunction(_generate(input, lambdaFunTypes)), PortOutHandshake(t), len)), Seq())
    case VectorGenerator(input: Expr, _, t: HWDataTypeT) =>
      Graph(Seq(BehaviourNodeVectorGenerator(BehaviourOutFunction(_generate(input, lambdaFunTypes)), PortOutHandshake(t))), Seq())
    case Sink(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeSink(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case Registered(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeRegistered(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case Delay(input: Expr, cycles: ArithTypeT, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeDelay(pin, pout, cycles)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )

    /*
     *******************************************************************************************************************
     simple operations
     *******************************************************************************************************************
     */
    case Id(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeId(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case Signed(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeSigned(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case Unsigned(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeUnsigned(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ResizeInteger(input: Expr, _, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeResizeInteger(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ClipBankersRoundShift(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeClipBankersRoundShift(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case BankersRoundInt(input: Expr, lowElements, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeBankersRoundInt(pin, pout, lowElements)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ClipInt(input: Expr, _, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeClipInt(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case AddInt(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeAddInt(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case AddFloat(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeAddFloat(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case SubInt(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeSubInt(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case MulInt(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeMulInt(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case MulFloat(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeMulFloat(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case MinInt(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeMinInt(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case MaxInt(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeMaxInt(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ConstrainedSum(input: Expr, lowerBoundW: ArithTypeT, upperBoundW: ArithTypeT, lowerBoundH: ArithTypeT, upperBoundH: ArithTypeT, _, _, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeConstrainedSum(pin, pout, lowerBoundW, upperBoundW, lowerBoundH, upperBoundH)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case Select(input: Expr, n: ArithTypeT, selection: ArithTypeT, t: HWDataTypeT) =>
      val inputType = input.t.asInstanceOf[NamedTupleTypeT]
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      val dimensionReduction = t match {
        case s: UnorderedStreamTypeT if !inputType.dimensions.zip(s.dimensions).forall(p => p._1 == p._2) =>
          Log.error(this, "Error in dimensions! Cannot extract stream with dimensions " + s.dimensions + " from a tuple with dimensions " + inputType.dimensions + "! Trying to continue ...")
          0
        case s: UnorderedStreamTypeT => inputType.dimensions.length - s.dimensions.length
        case _ => inputType.dimensions.length
      }
      assert(dimensionReduction <= 1, s"Reduction of ${dimensionReduction} is not supported yet by Select template")
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeSelect(pin, pout, selection, dimensionReduction == 1)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    /*
     *******************************************************************************************************************
     complex operations (map, reduce, slide, ...)
     *******************************************************************************************************************
     */
    case MapOrderedStream(function: Expr, input: Expr, t: OrderedStreamTypeT) =>
      Log.warn(this, "MapOrderedStream is deprecated! Try to use MapSimpleOrderedStream without stream repetitions instead.")
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      val functionGraph = _generate(function, lambdaFunTypes)
      Graph(
        inputGraph.nodes :+ BehaviourNodeMapOrderedStream(BehaviourFunction(functionGraph), pin, pout, t.len),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case MapSimpleOrderedStream(function: Expr, input: Expr, t: OrderedStreamTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      val functionGraph = _generate(function, lambdaFunTypes)
      Graph(
        inputGraph.nodes :+ BehaviourNodeMapSimpleOrderedStream(BehaviourFunction(functionGraph), pin, pout, t.len),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case MapUnorderedStream(function: Expr, input: Expr, t: UnorderedStreamTypeT) =>
      Log.warn(this, "MapUnorderedStream is deprecated! Try to use MapSimpleOrderedStream without stream repetitions instead.")
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      val functionGraph = _generate(function, lambdaFunTypes)
      Graph(
        inputGraph.nodes :+ BehaviourNodeMapUnorderedStream(BehaviourFunction(functionGraph), pin, pout, t.len),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case MapVector(function: Expr, input: Expr, t: VectorTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val vectorLen = t.len

      // modules must have the SAME id in order to reuse the module for all the components
      // ports must have DIFFERENT, unique ids to allow different connections for each component
      val nodeId = Counter(Node.getClass).next()
      val functionNodes = Seq.fill(vectorLen.ae.evalInt)({
        val functionGraph = _generate(function, lambdaFunTypes)
        val portForwarding = functionGraph.getFreePorts.map(_.forwardToParent)
        val newFuncGraph = Graph(functionGraph.nodes, functionGraph.connections ++ portForwarding.flatMap(_._2))
        Node("mapvector_function", portForwarding.map(_._1), Some(newFuncGraph), nodeId)
      }) // create n function nodes with the same id

      val forkGraph = BehaviourNodeVectorFork(inputGraph.portsOutHandshake.head, functionNodes.map(_.portsInHandshake.head))
      val joinGraph = BehaviourNodeVectorJoin(functionNodes.map(_.portsOutHandshake.head), t)

      Graph(
        inputGraph.nodes ++ forkGraph.nodes ++ functionNodes ++ joinGraph.nodes,
        inputGraph.connections ++ forkGraph.connections ++ joinGraph.connections
      )
    case MapAsyncOrderedStream(function: Expr, inputs: Seq[Expr], t: OrderedStreamTypeT) =>
      val inputGraphs = inputs.map(_generate(_, lambdaFunTypes))
      val pins = inputs.map(e => PortInHandshake(e.t.asInstanceOf[HWDataTypeT]))
      val pout = PortOutHandshake(t)
      val functionGraph = _generate(function, lambdaFunTypes)
      // Sort pin order for the function so that it matches with inputs.
      val paramOrder = getLambdaParamIds(function)
      functionGraph.portsInHandshake.foreach(e => e.portOrder = paramOrder.indexOf(e.portOrder))
      Graph(
        inputGraphs.flatMap(_.nodes) ++ Seq(BehaviourNodeMapAsyncOrderedStream(BehaviourFunction(functionGraph), pins, pout, t.len)),
        inputGraphs.flatMap(_.connections) ++ (inputGraphs.zip(pins).flatMap(p =>
          p._1.portsOutHandshake.head.connectTo(p._2)))
      )
    case FoldOrderedStream(function: Expr, input: Expr, t: HWDataTypeT) =>
      val inputType = input.t match {
        case ubt: UpperBoundedStreamTypeT => ubt
        case ot: OrderedStreamTypeT => ot
      }
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(inputType)
      val pout = PortOutHandshake(t)
      val functionGraph = _generate(function, lambdaFunTypes)
      // Convert the param id of each port into port order.
      // A sorting will happen at the vhdl level.
      val paramOrder = getLambdaParamIds(function)
      functionGraph.portsInHandshake.foreach(e => e.portOrder = paramOrder.indexOf(e.portOrder))
      Graph(
        inputGraph.nodes :+ BehaviourNodeFoldOrderedStream(BehaviourFunction(functionGraph), pin, pout, inputType.dimensions, t),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ReduceOrderedStream(function: Expr, initialValue: Expr, input: Expr, t: HWDataTypeT) =>
      val initialValueGraph = _generate(initialValue, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pinInitialValue = PortInHandshake(initialValue.t.asInstanceOf[HWDataTypeT])
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      val functionGraph = _generate(function, lambdaFunTypes)
      // Convert the param id of each port into port order.
      // A sorting will happen at the vhdl level.
      val paramOrder = getLambdaParamIds(function)
      functionGraph.portsInHandshake.foreach(e => e.portOrder = paramOrder.indexOf(e.portOrder))
      Graph(
        initialValueGraph.nodes ++ inputGraph.nodes :+ BehaviourNodeReduceOrderedStream(BehaviourFunction(functionGraph), pinInitialValue, pin, pout),
        initialValueGraph.connections ++ inputGraph.connections ++ initialValueGraph.portsOutHandshake.head.connectTo(pinInitialValue) ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case SlideOrderedStream(input: Expr, windowWidth: ArithTypeT, stepSize: ArithTypeT, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      // assert on faulty configurations!
      val window = windowWidth.ae.evalInt
      val step = stepSize.ae.evalInt
      assert(0 < step && step < window, s"SlideOrderedStream must follow 0 < stepSize(${step}) < windowSize(${window})")
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeSlideOrderedStream(pin, pout, windowWidth, stepSize)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case Alternate(f1: Expr, f2: Expr, input: Expr, t: HWDataTypeT) =>
      val function1Graph = _generate(f1, lambdaFunTypes)
      val function2Graph = _generate(f2, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeAlternate(BehaviourFunction(function1Graph), BehaviourFunction(function2Graph), pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case Ifelse(f1: Expr, f2: Expr, input: Expr, t: HWDataTypeT) =>
      val function1Graph = _generate(f1, lambdaFunTypes)
      val function2Graph = _generate(f2, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeIfelse(BehaviourFunction(function1Graph), BehaviourFunction(function2Graph), pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    /*
     *******************************************************************************************************************
     data type conversions / rearrangement
     *******************************************************************************************************************
     */
    case VectorToTuple(input: Expr, n: ArithTypeT, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeVectorToTuple(pin, pout, n)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case VectorToOrderedStream(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeVectorToOrderedStream(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case OrderedStreamToVector(input: Expr, t: VectorTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeOrderedStreamToVector(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case OrderedStreamToItem(input, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeOrderedStreamToItem(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case IndexUnorderedStream(input, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeIndexUnorderedStream(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case UnorderedStreamToOrderedStream(input: Expr, t: OrderedStreamTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeUnorderedStreamToOrderedStream(pin, pout, t.len)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case OrderedStreamToUnorderedStream(input: Expr, t: UnorderedStreamTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeOrderedStreamToUnorderedStream(pin, pout, t.len)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case OrderedStreamToUpperBoundedStream(input, _, t) =>
      // UpperBoundedStream should only influence the primitives that do not have embedded counters.
      // Note that HW for this conversion is basically identity function since we do not change anything in hardware.
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t.asInstanceOf[HWDataTypeT])
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeConversion(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case Tuple(inputs: Seq[Expr], t: HWDataTypeT) =>
      val inputGraphs = inputs.map(_generate(_, lambdaFunTypes))
      val pins = inputs.map(e => PortInHandshake(e.t.asInstanceOf[HWDataTypeT]))
      val pout = PortOutHandshake(t)
      Graph(
        inputGraphs.flatMap(_.nodes) ++ Seq(BehaviourNodeTuple(pins, pout)),
        inputGraphs.flatMap(_.connections) ++ (inputGraphs.zip(pins).flatMap(p =>
          p._1.portsOutHandshake.head.connectTo(p._2)))
      )
    case FlipTuple(input: Expr, n: ArithTypeT, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeFlipTuple(pin, pout, n)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ZipOrderedStream(input: Expr, n: ArithTypeT, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeZipStream(pin, pout, n)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ZipVector(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeZipVector(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case SplitOrderedStream(input: Expr, _, t: OrderedStreamTypeT) =>
      val innerType: OrderedStreamTypeT = t.et.asInstanceOf[OrderedStreamTypeT]
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeSplitOrderedStream(pin, pout, innerType.len, t.len)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case JoinOrderedStream(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeJoinOrderedStream(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case JoinUnorderedStream(input: Expr, t: UnorderedStreamTypeT) =>
      val inputType = input.t.asInstanceOf[UnorderedStreamTypeT]
      val inputInnerType = inputType.et.asInstanceOf[UnorderedStreamTypeT]
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(inputType)
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeJoinUnorderedStream(pin, pout, inputInnerType.len)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case JoinVector(input: Expr, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeJoinVector(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case DropOrderedStream(input: Expr, firstElements, lastElements, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeDropOrderedStream(pin, pout, input.t.asInstanceOf[OrderedStreamTypeT].len, firstElements, lastElements)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case DropVector(input: Expr, lowElements, highElements, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeDropVector(pin, pout, lowElements, highElements)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ConcatOrderedStream(input1: Expr, input2: Expr, dimension: ArithTypeT, t: HWDataTypeT) =>
      val input1Graph = _generate(input1, lambdaFunTypes)
      val input2Graph = _generate(input2, lambdaFunTypes)
      val pin1 = PortInHandshake(input1.t.asInstanceOf[HWDataTypeT])
      val pin2 = PortInHandshake(input2.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        input1Graph.nodes ++ input2Graph.nodes ++ Seq(BehaviourNodeConcatOrderedStream(pin1, pin2, pout, dimension)),
        input1Graph.connections ++ input2Graph.connections ++ input1Graph.portsOutHandshake.head.connectTo(pin1) ++ input2Graph.portsOutHandshake.head.connectTo(pin2)
      )
    case ConcatVector(input: Expr, t: VectorTypeT) =>
      val inputType = input.t.asInstanceOf[NamedTupleTypeT]
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(inputType)
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeConcatVector(pin, pout, inputType.namedTypes.values.head, inputType.namedTypes.values.last, t.et)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case UpdateVector(input: Expr, t: VectorTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeUpdateVector(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case RotateVector(input: Expr, rotateLeft, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeRotateVector(pin, pout, rotateLeft)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case PermuteVector(input: Expr, permuteFun: ArithLambdaType, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodePermuteVector(pin, pout, permuteFun)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case SlideVector(input: Expr, windowWidth: ArithTypeT, stepSize: ArithTypeT, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeSlideVector(pin, pout, windowWidth, stepSize)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    /*
     *******************************************************************************************************************
     memory related expressions
     *******************************************************************************************************************
     */
    case MemoryAllocation(_, _, _, _, _) | MemoryRegion(_, _, _, _, _, _) | Read(_, _, _) | Write(_, _, _) =>
      throw MalformedExprException("Cannot generate dataflow graph from high-level memory expressions")
    /*
    sync
     */
    case ReadSync(memoryController, baseAddr, input, t: HWDataTypeT) =>
      val baseAddrGraph = _generate(baseAddr, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pinBaseAddr = PortInHandshake(baseAddr.t.asInstanceOf[HWDataTypeT])
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        baseAddrGraph.nodes ++ inputGraph.nodes :+ BehaviourNodeReadSync(BehaviourFunction(_generate(memoryController, lambdaFunTypes)), pinBaseAddr, pin, pout),
        baseAddrGraph.connections ++ inputGraph.connections ++ baseAddrGraph.portsOutHandshake.head.connectTo(pinBaseAddr) ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case WriteSync(memoryController, baseAddr, input, t: HWDataTypeT) =>
      val baseAddrGraph = _generate(baseAddr, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pinBaseAddr = PortInHandshake(baseAddr.t.asInstanceOf[HWDataTypeT])
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        baseAddrGraph.nodes ++ inputGraph.nodes :+ BehaviourNodeWriteSync(BehaviourFunction(_generate(memoryController, lambdaFunTypes)), pinBaseAddr, pin, pout),
        baseAddrGraph.connections ++ inputGraph.connections ++ baseAddrGraph.portsOutHandshake.head.connectTo(pinBaseAddr) ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ReadSyncMemoryController(ram, input, t: BasicDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[BasicDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeReadSyncMemoryController(BehaviourFunction(_generate(ram, lambdaFunTypes)), pin, pout),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case WriteSyncMemoryController(ram, input, t: BasicDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[BasicDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeWriteSyncMemoryController(BehaviourFunction(_generate(ram, lambdaFunTypes)), pin, pout),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case BlockRamBuffer(input, _, len, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeBlockRam(pin, pout, len),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    /*
    banked
     */
    case ReadSyncBanked(memoryController, baseAddr, input, t: HWDataTypeT) =>
      val baseAddrGraph = _generate(baseAddr, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pinBaseAddr = PortInHandshake(baseAddr.t.asInstanceOf[HWDataTypeT])
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        baseAddrGraph.nodes ++ inputGraph.nodes :+ BehaviourNodeReadSyncBanked(BehaviourFunction(_generate(memoryController, lambdaFunTypes)), pinBaseAddr, pin, pout),
        baseAddrGraph.connections ++ inputGraph.connections ++ baseAddrGraph.portsOutHandshake.head.connectTo(pinBaseAddr) ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case WriteSyncBanked(memoryController, baseAddr, input, t: HWDataTypeT) =>
      val baseAddrGraph = _generate(baseAddr, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pinBaseAddr = PortInHandshake(baseAddr.t.asInstanceOf[HWDataTypeT])
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        baseAddrGraph.nodes ++ inputGraph.nodes :+ BehaviourNodeWriteSyncBanked(BehaviourFunction(_generate(memoryController, lambdaFunTypes)), pinBaseAddr, pin, pout),
        baseAddrGraph.connections ++ inputGraph.connections ++ baseAddrGraph.portsOutHandshake.head.connectTo(pinBaseAddr) ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ReadSyncBankedMemoryController(ram, input, _, t: BasicDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[BasicDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeReadSyncBankedMemoryController(BehaviourFunction(_generate(ram, lambdaFunTypes)), pin, pout),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case WriteSyncBankedMemoryController(ram, input, t: BasicDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[BasicDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeWriteSyncBankedMemoryController(BehaviourFunction(_generate(ram, lambdaFunTypes)), pin, pout),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case SyncBankedBuffer(input, _, len, numBanks,  t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeBankedBlockRam(pin, pout, len, numBanks),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    /*
    async
     */
    case ReadAsync(memoryController, baseAddr, input, t: UnorderedStreamTypeT) =>
      if (t.len.ae.evalInt > FPGA.current.readReqBufferSize.ae.evalInt)
        Log.warn(this, "This asynchronous read will create a large internal on-chip buffer. Resizing with rewrite rules may be required to make the design fit on the FPGA.")
      val baseAddrGraph = _generate(baseAddr, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pinBaseAddr = PortInHandshake(baseAddr.t.asInstanceOf[HWDataTypeT])
      val inputType = input.t.asInstanceOf[UnorderedStreamTypeT]
      val pin = PortInHandshake(inputType)
      val pout = PortOutHandshake(t)
      Graph(
        baseAddrGraph.nodes ++ inputGraph.nodes :+ BehaviourNodeReadAsync(BehaviourAsyncFunction(_generate(memoryController, lambdaFunTypes)), pinBaseAddr, pin, pout, inputType.len),
        baseAddrGraph.connections ++ inputGraph.connections ++ baseAddrGraph.portsOutHandshake.head.connectTo(pinBaseAddr) ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ReadAsyncOrdered(memoryController, baseAddr, input, t: OrderedStreamTypeT) =>
      if (t.len.ae.evalInt > FPGA.current.readReqBufferSize.ae.evalInt)
        Log.warn(this, "This asynchronous read will create a large internal on-chip buffer. Resizing with rewrite rules may be required to make the design fit on the FPGA.")
      val baseAddrGraph = _generate(baseAddr, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pinBaseAddr = PortInHandshake(baseAddr.t.asInstanceOf[HWDataTypeT])
      val inputType = input.t.asInstanceOf[OrderedStreamTypeT]
      val pin = PortInHandshake(inputType)
      val pout = PortOutHandshake(t)
      Graph(
        baseAddrGraph.nodes ++ inputGraph.nodes :+ BehaviourNodeReadAsyncOrdered(BehaviourAsyncFunction(_generate(memoryController, lambdaFunTypes)), pinBaseAddr, pin, pout, inputType.len),
        baseAddrGraph.connections ++ inputGraph.connections ++ baseAddrGraph.portsOutHandshake.head.connectTo(pinBaseAddr) ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ReadAsyncOrderedOutOfBounds(memoryController, baseAddr, input, paddedValue, addrRule, t: OrderedStreamTypeT) =>
      if (t.len.ae.evalInt > FPGA.current.readReqBufferSize.ae.evalInt)
        Log.warn(this, "This asynchronous read will create a large internal on-chip buffer. Resizing with rewrite rules may be required to make the design fit on the FPGA.")
      val baseAddrGraph = _generate(baseAddr, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pinBaseAddr = PortInHandshake(baseAddr.t.asInstanceOf[HWDataTypeT])
      val inputType = input.t.asInstanceOf[OrderedStreamTypeT]
      val pin = PortInHandshake(inputType)
      val pout = PortOutHandshake(t)
      Graph(
        baseAddrGraph.nodes ++ inputGraph.nodes :+ BehaviourNodeReadAsyncOrderedOutOfBounds(BehaviourAsyncFunction(_generate(memoryController, lambdaFunTypes)), pinBaseAddr, pin, pout, inputType.len, paddedValue, addrRule),
        baseAddrGraph.connections ++ inputGraph.connections ++ baseAddrGraph.portsOutHandshake.head.connectTo(pinBaseAddr) ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case WriteAsync(memoryController, baseAddr, input, t: HWDataTypeT) =>
      val baseAddrGraph = _generate(baseAddr, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pinBaseAddr = PortInHandshake(baseAddr.t.asInstanceOf[HWDataTypeT])
      val inputType = input.t.asInstanceOf[UnorderedStreamTypeT]
      val pin = PortInHandshake(inputType)
      val pout = PortOutHandshake(t)
      Graph(
        baseAddrGraph.nodes ++ inputGraph.nodes :+ BehaviourNodeWriteAsync(BehaviourAsyncFunction(_generate(memoryController, lambdaFunTypes)), pinBaseAddr, pin, pout, inputType.len),
        baseAddrGraph.connections ++ inputGraph.connections ++ baseAddrGraph.portsOutHandshake.head.connectTo(pinBaseAddr) ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case WriteAsyncOutOfBounds(memoryController, baseAddr, input, addrRule, t: HWDataTypeT) =>
      val baseAddrGraph = _generate(baseAddr, lambdaFunTypes)
      val inputGraph = _generate(input, lambdaFunTypes)
      val pinBaseAddr = PortInHandshake(baseAddr.t.asInstanceOf[HWDataTypeT])
      val inputType = input.t.asInstanceOf[UnorderedStreamTypeT]
      val pin = PortInHandshake(inputType)
      val pout = PortOutHandshake(t)
      Graph(
        baseAddrGraph.nodes ++ inputGraph.nodes :+ BehaviourNodeWriteAsyncOutOfBounds(BehaviourAsyncFunction(_generate(memoryController, lambdaFunTypes)), pinBaseAddr, pin, pout, inputType.len, addrRule),
        baseAddrGraph.connections ++ inputGraph.connections ++ baseAddrGraph.portsOutHandshake.head.connectTo(pinBaseAddr) ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    case ReadHostMemoryController(input, _, reqBufferSize, t: NamedTupleTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInRequest(input.t.asInstanceOf[NamedTupleTypeT])
      val pout = PortOutResponse(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeReadHostMemoryController(pin, pout, reqBufferSize),
        inputGraph.connections ++ inputGraph.portsOutRequest.head.connectTo(pin)
      )
    case WriteHostMemoryController(input, _, reqBufferSize, t: NamedTupleTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInRequest(input.t.asInstanceOf[NamedTupleTypeT])
      val pout = PortOutResponse(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeWriteHostMemoryController(pin, pout, reqBufferSize),
        inputGraph.connections ++ inputGraph.portsOutRequest.head.connectTo(pin)
      )
    case ReadLocalMemoryController(ram, input, t: NamedTupleTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInRequest(input.t.asInstanceOf[NamedTupleTypeT])
      val pout = PortOutResponse(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeReadLocalMemoryController(BehaviourAsyncFunction(_generate(ram, lambdaFunTypes)), pin, pout),
        inputGraph.connections ++ inputGraph.portsOutRequest.head.connectTo(pin)
      )
    case WriteLocalMemoryController(ram, input, t: NamedTupleTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInRequest(input.t.asInstanceOf[NamedTupleTypeT])
      val pout = PortOutResponse(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeWriteLocalMemoryController(BehaviourAsyncFunction(_generate(ram, lambdaFunTypes)), pin, pout),
        inputGraph.connections ++ inputGraph.portsOutRequest.head.connectTo(pin)
      )
    case OnBoardRamBuffer(input, _, _, burstType, _, t: NamedTupleTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInRequest(input.t.asInstanceOf[NamedTupleTypeT])
      val pout = PortOutResponse(t)
      Graph(
        inputGraph.nodes :+ BehaviourNodeOnBoardRam(pin, pout, burstType),
        inputGraph.connections ++ inputGraph.portsOutRequest.head.connectTo(pin)
      )
    /*
     *******************************************************************************************************************
     device specific
     *******************************************************************************************************************
     */
    case Mul2AddInt(input, t: HWDataTypeT) =>
      val inputGraph = _generate(input, lambdaFunTypes)
      val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
      val pout = PortOutHandshake(t)
      Graph(
        inputGraph.nodes ++ Seq(BehaviourNodeMul2AddInt(pin, pout)),
        inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
      )
    /*
     *******************************************************************************************************************
     general (core) expressions
     *******************************************************************************************************************
     */
    case expr: CoreExpr => expr match {
      case Value(_) | BuiltinFunction(_) | BuiltinTypeFunction(_) =>
        throw MalformedExprException("These expressions must be hidden within builtin expressions / primitives.")
      case TypeLambda(body, _) =>
        _generate(body, lambdaFunTypes)
      case TypeFunctionCall(tf, _, _) =>
        _generate(tf, lambdaFunTypes)
      case l: LambdaT =>
        val bodyGraph = _generate(l.body, lambdaFunTypes ++ Map(l.param.id -> l.t))

        val syncType = l.t match {
          case hwFt: HWFunTypeT if hwFt.comType == AsyncCommunication => AsyncCommunication
          case _ => SyncCommunication
        }

        val controlLogic: (Seq[NodeT], Seq[Connection]) = l.param.t match {
          case HWFunType(inType: NamedTupleTypeT, outType: NamedTupleTypeT, AsyncCommunication, schedulingStrategy) =>
            val poutReq = PortOutRequest(inType)
            val pinRsp = PortInResponse(outType)
            val reqClients: Seq[PortOutRequest] = bodyGraph.paramPorts.filter(_.param.exists(_.id == l.param.id)).filter(_.isInstanceOf[PortOutRequest]).map(_.asInstanceOf[PortOutRequest])
            val rspClients: Seq[PortInResponse] = bodyGraph.paramPorts.filter(_.param.exists(_.id == l.param.id)).filter(_.isInstanceOf[PortInResponse]).map(_.asInstanceOf[PortInResponse])
            assert(reqClients.size == rspClients.size)
            reqClients.size match {
              case 0 =>
                throw MalformedExprException("Parameter defined, but never used!")
              case 1 =>
                val pinReq = PortInRequest(inType, Some(l.param))
                val poutRsp = PortOutResponse(outType, Some(l.param))
                (Seq(BehaviourNodeId(pinReq, poutReq), BehaviourNodeId(pinRsp, poutRsp)), reqClients.head.connectTo(pinReq) ++ poutRsp.connectTo(rspClients.head))
              case _ =>
                val pinReq = PortInRequest(inType, Some(l.param))
                val poutRsp = PortOutResponse(outType, Some(l.param))
                val arbiter = BehaviourNodeArbiterAsyncFunction(pinReq, poutRsp, reqClients, rspClients, schedulingStrategy)
                (arbiter.nodes ++ Seq(BehaviourNodeId(pinReq, poutReq), BehaviourNodeId(pinRsp, poutRsp)), arbiter.connections)
            }
          case vType: NamedTupleTypeT if syncType == AsyncCommunication =>
            val pin = PortInRequest(vType)
            val clients: Seq[PortInRequest] = bodyGraph.paramPorts.filter(_.param.exists(_.id == l.param.id)).map(_.asInstanceOf[PortInRequest])
            clients.size match {
              case 0 =>
                throw MalformedExprException("Parameter defined, but never used!")
              case 1 =>
                val pout = PortOutRequest(vType, Some(l.param))
                (Seq(BehaviourNodeId(pin, pout)), pout.connectTo(clients.head)) // shortcut: parameter is only used once => we can directly connect parameter definition (source) and parameter use (target)
              case _ => ???
              //              val graph = BehaviourNodeArbiterHandshakeIn(clients, hs, RoundRobinScheduling())
              //              (Some(graph.nodes.head), graph.connections)
            }
          case hft@ HWFunType(_, _, _, schedulingStrategy) if GraphGeneratorHelper.isSyncCommunication(hft) =>
            nonOutputsToFun = bodyGraph.portsOutHandshake :: nonOutputsToFun
            val allTypes = GraphGeneratorHelper.getAllHWFunIOs(hft).map(_.asInstanceOf[HWDataTypeT])
            val inTypes = allTypes.init
            val outType = allTypes.last
            val numberOfInTypes = inTypes.length
            val poutIns = inTypes.map(inType => PortOutHandshake(inType))
            val pinOut = PortInHandshake(outType)
            val poutClients: Seq[PortOutHandshake] = bodyGraph.paramPorts.filter(_.param.exists(_.id == l.param.id)).filter(_.isInstanceOf[PortOutHandshake]).map(_.asInstanceOf[PortOutHandshake])
            // The modulo of poutClient index is exactly the port's order.
            val poutClientsCategorized: Seq[Seq[PortOutHandshake]] = Seq.range(0, numberOfInTypes).map(id =>{
              poutClients.zipWithIndex.filter(_._2 % numberOfInTypes == id).map(_._1)
            })
            val pinClients: Seq[PortInHandshake] = bodyGraph.paramPorts.filter(_.param.exists(_.id == l.param.id)).filter(_.isInstanceOf[PortInHandshake]).map(_.asInstanceOf[PortInHandshake])
            assert(poutClientsCategorized.map(e => e.length == poutClientsCategorized.head.length).foldLeft(true)(_&&_))
            poutClientsCategorized.head.size match {
              case 0 =>
                throw MalformedExprException("Parameter defined, but never used!")
              case 1 =>
                val pinIns = inTypes.map(inType => PortInHandshake(inType, Some(l.param)))
                val poutOut = PortOutHandshake(outType, Some(l.param))
                (pinIns.zip(poutIns).map(e => BehaviourNodeId(e._1, e._2)) :+ BehaviourNodeId(pinOut, poutOut),
                  poutClientsCategorized.zip(pinIns).map(e => e._1.head.connectTo(e._2)).flatten ++ poutOut.connectTo(pinClients.head))
              case _ =>
                val pinParams = inTypes.map(inType => PortInHandshake(inType, Some(l.param)))
                val poutParam = PortOutHandshake(outType, Some(l.param))
                // Sort the clients if every client has a "client order"
                val arbiter = if (poutClientsCategorized.flatten.forall(_.order >= 0) && pinClients.forall(_.order >= 0)) {
                  val poutClientsCategorizedSorted = poutClientsCategorized.map(_.sortBy(_.order))
                  val pinClientsSorted = pinClients.sortBy(_.order)
                  BehaviourNodeArbiterSyncFunction(pinParams, poutParam, pinClientsSorted, poutClientsCategorizedSorted, OrderedScheduling())
                } else {
                  BehaviourNodeArbiterSyncFunction(pinParams, poutParam, pinClients, poutClientsCategorized, schedulingStrategy)
                }
                (arbiter.nodes ++ pinParams.zip(poutIns).map(e => BehaviourNodeId(e._1, e._2)) :+ BehaviourNodeId(pinOut, poutParam), arbiter.connections)
            }
          case vType: HWDataTypeT => // sync
            val pin = PortInHandshake(vType)
            pin.portOrder = l.param.id // Store param ids for port order sorting later.
            val clients: Seq[PortInHandshake] = bodyGraph.paramPortsInHandshake.filter(_.param.exists(_.id == l.param.id))
            clients.size match {
              case 0 =>
                throw MalformedExprException("Parameter defined, but never used!")
              case 1 =>
                val pout = PortOutHandshake(vType, Some(l.param))
                (Seq(BehaviourNodeId(pin, pout)), pout.connectTo(clients.head)) // shortcut: parameter is only used once => we can directly connect parameter definition (source) and parameter use (target)
              case _ =>
                val pout = PortOutHandshake(vType, Some(l.param))
                val distributor = BehaviourNodeDistributor(pout, clients)
                (distributor.nodes ++ Seq(BehaviourNodeId(pin, pout)), distributor.connections)
            }
        }

        //
        //        // connect all the provided handshake out port to all the clients
        //        case hs: PortOutHandshake => {
        //          val clients: Seq[PortInHandshake] = bodyGraph.paramPortsInHandshake.filter(_.param.id == hs.param.id)
        //          if (clients.isEmpty)
        //            throw MalformedGraphException("Parameter defined, but never used!")
        //          // does not work!! TODO TODO avoid multipe connections to ready back signal
        //          (None, clients.flatMap(hs.connectTo(_)))
        //        }
        Graph(bodyGraph.nodes ++ controlLogic._1, bodyGraph.connections ++ controlLogic._2)

      case fc @ FunctionCall(_, _, _) if GraphGeneratorHelper.getAllFunCallArgs(fc).length > 2 => // only consider more than one inputs (# of inputs + output > 2)
        val funCallArgs = GraphGeneratorHelper.getAllFunCallArgs(fc)
        val f = funCallArgs.head
        val args = funCallArgs.tail
        val inputGraphs = args.map(arg => _generate(arg, lambdaFunTypes))
        val fGraph = _generate(f, lambdaFunTypes)
        // Convert the param id of each port into port order.
        // A sorting will happen at the vhdl level.
        val paramOrder = getLambdaParamIds(f)
        fGraph.portsInHandshake.foreach(e => e.portOrder = paramOrder.indexOf(e.portOrder))
        val sortedFGraphPortsIn = fGraph.portsInHandshake.sortBy(p => p.portOrder)
        val newConnections = inputGraphs.zipWithIndex.map(e =>
          e._1.portsOutHandshake.head.connectTo(sortedFGraphPortsIn.dropRight(args.length - 1 - e._2).last)
        ).flatten
        Graph(inputGraphs.map(_.nodes).flatten ++ fGraph.nodes,
          inputGraphs.map(_.connections).flatten ++ fGraph.connections ++ newConnections)

      case FunctionCall(f, arg@ArchLambda(_, _, _), _) if GraphGeneratorHelper.getArchLambdaDepth(arg) > 1 =>
        val inputGraph = _generate(arg, lambdaFunTypes, false)
        val fGraph = _generate(f, lambdaFunTypes, false)
        val newConnections = f.t.asInstanceOf[FunTypeT].inType match {
          case HWFunType(_, _: FunTypeT, _, _) =>
            if (fGraph.portsInHandshake.isEmpty)
              fGraph.portsOutHandshake.drop(1).zip(inputGraph.portsInHandshake.reverse).map(e => e._1.connectTo(e._2)).flatten
            else
              inputGraph.portsOutHandshake.head.connectTo(fGraph.portsInHandshake.last) ++
                fGraph.portsOutHandshake.drop(1).zip(inputGraph.portsInHandshake.reverse).map(e => e._1.connectTo(e._2)).flatten
          case _ => ???
        }
        cleanup(Graph(inputGraph.nodes ++ fGraph.nodes, inputGraph.connections ++ fGraph.connections ++ newConnections), true)

      case FunctionCall(f, arg, _) =>
        val inputGraph = _generate(arg, lambdaFunTypes)
        // Save the current nonOutputsToFun to restore it later at the end of the scope
        val nonOutputsToFunOld = nonOutputsToFun
        val fGraph = _generate(f, lambdaFunTypes)
        val newConnections = f.t.asInstanceOf[FunTypeT].inType match {
          // we must use last whenever we access the ports of the fGraph in order to get the mapping of nested functioncalls and functions right
          // However, using 'last' seems a bit unsafe, maybe there is a better solution?
          case HWFunType(_, _, AsyncCommunication, _) =>
            fGraph.portsOutRequest.last.connectTo(inputGraph.portsInRequest.head) ++
              inputGraph.portsOutResponse.head.connectTo(fGraph.portsInResponse.last)
          case _: FunTypeT if fGraph.portsInHandshake.isEmpty => // For the case if we only use let parameter once
            fGraph.portsOutHandshake.last.connectTo(inputGraph.portsInHandshake.head)
          case _: FunTypeT => // Sync
            inputGraph.portsOutHandshake.head.connectTo(fGraph.portsInHandshake.last) ++
              fGraph.portsOutHandshake.filterNot(port => nonOutputsToFun.exists(_.contains(port))).last.connectTo(inputGraph.portsInHandshake.head)
          case _: HWDataTypeT =>
            inputGraph.portsOutHandshake.head.connectTo(fGraph.portsInHandshake.last)
        }
        // restoring nonOutputsToFun here
        nonOutputsToFun = nonOutputsToFunOld
        Graph(inputGraph.nodes ++ fGraph.nodes, inputGraph.connections ++ fGraph.connections ++ newConnections)
      case ParamDef(_, _) => throw MalformedExprException("A " + ParamDef.getClass.getName + " must be in a Lambda only!")
      case Marker(ParamUse(p: ParamDef), ts) if ts.s.forall(_.isDigit) =>
        val order = ts.s.toInt
        val syncType = lambdaFunTypes(p.id) match {
          case hwft: HWFunType if hwft.comType == AsyncCommunication => AsyncCommunication
          case _ => SyncCommunication
        }
        p.t match {
          case hwFunt: HWFunTypeT if GraphGeneratorHelper.getAllHWFunIOs(hwFunt).length > 2 => // only consider more than one inputs (# of inputs + output > 2)
            val allIOs = GraphGeneratorHelper.getAllHWFunIOs(hwFunt)
            val inTypes = allIOs.dropRight(1)
            val outType = allIOs.last
            Graph(
              inTypes.map(it => BehaviourNodeId(PortInHandshake(it, order = order), PortOutHandshake(it, Some(p), order = order))) :+
                BehaviourNodeId(PortInHandshake(outType, Some(p), order = order), PortOutHandshake(outType, order = order)),
              Seq()
            )
        }
      case ParamUse(p: ParamDef) =>
        val syncType = lambdaFunTypes(p.id) match {
          case hwft: HWFunType if hwft.comType == AsyncCommunication => AsyncCommunication
          case _ => SyncCommunication
        }
        p.t match {
          case HWFunType(inType: NamedTupleTypeT, outType: NamedTupleTypeT, AsyncCommunication, _) =>
            val reqPortId = Counter(PortRequestT.getClass).next() // set same id for request and response
            Graph(Seq(
              BehaviourNodeId(PortInRequest(inType, None, Some(reqPortId)), PortOutRequest(inType, Some(p), Some(reqPortId))),
              BehaviourNodeId(PortInResponse(outType, Some(p), Some(reqPortId)), PortOutResponse(outType, None, Some(reqPortId)))
            ), Seq())
          case hft @ HWFunType(_, _, _, _) if GraphGeneratorHelper.isSyncCommunication(hft) =>
            val allTypes = GraphGeneratorHelper.getAllHWFunIOs(hft)
            val inTypes = allTypes.init
            val outType = allTypes.last
            Graph(
              inTypes.map(inType => BehaviourNodeId(PortInHandshake(inType), PortOutHandshake(inType, Some(p)))) :+
              BehaviourNodeId(PortInHandshake(outType, Some(p)), PortOutHandshake(outType))
            , Seq())
          case vType: NamedTupleTypeT if syncType == AsyncCommunication => // this parameter is the input of an async function => use the request port!
            Graph(Seq(BehaviourNodeId(PortInRequest(vType, Some(p)), PortOutRequest(vType))), Seq())
          case vType: HWDataTypeT => // no async function => use a 'basic' handshake port
            Graph(Seq(BehaviourNodeId(PortInHandshake(vType, Some(p)), PortOutHandshake(vType))), Seq())
        }
      case Conversion(input, t) =>
        // conversion is only happening in the expression and type world. The actual HW does not require conversion
        val inputGraph = _generate(input, lambdaFunTypes)
        val pin = PortInHandshake(input.t.asInstanceOf[HWDataTypeT])
        val pout = PortOutHandshake(t.asInstanceOf[HWDataTypeT])
        Graph(
          inputGraph.nodes ++ Seq(BehaviourNodeConversion(pin, pout)),
          inputGraph.connections ++ inputGraph.portsOutHandshake.head.connectTo(pin)
        )
      case Marker(input, label: TextTypeT) =>
        val inputGraph = _generate(input, lambdaFunTypes)
        Graph(Seq(BehaviourNodeMarker(BehaviourOutFunction(inputGraph), inputGraph.portsOutHandshake.head.copy, label)), Seq())
    }
  }, cleanIdNodes)

  private def cleanup(graph: Graph, enable: Boolean): Graph = if(enable) removeIdNodePairs(graph) else graph

  private def removeIdNodePairs(graph: Graph): Graph = {
    val idPairNodes = graph.nodes.flatMap {
      case n1@BehaviourNodeId(_, pout, _) => pout.param match {
        case None => Seq()
        case Some(_) =>
          graph.nodes.flatMap {
            case n2@BehaviourNodeId(pin, _, _)
              // check that params of ports are equal
              if pin.param == pout.param &&
                // check that pin and pout are also connected
                pin.flatten.forall(p => {
                  graph.connections.find(_.contains(p)) match {
                    case Some(con) => pout.flatten.exists(con.contains)
                    case None => false
                  }
                })
              => Seq(n1, n2)
            case _ => Seq() // do not remove n1 if there is no connected id node n2 with the same parameter
          }
      }
      case _ => Seq()
    }

    if (graph.nodes.length == idPairNodes.length) {
      // we cannot remove all the nodes and return an empty graph. Therefore just return the original graph in this case
      graph
    } else {
      idPairNodes.foldRight(graph)((idNode, g) => removeIdNode(g, idNode))
    }
  }

  private def removeIdNode(graph: Graph, nodeToRemove: BehaviourNodeId): Graph = {
    val BehaviourNodeId(pin, pout, _) = nodeToRemove
    val portsToRemove = pin.flatten.zip(pout.flatten).map{
      case (p1: PortInT, p2: PortOutT) => (p1, p2)
      case (p1: PortOutT, p2: PortInT) => (p2, p1)
      case _ => throw new RuntimeException("never happens")
    }

    val newNodes = graph.nodes.filterNot(_ == nodeToRemove)

    // Preserve info of pin order so that we can sort pins for Map2Input and Map3Input.
    (pin, pout) match {
      case (i: PortInHandshake, o: PortOutHandshake) if i.portOrder > -1 =>
        // find port connected to IdNode's PortOutHandshake.
        val targetConnect = graph.connections.find(c => c.pout == o.data)
        val targetPin = targetConnect match {
          case Some(conn) =>
            conn.pin match {
              case cPin: PortInSimple => cPin.parent
              case _ =>
            }
          case _ =>
        }
        targetPin match {
          case Some(tPin: PortInHandshake) =>
            tPin.portOrder = i.portOrder
          case _ =>
        }
      case _ =>
    }

    val newConnections = graph.connections.flatMap(conin => {
      (portsToRemove.find(_._1 == conin.pin), portsToRemove.find(p => p._2 == conin.pout)) match {
        case (Some((_, pout)), _) =>
          graph.connections.find(_.pout == pout) match {
            case Some(conout) =>
              (conin.pout, conout.pin) match {
                case (psout: PortOutSimple, psin: PortInSimple) => psout.connectTo(psin)
                case (psout: PortOutSimple, psout2: PortOutSimple) => psout.connectToParentOut(psout2)
                case (psin: PortInSimple, psin2: PortInSimple) => psin.connectToChildIn(psin2)
                case _ => throw new RuntimeException("never happens")
              }
            case None => Seq()
          }
        case (_, Some(_)) => Seq() // has already been replaced in the case above
        case _ => Seq(conin) // keep this connection if no port of it shall be removed
      }
    })

    Graph(newNodes, newConnections)
  }
}
