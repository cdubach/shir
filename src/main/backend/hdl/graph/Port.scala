package backend.hdl.graph

import backend.hdl._
import core.{Counter, ParamDef}

import scala.collection.Seq

sealed trait PortT {
  val id: Long

  def dataType: HWDataTypeT

  def param: Option[ParamDef]

  def flatten: Seq[PortSimpleT]

  def copy: PortT

  def copyInvDirection: PortT

  /**
    * creates a copy of this port (different id) and a connection from this port to the copy
    * useful when child ports shall be forwarded to the parent
    * @return
    */
  def forwardToParent: (PortT, Seq[Connection])
}
object PortT // for the counter
sealed trait PortInT extends PortT {
  override def copy: PortInT
  override def copyInvDirection: PortOutT
  protected def _connectToChildIn(child: PortInT, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection]
  final override def forwardToParent: (PortT, Seq[Connection]) = {
    val np = copy
    (np, np._connectToChildIn(this))
  }
}
sealed trait PortOutT extends PortT {
  override def copy: PortOutT
  override def copyInvDirection: PortInT
  protected def _connectToParentOut(parent: PortOutT, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection]
  final override def forwardToParent: (PortT, Seq[Connection]) = {
    val np = copy
    (np, this._connectToParentOut(np))
  }
}

/**
  * simple ports (transmit one signal only)
  */
sealed trait PortSimpleT extends PortT {
  override def flatten: Seq[PortSimpleT] = Seq(this)
  def parent: Option[PortT]
}
final case class PortInSimple(dataType: HWDataTypeT, parent: Option[PortT] = None, param: Option[ParamDef] = None, override val id: Long = Counter(PortT.getClass).next()) extends PortSimpleT with PortInT {
  override def copy: PortInSimple = PortInSimple(dataType, None, param)
  override def copyInvDirection: PortOutSimple = PortOutSimple(dataType, None, param)
  override protected def _connectToChildIn(child: PortInT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToChildIn(child.asInstanceOf[PortInSimple], thisPart, thatPart)
  def connectToChildIn(child: PortInSimple, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = Seq(new Connection(this, child, thisPart, thatPart))
}
final case class PortOutSimple(dataType: HWDataTypeT, parent: Option[PortT] = None, param: Option[ParamDef] = None, override val id: Long = Counter(PortT.getClass).next()) extends PortSimpleT with PortOutT {
  override def copy: PortOutSimple = PortOutSimple(dataType, None, param)
  override def copyInvDirection: PortInSimple = PortInSimple(dataType, None, param)
  override protected def _connectToParentOut(parent: PortOutT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToParentOut(parent.asInstanceOf[PortOutSimple], thisPart, thatPart)
  def connectTo(that: PortInSimple, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = Seq(new Connection(this, that, thisPart, thatPart))
  def connectToParentOut(parent: PortOutSimple, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = Seq(new Connection(this, parent, thisPart, thatPart))
}

/**
  * handshake ports have multiple signals
  * a data signal, a valid signal to confirm that the outgoing data is on the signals, a ready signal to tell if the reader is ready to receive new data
  */
sealed trait PortHandshakeT extends PortT {
  val data: PortSimpleT
  val last: PortSimpleT
  val valid: PortSimpleT
  val ready: PortSimpleT
  override def flatten: Seq[PortSimpleT] = data.flatten ++ last.flatten ++ valid.flatten ++ ready.flatten
}
final case class PortInHandshake(dataType: HWDataTypeT, param: Option[ParamDef] = None, override val id: Long = Counter(PortT.getClass).next(), order: Int = -1) extends PortHandshakeT with PortInT {
  override val data: PortInSimple = PortInSimple(dataType, Some(this), param)
  override val last: PortInSimple = PortInSimple(LastVectorType(dataType), Some(this), param)
  override val valid: PortInSimple = PortInSimple(LogicType(), Some(this), param)
  override val ready: PortOutSimple = PortOutSimple(ReadyVectorType(dataType), Some(this), param)
  var portOrder: Long = -1
  override def copy: PortInHandshake = PortInHandshake(dataType, param, order=order)
  override def copyInvDirection: PortOutHandshake = {
    val invPort = PortOutHandshake(dataType, param, order=order)
    invPort.portOrder = portOrder
    invPort
  }
  override protected def _connectToChildIn(child: PortInT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToChildIn(child.asInstanceOf[PortInHandshake], thisPart, thatPart)
  def connectToChildIn(child: PortInHandshake, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = {
    data.connectToChildIn(child.data, thisPart, thatPart) ++
      last.connectToChildIn(child.last) ++
      valid.connectToChildIn(child.valid) ++
      child.ready.connectToParentOut(ready)
  }
}

final case class PortOutHandshake(dataType: HWDataTypeT, param: Option[ParamDef] = None, override val id: Long = Counter(PortT.getClass).next(), order: Int = -1) extends PortHandshakeT with PortOutT {
  override val data: PortOutSimple = PortOutSimple(dataType, Some(this), param)
  override val last: PortOutSimple = PortOutSimple(LastVectorType(dataType), Some(this), param)
  override val valid: PortOutSimple = PortOutSimple(LogicType(), Some(this), param)
  override val ready: PortInSimple = PortInSimple(ReadyVectorType(dataType), Some(this), param)
  var portOrder: Long = -1
  override def copy: PortOutHandshake = PortOutHandshake(dataType, param, order=order)
  override def copyInvDirection: PortInHandshake = PortInHandshake(dataType, param, order=order)
  override protected def _connectToParentOut(parent: PortOutT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToParentOut(parent.asInstanceOf[PortOutHandshake], thisPart, thatPart)
  def connectTo(that: PortInHandshake, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = {
    data.connectTo(that.data, thisPart, thatPart) ++
      last.connectTo(that.last) ++
      valid.connectTo(that.valid) ++
      that.ready.connectTo(ready)
  }
  def connectToParentOut(parent: PortOutHandshake, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = {
    data.connectToParentOut(parent.data, thisPart, thatPart) ++
      last.connectToParentOut(parent.last) ++
      valid.connectToParentOut(parent.valid) ++
      parent.ready.connectToChildIn(ready)
  }
}

/**
  * handshake ports for requests and responses to connect to async functions
  */
sealed trait PortRequestT extends PortT {
  val data: PortSimpleT
  val valid: PortSimpleT
  val ready: PortSimpleT
  override def flatten: Seq[PortSimpleT] = data.flatten ++ valid.flatten ++ ready.flatten
}
object PortRequestT // for the counter
sealed trait PortResponseT extends PortT {
  val data: PortSimpleT
  val valid: PortSimpleT
  override def flatten: Seq[PortSimpleT] = data.flatten ++ valid.flatten
}
final case class PortInRequest(dataType: NamedTupleTypeT, param: Option[ParamDef] = None, reqPortId: Option[Long] = None, override val id: Long = Counter(PortT.getClass).next()) extends PortRequestT with PortInT {
  override val data: PortInSimple = PortInSimple(dataType, Some(this), param)
  override val valid: PortInSimple = PortInSimple(LogicType(), Some(this), param)
  override val ready: PortOutSimple = PortOutSimple(LogicType(), Some(this), param)
  override def copy: PortInRequest = PortInRequest(dataType, param, reqPortId)
  override def copyInvDirection: PortOutRequest = PortOutRequest(dataType, param, reqPortId)
  override protected def _connectToChildIn(child: PortInT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToChildIn(child.asInstanceOf[PortInRequest], thisPart, thatPart)
  def connectToChildIn(child: PortInRequest, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = {
    data.connectToChildIn(child.data, thisPart, thatPart) ++
      valid.connectToChildIn(child.valid) ++
      child.ready.connectToParentOut(ready)
  }
}
final case class PortOutRequest(dataType: NamedTupleTypeT, param: Option[ParamDef] = None, reqPortId: Option[Long] = None, override val id: Long = Counter(PortT.getClass).next()) extends PortRequestT with PortOutT {
  override val data: PortOutSimple = PortOutSimple(dataType, Some(this), param)
  override val valid: PortOutSimple = PortOutSimple(LogicType(), Some(this), param)
  override val ready: PortInSimple = PortInSimple(LogicType(), Some(this), param)
  override def copy: PortOutRequest = PortOutRequest(dataType, param, reqPortId)
  override def copyInvDirection: PortInRequest = PortInRequest(dataType, param, reqPortId)
  override protected def _connectToParentOut(parent: PortOutT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToParentOut(parent.asInstanceOf[PortOutRequest], thisPart, thatPart)
  def connectTo(that: PortInRequest, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = {
    data.connectTo(that.data, thisPart, thatPart) ++
      valid.connectTo(that.valid) ++
      that.ready.connectTo(ready)
  }
  def connectToParentOut(parent: PortOutRequest, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = {
    data.connectToParentOut(parent.data, thisPart, thatPart) ++
      valid.connectToParentOut(parent.valid) ++
      parent.ready.connectToChildIn(ready)
  }
}
final case class PortInResponse(dataType: NamedTupleTypeT, param: Option[ParamDef] = None, reqPortId: Option[Long] = None, override val id: Long = Counter(PortT.getClass).next()) extends PortResponseT with PortInT {
  override val data: PortInSimple = PortInSimple(dataType, Some(this), param)
  override val valid: PortInSimple = PortInSimple(LogicType(), Some(this), param)
  override def copy: PortInResponse = PortInResponse(dataType, param, reqPortId)
  override def copyInvDirection: PortOutResponse = PortOutResponse(dataType, param, reqPortId)
  override protected def _connectToChildIn(child: PortInT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToChildIn(child.asInstanceOf[PortInResponse], thisPart, thatPart)
  def connectToChildIn(child: PortInResponse, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = {
    data.connectToChildIn(child.data, thisPart, thatPart) ++
      valid.connectToChildIn(child.valid)
  }
}
final case class PortOutResponse(dataType: NamedTupleTypeT, param: Option[ParamDef] = None, reqPortId: Option[Long] = None, override val id: Long = Counter(PortT.getClass).next()) extends PortResponseT with PortOutT {
  override val data: PortOutSimple = PortOutSimple(dataType, Some(this), param)
  override val valid: PortOutSimple = PortOutSimple(LogicType(), Some(this), param)
  override def copy: PortOutResponse = PortOutResponse(dataType, param, reqPortId)
  override def copyInvDirection: PortInResponse = PortInResponse(dataType, param, reqPortId)
  override protected def _connectToParentOut(parent: PortOutT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToParentOut(parent.asInstanceOf[PortOutResponse], thisPart, thatPart)
  def connectTo(that: PortInResponse, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = {
    data.connectTo(that.data, thisPart, thatPart) ++
      valid.connectTo(that.valid)
  }
  def connectToParentOut(parent: PortOutResponse, thisPart: Option[Int] = None, thatPart: Option[Int] = None): Seq[Connection] = {
    data.connectToParentOut(parent.data, thisPart, thatPart) ++
      valid.connectToParentOut(parent.valid)
  }
}

/**
  * On Board Ram Access ports
  */
//TODO: Change made here, only ports going in/out of actual on board ram

final case class PortOnBoardRamBank(dataType: BasicDataTypeT, addrType: BasicDataTypeT, burstType: BasicDataTypeT, override val id: Long = Counter(PortT.getClass).next()) extends PortInT {
  override val param: Option[ParamDef] = None
  val waitRequest: PortInSimple = PortInSimple(LogicType(), Some(this))
  val readData: PortInSimple = PortInSimple(dataType, Some(this))
  val readDataValid: PortInSimple = PortInSimple(LogicType(), Some(this))
  val burstCount: PortOutSimple = PortOutSimple(burstType, Some(this))
  val writeData: PortOutSimple = PortOutSimple(dataType, Some(this))
  val address: PortOutSimple = PortOutSimple(addrType, Some(this))
  val write: PortOutSimple = PortOutSimple(LogicType(), Some(this))
  val read: PortOutSimple = PortOutSimple(LogicType(), Some(this))
  val byteEnable: PortOutSimple = PortOutSimple(VectorType(LogicType(), (dataType.bitWidth.ae.evalInt + 7)/8), Some(this))
  val numCyclesWrites: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
  val numCyclesReads: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
  //  val write_req_count: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val write_rsp_count: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val read_req_count: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val read_rsp_count: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val obr_write_req_count: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val obr_read_req_count: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val obr_read_rsp_count: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val write_req_id_req_sum: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val write_req_id_rsp_sum: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val read_req_id_req_sum: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val read_req_id_rsp_sum: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val client_read_req_count: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val idx_buffer_count: PortOutSimple = PortOutSimple(VectorType(LogicType(), 64), Some(this))
//  val information: PortOutSimple = PortOutSimple(VectorType(LogicType(), 5), Some(this))

  override def flatten: Seq[PortSimpleT] = waitRequest.flatten ++ readData.flatten ++ readDataValid.flatten ++ burstCount.flatten ++ writeData.flatten ++ address.flatten ++ write.flatten ++ read.flatten ++ byteEnable.flatten ++ numCyclesWrites.flatten ++ numCyclesReads.flatten
//  ++ write_req_count.flatten ++ write_rsp_count.flatten ++ read_req_count.flatten ++ read_rsp_count.flatten ++ obr_read_req_count.flatten ++ obr_read_rsp_count.flatten ++ obr_write_req_count.flatten ++ write_req_id_req_sum.flatten ++ write_req_id_rsp_sum.flatten ++ read_req_id_req_sum.flatten ++ read_req_id_rsp_sum.flatten ++ client_read_req_count.flatten ++ idx_buffer_count.flatten ++ information.flatten
  override def copy: PortOnBoardRamBank = PortOnBoardRamBank(dataType, addrType, burstType)
  override def copyInvDirection = throw MalformedGraphException("a memory read port with an inverted direction should never be needed and created")
  override protected def _connectToChildIn(child: PortInT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToChildIn(child.asInstanceOf[PortOnBoardRamBank])
  def connectToChildIn(child: PortOnBoardRamBank): Seq[Connection] = {
      waitRequest.connectToChildIn(child.waitRequest) ++
      readData.connectToChildIn(child.readData) ++
      readDataValid.connectToChildIn(child.readDataValid) ++
      child.burstCount.connectToParentOut(burstCount) ++
      child.writeData.connectToParentOut(writeData) ++
      child.address.connectToParentOut(address) ++
      child.write.connectToParentOut(write) ++
      child.read.connectToParentOut(read) ++
      child.byteEnable.connectToParentOut(byteEnable) ++
      child.numCyclesWrites.connectToParentOut(numCyclesWrites) ++
      child.numCyclesReads.connectToParentOut(numCyclesReads)
//      child.write_req_count.connectToParentOut(write_req_count) ++
//      child.write_rsp_count.connectToParentOut(write_rsp_count) ++
//      child.read_req_count.connectToParentOut(read_req_count) ++
//      child.read_rsp_count.connectToParentOut(read_rsp_count) ++
//      child.obr_write_req_count.connectToParentOut(obr_write_req_count) ++
//      child.obr_read_rsp_count.connectToParentOut(obr_read_rsp_count) ++
//      child.obr_read_req_count.connectToParentOut(obr_read_req_count) ++
//      child.write_req_id_req_sum.connectToParentOut(write_req_id_req_sum) ++
//      child.write_req_id_rsp_sum.connectToParentOut(write_req_id_rsp_sum) ++
//      child.read_req_id_req_sum.connectToParentOut(read_req_id_req_sum) ++
//      child.read_req_id_rsp_sum.connectToParentOut(read_req_id_rsp_sum) ++
//      child.client_read_req_count.connectToParentOut(client_read_req_count) ++
//      child.idx_buffer_count.connectToParentOut(idx_buffer_count) ++
//      child.information.connectToParentOut(information)
  }
}

/**
  * port to the DMA interface of the FPGA
  */
final case class PortMemoryRead(dataType: BasicDataTypeT, addrType: BasicDataTypeT, metaDataType: BasicDataTypeT, override val id: Long = Counter(PortT.getClass).next()) extends PortInT {
  override val param: Option[ParamDef] = None
  val memReady: PortInSimple = PortInSimple(LogicType(), Some(this))
  val reqAddr: PortOutSimple = PortOutSimple(addrType, Some(this))
  val reqMData: PortOutSimple = PortOutSimple(metaDataType, Some(this))
  val reqValid: PortOutSimple = PortOutSimple(LogicType(), Some(this))
  val reqTotal: PortOutSimple = PortOutSimple(IntType(64), Some(this))
  val reqPending: PortOutSimple = PortOutSimple(IntType(64), Some(this))
  val reqALMFull: PortInSimple =  PortInSimple(LogicType(), Some(this))
  val rspData: PortInSimple = PortInSimple(dataType, Some(this))
  val rspMData: PortInSimple = PortInSimple(metaDataType, Some(this))
  val rspValid: PortInSimple = PortInSimple(LogicType(), Some(this))
  override def flatten: Seq[PortSimpleT] = memReady.flatten ++ reqAddr.flatten ++ reqMData.flatten ++ reqValid.flatten ++ reqTotal.flatten ++ reqPending.flatten ++ reqALMFull.flatten ++ rspData.flatten ++ rspMData.flatten ++ rspValid.flatten
  override def copy: PortMemoryRead = PortMemoryRead(dataType, addrType, metaDataType)
  override def copyInvDirection = throw MalformedGraphException("a memory read port with an inverted direction should never be needed and created")
  override protected def _connectToChildIn(child: PortInT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToChildIn(child.asInstanceOf[PortMemoryRead])
  def connectToChildIn(child: PortMemoryRead): Seq[Connection] = {
    memReady.connectToChildIn(child.memReady) ++
      child.reqAddr.connectToParentOut(reqAddr) ++
      child.reqMData.connectToParentOut(reqMData) ++
      child.reqValid.connectToParentOut(reqValid) ++
      child.reqTotal.connectToParentOut(reqTotal) ++
      child.reqPending.connectToParentOut(reqPending) ++
      reqALMFull.connectToChildIn(child.reqALMFull) ++
      rspData.connectToChildIn(child.rspData) ++
      rspMData.connectToChildIn(child.rspMData) ++
      rspValid.connectToChildIn(child.rspValid)
  }
}

final case class PortMemoryWrite(dataType: BasicDataTypeT, addrType: BasicDataTypeT, metaDataType: BasicDataTypeT, override val id: Long = Counter(PortT.getClass).next()) extends PortOutT {
  override val param: Option[ParamDef] = None
  val memReady: PortInSimple = PortInSimple(LogicType(), Some(this))
  val reqAddr: PortOutSimple = PortOutSimple(addrType, Some(this))
  val reqMData: PortOutSimple = PortOutSimple(metaDataType, Some(this))
  val reqData: PortOutSimple = PortOutSimple(dataType, Some(this))
  val reqValid: PortOutSimple = PortOutSimple(LogicType(), Some(this))
  val reqTotal: PortOutSimple = PortOutSimple(IntType(64), Some(this))
  val reqPending: PortOutSimple = PortOutSimple(IntType(64), Some(this))
  val reqALMFull: PortInSimple =  PortInSimple(LogicType(), Some(this))
//  val rspData: PortInSimple = PortInSimple(dataType, Some(this))
  val rspMData: PortInSimple = PortInSimple(metaDataType, Some(this))
  val rspValid: PortInSimple = PortInSimple(LogicType(), Some(this))
  override def flatten: Seq[PortSimpleT] = memReady.flatten ++ reqAddr.flatten ++ reqMData.flatten ++ reqData.flatten ++ reqValid.flatten ++ reqTotal.flatten ++ reqPending.flatten ++ reqALMFull.flatten/*++ rspData.flatten*/ ++ rspMData.flatten ++ rspValid.flatten
  override def copy: PortMemoryWrite = PortMemoryWrite(dataType, addrType, metaDataType)
  override def copyInvDirection = throw MalformedGraphException("a memory write port with an inverted direction should never be needed and created")
  override protected def _connectToParentOut(parent: PortOutT, thisPart: Option[Int], thatPart: Option[Int]): Seq[Connection] = connectToParentOut(parent.asInstanceOf[PortMemoryWrite])
  def connectToParentOut(parent: PortMemoryWrite): Seq[Connection] = {
    parent.memReady.connectToChildIn(memReady) ++
      reqAddr.connectToParentOut(parent.reqAddr) ++
      reqMData.connectToParentOut(parent.reqMData) ++
      reqData.connectToParentOut(parent.reqData) ++
      reqValid.connectToParentOut(parent.reqValid) ++
      reqTotal.connectToParentOut(parent.reqTotal) ++
      reqPending.connectToParentOut(parent.reqPending) ++
      parent.reqALMFull.connectToChildIn(reqALMFull) ++
//      parent.rspData.connectToChildIn(rspData) ++
      parent.rspMData.connectToChildIn(rspMData) ++
      parent.rspValid.connectToChildIn(rspValid)
  }
}
