package backend.hdl.graph

import core._
import backend.hdl._
import backend.hdl.util.{BooleanInput, BooleanInputVector, BooleanLogicIR}

trait BehaviourNodeSimpleT extends BehaviourNodeT {
  final def behaviourPorts: Seq[PortT] = Seq()
  final override def subGraph: Option[GraphT] = None
}

final case class BehaviourFunction(graph: Graph) {
  val pin: PortInHandshake = graph.portsOutHandshake.head.copyInvDirection
  val pouts: Seq[PortOutHandshake] = graph.portsInHandshake.map(_.copyInvDirection)
  val innerConnections: Seq[Connection] = graph.connections ++ graph.portsOutHandshake.head.connectTo(pin) ++ pouts.zip(graph.portsInHandshake).flatMap(p => p._1.connectTo(p._2))
  val portForwarding: Seq[(PortT, Seq[Connection])] = Graph.getFreePorts(graph.nodes, innerConnections).map(_.forwardToParent)
}

trait BehaviourNodeWithFunctionsT extends BehaviourNodeT {
  val functions: Seq[BehaviourFunction]
  final protected def portForwarding: Seq[(PortT, Seq[Connection])] = functions.flatMap(_.portForwarding)
  final override def behaviourPorts: Seq[PortT] = functions.map(_.pin) ++ functions.flatMap(_.pouts)
  final override def subGraph: Option[GraphT] = Some(Graph(functions.flatMap(_.graph.nodes), portForwarding.flatMap(_._2) ++ functions.flatMap(_.innerConnections)))
}

trait BehaviourNodeWithFunctionT extends BehaviourNodeWithFunctionsT {
  val function: BehaviourFunction
  final override val functions: Seq[BehaviourFunction] = Seq(function)
}

final case class BehaviourAsyncFunction(fGraph: Graph) {
  val pin: PortInResponse = fGraph.portsOutResponse.head.copyInvDirection
  val pout: PortOutRequest = fGraph.portsInRequest.head.copyInvDirection
  val innerConnections: Seq[Connection] = fGraph.connections ++ fGraph.portsOutResponse.head.connectTo(pin) ++ pout.connectTo(fGraph.portsInRequest.head)
  val portForwarding: Seq[(PortT, Seq[Connection])] = Graph.getFreePorts(fGraph.nodes, innerConnections).map(_.forwardToParent)
}

trait BehaviourNodeWithAsyncFunctionsT extends BehaviourNodeT {
  val functions: Seq[BehaviourAsyncFunction]
  final protected def portForwarding: Seq[(PortT, Seq[Connection])] = functions.flatMap(_.portForwarding)
  final override def behaviourPorts: Seq[PortT] = functions.map(_.pin) ++ functions.map(_.pout)
  final override def subGraph: Option[GraphT] = Some(Graph(functions.flatMap(_.fGraph.nodes), portForwarding.flatMap(_._2) ++ functions.flatMap(_.innerConnections)))
}

trait BehaviourNodeWithAsyncFunctionT extends BehaviourNodeWithAsyncFunctionsT {
  val function: BehaviourAsyncFunction
  final override val functions: Seq[BehaviourAsyncFunction] = Seq(function)
}

final case class BehaviourOutFunction(fGraph: Graph) {
  val pin: PortInHandshake = fGraph.portsOutHandshake.head.copyInvDirection
  val innerConnections: Seq[Connection] = fGraph.connections ++ fGraph.portsOutHandshake.head.connectTo(pin)
  val portForwarding: Seq[(PortT, Seq[Connection])] = Graph.getFreePorts(fGraph.nodes, innerConnections).map(_.forwardToParent)
}

trait BehaviourNodeWithOutFunctionT extends BehaviourNodeT {
  val function: BehaviourOutFunction
  final protected def portForwarding: Seq[(PortT, Seq[Connection])] = Seq(function).flatMap(_.portForwarding)
  final override def behaviourPorts: Seq[PortT] = Seq(function.pin)
  final override def subGraph: Option[GraphT] = Some(Graph(function.fGraph.nodes, portForwarding.flatMap(_._2) ++ function.innerConnections))
}

/*
 ***********************************************************************************************************************
 value/input generators
 ***********************************************************************************************************************
 */
final case class BehaviourNodeConstantValue(pout: PortOutHandshake, value: ArithTypeT, vType: HWDataTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pout)
}
final case class BehaviourNodeConstantBitVector(pout: PortOutHandshake, values: Seq[ArithTypeT], vType: HWDataTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pout)
}
final case class BehaviourNodeCounterInteger(pout: PortOutHandshake, start: ArithTypeT, increment: ArithTypeT, loop: ArithTypeT, dimensions: Seq[ArithTypeT], repetitions: Seq[ArithTypeT], id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pout)
}
final case class BehaviourNodeCounterIntegerND(pout: PortOutHandshake, start: ArithTypeT, loop: ArithTypeT, increments: Seq[ArithTypeT], dimensions: Seq[ArithTypeT], id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pout)
}
final case class BehaviourNodeRepeat(function: BehaviourOutFunction, pout: PortOutHandshake, len: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithOutFunctionT {
  override def ports: Seq[PortT] = Seq(pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeRepeatHidden(function: BehaviourOutFunction, pout: PortOutHandshake, len: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithOutFunctionT {
  override def ports: Seq[PortT] = Seq(pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeVectorGenerator(function: BehaviourOutFunction, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithOutFunctionT {
  override def ports: Seq[PortT] = Seq(pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeSink(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeRegistered(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeDelay(pin: PortInHandshake, pout: PortOutHandshake, cycles: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeMarker(function: BehaviourOutFunction, pout: PortOutHandshake, label: TextTypeT, highlight: Boolean = true, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithOutFunctionT {
  override def ports: Seq[PortT] = Seq(pout) ++ portForwarding.map(_._1)
}
/*
 ***********************************************************************************************************************
 simple operations
 ***********************************************************************************************************************
 */
final case class BehaviourNodeId(pin: PortInT, pout: PortOutT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeSigned(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeUnsigned(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeClipBankersRoundShift(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeBankersRoundInt(pin: PortInHandshake, pout: PortOutHandshake, lowElements: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeClipInt(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeAddInt(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeAddFloat(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeSubInt(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeMulInt(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeMulFloat(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeMinInt(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeMaxInt(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeSelect(pin: PortInHandshake, pout: PortOutHandshake, selection: ArithTypeT, reduction: Boolean, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeEnable(pin: PortInHandshake, pout: PortOutHandshake, condition: ArithTypeT,id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeConstrainedSum(pin: PortInHandshake, pout: PortOutHandshake, lowerBoundW: ArithTypeT, upperBoundW: ArithTypeT, lowerBoundH: ArithTypeT, upperBoundH: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
/*
 ***********************************************************************************************************************
 complex operations (map, reduce, slide, ...)
 ***********************************************************************************************************************
 */
sealed trait BehaviourNodeMapStream extends BehaviourNodeT {
  def function: BehaviourFunction
  def pin: PortInHandshake
  def pout: PortOutHandshake

  val delayCtrlLastOut: PortOutSimple = pin.last.copyInvDirection
  val delayCtrlLastIn: PortInSimple = delayCtrlLastOut.copyInvDirection // hack (not needed normally)
  val delayCtrlReadyOut: PortOutSimple = pin.ready.copy
  val delayCtrlReadyIn: PortInSimple = delayCtrlReadyOut.copyInvDirection // hack

  override def behaviourPorts: Seq[PortT] = Seq(function.pin, function.pouts.head, delayCtrlLastOut, delayCtrlLastIn, delayCtrlReadyOut, delayCtrlReadyIn)

  val fGraphWithDelay: Graph = {
    val innerConnections =
      function.graph.connections ++
      function.graph.portsOutHandshake.head.connectTo(function.pin) ++
      function.pouts.head.connectTo(function.graph.portsInHandshake.head) ++
      delayCtrlLastOut.connectTo(delayCtrlLastIn) ++ // this is a hack to make the delay signal part of an unused connection so it is visible in later code generation steps
      delayCtrlReadyOut.connectTo(delayCtrlReadyIn) // this is a hack

    // do not select handshake ports that have a parameter of function type: access to functions MUST NOT be delayed!
    val freeHandshakeInPorts = Graph.getFreePorts(function.graph.nodes, innerConnections).collect{
      case pih: PortInHandshake if
      (pih.param match {
        case Some(p) if p.t.isInstanceOf[HWFunTypeT] => false
        case _ => true
      }) =>
        pih
    }
    val delayComponents = freeHandshakeInPorts.map(pih => {
      val delayPout = pih.copyInvDirection
      val lastDelayCtrlPin = delayCtrlLastOut.copyInvDirection
      val readyDelayCtrlPin = delayCtrlReadyOut.copyInvDirection
      (BehaviourNodeMapStreamDelay(pih.copy, delayPout, lastDelayCtrlPin, readyDelayCtrlPin),
        delayPout.connectTo(pih) ++
        delayCtrlLastOut.connectTo(lastDelayCtrlPin) ++
        delayCtrlReadyOut.connectTo(readyDelayCtrlPin)
      )
    })
    Graph(function.graph.nodes ++ delayComponents.map(_._1), innerConnections ++ delayComponents.flatMap(_._2))
  }

  final protected val portForwarding = fGraphWithDelay.getFreePorts.map(_.forwardToParent)
  override def subGraph: Option[GraphT] = Some(Graph(fGraphWithDelay.nodes, fGraphWithDelay.connections ++ portForwarding.flatMap(_._2)))
  override def ports: Seq[PortT] = Seq(pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeMapStreamDelay(pin: PortInHandshake, pout: PortOutHandshake, controlLastPin: PortInSimple, controlReadyPin: PortInSimple, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout, controlLastPin, controlReadyPin)
}
final case class BehaviourNodeMapOrderedStream(function: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, len: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeMapStream
final case class BehaviourNodeMapSimpleOrderedStream(function: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, len: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeMapStream
final case class BehaviourNodeMapUnorderedStream(function: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, len: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeMapStream
final case class BehaviourNodeMapBufferedUnorderedStream(function: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, len: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeMapStream
final case class BehaviourNodeMapAsyncOrderedStream(function: BehaviourFunction, pins: Seq[PortInHandshake], pout: PortOutHandshake, len: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  assert(
    {
      pins.exists(p => !p.dataType.isInstanceOf[UnorderedStreamTypeT]) || (
        pins.map(p => p.dataType.asInstanceOf[UnorderedStreamTypeT].len.ae.evalInt).distinct.size < 2)
    },
    "MapAsync consisting streams of  different lengths may not be fully supported with the current VHDL templates."
  )
  override def ports: Seq[PortT] = (pins :+ pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeFoldOrderedStream(function: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, dimensionsIn: Seq[ArithTypeT], bufferDataType: HWDataTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  override def ports: Seq[PortT] = Seq(pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeReduceOrderedStream(function: BehaviourFunction, pinInitialValue: PortInHandshake, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  override def ports: Seq[PortT] = Seq(pinInitialValue, pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeSlideOrderedStream(pin: PortInHandshake, pout: PortOutHandshake, windowWidth: ArithTypeT, stepSize: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeAlternate(function1: BehaviourFunction, function2: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionsT {
  override val functions: Seq[BehaviourFunction] = Seq(function1, function2)
  override def ports: Seq[PortT] = Seq(pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeIfelse(function1: BehaviourFunction, function2: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionsT {
  override val functions: Seq[BehaviourFunction] = Seq(function1, function2)
  override def ports: Seq[PortT] = Seq(pin, pout) ++ portForwarding.map(_._1)
}
/*
 ***********************************************************************************************************************
 data type conversions / rearrangement
 ***********************************************************************************************************************
 */
final case class BehaviourNodeConversion(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeResizeInteger(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeOrderedStreamToItem(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeVectorToTuple(pin: PortInHandshake, pout: PortOutHandshake, n: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeVectorToOrderedStream(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeOrderedStreamToVector(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeIndexUnorderedStream(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeUnorderedStreamToOrderedStream(pin: PortInHandshake, pout: PortOutHandshake, len: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeOrderedStreamToUnorderedStream(pin: PortInHandshake, pout: PortOutHandshake, len: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeTuple(pins: Seq[PortInHandshake], pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  assert(
    {
      pins.exists(p => !p.dataType.isInstanceOf[UnorderedStreamTypeT]) || (
        pins.map(p => p.dataType.asInstanceOf[UnorderedStreamTypeT].len.ae.evalInt).distinct.size < 2)
    },
    "Tuples consisting streams of different lengths may not be fully supported with the current VHDL templates."
  )
  override def ports: Seq[PortT] = pins :+ pout
}
final case class BehaviourNodeFlipTuple(pin: PortInHandshake, pout: PortOutHandshake, n: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeZipStream(pin: PortInHandshake, pout: PortOutHandshake, n: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeZipVector(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeSwitchStream(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeSplitOrderedStream(pin: PortInHandshake, pout: PortOutHandshake, innerLength: ArithTypeT, outerLength: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeJoinOrderedStream(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeJoinUnorderedStream(pin: PortInHandshake, pout: PortOutHandshake, innerLength: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeJoinVector(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeDropOrderedStream(pin: PortInHandshake, pout: PortOutHandshake, len: ArithTypeT, firstElements: ArithTypeT, lastElements: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeDropVector(pin: PortInHandshake, pout: PortOutHandshake, lowElements: ArithTypeT, highElements: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeConcatOrderedStream(pin1: PortInHandshake, pin2: PortInHandshake, pout: PortOutHandshake, dimension: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin1, pin2, pout)
}
final case class BehaviourNodeConcatVector(pin: PortInHandshake, pout: PortOutHandshake, lowDataType: HWDataTypeT, highDataType: HWDataTypeT, wordType: HWDataTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeUpdateVector(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeRotateVector(pin: PortInHandshake, pout: PortOutHandshake, rotateLeft: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodePermuteVector(pin: PortInHandshake, pout: PortOutHandshake, permuteFun: ArithLambdaType, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
final case class BehaviourNodeSlideVector(pin: PortInHandshake, pout: PortOutHandshake, windowWidth: ArithTypeT, StepSize: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
/*
 ***********************************************************************************************************************
 memory related behaviour
 ***********************************************************************************************************************
 */
final case class BehaviourNodeReadSync(function: BehaviourFunction, pinBaseAddr: PortInHandshake, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  override def ports: Seq[PortT] = Seq(pinBaseAddr, pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeWriteSync(function: BehaviourFunction, pinBaseAddr: PortInHandshake, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  override def ports: Seq[PortT] = Seq(pinBaseAddr, pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeReadSyncMemoryController(function: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  override def ports: Seq[PortT] = Seq(pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeWriteSyncMemoryController(function: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  override def ports: Seq[PortT] = Seq(pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeBlockRam(pin: PortInHandshake, pout: PortOutHandshake, entries: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}

final case class BehaviourNodeReadSyncBanked(function: BehaviourFunction, pinBaseAddr: PortInHandshake, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  override def ports: Seq[PortT] = Seq(pinBaseAddr, pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeWriteSyncBanked(function: BehaviourFunction, pinBaseAddr: PortInHandshake, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  override def ports: Seq[PortT] = Seq(pinBaseAddr, pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeReadSyncBankedMemoryController(function: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  override def ports: Seq[PortT] = Seq(pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeWriteSyncBankedMemoryController(function: BehaviourFunction, pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithFunctionT {
  override def ports: Seq[PortT] = Seq(pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeBankedBlockRam(pin: PortInHandshake, pout: PortOutHandshake, entries: ArithTypeT, numBanks: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}

//TODO: Change made here
//TODO: Modelled after SyncController, needs to change ?
final case class BehaviourNodeOnBoardRam(pin: PortInRequest, pout: PortOutResponse, burstType: BasicDataTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  val onBoardRamBankPort: PortOnBoardRamBank = {
    val AsyncRamInputType(dataType, addrType, _) = pin.dataType
    PortOnBoardRamBank(dataType, addrType, burstType)
  }
  override def ports: Seq[PortT] = Seq(pin, pout, onBoardRamBankPort)
}

final case class BehaviourNodeReadLocalMemoryController(function: BehaviourAsyncFunction, pin: PortInRequest, pout: PortOutResponse, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithAsyncFunctionT {
  override def ports: Seq[PortT] = Seq(pin, pout) ++ portForwarding.map(_._1)
}

final case class BehaviourNodeWriteLocalMemoryController(function: BehaviourAsyncFunction, pin: PortInRequest, pout: PortOutResponse, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithAsyncFunctionT {
  override def ports: Seq[PortT] = Seq(pin, pout) ++ portForwarding.map(_._1)
}

final case class BehaviourNodeReadAsync(function: BehaviourAsyncFunction, pinBaseAddr: PortInHandshake, pin: PortInHandshake, pout: PortOutHandshake, numElements: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithAsyncFunctionT {
  override def ports: Seq[PortT] = Seq(pinBaseAddr, pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeReadAsyncOrdered(function: BehaviourAsyncFunction, pinBaseAddr: PortInHandshake, pin: PortInHandshake, pout: PortOutHandshake, numElements: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithAsyncFunctionT {
  override def ports: Seq[PortT] = Seq(pinBaseAddr, pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeReadAsyncOrderedOutOfBounds(function: BehaviourAsyncFunction, pinBaseAddr: PortInHandshake, pin: PortInHandshake, pout: PortOutHandshake, numElements: ArithTypeT, paddedValue: BitVectorType, addrRule: ArithLambdaType, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithAsyncFunctionT {
  override def ports: Seq[PortT] = Seq(pinBaseAddr, pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeWriteAsync(function: BehaviourAsyncFunction, pinBaseAddr: PortInHandshake, pin: PortInHandshake, pout: PortOutHandshake, numElements: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithAsyncFunctionT {
  override def ports: Seq[PortT] = Seq(pinBaseAddr, pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeWriteAsyncOutOfBounds(function: BehaviourAsyncFunction, pinBaseAddr: PortInHandshake, pin: PortInHandshake, pout: PortOutHandshake, numElements: ArithTypeT, addrRule: ArithLambdaType, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeWithAsyncFunctionT {
  override def ports: Seq[PortT] = Seq(pinBaseAddr, pin, pout) ++ portForwarding.map(_._1)
}
final case class BehaviourNodeReadHostMemoryController(pin: PortInRequest, pout: PortOutResponse, reqBufferSize: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  val memReadPort: PortMemoryRead = {
    val RequestType(addrType, reqIdType) = pin.dataType
    val RequestType(dataType, _) = pout.dataType
    PortMemoryRead(dataType, addrType, reqIdType)
  }

  override def ports: Seq[PortT] = Seq(pin, pout, memReadPort)
}
final case class BehaviourNodeWriteHostMemoryController(pin: PortInRequest, pout: PortOutResponse, reqBufferSize: ArithTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  val memWritePort: PortMemoryWrite = {
    val RequestType(AddressedDataType(dataType, addrType), reqIdType) = pin.dataType
    PortMemoryWrite(dataType, addrType, reqIdType)
  }
  override def ports: Seq[PortT] = Seq(pin, pout, memWritePort)
}
/*
 ***********************************************************************************************************************
 device specific behaviour
 ***********************************************************************************************************************
 */
final case class BehaviourNodeMul2AddInt(pin: PortInHandshake, pout: PortOutHandshake, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(pin, pout)
}
/*
 ***********************************************************************************************************************
 general purpose behaviour
 ***********************************************************************************************************************
 */
final case class BehaviourNodeTestbench(uut: NodeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeT {
  private val portForwarding = uut.ports.map(_.forwardToParent)
  override def ports: Seq[PortT] = Seq()
  override def behaviourPorts: Seq[PortT] = portForwarding.map(_._1)
  override def subGraph: Option[GraphT] = Some(Graph(Seq(uut), portForwarding.flatMap(_._2)))
}

final case class BehaviourNodeBooleanLogic(logic: BooleanLogicIR, pins: Seq[PortInSimple], pout: PortOutSimple, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = pins ++ Seq(pout)
}
object BehaviourNodeBooleanLogic {
  def apply(logic: BooleanLogicIR): BehaviourNodeBooleanLogic = {
    val pins: Seq[PortInSimple] = logic.getInputs.map{
      case BooleanInputVector(inputs: ArithTypeT) => PortInSimple(VectorType(LogicType(), inputs.ae.evalInt))
      case BooleanInput() => PortInSimple(LogicType())
    }
    val pout: PortOutSimple = PortOutSimple(LogicType())
    BehaviourNodeBooleanLogic(logic, pins, pout)
  }
}

final case class BehaviourNodeDistributor private(
    fromMaster: PortInHandshake,
    toClientsData: PortOutSimple, toClientsLast: PortOutSimple, toClientsValid: PortOutSimple, toClientsReady: PortInSimple,
    id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(fromMaster, toClientsData, toClientsLast, toClientsValid, toClientsReady)
}
object BehaviourNodeDistributor {
  def apply(masterOut: PortOutHandshake, clientsIn: Seq[PortInHandshake]): Graph = {
    val fromMaster = PortInHandshake(masterOut.dataType)
    val toClientsData = PortOutSimple(VectorType(clientsIn.head.data.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))
    val toClientsLast = PortOutSimple(VectorType(clientsIn.head.last.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))
    val toClientsValid = PortOutSimple(VectorType(clientsIn.head.valid.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))
    val toClientsReady = PortInSimple(VectorType(clientsIn.head.ready.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))

    Graph(
      Seq(BehaviourNodeDistributor(fromMaster, toClientsData, toClientsLast, toClientsValid, toClientsReady)),
      clientsIn.zipWithIndex.flatMap(p => toClientsData.connectTo(p._1.data, Some(p._2))) ++
      clientsIn.zipWithIndex.flatMap(p => toClientsLast.connectTo(p._1.last, Some(p._2))) ++
      clientsIn.zipWithIndex.flatMap(p => toClientsValid.connectTo(p._1.valid, Some(p._2))) ++
      clientsIn.zipWithIndex.flatMap(p => p._1.ready.connectTo(toClientsReady, None, Some(p._2))) ++
      masterOut.connectTo(fromMaster)
    )
  }
}

final case class BehaviourNodeCollectorArbiter private(
    fromClientsData: PortInSimple, fromClientsLast: PortInSimple, fromClientsValid: PortInSimple, fromClientsReady: PortOutSimple,
    toMaster: PortOutHandshake,
    strategy: SchedulingStrategyTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(fromClientsData, fromClientsLast, fromClientsValid, fromClientsReady, toMaster)
}
object BehaviourNodeCollectorArbiter {
  def apply(clientsOut: Seq[PortOutHandshake], masterIn: PortInHandshake, strategy: SchedulingStrategyTypeT): Graph = create(clientsOut, Some(masterIn), masterIn.dataType, strategy)
  def apply(clientsOut: Seq[PortOutHandshake], masterInType: HWDataTypeT, strategy: SchedulingStrategyTypeT): Graph = create(clientsOut, None, masterInType, strategy)
  private def create(clientsOut: Seq[PortOutHandshake], masterIn: Option[PortInHandshake], masterInType: HWDataTypeT, strategy: SchedulingStrategyTypeT): Graph = {
    val fromClientsData = PortInSimple(VectorType(clientsOut.head.data.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    val fromClientsLast = PortInSimple(VectorType(clientsOut.head.last.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    val fromClientsValid = PortInSimple(VectorType(clientsOut.head.valid.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    val fromClientsReady = PortOutSimple(VectorType(clientsOut.head.ready.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    val toMaster = PortOutHandshake(masterInType)

    Graph(
      Seq(BehaviourNodeCollectorArbiter(fromClientsData, fromClientsLast, fromClientsValid, fromClientsReady, toMaster, strategy)),
      clientsOut.zipWithIndex.flatMap(p => p._1.data.connectTo(fromClientsData, None, Some(p._2))) ++
      clientsOut.zipWithIndex.flatMap(p => p._1.last.connectTo(fromClientsLast, None, Some(p._2))) ++
      clientsOut.zipWithIndex.flatMap(p => p._1.valid.connectTo(fromClientsValid, None, Some(p._2))) ++
      clientsOut.zipWithIndex.flatMap(p => fromClientsReady.connectTo(p._1.ready, Some(p._2))) ++
      masterIn.toSeq.flatMap(toMaster.connectTo(_))
    )
  }
}

final case class BehaviourNodeVectorFork private(
    fromMaster: PortInHandshake,
    toClientsData: PortOutSimple, toClientsLast: PortOutSimple, toClientsValid: PortOutSimple, toClientsReady: PortInSimple,
    id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(fromMaster, toClientsData, toClientsLast, toClientsValid, toClientsReady)
}
object BehaviourNodeVectorFork {
  def apply(masterOut: PortOutHandshake, clientsIn: Seq[PortInHandshake]): Graph = {
    val fromMaster = PortInHandshake(masterOut.dataType)
    val toClientsData = PortOutSimple(VectorType(clientsIn.head.data.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))
    val toClientsLast = PortOutSimple(VectorType(clientsIn.head.last.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))
    val toClientsValid = PortOutSimple(VectorType(clientsIn.head.valid.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))
    val toClientsReady = PortInSimple(VectorType(clientsIn.head.ready.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))

    Graph(
      Seq(BehaviourNodeVectorFork(fromMaster, toClientsData, toClientsLast, toClientsValid, toClientsReady)),
      clientsIn.zipWithIndex.flatMap(p => toClientsData.connectTo(p._1.data, Some(p._2))) ++
      clientsIn.zipWithIndex.flatMap(p => toClientsLast.connectTo(p._1.last, Some(p._2))) ++
      clientsIn.zipWithIndex.flatMap(p => toClientsValid.connectTo(p._1.valid, Some(p._2))) ++
      clientsIn.zipWithIndex.flatMap(p => p._1.ready.connectTo(toClientsReady, None, Some(p._2))) ++
      masterOut.connectTo(fromMaster)
    )
  }
}

final case class BehaviourNodeVectorJoin private(
    fromClientsData: PortInSimple, fromClientsLast: PortInSimple, fromClientsValid: PortInSimple, fromClientsReady: PortOutSimple,
    toMaster: PortOutHandshake,
    id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(fromClientsData, fromClientsLast, fromClientsValid, fromClientsReady, toMaster)
}
object BehaviourNodeVectorJoin {
  def apply(clientsOut: Seq[PortOutHandshake], masterIn: PortInHandshake): Graph = create(clientsOut, Some(masterIn), masterIn.dataType)
  def apply(clientsOut: Seq[PortOutHandshake], masterInType: HWDataTypeT): Graph = create(clientsOut, None, masterInType)
  private def create(clientsOut: Seq[PortOutHandshake], masterIn: Option[PortInHandshake], masterInType: HWDataTypeT): Graph = {
    val fromClientsData = PortInSimple(VectorType(clientsOut.head.data.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    val fromClientsLast = PortInSimple(VectorType(clientsOut.head.last.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    val fromClientsValid = PortInSimple(VectorType(clientsOut.head.valid.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    val fromClientsReady = PortOutSimple(VectorType(clientsOut.head.ready.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    val toMaster = PortOutHandshake(masterInType)

    Graph(
      Seq(BehaviourNodeVectorJoin(fromClientsData, fromClientsLast, fromClientsValid, fromClientsReady, toMaster)),
      clientsOut.zipWithIndex.flatMap(p => p._1.data.connectTo(fromClientsData, None, Some(p._2))) ++
      clientsOut.zipWithIndex.flatMap(p => p._1.last.connectTo(fromClientsLast, None, Some(p._2))) ++
      clientsOut.zipWithIndex.flatMap(p => p._1.valid.connectTo(fromClientsValid, None, Some(p._2))) ++
      clientsOut.zipWithIndex.flatMap(p => fromClientsReady.connectTo(p._1.ready, Some(p._2))) ++
      masterIn.toSeq.flatMap(toMaster.connectTo(_))
    )
  }
}

final case class BehaviourNodeArbiterSyncFunction private(
  toMasters: Seq[PortOutHandshake], fromMaster: PortInHandshake,
  toClientsData: PortOutSimple, toClientsLast: PortOutSimple, toClientsValid: PortOutSimple, toClientsReady: PortInSimple,
  fromClientsDatas: Seq[PortInSimple], fromClientsLasts: Seq[PortInSimple], fromClientsValids: Seq[PortInSimple], fromClientsReadys: Seq[PortOutSimple],
  strategy: SchedulingStrategyTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = toMasters ++ Seq(fromMaster) ++
    fromClientsDatas ++ fromClientsLasts ++ fromClientsValids ++ fromClientsReadys ++
    Seq(toClientsData, toClientsLast, toClientsValid, toClientsReady)
}
object BehaviourNodeArbiterSyncFunction {
  def apply(masterIns: Seq[PortInHandshake], masterOut: PortOutHandshake, clientsIn: Seq[PortInHandshake], clientsOuts: Seq[Seq[PortOutHandshake]], strategy: SchedulingStrategyTypeT): Graph = {
    val toClientsData = clientsIn.head.data.dataType match {
      case st: OrderedStreamTypeT =>
        PortOutSimple(VectorType(st.leafType, clientsIn.length))
      case bt: BasicDataTypeT =>
        PortOutSimple(VectorType(bt, clientsIn.length))
      case _ => throw MalformedExprException("Data type for arbiter must be either a stream of a basic data type.")
    }
    val toClientsLast = PortOutSimple(VectorType(clientsIn.head.last.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))
    val toClientsValid = PortOutSimple(VectorType(clientsIn.head.valid.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))
    val toClientsReady = PortInSimple(VectorType(clientsIn.head.ready.dataType.asInstanceOf[BasicDataTypeT], clientsIn.length))

    val fromClientsDatas = clientsOuts.map(clientsOut => clientsOut.head.data.dataType match {
      case st: OrderedStreamTypeT =>
        PortInSimple(VectorType(st.leafType, clientsOut.length))
      case bt: BasicDataTypeT =>
        PortInSimple(VectorType(bt, clientsOut.length))
      case _ => throw MalformedExprException("Data type for arbiter must be either a stream of a basic data type.")
    })
    val fromClientsLasts = clientsOuts.map(clientsOut =>
      PortInSimple(VectorType(clientsOut.head.last.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    )
    val fromClientsValids = clientsOuts.map(clientsOut =>
      PortInSimple(VectorType(clientsOut.head.valid.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    )
    val fromClientsReadys = clientsOuts.map(clientsOut =>
      PortOutSimple(VectorType(clientsOut.head.ready.dataType.asInstanceOf[BasicDataTypeT], clientsOut.length))
    )

    val toMasters = masterIns.map(masterIn => PortOutHandshake(masterIn.dataType))
    val fromMaster = PortInHandshake(masterOut.dataType)

    Graph(
      Seq(BehaviourNodeArbiterSyncFunction(toMasters, fromMaster, toClientsData, toClientsLast, toClientsValid, toClientsReady, fromClientsDatas, fromClientsLasts, fromClientsValids, fromClientsReadys, strategy)),
      clientsOuts.zipWithIndex.map(e => {
        val clientsOut = e._1
        val id = e._2
        clientsOut.zipWithIndex.flatMap(p => {
          p._1.data.connectTo(fromClientsDatas(id), None, Some(p._2)) ++
            p._1.last.connectTo(fromClientsLasts(id), None, Some(p._2)) ++
            p._1.valid.connectTo(fromClientsValids(id), None, Some(p._2)) ++
            fromClientsReadys(id).connectTo(p._1.ready, Some(p._2))
        })
      }).flatten
        ++
        clientsIn.zipWithIndex.flatMap(p => {
          toClientsData.connectTo(p._1.data, Some(p._2)) ++
            toClientsLast.connectTo(p._1.last, Some(p._2)) ++
            toClientsValid.connectTo(p._1.valid, Some(p._2)) ++
            p._1.ready.connectTo(toClientsReady, None, Some(p._2))
        }) ++
        masterOut.connectTo(fromMaster) ++
        toMasters.zip(masterIns).map(e => {
          val toMaster = e._1
          val masterIn = e._2
          toMaster.connectTo(masterIn)
        }).flatten
    )
  }
}

final case class BehaviourNodeArbiterAsyncFunction private(
    fromMasterRequest: PortOutRequest, fromMasterResponse: PortInResponse,
    toClientsReqData: PortInSimple, toClientsReqValid: PortInSimple, toClientsReqReady: PortOutSimple,
    toClientsRspData: PortOutSimple, toClientsRspValid: PortOutSimple,
    schedulingStrategy: SchedulingStrategyTypeT, id: Long = Counter(Node.getClass).next()) extends BehaviourNodeSimpleT {
  override def ports: Seq[PortT] = Seq(
    fromMasterRequest, fromMasterResponse,
    toClientsReqData, toClientsReqValid, toClientsReqReady,
    toClientsRspData, toClientsRspValid
  )
}
object BehaviourNodeArbiterAsyncFunction {
  def apply(masterRequest: PortInRequest, masterResponse: PortOutResponse, clientsRequests: Seq[PortOutRequest], clientsResponses: Seq[PortInResponse], strategy: SchedulingStrategyTypeT): Graph = {
    val toClientsReqData = PortInSimple(VectorType(clientsRequests.head.data.dataType.asInstanceOf[BasicDataTypeT], clientsRequests.length))
    val toClientsReqValid = PortInSimple(VectorType(clientsRequests.head.valid.dataType.asInstanceOf[BasicDataTypeT], clientsRequests.length))
    val toClientsReqReady = PortOutSimple(VectorType(clientsRequests.head.ready.dataType.asInstanceOf[BasicDataTypeT], clientsRequests.length))
    val toClientsRspData = PortOutSimple(VectorType(clientsResponses.head.data.dataType.asInstanceOf[BasicDataTypeT], clientsResponses.length))
    val toClientsRspValid = PortOutSimple(VectorType(clientsResponses.head.valid.dataType.asInstanceOf[BasicDataTypeT], clientsResponses.length))
    val fromMasterRequest = PortOutRequest(masterRequest.dataType)
    val fromMasterResponse = PortInResponse(masterResponse.dataType)

    Graph(
      Seq(BehaviourNodeArbiterAsyncFunction(fromMasterRequest, fromMasterResponse, toClientsReqData, toClientsReqValid, toClientsReqReady, toClientsRspData, toClientsRspValid, strategy)),
      clientsRequests.zipWithIndex.flatMap(p => {
        p._1.data.connectTo(toClientsReqData, None, Some(p._2)) ++
          p._1.valid.connectTo(toClientsReqValid, None, Some(p._2)) ++
          toClientsReqReady.connectTo(p._1.ready, Some(p._2))
      }) ++
      clientsResponses.zipWithIndex.flatMap(p => {
        toClientsRspData.connectTo(p._1.data, Some(p._2)) ++
        toClientsRspValid.connectTo(p._1.valid, Some(p._2))
      }) ++
      fromMasterRequest.connectTo(masterRequest) ++
      masterResponse.connectTo(fromMasterResponse)
    )
  }
}
