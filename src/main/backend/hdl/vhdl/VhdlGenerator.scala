package backend.hdl.vhdl

import backend.hdl._
import backend.hdl.graph._
import core._
import lift.arithmetic.Predicate.Operator
import lift.arithmetic.{Cst, IfThenElse, Mod, Predicate, Prod, Sum, Var}

import scala.collection.immutable.HashMap

object VhdlGenerator {

  def generate(topNode: NodeT): VhdlModule = {
    assert(topNode.checkConnections, "Graph contains invalid / is missing connections.")
    _generate(topNode)
  }

  private def generateModulePorts(ports: Seq[PortT]): Seq[(PortSimpleT, VhdlPort)] = ports.zipWithIndex.flatMap(p => generatePorts("p" + p._2, p._1))

  private def generatePorts(id: String, port: PortT): Seq[(PortSimpleT, VhdlPort)] = {
    val (direction, invDirection) = port match {
      case _: PortInT => (VhdlSignalDirection.IN, VhdlSignalDirection.OUT)
      case _: PortOutT => (VhdlSignalDirection.OUT, VhdlSignalDirection.IN)
    }

    port match {
      case ps: PortSimpleT =>
        Seq((ps, VhdlPort(id, "data", direction, ps.dataType)))
      case preq: PortRequestT =>
        Seq(
          (preq.data, VhdlPort(id, "req_data", direction, preq.data.dataType)),
          (preq.valid, VhdlPort(id, "req_valid", direction, preq.valid.dataType)),
          (preq.ready, VhdlPort(id, "req_ready", invDirection, preq.ready.dataType))
        )
      case prsp: PortResponseT =>
        Seq(
          (prsp.data, VhdlPort(id, "rsp_data", direction, prsp.data.dataType)),
          (prsp.valid, VhdlPort(id, "rsp_valid", direction, prsp.valid.dataType))
        )
      case ph: PortHandshakeT =>
        Seq(
          (ph.data, VhdlPort(id, "data", direction, ph.data.dataType)),
          (ph.last, VhdlPort(id, "last", direction, ph.last.dataType)),
          (ph.valid, VhdlPort(id, "valid", direction, ph.valid.dataType)),
          (ph.ready, VhdlPort(id, "ready", invDirection, ph.ready.dataType))
        )
      case onBoardRamPort: PortOnBoardRamBank =>
        Seq(
          (onBoardRamPort.waitRequest, VhdlPort(id, "bank_waitrequest", direction, onBoardRamPort.waitRequest.dataType)),
          (onBoardRamPort.readData, VhdlPort(id, "bank_readdata", direction, onBoardRamPort.readData.dataType)),
          (onBoardRamPort.readDataValid, VhdlPort(id, "bank_readdatavalid", direction, onBoardRamPort.readDataValid.dataType)),
          (onBoardRamPort.burstCount, VhdlPort(id, "bank_burstcount", invDirection, onBoardRamPort.burstCount.dataType)),
          (onBoardRamPort.writeData, VhdlPort(id, "bank_writedata", invDirection, onBoardRamPort.writeData.dataType)),
          (onBoardRamPort.address, VhdlPort(id, "bank_address", invDirection, onBoardRamPort.address.dataType)),
          (onBoardRamPort.write, VhdlPort(id, "bank_write", invDirection, onBoardRamPort.write.dataType)),
          (onBoardRamPort.read, VhdlPort(id, "bank_read", invDirection, onBoardRamPort.read.dataType)),
          (onBoardRamPort.byteEnable, VhdlPort(id, "bank_byteenable", invDirection, onBoardRamPort.byteEnable.dataType)),
          (onBoardRamPort.numCyclesWrites, VhdlPort(id, "num_cycles_writes", invDirection, onBoardRamPort.numCyclesWrites.dataType)),
          (onBoardRamPort.numCyclesReads, VhdlPort(id, "num_cycles_reads", invDirection, onBoardRamPort.numCyclesReads.dataType))
          //          (onBoardRamPort.write_req_count, VhdlPort(id, "write_req_count", invDirection, onBoardRamPort.write_req_count.dataType)),
//          (onBoardRamPort.write_rsp_count, VhdlPort(id, "write_rsp_count", invDirection, onBoardRamPort.write_rsp_count.dataType)),
//          (onBoardRamPort.read_req_count, VhdlPort(id, "read_req_count", invDirection, onBoardRamPort.read_req_count.dataType)),
//          (onBoardRamPort.read_rsp_count, VhdlPort(id, "read_rsp_count", invDirection, onBoardRamPort.read_rsp_count.dataType)),
//          (onBoardRamPort.obr_read_req_count, VhdlPort(id, "obr_read_req_count", invDirection, onBoardRamPort.obr_read_req_count.dataType)),
//          (onBoardRamPort.obr_write_req_count, VhdlPort(id, "obr_write_req_count", invDirection, onBoardRamPort.obr_write_req_count.dataType)),
//          (onBoardRamPort.obr_read_rsp_count, VhdlPort(id, "obr_read_rsp_count", invDirection, onBoardRamPort.obr_read_rsp_count.dataType)),
//          (onBoardRamPort.write_req_id_req_sum, VhdlPort(id, "write_req_id_req_sum", invDirection, onBoardRamPort.write_req_id_req_sum.dataType)),
//          (onBoardRamPort.write_req_id_rsp_sum, VhdlPort(id, "write_req_id_rsp_sum", invDirection, onBoardRamPort.write_req_id_rsp_sum.dataType)),
//          (onBoardRamPort.read_req_id_req_sum, VhdlPort(id, "read_req_id_req_sum", invDirection, onBoardRamPort.read_req_id_req_sum.dataType)),
//          (onBoardRamPort.read_req_id_rsp_sum, VhdlPort(id, "read_req_id_rsp_sum", invDirection, onBoardRamPort.read_req_id_rsp_sum.dataType)),
//          (onBoardRamPort.client_read_req_count, VhdlPort(id, "client_read_req_count", invDirection, onBoardRamPort.client_read_req_count.dataType)),
//          (onBoardRamPort.idx_buffer_count, VhdlPort(id, "idx_buffer_count", invDirection, onBoardRamPort.idx_buffer_count.dataType)),
//          (onBoardRamPort.information, VhdlPort(id, "OBR_controller_information", invDirection, onBoardRamPort.information.dataType))
        )
      case mem: PortMemoryRead =>
        Seq(
          (mem.memReady, VhdlPort("p", "read_mem_ready", direction, mem.memReady.dataType)),
          (mem.reqAddr, VhdlPort("p", "read_req_address", invDirection, mem.reqAddr.dataType)),
          (mem.reqMData, VhdlPort("p", "read_req_mdata", invDirection, mem.reqMData.dataType)),
          (mem.reqValid, VhdlPort("p", "read_req_valid", invDirection, mem.reqValid.dataType)),
          (mem.reqTotal, VhdlPort("p", "read_req_total", invDirection, mem.reqTotal.dataType)),
          (mem.reqPending, VhdlPort("p", "read_req_pending", invDirection, mem.reqPending.dataType)),
          (mem.reqALMFull, VhdlPort("p", "read_req_almFull", direction, mem.reqALMFull.dataType)),
          (mem.rspData, VhdlPort("p", "read_rsp_data", direction, mem.rspData.dataType)),
          (mem.rspMData, VhdlPort("p", "read_rsp_mdata", direction, mem.rspMData.dataType)),
          (mem.rspValid, VhdlPort("p", "read_rsp_valid", direction, mem.rspValid.dataType)),
        )
      case mem: PortMemoryWrite =>
        Seq(
          (mem.memReady, VhdlPort("p", "write_mem_ready", invDirection, mem.memReady.dataType)),
          (mem.reqAddr, VhdlPort("p", "write_req_address", direction, mem.reqAddr.dataType)),
          (mem.reqMData, VhdlPort("p", "write_req_mdata", direction, mem.reqMData.dataType)),
          (mem.reqData, VhdlPort("p", "write_req_data", direction, mem.reqData.dataType)),
          (mem.reqValid, VhdlPort("p", "write_req_valid", direction, mem.reqValid.dataType)),
          (mem.reqTotal, VhdlPort("p", "write_req_total", direction, mem.reqTotal.dataType)),
          (mem.reqPending, VhdlPort("p", "write_req_pending", direction, mem.reqPending.dataType)),
          (mem.reqALMFull, VhdlPort("p", "write_req_almFull", invDirection, mem.reqALMFull.dataType)),
          (mem.rspMData, VhdlPort("p", "write_rsp_mdata", invDirection, mem.rspMData.dataType)),
          (mem.rspValid, VhdlPort("p", "write_rsp_valid", invDirection, mem.rspValid.dataType)),
        )
    }
  }

  private def generateUniqueNodeName(node: NodeT): String = node match {
    case TopNode(_) => "top"
    case BehaviourNodeTestbench(uut, _) => "tb_" + uut.name
    case BehaviourNodeMarker(_, _, label, _, _) => node.name + "_" + node.id + "_" + label.s
    case _ => node.name + "_" + node.id
  }

  private def _generate(node: NodeT): VhdlModule = {
    // (direct!) subnodes of this node
    val subNodes: Seq[NodeT] = node.subGraph.toSeq.flatMap(_.nodes)

    // create VhdlPorts for all inputs and outputs of the current node
    val nodePortSeq = generateModulePorts(node.ports)
    val nodePorts: Map[PortSimpleT, VhdlPort] = HashMap(nodePortSeq: _*)
    // create VhdlPorts for all inputs and outputs of direct subnodes of the current node
    val subNodePorts: Map[PortSimpleT, VhdlPort] = subNodes.flatMap(n => generateModulePorts(n.ports)).toMap ++
      (node match {
          // ... and for the behaviour of this node
        case bh: BehaviourNodeT => generateModulePorts(bh.behaviourPorts)
        case _ => Map()
      })

    // connections in this node
    val subConnections: Seq[Connection] = node.subGraph.toSeq.flatMap(_.connections)
    // external connections go from a subnode to a parent node; internal connections are between subnodes only
    val (externalConnections, internalConnections): (Seq[Connection], Seq[Connection]) = subConnections.partition { con =>
      nodePorts.contains(con.pout) || nodePorts.contains(con.pin)
    }
    // map external connections to existing vhdl signals from this node's ports
    val externalConnectionSignals: Seq[(Connection, VhdlSignalT)] = externalConnections.map { con =>
      con -> nodePorts.get(con.pout).orElse(nodePorts.get(con.pin)).get
    }
    // create new signals for internal connections
    val internalConnectionSignals: Map[Connection, VhdlSignalT] = {
      // distinct by port, because there may be more than 1 connection going (partly!) to a single target
      // here: source port (pout) defines the internal signal (source port type = internal signal type). (Could also be changed, but must be consistent!)
      // These connections must then have the same name/string. So they get the same index (using zipWithIndex)
      val ids: Map[PortSimpleT, Int] = internalConnections.map(_.pout).distinct.zipWithIndex.toMap
      internalConnections.map(con => (con, VhdlSignal("s%02d".format(ids(con.pout)), subNodePorts(con.pout).purpose, con.pout.dataType))).toMap
    }
    val signals = {
      val s0: Map[PortSimpleT, VhdlSignalT] = nodePorts // <-- needed to implicitly upcast
      val s1 = externalConnectionSignals.foldLeft(s0)((m, con) => m + (con._1.pin -> con._2) + (con._1.pout -> con._2))
      internalConnectionSignals.foldLeft(s1)((m, con) => m + (con._1.pin -> con._2) + (con._1.pout -> con._2))
    }

    // add clock and reset port to every module, except testbenches
    val ports = node match {
      case BehaviourNodeTestbench(_, _) => nodePortSeq.map(_._2)
      case _ => VhdlPort.CLOCK +: VhdlPort.RESET +: nodePortSeq.map(_._2)
    }

    // avoid multiple definitions of the same signal, by grouping them and taking the head of the group only
    val internalSignals = internalConnectionSignals.values.groupBy(p => p.id + "" + p.purpose).values.map(_.head).toSeq.sortBy(_.id.s)
    // grouping of subnodes in order to generate only one component declaration per entity (and not per instance)
    val components = subNodes.groupBy(generateUniqueNodeName).values.map(_.head).map(_generate).toSeq

    // group the sub-connections by its ports up-front to lower the cost of instantiating subnodes.
    val subConOut = subConnections.groupBy(_.pout)
    val subConIn = subConnections.groupBy(_.pin)
    // create instantiations of hierarchy subnodes
    val instantiations = subNodes.zipWithIndex.map(sn =>
      VhdlModuleInstance("U" + sn._2 + "_" + sn._1.name, generateUniqueNodeName(sn._1), {
        val snPorts = sn._1.ports.flatMap(_.flatten)
        val vhdlCons = new collection.mutable.ListBuffer[VhdlConnection]()

        // for each port, go through all connections that are relevant for the current subnode
        snPorts.foreach { port =>
          // for each connection, rearrange so that the port of the component is on the left of the assignment.
          // since we separated them into two maps, we need one for each connection direction.

          subConOut.get(port) match {
            case Some(cons) => cons.foreach { con =>
              vhdlCons += (internalConnectionSignals.get(con) match {
                case Some(signal) =>
                  VhdlConnection(subNodePorts(con.pout), signal) // directly + fully connect outgoing port to internal signal (NO PARTIAL CONNECTION)
                case _ =>
                  VhdlConnection(subNodePorts(con.pout), nodePorts(con.pin), con.pinPart, con.poutPart) // no internal signal -> always apply partial connections!
              })
            }
            case _ =>
          }

          subConIn.get(port) match {
            case Some(cons) => cons.foreach { con =>
              vhdlCons += (internalConnectionSignals.get(con) match {
                case Some(signal) =>
                  VhdlConnection(subNodePorts(con.pin), signal, con.pinPart, con.poutPart) // from the internal signal only connect the specified parts to the target (PARTIAL CONNECTION)
                case _ =>
                  VhdlConnection(subNodePorts(con.pin), nodePorts(con.pout), con.pinPart, con.poutPart) // no internal signal -> always apply partial connections!
              })
            }
            case _ =>
          }
        }

        // connect instance's clock and reset to parent clock and reset
        VhdlConnection(VhdlPort.CLOCK, VhdlPort.CLOCK) ::
          VhdlConnection(VhdlPort.RESET, VhdlPort.RESET) ::
          vhdlCons.toList.distinct
      })
    )

    // NOTE if a new behaviour is added, only change the code below this line!

    VhdlModule(
      generateUniqueNodeName(node),
      // generics
      node match {
        case BehaviourNodeConstantValue(_, value, vType, _) =>
          Seq(
            vType match {
              case LogicType() => VhdlGeneric("value", VhdlConstantLogic(value.ae.evalInt != 0))
              case _ => VhdlGeneric("value", VhdlConstantLogicVector(value.ae.evalInt, vType.bitWidth.ae.evalInt))
            },
            VhdlGeneric("loop_all", VhdlConstantLogic(true))
          )
        case BehaviourNodeConstantBitVector(_, values, vType, _) =>
          Seq(VhdlGeneric("value", VhdlConstantLogicVector(values.map(_.ae.evalInt == 1))),
            VhdlGeneric("loop_all", VhdlConstantLogic(true))
          )
        case BehaviourNodeCounterInteger(_, start, increment, loop, dimensions, repetitions, _) =>
          Seq(
            VhdlGeneric("start", VhdlConstantNatural(start.ae.evalInt)),
            VhdlGeneric("increment", VhdlConstantNatural(increment.ae.evalInt)),
            // reverse the dimensions! in HDL the representation is different to this IR
            VhdlGeneric("dimensions", VhdlConstantNaturalVector(dimensions.reverse.map(_.ae.evalInt))),
            VhdlGeneric("repetitions", VhdlConstantLogicVector(repetitions.reverse.map(_.evalBool))),
            VhdlGeneric("loop_all", VhdlConstantLogic(loop.evalBool))
          )
        case BehaviourNodeCounterIntegerND(_, start, loop, increments, dimensions, _) =>
          val repetitions = increments.map(e => if(e.ae.evalInt > 0) ArithType(0) else ArithType(1))
          val newIncrements = increments.map(e => if(e.ae.evalInt > 0) e else ArithType(1))
          Seq(
            VhdlGeneric("start", VhdlConstantNatural(start.ae.evalInt)),
            VhdlGeneric("increments", VhdlConstantNaturalVector(newIncrements.reverse.map(_.ae.evalInt))),
            // reverse the dimensions! in HDL the representation is different to this IR
            VhdlGeneric("dimensions", VhdlConstantNaturalVector(dimensions.reverse.map(_.ae.evalInt))),
            VhdlGeneric("repetitions", VhdlConstantLogicVector(repetitions.reverse.map(_.evalBool))),
            VhdlGeneric("loop_all", VhdlConstantLogic(loop.evalBool))
          )
        case BehaviourNodeConstrainedSum(_, _, lowerBoundW, upperBoundW, lowerBoundH, upperBoundH, _) => Seq(
          VhdlGeneric("w_lower", VhdlConstantNatural(lowerBoundW.ae.evalInt)), VhdlGeneric("w_upper", VhdlConstantNatural(upperBoundW.ae.evalInt)),
          VhdlGeneric("h_lower", VhdlConstantNatural(lowerBoundH.ae.evalInt)), VhdlGeneric("h_upper", VhdlConstantNatural(upperBoundH.ae.evalInt)),
          VhdlGeneric("offset", VhdlConstantNatural(upperBoundW.ae.evalInt - lowerBoundW.ae.evalInt)))
        case BehaviourNodeRepeat(_, _, len, _) => Seq(VhdlGeneric("stream_length", VhdlConstantNatural(len.ae.evalInt)))
        case BehaviourNodeRepeatHidden(_, _, len, _) => Seq(VhdlGeneric("stream_length", VhdlConstantNatural(len.ae.evalInt)))
        case BehaviourNodeDelay(_, _, cycles, _) => Seq(VhdlGeneric("delay_cycles", VhdlConstantNatural(cycles.ae.evalInt)))
        case BehaviourNodeMapOrderedStream(_, _, _, len, _) => Seq(VhdlGeneric("stream_length", VhdlConstantNatural(len.ae.evalInt)))
        case BehaviourNodeMapSimpleOrderedStream(_, _, _, len, _) => Seq(VhdlGeneric("stream_length", VhdlConstantNatural(len.ae.evalInt)))
        case BehaviourNodeMapUnorderedStream(_, _, _, len, _) => Seq(VhdlGeneric("stream_length", VhdlConstantNatural(len.ae.evalInt)))
        case BehaviourNodeMapBufferedUnorderedStream(_, _, _, len, _) => Seq(VhdlGeneric("stream_length", VhdlConstantNatural(len.ae.evalInt)))
        case BehaviourNodeMapAsyncOrderedStream(_, _, _, len, _) => Seq(VhdlGeneric("stream_length", VhdlConstantNatural(len.ae.evalInt)))
        case BehaviourNodeFoldOrderedStream(_, _, _, dimensionsIn, _, _) if dimensionsIn.length == 2 => Seq(VhdlGeneric("inner_length", VhdlConstantNatural(dimensionsIn.head.ae.evalInt)))
        case BehaviourNodeReduceOrderedStream(f, _, _, _, _) if  f.graph.nodes.head.isInstanceOf[BehaviourNodeWriteSync] => Seq(VhdlGeneric("pipeline_length", VhdlConstantNatural(2)))
        case BehaviourNodeSlideOrderedStream(_, _, windowWidth, stepSize, _) => Seq(VhdlGeneric("window_width", VhdlConstantNatural(windowWidth.ae.evalInt)), VhdlGeneric("step_size", VhdlConstantNatural(stepSize.ae.evalInt)))
        case BehaviourNodeUnorderedStreamToOrderedStream(_, _, len, _) => Seq(VhdlGeneric("stream_length", VhdlConstantNatural(len.ae.evalInt)))
        case BehaviourNodeOrderedStreamToUnorderedStream(_, _, len, _) => Seq(VhdlGeneric("stream_length", VhdlConstantNatural(len.ae.evalInt)))
        case BehaviourNodeSplitOrderedStream(_, _, innerLength, outerLength, _) => Seq(VhdlGeneric("inner_stream_length", VhdlConstantNatural(innerLength.ae.evalInt)), VhdlGeneric("outer_stream_length", VhdlConstantNatural(outerLength.ae.evalInt)))
        case BehaviourNodeJoinUnorderedStream(_, _, innerLength, _) => Seq(VhdlGeneric("inner_stream_length", VhdlConstantNatural(innerLength.ae.evalInt)))
        case BehaviourNodeDropOrderedStream(_, _, len, firstElements, lastElements, _) => Seq(VhdlGeneric("stream_length", VhdlConstantNatural(len.ae.evalInt)), VhdlGeneric("first_elements", VhdlConstantNatural(firstElements.ae.evalInt)), VhdlGeneric("last_elements", VhdlConstantNatural(lastElements.ae.evalInt)))
        case BehaviourNodeDropVector(_, _, lowElements, highElements, _) => Seq(VhdlGeneric("low_elements", VhdlConstantNatural(lowElements.ae.evalInt)), VhdlGeneric("high_elements", VhdlConstantNatural(highElements.ae.evalInt)))
        case BehaviourNodeBankersRoundInt(_, _, lowElements, _) => Seq(VhdlGeneric("low_elements", VhdlConstantNatural(lowElements.ae.evalInt)))
        case BehaviourNodeConcatOrderedStream(_, _, _, dimension, _) => Seq(VhdlGeneric("concat_dimension", VhdlConstantNatural(dimension.ae.evalInt)))
        case BehaviourNodeRotateVector(_, _, rotateLeft, _) => Seq(VhdlGeneric("rotate_left", VhdlConstantLogic(rotateLeft.evalBool)))
        case BehaviourNodeSlideVector(_, _, _, stepSize, _) => Seq(VhdlGeneric("step_size", VhdlConstantNatural(stepSize.ae.evalInt)))
        case BehaviourNodeBlockRam(_, pout, entries, _) => Seq(VhdlGeneric("entries", VhdlConstantNatural(entries.ae.evalInt)), VhdlGeneric("data_width", VhdlConstantNatural(pout.dataType.bitWidth.ae.evalInt)))
        case BehaviourNodeBankedBlockRam(_, pout, entries, numBanks, _) => Seq(VhdlGeneric("entries", VhdlConstantNatural(entries.ae.evalInt)), VhdlGeneric("data_width", VhdlConstantNatural(pout.dataType.bitWidth.ae.evalInt)), VhdlGeneric("num_banks", VhdlConstantNatural(numBanks.ae.evalInt)))
        case BehaviourNodeReadAsync(_, _, _, _, numElements, _) =>  Seq(VhdlGeneric("data_length", VhdlConstantNatural(numElements.ae.evalInt)))
        case BehaviourNodeReadAsyncOrdered(_, _, _, _, numElements, _) =>  Seq(VhdlGeneric("data_length", VhdlConstantNatural(numElements.ae.evalInt)))
        case BehaviourNodeReadAsyncOrderedOutOfBounds(_, _, _, _, numElements, paddedValue, addrRule: ArithLambdaType, _) =>
          Seq(VhdlGeneric("data_length", VhdlConstantNatural(numElements.ae.evalInt)),
            VhdlGeneric("padded_value", VhdlConstantLogicVector(paddedValue.toBoolSeq)),
            VhdlGeneric("physical_addr_limit",
            VhdlConstantNatural(
              addrRule.b.ae match {
                case IfThenElse(Predicate(Var(_), Cst(value), Operator.>), _, _) => value.toInt + 1
                case _ => addrRule.v.range.max.evalInt + 1
              }
            )))
        case BehaviourNodeWriteAsync(_, _, _, _, numElements, _) =>  Seq(VhdlGeneric("data_length", VhdlConstantNatural(numElements.ae.evalInt)))
        case BehaviourNodeWriteAsyncOutOfBounds(_, _, _, _, numElements, addrRule: ArithLambdaType,_) =>
          Seq(VhdlGeneric("data_length", VhdlConstantNatural(numElements.ae.evalInt)), VhdlGeneric("physical_addr_limit",
            VhdlConstantNatural(
              addrRule.b.ae match {
                case IfThenElse(Predicate(Var(_), Cst(value), Operator.>), _, _) => value.toInt + 1
                case _ => addrRule.v.range.max.evalInt + 1
              }
            )
          ))
        case BehaviourNodeReadHostMemoryController(_, _, reqBufferSize, _) => Seq(VhdlGeneric("req_buffer_size", VhdlConstantNatural(reqBufferSize.ae.evalInt)))
        case BehaviourNodeWriteHostMemoryController(_, _, reqBufferSize, _) => Seq(VhdlGeneric("req_buffer_size", VhdlConstantNatural(reqBufferSize.ae.evalInt)))
        case BehaviourNodeDistributor(_, toClientsData, _, _, _, _) => Seq(VhdlGeneric("num_clients", VhdlConstantNatural(toClientsData.dataType.asInstanceOf[VectorTypeT].len.ae.evalInt)))
        case BehaviourNodeCollectorArbiter(fromClientsData, _, _, _, _, _, _) => Seq(VhdlGeneric("num_clients", VhdlConstantNatural(fromClientsData.dataType.asInstanceOf[VectorTypeT].len.ae.evalInt)))
        case BehaviourNodeVectorFork(_, toClientsData, _, _, _, _) => Seq(VhdlGeneric("num_clients", VhdlConstantNatural(toClientsData.dataType.asInstanceOf[VectorTypeT].len.ae.evalInt)))
        case BehaviourNodeVectorJoin(fromClientsData, _, _, _, _, _) => Seq(VhdlGeneric("num_clients", VhdlConstantNatural(fromClientsData.dataType.asInstanceOf[VectorTypeT].len.ae.evalInt)))
        case BehaviourNodeArbiterSyncFunction(_, _, toClientsData, _, _, _, _, _, _, _,  _, _) => Seq(VhdlGeneric("num_clients", VhdlConstantNatural(toClientsData.dataType.asInstanceOf[VectorTypeT].len.ae.evalInt)))
        case BehaviourNodeArbiterAsyncFunction(_, _, toClientsReqData, _, _, _, _, _, _) => Seq(VhdlGeneric("num_clients", VhdlConstantNatural(toClientsReqData.dataType.asInstanceOf[VectorTypeT].len.ae.evalInt)), VhdlGeneric("req_list_size", VhdlConstantNatural(64))) // TODO get this number 64 from the ramType somehow?? in the Graph Generator
        case _ => Seq()
      },
      ports,
      components,
      internalSignals ++ (node match {
        case BehaviourNodeMapAsyncOrderedStream(_, pins, _, len, _) =>
          pins.zipWithIndex.map(p => VhdlNaturalRange("in_counter" + p._2, VhdlConstantNatural(0), VhdlConstantNatural(len.ae.evalInt - 1)))
        case BehaviourNodeArbiterSyncFunction(toMasters, _, _, _, _, _, _, _, _, _, _, _) if
          !(toMasters.length == 1 && toMasters.head.dataType.isInstanceOf[BasicDataTypeT]) =>
          val inputIds = Seq.range(0, toMasters.length)
          inputIds.map(i => VhdlNatural(s"start_count_${i}")) ++
            inputIds.map(i => VhdlNatural(s"start_count_${i}_in"))
        case _ => Seq()
      }),
      instantiations,
      // statements
      node match {
        case BehaviourNodeBooleanLogic(logic, pins, pout, _) =>
          Seq(VhdlStatement(s"${nodePorts(pout)} <= ${pins.map(pin => nodePorts(pin)).mkString(" and ")};")) // TODO generate logic code + unfold vectorType / forbid vector Type?
        case BehaviourNodePermuteVector(pin, pout, permuteFun, _) =>
          val vectorLen = pin.dataType.asInstanceOf[VectorTypeT].len.ae.evalInt
          val indexSeq = Seq.range(0, vectorLen, 1)
          indexSeq.map(idx =>
            VhdlStatement(s"${nodePorts(pout.data)}(%d) <= ${nodePorts(pin.data)}(%d);".format(permuteFun.call(ArithType(idx)).ae.evalInt, idx))
          )
        case BehaviourNodeZipStream(pin, pout, n, _) =>
          Seq.range(0, n.ae.evalInt)
            .map(i => VhdlStatement(s"${nodePorts(pout.data)}.t${i} <= ${nodePorts(pin.data)}.t${i};"))
        case BehaviourNodeSelect(pin, pout, sel, _, _) =>
          Seq(VhdlStatement(s"selected_data <= ${nodePorts(pin.data)}.t${sel.ae.evalInt};"))
        case BehaviourNodeVectorToTuple(pin, pout, n, _) =>
          Seq.range(0, n.ae.evalInt)
            .map(i => VhdlStatement(s"${nodePorts(pout.data)}.t${i} <= ${nodePorts(pin.data)}(${i});"))
        case BehaviourNodeFlipTuple(pin, pout, n, _) =>
          val max = n.ae.evalInt
          Seq.range(0, max)
            .map(i => VhdlStatement(s"${nodePorts(pout.data)}.t${i} <= ${nodePorts(pin.data)}.t${max - i - 1};"))
        case BehaviourNodeReadAsyncOrderedOutOfBounds(_, _, pin, _, _, _, addrRule: ArithLambdaType, _) =>
          addrRule.b.ae match{
            case IfThenElse(Predicate(v1 @ Var(_), Cst(value1), Operator.>), Sum(Cst(value2)::Prod(Cst(value3)::(v2 @ Var(_))::Nil)::Nil), v3 @ Var(_)) if // Mirroring Max
              v1.id == v2.id && v1.id == v3.id && value2 == 2 * value1 && value3 == -1 =>
              Seq(VhdlStatement(s"converted_addr <= std_logic_vector(to_unsigned(%d, converted_addr'length) - unsigned(${nodePorts(pin.data)})) when to_integer(unsigned(${nodePorts(pin.data)})) > %d else ${nodePorts(pin.data)};".format(value2, value1)))
            case IfThenElse(Predicate(v1 @ Var(_), Cst(value1), Operator.>), Cst(value2), v2 @ Var(_)) if v1.id == v2.id && value1 == value2 => // Clipping Max
              Seq(VhdlStatement(s"converted_addr <= std_logic_vector(to_unsigned(%d, converted_addr'length)) when to_integer(unsigned(${nodePorts(pin.data)})) > %d else ${nodePorts(pin.data)};".format(value1, value1)))
            case _ =>
              Seq(VhdlStatement(s"converted_addr <= ${nodePorts(pin.data)};"))
          }
        case BehaviourNodeWriteAsyncOutOfBounds(_, _, pin, _, _, addrRule: ArithLambdaType, _) =>
          addrRule.b.ae match{
            case IfThenElse(Predicate(v1 @ Var(_), Cst(value1), Operator.>), Sum(Cst(value2)::Prod(Cst(value3)::(v2 @ Var(_))::Nil)::Nil), v3 @ Var(_)) if // Mirroring Max
              v1.id == v2.id && v1.id == v3.id && value2 == 2 * value1 && value3 == -1 =>
              Seq(VhdlStatement(s"converted_addr <= std_logic_vector(to_unsigned(%d, converted_addr'length) - unsigned(${nodePorts(pin.data)}.t1)) when to_integer(unsigned(${nodePorts(pin.data)}.t1)) > %d else ${nodePorts(pin.data)}.t1;".format(value2, value1)))
            case IfThenElse(Predicate(v1 @ Var(_), Cst(value1), Operator.>), Cst(value2), v2 @ Var(_)) if v1.id == v2.id && value1 == value2 => // Clipping Max
              Seq(VhdlStatement(s"converted_addr <= std_logic_vector(to_unsigned(%d, converted_addr'length)) when to_integer(unsigned(${nodePorts(pin.data)}.t1)) > %d else ${nodePorts(pin.data)}.t1;".format(value1+1, value1)))
            case _ =>
              Seq(VhdlStatement(s"converted_addr <=  ${nodePorts(pin.data)};"))
          }
        case BehaviourNodeTuple(pins, pout, _) =>
          val data = pins.zipWithIndex
            .map(p => VhdlStatement(s"${nodePorts(pout.data)}.t${p._2} <= ${nodePorts(p._1.data)};"))
          val valid = Seq(
            VhdlStatement(s"${nodePorts(pout.valid)} <= ${pins.map(p => nodePorts(p.valid)).mkString(" and ")};")
          )
          val ready = pins.map { p =>
            val sel = s"${nodePorts(pout.ready)}'high downto ${nodePorts(pout.ready)}'high - ${nodePorts(p.ready)}'length + 1"
            val cond = pins.iterator.collect { case q if q != p => s"(${nodePorts(q.valid)} = '1')" }.mkString(" and ")
            VhdlStatement(s"${nodePorts(p.ready)} <= ${nodePorts(pout.ready)}(${sel}) when ${cond} else (others => '0');")
          }
          val last_signal = Seq(
            VhdlStatement(s"last_signal: process(${pins.map(p => nodePorts(p.last)).mkString(", ")})"),
            VhdlStatement(s"variable out_last: std_logic_vector(${nodePorts(pout.last)}'range) := (others => '1');"),
            VhdlStatement("begin"),
            VhdlStatement("out_last := (others => '1');")
          ) ++ pins.map { p =>
            val sel = s"out_last'high downto out_last'high - ${nodePorts(p.last)}'length + 1"
            VhdlStatement(s"out_last(${sel}) := ${nodePorts(p.last)} and out_last(${sel});")
          } ++ Seq(
            VhdlStatement(s"${nodePorts(pout.last)} <= out_last;"),
            VhdlStatement("end process;")
          )
          data ++ valid ++ ready ++ last_signal
        case b@BehaviourNodeMapAsyncOrderedStream(_, pins, _, _, _)  =>
          val sortedFunPorts = b.function.pouts.sortBy(p => p.portOrder)
          val data = pins.zip(sortedFunPorts).zipWithIndex
            .map(p => VhdlStatement(s"${internalConnectionSignals.find(_._1.pout == p._1._2.data).get._2} <= ${nodePorts(p._1._1.data)};"))
          val valid = pins.zip(sortedFunPorts).zipWithIndex
            .map(p => VhdlStatement(s"${internalConnectionSignals.find(_._1.pout == p._1._2.valid).get._2} <= ${nodePorts(p._1._1.valid)};"))
          val last_signal = pins.zip(sortedFunPorts).zipWithIndex.map(p => {
            val internalLast = internalConnectionSignals.find(_._1.pout == p._1._2.last).get._2
            VhdlStatement(s"${internalLast} <= ${nodePorts(p._1._1.last)}(${internalLast}'high downto ${internalLast}'low);")
          })
          val ready = pins.zip(sortedFunPorts).zipWithIndex.map(p => {
            val internalReady = internalConnectionSignals.find(_._1.pin == p._1._2.ready).get._2
            VhdlStatement(s"${nodePorts(p._1._1.ready)} <= ${"\""}1${"\""} & ${internalReady} when in_counter${p._2} = stream_length - 1 and ${internalReady}(${internalReady}'high) = '1' else ${"\""}0${"\""} & ${internalReady};")
          })
          val control = pins.zip(sortedFunPorts).zipWithIndex.map(p => {
            val internalValid = internalConnectionSignals.find(_._1.pout == p._1._2.valid).get._2
            val internalReady = internalConnectionSignals.find(_._1.pin == p._1._2.ready).get._2
            VhdlStatement(s"ingoing_" + p._2 + "_elements_counter_logic: process(clk)\n" +
              s"begin\n" +
              s"if rising_edge(clk) then\n" +
              s"if reset = '1' then\n" +
              s"    in_counter${p._2} <= 0;\n" +
              s"else\n" +
              s"    if ${internalValid} = '1' and ${internalReady}(${internalReady}'high) = '1' then\n" +
              s"        if in_counter${p._2}  < stream_length - 1 then\n" +
              s"            in_counter${p._2} <= in_counter${p._2} + 1;\n" +
              s"        else\n" +
              s"            in_counter${p._2} <= 0;\n" +
              s"        end if;\n" +
              s"    end if;\n" +
              s"end if;\n" +
              s"end if;\n" +
              s"end process;\n"
            )
          })
          data ++ valid ++ last_signal ++ ready ++ control
        case BehaviourNodeArbiterSyncFunction(toMasters, fromMaster, toClientsData, toClientsLast, toClientsValid, toClientsReady, fromClientsDatas, fromClientsLasts, fromClientsValids, fromClientsReadys, _, _) if
          !(toMasters.length == 1 && toMasters.head.dataType.isInstanceOf[BasicDataTypeT]) =>
          val basicDataOnly = toMasters.forall(_.dataType.isInstanceOf[BasicDataTypeT])

          val inputIds = Seq.range(0, toMasters.length)
          val allClientValid = VhdlStatement(s"clients_all_valid_logic: process(" +
            fromClientsValids.map(p => s"${nodePorts(p)}").mkString(", ") +
            s")\n" +
            s"begin\n"+
            s"    for i in 0 to num_clients - 1 loop\n"+
            s"        if " + fromClientsValids.map(p => s"${nodePorts(p)}(i) = '1'").mkString(" and ") + " then\n" +
            s"            port_in_clients_all_valid(i) <= '1';\n" +
            s"        else\n"+
            s"            port_in_clients_all_valid(i) <= '0';\n" +
            s"        end if;\n"+
            s"    end loop;\n"+
            s"end process;\n"
          )
          val oneRemainingItem = VhdlStatement(s"one_remaining_item <= '1' when " +
            inputIds.map(i => s"start_count_${i}_in = 1").mkString(" and ") +
            " else '0';")
          val dataSignals = toMasters.zip(fromClientsDatas).map(p => VhdlStatement(s"${nodePorts(p._1.data)} <= ${nodePorts(p._2)}(selected_client);"))
          val lastSignals = toMasters.zip(fromClientsLasts).map(p => VhdlStatement(s"${nodePorts(p._1.last)} <= ${nodePorts(p._2)}(selected_client);"))
          val validSignals = if (basicDataOnly) {
            toMasters.zipWithIndex.map(p =>
              VhdlStatement(s"${nodePorts(p._1.valid)} <= ${nodePorts(fromClientsValids(p._2))}(selected_client);")
            )
          } else {
            toMasters.zipWithIndex.map(p =>
              VhdlStatement(s"master_valid_signal_${p._2}: process(selected_client, ${nodePorts(fromClientsValids(p._2))}, start_arrival)\n" +
              s"begin\n" +
              s"    if start_arrival = '1' then\n" +
              s"        ${nodePorts(p._1.valid)} <= ${nodePorts(fromClientsValids(p._2))}(selected_client);\n" +
              s"    else\n" +
              s"        ${nodePorts(p._1.valid)} <= '0';\n" +
              s"    end if;\n" +
              s"end process;\n"
              )
            )
          }
          val readySignals = if (basicDataOnly) {
            toMasters.zipWithIndex.map(p =>
              VhdlStatement(s"master_ready_signal_${p._2}: process(selected_client, ${nodePorts(p._1.ready)}, start_arrival)\n" +
                s"begin\n" +
                s"    ${nodePorts(fromClientsReadys(p._2))} <= (others => (others => '0'));\n" +
                s"    ${nodePorts(fromClientsReadys(p._2))}(selected_client) <= ${nodePorts(p._1.ready)};\n" +
                s"end process;\n"
              )
            )
          } else {
            toMasters.zipWithIndex.map(p =>
              VhdlStatement(s"master_ready_signal_${p._2}: process(selected_client, ${nodePorts(p._1.ready)}, start_arrival)\n" +
              s"begin\n" +
              s"    ${nodePorts(fromClientsReadys(p._2))} <= (others => (others => '0'));\n" +
              s"    if start_arrival = '1' then\n" +
              s"        ${nodePorts(fromClientsReadys(p._2))}(selected_client) <= ${nodePorts(p._1.ready)};\n" +
              s"    else\n" +
              s"        ${nodePorts(fromClientsReadys(p._2))}(selected_client) <= (others => '0');\n" +
              s"    end if;\n" +
              s"end process;\n"
              )
            )
          }
          val startCountInSignals = toMasters.zipWithIndex.map(p =>
            VhdlStatement(s"start_count_${p._2}_in_logic: process(selected_client, start_count_${p._2}, ${nodePorts(fromClientsValids(p._2))}, ${nodePorts(fromClientsLasts(p._2))}, ${nodePorts(p._1.ready)})\n" +
              s"    constant in_clients_last_all_one: std_logic_vector(${nodePorts(fromClientsLasts(p._2))}(0)'range) := (others => '1');\n" +
              s"begin\n" +
              s"    if ${nodePorts(fromClientsValids(p._2))}(selected_client) = '1' and ${nodePorts(fromClientsLasts(p._2))}(selected_client) = in_clients_last_all_one and ${nodePorts(p._1.ready)}(${nodePorts(p._1.ready)}'low) = '1' then\n" +
              s"        start_count_${p._2}_in <= start_count_${p._2} + 1;\n" +
              s"    else\n" +
              s"        start_count_${p._2}_in <= start_count_${p._2};\n" +
              s"    end if;\n" +
              s"end process;\n"
            )
          )
          val startCountSignals = toMasters.zipWithIndex.map(p =>
            VhdlStatement(s"start_count_${p._2}_logic: process(clk)\n" +
              s"    constant out_clients_last_all_one: std_logic_vector(${nodePorts(toClientsLast)}(0)'range) := (others => '1');\n" +
              s"    variable start_count_v: natural := 0;\n" +
              s"begin\n" +
              s"    if rising_edge(clk) then\n" +
              s"        if reset = '1' then\n" +
              s"            start_count_${p._2} <= 0;\n" +
              s"        else\n" +
              s"            start_count_v := start_count_${p._2}_in;\n" +
              s"            if ${nodePorts(fromMaster.valid)} = '1' and ${nodePorts(fromMaster.last)} = out_clients_last_all_one and ${nodePorts(toClientsReady)}(selected_client)(${nodePorts(p._1.ready)}'low) = '1' then\n" +
              s"                start_count_v := start_count_v - 1;\n" +
              s"            end if;\n" +
              s"            start_count_${p._2} <= start_count_v;\n" +
              s"        end if;\n" +
              s"    end if;\n" +
              s"end process;\n"
            )
          )
          Seq(allClientValid, oneRemainingItem) ++ dataSignals ++ lastSignals ++ validSignals ++ readySignals ++
            startCountInSignals ++ startCountSignals
        case _ => Seq()
      },
      // template
      template = node match {
        case b: BehaviourNodeT => b match {
          /*
           *************************************************************************************************************
           value/input generators
           *************************************************************************************************************
           */
          case BehaviourNodeConstantValue(pout, _, vType, _) =>
            Some(VhdlBehaviourTemplate(
              vType match {
                case LogicType() => "const_bit"
                case _ => "const_bit_vector"
              },
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid))
            ))
          case BehaviourNodeConstantBitVector(pout, _, vType, _) =>
            Some(VhdlBehaviourTemplate("const_bit_vector",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid))
            ))
          case BehaviourNodeCounterInteger(pout, _, _, _, dimensions, _, _) =>
            Some(VhdlBehaviourTemplate("counter",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready)),
              Map("natural_vector_type" -> VhdlConstantNaturalVector(dimensions.map(_.ae.evalInt)).vType)
            ))
          case BehaviourNodeCounterIntegerND(pout, _, _, _, dimensions, _) =>
            Some(VhdlBehaviourTemplate("counterND",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready)),
              Map("natural_vector_type" -> VhdlConstantNaturalVector(dimensions.map(_.ae.evalInt)).vType)
            ))
          case b @ BehaviourNodeRepeat(_, pout, _, _) =>
            Some(VhdlBehaviourTemplate("repeat",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_f_in_data" -> signals(b.function.pin.data), "port_f_in_last" -> signals(b.function.pin.last), "port_f_in_valid" -> signals(b.function.pin.valid), "port_f_in_ready" -> signals(b.function.pin.ready))
            ))
          case b @ BehaviourNodeRepeatHidden(_, pout, _, _) =>
            Some(VhdlBehaviourTemplate("repeat_hidden",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_f_in_data" -> signals(b.function.pin.data), "port_f_in_last" -> signals(b.function.pin.last), "port_f_in_valid" -> signals(b.function.pin.valid), "port_f_in_ready" -> signals(b.function.pin.ready))
            ))
          case b @ BehaviourNodeVectorGenerator(_, pout, _) =>
            Some(VhdlBehaviourTemplate("vector_generator",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_f_in_data" -> signals(b.function.pin.data), "port_f_in_last" -> signals(b.function.pin.last), "port_f_in_valid" -> signals(b.function.pin.valid), "port_f_in_ready" -> signals(b.function.pin.ready))
            ))
          case BehaviourNodeSink(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("sink",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeRegistered(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("registered",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_type" -> pin.dataType)
            ))
          case BehaviourNodeDelay(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("delay",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case b @ BehaviourNodeMarker(_, pout, _, _, _) =>
            Some(VhdlBehaviourTemplate("marker",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_f_in_data" -> signals(b.function.pin.data), "port_f_in_last" -> signals(b.function.pin.last), "port_f_in_valid" -> signals(b.function.pin.valid), "port_f_in_ready" -> signals(b.function.pin.ready))
            ))
          /*
           *************************************************************************************************************
           simple operations
           *************************************************************************************************************
           */
          case BehaviourNodeId(pin, pout, _) =>
            (pin, pout) match {
              case (pin: PortInHandshake, pout: PortOutHandshake) =>
                Some(VhdlBehaviourTemplate("id_hs",
                  Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                    "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                    "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready))
                ))
              case (pin: PortInRequest, pout: PortOutRequest) =>
                Some(VhdlBehaviourTemplate("id_req",
                  Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                    "port_in_data" -> signals(pin.data), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready), "port_out_data" -> signals(pout.data), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready)),
                ))
              case (pin: PortInResponse, pout: PortOutResponse) =>
                Some(VhdlBehaviourTemplate("id_rsp",
                  Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                    "port_in_data" -> signals(pin.data), "port_in_valid" -> signals(pin.valid), "port_out_data" -> signals(pout.data), "port_out_valid" -> signals(pout.valid)),
                ))
              case _ => throw VhdlCodeException("Node with id behaviour cannot be generated for these ports (different type maybe?): " + pin + " and " + pout)
            }
          case BehaviourNodeSigned(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("signed",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeUnsigned(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("unsigned",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeClipBankersRoundShift(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("clip_bankers_round_shift",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_tuple_type" -> pin.dataType, "data_word_type" -> pout.dataType)
            ))
          case BehaviourNodeBankersRoundInt(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate(
              pout.dataType match {
                case SignedIntType(_) => "bankers_round_signed_int"
                case IntType(_) => "bankers_round_int"
              },
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeClipInt(pin, pout, _) =>
            Some(VhdlBehaviourTemplate(
              pout.dataType match {
                case SignedIntType(_) => "clip_signed_int"
                case IntType(_) => "clip_int"
              },
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeAddInt(pin, pout, _) =>
            Some(VhdlBehaviourTemplate(
              pout.dataType match {
                case SignedIntType(_) => "add_signed_int"
                case IntType(_) => "add_int"
                case _ => ???
              },
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeAddFloat(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("add_float",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeSubInt(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("sub_int",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeMulInt(pin, pout, _) =>
            Some(VhdlBehaviourTemplate(
              pout.dataType match {
                case SignedIntType(_) => "mul_signed_int"
                case IntType(_) => "mul_int"
                case _ => ???
              },
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeMulFloat(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("mul_float",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeMinInt(pin, pout, _) =>
            pout.dataType match {
              case t: SignedIntTypeT =>
                Some(VhdlBehaviourTemplate("min_signed_int",
                  Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                    "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                    "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready))
                ))
              case _ =>
                Some(VhdlBehaviourTemplate("min_int",
                  Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                    "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                    "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready))
                ))
            }
          case BehaviourNodeMaxInt(pin, pout, _) =>
            pout.dataType match {
              case t: SignedIntTypeT =>
                Some(VhdlBehaviourTemplate("max_signed_int",
                  Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                    "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                    "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready))
                ))
              case _ =>
                Some(VhdlBehaviourTemplate("max_int",
                  Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                    "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                    "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready))
                ))
            }
          case BehaviourNodeConstrainedSum(pin, pout, _, _, _, _, _) =>
            Some(VhdlBehaviourTemplate("constrained_sum",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeSelect(pin, pout, selection, reduction, _) =>
            Some(VhdlBehaviourTemplate(
              if (reduction) "select_reduced" else "select",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_type" -> pout.dataType)
            ))
          /*
           *************************************************************************************************************
           complex operations (map, reduce, slide, ...)
           *************************************************************************************************************
           */
          case b@BehaviourNodeMapOrderedStream(_, pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("map_ordered_stream",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_from_f_data" -> signals(b.function.pin.data), "port_from_f_last" -> signals(b.function.pin.last), "port_from_f_valid" -> signals(b.function.pin.valid), "port_from_f_ready" -> signals(b.function.pin.ready),
                "port_to_f_data" -> signals(b.function.pouts.head.data), "port_to_f_last" -> signals(b.function.pouts.head.last), "port_to_f_valid" -> signals(b.function.pouts.head.valid), "port_to_f_ready" -> signals(b.function.pouts.head.ready),
                "delay_ctrl_last" -> signals(b.delayCtrlLastOut), "delay_ctrl_ready" -> signals(b.delayCtrlReadyOut)),
              Map("data_from_f_type" -> b.function.pin.dataType, "data_to_f_type" -> b.function.pouts.head.dataType)
            ))
          case b@BehaviourNodeMapSimpleOrderedStream(_, pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("map_simple_ordered_stream",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_f_in_data" -> signals(b.function.pin.data), "port_f_in_last" -> signals(b.function.pin.last), "port_f_in_valid" -> signals(b.function.pin.valid), "port_f_in_ready" -> signals(b.function.pin.ready),
                "port_f_out_data" -> signals(b.function.pouts.head.data), "port_f_out_last" -> signals(b.function.pouts.head.last), "port_f_out_valid" -> signals(b.function.pouts.head.valid), "port_f_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case b@BehaviourNodeMapUnorderedStream(_, pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("map_unordered_stream",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_f_in_data" -> signals(b.function.pin.data), "port_f_in_last" -> signals(b.function.pin.last), "port_f_in_valid" -> signals(b.function.pin.valid), "port_f_in_ready" -> signals(b.function.pin.ready),
                "port_f_out_data" -> signals(b.function.pouts.head.data), "port_f_out_last" -> signals(b.function.pouts.head.last), "port_f_out_valid" -> signals(b.function.pouts.head.valid), "port_f_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case b@BehaviourNodeMapBufferedUnorderedStream(_, pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("map_buffered_unordered_stream",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_f_in_data" -> signals(b.function.pin.data), "port_f_in_last" -> signals(b.function.pin.last), "port_f_in_valid" -> signals(b.function.pin.valid), "port_f_in_ready" -> signals(b.function.pin.ready),
                "port_f_out_data" -> signals(b.function.pouts.head.data), "port_f_out_last" -> signals(b.function.pouts.head.last), "port_f_out_valid" -> signals(b.function.pouts.head.valid), "port_f_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case BehaviourNodeMapStreamDelay(pin, pout, controlLastPin, controlReadyPin, _) =>
            Some(VhdlBehaviourTemplate("map_stream_delay",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_in_control_last" -> signals(controlLastPin),
                "port_in_control_ready" -> signals(controlReadyPin))
            ))
          case b@BehaviourNodeMapAsyncOrderedStream(_, pins, pout, _, _) =>
            val sortedFunPorts = b.function.pouts.sortBy(p => p.portOrder)
            val inputMap = pins.zipWithIndex
              .flatMap(p => Seq(
                s"port_in_${p._2}_data" -> signals(p._1.data),
                s"port_in_${p._2}_last" -> signals(p._1.last),
                s"port_in_${p._2}_valid" -> signals(p._1.valid),
                s"port_in_${p._2}_ready" -> signals(p._1.ready)
              ))
              .toMap
            val fOutputMap = sortedFunPorts.zipWithIndex
              .flatMap(p => Seq(
                s"port_f_out_${p._2}_data" -> signals(p._1.data),
                s"port_f_out_${p._2}_last" -> signals(p._1.last),
                s"port_f_out_${p._2}_valid" -> signals(p._1.valid),
                s"port_f_out_${p._2}_ready" -> signals(p._1.ready)
              ))
              .toMap
            Some(VhdlBehaviourTemplate("map_async_ordered_stream",
              inputMap ++ fOutputMap ++ Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_f_in_data" -> signals(b.function.pin.data), "port_f_in_last" -> signals(b.function.pin.last), "port_f_in_valid" -> signals(b.function.pin.valid), "port_f_in_ready" -> signals(b.function.pin.ready))
            ))
          case b@BehaviourNodeFoldOrderedStream(_, pin, pout, dimensionsIn, bufferDataType, _) =>
            // Please check the explanation in Reduce about the negation.
            val sortedFunPorts = b.function.pouts.sortBy(p => -p.portOrder)
              Some(VhdlBehaviourTemplate(
                (dimensionsIn.length, bufferDataType) match {
                  case (1, SignedIntType(_)) => "fold_signed_int_ordered_stream"
                  case (1, IntType(_)) => "fold_int_ordered_stream"
                  case (1, _) => "fold_ordered_stream"
                  case (2, _) => "fold_ordered_stream_2D"
                  case _ => ??? // unsupported for now!
                },
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_f_out_acc_data" -> signals(sortedFunPorts.head.data), "port_f_out_acc_last" -> signals(sortedFunPorts.head.last), "port_f_out_acc_valid" -> signals(sortedFunPorts.head.valid), "port_f_out_acc_ready" -> signals(sortedFunPorts.head.ready),
                "port_f_out_data_data" -> signals(sortedFunPorts(1).data), "port_f_out_data_last" -> signals(sortedFunPorts(1).last), "port_f_out_data_valid" -> signals(sortedFunPorts(1).valid), "port_f_out_data_ready" -> signals(sortedFunPorts(1).ready),
                "port_f_in_data" -> signals(b.function.pin.data), "port_f_in_last" -> signals(b.function.pin.last), "port_f_in_valid" -> signals(b.function.pin.valid), "port_f_in_ready" -> signals(b.function.pin.ready)),
              Map("data_word_type" -> pout.dataType)
            ))
          case b@BehaviourNodeReduceOrderedStream(f, pinInitialValue, pin, pout, _) =>
            // The negation is important because the function's port order in Reduce is the reverse of the inputs.
            // Example: Reduce(Lambda(p0, Lambda(p1, body)), initValue, input)
            // p0 is tied with input and p1 is tied with initValue!
            val sortedFunPorts = b.function.pouts.sortBy(p => -p.portOrder)
            Some(VhdlBehaviourTemplate(
              f.graph.nodes.head match{
                case BehaviourNodeWriteSync(_, _, _, _, _) => "reduce_ordered_stream_pipelined"
                case _ => "reduce_ordered_stream"
              },
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_init_data" -> signals(pinInitialValue.data), "port_in_init_last" -> signals(pinInitialValue.last), "port_in_init_valid" -> signals(pinInitialValue.valid), "port_in_init_ready" -> signals(pinInitialValue.ready),
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_f_out_acc_data" -> signals(sortedFunPorts.head.data), "port_f_out_acc_last" -> signals(sortedFunPorts.head.last), "port_f_out_acc_valid" -> signals(sortedFunPorts.head.valid), "port_f_out_acc_ready" -> signals(sortedFunPorts.head.ready),
                "port_f_out_data_data" -> signals(sortedFunPorts(1).data), "port_f_out_data_last" -> signals(sortedFunPorts(1).last), "port_f_out_data_valid" -> signals(sortedFunPorts(1).valid), "port_f_out_data_ready" -> signals(sortedFunPorts(1).ready),
                "port_f_in_data" -> signals(b.function.pin.data), "port_f_in_last" -> signals(b.function.pin.last), "port_f_in_valid" -> signals(b.function.pin.valid), "port_f_in_ready" -> signals(b.function.pin.ready)),
              Map("data_word_type" -> pout.dataType)
            ))
          case BehaviourNodeSlideOrderedStream(pin, pout, _, _, _) =>
            Some(VhdlBehaviourTemplate("slide_ordered_stream",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_type" -> pin.dataType)
            ))
          case b@BehaviourNodeAlternate(_, _, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("alternate",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_to_f1_data" -> signals(b.function1.pouts.head.data), "port_to_f1_last" -> signals(b.function1.pouts.head.last), "port_to_f1_valid" -> signals(b.function1.pouts.head.valid), "port_to_f1_ready" -> signals(b.function1.pouts.head.ready),
                "port_from_f1_data" -> signals(b.function1.pin.data), "port_from_f1_last" -> signals(b.function1.pin.last), "port_from_f1_valid" -> signals(b.function1.pin.valid), "port_from_f1_ready" -> signals(b.function1.pin.ready),
                "port_to_f2_data" -> signals(b.function2.pouts.head.data), "port_to_f2_last" -> signals(b.function2.pouts.head.last), "port_to_f2_valid" -> signals(b.function2.pouts.head.valid), "port_to_f2_ready" -> signals(b.function2.pouts.head.ready),
                "port_from_f2_data" -> signals(b.function2.pin.data), "port_from_f2_last" -> signals(b.function2.pin.last), "port_from_f2_valid" -> signals(b.function2.pin.valid), "port_from_f2_ready" -> signals(b.function2.pin.ready)),
              Map("data_word_type" -> pin.dataType)
            ))
          case b@BehaviourNodeIfelse(_, _, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("ifelse",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_to_f1_data" -> signals(b.function1.pouts.head.data), "port_to_f1_last" -> signals(b.function1.pouts.head.last), "port_to_f1_valid" -> signals(b.function1.pouts.head.valid), "port_to_f1_ready" -> signals(b.function1.pouts.head.ready),
                "port_from_f1_data" -> signals(b.function1.pin.data), "port_from_f1_last" -> signals(b.function1.pin.last), "port_from_f1_valid" -> signals(b.function1.pin.valid), "port_from_f1_ready" -> signals(b.function1.pin.ready),
                "port_to_f2_data" -> signals(b.function2.pouts.head.data), "port_to_f2_last" -> signals(b.function2.pouts.head.last), "port_to_f2_valid" -> signals(b.function2.pouts.head.valid), "port_to_f2_ready" -> signals(b.function2.pouts.head.ready),
                "port_from_f2_data" -> signals(b.function2.pin.data), "port_from_f2_last" -> signals(b.function2.pin.last), "port_from_f2_valid" -> signals(b.function2.pin.valid), "port_from_f2_ready" -> signals(b.function2.pin.ready)),
              Map("data_word_tuple_type" -> pin.dataType, "data_word_type" -> pout.dataType)
            ))
          /*
           *************************************************************************************************************
           data type conversions / rearrangement
           *************************************************************************************************************
           */
          case BehaviourNodeConversion(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("id_hs",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeResizeInteger(pin, pout, _) =>
            Some(VhdlBehaviourTemplate(
              pin.dataType match {
                case SignedIntType(_) => "id_signed_resize"
                case IntType(_) => "id_resize"
                case _ => ???
              },
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeOrderedStreamToItem(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("id_stm1",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeVectorToTuple(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("vector_to_tuple",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeVectorToOrderedStream(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("vector_to_ordered_stream",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeOrderedStreamToVector(pin, pout, _) =>
            Some(VhdlBehaviourTemplate(
              pin.dataType match {
                case ot: OrderedStreamTypeT if ot.len.ae.evalInt == 1 => "ordered_stream_to_vector_stm1"
                case _ => "ordered_stream_to_vector"
              },
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_vector_type" -> pout.dataType, "data_word_type" -> pin.dataType)
            ))
          case BehaviourNodeIndexUnorderedStream(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("index_unordered_stream",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeUnorderedStreamToOrderedStream(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("unordered_stream_to_ordered_stream",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_type" -> pout.dataType)
            ))
          case BehaviourNodeOrderedStreamToUnorderedStream(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("ordered_stream_to_unordered_stream",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeTuple(pins, pout, _) =>
            val inputMap = pins.zipWithIndex
              .flatMap(p => Seq(
                s"port_in_${p._2}_data" -> signals(p._1.data),
                s"port_in_${p._2}_last" -> signals(p._1.last),
                s"port_in_${p._2}_valid" -> signals(p._1.valid),
                s"port_in_${p._2}_ready" -> signals(p._1.ready)
              ))
              .toMap
            Some(VhdlBehaviourTemplate("tuple",
              inputMap ++ Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready))
            ))
          case BehaviourNodeFlipTuple(pin, pout, n, _) =>
            Some(VhdlBehaviourTemplate("flip_tuple",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeZipStream(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("zip_stream",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeZipVector(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("zip_vector",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeSplitOrderedStream(pin, pout, _, _, _) =>
            Some(VhdlBehaviourTemplate("split_ordered_stream",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeJoinOrderedStream(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("join_ordered_stream",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeJoinUnorderedStream(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("join_unordered_stream",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeJoinVector(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("join_vector",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeDropOrderedStream(pin, pout, _, _, _, _) =>
            Some(VhdlBehaviourTemplate("drop_ordered_stream",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_type" -> pin.dataType)
            ))
          case BehaviourNodeDropVector(pin, pout, _, _, _) =>
            Some(VhdlBehaviourTemplate("drop_vector",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_vector_type" -> pin.dataType, "data_word_vector_slice_type" -> pout.dataType)
            ))
          case BehaviourNodeConcatOrderedStream(pin1, pin2, pout, _, _) =>
            Some(VhdlBehaviourTemplate("concat_ordered_stream",
              VhdlPortMap.simpleTwoPortMap(pin1, pin2, pout, signals)
            ))
          case BehaviourNodeConcatVector(pin, pout, lowDataType, highDataType, wordType, _) =>
            Some(VhdlBehaviourTemplate("concat_vector",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_vector_slice_low_type" -> lowDataType, "data_word_vector_slice_high_type" -> highDataType, "data_word_vector_type" -> pout.dataType, "data_word_type" -> wordType)
            ))
          case BehaviourNodeUpdateVector(pin, pout, _) =>
            Some(VhdlBehaviourTemplate("update_vector",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case BehaviourNodeRotateVector(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("rotate_vector",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_type" -> pout.dataType.asInstanceOf[VectorTypeT].et)
            ))
          case BehaviourNodePermuteVector(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("permute_vector",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_type" -> pin.dataType)
            ))
          case BehaviourNodeSlideVector(pin, pout, _, _, _) =>
            Some(VhdlBehaviourTemplate("slide_vector",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals),
              Map("data_word_type" -> pin.dataType)
            ))
          /*
           *************************************************************************************************************
           memory related behaviour
           *************************************************************************************************************
           */
          case b@BehaviourNodeReadSync(_, pinBaseAddr, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("read_sync",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_baseaddr_data" -> signals(pinBaseAddr.data), "port_in_baseaddr_last" -> signals(pinBaseAddr.last), "port_in_baseaddr_valid" -> signals(pinBaseAddr.valid), "port_in_baseaddr_ready" -> signals(pinBaseAddr.ready),
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_last" -> signals(b.function.pin.last), "port_mem_in_valid" -> signals(b.function.pin.valid), "port_mem_in_ready" -> signals(b.function.pin.ready),
                "port_mem_out_data" -> signals(b.function.pouts.head.data), "port_mem_out_last" -> signals(b.function.pouts.head.last), "port_mem_out_valid" -> signals(b.function.pouts.head.valid), "port_mem_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case b@BehaviourNodeWriteSync(_, pinBaseAddr, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("write_sync",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_baseaddr_data" -> signals(pinBaseAddr.data), "port_in_baseaddr_last" -> signals(pinBaseAddr.last), "port_in_baseaddr_valid" -> signals(pinBaseAddr.valid), "port_in_baseaddr_ready" -> signals(pinBaseAddr.ready),
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_last" -> signals(b.function.pin.last), "port_mem_in_valid" -> signals(b.function.pin.valid), "port_mem_in_ready" -> signals(b.function.pin.ready),
                "port_mem_out_data" -> signals(b.function.pouts.head.data), "port_mem_out_last" -> signals(b.function.pouts.head.last), "port_mem_out_valid" -> signals(b.function.pouts.head.valid), "port_mem_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case b@BehaviourNodeReadSyncMemoryController(_, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("read_sync_memory_controller",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_last" -> signals(b.function.pin.last), "port_mem_in_valid" -> signals(b.function.pin.valid), "port_mem_in_ready" -> signals(b.function.pin.ready),
                "port_mem_out_data" -> signals(b.function.pouts.head.data), "port_mem_out_last" -> signals(b.function.pouts.head.last), "port_mem_out_valid" -> signals(b.function.pouts.head.valid), "port_mem_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case b@BehaviourNodeWriteSyncMemoryController(_, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("write_sync_memory_controller",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_last" -> signals(b.function.pin.last), "port_mem_in_valid" -> signals(b.function.pin.valid), "port_mem_in_ready" -> signals(b.function.pin.ready),
                "port_mem_out_data" -> signals(b.function.pouts.head.data), "port_mem_out_last" -> signals(b.function.pouts.head.last), "port_mem_out_valid" -> signals(b.function.pouts.head.valid), "port_mem_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case BehaviourNodeBlockRam(pin, pout, entries, _) =>
            Some(VhdlBehaviourTemplate(if (entries.ae.evalInt == 1) "ram_buffer_stm1" else "ram_buffer",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          case b@BehaviourNodeReadSyncBanked(_, pinBaseAddr, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("read_sync_banked",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_baseaddr_data" -> signals(pinBaseAddr.data), "port_in_baseaddr_last" -> signals(pinBaseAddr.last), "port_in_baseaddr_valid" -> signals(pinBaseAddr.valid), "port_in_baseaddr_ready" -> signals(pinBaseAddr.ready),
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_last" -> signals(b.function.pin.last), "port_mem_in_valid" -> signals(b.function.pin.valid), "port_mem_in_ready" -> signals(b.function.pin.ready),
                "port_mem_out_data" -> signals(b.function.pouts.head.data), "port_mem_out_last" -> signals(b.function.pouts.head.last), "port_mem_out_valid" -> signals(b.function.pouts.head.valid), "port_mem_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case b@BehaviourNodeWriteSyncBanked(_, pinBaseAddr, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("write_sync_banked",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_baseaddr_data" -> signals(pinBaseAddr.data), "port_in_baseaddr_last" -> signals(pinBaseAddr.last), "port_in_baseaddr_valid" -> signals(pinBaseAddr.valid), "port_in_baseaddr_ready" -> signals(pinBaseAddr.ready),
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_last" -> signals(b.function.pin.last), "port_mem_in_valid" -> signals(b.function.pin.valid), "port_mem_in_ready" -> signals(b.function.pin.ready),
                "port_mem_out_data" -> signals(b.function.pouts.head.data), "port_mem_out_last" -> signals(b.function.pouts.head.last), "port_mem_out_valid" -> signals(b.function.pouts.head.valid), "port_mem_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case b@BehaviourNodeReadSyncBankedMemoryController(_, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("read_sync_banked_memory_controller",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_last" -> signals(b.function.pin.last), "port_mem_in_valid" -> signals(b.function.pin.valid), "port_mem_in_ready" -> signals(b.function.pin.ready),
                "port_mem_out_data" -> signals(b.function.pouts.head.data), "port_mem_out_last" -> signals(b.function.pouts.head.last), "port_mem_out_valid" -> signals(b.function.pouts.head.valid), "port_mem_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case b@BehaviourNodeWriteSyncBankedMemoryController(_, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("write_sync_banked_memory_controller",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_last" -> signals(b.function.pin.last), "port_mem_in_valid" -> signals(b.function.pin.valid), "port_mem_in_ready" -> signals(b.function.pin.ready),
                "port_mem_out_data" -> signals(b.function.pouts.head.data), "port_mem_out_last" -> signals(b.function.pouts.head.last), "port_mem_out_valid" -> signals(b.function.pouts.head.valid), "port_mem_out_ready" -> signals(b.function.pouts.head.ready))
            ))
          case BehaviourNodeBankedBlockRam(pin, pout, _, _, _) =>
            Some(VhdlBehaviourTemplate("banked_ram_buffer",
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          //TODO: Change made here
          case b@BehaviourNodeOnBoardRam(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("on_board_ram_buffer",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "bank_waitrequest" -> signals(b.onBoardRamBankPort.waitRequest), "bank_read_data" -> signals(b.onBoardRamBankPort.readData), "bank_read_valid" -> signals(b.onBoardRamBankPort.readDataValid),
                "bank_burstcount" -> signals(b.onBoardRamBankPort.burstCount), "bank_write_data" -> signals(b.onBoardRamBankPort.writeData), "bank_address" -> signals(b.onBoardRamBankPort.address),
                "bank_write_enable" -> signals(b.onBoardRamBankPort.write), "bank_read_enable" -> signals(b.onBoardRamBankPort.read), "bank_byteenable" -> signals(b.onBoardRamBankPort.byteEnable),
//                "port_out_write_req_count" -> signals(b.onBoardRamBankPort.write_req_count), "port_out_write_rsp_count" -> signals(b.onBoardRamBankPort.write_rsp_count),
//                "port_out_read_req_count" -> signals(b.onBoardRamBankPort.read_req_count), "port_out_read_rsp_count" -> signals(b.onBoardRamBankPort.read_rsp_count),
//                "port_out_obr_write_req_count" -> signals(b.onBoardRamBankPort.obr_write_req_count), "port_out_obr_read_req_count" -> signals(b.onBoardRamBankPort.obr_read_req_count), "port_out_obr_read_rsp_count" -> signals(b.onBoardRamBankPort.obr_read_rsp_count),
//                "port_out_write_req_id_req_sum" -> signals(b.onBoardRamBankPort.write_req_id_req_sum), "port_out_write_req_id_rsp_sum" -> signals(b.onBoardRamBankPort.write_req_id_rsp_sum),
//                "port_out_read_req_id_req_sum" -> signals(b.onBoardRamBankPort.read_req_id_req_sum), "port_out_read_req_id_rsp_sum" -> signals(b.onBoardRamBankPort.read_req_id_rsp_sum),
//                "port_out_client_read_req_count" -> signals(b.onBoardRamBankPort.client_read_req_count), "port_out_idx_buffer_count" -> signals(b.onBoardRamBankPort.idx_buffer_count),
//                "port_out_information" -> signals(b.onBoardRamBankPort.information),
                "port_out_num_cycles_writes" -> signals(b.onBoardRamBankPort.numCyclesWrites),
                "port_out_num_cycles_reads" -> signals(b.onBoardRamBankPort.numCyclesReads),
                "port_in_data" -> signals(pin.data), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_valid" -> signals(pout.valid))
            ))

          case b@BehaviourNodeReadLocalMemoryController(_, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("read_local_memory_controller",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_valid" -> signals(pout.valid),
                "port_in_rsp_data" -> signals(b.function.pin.data), "port_in_rsp_valid" -> signals(b.function.pin.valid),
                "port_out_req_data" -> signals(b.function.pout.data), "port_out_req_valid" -> signals(b.function.pout.valid), "port_in_mem_ready" -> signals(b.function.pout.ready))
            ))

          case b@BehaviourNodeWriteLocalMemoryController(_, pin, pout, _) =>
            Some(VhdlBehaviourTemplate("write_local_memory_controller",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_valid" -> signals(pout.valid),
                "port_in_rsp_data" -> signals(b.function.pin.data), "port_in_rsp_valid" -> signals(b.function.pin.valid),
                "port_out_req_data" -> signals(b.function.pout.data), "port_out_req_valid" -> signals(b.function.pout.valid), "port_in_mem_ready" -> signals(b.function.pout.ready))
            ))

          case b@BehaviourNodeReadAsync(_, pinBaseAddr, pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("read_async",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_baseaddr_data" -> signals(pinBaseAddr.data), "port_in_baseaddr_last" -> signals(pinBaseAddr.last), "port_in_baseaddr_valid" -> signals(pinBaseAddr.valid), "port_in_baseaddr_ready" -> signals(pinBaseAddr.ready),
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_valid" -> signals(b.function.pin.valid),
                "port_mem_out_data" -> signals(b.function.pout.data), "port_mem_out_valid" -> signals(b.function.pout.valid), "port_mem_out_ready" -> signals(b.function.pout.ready))
            ))
          case b@BehaviourNodeReadAsyncOrdered(_, pinBaseAddr, pin, pout, numElements, _) =>
            Some(VhdlBehaviourTemplate(if (numElements.ae.evalInt == 1) "read_async_ordered_stm1" else "read_async_ordered",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_baseaddr_data" -> signals(pinBaseAddr.data), "port_in_baseaddr_last" -> signals(pinBaseAddr.last), "port_in_baseaddr_valid" -> signals(pinBaseAddr.valid), "port_in_baseaddr_ready" -> signals(pinBaseAddr.ready),
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_valid" -> signals(b.function.pin.valid),
                "port_mem_out_data" -> signals(b.function.pout.data), "port_mem_out_valid" -> signals(b.function.pout.valid), "port_mem_out_ready" -> signals(b.function.pout.ready))
            ))
          case b@BehaviourNodeReadAsyncOrderedOutOfBounds(_, pinBaseAddr, pin, pout, _, _, _, _) =>
            Some(VhdlBehaviourTemplate("read_async_ordered_out_of_bounds",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_baseaddr_data" -> signals(pinBaseAddr.data), "port_in_baseaddr_last" -> signals(pinBaseAddr.last), "port_in_baseaddr_valid" -> signals(pinBaseAddr.valid), "port_in_baseaddr_ready" -> signals(pinBaseAddr.ready),
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_valid" -> signals(b.function.pin.valid),
                "port_mem_out_data" -> signals(b.function.pout.data), "port_mem_out_valid" -> signals(b.function.pout.valid), "port_mem_out_ready" -> signals(b.function.pout.ready)),
              Map("data_word_type" -> pout.dataType.asInstanceOf[OrderedStreamTypeT].et, "data_word_stream_type" -> pin.dataType.asInstanceOf[OrderedStreamTypeT].et)
            ))
          case b@BehaviourNodeWriteAsync(_, pinBaseAddr, pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("write_async",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_baseaddr_data" -> signals(pinBaseAddr.data), "port_in_baseaddr_last" -> signals(pinBaseAddr.last), "port_in_baseaddr_valid" -> signals(pinBaseAddr.valid), "port_in_baseaddr_ready" -> signals(pinBaseAddr.ready),
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_valid" -> signals(b.function.pin.valid),
                "port_mem_out_data" -> signals(b.function.pout.data), "port_mem_out_valid" -> signals(b.function.pout.valid), "port_mem_out_ready" -> signals(b.function.pout.ready)),
            ))
          case b@BehaviourNodeWriteAsyncOutOfBounds(_, pinBaseAddr, pin, pout, _, _, _) =>
            Some(VhdlBehaviourTemplate("write_async_out_of_bounds",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_baseaddr_data" -> signals(pinBaseAddr.data), "port_in_baseaddr_last" -> signals(pinBaseAddr.last), "port_in_baseaddr_valid" -> signals(pinBaseAddr.valid), "port_in_baseaddr_ready" -> signals(pinBaseAddr.ready),
                "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
                "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready),
                "port_mem_in_data" -> signals(b.function.pin.data), "port_mem_in_valid" -> signals(b.function.pin.valid),
                "port_mem_out_data" -> signals(b.function.pout.data), "port_mem_out_valid" -> signals(b.function.pout.valid), "port_mem_out_ready" -> signals(b.function.pout.ready)),
              Map("data_word_type" -> pout.dataType, "data_word_stream_type" -> pin.dataType.asInstanceOf[OrderedStreamTypeT].et.asInstanceOf[NamedTupleTypeT].namedTypes.last._2)
            ))
          case b@BehaviourNodeReadHostMemoryController(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("read_host_memory_controller",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready), "port_out_data" -> signals(pout.data), "port_out_valid" -> signals(pout.valid),
                "port_in_mem_ready" -> signals(b.memReadPort.memReady),
                "port_out_req_address" -> signals(b.memReadPort.reqAddr), "port_out_req_mdata" -> signals(b.memReadPort.reqMData), "port_out_req_valid" -> signals(b.memReadPort.reqValid), "port_out_req_total" -> signals(b.memReadPort.reqTotal), "port_out_req_pending" -> signals(b.memReadPort.reqPending), "port_in_req_almFull" -> signals(b.memReadPort.reqALMFull),
                "port_in_rsp_data" -> signals(b.memReadPort.rspData), "port_in_rsp_mdata" -> signals(b.memReadPort.rspMData), "port_in_rsp_valid" -> signals(b.memReadPort.rspValid))
            ))
          case b@BehaviourNodeWriteHostMemoryController(pin, pout, _, _) =>
            Some(VhdlBehaviourTemplate("write_host_memory_controller",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(pin.data), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready), "port_out_data" -> signals(pout.data), "port_out_valid" -> signals(pout.valid),
                "port_in_mem_ready" -> signals(b.memWritePort.memReady),
                "port_out_req_address" -> signals(b.memWritePort.reqAddr), "port_out_req_mdata" -> signals(b.memWritePort.reqMData), "port_out_req_data" -> signals(b.memWritePort.reqData), "port_out_req_valid" -> signals(b.memWritePort.reqValid), "port_out_req_total" -> signals(b.memWritePort.reqTotal), "port_out_req_pending" -> signals(b.memWritePort.reqPending), "port_in_req_almFull" -> signals(b.memWritePort.reqALMFull),
                "port_in_rsp_mdata" -> signals(b.memWritePort.rspMData), "port_in_rsp_valid" -> signals(b.memWritePort.rspValid))
            ))
          /*
           ***********************************************************************************************************************
           device specific behaviour
           ***********************************************************************************************************************
           */
          case BehaviourNodeMul2AddInt(pin, pout, _) =>
            Some(VhdlBehaviourTemplate(
              pout.dataType match {
                case SignedIntType(_) => "mul2add_signed_int"
                case IntType(_) => "mul2add_int"
                case _ => ???
              },
              VhdlPortMap.simpleOnePortMap(pin, pout, signals)
            ))
          /*
           *************************************************************************************************************
           general purpose behaviour
           *************************************************************************************************************
           */
          case BehaviourNodeTestbench(uut, _) =>
            //TODO use? ports.flatMap(_.flatten).map(signals)
            Some(VhdlBehaviourTemplate("testbench",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET)
            ))
          case BehaviourNodeDistributor(fromMaster, toClientsData, toClientsLast, toClientsValid, toClientsReady, _) =>
            Some(VhdlBehaviourTemplate("distributor",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(fromMaster.data), "port_in_last" -> signals(fromMaster.last), "port_in_valid" -> signals(fromMaster.valid), "port_in_ready" -> signals(fromMaster.ready),
                "port_out_clients_data" -> signals(toClientsData), "port_out_clients_last" -> signals(toClientsLast), "port_out_clients_valid" -> signals(toClientsValid), "port_out_clients_ready" -> signals(toClientsReady))
            ))
          case BehaviourNodeCollectorArbiter(fromClientsData, fromClientsLast, fromClientsValid, fromClientsReady, toMaster, strategy, _) =>
            // TODO strategy
            Some(VhdlBehaviourTemplate("collector_arbiter",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_clients_data" -> signals(fromClientsData), "port_in_clients_last" -> signals(fromClientsLast), "port_in_clients_valid" -> signals(fromClientsValid), "port_in_clients_ready" -> signals(fromClientsReady),
                "port_out_data" -> signals(toMaster.data), "port_out_last" -> signals(toMaster.last), "port_out_valid" -> signals(toMaster.valid), "port_out_ready" -> signals(toMaster.ready))
            ))
          case BehaviourNodeVectorFork(fromMaster, toClientsData, toClientsLast, toClientsValid, toClientsReady, _) =>
            Some(VhdlBehaviourTemplate("vector_fork",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(fromMaster.data), "port_in_last" -> signals(fromMaster.last), "port_in_valid" -> signals(fromMaster.valid), "port_in_ready" -> signals(fromMaster.ready),
                "port_out_clients_data" -> signals(toClientsData), "port_out_clients_last" -> signals(toClientsLast), "port_out_clients_valid" -> signals(toClientsValid), "port_out_clients_ready" -> signals(toClientsReady))
            ))
          case BehaviourNodeVectorJoin(fromClientsData, fromClientsLast, fromClientsValid, fromClientsReady, toMaster, _) =>
            Some(VhdlBehaviourTemplate("vector_join",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_clients_data" -> signals(fromClientsData), "port_in_clients_last" -> signals(fromClientsLast), "port_in_clients_valid" -> signals(fromClientsValid), "port_in_clients_ready" -> signals(fromClientsReady),
                "port_out_data" -> signals(toMaster.data), "port_out_last" -> signals(toMaster.last), "port_out_valid" -> signals(toMaster.valid), "port_out_ready" -> signals(toMaster.ready))
            ))
          case BehaviourNodeArbiterSyncFunction(toMasters, fromMaster, toClientsData, toClientsLast, toClientsValid, toClientsReady, fromClientsDatas, fromClientsLasts, fromClientsValids, fromClientsReadys, strategy, _) if
            (toMasters.length == 1 && toMasters.head.dataType.isInstanceOf[BasicDataTypeT]) =>
            // TODO strategy
            // Handle special case for RAM and scalar function.
            Some(VhdlBehaviourTemplate("arbiter_sync_function",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(fromMaster.data), "port_in_last" -> signals(fromMaster.last), "port_in_valid" -> signals(fromMaster.valid), "port_in_ready" -> signals(fromMaster.ready),
                "port_out_data" -> signals(toMasters.head.data), "port_out_last" -> signals(toMasters.head.last), "port_out_valid" -> signals(toMasters.head.valid), "port_out_ready" -> signals(toMasters.head.ready),
                "port_in_clients_data" -> signals(fromClientsDatas.head), "port_in_clients_last" -> signals(fromClientsLasts.head), "port_in_clients_valid" -> signals(fromClientsValids.head), "port_in_clients_ready" -> signals(fromClientsReadys.head),
                "port_out_clients_data" -> signals(toClientsData), "port_out_clients_last" -> signals(toClientsLast), "port_out_clients_valid" -> signals(toClientsValid), "port_out_clients_ready" -> signals(toClientsReady))
            ))
          case BehaviourNodeArbiterSyncFunction(toMasters, fromMaster, toClientsData, toClientsLast, toClientsValid, toClientsReady, fromClientsDatas, fromClientsLasts, fromClientsValids, fromClientsReadys, strategy, _) =>
            val toMasterMap = toMasters.zipWithIndex.flatMap(p => Seq(
              s"port_out_${p._2}_data" -> signals(p._1.data),
              s"port_out_${p._2}_last" -> signals(p._1.last),
              s"port_out_${p._2}_valid" -> signals(p._1.valid),
              s"port_out_${p._2}_ready" -> signals(p._1.ready)
            )).toMap
            val fromClientsDataMap = fromClientsDatas.zipWithIndex.map(p => s"port_in_clients_${p._2}_data" -> signals(p._1)).toMap
            val fromClientsLastMap = fromClientsLasts.zipWithIndex.map(p => s"port_in_clients_${p._2}_last" -> signals(p._1)).toMap
            val fromClientsValidMap = fromClientsValids.zipWithIndex.map(p => s"port_in_clients_${p._2}_valid" -> signals(p._1)).toMap
            val fromClientsReadyMap = fromClientsReadys.zipWithIndex.map(p => s"port_in_clients_${p._2}_ready" -> signals(p._1)).toMap
            Some(VhdlBehaviourTemplate(
              if(strategy == OrderedScheduling()) {
                "arbiter_sync_function_ordered"
              } else if(toMasters.forall(_.dataType.isInstanceOf[BasicDataTypeT])) {
                "arbiter_sync_function_basic_data"
              }else {
                "arbiter_sync_function_general_heavy_load"
              },
              toMasterMap ++ fromClientsDataMap ++ fromClientsLastMap ++ fromClientsValidMap ++ fromClientsReadyMap ++
                Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_in_data" -> signals(fromMaster.data), "port_in_last" -> signals(fromMaster.last), "port_in_valid" -> signals(fromMaster.valid), "port_in_ready" -> signals(fromMaster.ready),
                "port_out_clients_data" -> signals(toClientsData), "port_out_clients_last" -> signals(toClientsLast), "port_out_clients_valid" -> signals(toClientsValid), "port_out_clients_ready" -> signals(toClientsReady))
            ))
          case BehaviourNodeArbiterAsyncFunction(fromMasterRequest, fromMasterResponse, toClientsReqData, toClientsReqValid, toClientsReqReady, toClientsRspData, toClientsRspValid, schedulingStrategy, _) =>
            // TODO strategy
            Some(VhdlBehaviourTemplate("arbiter_async_function",
              Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
                "port_out_data" -> signals(fromMasterRequest.data), "port_out_valid" -> signals(fromMasterRequest.valid), "port_out_ready" -> signals(fromMasterRequest.ready),
                "port_in_data" -> signals(fromMasterResponse.data), "port_in_valid" -> signals(fromMasterResponse.valid),
                "port_in_clients_data" -> signals(toClientsReqData), "port_in_clients_valid" -> signals(toClientsReqValid), "port_in_clients_ready" -> signals(toClientsReqReady),
                "port_out_clients_data" -> signals(toClientsRspData), "port_out_clients_valid" -> signals(toClientsRspValid))
            ))
        }
        case _ => None
      }
    )
  }
}
