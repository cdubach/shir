package backend.hdl.vhdl

import backend.hdl.graph.{PortInHandshake, PortOutHandshake, PortSimpleT}

import scala.collection.immutable.ListMap

/**
 * Constructor that creates port assignments in VHDLGenerator to avoid code size limitation errors!
 */

object VhdlPortMap {
  def simpleOnePortMap(pin: PortInHandshake, pout: PortOutHandshake, signals: Map[PortSimpleT, VhdlSignalT]): Map[String, VhdlSignalT] = {
    Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
      "port_in_data" -> signals(pin.data), "port_in_last" -> signals(pin.last), "port_in_valid" -> signals(pin.valid), "port_in_ready" -> signals(pin.ready),
      "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready))
  }

  def simpleTwoPortMap(pin1: PortInHandshake, pin2: PortInHandshake, pout: PortOutHandshake, signals: Map[PortSimpleT, VhdlSignalT]): Map[String, VhdlSignalT] = {
    Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
      "port_in_1_data" -> signals(pin1.data), "port_in_1_last" -> signals(pin1.last), "port_in_1_valid" -> signals(pin1.valid), "port_in_1_ready" -> signals(pin1.ready),
      "port_in_2_data" -> signals(pin2.data), "port_in_2_last" -> signals(pin2.last), "port_in_2_valid" -> signals(pin2.valid), "port_in_2_ready" -> signals(pin2.ready),
      "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready))
  }

  def simpleThreePortMap(pin1: PortInHandshake, pin2: PortInHandshake, pin3: PortInHandshake, pout: PortOutHandshake, signals: Map[PortSimpleT, VhdlSignalT]): Map[String, VhdlSignalT] = {
    Map("clk" -> VhdlPort.CLOCK, "reset" -> VhdlPort.RESET,
      "port_in_1_data" -> signals(pin1.data), "port_in_1_last" -> signals(pin1.last), "port_in_1_valid" -> signals(pin1.valid), "port_in_1_ready" -> signals(pin1.ready),
      "port_in_2_data" -> signals(pin2.data), "port_in_2_last" -> signals(pin2.last), "port_in_2_valid" -> signals(pin2.valid), "port_in_2_ready" -> signals(pin2.ready),
      "port_in_3_data" -> signals(pin3.data), "port_in_3_last" -> signals(pin3.last), "port_in_3_valid" -> signals(pin3.valid), "port_in_3_ready" -> signals(pin3.ready),
      "port_out_data" -> signals(pout.data), "port_out_last" -> signals(pout.last), "port_out_valid" -> signals(pout.valid), "port_out_ready" -> signals(pout.ready))
  }
}
