// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class UnSndExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): UnSndExpr = UnSndExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    UnSnd(args(0), types(0).asInstanceOf[CGenDataTypeT])
}

// UnSnd :: a^DataT |-> b^DataT |-> b -D> (a, b)
object UnSnd {
  def apply(in0: Expr, a: CGenDataTypeT): UnSndExpr = UnSndExpr({
    val btv = CGenDataTypeVar()
    val outt = TupleType(a, btv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(btv), CFunType(Seq(btv), outt, DestViewType()))), Seq(in0.t)), Seq(in0))
  }, Seq(a))
  
  def unapply(expr: UnSndExpr): Some[(Expr, CGenDataTypeT, Type)] =
    Some((expr.args(0), expr.types(0).asInstanceOf[CGenDataTypeT], expr.t))
        
}

