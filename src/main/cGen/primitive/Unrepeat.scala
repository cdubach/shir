// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class UnrepeatExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): UnrepeatExpr = UnrepeatExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    Unrepeat(args(0))
}

// Unrepeat :: a^DataT |-> n^NatT |-> Addr[[a]n] -S> Addr[a]
object Unrepeat {
  def apply(in0: Expr): UnrepeatExpr = UnrepeatExpr({
    val atv = CGenDataTypeVar()
    val ntv = ArithTypeVar()
    val in0tv = AddressTypeVar(ArrayTypeVar(atv, ntv))
    val outt = AddressType(atv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv), CFunType(Seq(in0tv), outt, SrcViewType()))), Seq(in0.t)), Seq(in0))
  }, Seq())
  
  def unapply(expr: UnrepeatExpr): Some[(Expr, Type)] =
    Some((expr.args(0), expr.t))
        
}

