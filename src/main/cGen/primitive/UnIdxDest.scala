// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class UnIdxDestExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): UnIdxDestExpr = UnIdxDestExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    UnIdxDest(args(0), args(1), types(0).asInstanceOf[ArithTypeT])
}

// UnIdxDest :: a^DataT |-> b^IntT |-> n^NatT |-> Addr[a] -> b -S> Addr[[a]n]
object UnIdxDest {
  def apply(in0: Expr, in1: Expr, n: ArithTypeT): UnIdxDestExpr = UnIdxDestExpr({
    val atv = CGenDataTypeVar()
    val btv = IntTypeVar(32)
    val in0tv = AddressTypeVar(atv)
    val outt = AddressType(ArrayType(atv, n))
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv, btv), CFunType(Seq(in0tv, btv), outt, SrcViewType()))), Seq(in0.t, in1.t)), Seq(in0, in1))
  }, Seq(n))
  
  def unapply(expr: UnIdxDestExpr): Some[(Expr, Expr, ArithTypeT, Type)] =
    Some((expr.args(0), expr.args(1), expr.types(0).asInstanceOf[ArithTypeT], expr.t))
        
}

