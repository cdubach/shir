// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class IdExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IdExpr = IdExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    Id(args(0))

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Id(value, _) =>
            Some(formatter.makeAtomic(s"id ${formatter.formatAtomic(value)}"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}

// Id    :: a^DataT |-> a -E> a
object Id {
  def apply(in0: Expr): IdExpr = IdExpr({
    val atv = CGenDataTypeVar()
    val outt = atv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(atv), CFunType(Seq(atv), outt, EagerType()))), Seq(in0.t)), Seq(in0))
  }, Seq())
  
  def unapply(expr: IdExpr): Some[(Expr, Type)] =
    Some((expr.args(0), expr.t))
        
}

