// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class FstExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): FstExpr = FstExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    Fst(args(0), types(0).asInstanceOf[ViewTypeT])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Fst(value, viewType, _) =>
            val viewPrefix = viewType.toShortString.head.toLower
            Some(s"${viewPrefix}fst ${formatter.formatAtomic(value)}")
        }
      case _ =>
        super.tryFormat(formatter)
    }
  }
}

// Fst   :: a^DataT |-> b^DataT |-> v^ViewT |-> (a, b) -v> a
object Fst {
  def apply(in0: Expr, v: ViewTypeT): FstExpr = FstExpr({
    val atv = CGenDataTypeVar()
    val btv = CGenDataTypeVar()
    val in0tv = TupleTypeVar(atv, btv)
    val outt = atv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv), CFunType(Seq(in0tv), outt, v))), Seq(in0.t)), Seq(in0))
  }, Seq(v))
  
  def unapply(expr: FstExpr): Some[(Expr, ViewTypeT, Type)] =
    Some((expr.args(0), expr.types(0).asInstanceOf[ViewTypeT], expr.t))
        
}

