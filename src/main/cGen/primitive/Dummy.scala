// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class DummyExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DummyExpr = DummyExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    Dummy()
}

// Dummy :: VoidT
object Dummy {
  def apply(): DummyExpr = DummyExpr({
    
    val outt = VoidType()
    Value(outt)
  }, Seq())
  def unapply(expr: DummyExpr): Some[(Type)] = Some((expr.t))
}

