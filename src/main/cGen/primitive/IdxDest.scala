// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class IdxDestExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IdxDestExpr = IdxDestExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    IdxDest(args(0), args(1))
}

// IdxDest :: a^DataT |-> b^IntT |-> n^NatT |-> Addr[[a]n] -> b -S> Addr[a]
object IdxDest {
  def apply(in0: Expr, in1: Expr): IdxDestExpr = IdxDestExpr({
    val atv = CGenDataTypeVar()
    val ntv = ArithTypeVar()
    val btv = IntTypeVar(32)
    val in0tv = AddressTypeVar(ArrayTypeVar(atv, ntv))
    val outt = AddressType(atv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv, btv), CFunType(Seq(in0tv, btv), outt, SrcViewType()))), Seq(in0.t, in1.t)), Seq(in0, in1))
  }, Seq())
  
  def unapply(expr: IdxDestExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args(0), expr.args(1), expr.t))
        
}

