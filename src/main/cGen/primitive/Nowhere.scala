// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class NowhereExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): NowhereExpr = NowhereExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    Nowhere(types(0).asInstanceOf[CGenDataTypeT])
}

// Nowhere :: a^DataT |-> a
object Nowhere {
  def apply(a: CGenDataTypeT): NowhereExpr = NowhereExpr({
    
    val outt = a
    Value(outt)
  }, Seq(a))
  
  def unapply(expr: NowhereExpr): Some[(CGenDataTypeT, Type)] =
    Some((expr.types(0).asInstanceOf[CGenDataTypeT], expr.t))
        
}

