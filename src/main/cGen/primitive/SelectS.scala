package cGen.primitive

import cGen.{CGenExpr, SrcViewType}
import core.{ArithTypeT, Expr, MalformedExprException, Type}

/**For backward compatibility
 * Select as source view, will be transformed into Fst and Snd
 */
object SelectS {
  def apply(input: Expr, selection: ArithTypeT, len: Int = 3): CGenExpr = selection.ae.evalInt match {
    case 0 => Fst(input, SrcViewType())
    case 1 => Snd(input, SrcViewType())
    case _ => throw new MalformedExprException("Select only supports 0 or 1")
  }

  def unapply(expr: Expr): Option[(Expr, ArithTypeT, Type)] = expr match {
    case Fst(in, _: SrcViewType, _) => Some((in), 0, expr.t)
    case Snd(in, _: SrcViewType, _) => Some((in), 1, expr.t)
    case _ => None
  }
}
