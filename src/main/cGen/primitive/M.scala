// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class MExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): MExpr = MExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    M(args(0))
}

// M :: a^DataT |-> a -E> a
object M {
  def apply(in0: Expr): MExpr = MExpr({
    val atv = CGenDataTypeVar()
    val outt = atv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(atv), CFunType(Seq(atv), outt, EagerType()))), Seq(in0.t)), Seq(in0))
  }, Seq())
  
  def unapply(expr: MExpr): Some[(Expr, Type)] =
    Some((expr.args(0), expr.t))
        
}

