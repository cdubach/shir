// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class ConcatExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ConcatExpr = ConcatExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    Concat(args(0), types(0).asInstanceOf[ViewTypeT])
}

// Concat :: a^DataT |-> n^NatT |-> m^NatT |-> v^ViewT |-> ([a]n, [a]m) -v> [a]n+m
object Concat {
  def apply(in0: Expr, v: ViewTypeT): ConcatExpr = ConcatExpr({
    val atv = CGenDataTypeVar()
    val ntv = ArithTypeVar()
    val mtv = ArithTypeVar()
    val in0tv = TupleTypeVar(ArrayTypeVar(atv, ntv), ArrayTypeVar(atv, mtv))
    val outt = ArrayType(atv, ntv.ae + mtv.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv), CFunType(Seq(in0tv), outt, v))), Seq(in0.t)), Seq(in0))
  }, Seq(v))
  
  def unapply(expr: ConcatExpr): Some[(Expr, ViewTypeT, Type)] =
    Some((expr.args(0), expr.types(0).asInstanceOf[ViewTypeT], expr.t))
        
}

