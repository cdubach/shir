// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class FstDestExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): FstDestExpr = FstDestExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    FstDest(args(0))
}

// FstDest   :: a^DataT |-> b^DataT |-> Addr[(a, b)] -S> Addr[a]
object FstDest {
  def apply(in0: Expr): FstDestExpr = FstDestExpr({
    val atv = CGenDataTypeVar()
    val btv = CGenDataTypeVar()
    val in0tv = AddressTypeVar(TupleTypeVar(atv, btv))
    val outt = AddressType(atv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv), CFunType(Seq(in0tv), outt, SrcViewType()))), Seq(in0.t)), Seq(in0))
  }, Seq())
  
  def unapply(expr: FstDestExpr): Some[(Expr, Type)] =
    Some((expr.args(0), expr.t))
        
}

