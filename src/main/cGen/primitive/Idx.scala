// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class IdxExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IdxExpr = IdxExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    Idx(args(0), args(1), types(0).asInstanceOf[ViewTypeT])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Idx(array, index, _, _) =>
            Some(s"${formatter.formatAtomic(array)}[${formatter.formatNonAtomic(index)}]")
        }

      case _ => super.tryFormat(formatter)
    }
  }
}

// Idx :: a^DataT |-> b^IntT |-> n^NatT |-> v^ViewT |-> [a]n -> b -v> a
object Idx {
  def apply(in0: Expr, in1: Expr, v: ViewTypeT): IdxExpr = IdxExpr({
    val atv = CGenDataTypeVar()
    val ntv = ArithTypeVar()
    val btv = IntTypeVar(32)
    val in0tv = ArrayTypeVar(atv, ntv)
    val outt = atv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv, btv), CFunType(Seq(in0tv, btv), outt, v))), Seq(in0.t, in1.t)), Seq(in0, in1))
  }, Seq(v))
  
  def unapply(expr: IdxExpr): Some[(Expr, Expr, ViewTypeT, Type)] =
    Some((expr.args(0), expr.args(1), expr.types(0).asInstanceOf[ViewTypeT], expr.t))
        
}

