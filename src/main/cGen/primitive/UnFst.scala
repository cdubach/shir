// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class UnFstExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): UnFstExpr = UnFstExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    UnFst(args(0), types(0).asInstanceOf[CGenDataTypeT])
}

// UnFst :: a^DataT |-> b^DataT |-> a -D> (a, b)
object UnFst {
  def apply(in0: Expr, b: CGenDataTypeT): UnFstExpr = UnFstExpr({
    val atv = CGenDataTypeVar()
    val outt = TupleType(atv, b)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(atv), CFunType(Seq(atv), outt, DestViewType()))), Seq(in0.t)), Seq(in0))
  }, Seq(b))
  
  def unapply(expr: UnFstExpr): Some[(Expr, CGenDataTypeT, Type)] =
    Some((expr.args(0), expr.types(0).asInstanceOf[CGenDataTypeT], expr.t))
        
}

