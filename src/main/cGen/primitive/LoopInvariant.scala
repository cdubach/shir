// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class LoopInvariantExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): LoopInvariantExpr = LoopInvariantExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    LoopInvariant(args(0))
}

// LoopInvariant:: a^DataT |-> a -E> a
object LoopInvariant {
  def apply(in0: Expr): LoopInvariantExpr = LoopInvariantExpr({
    val atv = CGenDataTypeVar()
    val outt = atv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(atv), CFunType(Seq(atv), outt, EagerType()))), Seq(in0.t)), Seq(in0))
  }, Seq())
  
  def unapply(expr: LoopInvariantExpr): Some[(Expr, Type)] =
    Some((expr.args(0), expr.t))
        
}

