// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class SndExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SndExpr = SndExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    Snd(args(0), types(0).asInstanceOf[ViewTypeT])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Snd(value, viewType, _) =>
            val viewPrefix = viewType.toShortString.head.toLower
            Some(s"${viewPrefix}snd ${formatter.formatAtomic(value)}")
        }
      case _ =>
        super.tryFormat(formatter)
    }
  }
}

// Snd   :: a^DataT |-> b^DataT |-> v^ViewT |-> (a, b) -v> b
object Snd {
  def apply(in0: Expr, v: ViewTypeT): SndExpr = SndExpr({
    val atv = CGenDataTypeVar()
    val btv = CGenDataTypeVar()
    val in0tv = TupleTypeVar(atv, btv)
    val outt = btv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv), CFunType(Seq(in0tv), outt, v))), Seq(in0.t)), Seq(in0))
  }, Seq(v))
  
  def unapply(expr: SndExpr): Some[(Expr, ViewTypeT, Type)] =
    Some((expr.args(0), expr.types(0).asInstanceOf[ViewTypeT], expr.t))
        
}

