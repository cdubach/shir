// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class UnFstDestExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): UnFstDestExpr = UnFstDestExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    UnFstDest(args(0), types(0).asInstanceOf[CGenDataTypeT])
}

// UnFstDest :: a^DataT |-> b^DataT |-> Addr[a] -S> Addr[(a, b)]
object UnFstDest {
  def apply(in0: Expr, b: CGenDataTypeT): UnFstDestExpr = UnFstDestExpr({
    val atv = CGenDataTypeVar()
    val in0tv = AddressTypeVar(atv)
    val outt = AddressType(TupleType(atv, b))
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv), CFunType(Seq(in0tv), outt, SrcViewType()))), Seq(in0.t)), Seq(in0))
  }, Seq(b))
  
  def unapply(expr: UnFstDestExpr): Some[(Expr, CGenDataTypeT, Type)] =
    Some((expr.args(0), expr.types(0).asInstanceOf[CGenDataTypeT], expr.t))
        
}

