// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class AddExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): AddExpr = AddExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    Add(args(0))
}

// Add   :: a^DataT |-> (a, a) -E> a
object Add {
  def apply(in0: Expr): AddExpr = AddExpr({
    val atv = CGenDataTypeVar()
    val in0tv = TupleTypeVar(atv, atv)
    val outt = atv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv), CFunType(Seq(in0tv), outt, EagerType()))), Seq(in0.t)), Seq(in0))
  }, Seq())
  
  def unapply(expr: AddExpr): Some[(Expr, Type)] =
    Some((expr.args(0), expr.t))
        
}

