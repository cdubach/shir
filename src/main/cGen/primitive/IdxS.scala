package cGen.primitive

import cGen.SrcViewType
import core.{Expr, Type}

object IdxS {
  def apply(input: Expr, idx: Expr): IdxExpr = Idx(input, idx, SrcViewType())

  def unapply(expr: Expr): Option[(Expr, Expr, Type)] = expr match {
    case Idx(in, idx, vt, t) => Some((in, idx, t))
    case _ => None
  }
}
