// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class UnSndDestExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): UnSndDestExpr = UnSndDestExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    UnSndDest(args(0), types(0).asInstanceOf[CGenDataTypeT])
}

// UnSndDest :: a^DataT |-> b^DataT |-> Addr[b] -S> Addr[(a, b)]
object UnSndDest {
  def apply(in0: Expr, a: CGenDataTypeT): UnSndDestExpr = UnSndDestExpr({
    val btv = CGenDataTypeVar()
    val in0tv = AddressTypeVar(btv)
    val outt = AddressType(TupleType(a, btv))
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv), CFunType(Seq(in0tv), outt, SrcViewType()))), Seq(in0.t)), Seq(in0))
  }, Seq(a))
  
  def unapply(expr: UnSndDestExpr): Some[(Expr, CGenDataTypeT, Type)] =
    Some((expr.args(0), expr.types(0).asInstanceOf[CGenDataTypeT], expr.t))
        
}

