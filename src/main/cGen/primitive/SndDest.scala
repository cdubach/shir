// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class SndDestExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SndDestExpr = SndDestExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    SndDest(args(0))
}

// SndDest   :: a^DataT |-> b^DataT |-> Addr[(a, b)] -S> Addr[b]
object SndDest {
  def apply(in0: Expr): SndDestExpr = SndDestExpr({
    val atv = CGenDataTypeVar()
    val btv = CGenDataTypeVar()
    val in0tv = AddressTypeVar(TupleTypeVar(atv, btv))
    val outt = AddressType(btv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv), CFunType(Seq(in0tv), outt, SrcViewType()))), Seq(in0.t)), Seq(in0))
  }, Seq())
  
  def unapply(expr: SndDestExpr): Some[(Expr, Type)] =
    Some((expr.args(0), expr.t))
        
}

