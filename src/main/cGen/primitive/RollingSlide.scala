// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
package cGen.primitive

import cGen._
import core._

case class RollingSlideExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): RollingSlideExpr = RollingSlideExpr(newInnerIR, newTypes)
  override def argsBuild(args: Seq[Expr]): CGenExpr =
    RollingSlide(args(0), types(0).asInstanceOf[ArithTypeT], types(1).asInstanceOf[ViewTypeT])
}

// RollingSlide :: a^DataT |-> n^NatT |-> w^NatT |-> v^ViewT |-> [a]n -v> [[a]w]n-w+1
object RollingSlide {
  def apply(in0: Expr, w: ArithTypeT, v: ViewTypeT): RollingSlideExpr = RollingSlideExpr({
    val atv = CGenDataTypeVar()
    val ntv = ArithTypeVar()
    val in0tv = ArrayTypeVar(atv, ntv)
    val outt = ArrayType(ArrayType(atv, w), ntv.ae - w.ae + 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(in0tv), CFunType(Seq(in0tv), outt, v))), Seq(in0.t)), Seq(in0))
  }, Seq(w, v))
  
  def unapply(expr: RollingSlideExpr): Some[(Expr, ArithTypeT, ViewTypeT, Type)] =
    Some((expr.args(0), expr.types(0).asInstanceOf[ArithTypeT], expr.types(1).asInstanceOf[ViewTypeT], expr.t))
        
}

