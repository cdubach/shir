package cGen

import cGen.pass.AlgoPass.BinaryPass
import cGen.pass.CommonPass.{BetaReductionPass, DeadLetRemovalPass}
import cGen.pass.DataflowPass._
import cGen.pass.MemoryPass.{DPS, LowerSrcViewAndEagerPass}
import cGen.pass.NormalizePass._
import cGen.pass.RemoveViewPass._
import cGen.pass.TuningPass.{DetermineViewTypePass, checkNoDoNotCareLazyType}
import cGen.pass.{NormalizePass, OrchestratePass, Pass}
import core._
import core.util.IRDotGraph

import scala.language.{existentials, postfixOps}

object DPSPass {
  def apply(expr: Expr): Expr = {
    var tree: Expr = expr
    tree = TypeChecker.check(tree)
    tree = TypeChecker.check(tree)
    tree = BinaryPass()(tree) // required
    tree = BetaReductionPass()(tree) // required by determineViewTypePass and DPS
    tree = DetermineViewTypePass(tree).asInstanceOf[Expr] // optional, used when view type is not set properly
    checkNoDoNotCareLazyType(tree) // optional, make sures that view type is set properly
    tree = LiftLetPass(ExplicitEvalOrderNormalizePass(tree)) // required, make sures eager primitives are not nested, MapE(MapE) => a=MapE;MapE(a)
    tree = TypeChecker.check(tree)
    tree = TypeChecker.check(tree)
    tree = TypeChecker.check(tree)
    tree = LowerSrcViewAndEagerPass(tree) // remove src views first, significantly speed up the following typecheckers
    tree = LiftLetPass(ExplicitEvalOrderNormalizePass(tree)) // required
    tree = OrchestratePass(tree, RemoveViewPass(), BetaReductionPass(), DeadLetRemovalPass())
    tree = TypeChecker.check(tree)
    tree = DPS(tree, None) // required
//    tree = TypeChecker.check(tree)
    checkNoDoNotCareLazyType(tree) // optional, make sures that view type is set properly
    tree = OrchestratePass(tree, RemoveViewPass(), BetaReductionPass()) // remove dest views
    //    tree = TypeChecker.check(tree)
    tree = RemoveDummyPass()(tree) // optional
//    tree = TypeChecker.check(tree)
    tree = ANF(tree)
    tree = DeadLetRemovalPass()(tree) // optional
    tree = TypeChecker.check(tree) // optional, but required for change alloc kind pass
    tree = ChangeAllocKindPass()(tree) // optional
//    println("HOIST BEGIN")
//    tree = Hoist(tree)
//    println("HOIST DONE")
    tree = Hoist2(tree)
    tree = InsertFree(tree)(Seq())
    tree = TypeChecker.check(tree)
    tree = TypeChecker.check(tree)
    tree = TypeChecker.check(tree)
//    IRDotGraph(tree).show()
    tree
  }

  def isScalar(t: Type): Boolean = t.isInstanceOf[CScalarTypeT]
}


