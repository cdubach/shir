package cGen

import cGen.AlgoOp.AlgoOp
import cGen.AllocKind.{AllocKind, toAk, toAt}
import cGen.idioms.IntrinsicCallExpr
import cGen.primitive.{FstExpr, IdExpr, IdxExpr, RollingSlideExpr, SndExpr}
import core._
import core.util.Log
import lift.arithmetic.{ArithExpr, Cst, StartFromRange}

import scala.annotation.tailrec
import scala.collection.mutable
import scala.language.postfixOps
import java.util.concurrent.ConcurrentHashMap
import scala.collection.parallel.mutable.ParArray

object AllocKind extends Enumeration {
  type AllocKind = Value
  val Heap, Stack, Static = Value

  def toAt(ak: AllocKind): ArithType = ak match {
    case AllocKind.Heap => ArithType(0)
    case AllocKind.Stack => ArithType(1)
    case AllocKind.Static => ArithType(2)
  }

  def toAk(at: ArithTypeT): AllocKind = at match {
    case ArithType(Cst(0)) => AllocKind.Heap
    case ArithType(Cst(1)) => AllocKind.Stack
    case ArithType(Cst(2)) => AllocKind.Static
    case _ => throw new Exception("Not Implemented")
  }
}

case class CLambda(param: ParamDef, body: Expr, t: CFunTypeT) extends LambdaT {
  override def build(newChildren: Seq[ShirIR]): CLambda = {
    val param = newChildren.head.asInstanceOf[ParamDef]
    val body = newChildren(1).asInstanceOf[Expr]
    CLambda(param, body, CFunType(param.t, body.t, t.viewType))
  }
}

object CLambda {
  def apply(param: ParamDef, body: Expr, viewType: Option[ViewTypeT] = None): CLambda = body match {
    case _: CLambda => CLambda(param, body, CFunType(param.t, body.t, NoEffectType()))
    case _: CGenExpr => viewType match {
      case None => CLambda(param, body, CFunType(param.t, body.t, getViewType(body)))
      case Some(vt) => CLambda(param, body, CFunType(param.t, body.t, vt)) // specify the viewtype
    }
    case _: ParamUse => CLambda(param, body, CFunType(param.t, body.t, SrcViewType()))
    case _ => CLambda(param, body, CFunType(param.t, body.t, SrcViewType()))
  }

  @tailrec
  def apply(params: Seq[ParamDef], body: Expr): CLambda = {
    if (params.isEmpty) {
      throw MalformedExprException("Lambda expression must have at least one parameter")
    } else if (params.size == 1) {
      CLambda(params.head, body)
    } else {
      CLambda(params.tail, CLambda(params.head, body))
    }
  }

  // TODO: a body should be eager when it contains any eager expression
  private def getViewType(body: Expr) = body.asInstanceOf[CGenExpr].rightmostViewType
}

object CGenLet {
  def apply(param: ParamDef, arg: Expr): Expr => CGenExpr = {
    body: Expr => CFC(CLambda(param, body), arg, isLet = true)
  }

  def apply(param: ParamDef, body: Expr, arg: Expr): CGenExpr = {
    CFC(CLambda(param, body), arg, isLet = true)
  }

  def apply(params: Seq[ParamDef], body: Expr, args: Seq[Expr]): CGenExpr = {
    assert(params.nonEmpty)
    assert(params.length == args.length)
    // ugly nesting: FunctionCall(Lambda(params, body), args, applyBetaReduction = false)
    // clean nesting:
    params.length match {
      case 1 => apply(params.head, body, args.head)
      case _ => apply(params.head, apply(params.tail, body, args.tail), args.head)
    }
  }

  def unapply(expr: CFCExpr): Option[(ParamDef, Expr, Expr, Type)] = expr match {
    case CFC(f: LambdaT, arg, t) if expr.isLet => Some(f.param, f.body, arg, t)
    case _ => None
  }
}

object GetId {
  var cur = 0
  def apply(): Int = {
    cur = cur + 1
    cur
  }
}

object Isomorphic {
  def isIsomorphic(l: Expr, r: Expr) = digest(l) == digest(r)
    //_isIsomorphic(TypeChecker.check(l), TypeChecker.check(r))

  def _isIsomorphic(l: ShirIR, r: ShirIR): Boolean = {
    if (l.getClass != r.getClass) false
    else (l, r) match {
      case (ParamDef(lt, _), ParamDef(rt, _)) => _isIsomorphic(lt, rt)
      case (lat: ArithTypeT, rat: ArithTypeT) => lat.ae === rat.ae
      case (_, _) => l.children.zip(r.children).map(p => _isIsomorphic(p._1, p._2)).forall(x => x)
    }
  }

  def digest(expr: Expr) = _digest(TypeChecker.check(expr))

  private def _digest(ir: ShirIR): String = ir match {
    case (ParamDef(t, _)) => s"ParamDef($t)"
    case other => other.toShortString + (if (other.children.nonEmpty) "[" + other.children.map(_digest).mkString(";") + "]" else "")
  }

  def deduplicate(exprs: ParArray[Expr]): ParArray[Expr] = _deduplicate(exprs.zip(exprs.map(digest)))

  // this method cannot be scale up, it takes appromixately 1min for 800 input exprs.
  private def _deduplicate(exprsAndDigests: ParArray[(Expr, String)]): ParArray[Expr] = {
    Log.warn(this, s"start deduplicate")
//    val set: mutable.Set[String] = mutable.Se()
    val set: ConcurrentHashMap[String, Unit] = new ConcurrentHashMap()
    val result = exprsAndDigests.map(p => {
      val e = p._1
      val id = p._2
      if (set contains id) None
      else {
        set.put(id, Unit)
        Some(e)
      }
    }).filter(_.isDefined).map(_.get)
    Log.warn(this, s"before ${exprsAndDigests.size}, after ${result.size}")
    result
  }

  private class ExprContainer() {
    private val container: mutable.Map[String, Expr] = mutable.Map()

    def add(expr: Expr): Boolean = {
      val id = digest(expr)
      if (container contains id) return false
      else {
        container += (id -> expr)
        return true
      }
    }

    def fetch(): Seq[Expr] = container.values.toSeq

    def size: Int = container.size
  }
}

object CGenExpr {
  def unapply(ce: CGenExpr): Option[(Seq[Expr], ViewTypeT)] = Some(ce.args, ce.rightmostViewType)
}

abstract class CGenExpr extends BuiltinExpr with CGenIR {
  private var theId: Int = GetId()

  def updateId(newId: Int): CGenExpr = {
    theId = newId
    this
  }

  def id: Int = theId

  final def build(newInnerIR: Expr, newTypes: Seq[Type]): CGenExpr = {
    cgenBuild(newInnerIR, newTypes).updateId(theId)
  }

  protected def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): CGenExpr

  final def build(args: Seq[Expr]): CGenExpr = argsBuild(args).updateId(theId)

  protected def argsBuild(args: Seq[Expr]): CGenExpr

  // change the view type of a primitive, should be use carefully, since some primitives should have a fixed view type
  def toViewType(vt: ViewTypeT): CGenExpr = build(changeViewType(innerIR)(vt).asInstanceOf[Expr], types.map({
    case _: ViewTypeT => vt
    case other => other
  }))

  def toSrcViewT: CGenExpr = toViewType(SrcViewType())

  def toDestViewT: CGenExpr = toViewType(DestViewType())

  def toEagerT: CGenExpr = toViewType(EagerType())

  def toNoEffectT: CGenExpr = toViewType(NoEffectType())

  // aka semicolon operator
  def >>(other: CGenExpr): CGenExpr = {
    CGenLet(ParamDef(VoidType()), other, this)
  }

  def isSrcView: Boolean = rightmostViewType.isInstanceOf[SrcViewTypeT]

  def isDestView: Boolean = rightmostViewType.isInstanceOf[DestViewTypeT]

  def isEager: Boolean = rightmostViewType.isInstanceOf[EagerTypeT]

  def isLazy: Boolean = isSrcView || isDestView

  def isUndetermined: Boolean = rightmostViewType.isInstanceOf[NoEffectTypeT]

  /**
    * Gets the expected view types for each expression argument of this expression.
    * @return A sequence of view types, each of which matches an argument.
    */
  def paramViewTypes: Seq[ViewTypeT] = {
    this match {
      // Most operators are flexible: their parameter view types change depending on their result view type.
      case ifExpr: IfExpr => SrcViewType() +: Seq.fill(2)(ifExpr.rightmostViewType)
      case idx: IdxExpr => Seq(idx.rightmostViewType, SrcViewType())
      case tuple: TupleExpr => Seq.fill(tuple.args.length)(tuple.rightmostViewType)
      case fst: FstExpr => Seq(fst.rightmostViewType)
      case snd: SndExpr => Seq(snd.rightmostViewType)
      case _: TypeConvExpr => Seq(SrcViewType())

      // Some operators are source-only.
      case _: RollingSlideExpr => Seq(SrcViewType())

      // The eager operators consume source views and produce eager values.
      case _: ExternFunctionCallExpr | _: IntrinsicCallExpr => Seq.fill(args.length)(SrcViewType())
      case _: UnaryOpExpr => Seq(SrcViewType())
      case _: BinaryOpExpr => Seq(SrcViewType(), SrcViewType())
      case _: IdExpr => Seq(SrcViewType())

      // If the expression takes no arguments, then the list of expected view types is empty.
      case _ if args.isEmpty => Seq()

      // This function doesn't support higher-order primitives.
      case _: BuildExpr | _: IfoldExpr | _: CFCExpr | _: MapGlbExpr =>
        throw new Exception(s"Cannot produce view types for higher-order primitive '$this'")

      // Oopsie, unimplemented.
      case _ =>
        throw new Exception(s"Cannot produce view types for unknown primitive '$this'")
    }
  }

  // a primitive usually have type: (a -NoEffectT-> b) -v-> c
  // the rightmost view type "v" is on the second function
  def rightmostViewType: ViewTypeT = {
    @tailrec
    def helper(n: ShirIR): ViewTypeT = n match {
      case fc: FunctionCall => helper(fc.f)
      case bf: BuiltinFunction => helper(bf.t)
      case tfc: TypeFunctionCall => helper(tfc.tf)
      case btf: BuiltinTypeFunction => helper(btf.t.outType)
      case tft: TypeFunTypeT => helper(tft.outType)
      case ct: CFunTypeT => ct.rightmostViewType
      case CLambda(_, _, t) => helper(t)
      case CFC(f, _, _) => helper(f)
      case Value(_) => SrcViewType() // Constant's innerIR is a value, can be treated as a source view
      case use@ParamUse(_) if use.t.isInstanceOf[FunTypeT] => helper(use.t)
      case Alloc(_, _, _) => NoEffectType() // TODO: what is the viewtype for alloc??
      case Decor(in) => helper(in)
      case _ =>
        throw new Exception(s"cannot match $n")
    }
    helper(innerIR)
  }

  /**
   * @param n        : an innerIR
   * @param viewType : the view type that we want to change to
   * @return
   */
  private def changeViewType(n: ShirIR)(implicit viewType: ViewTypeT): ShirIR = n match {
    case FunctionCall(f, arg, _) => FunctionCall(changeViewType(f).asInstanceOf[Expr], arg)
    case BuiltinFunction(t) => BuiltinFunction(changeViewType(t).asInstanceOf[FunTypeT])
    case TypeFunctionCall(tf, arg, _) => TypeFunctionCall(changeViewType(tf).asInstanceOf[Expr], arg)
    case BuiltinTypeFunction(t) => BuiltinTypeFunction(changeViewType(t).asInstanceOf[TypeFunTypeT])
    case TypeFunType(tv, outt) => TypeFunType(tv, changeViewType(outt).asInstanceOf[Type])
    case CFunType(int, outt: CFunTypeT, vt) => CFunType(int, changeViewType(outt).asInstanceOf[Type], vt)
    case CFunType(int, outt, _) => CFunType(int, outt, viewType)
    case CLambda(p, b: LambdaT, _) => CLambda(p, changeViewType(b).asInstanceOf[Expr])
    case CLambda(p, b, _) => CLambda(p, b)
    case cfc@CFC(f, arg, _) if cfc.isLet => CFC(changeViewType(f).asInstanceOf[Expr], changeViewType(arg).asInstanceOf[Expr], isLet = true)
    case Value(_) if viewType.isInstanceOf[SrcViewTypeT] => n
    case _ => throw new MalformedExprException(s"Can not change the view type of $n to $viewType")
  }

  override def toString: String = rightmostViewType.toShortString.head + super.toString
}

abstract class CanBeLoweredCGenExpr extends CGenExpr {
  val isLowered: Boolean = false
}

/** A kinds of expression that are used as a decoration
 */
abstract class DecorExpr() extends CGenExpr

object Decor {
  def unapply(de: DecorExpr): Some[Expr] = Some(de.innerIR)
}

case class AddressOfExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): AddressOfExpr = AddressOfExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = AddressOf(args(0))
}

object AddressOf {
  private[cGen] def apply(input: Expr): AddressOfExpr = AddressOfExpr({
    val intv = CGenDataTypeVar()
    val outtv = AddressTypeVar(intv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intv, CGenFunType(intv, outtv))), input.t), input)
  })

  def unapply(expr: AddressOfExpr): Some[(Expr, Type)] = Some((expr.args.head, expr.t))
}

case class DestTupleExpr(innerIR: Expr) extends CGenExpr {
  override def types = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DestTupleExpr = DestTupleExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = DestTuple(args(0), args(1))
}

// n |-> (idx -> Addr[B]) -> Addr[[B]n]
// Addr[T] -> Addr[U] -> Addr[(T, U)]
object DestTuple {
  def apply(addrt: Expr, addru: Expr): DestTupleExpr = DestTupleExpr({
    val ttv = CGenDataTypeVar()
    val utv = CGenDataTypeVar()
    val addrttv = AddressTypeVar(ttv)
    val addrutv = AddressTypeVar(utv)
    val outt = AddressType(TupleType(addrttv, addrutv))
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ttv, utv),
      CFunType(Seq(ttv, utv), outt, SrcViewType()))), Seq(addrt.t, addru.t)), Seq(addrt, addru))
  })

  def unapply(expr: DestTupleExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class DestBuildExpr(innerIR: Expr, n: Type) extends CGenExpr {
  override def types = Seq(n)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DestBuildExpr = DestBuildExpr(newInnerIR, newTypes.head)

  override def argsBuild(args: Seq[Expr]): CGenExpr = DestBuild(args(0), n.asInstanceOf[ArithTypeT])
}

// n, |-> (idx -> Addr[B]) -> Addr[[B]n]
object DestBuild {
  def apply(idxf: Expr, n: ArithTypeT): DestBuildExpr = DestBuildExpr({
    val btv = CGenDataTypeVar() //B
    val addrtv = AddressTypeVar(btv)
    val idxtv = IntTypeVar(32)
    val idxftv = FunTypeVar(idxtv, addrtv)
    val outtv = AddressType(ArrayType(btv, n))
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(idxftv),
      CFunType(Seq(idxftv), outtv, SrcViewType()))), Seq(idxf.t)), Seq(idxf))
  }, n)

  def unapply(expr: DestBuildExpr): Some[(Expr, ArithTypeT, Type)] = Some((expr.args.head, expr.n.asInstanceOf[ArithTypeT], expr.t))
}

object ConcatFactory {
  def apply(in0: Expr, in1: Expr, vt: ViewTypeT = DestViewType()) = cGen.primitive.Concat(Tuple(in0, in1, vt), vt)
}

case class BlockFoldExpr(innerIR: Expr, dims: Seq[ArithTypeT], n: ArithTypeT) extends CGenExpr {
  override def types: Seq[Type] = n +: dims

  override protected def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): BlockFoldExpr = {
    BlockFoldExpr(
      newInnerIR,
      newTypes.tail.map(_.asInstanceOf[ArithTypeT]),
      newTypes.head.asInstanceOf[ArithTypeT])
  }

  override def argsBuild(args: Seq[Expr]): CGenExpr = {
    BlockFold(args(1), args(0), dims, n)
  }
}

object BlockFold {
  def apply(init: Expr, body: Expr, dims: Seq[ArithTypeT], n: ArithTypeT): BlockFoldExpr = BlockFoldExpr({
    val btv = CGenDataTypeVar()
    val idxtv = dims.map(_ => IntTypeVar(32))
    val idxftv = FunTypeVar(idxtv, btv)
    val bodyftv = FunTypeVar(btv +: IntTypeVar(32) +: idxtv, btv)
    val outt = dims.reverse.foldLeft[CGenDataTypeT](btv)((result, n) => ArrayType(result, n))

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(bodyftv, idxftv),
      CFunType(Seq(bodyftv, idxftv), outt, DestViewType()))), Seq(body.t, init.t)), Seq(body, init))
  }, dims, n)

  def unapply(expr: BlockFoldExpr): Option[(Expr, Expr, Seq[ArithTypeT], ArithTypeT, Type)] = {
    Some((expr.args(1), expr.args.head, expr.dims, expr.n, expr.t))
  }
}

case class BuildExpr(innerIR: Expr, n: Type, override val isLowered: Boolean = false) extends CanBeLoweredCGenExpr {
  override def types = Seq(n)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): BuildExpr = {
    BuildExpr(newInnerIR, newTypes.head, isLowered).asInstanceOf[BuildExpr]
  }

  override def argsBuild(args: Seq[Expr]): CGenExpr = {
    if (isLowered) DBuild(args(0), n.asInstanceOf[ArithTypeT], this.rightmostViewType)
    else Build(args(0), n.asInstanceOf[ArithTypeT], this.rightmostViewType)
  }

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Build(idxf, n, _, _) =>
            val buildName = rightmostViewType.toShortString.head.toLower + "build"
            Some(formatter.makeAtomic(s"$buildName ${formatter.formatAtomic(idxf)} ${formatter.formatAtomic(n)}"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}

object DBuild {
  def apply(idxf: Expr, n: ArithTypeT, vt: ViewTypeT = NoEffectType()): BuildExpr = BuildExpr({
    val idxtv = IntTypeVar(32)
    val idxftv = FunTypeVar(idxtv, VoidTypeVar())
    val outtv = VoidType()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(idxftv),
      CFunType(Seq(idxftv), outtv, EagerType()))), Seq(idxf.t)), Seq(idxf))
  }, n, isLowered = true)

  def unapply(expr: BuildExpr): Option[(Expr, ArithTypeT, ViewTypeT, Type)] = {
    if (expr.isLowered) Some((expr.args.head, expr.n.asInstanceOf[ArithTypeT], expr.rightmostViewType, expr.t))
    else None
  }
}

// n |-> (idx -> B) -> [B]n
object Build {
  def apply(idxf: Expr, n: ArithTypeT, vt: ViewTypeT = NoEffectType()): BuildExpr = BuildExpr({
    val btv = CGenDataTypeVar() //B
    val idxtv = IntTypeVar(32)
    val idxftv = FunTypeVar(idxtv, btv)
    val outtv = ArrayType(btv, n)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(idxftv),
      CFunType(Seq(idxftv), outtv, vt))), Seq(idxf.t)), Seq(idxf))
  }, n)

  def unapply(expr: BuildExpr): Option[(Expr, ArithTypeT, ViewTypeT, Type)] = {
    if (expr.isLowered)
      None
    else
      Some((expr.args.head, expr.n.asInstanceOf[ArithTypeT], expr.rightmostViewType, expr.t))
  }
}


case class DIdExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DIdExpr = DIdExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = DId(args(0), args(1))
}

object DId {
  def apply(input: Expr, dest: Expr): DIdExpr = DIdExpr({
    val inttv = CGenDataTypeVar()
    val dtv = AddressTypeVar(inttv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(inttv, dtv), CFunType(Seq(inttv, dtv), VoidType(), EagerType()))), Seq(input.t, dest.t)), Seq(input, dest))
  })

  def unapply(expr: DIdExpr): Option[(Expr, Expr, Type)] = {
    Some((expr.args.head, expr.args(1), expr.t))
  }
}

object Assign {
  def apply(input: Expr, dest: Expr): DIdExpr = DId(input, dest)

  def unapply(expr: DIdExpr): Option[(Expr, Expr, Type)] = {
    Some((expr.args.head, expr.args(1), expr.t))
  }
}

case class UnaryOpExpr(innerIR: Expr, op: AlgoOp) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): UnaryOpExpr = UnaryOpExpr(newInnerIR, op).asInstanceOf[UnaryOpExpr]

  override def argsBuild(args: Seq[Expr]): CGenExpr = UnaryOp(args(0), op)
}

object UnaryOp {
  def apply(in: Expr, op: AlgoOp): UnaryOpExpr = {
    UnaryOpExpr({
      val itv = CScalarTypeVar()
      val outit = itv
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(itv, CFunType(itv, outit, EagerType()))), in.t), in)
    }, op)
  }

  def unapply(expr: UnaryOpExpr): Option[(Expr, AlgoOp, Type)] = {
    Some((expr.args.head, expr.op, expr.t))
  }
}

/**
 * The tree node kind for binary operation expressions.
 * @param op The operator of the binary operation.
 * @param isBool True if the binary operation is a boolean operation; otherwise, false.
 */
private case class BinaryOpExprKind(op: AlgoOp, isBool: Boolean) extends TreeNodeKind

case class BinaryOpExpr(innerIR: Expr, op: AlgoOp, isBool: Boolean) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): BinaryOpExpr = BinaryOpExpr(newInnerIR, op, isBool).asInstanceOf[BinaryOpExpr]

  override def argsBuild(args: Seq[Expr]): CGenExpr = BinaryOp(args(0), args(1), op, isBool)

  override def nodeKind: TreeNodeKind = this match {
    case BinaryOp(_, _, op, isBool, _) => BinaryOpExprKind(op, isBool)
  }

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        val opStr = op match {
          case AlgoOp.Add => "+"
          case AlgoOp.Sub => "-"
          case AlgoOp.Mul => "*"
          case AlgoOp.Div => "/"
          case AlgoOp.Mod => "%"
          case AlgoOp.RightShift => ">>"
          case AlgoOp.LeftShift => "<<"
          case AlgoOp.Eq => "=="
          case AlgoOp.Lt => "<"
          case AlgoOp.Gt => ">"
          case AlgoOp.Le => "<="
          case AlgoOp.Ge => ">="
          case AlgoOp.Andand => "&&"
          case AlgoOp.Pow => "pow"
          case AlgoOp.Log => "log"
          case AlgoOp.Sqrt => "sqrt"
          case AlgoOp.Exp => "exp"
          case AlgoOp.Sin => "sin"
          case AlgoOp.Cos => "cos"
          case _ => throw new Exception(s"Unknown operator $op")
        }

        this match {
          case BinaryOp(left@BinaryOp(_, _, op1, _, _), right, op2, _, _) if op1 == op2 =>
            Some(formatter.makeAtomic(s"${formatter.formatNonAtomic(left)} $opStr ${formatter.formatAtomic(right)}"))

          case BinaryOp(left, right, _, _, _) =>
            Some(formatter.makeAtomic(s"${formatter.formatAtomic(left)} $opStr ${formatter.formatAtomic(right)}"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}

object BinaryOp {
  def apply(in1: Expr, in2: Expr, op: AlgoOp, isBool: Boolean = false, isWritten: Boolean = false): BinaryOpExpr = {
    BinaryOpExpr({
      val itv1 = CScalarTypeVar()
      val itv2 = itv1
      val outit = if (isBool) BoolType() else itv1
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(itv1, itv2), CFunType(Seq(itv1, itv2), outit, EagerType()))), Seq(in1.t, in2.t)), Seq(in1, in2))
    }, op, isBool = isBool)
  }

  def unapply(expr: BinaryOpExpr): Option[(Expr, Expr, AlgoOp, Boolean, Type)] = {
    Some((expr.args.head, expr.args.tail.head, expr.op, expr.isBool, expr.t))
  }
}

case class CFCExpr(innerIR: Expr, isLet: Boolean) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): CFCExpr = CFCExpr(newInnerIR, isLet)

  override def argsBuild(args: Seq[Expr]): CGenExpr =
    CFC(innerIR.asInstanceOf[FunctionCall].f, args(0), isLet)

  override def toString: String = {
    val paramString = innerIR.asInstanceOf[FunctionCall].f match {
      case lam: LambdaT => lam.param.toShortString + ", "
      case _ => "_, "
    }
    val name = if (isLet) "CGenLet" else this.getClass.getSimpleName
    name + "(" + paramString + "_, " + args.mkString(", ").stripSuffix(", ") + ")"
  }

  override def toShortString: String = {
    if (isLet) "CGenLet"
    else this.getClass.getSimpleName
  }
}

// Customized Function Call. Used to differentiate normal FunctionCall from let expression,
// avoid let expression being beta-reduced
object CFC {
  def apply(f: Expr, arg: Expr): CFCExpr = CFC(f, arg, false)

  def apply(f: Expr, arg: Expr, isLet: Boolean): CFCExpr = CFCExpr(FunctionCall(f, arg), isLet)

  def apply(f: Expr, args: Seq[Expr]): CFCExpr = CFC(f, args, false)

  def apply(f: Expr, args: Seq[Expr], isLet: Boolean): CFCExpr = {
    if (args.isEmpty) {
      throw MalformedExprException("Function Call expression must have at least one argument")
    } else if (args.size == 1) {
      CFC(f, args.head, isLet)
    } else {
      CFC(CFC(f, args.tail, isLet), args.head, isLet)
    }
  }

  def unapply(expr: CFCExpr): Some[(Expr, Expr, Type)] = Some(
    (expr.innerIR.asInstanceOf[FunctionCall].f,
      expr.innerIR.asInstanceOf[FunctionCall].arg,
      expr.t,
    ))
}

case class DExternFunctionCallExpr(innerIR: Expr, name: String, ins: Seq[Expr], intvs: Seq[TypeVarT]) extends CanBeLoweredCGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DExternFunctionCallExpr = DExternFunctionCallExpr(newInnerIR, name, ins, intvs)

  override def argsBuild(args: Seq[Expr]): CGenExpr = DExternFunctionCall(name, args, intvs)
}

object DExternFunctionCall { // the input intvs/outt/ins/ should be in-order
  def apply(name: String, ins: Seq[Expr], intvs: Seq[TypeVarT]): DExternFunctionCallExpr = {
    val outt = VoidType()
    DExternFunctionCallExpr({
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intvs, CFunType(intvs, outt, EagerType()))), ins.map(_.t)), ins)
    }, name, ins, intvs)
  }

  def unapply(expr: DExternFunctionCallExpr): Some[(String, Seq[Expr], Seq[TypeVarT], Type)] =
    Some(expr.name, expr.ins.indices.map(expr.args(_)), expr.intvs, VoidType())
}

/**
 * The tree node kind for external function call expressions.
 * @param functionName The name of the external function.
 * @param argCount The number of arguments to the external function.
 */
private case class ExternFunctionCallExprKind(functionName: String, argCount: Int) extends TreeNodeKind

/**
 * An external function call expression.
 * @param innerIR The inner IR of the expression.
 * @param name The name of the external function.
 * @param intvs The input type variables.
 * @param outt The output type.
 */
case class ExternFunctionCallExpr(innerIR: Expr, name: String, intvs: Seq[TypeVarT], outt: Type) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ExternFunctionCallExpr = ExternFunctionCallExpr(newInnerIR, name, intvs, outt)

  override def argsBuild(args: Seq[Expr]): CGenExpr = ExternFunctionCall(name, args, intvs, outt)

  override def nodeKind: TreeNodeKind = this match {
    case ExternFunctionCall(name, args, _, _) => ExternFunctionCallExprKind(name, args.length)
  }

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case ExternFunctionCall(name, args, _, _) =>
            Some(formatter.makeAtomic(s"$name(${args.map(formatter.formatNonAtomic).mkString(", ")})"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}

object ExternFunctionCall { // the input intvs/outt/ins/ should be reversed
  def apply(name: String, ins: Seq[Expr], intvs: Seq[TypeVarT], outt: Type, isWritten: Boolean = false): ExternFunctionCallExpr = {
    ExternFunctionCallExpr({
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intvs, CFunType(intvs, outt, EagerType()))), ins.map(_.t)), ins)
    }, name, intvs, outt)
  }

  def unapply(expr: ExternFunctionCallExpr): Some[(String, Seq[Expr], Seq[TypeVarT], Type)] = {
    Some(expr.name, expr.args, expr.intvs, expr.outt)
  }
}

object GetArgument {
  def apply(functionCall: Expr, argPos: Int): Option[Expr] = functionCall match {
    case fc: FunctionCall =>
      if (argPos == 0) {
        Some(fc.arg)
      } else {
        apply(fc.f, argPos - 1)
      }
    case _ => None
  }
}

case class DIfExpr(innerIR: Expr) extends CanBeLoweredCGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): DIfExpr = {
    val computeCond = GetArgument(newInnerIR, 0).get
    val cond = GetArgument(newInnerIR, 1).get
    val b0 = GetArgument(newInnerIR, 2).get
    val b1 = GetArgument(newInnerIR, 3).get
    // when the type is changed in any branch, the return type of if need to be changed
    if (!b0.t.isInstanceOf[AnyTypeVar] && !newInnerIR.t.isInstanceOf[AnyTypeVar]) {
      val vt = DIfExpr(newInnerIR).rightmostViewType
      DIf(computeCond, cond, b0, b1, vt)
    } else {
      DIfExpr(newInnerIR)
    }
  }

  override def argsBuild(args: Seq[Expr]): CGenExpr = DIf(args(0), args(1), args(2), args(3), this.rightmostViewType)
}

/* DIf will be compiled to in DPS:
condStmt; // computes the value of cond and stored to it
if(cond) branch0
else branch1
 */
object DIf {
  def apply(computeCond: Expr, cond: Expr, branch0: Expr, branch1: Expr, vt: ViewTypeT = DestViewType()): DIfExpr = DIfExpr({
    val computeCondtv = CGenDataTypeVar()
    val condtv = BoolTypeVar()
    val btv = CGenDataTypeVar() // U

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(computeCondtv, condtv, btv, btv),
      CFunType(Seq(computeCondtv, condtv, btv, btv), VoidType(), vt))), Seq(computeCond.t, cond.t, branch0.t, branch1.t)), Seq(computeCond, cond, branch0, branch1))
  })

  def unapply(expr: DIfExpr): Some[(Expr, Expr, Expr, Expr, Type)] =
    Some(expr.args.head, expr.args.tail.head, expr.args(2), expr.args(3), expr.t)
}

case class SIfExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SIfExpr = {
    val cond = GetArgument(newInnerIR, 0).get
    val b0 = GetArgument(newInnerIR, 1).get
    val b1 = GetArgument(newInnerIR, 2).get
    // when the type is changed in any branch, the return type of if need to be changed
    if (!b0.t.isInstanceOf[AnyTypeVar] && !newInnerIR.t.isInstanceOf[AnyTypeVar]) {
      SIf(cond, b0, b1)
    } else {
      SIfExpr(newInnerIR)
    }
  }

  override def argsBuild(args: Seq[Expr]): CGenExpr = SIf(args(0), args(1), args(2))
}

object SIf {
  def apply(cond: Expr, branch0: Expr, branch1: Expr): SIfExpr = SIfExpr({
    val condtv = BoolTypeVar()
    val btv = CGenDataTypeVar() // U

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(condtv, btv, btv),
      CFunType(Seq(condtv, btv, btv), btv, SrcViewType()))), Seq(cond.t, branch0.t, branch1.t)), Seq(cond, branch0, branch1))
  })

  def unapply(expr: SIfExpr): Some[(Expr, Expr, Expr, Type)] =
    Some(expr.args.head, expr.args(1), expr.args(2), expr.t)
}

case class IfExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IfExpr = {
    val cond = GetArgument(newInnerIR, 0).get
    val b0 = GetArgument(newInnerIR, 1).get
    val b1 = GetArgument(newInnerIR, 2).get
    // when the type is changed in any branch, the return type of if need to be changed
    if (!b0.t.isInstanceOf[AnyTypeVar] && !newInnerIR.t.isInstanceOf[AnyTypeVar] && b0.t != newInnerIR.t) {
      val vt = IfExpr(newInnerIR).rightmostViewType // a hackery to get the viewType from innerIR
      If(cond, b0, b1, vt)
    } else {
      IfExpr(newInnerIR)
    }
  }

  override def argsBuild(args: Seq[Expr]): CGenExpr = If(args(0), args(1), args(2), this.rightmostViewType)
}

// Bool -> U -> U -> U
object If {
  def apply(cond: Expr, branch0: Expr, branch1: Expr, vt: ViewTypeT = NoEffectType()): IfExpr = IfExpr({
    val condtv = BoolTypeVar()
    val btv = CGenDataTypeVar() // U

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(condtv, btv, btv),
      CFunType(Seq(condtv, btv, btv), btv, vt))), Seq(cond.t, branch0.t, branch1.t)), Seq(cond, branch0, branch1))
  })

  def unapply(expr: IfExpr): Some[(Expr, Expr, Expr, Type)] =
    Some(expr.args.head, expr.args.tail.head, expr.args(2), expr.t)
}

case class IfoldExpr(innerIR: Expr, n: ArithTypeT, override val isLowered: Boolean) extends CanBeLoweredCGenExpr {
  override def types: Seq[Type] = Seq(n)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IfoldExpr = IfoldExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT], isLowered)

  override def argsBuild(args: Seq[Expr]): CGenExpr = {
    if (isLowered) DIfold(args(0), args(1), args(2), n)
    else Ifold(args(0), args(1), n)
  }

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Ifold(idxf, init, n, _) =>
            Some(formatter.makeAtomic(
              s"ifold ${formatter.formatAtomic(idxf)} ${formatter.formatAtomic(init)} ${formatter.formatAtomic(n)}"))
        }
      case _ => super.tryFormat(formatter)
    }
  }
}

object DIfold {
  def apply(idxf: Expr, init: Expr, dest: Expr, n: ArithTypeT): IfoldExpr = IfoldExpr({
    // (U -> T -> U) -> U -> [T]n -> U
    // n |-> (idx -> Void) -> U -> Addr[U] -> Void
    // n |-> (utv -> idx -> Void) -> Void -> Addr[U] -> Void
    val utv = CGenDataTypeVar()
    val inittv = VoidTypeVar()
    val dtv = AddressTypeVar(utv)
    val idxtv = IntTypeVar(32)
    val ftv = CFunTypeVar(Seq(utv, idxtv), VoidTypeVar())

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, inittv, dtv),
      CFunType(Seq(ftv, inittv, dtv), VoidType(), EagerType()))), Seq(idxf.t, init.t, dest.t)), Seq(idxf, init, dest))
  }, n, true)

  def unapply(expr: IfoldExpr): Option[(Expr, Expr, Expr, ArithTypeT, Type)] = {
    if (expr.isLowered) Some((expr.args.head, expr.args.tail.head, expr.args(2), expr.n, expr.t))
    else None
  }
}

object Ifold {
  def apply(idxf: Expr, init: Expr, n: ArithTypeT): IfoldExpr = IfoldExpr({
    // (U -> T -> U) -> U -> [T]n -> U
    // n |-> (idx -> U -> U) -> U -> U
    val utv = CGenDataTypeVar() // U
    val idxtv = IntTypeVar(32)
    val ftv = CFunTypeVar(Seq(utv, idxtv), utv)

    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, utv),
      CFunType(Seq(ftv, utv), utv, EagerType()))), Seq(idxf.t, init.t)), Seq(idxf, init))
  }, n, false)

  def unapply(expr: IfoldExpr): Option[(Expr, Expr, ArithTypeT, Type)] = {
    if (expr.isLowered) None
    else Some((expr.args.head, expr.args.tail.head, expr.n, expr.t))
  }
}

// Split:   T |-> n(nat) |-> m(nat) -> [T]n-> [[T]m]n/m
// RVSplit: T |-> n(nat) |-> m(nat) -> Addr[[[T]m]n/m] -> Addr[[T]n]
case class ValueAtExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ValueAtExpr = ValueAtExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = ValueAt(args(0))
}

object ValueAt {
  private[cGen] def apply(input: Expr): ValueAtExpr = ValueAtExpr({
    //    if (input.isInstanceOf[ParamDef])
    //      throw new Exception("Should not be a ParamDef")
    val outtv = CGenDataTypeVar()
    val intv = AddressTypeVar(outtv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intv, CFunType(intv, outtv, SrcViewType()))), input.t), input)
  })

  def unapply(expr: ValueAtExpr): Some[(Expr, Type)] = Some((expr.args.head, expr.t))
}

case class IdxOffsetOpExpr(innerIR: Expr, n: ArithTypeT, op: AlgoOp) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IdxOffsetOpExpr = IdxOffsetOpExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT], op)

  override def types: Seq[Type] = Seq(n)

  override def argsBuild(args: Seq[Expr]): CGenExpr = IdxOffsetOp(args(0), n, op)
}

// n |-> idx -> idx
object IdxOffsetOp {
  def apply(idx: Expr, n: ArithTypeT, op: AlgoOp): IdxOffsetOpExpr = IdxOffsetOpExpr({
    val idxtv = IntTypeVar(32)
    val outt = IntType(32)
    FunctionCall(
      TypeFunctionCall(
        BuiltinTypeFunction(TypeFunType(idxtv, CFunType(idxtv, outt))),
        idx.t),
      idx)
  }, n, op)

  def unapply(expr: IdxOffsetOpExpr): Some[(Expr, ArithTypeT, AlgoOp, Type)] = Some(
    expr.args.head,
    expr.types.head.asInstanceOf[ArithTypeT],
    expr.op,
    expr.t
  )
}

case class IdxBiOpExpr(innerIR: Expr, op: AlgoOp) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IdxBiOpExpr = IdxBiOpExpr(newInnerIR, op)

  override def types: Seq[Type] = Seq()

  override def argsBuild(args: Seq[Expr]): CGenExpr = IdxBiOp(args(0), args(1), op)
}

// n |-> idx -> idx
object IdxBiOp {
  def apply(idx0: Expr, idx1: Expr, op: AlgoOp): IdxBiOpExpr = IdxBiOpExpr({
    val idx0tv = IntTypeVar(32)
    val idx1tv = IntTypeVar(32)
    val outt = IntType(32)
    FunctionCall(
      TypeFunctionCall(
        BuiltinTypeFunction(TypeFunType(Seq(idx0tv, idx1tv), CFunType(Seq(idx0tv, idx1tv), outt))),
        Seq(idx0.t, idx1.t)),
      Seq(idx0, idx1))
  }, op)

  def unapply(expr: IdxBiOpExpr): Some[(Expr, Expr, AlgoOp, Type)] = Some(
    expr.args.head,
    expr.args.tail.head,
    expr.op,
    expr.t
  )
}

// A -> [A]1
object RepeatOnce {
  def apply(in: Expr): BuildExpr = {
    Build(CLambda(ParamDef(IntType(32)), in), ArithType(1), EagerType()) // in should be a destview or an eager
  }
}

case class RepeatExpr(innerIR: Expr, m: ArithTypeT) extends CGenExpr {
  override def types: Seq[Type] = Seq(m)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): RepeatExpr = RepeatExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])

  override def argsBuild(args: Seq[Expr]): CGenExpr = Repeat(args(0), m, this.rightmostViewType)
}

object RepeatS {
  def apply(m: ArithTypeT, in: Expr) = Repeat(in, m, SrcViewType())
}

// m -> u -> [u]m
object Repeat {
  def apply(in: Expr,  m: ArithTypeT, vt: ViewTypeT = NoEffectType()): RepeatExpr = RepeatExpr({
    val intv = CGenDataTypeVar()
    val outtv = ArrayType(intv, m)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intv, CFunType(intv, outtv, vt))), in.t), in)
  }, m)

  def unapply(expr: RepeatExpr): Some[(ArithTypeT, Expr, Type)] = Some(expr.m, expr.args.head, expr.t)
}

case class SliceExpr(innerIR: Expr, m: ArithTypeT) extends CGenExpr {
  override def types: Seq[Type] = Seq(m)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SliceExpr = SliceExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])

  override def argsBuild(args: Seq[Expr]): CGenExpr = Slice(args(0), args(1), m)
}

// m |-> [A]n -> s -> [A]m
object Slice {
  def apply(arr: Expr, start: Expr, m: ArithTypeT): SliceExpr = SliceExpr({
    val starttv = IntTypeVar(32)
    val atv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val arrtv = ArrayTypeVar(atv, lentv)
    val outtv = ArrayType(atv, m)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(arrtv, starttv), CFunType(Seq(arrtv, starttv), outtv, SrcViewType())))
      , Seq(arr.t, start.t))
      , Seq(arr, start))
  }, m)

  def unapply(expr: SliceExpr): Some[(Expr, Expr, ArithTypeT, Type)] = Some(expr.args.head, expr.args(1), TypeChecker.check(expr).t.asInstanceOf[ArrayTypeT].len, expr.t)
}

case class MapExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): MapExpr = MapExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Map(args(0), args(1), this.rightmostViewType)
}

object MapD {
  def apply(function: Expr, input: Expr): MapExpr = Map(function, input, DestViewType())
}

object MapS {
  def apply(function: Expr, input: Expr): MapExpr = Map(function, input, SrcViewType())
}

object MapE {
  def apply(function: Expr, input: Expr): MapExpr = Map(function, input, EagerType())
}

object Map {
  def apply(function: Expr, input: Expr): MapExpr = apply(function, input, None)

  def apply(function: Expr, input: Expr, vt: ViewTypeT): MapExpr = apply(function, input, Some(vt))

  def apply(function: Expr, input: Expr, vt: Option[ViewTypeT]): MapExpr = MapExpr({
    val indtv = CGenDataTypeVar()
    val outdtv = CGenDataTypeVar()
    val ftv = CFunTypeVar(indtv, outdtv)
    val lentv = ArithTypeVar()
    val inseqtv = ArrayTypeVar(indtv, lentv)
    val outseqt = ArrayType(outdtv, lentv)
    val fc = FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, inseqtv),
      {
        CFunType(Seq(ftv, inseqtv), outseqt, vt match {
          // if view type is not provided, then map's view type wil be up to the input function
          case None => function.asInstanceOf[CLambda].t.viewType match {
            case _: DestViewTypeT | _: EagerTypeT => EagerType()
            case _: SrcViewTypeT => SrcViewType()
            case _: NoEffectTypeT => NoEffectType()
          }
          case Some(vt) => vt //specified by the programmer; usually it is set as a src view type for fusion between eager map
        })
      }
    )), Seq(function.t, input.t)), Seq(function, input))
    fc
  })

  def unapply(expr: MapExpr): Some[(Expr, Expr, ViewTypeT, Type)] =
    Some((expr.args.head, expr.args(1), expr.rightmostViewType.asInstanceOf[ViewTypeT], expr.t))
}

//case class AddExpr(innerIR: Expr) extends CGenExpr {
//  override def types: Seq[Type] = Seq()
//
//  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): AddExpr = AddExpr(newInnerIR)
//
//  override def argsBuild(args: Seq[Expr]): CGenExpr = Add(args(0))
//}
//
//object Add extends BuiltinExprFun {
//  override def args: Int = 1
//
//  def apply(input: Expr): AddExpr = AddExpr({
//    val itv1 = CScalarTypeVar()
//    val itv2 = itv1
//    val ttv = TupleTypeVar(itv1, itv2)
//    val outit = itv1
//    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, CFunType(ttv, outit))), input.t), input)
//  })
//
//  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): AddExpr = apply(inputs.head)
//
//  def unapply(expr: AddExpr): Some[(Expr, Type)] =
//    Some((expr.args.head, expr.t))
//}

case class SubExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SubExpr = SubExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Sub(args(0))
}

object Sub extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): SubExpr = SubExpr({
    val i1tv = CScalarTypeVar()
    val i2tv = i1tv
    val ttv = TupleTypeVar(i1tv, i2tv)
    val outit = i1tv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, CFunType(ttv, outit))), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SubExpr = apply(inputs.head)

  def unapply(expr: SubExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class MulExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): MulExpr = MulExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Mul(args(0))
}

object Mul extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): MulExpr = MulExpr({
    val i1tv = CScalarTypeVar()
    val i2tv = i1tv
    val ttv = TupleTypeVar(i1tv, i2tv)
    val outit = i1tv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(ttv, CFunType(ttv, outit))), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): MulExpr = apply(inputs.head)

  def unapply(expr: MulExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

/**
 * The tree node kind for constant expressions.
 * @param value The value of the constant.
 */
private case class ConstantExprKind(value: Double) extends TreeNodeKind

/**
 * A constant expression.
 * @param innerIR The inner IR of the expression.
 * @param types The types of the expression.
 * @param value The value of the constant.
 */
case class ConstantExpr(innerIR: Expr, types: Seq[Type], value: Double) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ConstantExpr =
    Constant(value, newTypes(0))

  override def argsBuild(args: Seq[Expr]): CGenExpr = this

  override def nodeKind: TreeNodeKind = this match {
    case Constant(value, _) => ConstantExprKind(value)
  }

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Constant(value, t: IntTypeT) =>
            Some(formatter.makeAtomic(s"const ${value.toLong}: ${formatter.formatNonAtomic(t)}"))

          case _ =>
            Some(formatter.makeAtomic(s"const $value: ${formatter.formatNonAtomic(t)}"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}

object Constant {
  def apply(value: Double, valueType: Type): ConstantExpr = ConstantExpr(Value(valueType), Seq(valueType), value)

  def unapply(expr: ConstantExpr): Some[(Double, Type)] = Some(expr.value, expr.t)
}

case class ReduceExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ReduceExpr = ReduceExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Reduce(args(0), args(1), args(2))
}

object Reduce {
  // f: acc -> cur -> acc
  def apply(function: Expr, initialValue: Expr, input: Expr): ReduceExpr = ReduceExpr({
    val initialValuetc = TypeChecker.checkIfUnknown(initialValue, initialValue.t)
    initialValuetc.t match {
      case _: IntTypeT =>
        val dtv = IntTypeVar()
        val initialValueTv = IntTypeVar()
        val lentv = ArithTypeVar(StartFromRange(1))
        val inseqtv = ArrayTypeVar(dtv, lentv)
        val outt = IntTypeVar() // IntType(dtv.width.ae)
        val ftv = FunTypeVar(Seq(outt, dtv), IntTypeVar())
        FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
          TypeFunType(
            Seq(ftv, initialValueTv, inseqtv),
            CFunType(
              Seq(ftv, initialValueTv, inseqtv),
              outt,
              EagerType()
            )
          )),
          Seq(function.t, initialValue.t, input.t)),
          Seq(function, initialValue, input)
        )
      case _ =>
        val dtv = CGenDataTypeVar()
        val acctv = CGenDataTypeVar()
        val ftv = FunTypeVar(acctv, FunTypeVar(dtv, acctv))
        val inseqtv = ArrayTypeVar(dtv)
        FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
          TypeFunType(
            Seq(ftv, acctv, inseqtv),
            CFunType(
              Seq(ftv, acctv, inseqtv),
              acctv,
              EagerType()
            )
          )),
          Seq(function.t, initialValue.t, input.t)),
          Seq(function, initialValue, input)
        )
    }
  })

  def unapply(expr: ReduceExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class ZipExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ZipExpr = ZipExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Zip(args(0), this.rightmostViewType)
}

object Zip extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr, vt: ViewTypeT = NoEffectType()): ZipExpr = ZipExpr({
    val dtv1 = CGenDataTypeVar()
    val dtv2 = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv1 = ArrayTypeVar(dtv1, lentv)
    val inseqtv2 = ArrayTypeVar(dtv2, lentv)
    val inttv = TupleTypeVar(inseqtv1, inseqtv2)
    val outseqt = ArrayType(TupleType(dtv1, dtv2), lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, CFunType(inttv, outseqt, vt))), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): ZipExpr = apply(inputs.head)

  def unapply(expr: ZipExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class Zip3Expr(innerIR: Expr) extends CGenExpr {
  override def types = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): Zip3Expr = Zip3Expr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Zip3(args(0))
}

object Zip3 {
  def apply(input: Expr): Zip3Expr = Zip3Expr({
    val dtv1 = CGenDataTypeVar()
    val dtv2 = CGenDataTypeVar()
    val dtv3 = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv1 = ArrayTypeVar(dtv1, lentv)
    val inseqtv2 = ArrayTypeVar(dtv2, lentv)
    val inseqtv3 = ArrayTypeVar(dtv3, lentv)
    val inttv = TupleTypeVar(inseqtv1, inseqtv2, inseqtv3)
    val outseqt = ArrayType(TupleType(dtv1, dtv2, dtv3), lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inttv, CFunType(inttv, outseqt))), input.t), input)
  })

  def unapply(expr: Zip3Expr): Some[(Expr, Type)] = Some((expr.args.head, expr.t))

}

case class TupleExpr(innerIR: Expr, name: String) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): TupleExpr = TupleExpr(newInnerIR, name)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Tuple(name, args(0), args(1), this.rightmostViewType)

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case Tuple(first, second, _) =>
            Some(formatter.makeAtomic(s"tuple ${formatter.formatAtomic(first)} ${formatter.formatAtomic(second)}"))
        }
      case _ => super.tryFormat(formatter)
    }
  }
}

// V |-> T |-> U |-> T_V -> U_V -> (T_V, U_V)_V
object Tuple {
  def apply(input1: Expr, input2: Expr): TupleExpr = Tuple("", input1, input2, NoEffectType())

  def apply(name: String, input1: Expr, input2: Expr): TupleExpr = Tuple(name, input1, input2, NoEffectType())

  def apply(input1: Expr, input2: Expr, viewType: ViewTypeT): TupleExpr = Tuple("", input1, input2, viewType)

  def apply(name: String, input1: Expr, input2: Expr, viewType: ViewTypeT): TupleExpr = TupleExpr({
    val dtv1 = CGenDataTypeVar()
    val dtv2 = CGenDataTypeVar()
    val outtt = TupleType(name, dtv1, dtv2)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(dtv1, dtv2), CFunType(Seq(dtv1, dtv2), outtt, viewType))), Seq(input1.t, input2.t)), Seq(input1, input2))
  }, name)

  def apply(inputs: Seq[Expr], types: Seq[Type]): TupleExpr = apply(inputs.head, inputs(1))

  def unapply(expr: TupleExpr): Some[(Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.t))
}

case class Tuple3Expr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): Tuple3Expr = Tuple3Expr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Tuple3(args(0), args(1), args(2))
}

object Tuple3 {
  def apply(input1: Expr, input2: Expr, input3: Expr): Tuple3Expr = Tuple3Expr({
    val dtv1 = CGenDataTypeVar()
    val dtv2 = CGenDataTypeVar()
    val dtv3 = CGenDataTypeVar()
    val outtt = TupleType(dtv1, dtv2, dtv3)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(dtv1, dtv2, dtv3), CFunType(Seq(dtv1, dtv2, dtv3), outtt))), Seq(input1.t, input2.t, input3.t)), Seq(input1, input2, input3))
  })

  def unapply(expr: Tuple3Expr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

case class SplitExpr(innerIR: Expr, chunkSize: ArithTypeT) extends CGenExpr {
  override def types = Seq(chunkSize)
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SplitExpr = SplitExpr(newInnerIR, newTypes(0).asInstanceOf[ArithTypeT])

  override def argsBuild(args: Seq[Expr]): CGenExpr = Split(args(0), chunkSize, this.rightmostViewType)
}

object Split {
  def apply(input: Expr, chunkSize: ArithTypeT): SplitExpr = apply(input, chunkSize, innerSize = true, NoEffectType())

  def apply(input: Expr, chunkSize: ArithTypeT, vt: ViewTypeT): SplitExpr = apply(input, chunkSize, innerSize = true, vt)

  def apply(input: Expr, size: ArithTypeT, innerSize: Boolean, vt: ViewTypeT): SplitExpr = {
    val dtv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv = ArrayTypeVar(dtv, lentv)

    val (chunkSize: ArithExpr, numberOfChunks: ArithExpr) =
      if (innerSize)
        (size.ae, lentv.ae / size.ae)
      else
        (lentv.ae / size.ae, size.ae)

    val outseqt = ArrayType(ArrayType(dtv, chunkSize), numberOfChunks)

    SplitExpr({
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, CFunType(inseqtv, outseqt, vt))), input.t), input)
    }, chunkSize)
  }

  def unapply(expr: SplitExpr): Some[(Expr, ArithTypeT, ViewTypeT, Type)] = {
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.rightmostViewType, expr.t))
  }
}

case class JoinExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): JoinExpr = JoinExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Join(args.head, this.rightmostViewType)
}

object Join extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): JoinExpr = apply(input, NoEffectType())

  def apply(input: Expr, vt: ViewTypeT): JoinExpr = JoinExpr({
    val dtv = CGenDataTypeVar()
    val innerlentv = ArithTypeVar()
    val outerlentv = ArithTypeVar()
    val inseqtv = ArrayTypeVar(ArrayTypeVar(dtv, innerlentv), outerlentv)
    val outseqt = ArrayType(dtv, innerlentv.ae * outerlentv.ae)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, CFunType(inseqtv, outseqt, vt))), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): JoinExpr = apply(inputs.head)

  def unapply(expr: JoinExpr): Some[(Expr, LazyTypeT, Type)] =
    Some((expr.args.head, expr.rightmostViewType.asInstanceOf[LazyTypeT], expr.t))
}

case class TransposeExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): TransposeExpr = TransposeExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Transpose(args.head, this.rightmostViewType)
}

object Transpose extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr): TransposeExpr = apply(input, NoEffectType())

  def apply(input: Expr, vt: ViewTypeT): TransposeExpr = TransposeExpr({
    val dtv = CGenDataTypeVar()
    val innerlentv = ArithTypeVar()
    val outerlentv = ArithTypeVar()
    val inseqtv = ArrayTypeVar(ArrayTypeVar(dtv, innerlentv), outerlentv)
    val outseqt = ArrayType(ArrayType(dtv, outerlentv), innerlentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, CFunType(inseqtv, outseqt, vt))), input.t), input)
  })

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): TransposeExpr = apply(inputs.head)

  def unapply(expr: TransposeExpr): Some[(Expr, Type)] =
    Some((expr.args.head, expr.t))
}

case class AllocExpr(innerIR: Expr, tt: Type, at: ArithTypeT) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): AllocExpr = AllocExpr(newInnerIR, newTypes(0), newTypes(1).asInstanceOf[ArithTypeT])

  override def argsBuild(args: Seq[Expr]): CGenExpr = Alloc(tt, toAk(at))

  override def types: Seq[Type] = Seq(tt, at)

  def ak = toAk(at)
}

// A |-> Dest[A]
object Alloc {
  def apply(t: Type, ak: AllocKind): AllocExpr = AllocExpr({
    AllocExpr(Value(AddressType(t)), t, toAt(ak))
  }, t, toAt(ak))

def unapply(expr: AllocExpr): Some[(Type, AllocKind, Type)] =
    Some((expr.t.asInstanceOf[AddressType].ptrType, expr.ak, expr.t))
}

case class TypeConvExpr(innerIR: Expr, toT: Type) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): TypeConvExpr = TypeConvExpr(newInnerIR, toT)

  override def argsBuild(args: Seq[Expr]): CGenExpr = TypeConv(args(0), toT)

  override def types: Seq[Type] = Seq(toT)
}

// A |-> B -> A
// TODO: the lift rule for typeconv
object TypeConv {
  def apply(in: Expr, toT: Type): TypeConvExpr = TypeConvExpr({
    val atv = CGenDataTypeVar()
    val intv = toT
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(atv, CFunType(atv, intv))), in.t), in)
  }, toT)

  def unapply(expr: TypeConvExpr): Some[(Expr, Type)] = {
    Some((expr.args.head, expr.t))
  }
}

case class FreeExpr(innerIR: Expr, at: ArithTypeT) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): FreeExpr = FreeExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])

  override def argsBuild(args: Seq[Expr]): CGenExpr = Free(args(0), toAk(at))

  override def types: Seq[Type] = Seq(at)

  def ak: AllocKind = toAk(at)
}

// A |-> Dest[A]
object Free {
  def apply(in: Expr, ak: AllocKind): FreeExpr = FreeExpr({
    val intv = AddressTypeVar()
    val out = VoidType()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intv, CFunType(intv, out))), in.t), in)
  }, toAt(ak))

  def unapply(expr: FreeExpr): Some[(Expr, AllocKind, Type)] =
    Some((expr.args.head, expr.ak, expr.t))
}

case class RangeExpr(innerIR: Expr, n: ArithTypeT) extends CGenExpr {
  override def types: Seq[Type] = Seq(n)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): RangeExpr = RangeExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])

  override def argsBuild(args: Seq[Expr]): CGenExpr = Range(args(0), args(1), n)
}

object Range extends BuiltinExprFun {
  override def args: Int = 3

  def apply(start: Expr, step: Expr, n: ArithTypeT): RangeExpr = RangeExpr({
    val starttv = IntTypeVar(32)
    val steptv = IntTypeVar(32)
    val outt = ArrayType(IntType(32), n)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(starttv, steptv), CFunType(Seq(starttv, steptv), outt, SrcViewType()))), Seq(start.t, step.t)), Seq(start, step))
  }, n)

  def unapply(expr: RangeExpr): Some[(Expr, Expr, ArithTypeT, Type)] = {
    Some((expr.args.head, expr.args(1), expr.types.head.asInstanceOf[ArithTypeT], expr.t))
  }

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): Expr = Range(inputs.head, inputs.tail.head, types.head.asInstanceOf[ArithTypeT])
}

case class AccMapExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): AccMapExpr = AccMapExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = this
}

object AccMap {

  // ([U]n -> T -> U) -> [U]n -> [T]n -> [U]n
  def apply(f: Expr, init: Expr, input: Expr): AccMapExpr = AccMapExpr({
    val utv = CGenDataTypeVar()
    val ttv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val uarrtv = ArrayTypeVar(utv, lentv)
    val ftv = FunTypeVar(uarrtv, FunTypeVar(ttv, utv))
    val inttv = ArrayTypeVar(ttv, lentv)
    val outt = ArrayType(utv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, uarrtv, inttv), CFunType(Seq(ftv, uarrtv, inttv), outt))), Seq(f.t, init.t, input.t)), Seq(f, init, input))
  })

  def unapply(expr: AccMapExpr): Some[(Expr, Expr, Expr, Type)] = {
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
  }
}

case class IdxAccMapExpr(innerIR: Expr, n: ArithTypeT) extends CGenExpr {
  override def types: Seq[Type] = Seq(n)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IdxAccMapExpr = IdxAccMapExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])

  override def argsBuild(args: Seq[Expr]): CGenExpr = this
}

object IdxAccMap {
  // n |-> ([U]n -> idx -> U) -> [U]n -> [U]n
  def apply(f: Expr, init: Expr, n: ArithTypeT): IdxAccMapExpr = IdxAccMapExpr({
    val utv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val uarrtv = ArrayTypeVar(utv, lentv)
    val idxtv = IntType(32)
    val ftv = FunTypeVar(uarrtv, FunTypeVar(idxtv, utv))
    val outt = ArrayType(utv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, uarrtv), CFunType(Seq(ftv, uarrtv), outt))), Seq(f.t, init.t)), Seq(f, init))
  }, n)

  def unapply(expr: IdxAccMapExpr): Some[(Expr, Expr, ArithTypeT, Type)] = {
    Some((expr.args.head, expr.args(1), expr.types.head.asInstanceOf[ArithTypeT], expr.t))
  }
}

case class PermuteExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): PermuteExpr = PermuteExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Permute(args(0), args(1), this.rightmostViewType)
}

object Permute {
  // s |-> (Idx -> Idx) -> [T]n -> [T]n
  def apply(f: Expr, in: Expr, vt: ViewTypeT = SrcViewType()): PermuteExpr = PermuteExpr({
    val idxt = IntType(32)
    val ttv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val inarrtv = ArrayTypeVar(ttv, lentv)
    val ftv = FunTypeVar(idxt, idxt)
    val outt = ArrayType(ttv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, inarrtv), CFunType(Seq(ftv, inarrtv), outt, vt))), Seq(f.t, in.t)), Seq(f, in))
  })

  def unapply(expr: PermuteExpr): Some[(Expr, Expr, Type)] = Some((expr.args.head, expr.args(1), expr.t))
}

case class SlideExpr(innerIR: Expr, windowWidth: ArithTypeT) extends CGenExpr {
  override def types: Seq[Type] = Seq(windowWidth)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): SlideExpr = SlideExpr(newInnerIR, newTypes.head.asInstanceOf[ArithTypeT])

  override def argsBuild(args: Seq[Expr]): CGenExpr = Slide(args(0), windowWidth)
}

object Slide extends BuiltinExprFun {
  override def args: Int = 1

  def apply(input: Expr, windowWidth: ArithTypeT): SlideExpr = SlideExpr({
    val dtv = CGenDataTypeVar()
    val lentv = ArithTypeVar()
    val inseqtv = ArrayTypeVar(dtv, lentv)
    val outseqt = ArrayType(ArrayType(dtv, windowWidth), lentv - windowWidth.ae + 1)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(inseqtv, CFunType(inseqtv, outseqt, SrcViewType()))), input.t), input)

  }, windowWidth)

  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): SlideExpr = apply(inputs.head, types.head.asInstanceOf[ArithTypeT])

  def unapply(expr: SlideExpr): Some[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.types.head.asInstanceOf[ArithTypeT], expr.t))
}

case class IterateExpr(innerIR: Expr) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IterateExpr = IterateExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Iterate(args(0), args(1), args(2))
}

object Iterate {
  def apply(in: Expr, f: Expr, m: Expr): IterateExpr = IterateExpr({
    val intv = CGenDataTypeVar()
    val ftv = CFunTypeVar(intv, intv)
    val mtv = IntTypeVar()
    val outseqt = intv
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(intv, ftv, mtv), CFunType(Seq(intv, ftv, mtv), outseqt))), Seq(in.t, f.t, m.t)),
      Seq(in, f, m))
  })

  def unapply(expr: IterateExpr): Some[(Expr, Expr, Expr, Type)] =
    Some((expr.args.head, expr.args(1), expr.args(2), expr.t))
}

/**
 * not supported any more
 * @param hoistCount , unconditional hoist count, used by Hoist. Allow it to be hoisted
 *                   even if it is not a loop invariant
 */
case class LoopExpr(innerIR: Expr, hoistCount: Int) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): LoopExpr = LoopExpr(newInnerIR, hoistCount)

  override def argsBuild(args: Seq[Expr]): CGenExpr = Loop(args(0), args(1), hoistCount)
}

object Loop {
  //  override def args: Int = 2
  def apply(f: Expr, m: Expr, hoistCount: Int = 0): LoopExpr = LoopExpr({
    val ftv = CFunTypeVar(IntType(32), VoidType())
    val mtv = IntTypeVar()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(ftv, mtv), CFunType(Seq(ftv, mtv), VoidType()))), Seq(f.t, m.t)),
      Seq(f, m))
  }, hoistCount)

  //  override protected def apply(inputs: Seq[Expr], types: Seq[Type]): LoopExpr = LoopExpr(inputs.head)
  def unapply(expr: LoopExpr): Some[(Expr, Expr, Int, Type)] =
    Some((expr.args.head, expr.args(1), expr.hoistCount, expr.t))
}