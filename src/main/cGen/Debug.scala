package cGen

/**
 * Global flags for c-code-gen compiler
 */
object Debug {
  // 0 means no profile info, 1 means standard info, 2 means verbose info
  var PROFILE: Int = try {
    scala.util.Properties.envOrElse("PROFILE", "1").toInt
  } catch {
    case _: Throwable => 0
  }

  // an RELATIVE path to the directory the where tests are generated
  var SHIRC_PATH = "./C"
}


