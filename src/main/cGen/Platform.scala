package cGen

import core.Type

abstract class Platform {
  val DOUBLE_SIZE: Int
  val FLOAT_SIZE: Int
  val POINTER_SIZE: Int
  val IS_FLATTEN: Boolean

  def getSizeForTupleSelect(t: Type): Int
  def getFlattenArrayElemType(t: Type): Int
  def getFlattenArrayOffset(t: Type)(implicit first: Boolean = true): Int
  def getFlattenArrayType(t: ArrayType): ArrayType

  def nameIntType(width: Int): String = s"int$width"
  def nameUIntType(width: Int): String = s"uint$width"
}

trait Bits64 extends {
  val DOUBLE_SIZE = 8;
  val POINTER_SIZE = 8;
  val FLOAT_SIZE = 4;
  // .... TBD

  def getSizeForTupleSelect(t: Type): Int = t match {
    case DoubleType() => DOUBLE_SIZE
    case AddressType(_) => POINTER_SIZE
    case IntType(width, _) => width.ae.evalInt / 8
    // a pointer size for TupleSelect. the actually array size should be: getSize(et) * len.ae.evalInt
    case ArrayType(et, len) => POINTER_SIZE
  }

  def getFlattenArrayElemType(t: Type): Int = t match {
    case ArrayType(et, _) => getFlattenArrayElemType(et)
    case DoubleType() => DOUBLE_SIZE
    case IntType(width, _) => width.ae.evalInt / 8
    case FloatType() => FLOAT_SIZE
  }

  def getFlattenArrayOffset(t: Type)(implicit first: Boolean = true): Int = t match {
    case ArrayType(et, _) if first => getFlattenArrayOffset(et)(false)
    case ArrayType(et, len) if !first => len.ae.evalInt * getFlattenArrayOffset(et)
    case _ => 1
  }

  def getFlattenArrayType(t: ArrayType): ArrayType = ArrayType(t.et, getFlattenArrayLength(t))

  private def getFlattenArrayLength(t: Type): Int = t match {
    case ArrayType(et, len) => len.ae.evalInt * getFlattenArrayLength(et)
    case _ => 1
  }
}

object Linux64 extends Platform with Bits64 {
  override val IS_FLATTEN: Boolean = false
}

/**
  * A version of the Linux64 platform with flat arrays.
  */
object FlatLinux64 extends Platform with Bits64 {
  override val IS_FLATTEN: Boolean = true
}

object OpenCL64 extends Platform with Bits64 {
  override val IS_FLATTEN: Boolean = true
}
