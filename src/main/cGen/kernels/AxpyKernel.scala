package cGen.kernels

import cGen.{ArrayType, CLambda, DoubleType, Extern}
import eqsat.{DeBruijnLambda, DeBruijnParamUse}
import core.{Expr, ParamDef, ParamUse, TypeChecker}

/**
  * A kernel that computes the sum of two vectors.
  * @param vectorLength The length of the vector to sum.
  */
case class AxpyKernel(vectorLength: Int) extends Kernel {
  def name: String = "axpy"
  def suite: String = "custom"
  def input: Expr = {
    implicit val t = DoubleType
    val N = vectorLength
    val alpha = ParamDef(t())
    val A = ParamDef(ArrayType(t(), N))
    val B = ParamDef(ArrayType(t(), N))
    CLambda(Seq(alpha, A, B).reverse, TypeChecker.check(
      LowLevelLib.vectorAdd(
        LowLevelLib.vectorScalarProduct(ParamUse(A), ParamUse(alpha)),
        ParamUse(B))))
  }

  def oracles: Seq[Expr] = {
    implicit val t = DoubleType
    val vectorType = ArrayType(t(), vectorLength)
    Seq(
      TypeChecker.check(
        DeBruijnLambda(t(), DeBruijnLambda(vectorType, DeBruijnLambda(vectorType,
          Extern.AXPY(DeBruijnParamUse(2, t()), DeBruijnParamUse(1, vectorType), DeBruijnParamUse(0, vectorType)))))))
  }

  override def maxExpansionSteps: Int = 3
}
