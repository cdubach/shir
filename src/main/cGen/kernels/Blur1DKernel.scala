package cGen.kernels

import eqsat.{DeBruijnLambda, DeBruijnParamUse, DeBruijnTransform}
import cGen.primitive.Idx
import cGen._
import core._

/**
  * A kernel that applies a blurring stencil to a vector.
  * @param vectorSize The size of the vector to blur.
  */
case class Blur1DKernel(vectorSize: Int) extends Kernel {
  override def name: String = "blur1d"
  override def suite: String = "custom"
  override def input: Expr = {
    implicit val t = DoubleType
    val len = vectorSize
    val in = ParamDef(ArrayType(t(), len))
    CLambda(
      in,
      LowLevelLib.pad1D(
        TypeChecker.check(
          LowLevelLib.stencil1D(ParamUse(in), LowLevelLib.constArray(Constant(1.0 / 3.0, t()), 3))),
        1,
        Idx(ParamUse(in), _, EagerType())))
  }

  override def oracles: Seq[Expr] = {
    // λ[double x 1024].
    //   (λ[double x 1022].
    //     build 1024 (λi32. if (p_0(i32) < 1)
    //                       then (if (p_0(i32) < 1022)
    //                             then p_2([double x 1024])[0]
    //                             else p_1([double x 1022])[p_0(i32)])
    //                       else p_2([double x 1024])[1023]))
    //   dgemv(
    //     1022,
    //     3,
    //     1.0,
    //     build 1022 (λi32. build 3 (λi32. p_2([double x 1024])[p_0(i32) + p_1(i32)])),
    //     build 3 (λi32. 0.3333333333333333),
    //     0.0,
    //     build 1022 (λi32. 1.0))

    val t = DoubleType()
    val i32 = IntType(32)

    val zeroVectors = Seq(
      (Constant(0.0, t), Build(DeBruijnLambda(i32, Constant(1.0, t)), vectorSize - 2)),
      (Constant(1.0, t), Build(DeBruijnLambda(i32, Constant(0.0, t)), vectorSize - 2)),
      (Constant(0.0, t), Build(DeBruijnLambda(i32, Constant(0.0, t)), vectorSize - 2))
    )

    val oneThirdVectors = Seq(
      (Constant(1.0, t), Build(DeBruijnLambda(i32, Constant(1.0 / 3, t)), 3)),
      (Constant(1.0 / 3, t), Build(DeBruijnLambda(i32, Constant(1.0, t)), 3))
    )

    oneThirdVectors.flatMap(oneThirdVec =>
      zeroVectors.map(zeroVec => {
        implicit val t = DoubleType
        val len = vectorSize
        val in = ParamDef(ArrayType(t(), len))
        DeBruijnTransform.forward(
          CLambda(
          in,
          LowLevelLib.pad1D(
            TypeChecker.check(
              Extern.GEMV(
                oneThirdVec._1,
                Build(DeBruijnLambda(i32,
                  Build(DeBruijnLambda(i32,
                    Idx(
                      ParamUse(in),
                      BinaryOp(DeBruijnParamUse(0, i32), DeBruijnParamUse(1, i32), AlgoOp.Add),
                      EagerType())),
                    3)),
                  vectorSize - 2),
                oneThirdVec._2,
                zeroVec._1,
                zeroVec._2)),
            1,
            Idx(ParamUse(in), _, EagerType()))))
      }))
  }

  override def maxExpansionSteps: Int = 3
  override def maxSimplificationSteps: Int = 0
}
