package cGen.kernels

import eqsat.DeBruijnTransform
import cGen.{ArrayType, CLambda, DoubleType, Extern}
import core.{Expr, ParamDef, ParamUse}

/**
  * A kernel that performs a GEMV operation.
  * @param N The size of the vectors to multiply. Each matrix will be `N` x `N` and each vector will be `N`.
  */
case class GemvKernel(N: Int) extends Kernel {
  override def name: String = "gemv"
  override def suite: String = "custom"
  override def input: Expr = {
    implicit val t = DoubleType
    val alpha = ParamDef(t())
    val beta = ParamDef(t())
    val A = ParamDef(ArrayType(ArrayType(t(), N), N))
    val B = ParamDef(ArrayType(t(), N))
    val C = ParamDef(ArrayType(t(), N))
    CLambda(Seq(alpha, A, B, beta, C).reverse,
      LowLevelLib.vectorAdd(
          LowLevelLib.matrixVectorProduct(
            LowLevelLib.matrixScalarProduct(ParamUse(A), ParamUse(alpha)),
            ParamUse(B)),
        LowLevelLib.vectorScalarProduct(ParamUse(C), ParamUse(beta))))
  }

  override def oracles: Seq[Expr] = {
    implicit val t = DoubleType
    val alpha = ParamDef(t())
    val beta = ParamDef(t())
    val A = ParamDef(ArrayType(ArrayType(t(), N), N))
    val B = ParamDef(ArrayType(t(), N))
    val C = ParamDef(ArrayType(t(), N))

    Seq(
      DeBruijnTransform.forward(
        CLambda(Seq(alpha, A, B, beta, C).reverse,
          Extern.GEMV(ParamUse(alpha), ParamUse(A), ParamUse(B), ParamUse(beta), ParamUse(C)))))
  }

  override def maxExpansionSteps: Int = 11
  override def maxSimplificationSteps: Int = 0

  //  override def idiomRules: Seq[Rewrite] = Seq(BlasIdiomRules(DefaultSmallest).gemmRule)
}
