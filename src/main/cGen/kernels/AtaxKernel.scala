package cGen.kernels
import eqsat.DeBruijnTransform
import cGen.{ArrayType, CLambda, DoubleType, Extern}
import core.{Expr, ParamDef, ParamUse}

case class AtaxKernel(N: Int, M: Int) extends Kernel {
  /**
    * The name of the kernel/benchmark.
    */
  override def name: String = "atax"

  /**
    * The name of the benchmark suite that defines the kernel/benchmark.
    */
  override def suite: String = "polybench"

  /**
    * The untransformed kernel, as an expression.
    */
  override def input: Expr = {
    val t = DoubleType()
    val A = ParamDef(ArrayType(ArrayType(t, N), M))
    val x = ParamDef(ArrayType(t, N))
    CLambda(
      Seq(A, x).reverse,
      LowLevelLib.matrixVectorProduct(
        LowLevelLib.matrixTranspose(ParamUse(A)),
        LowLevelLib.matrixVectorProduct(ParamUse(A), ParamUse(x))))
  }

  /**
    * A set of perfectly transformed versions of the kernel, as expressions.
    */
  override def oracles: Seq[Expr] = {
    implicit val t = DoubleType
    val A = ParamDef(ArrayType(ArrayType(t(), N), M))
    val x = ParamDef(ArrayType(t(), N))

    val zeroVectors = Seq(
      (LowLevelLib.zero, LowLevelLib.constArray(LowLevelLib.one, N)),
      (LowLevelLib.one, LowLevelLib.constArray(LowLevelLib.zero, N)),
      (LowLevelLib.zero, LowLevelLib.constArray(LowLevelLib.zero, N)))

    for { (b1, c1) <- zeroVectors; (b2, c2) <- zeroVectors }
      yield DeBruijnTransform.forward(
        CLambda(
          Seq(A, x).reverse,
          Extern.GEMV(
            LowLevelLib.one,
            LowLevelLib.matrixTranspose(ParamUse(A)),
            Extern.GEMV(LowLevelLib.one, ParamUse(A), ParamUse(x), b2, c2),
            b1,
            c1)))
  }

  override def maxExpansionSteps: Int = 7
  override def maxSimplificationSteps: Int = 0
}
