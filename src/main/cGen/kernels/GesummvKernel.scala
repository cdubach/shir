package cGen.kernels
import eqsat.{DeBruijnLambda, DeBruijnParamUse, DeBruijnTransform}
import cGen.{ArrayType, CLambda, DoubleType, Extern}
import core.{Expr, ParamDef, ParamUse, TypeChecker}

case class GesummvKernel(N: Int) extends Kernel {
  /**
    * The name of the kernel/benchmark.
    */
  override def name: String = "gesummv"

  /**
    * The name of the benchmark suite that defines the kernel/benchmark.
    */
  override def suite: String = "polybench"

  /**
    * The untransformed kernel, as an expression.
    */
  override def input: Expr = {
    val t = DoubleType()
    val alpha = ParamDef(t)
    val beta = ParamDef(t)
    val A = ParamDef(ArrayType(ArrayType(t, N), N))
    val B = ParamDef(ArrayType(ArrayType(t, N), N))
    val x = ParamDef(ArrayType(t, N))
    CLambda(
      Seq(alpha, beta, A, B, x).reverse,
      LowLevelLib.vectorAdd(
          LowLevelLib.matrixVectorProduct(
            LowLevelLib.matrixScalarProduct(ParamUse(A), ParamUse(alpha)),
            ParamUse(x)),
          LowLevelLib.matrixVectorProduct(
            LowLevelLib.matrixScalarProduct(ParamUse(B), ParamUse(beta)),
            ParamUse(x))))
  }

  /**
    * A set of perfectly transformed versions of the kernel, as expressions.
    */
  override def oracles: Seq[Expr] = {
    Seq()
    implicit val t = DoubleType
    val matrixType = ArrayType(ArrayType(t(), N), N)
    val vectorType = ArrayType(t(), N)
    val allZeros = TypeChecker.check(DeBruijnTransform.forward(LowLevelLib.constArray(LowLevelLib.one, N)))
    Seq(
      TypeChecker.check(
        DeBruijnLambda(t(), DeBruijnLambda(t(),
          DeBruijnLambda(matrixType, DeBruijnLambda(matrixType, DeBruijnLambda(vectorType,
            Extern.AXPY(
              LowLevelLib.one,
              Extern.GEMV(
                DeBruijnParamUse(4, t()),
                DeBruijnParamUse(2, matrixType),
                DeBruijnParamUse(0, vectorType),
                LowLevelLib.zero,
                allZeros),
              Extern.GEMV(
                DeBruijnParamUse(3, t()),
                DeBruijnParamUse(1, matrixType),
                DeBruijnParamUse(0, vectorType),
                LowLevelLib.zero,
                allZeros)))))))))
  }

  override def maxExpansionSteps: Int = 11
  override def maxSimplificationSteps: Int = 0
}
