package cGen.kernels

import eqsat.DeBruijnTransform
import cGen.{ArrayType, CLambda, DoubleType, Extern}
import core.{Expr, ParamDef, ParamUse, TypeChecker}

/**
  * A kernel that performs only two matrix multiplications, no additional computation.
  *
  * @param matrixSize The size of the matrices to multiply. Each matrix will be `matrixSize` x `matrixSize`.
  */
case class SlimTwoMMKernel(matrixSize: Int) extends Kernel {
  override def name: String = "slim-2mm"
  override def suite: String = "custom"
  override def input: Expr = {
    implicit val t = () => DoubleType()
    val Q = matrixSize
    val P = matrixSize
    val R = matrixSize
    val S = matrixSize

    val A = ParamDef(ArrayType(ArrayType(t(), Q), P))
    val B = ParamDef(ArrayType(ArrayType(t(), R), Q))
    val C = ParamDef(ArrayType(ArrayType(t(), S), R))

    CLambda(Seq(A, B, C).reverse, {
      LowLevelLib.matrixMatrixProduct(
        LowLevelLib.matrixMatrixProduct(ParamUse(A), ParamUse(B)),
        ParamUse(C))
    })
  }

  override def oracles: Seq[Expr] = {
    implicit val t = () => DoubleType()
    val Q = matrixSize
    val P = matrixSize
    val R = matrixSize
    val S = matrixSize

    val A = ParamDef(ArrayType(ArrayType(t(), Q), P))
    val B = ParamDef(ArrayType(ArrayType(t(), R), Q))
    val C = ParamDef(ArrayType(ArrayType(t(), S), R))

    Seq(
      TypeChecker.check(
        DeBruijnTransform.forward(
          CLambda(
            Seq(A, B, C).reverse,
            Extern.MatrixMul(
              Extern.MatrixMul(ParamUse(A), ParamUse(B)),
              ParamUse(C))))))
  }

  override def maxExpansionSteps: Int = 7
  override def maxSimplificationSteps: Int = 0
}
