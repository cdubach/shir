package cGen.kernels

import cGen.{ArrayType, CLambda, DoubleType, Extern}
import eqsat.DeBruijnTransform
import core.{Expr, ParamDef, ParamUse, TypeChecker}

/**
  * A kernel that performs two matrix multiplications.
  *
  * @param matrixSize The size of the matrices to multiply. Each matrix will be `matrixSize` x `matrixSize`.
  */
case class TwoMMKernel(matrixSize: Int) extends Kernel {
  override def name: String = "2mm"
  override def suite: String = "polybench"
  override def input: Expr = {
    implicit val t = () => DoubleType()
    val Q = matrixSize
    val P = matrixSize
    val R = matrixSize
    val S = matrixSize

    val A = ParamDef(ArrayType(ArrayType(t(), Q), P))
    val B = ParamDef(ArrayType(ArrayType(t(), R), Q))
    val C = ParamDef(ArrayType(ArrayType(t(), S), R))
    val D = ParamDef(ArrayType(ArrayType(t(), S), P))
    val alpha = ParamDef(t())
    val beta = ParamDef(t())

    CLambda(Seq(alpha, beta, A, B, C, D).reverse, {
      LowLevelLib.matrixAdd(
        LowLevelLib.matrixScalarProduct(
          LowLevelLib.matrixMatrixProduct(
            LowLevelLib.matrixMatrixProduct(ParamUse(A), ParamUse(B)),
            ParamUse(C)),
          ParamUse(alpha)),
        LowLevelLib.matrixScalarProduct(ParamUse(D), ParamUse(beta)))
    })
  }

  override def oracles: Seq[Expr] = {
    implicit val t = () => DoubleType()
    val Q = matrixSize
    val P = matrixSize
    val R = matrixSize
    val S = matrixSize

    val A = ParamDef(ArrayType(ArrayType(t(), Q), P))
    val B = ParamDef(ArrayType(ArrayType(t(), R), Q))
    val C = ParamDef(ArrayType(ArrayType(t(), S), R))
    val D = ParamDef(ArrayType(ArrayType(t(), S), P))
    val alpha = ParamDef(t())
    val beta = ParamDef(t())

    Seq(
      TypeChecker.check(
        DeBruijnTransform.forward(
          CLambda(
            Seq(alpha, beta, A, B, C, D).reverse,
            Extern.GEMM(
              ParamUse(alpha),
              Extern.MatrixMul(ParamUse(A), ParamUse(B)),
              ParamUse(C),
              ParamUse(beta),
              ParamUse(D))))))
  }

  override def maxExpansionSteps: Int = 6
  override def maxSimplificationSteps: Int = 3

//  override def idiomRules: Seq[Rewrite] = Seq(BlasIdiomRules(DefaultSmallest).gemmRule)
}
