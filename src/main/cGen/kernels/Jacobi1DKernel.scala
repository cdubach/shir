package cGen.kernels

import cGen.{ArrayType, CLambda, DoubleType, Ifold, IntType}
import eqsat.{DeBruijnLambda, DeBruijnParamUse}
import core.{Expr, FunctionCall, ParamDef, ParamUse, TypeChecker}

case class Jacobi1DKernel(vectorSize: Int, steps: Int) extends Kernel {
  /**
    * The name of the kernel/benchmark.
    */
  override def name: String = "jacobi1d"

  /**
    * The name of the benchmark suite that defines the kernel/benchmark.
    */
  override def suite: String = "polybench"

  private val blurKernel = Blur1DKernel(vectorSize)

  /**
    * The untransformed kernel, as an expression.
    */
  override def input: Expr = {
    val t = DoubleType
    val A = ParamDef(ArrayType(t(), vectorSize))
    CLambda(A, {
      Ifold(
        {
          val i = ParamDef(IntType(32))
          val acc = ParamDef(A.t)
          CLambda(Seq(acc, i), FunctionCall(blurKernel.input, ParamUse(acc)))
        },
        ParamUse(A),
        steps
      )
    })
  }

  /**
    * A set of perfectly transformed versions of the kernel, as expressions.
    */
  override def oracles: Seq[Expr] = blurKernel.oracles.map(impl => {
    val t = DoubleType
    val arrType = ArrayType(t(), vectorSize)
    TypeChecker.check(
      DeBruijnLambda(arrType,
        Ifold(
          DeBruijnLambda(IntType(32),
            DeBruijnLambda(arrType,
              impl.asInstanceOf[DeBruijnLambda].betaReduce(DeBruijnParamUse(0, arrType)))),
          DeBruijnParamUse(0, arrType),
          steps)))
  })

  override def maxExpansionSteps: Int = 3
  override def maxSimplificationSteps: Int = 0
}
