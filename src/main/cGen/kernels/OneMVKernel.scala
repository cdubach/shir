package cGen.kernels

import cGen.{ArrayType, Build, CLambda, DoubleType, Extern, IntType}
import eqsat.{DeBruijnLambda, DeBruijnParamUse}
import core.{Expr, ParamDef, ParamUse, TypeChecker}

/**
  * A kernel that performs a single matrix-vectors multiplication.
  * @param vectorSize The length of the vector.
  */
case class OneMVKernel(vectorSize: Int) extends Kernel {
  override def name: String = "1mv"
  override def suite: String = "custom"
  override def input: Expr = {
    implicit val t = DoubleType
    val N = vectorSize
    val A = ParamDef(ArrayType(ArrayType(t(), N), N))
    val B = ParamDef(ArrayType(t(), N))
    CLambda(Seq(A, B).reverse, LowLevelLib.matrixVectorProduct(ParamUse(A), ParamUse(B)))
  }

  override def oracles: Seq[Expr] = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val vectorType = ArrayType(t(), vectorSize)
    val matrixType = ArrayType(vectorType, vectorSize)
    Seq(
      TypeChecker.check(
        DeBruijnLambda(matrixType, DeBruijnLambda(vectorType,
          Extern.GEMV(
            LowLevelLib.one,
            DeBruijnParamUse(1, matrixType),
            DeBruijnParamUse(0, vectorType),
            LowLevelLib.one,
            Build(DeBruijnLambda(i32, LowLevelLib.zero), vectorSize))))))
  }

  override def maxExpansionSteps: Int = 2
  override def maxSimplificationSteps: Int = 0
}
