package cGen.kernels

import eqsat.DeBruijnTransform
import cGen.{ArrayType, CLambda, DoubleType, Extern}
import core.{Expr, ParamDef, ParamUse}

/**
  * A kernel that performs a GEMM operation.
  * @param NI The first dimension of the left-hand matrix in the multiplication.
  * @param NJ The second dimension of the left-hand matrix in the multiplication.
  * @param NK The second dimension of the right-hand matrix in the multiplication.
  */
case class GEMMKernel(NI: Int, NJ: Int, NK: Int) extends Kernel {
  override def name: String = "gemm"
  override def suite: String = "polybench"
  override def input: Expr = {
    implicit val t = DoubleType
    val alpha = ParamDef(t())
    val beta = ParamDef(t())
    val A = ParamDef(ArrayType(ArrayType(t(), NJ), NI))
    val B = ParamDef(ArrayType(ArrayType(t(), NK), NJ))
    val C = ParamDef(ArrayType(ArrayType(t(), NK), NI))
    CLambda(Seq(alpha, A, B, beta, C).reverse,
      LowLevelLib.matrixAdd(
        LowLevelLib.matrixMatrixProduct(
          LowLevelLib.matrixScalarProduct(ParamUse(A), ParamUse(alpha)),
          ParamUse(B)),
        LowLevelLib.matrixScalarProduct(ParamUse(C), ParamUse(beta))))
  }

  override def oracles: Seq[Expr] = {
    implicit val t = DoubleType
    val alpha = ParamDef(t())
    val beta = ParamDef(t())
    val A = ParamDef(ArrayType(ArrayType(t(), NJ), NI))
    val B = ParamDef(ArrayType(ArrayType(t(), NK), NJ))
    val C = ParamDef(ArrayType(ArrayType(t(), NK), NI))

    Seq(
      DeBruijnTransform.forward(
        CLambda(Seq(alpha, A, B, beta, C).reverse,
          Extern.GEMM(ParamUse(alpha), ParamUse(A), ParamUse(B), ParamUse(beta), ParamUse(C)))))
  }

  override def maxExpansionSteps: Int = 11
  override def maxSimplificationSteps: Int = 0

//  override def idiomRules: Seq[Rewrite] = Seq(BlasIdiomRules(DefaultSmallest).gemmRule)
}
