package cGen.kernels

import cGen.primitive.Idx
import cGen.{ArrayType, Build, CLambda, DoubleType, EagerType, IntType}
import core.{Expr, ParamDef, ParamUse}

case class GemverKernel(N: Int) extends Kernel {
  /**
    * The name of the kernel/benchmark.
    */
  override def name: String = "gemver"

  /**
    * The name of the benchmark suite that defines the kernel/benchmark.
    */
  override def suite: String = "polybench"

  /**
    * The untransformed kernel, as an expression.
    */
  override def input: Expr = {
    val i32 = IntType(32)
    val t = DoubleType()
    val alpha = ParamDef(t)
    val beta = ParamDef(t)
    val A = ParamDef(ArrayType(ArrayType(t, N), N))
    val u1 = ParamDef(ArrayType(t, N))
    val u2 = ParamDef(ArrayType(t, N))
    val v1 = ParamDef(ArrayType(t, N))
    val v2 = ParamDef(ArrayType(t, N))
    val x = ParamDef(ArrayType(t, N))
    val y = ParamDef(ArrayType(t, N))
    val z = ParamDef(ArrayType(t, N))
    val w = ParamDef(ArrayType(t, N))

    val valueOfB = Build({
      val i = ParamDef(i32)
      CLambda(i,
        Build({
          val j = ParamDef(i32)
          CLambda(j,
            LowLevelLib.add(
              LowLevelLib.add(
                Idx(Idx(ParamUse(A), ParamUse(i), EagerType()), ParamUse(j), EagerType()),
                LowLevelLib.mul(Idx(ParamUse(u1), ParamUse(i), EagerType()), Idx(ParamUse(v1), ParamUse(j), EagerType()))),
              LowLevelLib.mul(Idx(ParamUse(u2), ParamUse(i), EagerType()), Idx(ParamUse(v2), ParamUse(j), EagerType())))
          )
        }, N))
    }, N)

    val B = ParamDef(ArrayType(ArrayType(t, N), N))
    val body = LowLevelLib.let(B, valueOfB, {
     val newX = LowLevelLib.vectorAdd(
       ParamUse(x),
       LowLevelLib.vectorScalarProduct(
         LowLevelLib.matrixVectorProduct(
           LowLevelLib.matrixTranspose(ParamUse(B)),
           ParamUse(y)),
         ParamUse(beta)))

      val xPlusZ = LowLevelLib.vectorAdd(newX, ParamUse(z))

      LowLevelLib.vectorAdd(
        ParamUse(w),
        LowLevelLib.matrixVectorProduct(
          LowLevelLib.matrixScalarProduct(ParamUse(B), ParamUse(alpha)),
          xPlusZ))
    })

    CLambda(Seq(alpha, beta, A, u1, u2, v1, v2, x, y, z, w).reverse, body)
  }

  /**
    * A set of perfectly transformed versions of the kernel, as expressions.
    */
  override def oracles: Seq[Expr] = {
    Seq()
  }

  override def maxExpansionSteps: Int = 5
}
