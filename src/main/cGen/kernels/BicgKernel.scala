package cGen.kernels
import eqsat.DeBruijnTransform
import cGen.{ArrayType, CLambda, DoubleType, Extern, Tuple}
import core.{Expr, ParamDef, ParamUse}

case class BicgKernel(N: Int, M: Int) extends Kernel {
  /**
    * The name of the kernel/benchmark.
    */
  override def name: String = "bicg"

  /**
    * The name of the benchmark suite that defines the kernel/benchmark.
    */
  override def suite: String = "polybench"

  /**
    * The untransformed kernel, as an expression.
    */
  override def input: Expr = {
    val t = DoubleType()
    val A = ParamDef(ArrayType(ArrayType(t, N), M))
    val r = ParamDef(ArrayType(t, N))
    val p = ParamDef(ArrayType(t, M))
    CLambda(
      Seq(A, r, p).reverse,
      Tuple(
        LowLevelLib.matrixVectorProduct(LowLevelLib.matrixTranspose(ParamUse(A)), ParamUse(r)),
        LowLevelLib.matrixVectorProduct(ParamUse(A), ParamUse(p))))
  }

  /**
    * A set of perfectly transformed versions of the kernel, as expressions.
    */
  override def oracles: Seq[Expr] = {
    implicit val t = DoubleType
    val A = ParamDef(ArrayType(ArrayType(t(), N), M))
    val r = ParamDef(ArrayType(t(), N))
    val p = ParamDef(ArrayType(t(), M))

    val zeroVectors = Seq(
      (LowLevelLib.zero, LowLevelLib.constArray(LowLevelLib.one, N)),
      (LowLevelLib.one, LowLevelLib.constArray(LowLevelLib.zero, N)),
      (LowLevelLib.zero, LowLevelLib.constArray(LowLevelLib.zero, N)))

    for { (b1, c1) <- zeroVectors; (b2, c2) <- zeroVectors }
      yield DeBruijnTransform.forward(
        CLambda(
          Seq(A, r, p).reverse,
          Tuple(
            Extern.GEMV(LowLevelLib.one, LowLevelLib.matrixTranspose(ParamUse(A)), ParamUse(r), b1, c1),
            Extern.GEMV(LowLevelLib.one, ParamUse(A), ParamUse(p), b2, c2))))
  }

  override def maxExpansionSteps: Int = 3
  override def maxSimplificationSteps: Int = 0
}
