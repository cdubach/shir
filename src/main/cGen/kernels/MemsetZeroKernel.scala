package cGen.kernels
import eqsat.DeBruijnLambda
import cGen.{ArrayType, Build, CLambda, Constant, DoubleType, Extern, IntType, Tuple}
import core.{Expr, ParamDef, ParamUse}

case class MemsetZeroKernel(N: Int) extends Kernel {
  /**
    * The name of the kernel/benchmark.
    */
  override def name: String = "memset"

  /**
    * The name of the benchmark suite that defines the kernel/benchmark.
    */
  override def suite: String = "custom"

  /**
    * The untransformed kernel, as an expression.
    */
  override def input: Expr = {
    val t = DoubleType()
    val i = ParamDef(IntType(32))
    CLambda(ParamDef(t), Build(CLambda(i, Constant(0.0, t)), N))
  }

  /**
    * A set of perfectly transformed versions of the kernel, as expressions.
    */
  override def oracles: Seq[Expr] = {
    val t = DoubleType()
    Seq(DeBruijnLambda(t, Extern.memset(0, ArrayType(t, N))))
  }

  override def maxExpansionSteps: Int = 1
  override def maxSimplificationSteps: Int = 0
}
