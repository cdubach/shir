package cGen.kernels

import cGen.{ArrayType, Build, CLambda, Constant, DoubleType, EagerType, Extern, IntType}
import eqsat.{DeBruijnLambda, DeBruijnParamUse, DeBruijnTransform}
import cGen.primitive.Idx
import core.{Expr, ParamDef, ParamUse, TypeChecker}

/**
  * A kernel that performs a single matrix multiplication, implemented as a sum of outer products.
  * @param matrixSize The size of the matrices to multiply. Each matrix will be `matrixSize` x `matrixSize`.
  */
case class OuterMMKernel(matrixSize: Int) extends Kernel {
  override def name: String = "outer-1mm"
  override def suite: String = "custom"
  override def input: Expr = {
    implicit val t = DoubleType
    val N = matrixSize
    val A = ParamDef(ArrayType(ArrayType(t(), N), N))
    val B = ParamDef(ArrayType(ArrayType(t(), N), N))
    val i = ParamDef(IntType(32))
    CLambda(
      Seq(A, B).reverse,
      LowLevelLib.reduce(
        Build(
          CLambda(i,
            LowLevelLib.outerProduct(
              Idx(ParamUse(A), ParamUse(i), EagerType()),
              Idx(LowLevelLib.matrixTranspose(ParamUse(B)), ParamUse(i), EagerType()))),
          N,
          EagerType()),
        LowLevelLib.constMatrix(Constant(0.0, t()), N, N),
        LowLevelLib.matrixAdd))
  }

  override def oracles: Seq[Expr] = {
    implicit val t = DoubleType
    val matrixType = ArrayType(ArrayType(DoubleType(), matrixSize), matrixSize)

    val zeroMatrices = Seq(
      (LowLevelLib.one, LowLevelLib.constMatrix(LowLevelLib.zero, matrixSize, matrixSize)),
      (LowLevelLib.zero, LowLevelLib.constMatrix(LowLevelLib.zero, matrixSize, matrixSize)),
      (LowLevelLib.zero, LowLevelLib.constMatrix(LowLevelLib.one, matrixSize, matrixSize))
    )

    for { (b, c) <- zeroMatrices }
      yield TypeChecker.check(
        DeBruijnTransform.forward(
          DeBruijnLambda(matrixType, DeBruijnLambda(matrixType,
            Extern.GEMM(
              LowLevelLib.one,
              DeBruijnParamUse(1, matrixType),
              DeBruijnParamUse(0, matrixType),
              b,
              c)))))
  }

  override def maxExpansionSteps: Int = 7
  override def maxSimplificationSteps: Int = 0
}
