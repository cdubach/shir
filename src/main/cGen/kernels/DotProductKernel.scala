package cGen.kernels

import cGen.{ArrayType, CLambda, DoubleType, Extern}
import eqsat.{DeBruijnLambda, DeBruijnParamUse}
import core.{Expr, ParamDef, ParamUse, TypeChecker}

/**
  * A kernel that performs a dot product.
  * @param vectorLength The length of the vector to perform a dot product on.
  */
case class DotProductKernel(vectorLength: Int) extends Kernel {
  def name: String = "dot"
  def suite: String = "custom"
  def input: Expr = {
    implicit val t = DoubleType
    val N = vectorLength
    val A = ParamDef(ArrayType(t(), N))
    val B = ParamDef(ArrayType(t(), N))
    CLambda(Seq(A, B).reverse, LowLevelLib.dotProduct(ParamUse(A), ParamUse(B)))
  }

  def oracles: Seq[Expr] = {
    val vectorType = ArrayType(DoubleType(), vectorLength)
    Seq(
      TypeChecker.check(
        DeBruijnLambda(vectorType, DeBruijnLambda(vectorType,
          Extern.DoubleDotProd(DeBruijnParamUse(0, vectorType), DeBruijnParamUse(1, vectorType))))),
      TypeChecker.check(
        DeBruijnLambda(vectorType, DeBruijnLambda(vectorType,
          Extern.DoubleDotProd(DeBruijnParamUse(1, vectorType), DeBruijnParamUse(0, vectorType))))))
  }

  override def maxExpansionSteps: Int = 0
  override def maxSimplificationSteps: Int = 5
}
