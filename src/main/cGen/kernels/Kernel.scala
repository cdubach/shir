package cGen.kernels

import cGen.IntType
import eqsat.Extractor.DefaultSmallest
import eqsat.{AnalysesBuilder, DeBruijnParamUse, DeBruijnTransform, DeferredMergingEGraph, EClassT, EGraph, EGraphNullObserver, EGraphObserver, ENodeT, ExprToText, ExtractionAnalysis, Extractor, HashConsEClass, HashConsENode, HierarchicalStopwatch, IterationLimitedSaturationStrategy, MinDepthAnalysis, Pattern, RebasingRewriteEngine, RebasingStrategy, Rewrite, SaturationStrategy, SimpleSaturationStrategy, Willsey2021EGraph}
import cGen.idioms.{BlasIdiomRules, ExtractionBasedSemanticRules, LowerFunctionalPrimitives, TimeComplexityExtractor}
import core.{Expr, TypeChecker}

/**
  * Describes a kernel in a benchmark.
  */
trait Kernel {
  /**
    * The name of the kernel/benchmark.
    */
  def name: String

  /**
    * The name of the benchmark suite that defines the kernel/benchmark.
    */
  def suite: String

  /**
    * The qualified name of the kernel, which consists of both the kernel suite and its name.
    */
  def qualifiedName: String = s"$suite.$name"

  /**
    * The untransformed kernel, as an expression.
    */
  def input: Expr

  /**
    * A set of perfectly transformed versions of the kernel, as expressions.
    */
  def oracles: Seq[Expr]

  /**
    * A transformed version of the input that uses De Bruijn indices and the build/ifold primitives.
    */
  def transformedInput: Expr = {
    TypeChecker.check(
      DeBruijnTransform.forward(
        LowerFunctionalPrimitives(mustSimplify = false, mustInline = false)(input).asInstanceOf[Expr]))
  }

  /**
    * Gets the extraction analysis to use for this kernel.
    */
  def extractionAnalysis: ExtractionAnalysis[BigInt] = TimeComplexityExtractor.analysis

  /**
    * Gets the extractor to use for this kernel.
    */
  def extractor: Extractor = extractionAnalysis.extractor // StochasticExtractor(extractionAnalysis.cost, DeBruijnLambda.tryCheckParamTypes(_))

  /**
    * Tries to find an oracle that matches an expression.
    * @param expr An expression to inspect.
    * @return An oracle that matches the expression.
    */
  def tryFindOracle(expr: Expr): Option[Expr] = oracles.find(ExprToText(_) == ExprToText(expr))

  /**
    * Checks if an expression matches one of the oracles.
    * @param expr The expression to check.
    * @return `true` if the expression is exactly the same as one of the oracles; otherwise, `false`.
    */
  def matchesOracle(expr: Expr): Boolean = tryFindOracle(expr).isDefined

  /**
    * Checks if an e-class matches one of the oracles.
    * @param eGraph An e-graph.
    * @param eClass An e-class in `eGraph`.
    * @tparam ENode The type of an e-node.
    * @tparam EClass The type of an e-class.
    * @return `true` if one of the oracles can be extracted from the `eClass`; otherwise, `false`.
    */
  def matchesOracle[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass], eClass: EClass): Boolean = {
    oracles.exists(oracle => {
      val pattern = Pattern(oracle)
      pattern.searchEClass(eGraph)(eClass, HierarchicalStopwatch.disabled).nonEmpty
    })
  }

  /**
    * Tries to find a (maximally specific) pattern that matches both `eClass` and an oracle.
    * @param eGraph An e-graph.
    * @param eClass An e-class in `eGraph`.
    * @tparam ENode The type of an e-node.
    * @tparam EClass The type of an e-class.
    * @return A pattern that matches both an oracle and `eClass`.
    */
  def relaxOracle[ENode <: ENodeT, EClass <: EClassT](eGraph: EGraph[ENode, EClass], eClass: EClass): Option[Expr] = {
    for (oracle <- oracles) {
      Pattern(oracle).relax(eGraph)(eClass, HierarchicalStopwatch.disabled) match {
        case Some(pattern) => return Some(pattern)
        case None =>
      }
    }
    None
  }

  /**
    * The maximum number of expansion+simplification steps before an oracle ought to be found.
    */
  def maxExpansionSteps: Int = 4

  /**
    * The maximum number of simplification-only steps before an oracle ought to be found.
    */
  def maxSimplificationSteps: Int = 1

  /**
    * The set of idiom recognition rules to use.
    * @return A set of idiom recognition rules.
    */
  def idiomRules: Seq[Rewrite] = BlasIdiomRules(DefaultSmallest).rules

  def instantiate(extractionAnalysis: ExtractionAnalysis[BigInt],
                  rules: Seq[Rewrite],
                  idioms: Seq[Rewrite]): KernelInstance = {
    KernelInstance(this, extractionAnalysis, rules, idioms)
  }

  def saturateGraph[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass],
                                                        expansionSteps: Int = maxExpansionSteps,
                                                        simplificationSteps: Int = maxSimplificationSteps,
                                                        rules: ExtractionBasedSemanticRules = ExtractionBasedSemanticRules(extractor)): Unit = {
    graph.saturate(Seq(rules.inliningRule))
    graph.saturate(rules.rules, expansionSteps)
    graph.saturate(rules.inliningRule +: rules.simplificationRules, simplificationSteps)
    graph.saturate(idiomRules, 1)
  }

  def saturateGraphNaively[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass],
                                                               steps: Int = maxExpansionSteps + maxSimplificationSteps,
                                                               rules: Seq[Rewrite] = ExtractionBasedSemanticRules(extractor).rules,
                                                               idioms: Seq[Rewrite] = idiomRules): Unit = {
    graph.saturate(rules ++ idioms, steps)
  }

  def buildGraph(observer: EGraphObserver = EGraphNullObserver,
                 shouldPrintProgress: Boolean = false,
                 enableExplanations: Boolean = false,
                 checkConsistency: Boolean = false) = {
    instantiate(extractionAnalysis, ExtractionBasedSemanticRules(extractor).rules, idiomRules)
      .build(observer, shouldPrintProgress, enableExplanations, checkConsistency)
  }

  /**
    * Finds the minimum number of expansion+simplification and simplification-only steps required to turn the
    * untransformed kernel into one of the oracles.
    * @return A pair of integers. The first element represents the minimum number of expansion+simplification steps
    *         required. The second element represents the minimum number of simplification steps that must be performed
    *         thereafter to find the objective.
    */
  def findMinSteps(): Option[(Int, Int)] = {
    def canFindGoalWith(expansionSteps: Int, simplificationSteps: Int) = {
      println(s"Trying to find goal with $expansionSteps expansion steps and $simplificationSteps simplification steps")
      val (graph, root) = buildGraph()
      saturateGraph(graph, expansionSteps, simplificationSteps)
      graph.recomputeAnalysis(extractionAnalysis)
      matchesOracle(root.analysisResult(extractionAnalysis).get._1)
    }

    // First, find the minimum number of expansion+simplification steps to directly find the objective.
    Seq.range(0, maxExpansionSteps + maxSimplificationSteps + 1)
      .find(canFindGoalWith(_, 0))
      // Next, find the maximum number of expansion+simplification steps we can turn into simplification steps.
      .map(totalSteps => {
        val simplificationSteps = Seq.range(0, totalSteps + 1).reverse.find(simplificationSteps => {
          val expansionSteps = totalSteps - simplificationSteps
          canFindGoalWith(expansionSteps, simplificationSteps)
        }).get
        (totalSteps - simplificationSteps, simplificationSteps)
      })
  }

  /**
    * Finds the minimum number of naive steps required to turn the untransformed kernel into one of the oracles.
    * @return The total number of steps.
    */
  def findMinNaiveSteps(): Option[Int] = {
    def canFindGoalWith(expansionSteps: Int) = {
      println(s"Trying to find goal with $expansionSteps steps")
      val (graph, root) = buildGraph()
      saturateGraphNaively(graph, expansionSteps)
      graph.recomputeAnalysis(extractionAnalysis)
      matchesOracle(root.analysisResult(extractionAnalysis).get._1)
    }

    // Find the minimum number of steps required to directly find the objective.
    Seq.range(0, maxExpansionSteps + maxSimplificationSteps + 1).find(canFindGoalWith)
  }
}

/**
  * A kernel that has been instantiated for a particular target by augmenting it with an extractor and rules.
  * @param kernel The kernel that has been extracted.
  * @param extractionAnalysis The extraction analysis to use for the kernel.
  * @param rules The set of rules that can be applied to the kernel.
  * @param idioms The set of idiom rules that can be applied to the kernel.
  */
case class KernelInstance(kernel: Kernel,
                          extractionAnalysis: ExtractionAnalysis[BigInt],
                          rules: Seq[Rewrite],
                          idioms: Seq[Rewrite]) {
  /**
    * Gets the extractor to use for this kernel.
    */
  def extractor: Extractor = extractionAnalysis.extractor

  private def createGraph(expr: Expr,
                          observer: EGraphObserver = EGraphNullObserver,
                          shouldPrintProgress: Boolean = false,
                          enableExplanations: Boolean = false,
                          checkConsistency: Boolean = false) = {
    val graph = new DeferredMergingEGraph(
      new Willsey2021EGraph(
        observer,
        shouldPrintProgress = shouldPrintProgress,
        enableExplanations = enableExplanations,
        checkConsistency = checkConsistency,
        rebuildAfterMerge = false,
        analyses = new AnalysesBuilder()
          .add(MinDepthAnalysis)
          .add(ExtractionAnalysis.smallestExpressionExtractor)
          .add(extractionAnalysis)
          .toAnalyses))

    // Add the input to the graph.
    val root = graph.add(expr)

    // HACK: Add p_0: i32 to the graph so it can serve as inspiration to cartesian product matchers. This will allow us
    // to find patterns we otherwise wouldn't find.
    graph.add(DeBruijnParamUse(0, IntType(32)))

    // Rebuild the graph.
    graph.rebuild()

    (graph, root)
  }

  def build(observer: EGraphObserver = EGraphNullObserver,
            shouldPrintProgress: Boolean = false,
            enableExplanations: Boolean = false,
            checkConsistency: Boolean = false) = {
    createGraph(
      kernel.transformedInput,
      observer = observer,
      shouldPrintProgress = shouldPrintProgress,
      enableExplanations = enableExplanations,
      checkConsistency = checkConsistency)
  }

  /**
    * Saturates the graph using a given strategy.
    * @param graph The e-graph to saturate.
    * @param steps The maximum number of steps to take.
    * @param strategy The saturation strategy to use.
    * @tparam ENode The type of e-node.
    * @tparam EClass The type of e-class.
    */
  def saturate[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass],
                                                   steps: Int = kernel.maxExpansionSteps + kernel.maxSimplificationSteps,
                                                   strategy: Seq[Rewrite] => SaturationStrategy = SimpleSaturationStrategy): Unit = {
    graph.saturate(IterationLimitedSaturationStrategy(strategy(rules ++ idioms), steps))
  }

  /**
    * Creates a rebasing engine for the kernel instance.
    * @param observer The observer to use for the engine.
    * @return A rebasing engine for the kernel instance.
    */
  def createRebasingEngine(observer: EGraphObserver = EGraphNullObserver): RebasingRewriteEngine[HashConsENode, HashConsEClass] = {
    RebasingRewriteEngine[HashConsENode, HashConsEClass](
      kernel.transformedInput,
      extractor,
      createGraph(_, observer))
  }
}
