package cGen.kernels
import eqsat.DeBruijnTransform
import cGen.primitive.{Idx, M}
import cGen.{ArrayType, Build, CLambda, DoubleType, EagerType, Extern, IntType, Tuple}
import core.{Expr, ParamDef, ParamUse}

case class DoitgenKernel(NR: Int, NQ: Int, NP: Int) extends Kernel {
  /**
    * The name of the kernel/benchmark.
    */
  override def name: String = "doitgen"

  /**
    * The name of the benchmark suite that defines the kernel/benchmark.
    */
  override def suite: String = "polybench"

  /**
    * The untransformed kernel, as an expression.
    */
  override def input: Expr = {
    val i32 = IntType(32)
    val t = DoubleType()
    val A = ParamDef(ArrayType(ArrayType(ArrayType(t, NP), NQ), NR))
    val C4T = ParamDef(ArrayType(ArrayType(t, NP), NP))
    CLambda(
      Seq(A, C4T).reverse,
      Build({
        val r = ParamDef(i32)
        CLambda(r, Build({
          val q = ParamDef(i32)
          CLambda(q, Build({
            val p = ParamDef(i32)
            CLambda(p,
              LowLevelLib.dotProduct(
                Idx(Idx(ParamUse(A), ParamUse(r), EagerType()), ParamUse(q), EagerType()),
                Idx(ParamUse(C4T), ParamUse(p), EagerType())))
          }, NP))
        }, NQ))
      }, NR))
  }

  /**
    * A set of perfectly transformed versions of the kernel, as expressions.
    */
  override def oracles: Seq[Expr] = {
    val i32 = IntType(32)
    implicit val t = DoubleType
    val A = ParamDef(ArrayType(ArrayType(ArrayType(t(), NP), NQ), NR))
    val C4T = ParamDef(ArrayType(ArrayType(t(), NP), NP))

    val zeroVectors = Seq(
      (LowLevelLib.zero, LowLevelLib.constArray(LowLevelLib.one, NP)),
      (LowLevelLib.one, LowLevelLib.constArray(LowLevelLib.zero, NP)),
      (LowLevelLib.zero, LowLevelLib.constArray(LowLevelLib.zero, NP)))

    for { (b1, c1) <- zeroVectors }
      yield DeBruijnTransform.forward(
        CLambda(
          Seq(A, C4T).reverse,
          Build({
            val r = ParamDef(i32)
            CLambda(r, Build({
              val q = ParamDef(i32)
              CLambda(q,
                Extern.GEMV(
                  LowLevelLib.one,
                  ParamUse(C4T),
                  Idx(Idx(ParamUse(A), ParamUse(r), EagerType()), ParamUse(q), EagerType()),
                  b1,
                  c1))
            }, NQ))
          }, NR)))
  }

  override def maxExpansionSteps: Int = 3
  override def maxSimplificationSteps: Int = 0
}
