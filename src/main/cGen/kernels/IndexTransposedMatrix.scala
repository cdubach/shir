package cGen.kernels
import eqsat.{DeBruijnLambda, DeBruijnParamUse}
import cGen.{ArrayType, Build, CLambda, DoubleType, EagerType, IntType}
import cGen.primitive.Idx
import core.{Expr, ParamDef, ParamUse}

case class IndexTransposedMatrix(N: Int) extends Kernel {
  /**
    * The name of the kernel/benchmark.
    */
  override def name: String = "index-transposed-matrix"

  /**
    * The name of the benchmark suite that defines the kernel/benchmark.
    */
  override def suite: String = "test"

  /**
    * The untransformed kernel, as an expression.
    */
  override def input: Expr = {
    val i32 = IntType(32)
    val f = DoubleType()
    val index = ParamDef(i32)
    val matrix = ParamDef(ArrayType(ArrayType(f, N), N))
    CLambda(
      Seq(matrix, index).reverse,
      Idx(LowLevelLib.matrixTranspose(ParamUse(matrix)), ParamUse(index), EagerType()))
  }

  /**
    * A set of perfectly transformed versions of the kernel, as expressions.
    */
  override def oracles: Seq[Expr] = {
    val i32 = IntType(32)
    val f = DoubleType()
    val matrixType = ArrayType(ArrayType(f, N), N)
    Seq(
      DeBruijnLambda(matrixType,
        DeBruijnLambda(i32,
          Build(
            DeBruijnLambda(i32,
              Idx(
                Idx(DeBruijnParamUse(2, matrixType), DeBruijnParamUse(0, i32), EagerType()),
                DeBruijnParamUse(1, i32), EagerType())),
            N))))
  }
}
