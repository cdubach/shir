package cGen.kernels

import eqsat.{DeBruijnLambda, DeBruijnTransform}
import cGen._
import core._

/**
  * A kernel that applies a blurring stencil to a matrix.
  * @param matrixSize The size of the matrix to blur.
  */
case class Stencil2DKernel(matrixSize: Int, stencilDim: Int = 9) extends Kernel {
  private val resultDim = matrixSize - stencilDim + 1
  private val stencil = {
    implicit val t = DoubleType
    LowLevelLib.constArray(
      LowLevelLib.constArray(
        LowLevelLib.num(1.0 / (stencilDim * stencilDim)),
        stencilDim),
      stencilDim)
  }

  override def name: String = "stencil2d"
  override def suite: String = "custom"
  override def input: Expr = {
    val len = matrixSize
    val in = ParamDef(ArrayType(ArrayType(DoubleType(), len), len))
    CLambda(
      in,
      TypeChecker.check(LowLevelLib.stencil2D(ParamUse(in), stencil)))
  }

  override def oracles: Seq[Expr] = {
    val t = DoubleType()
    val i32 = IntType(32)

    val zeroVectors = Seq(
      (Constant(0.0, t), Build(DeBruijnLambda(i32, Constant(1.0, t)), resultDim * resultDim)),
      (Constant(1.0, t), Build(DeBruijnLambda(i32, Constant(0.0, t)), resultDim * resultDim)),
      (Constant(0.0, t), Build(DeBruijnLambda(i32, Constant(0.0, t)), resultDim * resultDim))
    )

    val stencilVecLength = stencilDim * stencilDim
    val blurFactor = 1.0 / (stencilDim * stencilDim)
    val oneNinthVectors = Seq(
      (Constant(1.0, t), Build(DeBruijnLambda(i32, Constant(blurFactor, t)), stencilVecLength)),
      (Constant(blurFactor, t), Build(DeBruijnLambda(i32, Constant(1.0, t)), stencilVecLength))
    )

    oneNinthVectors.flatMap(oneNinthVec =>
      zeroVectors.map(zeroVec => {
        implicit val t = DoubleType
        val in = ParamDef(ArrayType(ArrayType(t(), matrixSize), matrixSize))
        DeBruijnTransform.forward(
          CLambda(
            in,
            LowLevelLib.split(
              TypeChecker.check(
                Extern.GEMV(
                  oneNinthVec._1,
                  LowLevelLib.slide2dFlat(ParamUse(in), stencilDim, stencilDim),
                  oneNinthVec._2,
                  zeroVec._1,
                  zeroVec._2)),
              resultDim)))
      }))
  }

  override def maxExpansionSteps: Int = 3
  override def maxSimplificationSteps: Int = 0
}
