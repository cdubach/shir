package cGen.kernels

import cGen.idioms.{LowerFunctionalPrimitives, TypeArith}
import cGen.primitive.Idx
import cGen._
import core.{Expr, _}

object LowLevelLib {
  def let(param: ParamDef, paramValue: Expr, result: Expr): Expr = {
    FunctionCall(CLambda(param, result), paramValue)
  }

  /**
    * Sums up all elements of a vector.
    *
    * @param vector A vector.
    * @return The sum of the elements in `vector`.
    */
  def sum(vector: Expr): Expr = {
    val _vector = TypeChecker.check(vector)
    val elemType = Lib.elementType(_vector)
    reduce(_vector, Constant(0.0, elemType), add)
  }

  /**
    * Applies a reduction operator to the elements of a vector.
    * @param vector The vector to reduce to a single element.
    * @param init A seed value for the accumulator.
    * @param op An operator that folds an element into the accumulator.
    * @return The result of folding all elements of a vector into an accumulator.
    */
  def reduce(vector: Expr, init: Expr, op: (Expr, Expr) => Expr): Expr = {
    val _vector = TypeChecker.check(vector)
    val elemType = _vector.t.asInstanceOf[ArrayTypeT].et
    val i = ParamDef(IntType(32))
    val acc = ParamDef(elemType)
    TypeChecker.check(
      Ifold(
        CLambda(
          Seq(i, acc).reverse,
          op(
            Idx(_vector, ParamUse(i), EagerType()),
            ParamUse(acc))),
        init,
        Lib.length(_vector)))
  }

  /**
    * Computes the dot product of two vectors.
    *
    * @param left A first vector.
    * @param right A second vector.
    * @return The dot product of `left` and `right`.
    */
  def dotProduct(left: Expr, right: Expr): Expr = {
    val _left = TypeChecker.check(left)
    val elemType = Lib.elementType(_left)
    val i = ParamDef(IntType(32))
    val acc = ParamDef(elemType)
    TypeChecker.check(
      Ifold(
        CLambda(
          Seq(i, acc).reverse,
          add(
            mul(Idx(_left, ParamUse(i), EagerType()), Idx(right, ParamUse(i), EagerType())),
            ParamUse(acc))),
        Constant(0.0, elemType),
        Lib.length(_left)))
  }

  /**
    * Computes the outer product of two vectors.
    *
    * @param left  A first vector.
    * @param right A second vector.
    * @return The outer product of `left` and `right`.
    */
  def outerProduct(left: Expr, right: Expr): Expr = {
    val _left = TypeChecker.check(left)
    val _right = TypeChecker.check(right)
    val i = ParamDef(IntType(32))
    val j = ParamDef(IntType(32))
    TypeChecker.check(
      Build(
        CLambda(i,
          Build(
            CLambda(j, Lib.mul(Idx(_left, ParamUse(i), EagerType()), Idx(_right, ParamUse(j), EagerType()))),
            Lib.length(_right),
            EagerType())),
        Lib.length(_left),
        EagerType()))
  }

  /**
    * Computes a matrix-vector product.
    * @param matrix A matrix.
    * @param vector A vector.
    * @return The product of `matrix` and `vector`, as a vector.
    */
  def matrixVectorProduct(matrix: Expr, vector: Expr): Expr = {
    val i = ParamDef(IntType(32))
    TypeChecker.check(Build(CLambda(i, dotProduct(Idx(matrix, ParamUse(i), EagerType()), vector)), Lib.length(vector)))
  }

  /**
    * Computes a matrix-matrix product.
    * @param left A first matrix.
    * @param right A second, pre-transposed matrix.
    * @return The product of `left` and `right`.
    */
  def matrixMatrixProductPretransposed(left: Expr, right: Expr): Expr = {
    val i = ParamDef(IntType(32))
    TypeChecker.check(
      Build(
        CLambda(i, matrixVectorProduct(right, Idx(left, ParamUse(i), EagerType()))),
        Lib.rows(left)))
  }

  /**
    * Transposes a matrix.
    * @param matrix A matrix to transpose.
    * @return A transposed version of the matrix.
    */
  def matrixTranspose(matrix: Expr): Expr = {
    val i = ParamDef(IntType(32))
    val j = ParamDef(IntType(32))
    TypeChecker.check(
      Build(
        CLambda(i,
          Build(
            CLambda(j, Idx(Idx(matrix, ParamUse(j), EagerType()), ParamUse(i), EagerType())),
            Lib.rows(matrix))),
        Lib.cols(matrix)))
  }

  /**
    * Computes a matrix-matrix product.
    * @param left A first matrix.
    * @param right A second matrix.
    * @return The product of `left` and `right`.
    */
  def matrixMatrixProduct(left: Expr, right: Expr): Expr = {
    matrixMatrixProductPretransposed(left, matrixTranspose(right))
  }

  /**
    * Performs an elementwise binary operation on a vector.
    * @param vector A vector.
    * @param op A function that transforms a vector element.
    * @return A vector of transformed elements.
    */
  def vectorElementwiseUnary(vector: Expr, op: Expr => Expr): Expr = {
    val i = ParamDef(IntType(32))
    val _vector = TypeChecker.check(vector)
    TypeChecker.check(Build(CLambda(i, op(Idx(_vector, ParamUse(i), EagerType()))), Lib.length(_vector)))
  }

  /**
    * Performs an elementwise binary operation on a matrix.
    * @param matrix A matrix.
    * @param op A function that transforms a matrix element.
    * @return A matrix of transformed elements.
    */
  def matrixElementwiseUnary(matrix: Expr, op: Expr => Expr): Expr = {
    vectorElementwiseUnary(matrix, vectorElementwiseUnary(_, op))
  }

  /**
    * Performs an elementwise binary operation on a pair of vectors.
    * @param left A first vector.
    * @param right A second vector.
    * @param op A function that takes two vector elements and performs some operation on them.
    * @return A vector of which each element is the application of `op` to the corresponding elements of `left` and
    *         `right`.
    */
  def vectorElementwiseBinary(left: Expr, right: Expr, op: (Expr, Expr) => Expr): Expr = {
    val i = ParamDef(IntType(32))
    val _left = TypeChecker.check(left)
    TypeChecker.check(
      Build(
        CLambda(i, op(Idx(_left, ParamUse(i), EagerType()), Idx(right, ParamUse(i), EagerType()))),
        Lib.length(_left)))
  }

  /**
    * Performs an elementwise binary operation on a pair of matrices.
    * @param left A first matrix.
    * @param right A second matrix.
    * @param op A function that takes two matrix elements and performs some operation on them.
    * @return A matrix of which each element is the application of `op` to the corresponding elements of `left` and
    *         `right`.
    */
  def matrixElementwiseBinary(left: Expr, right: Expr, op: (Expr, Expr) => Expr): Expr = {
    vectorElementwiseBinary(left, right, vectorElementwiseBinary(_, _, op))
  }

  /**
    * Computes the product of a matrix and a scalar.
    * @param matrix A matrix.
    * @param scalar A scalar.
    * @return A matrix of which each element is the product of `scalar` and the corresponding element of `matrix`.
    */
  def matrixScalarProduct(matrix: Expr, scalar: Expr): Expr = {
    matrixElementwiseUnary(matrix, mul(_, scalar))
  }

  /**
    * Computes the product of a vector and a scalar.
    * @param vector A vector.
    * @param scalar A scalar.
    * @return A vector of which each element is the product of `scalar` and the corresponding element of `vector`.
    */
  def vectorScalarProduct(vector: Expr, scalar: Expr): Expr = {
    vectorElementwiseUnary(vector, mul(_, scalar))
  }

  /**
    * Computes the sum of two matrices.
    * @param left A first matrix.
    * @param right A second matrix.
    * @return The sum of `left` and `right`.
    */
  def matrixAdd(left: Expr, right: Expr): Expr = {
    matrixElementwiseBinary(left, right, add)
  }

  /**
    * Computes the sum of two vectors.
    * @param left A first vector.
    * @param right A second vector.
    * @return The sum of `left` and `right`.
    */
  def vectorAdd(left: Expr, right: Expr): Expr = {
    vectorElementwiseBinary(left, right, add)
  }

  /**
    * Applies a one-dimensional stencil to a vector.
    * @param vector A vector to apply a stencil to.
    * @param stencil The stencil to apply to `vector`.
    * @return The result of the stencil operation.
    */
  def stencil1D(vector: Expr, stencil: Expr): Expr = {
    implicit val t: () => Type = () => vector.t.asInstanceOf[ArrayTypeT].et
    val _stencil = TypeChecker.check(stencil)
    val len = _stencil.t.asInstanceOf[ArrayTypeT].len
    val slide = TypeChecker.check(
      LowerFunctionalPrimitives(mustSimplify = false)(Slide(vector, len)).asInstanceOf[Expr])
    map(slide, dotProduct(_, _stencil))
  }

  def ifLessThan(cmpLhs: Expr, cmpRhs: Expr, ifLess: Expr, otherwise: Expr): Expr = {
    If(BinaryOp(cmpLhs, cmpRhs, AlgoOp.Lt, isBool = true), ifLess, otherwise)
  }

  def pad1D(vector: Expr, paddingAmount: Int, padding: Expr => Expr): Expr = {
    val i32 = IntType(32)
    implicit val t: () => Type = () => i32
    val vecTmp = ParamDef(vector.t)
    val i = ParamDef(i32)
    FunctionCall(
      CLambda(vecTmp,
        Build(CLambda(i,
          ifLessThan(
            ParamUse(i),
            Constant(paddingAmount, i32),
            padding(ParamUse(i)),
            ifLessThan(
              ParamUse(i),
              TypeArith(Lib.length(vector).ae + paddingAmount, i32).simplified,
              Idx(ParamUse(vecTmp), sub(ParamUse(i), Constant(paddingAmount, i32)), EagerType()),
              padding(ParamUse(i))))),
          Lib.length(vector).ae + 2 * paddingAmount)),
      vector,
      applyBetaReduction = false)
  }

  def join(expr: Expr): Expr = {
    val _matrix = TypeChecker.check(expr)
    val rows = Lib.rows(_matrix)
    val cols = Lib.cols(_matrix)
    val i32 = IntType(32)
    TypeChecker.check(
      Build({
        val k = ParamDef(i32)
        CLambda(k, {
          val i = div(ParamUse(k), TypeArith(rows, i32))
          val j = mod(ParamUse(k), TypeArith(rows, i32))
          Idx(Idx(_matrix, i, EagerType()), j, EagerType())
        })
      }, rows.ae * cols.ae))
  }

  def split(vector: Expr, numberOfRows: ArithTypeT): Expr = {
    val _vector = TypeChecker.check(vector)
    val numberOfColumns = Lib.length(_vector).ae / numberOfRows.ae
    val i32 = IntType(32)
    val in = ParamDef(_vector.t)
    TypeChecker.check(
      FunctionCall(
        CLambda(in,
          Build({
            val i = ParamDef(i32)
            CLambda(i, Build({
              val j = ParamDef(i32)
              CLambda(j,
                Idx(
                  ParamUse(in),
                  add(mul(ParamUse(i), TypeArith(numberOfColumns, i32).simplified), ParamUse(j)),
                  EagerType()))
            }, numberOfColumns))
          }, numberOfRows)),
        _vector))
  }

  def map(vector: Expr, op: Expr => Expr): Expr = {
    val _vector = TypeChecker.check(vector)
    TypeChecker.check(
      Build({
        val i = ParamDef(IntType(32))
        CLambda(i, op(Idx(_vector, ParamUse(i), EagerType())))
      }, Lib.length(_vector)))
  }

  def zipWith(left: Expr, right: Expr, op: (Expr, Expr) => Expr): Expr = {
    val _left = TypeChecker.check(left)
    val _right = TypeChecker.check(right)
    Build({
      val i = ParamDef(IntType(32))
      CLambda(i, op(Idx(_left, ParamUse(i), EagerType()), Idx(_right, ParamUse(i), EagerType())))
    }, Lib.length(_left))
  }

  def slide2dFlat(matrix: Expr, windowRows: ArithTypeT, windowColumns: ArithTypeT): Expr = {
    val _matrix = TypeChecker.check(matrix)
    implicit val t: () => Type = () => Lib.elementType(_matrix)
    val rows = Lib.rows(_matrix)
    val cols = Lib.cols(_matrix)
    val slideRows = rows.ae - windowRows.ae + 1
    val slideCols = cols.ae - windowColumns.ae + 1
    val windowCount = slideRows * slideCols
    val windowLength = windowRows.ae * windowColumns.ae

    TypeChecker.check(
      Build({
        val windowIndex = ParamDef(IntType(32))
        val row = div(ParamUse(windowIndex), TypeArith(slideRows, IntType(32)).simplified)
        val col = div(ParamUse(windowIndex), TypeArith(slideCols, IntType(32)).simplified)
        CLambda(windowIndex, Build({
          val k = ParamDef(IntType(32))
          val windowRow = div(ParamUse(k), TypeArith(windowRows, IntType(32)).simplified)
          val windowCol = mod(ParamUse(k), TypeArith(windowRows, IntType(32)).simplified)
          CLambda(k, Idx(Idx(_matrix, add(row, windowRow), EagerType()), add(col, windowCol), EagerType()))
        }, windowLength))
      }, windowCount))
  }

  /**
    * Applies a two-dimensional stencil to a matrix.
    * @param matrix A matrix to apply a stencil to.
    * @param stencil The stencil to apply to `matrix`.
    * @return The result of the stencil operation.
    */
  def stencil2D(matrix: Expr, stencil: Expr): Expr = {
    val _matrix = TypeChecker.check(matrix)
    val _stencil = TypeChecker.check(stencil)
    implicit val t: () => Type = () => Lib.elementType(_matrix)
    val rows = Lib.rows(_matrix)
    val windowRows = Lib.rows(_stencil)
    val windowColumns = Lib.cols(_stencil)
    val slideRows = rows.ae - windowRows.ae + 1

    val slide = slide2dFlat(matrix, windowRows, windowColumns)
    val resultPoints = map(slide, dotProduct(_, join(_stencil)))
    TypeChecker.check(split(resultPoints, slideRows))
  }

  def zero(implicit t: () => Type): Expr = Lib.zero
  def one(implicit t: () => Type): Expr = Lib.one
  def num(value: Double)(implicit t: () => Type): Expr = Lib.num(value)

  def constArray(value: Expr, length: ArithTypeT): Expr = Lib.constArray(value, length)
  def constMatrix(value: Expr, N: ArithTypeT, M: ArithTypeT): Expr = constArray(constArray(value, N), M)
  def add(left: Expr, right: Expr): Expr =
    TypeChecker.check(BinaryOp(TypeChecker.check(left), TypeChecker.check(right), AlgoOp.Add))
  def sub(left: Expr, right: Expr): Expr =
    TypeChecker.check(BinaryOp(TypeChecker.check(left), TypeChecker.check(right), AlgoOp.Sub))
  def mul(left: Expr, right: Expr): Expr =
    TypeChecker.check(BinaryOp(TypeChecker.check(left), TypeChecker.check(right), AlgoOp.Mul))
  def div(left: Expr, right: Expr): Expr =
    TypeChecker.check(BinaryOp(TypeChecker.check(left), TypeChecker.check(right), AlgoOp.Div))
  def mod(left: Expr, right: Expr): Expr =
    TypeChecker.check(BinaryOp(TypeChecker.check(left), TypeChecker.check(right), AlgoOp.Mod))
}
