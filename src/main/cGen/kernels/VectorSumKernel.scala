package cGen.kernels

import cGen.{ArrayType, CLambda, DoubleType, Extern}
import eqsat.{DeBruijnLambda, DeBruijnParamUse, DeBruijnTransform}
import core.{Expr, ParamDef, ParamUse, TypeChecker}

/**
  * A kernel that performs a vector sum.
  * @param vectorLength The length of the vector to sum.
  */
case class VectorSumKernel(vectorLength: Int) extends Kernel {
  def name: String = "vsum"
  def suite: String = "custom"
  def input: Expr = {
    implicit val t = DoubleType
    val N = vectorLength
    val A = ParamDef(ArrayType(t(), N))
    CLambda(A, TypeChecker.check(LowLevelLib.sum(ParamUse(A))))
  }

  def oracles: Seq[Expr] = {
    implicit val t = DoubleType
    val vectorType = ArrayType(t(), vectorLength)
    val allOnes = TypeChecker.check(DeBruijnTransform.forward(LowLevelLib.constArray(LowLevelLib.one, vectorLength)))
    Seq(
      TypeChecker.check(
        DeBruijnLambda(vectorType,
          Extern.DoubleDotProd(DeBruijnParamUse(0, vectorType), allOnes))),
      TypeChecker.check(
        DeBruijnLambda(vectorType,
          Extern.DoubleDotProd(allOnes, DeBruijnParamUse(0, vectorType)))))
  }

  override def maxExpansionSteps: Int = 3
}
