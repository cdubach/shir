package cGen

import core.{ArithTypeT, ArithTypeVar, BuiltinTypeFunction, Expr, FunTypeVar, FunctionCall, Type, TypeFunType, TypeFunctionCall}

abstract class OpenCLExpr extends CGenExpr

case class MapGlbExpr(innerIR: Expr) extends OpenCLExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): MapGlbExpr = MapGlbExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = MapGlb(args(0), args(1))
}

object MapGlb {
  def apply(function: Expr, input: Expr): MapGlbExpr = MapGlbExpr({
    val indtv = CGenDataTypeVar()
    val outdtv = CGenDataTypeVar()
    val ftv = CFunTypeVar(indtv, outdtv)
    val lentv = ArithTypeVar()
    val inseqtv = ArrayTypeVar(indtv, lentv)
    val outseqt = ArrayType(outdtv, lentv)
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(
      TypeFunType(Seq(ftv, inseqtv), CFunType(Seq(ftv, inseqtv), outseqt, EagerType()))),
      Seq(function.t, input.t)), Seq(function, input))
  })

  def unapply(expr: MapGlbExpr): Some[(Expr, Expr, Type)] = Some((expr.args.head, expr.args(1), expr.t))
}

case class BuildGlbExpr(innerIR: Expr, n: Type) extends OpenCLExpr {
  override def types = Seq(n)

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): BuildGlbExpr = BuildGlbExpr(newInnerIR, newTypes.head)

  override def argsBuild(args: Seq[Expr]): CGenExpr = BuildGlb(args(0), n.asInstanceOf[ArithTypeT])
}


object BuildGlb {
  def apply(idxf: Expr, n: ArithTypeT): BuildGlbExpr = BuildGlbExpr({
    val idxtv = IntTypeVar(32)
    val idxftv = FunTypeVar(idxtv, VoidTypeVar())
    val outtv = VoidType()
    FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(idxftv),
      CFunType(Seq(idxftv), outtv, EagerType()))), Seq(idxf.t)), Seq(idxf))
  }, n)

  def unapply(expr: BuildGlbExpr): Option[(Expr, ArithTypeT, Type)] =
    Some((expr.args.head, expr.n.asInstanceOf[ArithTypeT], expr.t))
}


