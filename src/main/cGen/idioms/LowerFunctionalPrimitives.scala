package cGen.idioms

import cGen.AlgoOp.AlgoOp
import cGen.primitive.{Add, Concat, Fst, Id, Idx, Snd}
import cGen.{AlgoOp, ArrayTypeT, BinaryOp, Build, CFC, CLambda, EagerType, If, Ifold, IntType, Join, Map, MapExpr, Mul, NamedTupleStructType, Reduce, ReduceExpr, Repeat, Slice, Slide, Split, Sub, Tuple, Zip, ZipExpr}
import core.{Expr, FunctionCall, ParamDef, ParamUse, ShirIR, TypeChecker}

/**
 * A transform that lowers high-level functional primitives such as map and zip to low-level primitives such as build.
 */
case class LowerFunctionalPrimitives(mustSimplify: Boolean, mustInline: Boolean = true) {
  def apply(expr: ShirIR): ShirIR = {
    def simplify(expr: Expr): Expr = expr.visitAndRebuild({
      case FunctionCall(func, arg, _) => call(func, arg)
      case Idx(xs, index, _, _) => accessAt(xs, index)
      case Fst(tup, _, _) => fst(tup)
      case Snd(tup, _, _) => snd(tup)
      case other => other
    }).asInstanceOf[Expr]

    def call(func: Expr, arg: Expr) = func match {
      case CLambda(param, body, _) if mustSimplify =>
        simplify(
          body.visitAndRebuild({
            case ParamUse(pd) if pd.id == param.id => arg  // beta reduction
            case ir => ir
          }).asInstanceOf[Expr])
      case _ => FunctionCall(func, arg, applyBetaReduction = false)
    }

    def accessAt(xs: Expr, index: Expr) = xs match {
      case Build(f, _, _, _) if mustSimplify => call(f, index)
      case _ => Idx(xs, index, EagerType())
    }

    def fst(tuple: Expr) = tuple match {
      case Tuple(x, _, _) => x
      case _ => Fst(tuple, EagerType())
    }

    def snd(tuple: Expr) = tuple match {
      case Tuple(_, y, _) => y
      case _ => Snd(tuple, EagerType())
    }

    def binOp(left: Expr, right: Expr, op: AlgoOp, isBool: Boolean = false) =
      TypeChecker.check(BinaryOp(TypeChecker.check(left), TypeChecker.check(right), op, isBool))

    def lower(expr: ShirIR): ShirIR = expr match {
      case map: MapExpr =>
        val Map(f, in, _, _) = TypeChecker.check(map)
        val indexVar = ParamDef(IntType(32))
        Build(
          CLambda(indexVar, call(f, accessAt(in, ParamUse(indexVar)))),
          in.t.asInstanceOf[ArrayTypeT].len)

      case zip: ZipExpr =>
        val indexVar = ParamDef(IntType(32))
        val Zip(ts, t: ArrayTypeT) = TypeChecker.check(zip)
        Build(
          CLambda(
            indexVar,
            Tuple(
              accessAt(fst(ts), ParamUse(indexVar)),
              accessAt(snd(ts), ParamUse(indexVar)))),
          t.len)

      case reduce: ReduceExpr =>
        val Reduce(function, initialValue, input, _) = TypeChecker.check(reduce)
        TypeChecker.check(Ifold({
          val indexVar = ParamDef(IntType(32))
          val accumulator = ParamDef(initialValue.t)
          CLambda(indexVar,
            CLambda(accumulator,
              call(call(function, accessAt(input, ParamUse(indexVar))), ParamUse(accumulator))))
        }, initialValue, input.t.asInstanceOf[ArrayTypeT].len))

      case Join(matrix, _, _) =>
        val checkedMatrix = TypeChecker.check(matrix)
        val outerArrayT = checkedMatrix.t.asInstanceOf[ArrayTypeT]
        val innerArrayT = outerArrayT.et.asInstanceOf[ArrayTypeT]
        val index = ParamDef(IntType(32))
        Build(
          CLambda(index,
            accessAt(
              accessAt(
                checkedMatrix,
                binOp(ParamUse(index), TypeArith(outerArrayT.len, IntType(32)).simplified, AlgoOp.Div)),
              binOp(ParamUse(index), TypeArith(innerArrayT.len, IntType(32)).simplified, AlgoOp.Mod))),
          outerArrayT.len.ae * innerArrayT.len.ae)

      case Split(array, m, _, _) =>
        val checkedArray = TypeChecker.check(array)
        val i = ParamDef(IntType(32))
        val j = ParamDef(IntType(32))
        val cs = TypeArith(m, IntType(32)).simplified
        Build(
          CLambda(i,
            Build(
              CLambda(j,
                accessAt(checkedArray, binOp(binOp(ParamUse(i), cs, AlgoOp.Mul), ParamUse(j), AlgoOp.Add))),
              m)),
          checkedArray.t.asInstanceOf[ArrayTypeT].len.ae / m.ae)

      case slice@Slice(array, start, _, _) =>
        val checkedSlice = TypeChecker.check(slice)
        val in = TypeChecker.check(array)
        val i = ParamDef(IntType(32))
        Build(
          CLambda(i, accessAt(in, binOp(ParamUse(i), start, AlgoOp.Add))),
          checkedSlice.t.asInstanceOf[ArrayTypeT].len)

      case s@Slide(array, windowWidth, _) =>
        val checkedSlide = TypeChecker.check(s)
        val in = TypeChecker.check(array)
        val i = ParamDef(IntType(32))
        val j = ParamDef(IntType(32))
        Build(
          CLambda(i,
            Build(
              CLambda(j, accessAt(in, binOp(ParamUse(j), ParamUse(i), AlgoOp.Add))),
              windowWidth)),
          checkedSlide.t.asInstanceOf[ArrayTypeT].len)

      case Repeat(n, expr, _) =>
        val in = TypeChecker.check(expr)
        val i = ParamDef(IntType(32))
        Build(CLambda(i, in), n)

      case Concat(in, _, _) =>
        val idx = ParamDef(IntType(32))
        val _in = TypeChecker.check(in)
        val len0 = _in.t.asInstanceOf[NamedTupleStructType].fst.asInstanceOf[ArrayTypeT].len
        val len1 = _in.t.asInstanceOf[NamedTupleStructType].snd.asInstanceOf[ArrayTypeT].len
        Build(
          CLambda(idx,
          If(
            binOp(ParamUse(idx), TypeArith(len1, IntType(32)).simplified, AlgoOp.Lt, isBool = true),
            accessAt(fst(_in), ParamUse(idx)),
            accessAt(snd(_in), ParamUse(idx)))
        ), len0.ae + len1.ae)

      // Make views eager
      case Build(idxf, n, _, _) => Build(idxf, n)
      case Idx(array, index, _, _) => Idx(array, index, EagerType())

      case Id(arg, _) => arg
      case CFC(f, arg, _) => inline(FunctionCall(f, arg))

      case Add(tup, _) => binOp(fst(tup), snd(tup), AlgoOp.Add)
      case Sub(tup, _) => binOp(fst(tup), snd(tup), AlgoOp.Sub)
      case Mul(tup, _) => binOp(fst(tup), snd(tup), AlgoOp.Mul)

      case BinaryOp(left, right, op, isBool, _) => binOp(left, right, op, isBool)

      case other => other
    }

    def inline(expr: ShirIR) = expr match {
      case FunctionCall(CLambda(param, body, _), arg, _) =>
        body.visitAndRebuild({
          case ParamUse(pd) if pd.id == param.id => arg  // beta reduction
          case ir => ir
        }).asInstanceOf[Expr]

      case other => other
    }

    if (mustInline) {
      expr.visitAndRebuild(lower).visitAndRebuild(inline)
    } else {
      expr.visitAndRebuild(lower)
    }
  }
}
