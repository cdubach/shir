package cGen.idioms

import cGen.{ArrayTypeT, BinaryOp, Build, CLambda, Constant, ExternFunctionCall, If, Ifold, Lib, Tuple}
import eqsat.{DeBruijnForall, DeBruijnLambda, DeBruijnParamUse, DeBruijnShift, EClassT, ExtractionAnalysis, Extractor}
import cGen.primitive.{Fst, Idx, Snd}
import core.{Expr, FunctionCall, ParamUse}

/**
  * An extractor that tries to find the fastest expression. Idiom calls are discounted.
  */
object TimeComplexityExtractor extends Extractor {
  /**
    * The extraction analysis this extractor depends on.
    */
  val analysis: ExtractionAnalysis[BigInt] = ExtractionAnalysis[BigInt](
    "time-complexity-extractor",
    {
      case (
        ExternFunctionCall("idiom_dgemm", Seq(_, _, b, a, _, _, _, _, _, _), _, _),
        Seq(cCost, betaCost, bCost, aCost, alphaCost, _, _, _, _, _)) =>

        alphaCost + aCost + bCost + betaCost + cCost +
          rescale(Lib.rows(a).ae.evalLong * Lib.cols(a).ae.evalLong * Lib.rows(b).ae.evalLong, 6, 10) + 1

      case (
        ExternFunctionCall(TensorflowIdioms.MAT_MUL, Seq(_, _, b, a), _, _),
        Seq(bTransposeCost, aTransposeCost, bCost, aCost)) =>

        bTransposeCost + aTransposeCost + bCost + aCost +
          rescale(Lib.rows(a).ae.evalLong * Lib.cols(a).ae.evalLong * Lib.rows(b).ae.evalLong, 6, 10) + 1

      case (
        ExternFunctionCall(PytorchIdioms.MAT_MUL, Seq(b, a), _, _),
        Seq(bCost, aCost)) =>

        bCost + aCost +
          rescale(Lib.rows(a).ae.evalLong * Lib.cols(a).ae.evalLong * Lib.rows(b).ae.evalLong, 6, 10) + 1

      case (
        ExternFunctionCall("matrix_mul", Seq(_, _, _, b, a, _, _), _, _),
        Seq(_, _, _, bCost, aCost, _, _)) =>

        aCost + bCost +
          rescale(Lib.rows(a).ae.evalLong * Lib.cols(a).ae.evalLong * Lib.rows(b).ae.evalLong, 6, 10) + 1

      case (
        ExternFunctionCall("idiom_dgemv", Seq(_, _, _, a, _, _, _, _), _, _),
        Seq(cCost, betaCost, bCost, aCost, alphaCost, _, _, _)) =>

        alphaCost + aCost + bCost + betaCost + cCost +
          rescale(Lib.rows(a).ae.evalLong * Lib.cols(a).ae.evalLong, 7, 10) + 1

      case (ExternFunctionCall(TensorflowIdioms.MAT_VEC, Seq(_, _, a), _, _), Seq(_, bCost, aCost)) =>
        aCost + bCost +
          rescale(Lib.rows(a).ae.evalLong * Lib.cols(a).ae.evalLong, 7, 10) + 1

      case (
        ExternFunctionCall(PytorchIdioms.MAT_VEC, Seq(b, a), _, _),
        Seq(bCost, aCost)) =>

        bCost + aCost +
          rescale(Lib.rows(a).ae.evalLong * Lib.cols(a).ae.evalLong, 7, 10) + 1

      case (
        ExternFunctionCall("idiom_ddot", Seq(_, right, _, _, _), _, _),
        Seq(_, rightCost, _, leftCost, _)) =>

        rightCost + leftCost + rescale(Lib.length(right).ae.evalLong, 8, 10) + 1

      case (ExternFunctionCall(TensorflowIdioms.TENSOR_DOT, Seq(_, right, _), _, _), Seq(_, rightCost, leftCost)) =>
        rightCost + leftCost + rescale(Lib.length(right).ae.evalLong, 8, 10) + 1

      case (ExternFunctionCall(PytorchIdioms.DOT, Seq(right, _), _, _), Seq(rightCost, leftCost)) =>
        rightCost + leftCost + rescale(Lib.length(right).ae.evalLong, 8, 10) + 1

      case (ExternFunctionCall(TensorflowIdioms.REDUCE_SUM | PytorchIdioms.SUM, Seq(vec), _, _), Seq(vecCost)) =>
        vecCost + rescale(Lib.length(vec).ae.evalLong, 8, 10) + 1

      case (
        ExternFunctionCall("idiom_axpy", Seq(_, right, _, _, _, _), _, _),
        Seq(_, rightCost, _, leftCost, alphaCost, _)) =>

        alphaCost + rightCost + leftCost + rescale(Lib.length(right).ae.evalLong, 8, 10) + 1

      case (
        ExternFunctionCall(TensorflowIdioms.ADD | TensorflowIdioms.MUL | PytorchIdioms.ADD | PytorchIdioms.MUL, Seq(right, left), _, _),
        Seq(leftCost, rightCost)) =>

        rightCost + leftCost +
          rescale(Lib.flatLength(left).evalLong + Lib.flatLength(right).evalLong, 4, 10) + 1

      case (
        ExternFunctionCall("idiom_transpose", Seq(matrix, _, _), _, _),
        Seq(matrixCost, _, _)) =>

        matrixCost + rescale(Lib.flatLength(matrix).evalLong, 9, 10)

      case (
        ExternFunctionCall(TensorflowIdioms.TRANSPOSE | PytorchIdioms.TRANSPOSE, args, _, _),
        costs) =>

        costs.last + rescale(Lib.flatLength(args.last).evalLong, 9, 10)

      case (
        ExternFunctionCall("idiom_memset", Seq(_, _), _, t),
        Seq(valueCost, bytesCost)) =>

        valueCost + bytesCost + rescale(t.asInstanceOf[ArrayTypeT].len.ae.evalLong, 8, 10) + 1

      case (
        ExternFunctionCall(TensorflowIdioms.FILL | PytorchIdioms.FULL, Seq(_, _), _, t),
        Seq(lengthCost, valueCost)) =>

        valueCost + lengthCost + rescale(Lib.flatLength(t).evalLong, 8, 10) + 1

      case (
        ExternFunctionCall(TensorflowIdioms.SLIDING_WINDOW, Seq(_, _), _, t),
        Seq(windowWidthCost, dataCost)) =>

        dataCost + windowWidthCost + rescale(Lib.flatLength(t).evalLong, 9, 10) + 1

      case (
        ExternFunctionCall(PytorchIdioms.UNFOLD, Seq(_, _, _, _), _, t),
        Seq(_, windowWidthCost, _, dataCost)) =>

        dataCost + windowWidthCost + rescale(Lib.flatLength(t).evalLong, 9, 10) + 1

      case (Build(_, n, _, _), Seq(costF)) => n.ae.evalLong * (costF + 1) + 1
      case (Ifold(_, _, n, _), Seq(idxfCost, initCost)) => initCost + n.ae.evalLong * idxfCost + 1
      case (Idx(_, _, _, _), Seq(arrayCost, indexCost)) => arrayCost + indexCost + 1
      case (BinaryOp(_, _, _, _, _), Seq(in1Cost, in2Cost)) => in1Cost + in2Cost + 1
      case (ParamUse(_), _) => 2
      case (_: DeBruijnParamUse, _) => 2
      case (Constant(_, _), _) => 1
      case (TypeArith(_, _), _) => 1
      case (DeBruijnShift(_), Seq(innerCost)) => innerCost + 1
      case (DeBruijnForall(_), Seq(innerCost)) => innerCost + 1
      case (CLambda(_, _, _), Seq(bodyCost)) => bodyCost + 1
      case (_: DeBruijnLambda, Seq(bodyCost)) => bodyCost + 1
      case (FunctionCall(_, _, _), Seq(fCost, argCost)) => fCost + argCost + 1
      case (Tuple(_, _, _), Seq(lhsCost, rhsCost)) => lhsCost + rhsCost + 1
      case (Fst(_, _, _), Seq(tupleCost)) => tupleCost + 1
      case (Snd(_, _, _), Seq(tupleCost)) => tupleCost + 1
      case (If(_, _, _, _), Seq(condCost, ifTrueCost, ifFalseCost)) => condCost + ifTrueCost + ifFalseCost + 1

      case _ => ???
    },
    {
      case DeBruijnForall(_) => false
      case DeBruijnShift(_) => false
      case _ => true
    })

  private def rescale(value: BigInt, numerator: Int, denominator: Int): BigInt = {
    val (a, b) = value * numerator /% denominator
    if (b == 0) {
      a
    } else {
      a + 1
    }
  }

  /**
    * Attempts to recover a concrete expression from an e-class.
    *
    * @param eClass The e-class to extract an expression from.
    * @return An expression equivalent to `eClass` or `None`.
    */
  override def apply(eClass: EClassT): Option[Expr] = analysis.extractor.apply(eClass)
}
