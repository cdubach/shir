package cGen.idioms

import cGen.idioms.PrepareForPytorch.isListOfTensors
import cGen.{ArrayType, ArrayTypeVar, Build, CGenExpr, CLambda, Constant, ExternFunctionCall, IntType, Tuple}
import core.{AnyTypeVar, Expr, FunctionCall, ParamDef, ParamUse, TypeChecker}

/**
  * Prepares expressions in the idiom-oriented dialect for TensorFlow code generation.
  */
object PrepareForTensorflow {
  def apply(expr: Expr, createFunctions: Boolean = true): Expr = {
    // Insert tf.tensor/tf.stack calls whenever new arrays are created. The only exception is if the array is consumed
    // by a TensorFlow method that takes a list instead of a tensor.
    val tensorified = Tensorify(expr, listToTensor)

    if (createFunctions) {
      packageGraphs(TypeChecker.check(tensorified))
    } else {
      tensorified
    }
  }

  private def packageGraphs(expr: Expr): Expr = {
    expr match {
      case call if isWorthPackaging(call) =>
        val graph = extractGraph(call)

        val paddedGraph = if (graph.params.isEmpty) {
          val i32 = IntType(32)
          TensorflowGraph(graph.body, Seq(ParamDef(i32)), Seq(Constant(0.0, i32)))
        } else {
          graph
        }

        val lambda = TypeChecker.check(CLambda(paddedGraph.params.reverse, paddedGraph.body))
        val fun = ExternFunctionCall(TensorflowIdioms.FUNCTION, Seq(lambda), Seq(AnyTypeVar()), lambda.t)
        FunctionCall(fun, paddedGraph.args.reverse)

      case cGenExpr: CGenExpr => cGenExpr.buildFromArgs(cGenExpr.args.map(packageGraphs))

      case other => other.map({
        case arg: Expr => packageGraphs(arg)
        case other => other
      }).asInstanceOf[Expr]
    }
  }

  private def isWorthPackaging(expr: Expr): Boolean = {
    expr match {
//      case ExternFunctionCall(_, args, _, _) if isPackageableCall(expr) => args.exists(isPackageableCall)
      case ExternFunctionCall(_, _, _, _) if isPackageableCall(expr) => true
      case _ => false
    }
  }

  private def isPackageableCall(expr: Expr): Boolean = {
    expr match {
      case ExternFunctionCall(TensorflowIdioms.TENSOR, _, _, _) => false
      case ExternFunctionCall(TensorflowIdioms.STACK, Seq(Build(_, _, _, _)), _, _) => false
      case ExternFunctionCall(name, _, _, _) if name.startsWith("tf.") => true
      case _ => false
    }
  }

  private case class TensorflowGraph(body: Expr, params: Seq[ParamDef], args: Seq[Expr])

  private def extractGraph(expr: Expr): TensorflowGraph = {
    expr match {
      case ExternFunctionCall(name, args, intvs, outt) if isPackageableCall(expr) =>
        val packagedArgs = args.map(extractGraph)
        TensorflowGraph(
          ExternFunctionCall(name, packagedArgs.map(_.body), intvs, outt),
          packagedArgs.flatMap(_.params),
          packagedArgs.flatMap(_.args))

      case const@Constant(_, _) => TensorflowGraph(const, Seq(), Seq())
      case const@TypeArith(_, _) => TensorflowGraph(const, Seq(), Seq())

      case FunctionCall(callee, arg, _) =>
        val packagedCallee = extractGraph(callee)
        val packagedArg = extractGraph(arg)
        TensorflowGraph(
          FunctionCall(packagedCallee.body, packagedArg.body),
          packagedCallee.params ++ packagedArg.params,
          packagedCallee.args ++ packagedArg.args)

      case CLambda(param, body, _) =>
        val packagedBody = extractGraph(body)
        TensorflowGraph(CLambda(param, body), packagedBody.params, packagedBody.args)

      case Tuple(left, right, _) =>
        val leftGraph = extractGraph(left)
        val rightGraph = extractGraph(right)
        TensorflowGraph(
          Tuple(leftGraph.body, rightGraph.body),
          leftGraph.params ++ rightGraph.params,
          leftGraph.args ++ rightGraph.args)

      case _ =>
        val param = ParamDef(expr.t)
        TensorflowGraph(ParamUse(param), Seq(param), Seq(expr))
    }
  }

  private def listToTensor(expr: Expr): Expr = expr.t match {
    case at@ArrayType(et, n) =>
      if (isListOfTensors(expr)) {
        ExternFunctionCall(TensorflowIdioms.STACK, Seq(expr), Seq(ArrayTypeVar(et, n)), at)
      } else {
        ExternFunctionCall(TensorflowIdioms.TENSOR, Seq(expr), Seq(ArrayTypeVar(et, n)), at)
      }

    case _ => ???
  }
}
