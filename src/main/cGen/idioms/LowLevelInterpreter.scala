package cGen.idioms

import cGen.AlgoOp.AlgoOp
import cGen.{AlgoOp, BinaryOp, Build, CGenExpr, Constant, Ifold, IntType, Tuple}
import eqsat.{DeBruijnLambda, DeBruijnParamUse}
import cGen.primitive.{Fst, Idx, Snd}
import core.{Expr, FunctionCall, Type, Value}

/**
 * An interpreter for low-level expressions that may contain build and ifold.
 */
object LowLevelInterpreter {
  trait Value {
    def t: Type
  }
  case class Scalar(value: Double, t: Type) extends Value
  case class Array(values: Seq[Value], t: Type) extends Value
  case class Pair(fst: Value, snd: Value, t: Type) extends Value
  case class Function(f: DeBruijnLambda) extends Value {
    override def t: Type = f.t
  }

  case class Context(freeVariables: Map[Int, Value], boundVariables: List[Value])

  case class InterpreterValueExpr(innerIR: Expr, types: Seq[Type], value: Value) extends CGenExpr {
    override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): InterpreterValueExpr = InterpreterValue(value)

    override def argsBuild(args: Seq[Expr]): CGenExpr = this
  }

  object InterpreterValue {
    def apply(value: Value): InterpreterValueExpr =
      InterpreterValueExpr(Value(value.t), Seq(value.t), value)

    def unapply(expr: InterpreterValueExpr): Some[(Value, Type)] = Some(expr.value, expr.t)
  }

  private val scalarOps: Map[AlgoOp, (Double, Double) => Double] = Map(
    AlgoOp.Add -> (_ + _),
    AlgoOp.Sub -> (_ - _),
    AlgoOp.Mul -> (_ * _),
    AlgoOp.Div -> (_ / _),
    AlgoOp.Mod -> (_ % _)
  )

  def evaluate(expr: Expr, context: Context): Value = expr match {
    case InterpreterValue(value, _) => value
    case lambda: DeBruijnLambda => Function(lambda)
    case Constant(v, t) => Scalar(v, t)
    case use: DeBruijnParamUse =>
      val (i, t) = (use.index, use.t)
      val boundVal = context.boundVariables(i)
      if (boundVal.t != t) {
        throw new Exception()
      }
      boundVal

    case Build(idxf, n, _, t) =>
      Array(Seq.range(0, n.ae.evalInt).map(i => evaluate(FunctionCall(idxf, Constant(i, IntType(32))), context)), t)

    case Idx(arr, index, _, _) =>
      evaluate(arr, context).asInstanceOf[Array].values(evaluate(index, context).asInstanceOf[Scalar].value.toInt)

    case Ifold(idxf, init, n, _) =>
      val idxfEval = evaluate(idxf, context)
      Seq.range(0, n.ae.evalInt).foldLeft(evaluate(init, context))((acc, i) =>
        evaluateFunctionCall(evaluateFunctionCall(idxfEval, Scalar(i, IntType(32)), context), acc, context))

    case Fst(tup, _, _) => evaluate(tup, context).asInstanceOf[Pair].fst
    case Snd(tup, _, _) => evaluate(tup, context).asInstanceOf[Pair].snd
    case Tuple(fst, snd, t) => Pair(evaluate(fst, context), evaluate(snd, context), t)

    case FunctionCall(f, arg, _) => evaluateFunctionCall(evaluate(f, context), evaluate(arg, context), context)

    case BinaryOp(in1, in2, op, _, _) if scalarOps.contains(op) =>
      val in1Val = evaluate(in1, context).asInstanceOf[Scalar]
      val in2Val = evaluate(in2, context).asInstanceOf[Scalar]
      Scalar(scalarOps(op)(in1Val.value, in2Val.value), in1Val.t)

    case _ => ???
  }

  private def evaluateFunctionCall(f: Value, arg: Value, context: Context): Value = {
    val newContext = Context(context.freeVariables, arg :: context.boundVariables)
    evaluate(f.asInstanceOf[Function].f.body, newContext)
  }
}
