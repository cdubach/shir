package cGen.idioms

import eqsat._
import cGen.primitive.Idx
import cGen._
import core.{ArithTypeVar, ExprVar}

/**
  * Rewrite rules for recognizing BLAS idioms.
  */
case class BlasIdiomRules(extractor: Extractor) {
  private def oneWayRules: Seq[Rewrite] = Seq(axpyRule, gemmRule, gemvRule, ddotRule, memsetZeroRule, transposeRule)
  private def oneWayOptRules: Seq[Rewrite] =
    Seq(
      foldTransposeIntoGemvRule, foldUntransposeIntoGemvRule,
      foldTransposeAIntoGemmRule, foldTransposeBIntoGemmRule, foldTransposeAIntoSemitransposedGemmRule,
      foldUntransposeAIntoGemmRule, foldUntransposeBIntoGemmRule, foldUntransposeAIntoSemitransposedGemmRule,
      hoistMulFromDot)

  /**
    * The set of idiom recognition rules.
    * @return A set of idiom recognition rules.
    */
  def rules: Seq[Rewrite] = oneWayRules ++ oneWayRules.map(_.reverse) ++ oneWayOptRules ++ oneWayOptRules.map(_.reverse)

  /**
    * A rewrite rule that recognizes ax + y patterns and rewrites them as BLAS calls.
    * @return A rewrite rule.
    */
  def axpyRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val alpha = ExprVar(t())
    val n = ArithTypeVar()
    val x = ExprVar(ArrayType(t(), n))
    val y = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FindAXPY",
      Pattern({
          Build(
            DeBruijnLambda(i32,
              BinaryOp(
                BinaryOp(alpha, Idx(x, DeBruijnParamUse(0, i32), EagerType()), AlgoOp.Mul),
                Idx(y, DeBruijnParamUse(0, i32), EagerType()),
                AlgoOp.Add)),
            n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(Extern.AXPY(alpha, x, y), mayRename = _ => false),
        extractor,
        Seq(alpha -> -1, x -> -1, y -> -1))
    )
  }

  /**
    * A rewrite rule that recognizes GEMM patterns and rewrites them as BLAS calls.
    * @return A rewrite rule.
    */
  def gemmRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), m), n))
    val b = ExprVar(ArrayType(ArrayType(t(), m), k))
    val c = ExprVar(ArrayType(ArrayType(t(), k), n))
    Rewrite(
      "FindGEMM",
      Pattern({
        Build(
          DeBruijnLambda(i32,
            Extern.GEMV(
              alpha,                                           // t
              b,                                               // [k x [m x t]]
              Idx(a, DeBruijnParamUse(0, i32), EagerType()),   // [m x t]
              beta,                                            // t
              Idx(c, DeBruijnParamUse(0, i32), EagerType()))), // [k x t]
          n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(Extern.GEMM(alpha, a, b, beta, c, BTransposed = true), mayRename = _ => false),
        extractor,
        Seq(alpha -> -1, a -> -1, b -> -1, beta -> -1, c -> -1)))
    }

  /**
    * A rewrite rule that recognizes GEMV patterns and rewrites them as BLAS calls.
    * @return A rewrite rule.
    */
  def gemvRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), m), n))
    val b = ExprVar(ArrayType(t(), m))
    val c = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FindGEMV",
      Pattern(
        Build(
          DeBruijnLambda(i32,
            BinaryOp(
              BinaryOp(alpha, Extern.DoubleDotProd(Idx(a, DeBruijnParamUse(0, i32), EagerType()), b), AlgoOp.Mul),
              BinaryOp(
                beta,
                Idx(c, DeBruijnParamUse(0, i32), EagerType()),
                AlgoOp.Mul),
              AlgoOp.Add)),
          n)),
      ExtractionBasedShiftingApplier(
        Pattern(Extern.GEMV(alpha, a, b, beta, c), mayRename = _ => false),
        extractor,
        Seq(alpha -> -1, a -> -1, b -> -1, beta -> -1, c -> -1)))
  }

  /**
    * A rewrite rule that recognizes matrix multiplication patterns and rewrites them as specialized calls.
    * @return A rewrite rule.
    */
  def matmulRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), m), n))
    val b = ExprVar(ArrayType(ArrayType(t(), m), k))
    Rewrite(
      "FindMatMul",
      Pattern({
        Build(
          DeBruijnLambda(i32,
            Build(
              DeBruijnLambda(i32,
                Ifold(
                  DeBruijnLambda(i32,
                    DeBruijnLambda(t(),
                      BinaryOp(
                        BinaryOp(
                          Idx(Idx(a, DeBruijnParamUse(3, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType()),
                          Idx(Idx(b, DeBruijnParamUse(1, i32), EagerType()), DeBruijnParamUse(2, i32), EagerType()),
                          AlgoOp.Mul),
                        DeBruijnParamUse(0, t()), AlgoOp.Add))),
                  Constant(0.0, t()),
                  m)),
              k)),
          n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(Extern.MatrixMul(a, b), mayRename = _ => false),
        extractor,
        Seq(a -> -4, b -> -4)))
  }

  /**
    * A rewrite rule that recognizes dot product patterns and rewrites them as specialized calls.
    * @return A rewrite rule.
    */
  def ddotRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val a = ExprVar(ArrayType(t(), n))
    val b = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FindDDot",
      Pattern({
        Ifold(
          DeBruijnLambda(i32,
            DeBruijnLambda(t(),
              BinaryOp(
                BinaryOp(
                  Idx(a, DeBruijnParamUse(1, i32), EagerType()),
                  Idx(b, DeBruijnParamUse(1, i32), EagerType()),
                  AlgoOp.Mul),
                DeBruijnParamUse(0, t()), AlgoOp.Add))),
          Constant(0.0, t()),
          n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(Extern.DoubleDotProd(a, b), mayRename = _ => false),
        extractor,
        Seq(a -> -2, b -> -2)))
  }

  /**
    * A rewrite rule that recognizes all-zero arrays as compatible with the memset function.
    * @return A rewrite rule.
    */
  def memsetZeroRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val n = ArithTypeVar()
    Rewrite(
      "FindMemsetZero",
      Build(DeBruijnLambda(i32, Constant(0.0, t())), n),
      Extern.memset(0, ArrayType(t(), n)))
    }

  /**
    * A rewrite rule that recognizes matrix transpositions.
    * @return A rewrite rule.
    */
  def transposeRule: Rewrite = {
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(DoubleType(), n), m))
    Rewrite(
      "FindTranspose",
      Pattern(
        Build(
          DeBruijnLambda(i32,
            Build(
              DeBruijnLambda(i32,
                Idx(Idx(a, DeBruijnParamUse(0, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType())),
              m)),
          n)),
      ExtractionBasedShiftingApplier(
        Pattern(Extern.MatrixTranspose(a), mayRename = _ => false),
        extractor,
        Seq(a -> -2)))
  }

  def hoistMulFromDot: Rewrite = {
    val t = DoubleType()
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val alpha = ExprVar(t)
    val a = ExprVar(ArrayType(t, n))
    val b = ExprVar(ArrayType(t, n))
    Rewrite(
      "HoistLhsMulFromDot",
      Pattern(
        Extern.DoubleDotProd(
          Build(DeBruijnLambda(i32, BinaryOp(alpha, Idx(a, DeBruijnParamUse(0, i32), EagerType()), AlgoOp.Mul)), n),
          b)),
      ExtractionBasedShiftingApplier(
        Pattern(BinaryOp(alpha, Extern.DoubleDotProd(a, b), AlgoOp.Mul), mayRename = _ => false),
        extractor,
        Seq(alpha -> -1, a -> -1)))
  }

  /**
    * A rewrite rule that folds a matrix transpose into a GEMV call.
    * @return A rewrite rule.
    */
  def foldTransposeIntoGemvRule: Rewrite = {
    implicit val t = DoubleType
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), n), m))
    val b = ExprVar(ArrayType(t(), m))
    val c = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FoldTransposeIntoGEMV",
      Extern.GEMV(alpha, Extern.MatrixTranspose(a), b, beta, c),
      Extern.GEMV(alpha, a, b, beta, c, ATransposed = true))
  }

  /**
    * A rewrite rule that folds a matrix transpose into a GEMV call.
    * @return A rewrite rule.
    */
  def foldUntransposeIntoGemvRule: Rewrite = {
    implicit val t = DoubleType
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), n), m))
    val b = ExprVar(ArrayType(t(), m))
    val c = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FoldTransposeIntoGEMV",
      Extern.GEMV(alpha, Extern.MatrixTranspose(a), b, beta, c, ATransposed = true),
      Extern.GEMV(alpha, a, b, beta, c))
  }

  /**
    * A rewrite rule that folds a matrix transpose into a GEMM call.
    * @return A rewrite rule.
    */
  def foldTransposeAIntoGemmRule: Rewrite = {
    implicit val t = DoubleType
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), n), m))
    val b = ExprVar(ArrayType(ArrayType(t(), m), k))
    val c = ExprVar(ArrayType(ArrayType(t(), k), n))
    Rewrite(
      "FoldTransposeAIntoGEMM",
      Extern.GEMM(alpha, Extern.MatrixTranspose(a), b, beta, c),
      Extern.GEMM(alpha, a, b, beta, c, ATransposed = true))
  }

  /**
    * A rewrite rule that folds a matrix transpose into a GEMM call.
    * @return A rewrite rule.
    */
  def foldUntransposeAIntoGemmRule: Rewrite = {
    implicit val t = DoubleType
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), n), m))
    val b = ExprVar(ArrayType(ArrayType(t(), m), k))
    val c = ExprVar(ArrayType(ArrayType(t(), k), n))
    Rewrite(
      "FoldUntransposeAIntoGEMM",
      Extern.GEMM(alpha, Extern.MatrixTranspose(a), b, beta, c, ATransposed = true),
      Extern.GEMM(alpha, a, b, beta, c))
  }

  /**
    * A rewrite rule that folds a matrix transpose into a GEMM call.
    * @return A rewrite rule.
    */
  def foldTransposeAIntoSemitransposedGemmRule: Rewrite = {
    implicit val t = DoubleType
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), n), m))
    val b = ExprVar(ArrayType(ArrayType(t(), k), m))
    val c = ExprVar(ArrayType(ArrayType(t(), k), n))
    Rewrite(
      "FoldTransposeAIntoSemitransposedGEMM",
      Extern.GEMM(alpha, Extern.MatrixTranspose(a), b, beta, c, BTransposed = true),
      Extern.GEMM(alpha, a, b, beta, c, ATransposed = true, BTransposed = true))
  }

  /**
    * A rewrite rule that folds a matrix transpose into a GEMM call.
    *
    * @return A rewrite rule.
    */
  def foldUntransposeAIntoSemitransposedGemmRule: Rewrite = {
    implicit val t = DoubleType
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), n), m))
    val b = ExprVar(ArrayType(ArrayType(t(), k), m))
    val c = ExprVar(ArrayType(ArrayType(t(), k), n))
    Rewrite(
      "FoldTransposeAIntoSemitransposedGEMM",
      Extern.GEMM(alpha, Extern.MatrixTranspose(a), b, beta, c, ATransposed = true, BTransposed = true),
      Extern.GEMM(alpha, a, b, beta, c, BTransposed = true))
  }

  /**
    * A rewrite rule that folds a matrix transpose into a GEMM call.
    * @return A rewrite rule.
    */
  def foldTransposeBIntoGemmRule: Rewrite = {
    implicit val t = DoubleType
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), m), n))
    val b = ExprVar(ArrayType(ArrayType(t(), k), m))
    val c = ExprVar(ArrayType(ArrayType(t(), k), n))
    Rewrite(
      "FoldTransposeBIntoGEMM",
      Extern.GEMM(alpha, a, Extern.MatrixTranspose(b), beta, c),
      Extern.GEMM(alpha, a, b, beta, c, BTransposed = true))
  }

  /**
    * A rewrite rule that folds a matrix transpose into a GEMM call.
    * @return A rewrite rule.
    */
  def foldUntransposeBIntoGemmRule: Rewrite = {
    implicit val t = DoubleType
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), m), n))
    val b = ExprVar(ArrayType(ArrayType(t(), k), m))
    val c = ExprVar(ArrayType(ArrayType(t(), k), n))
    Rewrite(
      "FoldUntransposeBIntoGEMM",
      Extern.GEMM(alpha, a, Extern.MatrixTranspose(b), beta, c, BTransposed = true),
      Extern.GEMM(alpha, a, b, beta, c))
  }
}
