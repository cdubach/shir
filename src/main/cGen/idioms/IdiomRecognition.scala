package cGen.idioms

import cGen.PatternMatchingRewriter.{ParamReifier, RewriteRule, TypeReifier}
import cGen._
import core.{ArithTypeVar, Expr, ParamDef}

/**
  * Rewrites idioms as intrinsics and optimizes those intrinsics.
  */
object IdiomRecognition {
  def apply(expr: Expr): Expr = {
    rewriter.apply(expr)
  }

  /**
    * A set of rewrite rules that rewrite general-purpose computation patterns as specialized intrinsics.
    */
  private val instructionDefs: Seq[RewriteRule] = Seq(
    {
      // Pre-transposed matrix products in double precision.
      val scalarType = DoubleType()
      val n = ArithTypeVar()
      val m = ArithTypeVar()
      val k = ArithTypeVar()
      val lhs = ParamDef(ArrayType(ArrayType(scalarType, m), n))
      val rhs = ParamDef(ArrayType(ArrayType(scalarType, m), k))
      (
        "DoubleMatrixMulPretransposed",
        Lib.MatrixMulPretransposed(lhs, rhs)(isTransposeLazy = true, () => scalarType),
        (reifyParam, _) => Intrinsics.MatrixMul(reifyParam(lhs), reifyParam(rhs), rightTransposed = true)
      )
    },
    {
      // Matrix transposition in double precision.
      val scalarType = DoubleType()
      val n = ArithTypeVar()
      val m = ArithTypeVar()
      val matrix = ParamDef(ArrayType(ArrayType(scalarType, m), n))
      (
        "DoubleMatrixTranspose",
        Lib.transpose(matrix, EagerType()),
        (reifyParam, _) =>
          Intrinsics.MatrixTranspose(reifyParam(matrix))
      )
    },
    {
      // Dot products in double precision.
      val scalarType = DoubleType()
      val size = ArithTypeVar()
      val lhs = ParamDef(ArrayType(scalarType, size))
      val rhs = ParamDef(ArrayType(scalarType, size))
      (
        "DoubleDotProduct",
        Lib.DotProd(lhs, rhs)(() => scalarType),
        (reifyParam, _) => Intrinsics.DotProd(reifyParam(lhs), reifyParam(rhs))
      )
    }
  )

  private val peepholeOptimizations: Seq[RewriteRule] = Seq(
    {
      // Transposing matrices twice is equivalent to the identity function.
      val scalarType = DoubleType()
      val n = ArithTypeVar()
      val m = ArithTypeVar()
      val matrix = ParamDef(ArrayType(ArrayType(scalarType, m), n))
      (
        "DoubleEliminateBackToBackMatrixTranspose",
        Lib.transpose(Lib.transpose(matrix, EagerType()), EagerType()),
        (reifyParam, _) => reifyParam(matrix)
      )
    }
  )

  /**
    * Rewrite rules that fold transpositions into matrix multiplications.
    */
  private val matrixMulFolding: Seq[RewriteRule] = {
    val rules = for (leftTransposed <- Seq(true, false); rightTransposed <- Seq(true, false))
      yield {
        val scalarType = DoubleType()
        val n = ArithTypeVar()
        val m = ArithTypeVar()
        val k = ArithTypeVar()
        val l = ArithTypeVar()
        val lhs = ParamDef(ArrayType(ArrayType(scalarType, n), m))
        val rhs = ParamDef(ArrayType(ArrayType(scalarType, k), l))
        val suffix1 = if (leftTransposed) "LT" else ""
        val suffix2 = if (rightTransposed) "RT" else ""
        (
          (
            s"FoldLhsTransposeIntoMatrixMul$suffix1$suffix2",
            Intrinsics.MatrixMul(
              Intrinsics.MatrixTranspose(lhs),
              rhs,
              leftTransposed = leftTransposed,
              rightTransposed = rightTransposed),
            (reifyParam: ParamReifier, _: TypeReifier) =>
              Intrinsics.MatrixMul(
                reifyParam(lhs),
                reifyParam(rhs),
                leftTransposed = !leftTransposed,
                rightTransposed = rightTransposed)
          ),
          (
            s"FoldRhsTransposeIntoMatrixMul$suffix1$suffix2",
            Intrinsics.MatrixMul(
              lhs,
              Intrinsics.MatrixTranspose(rhs),
              leftTransposed = leftTransposed,
              rightTransposed = rightTransposed),
            (reifyParam: ParamReifier, _: TypeReifier) =>
              Intrinsics.MatrixMul(
                reifyParam(lhs),
                reifyParam(rhs),
                leftTransposed = leftTransposed,
                rightTransposed = !rightTransposed)
          ))
      }
    val (lhs, rhs) = rules.unzip
    lhs ++ rhs
  }

  /**
    * The full set of rewrite rules used by the idiom recognition pass.
    */
  val rewriteRules: Seq[RewriteRule] = instructionDefs ++ peepholeOptimizations ++ matrixMulFolding

  private val rewriter = new PatternMatchingRewriter(
    rewriteRules,
    order = PatternMatchingRewriter.RewriteOrder.BottomUp,
    preprocessRules = true)
}
