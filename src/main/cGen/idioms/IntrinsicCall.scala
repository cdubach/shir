package cGen.idioms

import cGen.{CFunType, CGenExpr, EagerType}
import core._

/**
 * The tree node kind for intrinsic call expressions.
 * @param intrinsicName The name of the intrinsic.
 * @param arity The number of arguments the intrinsic takes.
 */
private case class IntrinsicCallExprKind(intrinsicName: String, arity: Int) extends TreeNodeKind

case class IntrinsicCallExpr(innerIR: Expr, name: String, ins: Seq[Expr], intvs: Seq[TypeVarT], outt: Type) extends CGenExpr {
  override def types: Seq[Type] = Seq()

  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): IntrinsicCallExpr =
    IntrinsicCallExpr(newInnerIR, name, ins, intvs, outt)

  override protected def argsBuild(args: Seq[Expr]): CGenExpr =
    IntrinsicCall(name, args, intvs, outt)

  override def nodeKind: TreeNodeKind = this match {
    case IntrinsicCall(name, args, _, _) => IntrinsicCallExprKind(name, args.length)
  }

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case IntrinsicCall(name, args, _, _) =>
            Some(formatter.makeAtomic(s"$name(${args.map(formatter.formatNonAtomic).mkString(", ")})"))
        }

      case _ => super.tryFormat(formatter)
    }
  }
}

object IntrinsicCall { // the input intvs/outt/ins/ should be reversed
  def apply(name: String, ins: Seq[Expr], intvs: Seq[TypeVarT], outt: Type): IntrinsicCallExpr = {
    IntrinsicCallExpr({
      FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(intvs, CFunType(intvs, outt, EagerType()))), ins.map(_.t)), ins)
    }, name, ins, intvs, outt)
  }

  def unapply(expr: IntrinsicCallExpr): Some[(String, Seq[Expr], Seq[TypeVarT], Type)] = {
    Some(expr.name, expr.ins.indices.map(expr.args(_)), expr.intvs, expr.outt)
  }
}