package cGen.idioms

import cGen.CLambda
import core.{Expr, FunctionCall, ParamDef, ParamUse}

object EliminateUselessLambdas {
  def apply(expr: Expr): Expr = {
    expr.visitAndRebuild({
      // Eliminate function calls to lambdas that never use the function call argument.
      case FunctionCall(CLambda(p, body, _), _, _) if !usesParam(body, p) => body
      case other => other
    }).asInstanceOf[Expr]
  }

  private def usesParam(expr: Expr, p: ParamDef): Boolean = {
    var foundAny = false
    expr.visit({
      case ParamUse(used) if p.id == used.id =>
        foundAny = true
      case _ =>
    })
    foundAny
  }
}
