package cGen.idioms

import eqsat._
import cGen.primitive.Idx
import cGen._
import core.{ArithTypeVar, Expr, ExprVar}

/**
 * Rewrite rules for recognizing BLAS idioms in a piecewise fashion.
 */
case class PiecewiseBlasIdiomRules(extractor: Extractor) {
  private def tryExtract(eClass: EClassT, contextElisions: Int): Option[Expr] = {
    extractor(eClass).flatMap(expr =>
      DeBruijnParamUse.tryDecrementUnbound(
        DeBruijnShift.elideRec(expr).asInstanceOf[Expr],
        contextElisions))
  }

  /**
   * The set of idiom recognition rules.
   * @return A set of idiom recognition rules.
   */
  def rules: Seq[Rewrite] = Seq(gemmRule, gemvRule, ddotRule)

  /**
   * A rewrite rule that recognizes GEMM patterns and rewrites them as BLAS calls.
   * @return A rewrite rule.
   */
  def gemmRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), m), n))
    val b = ExprVar(ArrayType(ArrayType(t(), m), k))
    val c = ExprVar(ArrayType(ArrayType(t(), k), n))
    Rewrite(
      "FindGEMM",
      Pattern({
        Build(
          DeBruijnLambda(i32, Extern.GEMV(alpha, a, Idx(b, DeBruijnParamUse(0, i32), EagerType()), beta, c)),
          n)
      }),
      GeneratedApplier((m, _) => {
        val shiftedAlpha = tryExtract(m.instantiate(alpha), 1)
        val shiftedA = tryExtract(m.instantiate(a), 1)
        val shiftedB = tryExtract(m.instantiate(b), 2)
        val shiftedBeta = tryExtract(m.instantiate(beta), 1)
        val shiftedC = tryExtract(m.instantiate(c), 1)
        (shiftedAlpha, shiftedA, shiftedB, shiftedBeta, shiftedC) match {
          case (Some(alpha), Some(a), Some(b), Some(beta), Some(c)) =>
            Pattern(Extern.GEMM(alpha, a, b, beta, c, BTransposed = true), mayRename = _ => false)
          case _ =>
            NopApplier
        }
      }))
  }

  /**
   * A rewrite rule that recognizes GEMV patterns and rewrites them as BLAS calls.
   * @return A rewrite rule.
   */
  def gemvRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val alpha = ExprVar(t())
    val beta = ExprVar(t())
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), m), n))
    val b = ExprVar(ArrayType(t(), m))
    val c = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FindGEMV",
      Pattern(
        Build(
          DeBruijnLambda(i32,
            BinaryOp(
              Extern.DoubleDotProd(
                Build(
                  DeBruijnLambda(i32,
                    BinaryOp(
                      alpha,
                      Idx(Idx(a, DeBruijnParamUse(1, i32), EagerType()), DeBruijnParamUse(0, i32), EagerType()),
                      AlgoOp.Mul)),
                  m),
                b),
              BinaryOp(
                beta,
                Idx(c, DeBruijnParamUse(0, i32), EagerType()),
                AlgoOp.Mul),
              AlgoOp.Add)),
          n)),
      GeneratedApplier((m, _) => {
        val shiftedAlpha = tryExtract(m.instantiate(alpha), 2)
        val shiftedA = tryExtract(m.instantiate(a), 2)
        val shiftedB = tryExtract(m.instantiate(b), 2)
        val shiftedBeta = tryExtract(m.instantiate(beta), 1)
        val shiftedC = tryExtract(m.instantiate(c), 1)
        (shiftedAlpha, shiftedA, shiftedB, shiftedBeta, shiftedC) match {
          case (Some(alpha), Some(a), Some(b), Some(beta), Some(c)) =>
            Pattern(Extern.GEMV(alpha, a, b, beta, c), mayRename = _ => false)
          case _ =>
            NopApplier
        }
      }))
  }

  /**
   * A rewrite rule that recognizes dot product patterns and rewrites them as specialized calls.
   * @return A rewrite rule.
   */
  def ddotRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val a = ExprVar(ArrayType(t(), n))
    val b = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FindDDot",
      Pattern({
        Ifold(
          DeBruijnLambda(i32,
            DeBruijnLambda(t(),
              BinaryOp(
                BinaryOp(
                  Idx(a, DeBruijnParamUse(1, i32), EagerType()),
                  Idx(b, DeBruijnParamUse(1, i32), EagerType()),
                  AlgoOp.Mul),
                DeBruijnParamUse(0, t()), AlgoOp.Add))),
          Constant(0.0, t()),
          n)
      }),
      GeneratedApplier((m, _) => {
        val shiftedA = tryExtract(m.instantiate(a), 2)
        val shiftedB = tryExtract(m.instantiate(b), 2)
        (shiftedA, shiftedB) match {
          case (Some(a), Some(b)) =>
            Pattern(Extern.DoubleDotProd(a, b), mayRename = _ => false)
          case _ =>
            NopApplier
        }
      }))
  }
}
