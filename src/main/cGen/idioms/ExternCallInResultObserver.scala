package cGen.idioms

import cGen.ExternFunctionCall
import core.Expr
import eqsat.{EClassT, EGraph, EGraphStepObserver, ENodeT, Extractor}

import scala.collection.mutable

/**
 * An observer that, at every saturation step, counts the number of external calls in an e-graph's optimal result.
 */
class ExternCallInResultObserver(var root: Option[EClassT], extractor: Extractor) extends EGraphStepObserver {
  private val externCallCounts = new mutable.ArrayBuffer[Map[String, Int]]()

  /**
   * Returns the number of external calls over time.
   * @return A sequence of maps, each of which maps external function names to the number of times they were called
   *         at a particular step in the saturation process.
   */
  def externCallsOverTime: Seq[Map[String, Int]] = externCallCounts

  /**
   * Notes that the graph is about to perform a step in the Equality Saturation algorithm.
   * @param graph The graph that is about to take a step
   */
  override def onStepOrFinish[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass]): Unit = {
    externCallCounts.append(currentCount)
  }

  private def currentCount: Map[String, Int] = ExternCallInResultObserver.countExternCalls(root.get, extractor)
}

object ExternCallInResultObserver {
  def countExternCalls(expression: Expr): Map[String, Int] = {
    val counts = mutable.HashMap[String, Int]()
    expression.visit({
      case ExternFunctionCall(name, _, _, _) =>
        counts(name) = counts.getOrElse(name, 0) + 1

      case _ =>
    })
    counts.toMap
  }

  def countExternCalls(root: EClassT, extractor: Extractor): Map[String, Int] = {
    countExternCalls(extractor(root).get)
  }
}
