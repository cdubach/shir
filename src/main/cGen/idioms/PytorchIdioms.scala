package cGen.idioms

import cGen.{ArrayType, ArrayTypeT, ArrayTypeVar, BoolType, CGenDataTypeT, CGenDataTypeVar, CScalarTypeVar, Constant, Extern, ExternFunctionCall, IntType, IntTypeVar, Lib}
import core.{ArithTypeT, Expr}

object PytorchIdioms {
  val TENSOR = "torch.tensor"
  val STACK = "torch.stack"

  val SUM = "torch.sum"
  val DOT = "torch.dot"
  val MAT_VEC = "torch.mv"
  val MAT_MUL = "torch.mm"
  val ADD = "torch.add"
  val MUL = "torch.mul"
  val TRANSPOSE = "torch.transpose"
  val FULL = "torch.full"
  val UNFOLD = "torch.unfold$"

  def dot(a: Expr, b: Expr): Expr = {
    val arrayType = Lib.inferType(a).asInstanceOf[ArrayTypeT]
    val length = arrayType.len
    val dataType = arrayType.et
    ExternFunctionCall(
      DOT,
      Seq(a, b).reverse,
      Seq(
        ArrayTypeVar(dataType, length),
        ArrayTypeVar(dataType, length)).reverse,
      dataType)
  }

  def reduceSum(a: Expr): Expr = {
    val arrayType = Lib.inferType(a).asInstanceOf[ArrayTypeT]
    val length = arrayType.len
    val dataType = arrayType.et
    val one = Lib.one(() => IntType(32))
    ExternFunctionCall(
      SUM,
      Seq(a).reverse,
      Seq(ArrayTypeVar(dataType, length)).reverse,
      dataType)
  }

  def matvec(a: Expr, b: Expr): Expr = {
    val aType = Lib.inferType(a).asInstanceOf[ArrayTypeT]
    val n = aType.len
    val m = aType.et.asInstanceOf[ArrayTypeT].len
    val dataType = aType.et.asInstanceOf[ArrayTypeT].et

    ExternFunctionCall(
      MAT_VEC,
      Seq(a, b).reverse,
      Seq(
        ArrayTypeVar(aType.et, n),
        ArrayTypeVar(dataType, m)).reverse,
      ArrayType(dataType, n))
  }

  def matmul(a: Expr, b: Expr): Expr = {
    val aType = Lib.inferType(a)
    val bType = Lib.inferType(b)
    val dataType = Lib.elementType(aType)
    val n = Lib.rows(aType)
    val m = Lib.cols(aType)
    val k = Lib.rows(bType)
    val l = Lib.cols(bType)

    ExternFunctionCall(
      MAT_MUL,
      Seq(a, b).reverse,
      Seq(
        ArrayTypeVar(ArrayType(dataType, m), n),
        ArrayTypeVar(ArrayType(dataType, l), k)).reverse,
      ArrayType(ArrayType(dataType, l), n))
  }

  def add(a: Expr, b: Expr): Expr = {
    val aType = Lib.inferType(a).asInstanceOf[ArrayTypeT]

    ExternFunctionCall(
      ADD,
      Seq(a, b).reverse,
      Seq(
        ArrayTypeVar(aType.et, aType.len),
        ArrayTypeVar(aType.et, aType.len)).reverse,
      aType)
  }

  def mul(a: Expr, b: Expr): Expr = {
    val bType = Lib.inferType(b).asInstanceOf[ArrayTypeT]

    ExternFunctionCall(
      MUL,
      Seq(a, b).reverse,
      Seq(
        CGenDataTypeVar(),
        ArrayTypeVar(bType.et, bType.len)).reverse,
      bType)
  }

  def fill(value: Expr, len: ArithTypeT): Expr = {
    val t = Lib.inferType(value).asInstanceOf[CGenDataTypeT]
    val i32 = IntType(32)
    ExternFunctionCall(
      FULL,
      Seq(TypeArith(len, i32), value).reverse,
      Seq(IntTypeVar(32), CGenDataTypeVar()).reverse,
      ArrayType(t, len))
  }

  def transpose(a: Expr): Expr = {
    val aType = Lib.inferType(a).asInstanceOf[ArrayTypeT]
    val n = aType.len
    val t = aType.et.asInstanceOf[ArrayTypeT].et
    val m = aType.et.asInstanceOf[ArrayTypeT].len
    val i32 = () => IntType(32)

    ExternFunctionCall(
      TRANSPOSE,
      Seq(a, Lib.zero(i32), Lib.one(i32)).reverse,
      Seq(ArrayTypeVar(ArrayType(t, m), n), IntTypeVar(32), IntTypeVar(32)).reverse,
      ArrayType(ArrayType(t, n), m))
  }

  def slidingWindow(vec: Expr, windowWidth: ArithTypeT): Expr = {
    val vecType = Lib.inferType(vec).asInstanceOf[ArrayTypeT]
    val t = vecType.et
    val n = vecType.len
    val numberOfWindows = n.ae - windowWidth.ae + 1
    val i32 = IntType(32)

    ExternFunctionCall(
      UNFOLD,
      Seq(vec, Constant(0, i32), TypeArith(windowWidth, i32), Constant(1, i32)).reverse,
      Seq(ArrayTypeVar(t, n), IntTypeVar(32), IntTypeVar(32), IntTypeVar(32)).reverse,
      ArrayType(ArrayType(t, windowWidth), numberOfWindows))
  }
}
