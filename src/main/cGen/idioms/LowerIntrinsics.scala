package cGen.idioms

import cGen.PatternMatchingRewriter.{ParamReifier, RewriteRule, TypeReifier}
import cGen._
import core.{ArithTypeVar, Expr, ParamDef}

/**
  * A pass that lowers intrinsics to external calls.
  */
object LowerIntrinsics {
  def apply(expr: Expr): Expr = {
    rewriter.apply(expr)
  }

  private val matrixMulDefs: Seq[RewriteRule] = for (leftTransposed <- Seq(true, false); rightTransposed <- Seq(true, false))
    yield {
      val scalarType = DoubleType()
      val n = ArithTypeVar()
      val m = ArithTypeVar()
      val k = ArithTypeVar()
      val l = ArithTypeVar()
      val lhs = ParamDef(ArrayType(ArrayType(scalarType, n), m))
      val rhs = ParamDef(ArrayType(ArrayType(scalarType, k), l))
      (
        "DoubleMatrixMul",
        Intrinsics.MatrixMul(lhs, rhs, leftTransposed = leftTransposed, rightTransposed = rightTransposed),
        (reifyParam: ParamReifier, _: TypeReifier) => Extern.MatrixMul(
          reifyParam(lhs),
          reifyParam(rhs),
          leftTransposed = leftTransposed,
          rightTransposed = rightTransposed)
      )
    }

  /**
    * A set of rewrite rules that rewrite general-purpose computation patterns as specialized external calls.
    */
  private val instructionDefs: Seq[RewriteRule] = Seq(
    {
      // Matrix transposition in double precision.
      val scalarType = DoubleType()
      val n = ArithTypeVar()
      val m = ArithTypeVar()
      val matrix = ParamDef(ArrayType(ArrayType(scalarType, m), n))
      (
        "DoubleMatrixTranspose",
        Intrinsics.MatrixTranspose(matrix),
        (reifyParam, _) =>
          Extern.MatrixTranspose(reifyParam(matrix), "matrix_transpose")
      )
    },
    {
      // Dot products in double precision.
      val scalarType = DoubleType()
      val size = ArithTypeVar()
      val lhs = ParamDef(ArrayType(scalarType, size))
      val rhs = ParamDef(ArrayType(scalarType, size))
      (
        "DoubleDotProduct",
        Intrinsics.DotProd(lhs, rhs),
        (reifyParam, _) => Extern.DotProd(reifyParam(lhs), reifyParam(rhs), "idiom_ddot")(() => scalarType)
      )
    }
  )

  /**
    * The full set of rewrite rules used by the intrinsics lowering pass.
    */
  val rewriteRules: Seq[RewriteRule] = instructionDefs ++ matrixMulDefs

  private val rewriter = new PatternMatchingRewriter(
    rewriteRules,
    order = PatternMatchingRewriter.RewriteOrder.BottomUp,
    preprocessRules = false)
}
