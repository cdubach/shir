package cGen.idioms

import cGen.primitive.Idx
import cGen._
import core.{Expr, ParamDef, ParamUse}

/**
  * Prepares expressions in the idiom-oriented dialect for Pytorch code generation.
  */
object PrepareForPytorch {
  def apply(expr: Expr): Expr = {
    Tensorify(EliminateUselessLambdas(expr), listToTensor)
  }

  def isTensor(expr: Expr): Boolean = {
    expr match {
      case Build(CLambda(_, body, _), _, _, _) => isTensor(body)
      case CFC(CLambda(_, body, _), _, _) => isTensor(body)
      case Idx(array, _, _, _) => isTensor(array)
      case BinaryOp(lhs, rhs, _, _, _) => isTensor(lhs) || isTensor(rhs)
      case UnaryOp(arg, _, _) => isTensor(arg)
      case Ifold(CLambda(_, CLambda(_, body, _), _), init, _, _) => isTensor(body) || isTensor(init)
      case ParamUse(ParamDef(ArrayType(_, _), _)) => true
      case ExternFunctionCall(_, _, _, _) => true
      case _ => false
    }
  }

  def isListOfTensors(expr: Expr): Boolean = {
    expr.t match {
      case ArrayType(ArrayType(_, _), _) => true
      case _ => isTensor(expr)
    }
  }

  private def listToTensor(expr: Expr): Expr = expr.t match {
    case at@ArrayType(et, n) =>
      if (isListOfTensors(expr)) {
        ExternFunctionCall(PytorchIdioms.STACK, Seq(expr), Seq(ArrayTypeVar(et, n)), at)
      } else {
        ExternFunctionCall(PytorchIdioms.TENSOR, Seq(expr), Seq(ArrayTypeVar(et, n)), at)
      }

    case _ => ???
  }
}
