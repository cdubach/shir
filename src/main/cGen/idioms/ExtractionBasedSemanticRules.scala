package cGen.idioms

import cGen.{AlgoOp, BinaryOp, Build, CGenDataTypeVar, Constant, DoubleType, EagerType, IntType, IntTypeVar, Tuple, ViewTypeVar}
import eqsat.{AugmentedSearcher, ConditionalSearcher, DeBruijnForall, DeBruijnLambda, DeBruijnParamUse, DeBruijnShift, ExtractionBasedInliner, Extractor, GeneratedApplier, NopApplier, Pattern, Rewrite, Searcher, TypeSearcher}
import cGen.primitive.{Fst, Idx, Snd}
import core.{AnyTypeVar, ArithType, ArithTypeVar, ExprVar, FunTypeT, FunTypeVar, FunctionCall, TypeChecker}

/**
  * General-purpose rewrite rules for expressions based on build/ifold.
  */
case class ExtractionBasedSemanticRules(extractor: Extractor) {
  /**
    * The full set of rewrite rules for expressions based on build/ifold.
    * @return A set of rewrite rules.
    */
  def rules: Seq[Rewrite] = inliningRule +: (simplificationRules ++ expansionRules)

  /**
    * The inlining rule, which takes calls to lambdas and performs beta reduction.
    * @return A rule that encodes beta reduction.
    */
  def inliningRule: Rewrite = {
    // (\x. e) y -> e[y/x]
    val t = AnyTypeVar()
    val body = ExprVar(AnyTypeVar())
    val arg = ExprVar(t)
    Rewrite(
      "InlineLambda",
      Pattern(FunctionCall(DeBruijnLambda(t, body), arg)),
      ExtractionBasedInliner(inlineMaximally = true, extractor))
  }

  /**
    * A set of rules that simplify expressions. Running these rules on any e-graph should result in a fixpoint.
    * @return A set of rules.
    */
  def simplificationRules: Seq[Rewrite] = {
    Seq(
      // ====
      // Axioms, left to right.
      // (build f N)[i] -> f i
      {
        val f = ExprVar(AnyTypeVar())
        val i = ExprVar(IntTypeVar())
        Rewrite("EliminateBuild", Idx(Build(f, ArithTypeVar()), i, ViewTypeVar()), FunctionCall(f, i))
      },

      //      // (build f 1)[i] -> f 0
      //      {
      //        val f = ExprVar(AnyTypeVar())
      //        val intType = IntTypeVar()
      //        val i = ExprVar(intType)
      //        val zero = Constant(0, intType)
      //        Rewrite("EliminateBuildOfSize1", Idx(Build(f, 1, ViewTypeVar()), i, ViewTypeVar()), FunctionCall(f, zero))
      //      },

      // (a, b)._1 -> a
      {
        val a = ExprVar(CGenDataTypeVar())
        val b = ExprVar(CGenDataTypeVar())
        Rewrite("EliminateTuple1", Fst(Tuple(a, b), ViewTypeVar()), a)
      },

      // (a, b)._2 -> b
      {
        val a = ExprVar(CGenDataTypeVar())
        val b = ExprVar(CGenDataTypeVar())
        Rewrite("EliminateTuple2", Snd(Tuple(a, b), ViewTypeVar()), b)
      },

      // shift(x) -> shifted x
      {
        val x = ExprVar()
        Rewrite("ExpandShift", Pattern(DeBruijnShift(x)), GeneratedApplier((matchInfo, _) => {
          extractor(matchInfo.where) match {
            case Some(DeBruijnShift(expr, _)) => Pattern(DeBruijnParamUse.incrementUnbound(expr))
            case _ => NopApplier
          }
        }))
      },
      // ====

      //      // ====
      //      // Equality definitions, structural equality
      //      // xs[x_0] = ys[x_0] => xs = ys
      //      {
      //        Rewrite(
      //          "StructuralArrayEquality",
      //          StructuralArrayEquality.pattern,
      //          StructuralArrayEquality)
      //      },
      //
      //      // t._1 = u._1 and t._2 = u._2 => t = u
      //      {
      //        val t = ExprVar(TupleTypeVar(CGenDataTypeVar(), CGenDataTypeVar()))
      //        Rewrite(
      //          "StructuralTupleEquality",
      //          Pattern(Fst(t, ViewTypeVar())),
      //          StructuralTupleEquality)
      //      },
      //
      //      // f x_0 = g x_0 => f = g
      //      {
      //        Rewrite(
      //          "StructuralFunctionEquality",
      //          StructuralFunctionEquality.pattern,
      //          StructuralFunctionEquality)
      //      },
      //      // ====

      // ====
      // Arithmetic axioms
      // x + 0 -> x
      {
        val t = DoubleType()
        val exprVar = ExprVar(t)
        Rewrite("SimplifyAddZeroRight", BinaryOp(exprVar, Constant(0.0, t), AlgoOp.Add), exprVar)
      },

      // x * 1 -> x
      {
        val t = DoubleType()
        val exprVar = ExprVar(t)
        Rewrite("SimplifyMulOneRight", BinaryOp(Constant(1, t), exprVar, AlgoOp.Mul), exprVar)
      },

      // 1 * x -> x
      {
        val t = IntTypeVar()
        val exprVar = ExprVar(t)
        Rewrite("SimplifyMulOneLeft", BinaryOp(exprVar, Constant(1, t), AlgoOp.Mul), exprVar)
      }
      // ====
    )
  }

  /**
    * A set of rules that expand expressions. Applying these rules to an e-graph may not always result in a fixpoint.
    * @return A set of rules.
    */
  def expansionRules: Seq[Rewrite] = {
    def doNotMatchSpecial(searcher: Searcher) = {
      ConditionalSearcher(searcher, matchInfo => {
        !matchInfo.where.nodes.map(_.expr).exists({
          case _: DeBruijnForall => true
          case _ => false
        }) && !matchInfo.where.nodes.map(_.expr).forall({
          case DeBruijnShift(_) => true
          case _ => false
        })
      })
    }

    def doNotMatchFunctions(searcher: Searcher) = {
      ConditionalSearcher(searcher, matchInfo => !matchInfo.where.t.isInstanceOf[FunTypeT])
    }

    Seq(
      // ====
      // Rules derived from axioms, right to left.
      // x -> (\ shift(x)) y
      {
        val t = CGenDataTypeVar()
        val i32 = IntType(32)
        val x = ExprVar(t)
        val y = ExprVar(i32)
        Rewrite(
          "IntroduceConstLambda",
          AugmentedSearcher(doNotMatchFunctions(doNotMatchSpecial(Pattern(x))), doNotMatchSpecial(Pattern(y))),
//          GeneratedApplier((matchInfo, _) => {
//            Pattern(
//              FunctionCall(
//                DeBruijnLambda(i32, DeBruijnParamUse.incrementUnbound(matchInfo.instantiate(x).toSmallestExpr)),
//                y))
//          })
          Pattern(FunctionCall(DeBruijnLambda(i32, DeBruijnShift(x)), y))
        )
      },

      // f i -> (build f N)[i]
      {
        val f = ExprVar(FunTypeVar(CGenDataTypeVar(), CGenDataTypeVar()))
        val i = ExprVar(IntTypeVar())
        val N = ArithTypeVar()
        Rewrite(
          "IntroduceBuild",
          ConditionalSearcher(
            TypeSearcher(Pattern(FunctionCall(f, i)), N),
            solution => solution.instantiate(N) match {
              case ArithType(value) if value.isEvaluable => value.evalLong > 0
              case _ => false
            }),
          Pattern(Idx(Build(f, N), i, EagerType())))
      },
      // ====

      //      // ====
      //      // Equality definitions, quantifier introduction
      //      // xs -> xs, create forall i. shift(xs)[i],
      //      // if xs is not a shift
      //      {
      //        val t = CGenDataTypeVar()
      //        val xs = ExprVar(ArrayTypeVar(t))
      //        Rewrite(
      //          "IntroduceArrayForall",
      //          doNotMatchSpecial(Pattern(xs)),
      //          AdditivePattern({
      //            val indexType = IntType(32)
      //            DeBruijnForall(indexType, Idx(DeBruijnShift(xs), DeBruijnParamUse(0, indexType), EagerType()))
      //          }))
      //      },
      //
      //      // t -> t, create t._1
      //      {
      //        val t = ExprVar(TupleTypeVar(CGenDataTypeVar(), CGenDataTypeVar()))
      //        Rewrite(
      //          "IntroduceTupleFst",
      //          Pattern(t),
      //          AdditivePattern(Fst(t, EagerType())))
      //      },
      //
      //      // t -> t, create t._2
      //      {
      //        val t = ExprVar(TupleTypeVar(CGenDataTypeVar(), CGenDataTypeVar()))
      //        Rewrite(
      //          "IntroduceTupleSnd",
      //          Pattern(t),
      //          AdditivePattern(Snd(t, EagerType())))
      //      },
      //
      //      // f -> f, create forall x. shift(f) x,
      //      // if f is a function and f is not a forall quantifier or a shift
      //      {
      //        val paramType = AnyTypeVar()
      //        val f = ExprVar(FunTypeVar(paramType))
      //        Rewrite(
      //          "IntroduceFunctionForall",
      //          doNotMatchSpecial(Pattern(f)),
      //          AdditivePattern(DeBruijnForall(paramType, FunctionCall(DeBruijnShift(f), DeBruijnParamUse(0, paramType)))))
      //      },
      //
      //      // ====

      // ====
      // Arithmetic axioms
      // x -> x + 0
      {
        val t = DoubleType()
        val exprVar = ExprVar(t)
        Rewrite("IntroduceAddZero", exprVar, BinaryOp(exprVar, Constant(0.0, t), AlgoOp.Add))
      },

      // x -> 1 * x
      {
        val t = DoubleType()
        val exprVar = ExprVar(t)
        Rewrite("IntroduceMulOneLeft", exprVar, BinaryOp(Constant(1, t), exprVar, AlgoOp.Mul))
      },

      // x -> x * 1
      {
        val t = IntTypeVar()
        val exprVar = ExprVar(t)
        Rewrite("IntroduceMulOneRight", exprVar, BinaryOp(exprVar, Constant(1, t), AlgoOp.Mul))
      },

      // x * y -> y * x
      {
        val t = CGenDataTypeVar()
        val x = ExprVar(t)
        val y = ExprVar(t)
        Rewrite("MulCommutativity", BinaryOp(x, y, AlgoOp.Mul), BinaryOp(y, x, AlgoOp.Mul))
      },

      // x * (y * z) -> (x * y) * z
      {
        val t = CGenDataTypeVar()
        val x = ExprVar(t)
        val y = ExprVar(t)
        val z = ExprVar(t)
        Rewrite(
          "MulAssociativity1",
          BinaryOp(x, TypeChecker.check(BinaryOp(y, z, AlgoOp.Mul)), AlgoOp.Mul),
          BinaryOp(TypeChecker.check(BinaryOp(x, y, AlgoOp.Mul)), z, AlgoOp.Mul))
      },

      // x * (y * z) -> (x * y) * z
      {
        val t = CGenDataTypeVar()
        val x = ExprVar(t)
        val y = ExprVar(t)
        val z = ExprVar(t)
        Rewrite(
          "MulAssociativity2",
          BinaryOp(TypeChecker.check(BinaryOp(x, y, AlgoOp.Mul)), z, AlgoOp.Mul),
          BinaryOp(x, TypeChecker.check(BinaryOp(y, z, AlgoOp.Mul)), AlgoOp.Mul))
      }
      // ====
    )
  }
}
