package cGen.idioms

import eqsat.HierarchicalStopwatch

object Blas {
  def BuiltinIfold[A](n: Int, init: A, f: Int => A => A): A =
    Array.range(0, n).foldLeft(init)((acc, i) => f(i)(acc))

  def idiom_ddot(stopwatch: HierarchicalStopwatch, n: Int, a: Array[Double], offset1: Int, b: Array[Double], offset2: Int): Double = {
    stopwatch.child("idiom_ddot", {
      BuiltinIfold(n, 0.0, (j: Int) => (acc: Double) => a(j) * b(j) + acc)
    })
  }

  def idiom_dgemv(stopwatch: HierarchicalStopwatch, n: Int, m: Int, alpha: Double, a: Array[Array[Double]], b: Array[Double], beta: Double, c: Array[Double]): Array[Double] = {
    stopwatch.child("idiom_dgemv", {
      Array.range(0, n).map(i => {
        BuiltinIfold(m, beta * c(i), (j: Int) => (acc: Double) => alpha * a(i)(j) * b(j) + acc)
      })
    })
  }

  def idiom_dgemm(stopwatch: HierarchicalStopwatch, _u1: Int, _u2: Int, n: Int, m: Int, k: Int, alpha: Double, a: Array[Array[Double]], b: Array[Array[Double]], beta: Double, c: Array[Array[Double]]): Array[Array[Double]] = {
    stopwatch.child("idiom_dgemm", {
      Array.range(0, n).map(i => {
        Array.range(0, k).map(j => {
          BuiltinIfold(m, beta * c(i)(j), (l: Int) => (acc: Double) => alpha * a(i)(l) * b(l)(j) + acc)
        })
      })
    })
  }
}
