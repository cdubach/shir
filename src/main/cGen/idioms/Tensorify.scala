package cGen.idioms

import cGen.{ArrayType, ArrayTypeVar, Build, CGenExpr, ExternFunctionCall}
import core.{Expr, TypeChecker}

object Tensorify {
  private val BUILTIN_TO_TENSOR = "builtin.to_tensor"

  def apply(expr: Expr, listToTensor: Expr => Expr): Expr = {
    val checkedExpr = TypeChecker.check(expr)
    val tensorified = TypeChecker.check(tensorify(checkedExpr))
    TypeChecker.check(lowerBuiltins(tensorified, listToTensor))
  }

  private def lowerBuiltins(expr: Expr, listToTensor: Expr => Expr): Expr = {
    expr.visitAndRebuild({
      case ExternFunctionCall(BUILTIN_TO_TENSOR, Seq(arg), _, _) => listToTensor(arg)
      case other => other
    }).asInstanceOf[Expr]
  }

  private def tensorify(expr: Expr): Expr = {
    expr match {
      case Build(fun, n, vt, t@ArrayType(et, _)) =>
        ExternFunctionCall(
          BUILTIN_TO_TENSOR,
          Seq(Build(tensorify(fun), n, vt)),
          Seq(ArrayTypeVar(et, n)),
          t)

      case other => tensorifyArgs(other)
    }
  }

  private def tensorifyArgs(expr: Expr): Expr = {
    expr match {
      // Tensorification takes list arguments.
      case ExternFunctionCall(name@BUILTIN_TO_TENSOR, args, intv, outt) =>
        ExternFunctionCall(name, args.map(tensorifyArgs), intv, outt)

      case cGenExpr: CGenExpr => cGenExpr.buildFromArgs(cGenExpr.args.map(tensorify))

      case other => other.map({
        case arg: Expr => tensorify(arg)
        case other => other
      }).asInstanceOf[Expr]
    }
  }
}
