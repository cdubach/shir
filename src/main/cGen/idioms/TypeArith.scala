package cGen.idioms

import cGen.{CGenExpr, Constant}
import core.{ArithType, ArithTypeT, Expr, Formatter, TextFormatter, Type, Value}

/**
 * An expression that evaluates an arithmetic type.
 */
case class TypeArithExpr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): CGenExpr = {
    TypeArith(newTypes.head.asInstanceOf[ArithTypeT], newTypes(1))
  }

  override protected def argsBuild(args: Seq[Expr]): CGenExpr = TypeArith(types.head.asInstanceOf[ArithTypeT], types(1))

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case TypeArith(value, _) =>
            Some(formatter.formatAtomic(value))
        }

      case _ => super.tryFormat(formatter)
    }
  }

  /**
   * Simplifies this `TypeArithExpr` by rewriting it as a `ConstantExpr`, if possible.
   * @return A `ConstantExpr` equivalent to this `TypeArithExpr`, if one can be found; otherwise, returns `this`.
   */
  def simplified: CGenExpr = this match {
    case TypeArith(ArithType(ae), _) if ae.isEvaluable => Constant(ae.evalDouble, this.t)
    case _ => this
  }
}

/**
 * An expression that evaluates an arithmetic type.
 */
object TypeArith {
  /**
   * Creates an expression that evaluates an arithmetic type.
   * @param value The arithmetic type that is the value of the expression.
   * @param valueType The expression's type.
   * @return A `ArithTypeExpr`.
   */
  def apply(value: ArithTypeT, valueType: Type): TypeArithExpr = {
    TypeArithExpr(Value(valueType), Seq(value, valueType))
  }

  def unapply(expr: TypeArithExpr): Some[(ArithTypeT, Type)] = Some(
    expr.types.head.asInstanceOf[ArithTypeT],
    expr.types(1))
}
