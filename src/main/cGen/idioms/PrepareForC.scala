package cGen.idioms

import eqsat.DeBruijnLambda
import cGen.primitive.{Id, Idx, IdxS}
import cGen._
import core._

/**
  * Prepares expressions in the idiom-oriented dialect for C code generation.
  */
object PrepareForC {
  def apply(expr: Expr): Expr = {
    val exprWithNamedParams = TypeChecker.check(DeBruijnLambda.toNamedRec(expr).asInstanceOf[Expr])
    val massaged = exprWithNamedParams.visitAndRebuild({
      case Idx(array, index, EagerType(), _) => IdxS(array, index)
      case FunctionCall(CLambda(p, body, _), arg, _) => CGenLet(p, body, arg)
      case FunctionCall(innerCall@CFC(_, _, _), arg, _) => CFC(innerCall, arg)
      case FunctionCall(param@ParamUse(_), arg, _) => CFC(param, arg)
      case Build(f, len, _, _) => Build(f, len, EagerType())
      case If(cond, lhs, rhs, _) => If(cond, lhs, rhs, SrcViewType())
      case other => other
    }).asInstanceOf[Expr]

    val functionCallsAsLetBindings = massaged.visitAndRebuild({
      case CFC(CFC(CLambda(p2, CLambda(p1, body, _), _), arg2, _), arg1, _) =>
        CGenLet(p1, CGenLet(p2, body, arg2), arg1)
      case other => other
    }).asInstanceOf[Expr]

    fixViewTypes(TypeChecker.check(functionCallsAsLetBindings))
  }

  /**
    * Recursively changes an expression's arguments to conform to their expected view types.
    * @param expr The expression whose arugment view types need to be fixed.
    * @return A modified version of the expression.
    */
  private def fixViewTypes(expr: Expr): Expr = {
    expr match {
      case ParamUse(_) | ParamDef(_, _) => expr
      case CLambda(p, body@CLambda(_, _, _), _) => CLambda(p, fixViewTypes(body))
      case CLambda(p, body, _) => CLambda(p, fixViewTypes(changeViewType(body, DestViewType())))

      case build@Build(CLambda(p, body, _), n, _, _) if build.isDestView || build.isEager =>
        Build(CLambda(p, fixViewTypes(changeViewType(body, DestViewType()))), n, build.rightmostViewType)

      case map@Map(CLambda(p, body, _), array, _, _) if map.isDestView || map.isEager =>
        Map(CLambda(p, fixViewTypes(changeViewType(body, DestViewType()))), fixViewTypes(array), map.rightmostViewType)

      case Ifold(CLambda(index, CLambda(acc, body, _), _), init, n, _) =>
        Ifold(
          CLambda(index, CLambda(acc, fixViewTypes(changeViewType(body, DestViewType())))),
          fixViewTypes(changeViewType(init, EagerType())),
          n)

      case CGenLet(p, body, value, _) =>
        CGenLet(p, fixViewTypes(body), fixViewTypes(changeViewType(value, DestViewType())))

      case CFC(CLambda(p, body, _), arg, _) =>
        CGenLet(p, fixViewTypes(body), fixViewTypes(changeViewType(arg, DestViewType())))

      case CFC(f, arg, _) =>
        CFC(fixViewTypes(f), fixViewTypes(changeViewType(arg, DestViewType())))

      case expr: CGenExpr =>
        expr.buildFromArgs(expr.args.zip(expr.paramViewTypes).map(t => fixViewTypes(changeViewType(t._1, t._2))))

      case _ => ???
    }
  }

  /**
    * Tweaks an expression so its view type is a subtype of a desired view type.
    * @param expr The expression to tweak.
    * @param viewType The desired view type for the expression.
    * @return A modified version of the expression.
    */
  private def changeViewType(expr: Expr, viewType: ViewTypeT): Expr = {
    (expr, viewType) match {
      // Eager and destination view types decay readily to source view types, so we don't need to change those.
      case (_, SrcViewType()) => expr

      // Eager expressions readily decay to destination view types, so we can leave these unchanged as well.
      case (expr: CGenExpr, DestViewType()) if expr.isEager => expr

      // Sometimes the view type will already be what we want.
      case (expr: CGenExpr, vt) if expr.rightmostViewType == vt => expr
      case (expr: CLambda, vt) if expr.t.rightmostViewType == vt => expr

      // Materializing a scalar is essentially a zero-cost operation, so whenever we reach a scalar value we can simply
      // insert an Id expression instead of digging deeper.
      case (_, EagerType() | DestViewType()) if expr.t.isInstanceOf[CScalarTypeT] => Id(expr)

      // View type changes can be pushed down through certain expressions, ideally reducing the amount of
      // work/materialization that needs to be performed.
      case (If(cond, branch1, branch2, _), DestViewType()) =>
        If(
          changeViewType(cond, DestViewType()),
          changeViewType(branch1, DestViewType()),
          changeViewType(branch2, DestViewType()),
          DestViewType())

      case (Idx(array, index, _, _), DestViewType()) =>
        Idx(changeViewType(array, DestViewType()), index, DestViewType())

      case (Tuple(first, second, _), DestViewType()) =>
        Tuple(changeViewType(first, DestViewType()), changeViewType(second, DestViewType()), DestViewType())

      case (Build(CLambda(p, body, _), n, _, _), EagerType() | DestViewType()) =>
        Build(CLambda(p, changeViewType(body, DestViewType())), n, EagerType())

      case (CGenLet(p, body, value, _), _) => CGenLet(p, changeViewType(body, viewType), value)

      case (CFC(CLambda(p, body, _), arg, _), _) => CGenLet(p, changeViewType(body, viewType), arg)

      // Materialization can turn any view type into an eager type.
      case (_, EagerType() | DestViewType()) => Materialize(expr)

      case _ => ???
    }
  }
}
