package cGen.idioms

import cGen.AlgoOp.AlgoOp
import cGen.{AlgoOp, ArrayType, BinaryOp, BoolType, Build, CFC, CGenExpr, CLambda, Constant, DoubleType, DoubleTypeT, ExternFunctionCall, FloatType, If, Ifold, IntType, IntTypeT, NamedTupleStructType, Tuple}
import cGen.primitive.{Fst, Idx, Snd}
import core.{AnyTypeVar, ArithType, Expr, FunctionCall, ParamDef, ParamUse, ShirIR, Type}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
 * Generates Python code from C code generator `Expr` instances.
 */
object ExprToPython {
  private def nameParam(param: ParamDef): String = s"p${param.id}"

  case class FunctionDefinition(name: String, params: Seq[ParamDef], body: Expr, annotations: Seq[String] = Seq()) {
    def hasAnnotation(annotation: String): Boolean = annotations.contains(annotation)

    def withAnnotation(annotation: String): FunctionDefinition = {
      FunctionDefinition(name, params, body, annotation +: annotations)
    }
  }

  def apply(expr: ShirIR, mustBeAtomic: Boolean = false): String = {
    def parenthesize(value: String): String = if (mustBeAtomic) s"($value)" else value

    def opToText(algoOp: AlgoOp): String = algoOp match {
      case AlgoOp.Add => "+"
      case AlgoOp.Sub => "-"
      case AlgoOp.Mul => "*"
      case AlgoOp.Div => "/"
      case AlgoOp.Mod => "%"
      case AlgoOp.Lt => "<"
    }

    expr match {
      case Build(CLambda(p, body, _), n, _, _) =>
        s"[${apply(body)} for ${nameParam(p)} in range(${apply(n)})]"
      case Ifold(idxf, init, n, _) =>
        s"ifold(${apply(n, mustBeAtomic = true)}, ${apply(init, mustBeAtomic = true)}, ${apply(idxf, mustBeAtomic = true)})"
      case Tuple(a, b, _) => s"(${apply(a, mustBeAtomic = true)}, ${apply(b, mustBeAtomic = true)})"
      case Fst(a, _, _) => parenthesize(s"${apply(a, mustBeAtomic = true)}[0]")
      case Snd(a, _, _) => parenthesize(s"${apply(a, mustBeAtomic = true)}[1]")
      case Constant(value, _: IntTypeT) => value.toLong.toString
      case Constant(value, _: DoubleTypeT) => value.toString
      case TypeArith(value, BoolType()) => if (value.ae.evalLong == 0) "False" else "True"
      case TypeArith(value, _) => apply(value, mustBeAtomic = mustBeAtomic)
      case Idx(array, index, _, _) => s"${apply(array, mustBeAtomic = true)}[${apply(index)}]"
      case ExternFunctionCall(name, args, _, _) =>
        if (name == PytorchIdioms.FULL) {
          // HACK: there are more appropriate ways to do this. Circle back and fix later
          s"$name((${apply(args.last)},), ${apply(args.head)})"
        } else if (name.endsWith("$")) {
          val funcName = name.split('.').last
          val croppedFuncName = funcName.substring(0, funcName.length - 1)
          s"${apply(args.last)}.$croppedFuncName(${args.reverse.tail.map(apply(_)).mkString(", ")})"
        } else {
          s"$name(${args.reverse.map(apply(_)).mkString(", ")})"
        }
      case FunctionCall(f, arg, _) => s"${apply(f, mustBeAtomic = true)}(${apply(arg, mustBeAtomic = true)})"

      case CLambda(p, body, _) => parenthesize(s"lambda ${nameParam(p)}: ${apply(body, mustBeAtomic = true)}")
      case ParamUse(pDef) => nameParam(pDef)

      case BinaryOp(left, right, AlgoOp.Div, _, _) if left.t.isInstanceOf[IntTypeT] || right.t.isInstanceOf[IntTypeT] =>
        parenthesize(s"${apply(left, mustBeAtomic = true)} // ${apply(right, mustBeAtomic = true)}")
      case BinaryOp(left, right, op, _, _) =>
        parenthesize(
          s"${apply(left, mustBeAtomic = true)} ${opToText(op)} ${apply(right, mustBeAtomic = true)}")

      case If(cond, ifTrue, ifFalse, _) =>
        parenthesize(
          s"${apply(ifTrue, mustBeAtomic)} if ${apply(cond, mustBeAtomic)} " +
            s"else ${apply(ifFalse, mustBeAtomic)}")

      case IntType(_, _) => "int"
      case DoubleType() => "float"
      case ArrayType(et, _) => s"List[${apply(et)}]"
      case NamedTupleStructType(_, fields) =>
        val parts = fields.map(_._2).map(apply(_)).mkString(", ")
        s"Tuple[$parts]"
      case ArithType(ae) => ae.evalLong.toString
    }
  }

  /**
   * Generates Python code for a function definition.
   * @param function The function definition to generate code for.
   * @return Python code that defines `function`.
   */
  def apply(function: FunctionDefinition): String = {
    val params = function.params.map(nameParam).mkString(", ")
    val annotations = function.annotations.map(a => s"@$a")
    val header = s"def ${function.name}($params):"
    val body = s"    return ${this(function.body)}"
    (annotations :+ header :+ body).mkString("\n")
  }

  /**
   * Takes a functional-style program expressed as a lambda or expression; turns that program into an equivalent
   * Python/TensorFlow program structured as uncurried function definitions.
   * @param expr The expression to turn into a Python program.
   * @return A Python program consisting on a number of function definitions.
   */
  def toTensorflowProgram(expr: Expr): String = {
    val imports = Seq(
      "import tensorflow as tf",
      "from time_function import time_function, print_timing_stats",
      "from utils import ifold")
    val kernelName = "kernel"
    val functions = toFunctionDefinitions(expr, kernelName).map(func =>
      if (func.hasAnnotation(TensorflowIdioms.FUNCTION) || func.name == kernelName)
        func.withAnnotation("time_function")
      else
        func)
    val kernel = functions.find(_.name == kernelName).get
    val kernelArgs = kernel.params.map(p => generateTensorflowRandom(p.t)).mkString(", ")
    val mainFunction = "def main():" +: Seq(s"$kernelName($kernelArgs)", "print_timing_stats()").map("    " + _)
    val mainThunk = "if __name__ == '__main__':\n    main()"
    (imports.mkString("\n") +: functions.map(apply) :+ mainFunction.mkString("\n") :+ mainThunk).mkString("\n\n")
  }

  /**
   * Takes a functional-style program expressed as a lambda or expression; turns that program into an equivalent
   * Python/TensorFlow program structured as uncurried function definitions.
   *
   * @param expr The expression to turn into a Python program.
   * @param hoistFunctions Instructs the code generator to lift lambdas into function definitions.
   * @return A Python program consisting on a number of function definitions.
   */
  def toPytorchProgram(expr: Expr, hoistFunctions: Boolean = false): String = {
    val imports = Seq(
      "import torch",
      "from utils import ifold, print_pytorch_profiler_results",
      "from torch.profiler import profile, record_function, ProfilerActivity")
    val kernelName = "kernel"
    val kernelNameStr = "\"" + kernelName + "\""
    val functions = toFunctionDefinitions(expr, kernelName, hoistFunctions)
    val kernel = functions.find(_.name == kernelName).get
    val kernelArgs = kernel.params.map(p => generatePytorchRandom(p.t)).mkString(", ")
    val mainBody = Seq(
      s"func = torch.compile($kernelName)",
      s"args = [$kernelArgs]",
      s"with profile(activities=[ProfilerActivity.CPU], record_shapes=True) as prof:",
      s"    with record_function($kernelNameStr):",
      s"        func(*args)",
      s"print_pytorch_profiler_results(prof)")
    val mainFunction = "def main():" +: mainBody.map("    " + _)
    val mainThunk = "if __name__ == '__main__':\n    main()"
    (imports.mkString("\n") +: functions.map(apply) :+ mainFunction.mkString("\n") :+ mainThunk).mkString("\n\n")
  }

  /**
   * Takes a functional-style program expressed as a lambda or expression; turns that program into an equivalent
   * pure Python program structured as uncurried function definitions.
   *
   * @param expr The expression to turn into a Python program.
   * @return A Python program consisting on a number of function definitions.
   */
  def toPythonProgram(expr: Expr, hoistFunctions: Boolean = false): String = {
    val imports = Seq(
      "import random",
      "from time_function import time_function, print_timing_stats",
      "from utils import ifold")
    val kernelName = "kernel"
    val functions = toFunctionDefinitions(expr, kernelName, hoistFunctions).map(func =>
      if (func.name == kernelName)
        func.withAnnotation("time_function")
      else
        func)
    val kernel = functions.find(_.name == kernelName).get
    val kernelArgs = kernel.params.map(p => generatePythonRandom(p.t)).mkString(", ")
    val mainFunction = "def main():" +: Seq(s"$kernelName($kernelArgs)", "print_timing_stats()").map("    " + _)
    val mainThunk = "if __name__ == '__main__':\n    main()"
    (imports.mkString("\n") +: functions.map(apply) :+ mainFunction.mkString("\n") :+ mainThunk).mkString("\n\n")
  }

  /**
   * Takes a functional-style program expressed as a lambda or expression; turns that program into an equivalent
   * sequence of uncurried function definitions.
   * @param expr The expression to turn into function definitions.
   * @param kernelName The name of the function definition that represents the total program.
   * @param hoistFunctions Instructs the code generator to lift lambdas into function definitions.
   * @return A sequence of function definitions.
   */
  def toFunctionDefinitions(expr: Expr, kernelName: String = "kernel", hoistFunctions: Boolean = true): Seq[FunctionDefinition] = {
    // Decompose `expr` into parameters and body.
    val (body, params) = decomposeFunction(expr)

    // Hoist called lambdas from the expression and uncurry them.
    val helperFunctions = mutable.ArrayBuffer[FunctionDefinition]()
    def hoistCalls(value: Expr): Expr = {
      value match {
        case expr: Expr =>
          tryExtractHoistableCall(expr) match {
            case Some(HoistableLambdaCall(body, params, args, annotations)) =>
              val refactoredBody = hoistCalls(body)
              val refactoredArgs = args.map(hoistCalls)
              val function = FunctionDefinition(
                s"helper${helperFunctions.length}",
                params.reverse,
                refactoredBody,
                annotations)
              helperFunctions.append(function)
              ExternFunctionCall(function.name, refactoredArgs.reverse, args.map(_ => AnyTypeVar()), body.t)

            case None =>
              value match {
                case cGenExpr: CGenExpr => cGenExpr.buildFromArgs(cGenExpr.args.map(hoistCalls))

                case other => other.map({
                  case arg: Expr => hoistCalls(arg)
                  case other => other
                }).asInstanceOf[Expr]
              }
          }

        case other => other
      }
    }

    val refactored = if (hoistFunctions) hoistCalls(body) else body
    helperFunctions :+ FunctionDefinition(kernelName, params, refactored)
  }

  private case class HoistableLambdaCall(body: Expr, params: Seq[ParamDef], args: Seq[Expr], annotations: Seq[String])

  private def tryExtractHoistableCall(expr: Expr): Option[HoistableLambdaCall] = {
    val (callee, args) = decomposeFunctionCall(expr)
    if (args.isEmpty) {
      None
    } else {
      val (innerCallee, annotations) = decomposeAnnotations(callee)
      val (calleeBody, params) = decomposeFunction(innerCallee)
      val bodyParams = new ArrayBuffer[ParamDef]()
      calleeBody.visit({
        case ParamUse(p) if !(bodyParams ++ params).exists(_.id == p.id) => bodyParams.append(p)
        case _ =>
      })
      if (params.length == args.length) {
        Some(HoistableLambdaCall(calleeBody, params ++ bodyParams, args ++ bodyParams.map(ParamUse), annotations))
      } else {
        None
      }
    }
  }

  private def decomposeFunctionCall(expr: Expr): (Expr, Seq[Expr]) = {
    expr match {
      case FunctionCall(callee, arg, _) =>
        val (innerCallee, innerArgs) = decomposeFunctionCall(callee)
        (innerCallee, arg +: innerArgs)

      case CFC(callee, arg, _) =>
        val (innerCallee, innerArgs) = decomposeFunctionCall(callee)
        (innerCallee, arg +: innerArgs)

      case _ => (expr, Seq())
    }
  }

  private def decomposeAnnotations(expr: Expr): (Expr, Seq[String]) = {
    expr match {
      case ExternFunctionCall(name, Seq(arg), _, _) =>
        val (inner, annotations) = decomposeAnnotations(arg)
        (inner, annotations :+ name)

      case _ => (expr, Seq())
    }
  }

  private def decomposeFunction(expr: Expr): (Expr, Seq[ParamDef]) = {
    expr match {
      case CLambda(param, body, _) =>
        val (innerBody, params) = decomposeFunction(body)
        (innerBody, param +: params)

      case _ => (expr, Seq())
    }
  }

  private def tensorShape(t: Type): (Seq[Int], Type) = t match {
    case ArrayType(et, len) =>
      val (innerDims, innerElement) = tensorShape(et)
      (len.ae.evalInt +: innerDims, innerElement)

    case _ => (Seq(), t)
  }

  private def nameTensorflowPrimitive(t: Type, prefix: String = "tf"): String = t match {
    case DoubleType() => s"$prefix.float64"
    case FloatType() => s"$prefix.float32"
    case IntType(width, _) => s"$prefix.int${width.ae.evalInt}"
    case _ => ???
  }

  private def generatePythonRandom(t: Type): String = t match {
    case DoubleType() | FloatType() => "random.random()"
    case IntType(width, _) => s"random.randint(0, ${(1 << width.ae.evalLong) - 1})"
    case ArrayType(et, len) => s"[${generatePythonRandom(et)} for _ in range(${len.ae.evalLong})]"
    case _ => ???
  }

  private def generateTensorflowRandom(t: Type): String = {
    val (dims, primitive) = tensorShape(t)
    val dimsString = dims.mkString(", ")
    s"tf.random.uniform(shape=[$dimsString], dtype=${nameTensorflowPrimitive(primitive)})"
  }

  private def generatePytorchRandom(t: Type): String = {
    val (dims, primitive) = tensorShape(t)
    val dimsString = if (dims.isEmpty) "()" else dims.mkString(", ")
    s"torch.randn($dimsString, dtype=${nameTensorflowPrimitive(primitive, "torch")})"
  }
}
