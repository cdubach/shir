package cGen.idioms

import cGen.{AlgoOp, ArrayType, BinaryOp, Build, CGenDataTypeVar, Constant, DoubleType, EagerType, Ifold, IntType}
import eqsat.{DeBruijnLambda, DeBruijnParamUse, ExtractionBasedShiftingApplier, Extractor, Pattern, Rewrite}
import cGen.primitive.Idx
import core.{ArithType, ArithTypeVar, ExprVar}

case class PytorchIdiomRules(extractor: Extractor) {
  private def oneWayRules: Seq[Rewrite] = Seq(
    reduceSumRule, dotRule, matvecRule, matmulRule, transposeRule,
    addVecRule, mulScalarAndVecRule, fillRule,
//    slidingWindowRule
  )

  private def oneWayOptRules: Seq[Rewrite] =
    Seq(liftMulToHigherDim, liftAddToHigherDim, transposeIdentity)

  /**
    * The set of idiom recognition rules.
    *
    * @return A set of idiom recognition rules.
    */
  def rules: Seq[Rewrite] = oneWayRules ++ oneWayRules.map(_.reverse) ++ oneWayOptRules ++ oneWayOptRules.map(_.reverse)

  /**
    * A rewrite rule that recognizes dot product patterns and rewrites them as specialized calls.
    * @return A rewrite rule.
    */
  def dotRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val a = ExprVar(ArrayType(t(), n))
    val b = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FindDot",
      Pattern({
        Ifold(
          DeBruijnLambda(i32,
            DeBruijnLambda(t(),
              BinaryOp(
                BinaryOp(
                  Idx(a, DeBruijnParamUse(1, i32), EagerType()),
                  Idx(b, DeBruijnParamUse(1, i32), EagerType()),
                  AlgoOp.Mul),
                DeBruijnParamUse(0, t()), AlgoOp.Add))),
          Constant(0.0, t()),
          n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.dot(a, b), mayRename = _ => false),
        extractor,
        Seq(a -> -2, b -> -2)))
  }

  /**
    * A rewrite rule that recognizes sum reduction patterns and rewrites them as specialized calls.
    * @return A rewrite rule.
    */
  def reduceSumRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val a = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FindReduceSum",
      Pattern({
        Ifold(
          DeBruijnLambda(i32,
            DeBruijnLambda(t(),
              BinaryOp(
                Idx(a, DeBruijnParamUse(1, i32), EagerType()),
                DeBruijnParamUse(0, t()), AlgoOp.Add))),
          Constant(0.0, t()),
          n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.reduceSum(a), mayRename = _ => false),
        extractor,
        Seq(a -> -2)))
  }

  /**
    * A rewrite rule that recognizes matvec patterns and rewrites them as tf.linalg.matvec calls.
    * @return A rewrite rule.
    */
  def matvecRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), m), n))
    val b = ExprVar(ArrayType(t(), m))
    Rewrite(
      "FindMatvec",
      Pattern(
        Build(
          DeBruijnLambda(i32,
            PytorchIdioms.dot(Idx(a, DeBruijnParamUse(0, i32), EagerType()), b)),
          n)),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.matvec(a, b), mayRename = _ => false),
        extractor,
        Seq(a -> -1, b -> -1)))
  }

  /**
    * A rewrite rule that recognizes matrix multiplication patterns and rewrites them as `tf.matmul` calls.
    * @return A rewrite rule.
    */
  def matmulRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val k = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(t(), m), n))
    val b = ExprVar(ArrayType(ArrayType(t(), m), k))
    Rewrite(
      "FindMatmul",
      Pattern({
        Build(
          DeBruijnLambda(i32,
            PytorchIdioms.matvec(
              b, // [k x [m x t]]
              Idx(a, DeBruijnParamUse(0, i32), EagerType()))), // [k x t]
          n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.matmul(a, PytorchIdioms.transpose(b)), mayRename = _ => false),
        extractor,
        Seq(a -> -1, b -> -1)))
  }

  /**
    * A rewrite rule that recognizes x + y patterns and rewrites them as `tf.add` calls.
    * @return A rewrite rule.
    */
  def addVecRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val x = ExprVar(ArrayType(t(), n))
    val y = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FindAddVec",
      Pattern({
        Build(
          DeBruijnLambda(i32,
            BinaryOp(
              Idx(x, DeBruijnParamUse(0, i32), EagerType()),
              Idx(y, DeBruijnParamUse(0, i32), EagerType()),
              AlgoOp.Add)),
          n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.add(x, y), mayRename = _ => false),
        extractor,
        Seq(x -> -1, y -> -1))
    )
  }

  /**
    * A rewrite rule that recognizes ax patterns and rewrites them as `tf.mul` calls.
    *
    * @return A rewrite rule.
    */
  def mulScalarAndVecRule: Rewrite = {
    implicit val t = DoubleType
    val i32 = IntType(32)
    val alpha = ExprVar(t())
    val n = ArithTypeVar()
    val x = ExprVar(ArrayType(t(), n))
    Rewrite(
      "FindMulScalarAndVec",
      Pattern({
        Build(
          DeBruijnLambda(i32,
            BinaryOp(alpha, Idx(x, DeBruijnParamUse(0, i32), EagerType()), AlgoOp.Mul)),
          n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.mul(alpha, x), mayRename = _ => false),
        extractor,
        Seq(alpha -> -1, x -> -1))
    )
  }

  /**
    * A rewrite rule that recognizes vector-filling patterns and rewrites them as `tf.fill` calls.
    *
    * @return A rewrite rule.
    */
  def fillRule: Rewrite = {
    val t = CGenDataTypeVar()
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val value = ExprVar(t)
    Rewrite(
      "FindFillVec",
      Pattern(Build(DeBruijnLambda(i32, value), n)),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.fill(value, n), mayRename = _ => false),
        extractor,
        Seq(value -> -1))
    )
  }

  /**
    * A rewrite rule that recognizes indexed matrix transpositions.
    *
    * @return A rewrite rule.
    */
  def indexedTransposeRule: Rewrite = {
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(DoubleType(), n), m))
    Rewrite(
      "FindIndexedTranspose",
      Pattern(
        Build(
          DeBruijnLambda(i32,
            Idx(Idx(a, DeBruijnParamUse(0, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType())),
          m)),
      ExtractionBasedShiftingApplier(
        Pattern(Idx(PytorchIdioms.transpose(a), DeBruijnParamUse(0, i32), EagerType()), mayRename = _ => false),
        extractor,
        Seq(a -> -1)))
  }

  /**
    * A rewrite rule that recognizes matrix transpositions.
    *
    * @return A rewrite rule.
    */
  def transposeRule: Rewrite = {
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val m = ArithTypeVar()
    val a = ExprVar(ArrayType(ArrayType(DoubleType(), n), m))
    Rewrite(
      "FindTranspose",
      Pattern(
        Build(
          DeBruijnLambda(i32,
            Build(
              DeBruijnLambda(i32,
                Idx(Idx(a, DeBruijnParamUse(0, i32), EagerType()), DeBruijnParamUse(1, i32), EagerType())),
              m)),
          n)),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.transpose(a), mayRename = _ => false),
        extractor,
        Seq(a -> -2)))
  }

  /**
    * A rewrite rule that recognizes sliding window operations.
    *
    * @return A rewrite rule.
    */
  def slidingWindowRule: Rewrite = {
    val i32 = IntType(32)
    val windowWidth = ArithTypeVar()
    val numberOfWindows = ArithTypeVar() // = dataLength.ae - windowWidth.ae + 1
    val dataLength = ArithType(numberOfWindows.ae + windowWidth.ae - 1)
    val data = ExprVar(ArrayType(CGenDataTypeVar(), dataLength))
    Rewrite(
      "FindSlidingWindow",
      Pattern(
        Build(
          DeBruijnLambda(i32,
            Build(
              DeBruijnLambda(i32,
                Idx(data, BinaryOp(DeBruijnParamUse(0, i32), DeBruijnParamUse(1, i32), AlgoOp.Add), EagerType())),
              windowWidth)),
          numberOfWindows)),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.slidingWindow(data, windowWidth), mayRename = _ => false),
        extractor,
        Seq(data -> -2)))
  }

  /**
    * An optimization rule that lifts a `torch.mul` call to a higher dimension.
    * @return A rewrite rule.
    */
  def liftMulToHigherDim: Rewrite = {
    val i32 = IntType(32)
    val alpha = ExprVar(CGenDataTypeVar())
    val n = ArithTypeVar()
    val x = ExprVar(ArrayType(ArrayType(CGenDataTypeVar(), ArithTypeVar()), n))
    Rewrite(
      "LiftMulToHigherDim",
      Pattern({
        Build(
          DeBruijnLambda(i32,
            PytorchIdioms.mul(alpha, Idx(x, DeBruijnParamUse(0, i32), EagerType()))),
          n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.mul(alpha, x), mayRename = _ => false),
        extractor,
        Seq(alpha -> -1, x -> -1))
    )
  }

  /**
    * An optimization rule that lifts a `torch.add` call to a higher dimension.
    *
    * @return A rewrite rule.
    */
  def liftAddToHigherDim: Rewrite = {
    val i32 = IntType(32)
    val n = ArithTypeVar()
    val t = ArrayType(CGenDataTypeVar(), ArithTypeVar())
    val x = ExprVar(ArrayType(t, n))
    val y = ExprVar(ArrayType(t, n))
    Rewrite(
      "LiftAddToHigherDim",
      Pattern({
        Build(
          DeBruijnLambda(i32,
            PytorchIdioms.add(
              Idx(x, DeBruijnParamUse(0, i32), EagerType()),
              Idx(y, DeBruijnParamUse(0, i32), EagerType()))),
          n)
      }),
      ExtractionBasedShiftingApplier(
        Pattern(PytorchIdioms.add(x, y), mayRename = _ => false),
        extractor,
        Seq(x -> -1, y -> -1))
    )
  }

  def transposeIdentity: Rewrite = {
    val matrix = ExprVar(ArrayType(ArrayType(CGenDataTypeVar(), ArithTypeVar()), ArithTypeVar()))
    Rewrite(
      "TransposeIdentity",
      PytorchIdioms.transpose(PytorchIdioms.transpose(matrix)),
      matrix)
  }

  def dotIdentity: Rewrite = {
    val elementType = CGenDataTypeVar()
    val n = ArithTypeVar()
    val vec1 = ExprVar(ArrayType(elementType, n))
    val vec2 = ExprVar(ArrayType(elementType, n))
    Rewrite(
      "DotCommutativity",
      PytorchIdioms.dot(vec1, vec2),
      PytorchIdioms.dot(vec2, vec1))
  }
}
