package cGen.idioms

import cGen.{ArrayType, ArrayTypeT, ArrayTypeVar, BoolType, CGenDataTypeT, CGenDataTypeVar, CScalarTypeVar, Extern, ExternFunctionCall, IntType, IntTypeVar, Lib}
import core.{ArithTypeT, Expr}

object TensorflowIdioms {
  val TENSOR = "tf.Tensor"
  val STACK = "tf.stack"
  val FUNCTION = "tf.function"

  val REDUCE_SUM = "tf.reduce_sum"
  val TENSOR_DOT = "tf.tensordot"
  val MAT_VEC = "tf.linalg.matvec"
  val MAT_MUL = "tf.matmul"
  val ADD = "tf.math.add"
  val MUL = "tf.math.multiply"
  val TRANSPOSE = "tf.transpose"
  val FILL = "tf.fill"
  val SLIDING_WINDOW = "tf.text.sliding_window"

  def tensordot(a: Expr, b: Expr): Expr = {
    val arrayType = Lib.inferType(a).asInstanceOf[ArrayTypeT]
    val length = arrayType.len
    val dataType = arrayType.et
    val one = Lib.one(() => IntType(32))
    ExternFunctionCall(
      TENSOR_DOT,
      Seq(a, b, one).reverse,
      Seq(
        ArrayTypeVar(dataType, length),
        ArrayTypeVar(dataType, length),
        IntTypeVar(32)).reverse,
      dataType)
  }

  def reduceSum(a: Expr): Expr = {
    val arrayType = Lib.inferType(a).asInstanceOf[ArrayTypeT]
    val length = arrayType.len
    val dataType = arrayType.et
    val one = Lib.one(() => IntType(32))
    ExternFunctionCall(
      REDUCE_SUM,
      Seq(a).reverse,
      Seq(ArrayTypeVar(dataType, length)).reverse,
      dataType)
  }

  def matvec(a: Expr, b: Expr, aTransposed: Boolean = false): Expr = {
    val aType = Lib.inferType(a).asInstanceOf[ArrayTypeT]
    val n = aType.len
    val m = aType.et.asInstanceOf[ArrayTypeT].len
    val dataType = aType.et.asInstanceOf[ArrayTypeT].et

    ExternFunctionCall(
      MAT_VEC,
      Seq(
        a,
        b,
        TypeArith(Extern.bool2int(aTransposed), BoolType())).reverse,
      Seq(
        ArrayTypeVar(aType.et, aType.len),
        ArrayTypeVar(dataType, if (aTransposed) n else m),
        CScalarTypeVar()).reverse,
      ArrayType(dataType, if (aTransposed) m else n))
  }

  def matmul(a: Expr, b: Expr, aTransposed: Boolean = false, bTransposed: Boolean = false): Expr = {
    val aType = Lib.inferType(a)
    val bType = Lib.inferType(b)
    val dataType = Lib.elementType(aType)
    val n = Lib.rows(aType)
    val m = Lib.cols(aType)
    val k = Lib.rows(bType)
    val l = Lib.cols(bType)
    val dims = (aTransposed, bTransposed) match {
      case (false, false) => (n, m, k)
      case (false, true) => (n, m, l)
      case (true, false) => (m, n, k)
      case (true, true) => (m, n, l)
    }

    ExternFunctionCall(
      MAT_MUL,
      Seq(
        a,
        b,
        TypeArith(Extern.bool2int(aTransposed), BoolType()),
        TypeArith(Extern.bool2int(bTransposed), BoolType())).reverse,
      Seq(
        ArrayTypeVar(ArrayType(dataType, m), n),
        ArrayTypeVar(ArrayType(dataType, l), k),
        CGenDataTypeVar(),
        CGenDataTypeVar()).reverse,
      ArrayType(ArrayType(dataType, dims._3), dims._1))
  }

  def add(a: Expr, b: Expr): Expr = {
    val aType = Lib.inferType(a).asInstanceOf[ArrayTypeT]

    ExternFunctionCall(
      ADD,
      Seq(a, b).reverse,
      Seq(
        ArrayTypeVar(aType.et, aType.len),
        ArrayTypeVar(aType.et, aType.len)).reverse,
      aType)
  }

  def mul(a: Expr, b: Expr): Expr = {
    val bType = Lib.inferType(b).asInstanceOf[ArrayTypeT]

    ExternFunctionCall(
      MUL,
      Seq(a, b).reverse,
      Seq(
        CGenDataTypeVar(),
        ArrayTypeVar(bType.et, bType.len)).reverse,
      bType)
  }

  def fill(value: Expr, len: ArithTypeT): Expr = {
    val t = Lib.inferType(value).asInstanceOf[CGenDataTypeT]
    val i32 = IntType(32)
    ExternFunctionCall(
      FILL,
      Seq(TypeArith(len, i32), value).reverse,
      Seq(IntTypeVar(32), CGenDataTypeVar()).reverse,
      ArrayType(t, len))
  }

  def transpose(a: Expr): Expr = {
    val aType = Lib.inferType(a).asInstanceOf[ArrayTypeT]
    val n = aType.len
    val t = aType.et.asInstanceOf[ArrayTypeT].et
    val m = aType.et.asInstanceOf[ArrayTypeT].len

    ExternFunctionCall(
      TRANSPOSE,
      Seq(a).reverse,
      Seq(ArrayTypeVar(ArrayType(t, m), n)).reverse,
      ArrayType(ArrayType(t, n), m))
  }

  def slidingWindow(vec: Expr, windowWidth: ArithTypeT): Expr = {
    val vecType = Lib.inferType(vec).asInstanceOf[ArrayTypeT]
    val t = vecType.et
    val n = vecType.len
    val numberOfWindows = n.ae - windowWidth.ae + 1
    val i32 = IntType(32)

    ExternFunctionCall(
      SLIDING_WINDOW,
      Seq(vec, TypeArith(windowWidth, i32)).reverse,
      Seq(ArrayTypeVar(t, n), IntTypeVar(32)).reverse,
      ArrayType(ArrayType(t, windowWidth), numberOfWindows))
  }
}
