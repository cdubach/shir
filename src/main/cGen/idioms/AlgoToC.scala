package cGen.idioms

import cGen.{ArrayTypeT, CGenDataTypeT, CGenExpr, DestViewType, NoEffectType}
import core.{ArithType, Expr, FunTypeT, FunctionCall, ParamDef, ParamUse, Type, TypeChecker}

/**
 * Converts algorithmic (algo) primitives to C backend (cGen) primitives.
 */
object AlgoToC {
  def apply(expr: Expr): Expr = {
    expr match {
      case cExpr: CGenExpr => cExpr

      case algo.Map(f, in, _) => cGen.Map(apply(f), apply(in))

      // FIXME: create parallel build primitive in C backend
      case algo.MapParallel(f, in, _) => apply(algo.MapAsync(f, in:_*))

      case algo.Join(in, _) => cGen.Join(apply(in))
      case algo.Split(in, n, _) => cGen.Split(apply(in), n)

      case algo.MapAsync(f, in, _) =>
        val transformedIn = in.map(apply).map(TypeChecker.check)
        val transformedF = TypeChecker.check(apply(f))
        val length = transformedIn.map(_.t.asInstanceOf[ArrayTypeT].len.ae).head
        cGen.Build({
          val index = ParamDef(cGen.IntType(32))
          cGen.CLambda(index, FunctionCall(transformedF, transformedIn.map(cGen.primitive.IdxS(_, ParamUse(index)))))
        }, length)

      case algo.Pad(in, leftPadding, rightPadding, paddingValue, _) =>
        val inTransformed = TypeChecker.check(apply(in))
        val elementType = inTransformed.t.asInstanceOf[cGen.ArrayType].et
        val elements = Seq(
          cGen.kernels.LowLevelLib.constArray(TypeArith(apply(paddingValue).asInstanceOf[ArithType], elementType), leftPadding),
          inTransformed,
          cGen.kernels.LowLevelLib.constArray(TypeArith(apply(paddingValue).asInstanceOf[ArithType], elementType), rightPadding),
        ).filterNot(elem => {
          val len = cGen.Lib.length(elem).ae
          len.isEvaluable && len.evalInt == 0
        })
        elements.reduceLeft((accumulator, element) => cGen.primitive.Concat(cGen.Tuple(accumulator, element), cGen.NoEffectType()))

      case algo.TransposeND(in, transposedDims, expectedOutT) =>
        // TODO: pad dimensions in case transposedDim is shorter than the input dimensions
        val indices = Seq.fill(transposedDims.length)(ParamDef(cGen.IntType(32)))
        val transformedIn = TypeChecker.check(apply(in))
        val dims = cGen.Lib.dims(transformedIn).reverse
        val element = indices.reverse.foldLeft(transformedIn)((acc, index) => cGen.primitive.IdxS(acc, ParamUse(index)))
        val result = TypeChecker.check(transposedDims.foldLeft(element)((acc, index) => {
          val reorderedIndex = indices(index.ae.evalInt)
          val reorderedDim = dims(index.ae.evalInt)
          cGen.Build(cGen.CLambda(reorderedIndex, acc), reorderedDim, cGen.EagerType())
        }))
        assert(result.t == apply(expectedOutT))
        TypeChecker.check(result)

      case algo.SlideGeneral(in, chunkSize, stepSize, _) if stepSize.ae.isEvaluable && stepSize.ae.evalInt == 1 =>
        cGen.Slide(apply(in), chunkSize)

      case algo.Repeat(expr, n, _) => cGen.Repeat(apply(expr), n, cGen.SrcViewType())
      case algo.Zip(tuple, _, _) => cGen.Zip(apply(tuple))
      case algo.Fold(f, in, _) =>
        val transformedIn = TypeChecker.check(apply(in))
        val transformedF = TypeChecker.check(apply(f))
        val accType = transformedF.t.asInstanceOf[cGen.CFunTypeT].outType.asInstanceOf[cGen.CFunTypeT].inType
//        val reduceF = {
//          // Swap function parameters
//          val accParam = ParamDef(accType)
//          val elemParam = ParamDef(transformedIn.t.asInstanceOf[ArrayTypeT].et)
//          TypeChecker.check(cGen.CLambda(Seq(elemParam, accParam), FunctionCall(transformedF, Seq(accParam, elemParam))))
//        }
        val initialElement = castIfNecessary(cGen.primitive.IdxS(transformedIn, cGen.Constant(0, cGen.IntType(32))), accType)
        val inputSlice = TypeChecker.check(cGen.Slice(transformedIn, cGen.Constant(1, cGen.IntType(32)), cGen.Lib.length(transformedIn).ae - 1))
        TypeChecker.check(cGen.Reduce(transformedF, initialElement, inputSlice))

      case algo.Input(name, _, _, t) =>
        // FIXME: Find a better way to model inputs
        val inType = cGen.IntType(32)
        cGen.ExternFunctionCall(s"input_${name.s}", Seq(cGen.Constant(0, inType)), Seq(cGen.IntTypeVar(32)), apply(t))

      case algo.ResizeInteger(expr, _, t) => cGen.TypeConv(apply(expr), apply(t))
      case algo.Mul(tuple, t) => binary(tuple, cGen.AlgoOp.Mul, t)
      case algo.Add(tuple, t) => binary(tuple, cGen.AlgoOp.Add, t)

      case algo.Tuple(elements, _) if elements.length == 2 => cGen.Tuple(apply(elements.head), apply(elements(1)))
      case algo.Tuple(elements, _) => throw new Exception(s"Unsupported tuple arity ${elements.length}")

      case algo.AlgoLambda(param, body, _) if body.t.isInstanceOf[FunTypeT] =>
        cGen.CLambda(apply(param).asInstanceOf[ParamDef], apply(body), Some(NoEffectType()))

      case algo.AlgoLambda(param, body, _) =>
        cGen.CLambda(apply(param).asInstanceOf[ParamDef], apply(body), Some(DestViewType()))

      case ParamUse(p) => ParamUse(apply(p).asInstanceOf[ParamDef])
      case FunctionCall(f, arg, _) => FunctionCall(apply(f), apply(arg))
      case p: ParamDef => p.build(Seq(apply(p.t)))
      case _ => throw new Exception(s"Unsupported expression type ${expr.getClass.getName}")
    }
  }

  def apply(t: Type): Type = {
    t match {
      case t: cGen.CGenDataTypeT => t

      case algo.SignedIntType(bitWidth) => cGen.IntType(bitWidth, unsigned = 0)
      case algo.IntType(bitWidth) => cGen.IntType(bitWidth, unsigned = 1)
      case algo.AlgoFunType(inType, outType) => cGen.CFunType(apply(inType), apply(outType), DestViewType())
      case algo.SeqType(et, len) => cGen.ArrayType(apply(et).asInstanceOf[CGenDataTypeT], len)
      case algo.TupleType(elements) => cGen.TupleType(elements.map(apply).map(_.asInstanceOf[CGenDataTypeT]):_*)

      case arith: ArithType => arith
      case _ => throw new Exception(s"Unsupported type ${t.getClass.getName}")
    }
  }

  private def binary(tuple: Expr, op: cGen.AlgoOp.AlgoOp, t: Type): Expr = {
    val transformedTuple = TypeChecker.check(apply(tuple))
    val left = castIfNecessary(cGen.primitive.Fst(transformedTuple, cGen.SrcViewType()), t)
    val right = castIfNecessary(cGen.primitive.Snd(transformedTuple, cGen.SrcViewType()), t)
    cGen.BinaryOp(left, right, op)
  }

  private def castIfNecessary(expr: Expr, t: Type): Expr = {
    val checked = TypeChecker.check(expr)
    if (checked.t == t) checked
    else TypeChecker.check(cGen.TypeConv(checked, apply(t)))
  }
}
