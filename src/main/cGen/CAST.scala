package cGen

import cGen.AlgoOp.AlgoOp
import cGen.CCodeGen.FtdLet
import cGen.idioms.TypeArith
import cGen.primitive._
import core._
import lift.arithmetic.Cst

import java.util.concurrent.atomic.AtomicInteger
import scala.annotation.tailrec
import scala.collection.mutable

object CASTPass {
  def apply(expr: Expr, env: Env, platform: Platform = Linux64): Seq[CFunDecl] = {
    val castPass = new CASTPass(platform)
    castPass.AST(expr)(env)
    castPass.funDecls
  }
}

class CASTPass(platform: Platform) {
  var funDecls = Seq[CFunDecl]()
  val glbId = new AtomicInteger(0)

  /** CGenLet chain is very deep due to ANF.
   * To avoid stackoverflow (it will really happen), lets will be flattened first
   * before transforming into CASTs
   *
   * @param expr : CGenLet
   * @return A CManyStmts representing a flatten cgenlet
   */
  def letAST(expr: Expr)(implicit env: Env): CAST = {
    val dcpdLets: Seq[FtdLet] = flattenLet(expr) // expr should be a cgenlet
    CManyStmts(dcpdLets.map(decomposedLetAST).map(_.toStmt))
  }

  def assignForSIf(to: ParamDef, ast: CAST): CStmt = ast match {
    case CManyStmts(children) => CManyStmts(children.dropRight(1) ++ Seq(assignForSIf(to, children.last)))
    case CIf(cc, cond, b0, b1) => CIf(cc, cond, assignForSIf(to, b0), assignForSIf(to, b1))
    case other => CAssign(AST(ParamUse(to)).toExpr, other.toExpr)
  }

  /** Transforms a FtdLet into CStmt
   */
  def decomposedLetAST(ftdLet: FtdLet)(implicit env: Env): CAST = ftdLet match {
    case (Some(p: ParamDef), alc@Alloc(_, _, _)) =>
      allocAST(p, alc)
    case (Some(ParamDef(VoidType(), _)), arg) =>
      AST(arg)
    case (Some(p: ParamDef), arg@(Id(_, _) | UnaryOp(_, _, _) | BinaryOp(_, _, _, _, _))) =>
      // these three primitive has not effect on the memory by them result
      // they will always be captured by an assign, and codegen will handle those cases specially
      env.pdCExprMap.put(p.id, AST(arg).toExpr)
      CDummy().toStmt
    case (Some(p: ParamDef), tuple: TupleExpr) =>
      env.pdCExprMap.put(p.id, AST(tuple).toExpr) // not generate any code for this
      CDummy().toStmt
    case (None, SIf(cond, b0, b1, _)) => AST(DIf(Dummy(), cond, b0, b1))
    case (Some(p: ParamDef), SIf(cond, b0, b1, _)) =>
      assignForSIf(p, CIf(CDummy().toStmt, AST(cond).toExpr, AST(b0).toStmt, AST(b1).toStmt))
    //      // for a sif, make sure the assignment happens on the end of each branch
    //      val astb0 = AST(b0) match {
    //        case CManyStmts(children) => CManyStmts(children.dropRight(1) ++ Seq(CAssign(AST(ParamUse(p)).toExpr, children.last.toExpr)))
    //        case other => CAssign(AST(ParamUse(p)).toExpr, other.toExpr)
    //      }
    //      val astb1 = AST(b1) match {
    //        case CManyStmts(children) => CManyStmts(children.dropRight(1) ++ Seq(CAssign(AST(ParamUse(p)).toExpr, children.last.toExpr)))
    //        case other => CAssign(AST(ParamUse(p)).toExpr, other.toExpr)
    //      }
    //      CIf(AST(cc).toStmt, AST(cond).toExpr, astb0.toStmt, astb1.toStmt)
    case (Some(p: ParamDef), arg) =>
      val castPass = this
      val ASTArg = castPass.AST(arg)
      ASTArg match {
        //        case c: CManyStmts =>
        //          val last = c.toExpr
        //          val ASTArg1 = c.popLast
        //          CTwoStmts(ASTArg1.toStmt, CAssign(castPass.AST(ParamUse(p)).toExpr, last.toExpr))
        case CIfExpr(cond, b0, b1) =>
          CIf(CDummy().toStmt, cond, CAssign(castPass.AST(ParamUse(p)).toExpr, b0), CAssign(castPass.AST(ParamUse(p)).toExpr, b1))
        case _ =>
          // arg is an expr or can be converted to cexpr
          CAssign(castPass.AST(ParamUse(p)).toExpr, ASTArg.toExpr)
      }
    case (None, other) => AST(other)
  }

  @tailrec
  final def flattenLet(e: Expr)(implicit res: Seq[FtdLet] = Nil): Seq[FtdLet] = e match {
    case CGenLet(ParamDef(VoidType(), _), b, a, _) => flattenLet(b)(res ++ Seq((None, a)))
    case CGenLet(p, b, a, _) => flattenLet(b)(res ++ Seq((Some(p), a)))
    case other => res ++ Seq((None, other))
  }

  def allocAST(p: ParamDef, alc: Expr)(implicit env: Env): CAST = {
    alc match {
      case Alloc(t, ak, _) => ak match {
        case AllocKind.Heap => t match {
          case ArrayType(ArrayType(ArrayType(ArrayType(_, _), _), _), _) =>
            alloc(AST(ParamUse(p)).toExpr, t.asInstanceOf[CGenDataTypeT])
          case ArrayType(nT@ArrayType(mT@ArrayType(_, _), m), n) if !platform.IS_FLATTEN =>
            CTwoStmts(
              CAssign(AST(ParamUse(p)).toExpr, CMalloc(t)),
              {
                val newEnv = new Env(Some(env))
                val i = AST(ParamDef(IntType(32)))(newEnv).asInstanceOf[CVarDecl]
                CForloop(CConstant(0), CConstant(n.ae.evalInt), i,
                  CBlock(newEnv.varDecls, Seq(CAssign(CDestArrayAccess(AST(ParamUse(p)).toExpr, CVarExpr(i)), CMalloc(nT)), {
                    val newEnv = new Env(Some(env))
                    val j = AST(ParamDef(IntType(32)))(newEnv).asInstanceOf[CVarDecl]
                    CForloop(CConstant(0), CConstant(m.ae.evalInt), j,
                      CBlock(newEnv.varDecls, Seq(CAssign(CDestArrayAccess(CDestArrayAccess(AST(ParamUse(p)).toExpr, CVarExpr(i)), CVarExpr(j)), CMalloc(mT)))))
                  })))
              })
          case ArrayType(inT@ArrayType(_, _), len) if !platform.IS_FLATTEN =>
            CTwoStmts(
              CAssign(AST(ParamUse(p)).toExpr, CMalloc(t)),
              {
                val newEnv = new Env(Some(env))
                val idxDecl = AST(ParamDef(IntType(32)))(newEnv).asInstanceOf[CVarDecl]
                CForloop(CConstant(0), CConstant(len.ae.evalInt), idxDecl,
                  CBlock(newEnv.varDecls, Seq(CAssign(CDestArrayAccess(AST(ParamUse(p)).toExpr, CVarExpr(idxDecl)), CMalloc(inT)))))
              })
          case ArrayType(_, _) =>
            alloc(AST(ParamUse(p)).toExpr, t.asInstanceOf[CGenDataTypeT])
          case NamedTupleStructType(_, _) =>
            CTwoStmts(
              CAssign(AST(ParamUse(p)).toExpr, CMalloc(t)),
              alloc(AST(ParamUse(p)).toExpr, t.asInstanceOf[CGenDataTypeT])
            )
          case other =>
            throw new NotImplementedError(s"cannot assign ${other} to heap")
        }
        case AllocKind.Stack => t match {
          case at@ArrayType(_, _) => {
            if (platform.IS_FLATTEN) {
              val s = CVarDecl(ParamDef(platform.getFlattenArrayType(at)), onStack = true)
              env.addVarDecl(s) // add for declaration
              CAssign(AST(ParamUse(p)).toExpr, CAddressOf(CVarExpr(s).toExpr))
            } else throw new NotImplementedError(s"cannot assign ${t} to stack, with platform.is_flatten=${platform.IS_FLATTEN}")
          }
          case _ =>
            val s = ParamDef(t)
            CAssign(AST(ParamUse(p)).toExpr, CAddressOf(AST(ParamUse(s)).toExpr))
        }
      }
    }

  }

  // every block should has its onw env
  def AST(expr: Expr)(implicit env: Env = new Env(None)): CAST = expr match {
    case _: LambdaT =>
      val newEnv: Env = new Env(Some(env))
      val fd = CFunDecl(getParams(expr, newEnv), getBody(expr, newEnv))
      funDecls = funDecls :+ fd
      fd
    case CGenLet(_, _, _, _) => letAST(expr)
    case DBuild(f, n, ltt, _) => n.ae match {
      case Cst(0) => CDummy()
      case Cst(1) => // when n == 1, no need to generate a forloop
        val idxDecl = AST(f.asInstanceOf[LambdaT].param).asInstanceOf[CVarDecl]
        env.addVarDecl(idxDecl)
        CTwoStmts(
          CAssign(CVarExpr(idxDecl), CConstant(0)),
          AST(f.asInstanceOf[LambdaT].body).toStmt
        )
      case _ =>
        val newEnv = new Env(Some(env))
        val idxDecl = AST(f.asInstanceOf[LambdaT].param)(newEnv).asInstanceOf[CVarDecl]
        newEnv.addFuncVarDecl(idxDecl)
        val blockStmts = Seq(AST(f.asInstanceOf[LambdaT].body)(newEnv).toStmt)
        CForloop(CConstant(0), CConstant(n.ae.evalInt),
          idxDecl,
          CBlock(newEnv.varDecls, blockStmts))
    }
    case DIfold(f, _, d, n, _) =>
      val newEnv = new Env(Some(env))
      val idxDecl = AST(Util.param(f.asInstanceOf[LambdaT], 0))(newEnv).asInstanceOf[CVarDecl] // add vardecl to block
      newEnv.addFuncVarDecl(idxDecl)
      val accDecl = AST(Util.param(f.asInstanceOf[LambdaT], 1))(env).asInstanceOf[CVarDecl]
      env.addVarDecl(accDecl)
      val blockStmts = Seq(AST(Util.innermostBody(f.asInstanceOf[LambdaT]))(newEnv).toStmt)
      CForloop(CConstant(0), CConstant(n.ae.evalInt), idxDecl, CBlock(newEnv.varDecls, CAssign(CVarExpr(accDecl), AST(ValueAt(d)).toExpr).toStmt +: blockStmts))
    case BinaryOp(in1, in2, op, _, _) => CBinOp(op, AST(in1), AST(in2))
    case UnaryOp(in, op, _) => CSingleOp(op, AST(in))
    case Id(in, _) => AST(in)
    case Assign(in, d, _) => in match {
      case ParamUse(pd) if env.pdCExprMap.contains(pd.id) => CAssign(AST(ValueAt(d)).toExpr, env.pdCExprMap(pd.id).toExpr)
      case ParamUse(_) => CAssign(AST(ValueAt(d)).toExpr, AST(in).toExpr)
      case _ => CAssign(AST(ValueAt(d)).toExpr, AST(in).toExpr)
    }
    case FstDest(in, _) => {
      val tst = in.t.asInstanceOf[AddressType].ptrType.asInstanceOf[NamedTupleStructType]
      if (isNotScalarType(tst.namedTypes.head._2)) CSelect0(CValueAt(AST(in).toExpr))
      else CAddressOf(CSelect0(CValueAt(AST(in).toExpr)))
    }
    case SndDest(in, _) => {
      val tst = in.t.asInstanceOf[AddressType].ptrType.asInstanceOf[NamedTupleStructType]
      if (isNotScalarType(tst.namedTypes(1)._2)) CSelect1(CValueAt(AST(in).toExpr))
      else CAddressOf(CSelect1(CValueAt(AST(in).toExpr)))
    }
    case IdxOffsetOp(idx, offset, op, _) => CBinOp(op, AST(idx), AST(offset))
    case IdxDest(in, idx, _) => {
      val cin = AST(in).toExpr
      val cidx = AST(idx).toExpr
      if (platform.IS_FLATTEN) {
        val at = TypeChecker.check(in).t.asInstanceOf[AddressType].ptrType.asInstanceOf[ArrayType]
        at match {
          case ArrayType(ArrayType(ArrayType(_, m), n), _) => CBinOp(AlgoOp.Add, cin, CBinOp(AlgoOp.Mul, cidx, CConstant(n.ae.evalInt * m.ae.evalInt)))
          case ArrayType(ArrayType(_, m), _) => CBinOp(AlgoOp.Add, cin, CBinOp(AlgoOp.Mul, cidx, CConstant(m.ae.evalInt)))
          case ArrayType(_, _) => CBinOp(AlgoOp.Add, cin, cidx)
        }
      } else {
        CDestArrayAccess(cin, cidx)
      }
    }
    case IdxS(in, idx, _) =>
      if (platform.IS_FLATTEN) {
        val at = TypeChecker.check(in).t.asInstanceOf[ArrayTypeT]
        val cin = AST(in).toExpr
        val cidx = AST(idx).toExpr
        val r = at match {
          case ArrayType(ArrayType(ArrayType(_, m), n), _) => CBinOp(AlgoOp.Add, cin, CBinOp(AlgoOp.Mul, cidx, CConstant(n.ae.evalInt * m.ae.evalInt)))
          case ArrayType(ArrayType(_, m), _) => CBinOp(AlgoOp.Add, cin, CBinOp(AlgoOp.Mul, cidx, CConstant(m.ae.evalInt)))
          case ArrayType(_, _) => CBinOp(AlgoOp.Add, cin, cidx)
        }
        at.et match {
          case _: ArrayTypeT => r
          case _ => CValueAt(r)
        }
      } else {
        val cin = AST(in).toExpr
        val cidx = AST(idx).toExpr
        CArrayAccess(cin, cidx)
      }
    case pd: ParamDef =>
      pd.t.visit({
        case ntst@NamedTupleStructType(_, _) => env.addCStructDecls(CStructDecl(ntst))
        case _ =>
      })
      //      println("HI:" + pd.t)
      val cvd = CVarDecl(pd)
      env.pdvdMap.put(pd.id, cvd)
      cvd
    case ParamUse(pd) => env.pdvdMap.get(pd.id) match {
      case Some(cvd) => {
        if (!env.isPresent(cvd)) env.addVarDecl(cvd)
        CVarExpr(cvd)
      }
      case None =>
        // using a paramDef that is not defined yet (in the funcDecl)
        val cvd = AST(pd).asInstanceOf[CVarDecl]
        env.addVarDecl(cvd)
        CVarExpr(cvd)
    }
    case ValueAt(in, _) => CValueAt(AST(in).toExpr)
    case Constant(value, t) => CConstant(value, t)
    case TypeArith(value, t) if value.ae.isEvaluable => CConstant(value.ae.evalDouble, t)
    case TypeArith(value, _) => throw new Exception(s"Cannot generate code for non-evaluable literal type $value")
    case DIf(computeCond, cond, b0, b1, _) => CIf(AST(computeCond).toStmt, AST(cond).toExpr, AST(b0).toStmt, AST(b1).toStmt)
    case SIf(cond, b0, b1, _) => CIfExpr(AST(cond).toExpr, AST(b0).toExpr, AST(b1).toExpr)
    case DExternFunctionCall(name, ins, _, _) => CFunctionCall(name, ins.map(AST(_).toExpr))
    case Loop(f, m, _, _) => {
      val newEnv = new Env(Some(env))
      //      val idxDecl = AST(ParamDef(IntType(32)))(newEnv).asInstanceOf[CVarDecl]
      val idxDecl = AST(Util.param(f.asInstanceOf[LambdaT], 0))(newEnv).asInstanceOf[CVarDecl] // add vardecl to block
      newEnv.addFuncVarDecl(idxDecl)
      val blockStmts = Seq(AST(f.asInstanceOf[LambdaT].body)(newEnv).toStmt)
      CForloop(CConstant(0), AST(m).toExpr,
        idxDecl,
        CBlock(newEnv.varDecls, blockStmts))
    }
    case TypeConv(in, t) =>
      CTypeConv(AST(in).toExpr, t)
    case Free(in, ak, _) => ak match {
      case AllocKind.Static => CDummy()
      case AllocKind.Stack => CDummy()
      case AllocKind.Heap => in.t match {
        case AddressType(ArrayType(ArrayType(_, _), len)) if !platform.IS_FLATTEN => {
          val newEnv = new Env(Some(env))
          val idxDecl = AST(ParamDef(IntType(32)))(newEnv).asInstanceOf[CVarDecl]
          CTwoStmts(
            CForloop(CConstant(0), CConstant(len.ae.evalInt), idxDecl,
              CBlock(newEnv.varDecls, Seq(CFree(CDestArrayAccess(AST(in).toExpr, CVarExpr(idxDecl)))))),
            CFree(AST(in).toExpr)
          )
        }
        case AddressType(ArrayType(ArrayType(_, _), len)) if !platform.IS_FLATTEN => {
          val newEnv = new Env(Some(env))
          val idxDecl = AST(ParamDef(IntType(32)))(newEnv).asInstanceOf[CVarDecl]
          CTwoStmts(
            CForloop(CConstant(0), CConstant(len.ae.evalInt), idxDecl,
              CBlock(newEnv.varDecls, Seq(CFree(CDestArrayAccess(AST(in).toExpr, CVarExpr(idxDecl)))))),
            CFree(AST(in).toExpr)
          )
        }
        case AddressType(ArrayType(_, _)) => CFree(AST(in).toExpr)
        case AddressType(_: NamedTupleStructTypeT) => free(AST(in).toExpr, in.t.asInstanceOf[CGenDataTypeT])
        case _ => throw new NotImplementedError(s"[CCodegen] cannot handle free ${in.t.toString}")
      }
      case _ => throw new Exception("Not defined")
    }
    case SelectS(in, selection, _) => selection.ae match {
      case Cst(0) => CSelect0(AST(in).toExpr)
      case Cst(1) => CSelect1(AST(in).toExpr)
      case _ => throw new Exception("Not defined")
    }
    case Tuple(in1, in2, t: NamedTupleStructType) =>
      env.addCStructDecls(CStructDecl(t))
      CStructExpr(CStructDecl(t))
    // ----- OPENCL ------
    case BuildGlb(f, n, _) =>
      val idxDecl = AST(f.asInstanceOf[LambdaT].param).asInstanceOf[CVarDecl]
      env.addVarDecl(idxDecl)
      CTwoStmts(
        CAssign(CVarExpr(idxDecl), CGetGlbId(glbId.getAndIncrement())),
        AST(f.asInstanceOf[LambdaT].body).toStmt
      )
    case Dummy(_) => CDummy()
    case other =>
      throw new Exception(s"Should not reach: $other")
  }

  def isNotScalarType(t: CGenDataTypeT): Boolean = t match {
    case _: ArrayTypeT => true
    case _: NamedTupleStructTypeT => true
    case _ => false
  }

  // toFree is a pointer
  def free(toFree: CExpr, t: CGenDataTypeT): CStmt = t match {
    case AddressType(NamedTupleStructType(_, namedTypes)) =>
      Seq(
        Some(CFree(toFree)),
        {
          val curT = namedTypes.head._2
          if (isNotScalarType(curT)) Some(free(CSelect0(CValueAt(toFree)), curT)) // tup.t0 is also a pointer if it is not a scalar type
          else None
        },
        {
          val curT = namedTypes(1)._2
          if (isNotScalarType(curT)) Some(free(CSelect1(CValueAt(toFree)), curT)) // tup.t0 is also a pointer if it is not a scalar type
          else None
        }
      ).filter(_.isDefined).map(_.get).reduceRight((cur, acc) => CTwoStmts(acc, cur))
    case ArrayType(et, len) => {
      CFree(toFree);
    }
    case _ =>
      throw new NotImplementedError()
  }

  def alloc(assignTo: CExpr, t: CGenDataTypeT)(implicit env: Env = new Env(None)): CStmt = t match {
    case NamedTupleStructType(_, namedTypes) =>
      Seq(
        { // it is weired to repeat the same code twice, but it uses CSelect0 and CSelect1
          val curT = namedTypes.head._2
          if (isNotScalarType(curT)) Some(alloc(CSelect0(CValueAt(assignTo)), curT)) // assignTo is a pointer to struct
          else None
        },
        {
          val curT = namedTypes(1)._2
          if (isNotScalarType(curT)) Some(alloc(CSelect1(CValueAt(assignTo)), curT))
          else None
        }
      ).filter(_.isDefined).map(_.get) match {
        case Nil => CDummy().toStmt
        case other => other.reduceRight((cur, acc) => CTwoStmts(cur, acc))
      }
    case ArrayType(et, len) => CTwoStmts(
      CAssign(assignTo, CMalloc(t)),
      loopAlloc(assignTo, len, et)
    )
    case _ => throw new NotImplementedError()
  }

  def loopAlloc(assignTo: CExpr, len: ArithTypeT, et: CGenDataTypeT)(implicit env: Env = new Env(None)): CStmt = {
    val newEnv = new Env(Some(env))
    val idxDecl = AST(ParamDef(IntType(32)))(newEnv).asInstanceOf[CVarDecl]
    et match {
      case arrT@ArrayType(inT, inLen) if !platform.IS_FLATTEN =>
        CForloop(CConstant(0), CConstant(len.ae.evalInt), idxDecl,
          CBlock(newEnv.varDecls, Seq(
            CTwoStmts(
              CAssign(CDestArrayAccess(assignTo, CVarExpr(idxDecl)), CMalloc(arrT)),
              loopAlloc(CDestArrayAccess(assignTo, CVarExpr(idxDecl)), inLen, inT)
            ))))
      case ntst: NamedTupleStructTypeT =>
        CForloop(CConstant(0), CConstant(len.ae.evalInt), idxDecl,
          CBlock(newEnv.varDecls, Seq(
            alloc(CDestArrayAccess(assignTo, CVarExpr(idxDecl)), ntst)
          )))
      case _ => CDummy().toStmt
    }
  }

  def AST(t: Type): CAST = t match {
    case att: ArithTypeT => CConstant(att.ae.evalInt)
  }

  def getParams(expr: Expr, env: Env): Seq[CVarDecl] = expr match {
    case lam: LambdaT => {
      val cvd = AST(lam.param)(env).asInstanceOf[CVarDecl]
      env.addFuncVarDecl(cvd) // need a special handle, add the funcId set, not a general id set
      Seq(cvd) ++ getParams(lam.body, env)
    }
    case _ => Seq()
  }

  def getBody(expr: Expr, env: Env): CBlock = expr match {
    case CLambda(p, body, _) => getBody(body, env)
    case other =>
      val stmts = Seq(AST(other)(env).toStmt)
      CBlock(env.varDecls, stmts) // Block(other)
  }

}


object CAST {
  private val _id = new AtomicInteger(0)

  def id: Int = _id.getAndIncrement()
}

class CAST {
  val id = CAST.id

  def toStmt: CStmt = this match {
    case e: CExpr => CExprStmt(e)
    case s: CStmt => s
    case _ =>
      throw new Exception(s"$this cannot toStmt")
  }

  def toExpr: CExpr = {
    this match {
      case expr: CExpr => expr
      case _ =>
        throw new Exception(s"$this cannot toExpr")
    }
  }

  def isExpr: Boolean = this.isInstanceOf[CExpr]

  def children: Seq[CAST] = Seq()
}

// TODO: provide the correct type for CFunDecl
case class CFunDecl(params: Seq[CVarDecl], block: CBlock, t: Type = VoidType()) extends CAST {
  override def children: Seq[CAST] = params ++ Seq(block)

  //  val ints: Seq[Type] = params.map(_.t)
}

case class CVarDecl(pd: ParamDef, onStack: Boolean = false) extends CAST {
  override def children: Seq[CAST] = Seq()

  val t: Type = pd.t
}

trait CStmt extends CAST

trait CExpr extends CAST {
  def t: Type
}

case class CProgram(funDecls: Seq[CFunDecl], structDecls: mutable.Set[CStructDecl]) extends CAST {
  override def children: Seq[CAST] = funDecls
}

case class CExprStmt(e: CExpr) extends CStmt {
  override def children: Seq[CAST] = Seq(e)

  override def toExpr: CExpr = e
}

case class CBlock(varDecls: Seq[CVarDecl], stmts: Seq[CStmt]) extends CStmt {
  override def children: Seq[CAST] = varDecls ++ stmts
}

case class CIf(computeCond: CStmt, cond: CExpr, branch0: CStmt, branch1: CStmt) extends CStmt {
  override def children: Seq[CAST] = Seq(computeCond, cond, branch0, branch1)
}

case class CIfExpr(cond: CExpr, branch0: CExpr, branch1: CExpr) extends CExpr {
  override def children: Seq[CAST] = Seq(cond, branch0, branch1)

  override def t: Type = ???
}

case class CForloop(begin: CExpr, end: CExpr, idx: CVarDecl, block: CBlock) extends CStmt {
  override def children: Seq[CAST] = Seq(begin, end, idx, block)
}

case class CFunctionCall(name: String, ins: Seq[CExpr]) extends CExpr {
  override def children: Seq[CAST] = ins

  override def t: Type = ???
}


case class CConstant(num: Double, t: Type = IntType(32)) extends CExpr {
  override def children: Seq[CAST] = Seq()
}

case class CBinOp(op: AlgoOp, in1: CAST, in2: CAST) extends CExpr {
  override def children: Seq[CAST] = Seq(in1, in2)

  override def t: Type = IntType(32)
}

case class CSingleOp(op: AlgoOp, in: CAST) extends CExpr {
  override def children: Seq[CAST] = Seq(in)

  override def t: Type = IntType(32)
}

case class CAssign(lhs: CExpr, rhs: CExpr) extends CStmt {
  override def children: Seq[CAST] = Seq(lhs, rhs)
}

case class CVarExpr(vd: CVarDecl) extends CExpr {
  override def children: Seq[CAST] = Seq(vd)

  override def t: Type = vd.t
}

case class CValueAt(n: CExpr) extends CExpr {
  override def children: Seq[CAST] = Seq(n)

  override def t: Type = n.t.asInstanceOf[AddressType].ptrType
}

case class CAddressOf(n: CExpr) extends CExpr {
  override def children: Seq[CAST] = Seq(n)

  override def t: Type = AddressType(n.t)
}

case class CArrayAccess(array: CExpr, idx: CExpr) extends CExpr {
  override def children: Seq[CAST] = Seq(array, idx)

  override def t: Type = array.t.asInstanceOf[ArrayType].et
}

case class CDestArrayAccess(dest: CExpr, idx: CExpr) extends CExpr {
  override def children: Seq[CAST] = Seq(dest, idx)

  override def t: Type = dest.t match {
    case a: AddressType => AddressType(a.ptrType.asInstanceOf[ArrayType].et)
    case a: ArrayType => AddressType(a.et)
  }
}

case class CDummy() extends CExpr {
  override def children: Seq[CAST] = ???

  override def t: Type = ???
}

case class CManyStmts(override val children: Seq[CStmt]) extends CStmt {
  override def toExpr: CExpr =
    ???
}

object CManyStmts {
  def apply(cstmts: Seq[CStmt]): CManyStmts = new CManyStmts(flatten(cstmts))

  private def flatten(cstmts: Seq[CStmt]): Seq[CStmt] = cstmts.flatMap({
    case CManyStmts(children) => children
    case CExprStmt(CDummy()) => Seq()
    case other => Seq(other)
  })
}

object CTwoStmts {
  def apply(first: CStmt, second: CStmt): CManyStmts = CManyStmts(Seq(first, second))
}

//case class CTwoStmts(first: CStmt, second: CStmt) extends CStmt {
//  override def children: Seq[CAST] = Seq(first, second)
//
//  override def toExpr: CExpr = second.toExpr
//
//  def popLast: CStmt = second match {
//    case c@CTwoStmts(_, _) => CTwoStmts(first, c.popLast)
//    case other => first
//  }
//}

case class CMalloc(tt: Type) extends CExpr {
  override def children: Seq[CAST] = Seq()

  override def t: Type = AddressType(tt)
}

case class CStaticAlloc(tt: Type) extends CExpr {
  override def children: Seq[CAST] = Seq()

  override def t: Type = AddressType(tt)
}

case class CDeclare(tt: Type) extends CStmt

case class CSizeOf(tt: Type) extends CExpr {
  override def children: Seq[CAST] = Seq()

  override def t: Type = IntType(32);
}

case class CTypeConv(in: CExpr, tt: Type) extends CExpr {
  override def children: Seq[CAST] = Seq()

  override def t: Type = tt;
}

case class CFree(in: CExpr) extends CStmt {
  override def children: Seq[CAST] = Seq()
}

case class CSelect0(in: CExpr) extends CExpr {
  val name = "t0"

  override def children: Seq[CAST] = Seq()

  override def t: Type =
    in.t.asInstanceOf[NamedTupleStructType].namedTypes.head._2
}

case class CSelect1(in: CExpr) extends CExpr {
  val name = "t1"

  override def children: Seq[CAST] = Seq()

  override def t: Type = in.t.asInstanceOf[NamedTupleStructType].namedTypes(1)._2
}

case class CStructDecl(tst: NamedTupleStructType) extends CStmt {
  override def children: Seq[CAST] = Seq()

  override def equals(obj: Any): Boolean = obj match {
    case CStructDecl(_) => obj.hashCode() == this.hashCode()
    case _ => false
  }

  override def hashCode(): Int = tst.name.hashCode()
}

case class CStructExpr(csd: CStructDecl) extends CExpr {
  override def t: Type = csd.tst
}

case class CGetGlbId(dim: Int) extends CExpr {
  override def t: Type = IntType(32)
}
