package cGen.pass

import cGen._
import cGen.pass.CommonPass.{countEager, hasEager, hasSrcView}
import cGen.primitive.{FstExpr, IdxExpr, M, SndExpr}
import core.rewrite.{RewriteExhaustively, RewriteRandom, Rule}
import core.util.Log
import core.{Expr, ParamUse, ShirIR}

import scala.collection.mutable
import scala.collection.parallel.mutable.ParArray
import scala.math.abs
import scala.util.Random

object TuningPass {
  def tuningRules = Seq(StoDRule, MRemovalRule, SM2SRule)

  def unsupportedDestViewFilter(es: Seq[Expr]) = es.filter(!hasUnsupportedExpr(_))

  def perfModelFilter(top: Int)(es: Seq[Expr]): Seq[Expr] = CGenPerformanceModel.top(top, es)

  /** tune a program, by exploring variants.
   * only the top 30 programs ranked by the "performance model" will be returns
   * the performance model is simply counting the number of eager primitive.
   */
  def tuningPass(expr: Expr, top: Int): Seq[Expr] = {
    val eagers = toAllEagers(expr).asInstanceOf[Expr]
    val exprs = RewriteExhaustively(es =>
      perfModelFilter(top)(unsupportedDestViewFilter(es))).rewrite(tuningRules, Seq(eagers))
    exprs.foreach(e => println(checkInvalidPass(e)))
    exprs.map(OrchestratePass(_, M2MateralizePass(), ExternFunctionCallPass()))
  }


  def hasUnsupportedExpr(e: Expr): Boolean = {
    e.visit({
      case map@Map(f, in, _, _) if map.isDestView && (hasEager(f) || hasSrcView(f)) => return true
      case slide: SlideExpr if slide.isDestView => return true
      case idx: IdxExpr if idx.isDestView => return true
      case fst: FstExpr if fst.isDestView => return true
      case fst: SndExpr if fst.isDestView => return true
      case _ =>
    })
    false
  }

  /**
   * @param expr            , input to be tuned
   * @param num             , the length of the resulted randomly tuned exprs wanted
   * @param maxRewriteTimes for each expr, the max rewrite can be applied
   * @return a ParArray of randomly tuned exprs with DPS in placed
   */
  def randomTuningPassWithDPS(expr: Expr, num: Int, maxRewriteTimes: Int = 100): ParArray[Expr] = {
    Seq.fill(num)(expr).toParArray. // go parallel!!!
      map(randomTuningPass(_, maxRewriteTimes)).
      map(filterWithDPSPass).
      filter(_.isDefined).
      map(_.get)
  }

  def randomTuningPass(expr: Expr, maxRewriteTimes: Int = 100): Expr = {
    val init = toAllEagers(expr).asInstanceOf[Expr]
    val ramdom = new Random()
    var cur = init
    var pre = cur
    do {
      pre = cur
      val rewriteTimes = abs(ramdom.nextInt() % maxRewriteTimes)
      cur = RewriteRandom(rewriteTimes, hasUnsupportedExpr).rewrite(Seq(StoDRule, MRemovalRule, SM2SRule), init) // randomly rewrite the init expr
      if (hasUnsupportedExpr(cur)) Log.info(this, "found unsupported expr")
    } while (hasUnsupportedExpr(cur) && pre != cur)
    OrchestratePass(cur, M2MateralizePass(), ExternFunctionCallPass())
  }

  def filterWithDPSPass(expr: Expr): Option[Expr] = {
    try {
      Some(DPSPass.apply(expr))
    } catch {
      case exc: Throwable => Log.warn(this, exc.toString); None
    }
  }

  /** tuning and pass to dps, handled exception from dps automatically
   */
  def perfModelTuningPassWithDPS(expr: Expr, top: Int = 10): Seq[Expr] = {
    val tuneds = tuningPass(expr, top).sortBy(CGenPerformanceModel(_)).reverse
    val exprs = tuneds.map(e => {
      try {
        Some(DPSPass.apply(e))
      } catch {
        case exc: Throwable => Log.warn(this, exc.toString); None
      }
    }).filter(_.isDefined).map(_.get)
    exprs
  }

  object Distinct {
    def apply(exprs: Seq[Expr]): Seq[Expr] = {
      var m = mutable.HashMap[String, Expr]()
      exprs.foreach(e => m += (e.toString -> e))
      m.values.toSeq
    }
  }

  object CGenPerformanceModel {
    def apply(expr: Expr): Double = {
      var point: Double = 0
      expr.visit({
        case Map(f, _, _, _) if hasEager(f) => -countEager(f) * 10
        case Reduce(f, _, _, _) if hasEager(f) => -countEager(f) * 10
        case Build(f, _, _, _) if hasEager(f) => -countEager(f) * 10
        case CGenExpr(_, EagerType()) => point -= 1
        case CGenExpr(_, SrcViewType()) => point += 1
        case CGenExpr(_, DestViewType()) => point += 1.1
        case _ =>
      })
      point
    }

    def top(num: Int, exprs: Seq[Expr]): Seq[Expr] = {
      Log.info(this, s"start ranking for ${exprs.size} exprs")
      val result = exprs.sortBy(expr => apply(expr)).reverse.slice(0, num)
      Log.info(this, "end ranking")
      result
    }
  }

  def toDestView(e: CGenExpr): CGenExpr = e match {
    case _ if e.isDestView => e
    case _ => e.toDestViewT
    // TODO: Handle Map
  }

  def toEager(e: Expr): Expr = e match {
    case map: MapExpr if map.isEager => M(map.toSrcViewT)
    case build: BuildExpr if build.isEager => M(build.toSrcViewT)
    case pu@ParamUse(_) => M(pu)
    case ce: CGenExpr if ce.isEager => e
    case ce: CGenExpr => M(ce.toSrcViewT)
    case other => other
  }

  def toAllEagers(n: ShirIR): ShirIR = n match {
    case e: Expr => toEager(e.map(toAllEagers).asInstanceOf[Expr])
    case other => other.map(toAllEagers)
  }


  def getUpperBoundRewriteNum(expr: Expr): Int = {
    def collectPossibleRewrites(expr: Expr, rules: Seq[Rule]): Seq[(Expr, Rule)] = {
      rules.filter(_.isDefinedAt(expr)).map((expr, _)) ++
        expr.children.flatMap {
          case e: Expr => collectPossibleRewrites(e, rules)
          case _ => Seq()
        }
    }
    collectPossibleRewrites(toAllEagers(expr).asInstanceOf[Expr], tuningRules).size + 10
  }


  val StoDRule = Rule("StoDRule", { // SD => DD | SE => DE
    case ce@CGenExpr(args, SrcViewType()) if args.exists({
      case CGenExpr(_, DestViewType()) => true
      case _ => false
    }) => toDestView(ce.build(args.map({
      case in@CGenExpr(_, SrcViewType()) => M(in)
      case in: ParamUse => M(in)
      case other => other
    })))
    case ce@CGenExpr(args, SrcViewType()) if args.exists({
      case CGenExpr(_, EagerType()) => true
      case _ => false
    }) => toDestView(ce.build(args.map({
      case in@CGenExpr(_, SrcViewType()) => M(in)
      case in: ParamUse => M(in)
      case other => other
    })))
  })

  val SM2SRule = Rule("SM2SRule", {
    case ce@CGenExpr(args, SrcViewType()) if args.exists({ // SM => S
      case M(_, _) => true
      case _ => false
    }) => ce.build(args.map({
      case M(in, _) =>
        in
      case other => other
    }))
  })

  val MRemovalRule = Rule("MRemovalRule", {
    case M(M(in, _), _) => M(in) // MM => M
    case M(ce@CGenExpr(_, DestViewType()), _) => ce // MD => D
    case M(ce@CGenExpr(_, EagerType()), _) => ce // ME => E
    case ce@CGenExpr(args, EagerType()) if args.exists({ // EM => E
      case M(_, _) => true
      case _ => false
    }) => ce.build(args.map({
      case M(in, _) => in
      case other => other
    }))
    case map@Map(CLambda(p, M(ce@CGenExpr(_, SrcViewType()), _), _), in, _, _) if map.isSrcView => Map(CLambda(p, ce), in, SrcViewType()) // M => _
    case build@Build(CLambda(p, M(ce@CGenExpr(_, SrcViewType()), _), _), n, _, _) if build.isSrcView => Build(CLambda(p, ce), n, SrcViewType()) // M => _
    //    case map@Map(CLambda(p, M(ce@CGenExpr(_, EagerType()), _), _), in, _, _) if map.isSrcView => Map(CLambda(p, ce), in)
    //    case map@Map(CLambda(p, M(ce@CGenExpr(_, DestViewType()), _), _), in, _, _) if map.isDestView => Map(CLambda(p, ce), in)
    //    case map@Map(CLambda(p, M(ce@CGenExpr(_, EagerType()), _), _), in, _, _) if map.isDestView => Map(CLambda(p, ce), in)
  })

  def M2MateralizePass() = RebuildPass("M2MateralizePass", {
    case M(in, _) => Materialize(in)
  })

  /** ExternFuntionCall require the input to be either with the effect of eager or destination view
   */
  def ExternFunctionCallPass() = RebuildPass("ExternFunctionCallPass", {
    case ExternFunctionCall(name, ins, intvs, outt) if ins.exists({
      case CGenExpr(_, SrcViewType()) => true
      case _ => false
    }) => ExternFunctionCall(name, ins.map({
      case ce@CGenExpr(_, SrcViewType()) => M(ce)
      case other => other
    }), intvs, outt)
  })

  def checkInvalidPass(expr: Expr): Boolean = {
    expr.visit({
      case CGenExpr(args, DestViewType()) if args.exists({
        case CGenExpr(_, SrcViewType()) => true
        case ParamUse(_) => true
        case _ => false
      }) => return true
      case _ =>
    })
    return false
  }

  // after this pass, no primitive should have a do-not-care-view type
  def DetermineViewTypePass(n: ShirIR)(implicit hasReachedEager: Boolean = false): ShirIR = {
    n match {
      case Mtrl(in) => M(DetermineViewTypePass(in).asInstanceOf[Expr])
      //      case map@Map(f, in, _, _) if map.rightmostViewType.isInstanceOf[DontCareLazyTypeT] =>
      //        Map(DetermineViewTypePass(f).asInstanceOf[Expr], DetermineViewTypePass(in)(hasReachedEager = true).asInstanceOf[Expr])
      case map@Map(CLambda(p, b, _), in, _, _) if map.rightmostViewType.isInstanceOf[NoEffectTypeT] =>
        if (hasReachedEager) { // by default is src view
          Map(CLambda(p, DetermineViewTypePass(b)(hasReachedEager = true).asInstanceOf[Expr]), DetermineViewTypePass(in)(hasReachedEager = true).asInstanceOf[Expr], SrcViewType())
        } else {
          // have not meet eager yet. it will be set as an eager.
          // it is danger to treat a map as a dest view when not knowing if it has a inverse function
          // thus, if a user needs a dest map, they need to specify it
          Map(CLambda(p, DetermineViewTypePass(b)(hasReachedEager = false).asInstanceOf[Expr]), DetermineViewTypePass(in)(hasReachedEager = true).asInstanceOf[Expr], EagerType())
        }
      case build@Build(CLambda(p, b, _), n, _, _) if build.rightmostViewType.isInstanceOf[NoEffectTypeT] =>
        if (hasReachedEager) Build(CLambda(p, DetermineViewTypePass(b)(hasReachedEager = true).asInstanceOf[Expr]), n, SrcViewType())
        else Build(CLambda(p, DetermineViewTypePass(b)(hasReachedEager = false).asInstanceOf[Expr]), n, EagerType())
      case CGenLet(p, b, a, _) => CGenLet(p, DetermineViewTypePass(b).asInstanceOf[Expr], DetermineViewTypePass(a)(hasReachedEager = false).asInstanceOf[Expr])
      case ce: CGenExpr => ce.rightmostViewType match {
        case _: NoEffectTypeT => ce.toViewType(autoVt).map(DetermineViewTypePass)
        case _: EagerTypeT => ce.map(c => DetermineViewTypePass(c)(hasReachedEager = true))
        case _ => ce.map(DetermineViewTypePass)
      }
      case CLambda(p, b, _) => CLambda(p, DetermineViewTypePass(b)(hasReachedEager = false).asInstanceOf[Expr]) // need to rebuild CLambda, make sure it have the correct view type
      case other => other.map(DetermineViewTypePass)
    }
  }

  def checkNoDoNotCareLazyType(n: ShirIR): Unit = n.visit {
    case Alloc(_, _, _) => // it has a do-not-care-type, but we really do not care about it
    case TypeConv(_, _) => // it has a do-not-care-type, but we really do not care about it
    case ce: CGenExpr => if (ce.rightmostViewType.isInstanceOf[NoEffectTypeT]) {
      throw new Exception(s"${
        ce.toShortString
      } has not determined do-not-care-type")
    }
    case _ =>
  }

  def autoVt(implicit hasReachedEager: Boolean): LazyTypeT = {
    if (hasReachedEager) SrcViewType()
    else DestViewType()
  }
}
