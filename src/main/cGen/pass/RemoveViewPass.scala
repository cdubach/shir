package cGen.pass

import cGen.Lib.tt
import cGen._
import cGen.primitive.{Dummy, Fst, FstDest, Idx, IdxDest, IdxS, Nowhere, SelectS, Snd, SndDest, UnFstDest, UnIdxDest, UnSndDest, Unrepeat}
import core.{Expr, ParamDef, ParamUse, TypeChecker}
import cGen.pass.MemoryPass._
import cGen.pass.CommonPass._
import lift.arithmetic.Cst

object RemoveViewPass {
  def RemoveViewPass(): RebuildPass = RebuildPass("RemoveViewPass", {
    case SelectS(CGenLet(p, b, a, _), which, _) => CGenLet(p, SelectS(b, which), a)
    case IdxS(CGenLet(p, b, a, _), idx, _) => CGenLet(p, IdxS(b, idx), a)
    case SelectS(SIf(cond, b0, b1, _), which, _) => SIf(cond, SelectS(b0, which), SelectS(b1, which))
    case IdxS(SIf(cond, b0, b1, _), idx, _) => SIf(cond, IdxS(b0, idx), IdxS(b1, idx))
    case IdxS(b@Build(idxf, _, _, _), idx, _) if b.isSrcView => CFC(idxf, idx) // fuse when build is lazy
    case Idx(b@Build(idxf, _, _, _), idx, DestViewType(), _) if b.isEager => CFC(idxf, idx) // fuse when build is lazy

    case Fst(Tuple(in1, _, _), SrcViewType(), _) => in1
    case Snd(Tuple(_, in2, _), SrcViewType(), _) => in2

    // Rmeove Tuple as DestView
    case FstDest(UnFstDest(in, _, _), _) => in
    case SndDest(UnFstDest(_, t, _), _) => Nowhere(AddressType(t))
    case SndDest(UnSndDest(in, _, _), _) => in
    case FstDest(UnSndDest(_, t, _), _) => Nowhere(AddressType(t))

    case FstDest(DestTuple(in1, _, _), _) => in1
    case SndDest(DestTuple(_, in2, _), _) => in2

    case IdxDest(DestBuild(idxf, _, _), idx, _) => CFC(idxf, idx)

    case Assign(_, Nowhere(_), _) => Dummy()

    case IdxDest(Unrepeat(d, _), idx, _) =>
      val k = ParamDef(IntType(32))
      val n = TypeChecker.check(d).t.asInstanceOf[AddressType].ptrType.asInstanceOf[ArrayTypeT].len
      Unrepeat(DestBuild(CLambda(k, IdxDest(IdxDest(d, ParamUse(k)), idx)), n))
    case Assign(s, Unrepeat(d, _), _) =>
      val n = TypeChecker.check(d).t.asInstanceOf[AddressType].ptrType.asInstanceOf[ArrayTypeT].len
      val i = ParamDef(IntType(32))
      CGenLet(ParamDef(VoidType()), Assign(s, IdxDest(d, Constant(0, IntType(32))))) apply // write to d[0] first
        DBuild(CLambda(i, // d[0] write to d[1]...d[n]
          Assign(ValueAt(IdxDest(d, Constant(0, IntType(32)))),
            IdxDest(d, DPS(BinaryOp(ParamUse(i), Constant(1, IntType(32)), AlgoOp.Add), None)))), n.ae.evalInt - 1)

    case DBuild(CLambda(i, body, _), _, _, _) if {
      exists(body, { case IdxDest(UnIdxDest(_, p, _, _), ParamUse(q), _) if i.id == q.id => true }) &&
        count(body, { case ParamUse(pd) if pd.id == i.id => true }) <= 2 // i only used two times, (in one assignment, it will be appear twice in the source and the dest
    } =>
      var v0: Expr = Dummy()
      body.visit({ case IdxDest(UnIdxDest(_, p, _, _), ParamUse(q), _) if i.id == q.id => v0 = p; case _ => }) // v0 must be redefined here
      replace(body, { case ParamUse(pd) if pd.id == i.id => v0 })
    case IdxDest(UnIdxDest(d, ParamUse(p), _, _), ParamUse(q), _) if p.id == q.id => d // Remove UnIdxDest
  })

  /** return remove "some" of the dummy, still need the codegen to handle it probably
   */
  def RemoveDummyPass(): RebuildPass = RebuildPass("RemoveDummyPass", {
    case CGenLet(_, b, Dummy(_), _) => b
    case CGenLet(ParamDef(_: VoidTypeT, _), Dummy(_), a, _) => a // a has side effect
  })
}

