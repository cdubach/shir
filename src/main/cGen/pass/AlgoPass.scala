package cGen.pass

import cGen._
import cGen.primitive.{Add, AddExpr, SelectS}
import core.{Expr, ShirIR, ParamDef, ParamUse, TypeChecker}

object AlgoPass {
  def BinaryPass(): RebuildPass = RebuildPass("BinaryPass", {
    case Add(Tuple(in1, in2, _), _) => BinaryOp(in1, in2, AlgoOp.Add)
    case add: AddExpr => BinaryOp(SelectS(add.args.head, 0), SelectS(add.args.head, 1), AlgoOp.Add)
    case Sub(Tuple(in1, in2, _), _) => BinaryOp(in1, in2, AlgoOp.Sub)
    case sub: SubExpr => BinaryOp(SelectS(sub.args.head, 0), SelectS(sub.args.head, 1), AlgoOp.Sub)
    case Mul(Tuple(in1, in2, _), _) => BinaryOp(in1, in2, AlgoOp.Mul)
    case mul: MulExpr => BinaryOp(SelectS(mul.args.head, 0), SelectS(mul.args.head, 1), AlgoOp.Mul)
  })
}
