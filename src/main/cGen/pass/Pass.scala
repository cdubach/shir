package cGen.pass

import cGen.Util.{formatTime, tic, toc}
import core.util.Log
import core.{Expr, ShirIR, TypeChecker}

import scala.annotation.tailrec

trait Pass {
  val name: String = this.getClass.getSimpleName
  val isFixPoint: Boolean = true
  val isMuted: Boolean

  var rebuildTime: Long = 0
  var rebuildNum: Int = 0
  var rewriteTime: Long = 0
  var rewriteNum: Int = 0

  def apply(e: Expr, _isFixPoint: Boolean = this.isFixPoint): Expr = {
    if (!isMuted) Log.info(this, s"Apply Rule $name")
    var newE = e
    var done = true
    var iter = 0
    while (done) {
      val oldE = newE
      iter += 1
      val before = tic()
      newE = run(newE)
      if (!isMuted) Log.info(this, s"$name (iter $iter) finish takes ${formatTime(toc(before))}")
      if (!isFixPoint || newE == oldE) done = false
    }
    if (!isMuted) Log.info(this, summary())
    newE
  }

  def run(e: Expr): Expr

  def summary(): String = s"[$name] rebuild: ${formatTime(rebuildTime)}, $rebuildNum times, rewrite: ${formatTime(rewriteTime)}, $rewriteNum times"

  def resetStat(): Unit = {
    rebuildTime = 0
    rebuildNum = 0
    rewriteTime = 0
    rewriteNum = 0
  }
}

object OrchestratePass {
  /**
   * orchestrate determines whether expr is written by comparing with "=="
   */
  @tailrec
  def apply(expr: Expr, passes: Pass*): Expr = {
    val rewritten = passes.foldRight(expr)((pass, e) => pass(e))
    if (rewritten != expr) {
      passes.foreach(_.resetStat())
      apply(rewritten, passes: _*)
    }
    else rewritten
  }

  @tailrec
  def apply2(expr: Expr, passes: (Expr => Expr)*): Expr = {
    val rewritten = passes.foldRight(expr)((pass, e) => pass(e))
    if (rewritten != expr) apply2(rewritten, passes: _*)
    else rewritten
  }

}

case class RebuildPass(override val name: String,
                       rewrite: PartialFunction[Expr, Expr],
                       override val isFixPoint: Boolean = true,
                       isTypeCheck: Boolean = false,
                       override val isMuted: Boolean = true
                      ) extends Pass {
  def isDefinedAt(e: Expr): Boolean = rewrite.isDefinedAt(e)

  def rewriting(node: ShirIR): (ShirIR, Boolean) = node match {
    case e: Expr if isDefinedAt(e) =>
      val before = tic()
      val r = rewrite(e)
      rewriteTime += toc(before)
      rewriteNum += 1
      if (!isMuted) Log.info(this, s"$name(${formatTime(toc(before))}): $e => $r")
      (r, true)
    case other => (other, false)
  }

  // a better version of visitAndRebuild
  // only build when rewrite happens
  def visitAndRebuild(e: ShirIR): (ShirIR, Boolean) = {
    val children: Seq[(ShirIR, Boolean)] = e.children.map(visitAndRebuild)
    if (children.exists(_._2)) { // indicates that there is a rewrite on one of the children
      val before = tic()
      val newE = e.build(newChildren = children.map(_._1))
      rebuildTime += toc(before)
      rebuildNum += 1
      (rewriting(newE)._1, true) // the tree branch all the way up to the root needed to be rewritten
    }
    else rewriting(e) // no write on its children or no child
  }

  def run(e: Expr): Expr = {
    var tree = e
    if (isTypeCheck) {
      val before = tic()
      if (!isMuted) Log.info(this, "[TypeCheck] begins")
      try {
        tree = TypeChecker.check(tree)
      } catch {
        case other: Throwable => throw other
      }
      if (!isMuted) Log.info(this, s"[TypeCheck] takes ${formatTime(toc(before))}")
    }
    visitAndRebuild(tree)._1.asInstanceOf[Expr]
  }
}
