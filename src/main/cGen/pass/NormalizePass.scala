package cGen.pass

import cGen.Lib.Program
import cGen.pass.CommonPass.{RenameCGenExprToParamUsePass, hasUseParamDef}
import cGen._
import cGen.primitive.LoopInvariant
import core.{Expr, LambdaT, ParamDef, ParamUse}


object NormalizePass {
  def evalOrderIsNotExplicit(ce: CGenExpr) = ce match {
    case CGenLet(_, _, _, _) => false // CGenLet can also be a src/eager construct, rule it out
    case _ => {
      (ce.isSrcView || ce.isEager) && ce.args.exists({
        case child: CGenExpr => child.isEager || child.isDestView
        case _ => false
      })
    }
  }

  /**it reveals the explicit evaluation order at the functional level based on the view information.
   * it avoid the nesting of eager primitive.
   * For example, MapE(f, MapE(g)) => let x = MapE(g) in MapE(f, x).
   * By doing so, when lower into imperative constructs (Build), it will NOT become:
   * Build(i -> Build(j -> ..)[i]), where the innerloop will be executed numerous times.
   */
  def ExplicitEvalOrderNormalizePass(expr: Expr): Expr =
    RebuildPass("ExplicitEvalOrderNormalizePass", {
      case ce: CGenExpr if evalOrderIsNotExplicit(ce) =>
        val prog: Program = new Program()
        ce.args.foreach({
          case child: CGenExpr if child.isEager | child.isDestView => prog << child
          case other => other
        })
        var letBody: Expr = ce
        prog.pairs.foreach(p =>
          letBody = RenameCGenExprToParamUsePass(p._2.asInstanceOf[CGenExpr], ParamUse(p._1))(letBody)
        )
        prog.build(letBody) // let p = child in [child |-> p]ce
    })(expr)


  def LiftLetPass(expr: Expr): Expr = {
    RebuildPass("LiftLetPass", {
      case CGenLet(p, CGenLet(pin, bin, ain, _), a, _) if hasUseParamDef(pin, a) =>
        CGenLet(pin, CGenLet(p, bin, a), ain)
      case CGenLet(p, b, CGenLet(pin, bin, ain, _), _) =>
        CGenLet(pin, CGenLet(p, b, bin), ain)
      case ce: CGenExpr if {
        ce.args.exists({ case CGenLet(_, _, _, _) => true; case _ => false })
      } =>
        val prog: Program = new Program()
        val letBody = ce.build(ce.args.map({
          case CGenLet(p, b, a, _) =>
            prog.add(p, a)
            b
          case other => other
        }))
        prog.build(letBody)
    })(expr)
  }

  /**Transform into A-normal form
   */
  object ANF {
    def apply(expr: Expr): Expr = ANFPass(expr)

    def isComposed(expr: Expr): Boolean = expr match {
      case _: ParamDef => false
      case _: ParamUse => false
      case _: ConstantExpr => false
      case _: LambdaT => false
      case _ => true
    }

    def shouldBeANF(expr: Expr): Boolean = expr match {
      case db: DestBuildExpr => false
      case pd: ParamDef => false
      case pu: ParamUse => false
      case cs: ConstantExpr => false
      case alloc: AllocExpr => false
//      case va: ValueAtExpr => false
      case ioo: IdxOffsetOpExpr => false
      case CGenLet(_, _, _, _) => false
      case SIf(_, _, _, _) => false
      case DIf(_, _, _, _, _) => false
      case LoopInvariant(_, _) => false
      //        case Assign(_, _, _) => false // Assign(id/binop/unaryop, d) should be consider together
      case _ => true
    }

    def ANFPass(expr: Expr): Expr = {
      RebuildPass("ANFPass", {
        case CGenLet(p0, b0, CGenLet(p1, b1, a1, _), _) => CGenLet(p1, CGenLet(p0, b0, b1), a1)
        case ce: CGenExpr if ce.args.exists(isComposed) && shouldBeANF(ce) =>
          val prog = new Program()
          val letBody = ce.build(ce.args.map(arg => if (isComposed(arg)) prog << arg else arg))
          prog.build(letBody)
      })(expr)
    }
  }
}
