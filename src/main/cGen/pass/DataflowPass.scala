package cGen.pass

import cGen.AllocKind.Heap
import cGen.DPSPass.isScalar
import cGen._
import cGen.pass.NormalizePass.ANF
import cGen.primitive._
import core._

import scala.collection.mutable

object DataflowPass {

  def RemoveValueAtPass(): RebuildPass = RebuildPass("RemoveValueAtRule",
    rewrite = {
      case CGenLet(_, _: ValueAtExpr, a, _) => throw new Exception()
    },
    isFixPoint = false)

  // before using this rule, all alloc should be placed in the heap
  def ChangeAllocKindPass(): RebuildPass = RebuildPass("ChangeAllocKindRule", {
    case Alloc(t, Heap, _) if isScalar(t) => Alloc(t, AllocKind.Stack)
  })

  /**
   * @param expr      to insert free
   * @param isLetBody is used to check if we are in the tail of a let expression chain
   * @param toFree    is a seq of FreeExpr to be added to the end of the ANF branch
   * @return expr with free inserted
   */
  def InsertFree(expr: Expr, isLetBody: Boolean = false)(implicit toFree: Seq[FreeExpr] = Seq()): Expr = expr match {
    case CLambda(p, b, vt) => CLambda(p, InsertFree(b, isLetBody = true), vt)
    case CGenLet(p, b, a: AllocExpr, _) => {
      if (Debug.PROFILE >= 2) println(s"[Free] found $p $a, ${a.ak} ${a.t}")
      CGenLet(p, InsertFree(b, isLetBody = true)(toFree :+ Free(ParamUse(p), a.ak)), a)
    }
    case CGenLet(p, b, a, _) => CGenLet(p, InsertFree(b, true), InsertFree(a)(Seq()))
    case DBuild(idxf, n, ltt, _) => {
      val init: Expr = DBuild(InsertFree(idxf)(Seq()), n, ltt)
      if (isLetBody) toFree.foldRight(init)(CGenLet(ParamDef(VoidType()), _, _))
      else init
    }
    case DIfold(idxf, init, dest, ltt, _) =>
      val _init: Expr = DIfold(InsertFree(idxf)(Seq()), InsertFree(init)(Seq()), InsertFree(dest)(Seq()), ltt)
      if (isLetBody) toFree.foldRight(_init)(CGenLet(ParamDef(VoidType()), _, _))
      else _init
    case DIf(computeCond, cond, branch0, branch1, _) =>
      val a = TypeChecker.check(InsertFree(computeCond)(Seq()))
      val b = TypeChecker.check(InsertFree(cond)(Seq()))
      val c = TypeChecker.check(InsertFree(branch0)(Seq()))
      val d = TypeChecker.check(InsertFree(branch1)(Seq()))
      val init: Expr = TypeChecker.check(DIf(a, b, c, returnVoidForInsertFree(d)))
      if (isLetBody) {
        val r = TypeChecker.check(toFree.foldRight(init)(CGenLet(ParamDef(VoidType()), _, _)))
        r
      }
      else init
    case Loop(f, m, hc, _) =>
      val init: Expr = Loop(InsertFree(f)(Seq()), m, hc)
      if (isLetBody) toFree.foldRight(init)(CGenLet(ParamDef(VoidType()), _, _))
      else init
    case other =>
      if (Debug.PROFILE >= 2 && toFree.nonEmpty) println(s"[Free] $toFree ${toFree.map(_.ak)}")
      if (isLetBody) toFree.foldRight(other)(CGenLet(ParamDef(VoidType()), _, _))
      else other
  }

  // a specify workaround for insertfree DIf where one of the branch doesn't return void
  def returnVoidForInsertFree(expr: Expr): Expr = {
    var modified = true

    def helper(e: Expr): Expr = e match {
      case CGenLet(p, b@CGenLet(_, _, _, _), a, _) => CGenLet(p, helper(b), a)
      case CGenLet(p, va: ValueAtExpr, a, _) =>
        modified = true
        a
      case CGenLet(p, da: IdxDestExpr, a, _) =>
        modified = true
        a
      case other => other
    }

    var r = expr
    while (modified) {
      modified = false
      r = helper(r)
//      println(modified)
    }
    r
  }

  /** A fix point algorithm for loop invariant motion.
   * It requires the input IR in strict ANF. Thus, need to use with ANFLiftingRule
   * since HoistPass may sabotage the full ANF structure
   */
  object Hoist {
    def apply(expr: Expr): Expr = HoistPass(expr)

    def HoistPass(expr: Expr): Expr = {
      var modified = true
      var cur = expr
      while (modified) {
        modified = false
        cur = ANF(RebuildPass("HoistPass", {
          case DBuild(CLambda(idx, e, _), n, ltt, _) =>
            val hoisted = mutable.ArrayBuffer[(ParamDef, Expr)]() // holds whatever is free in the build function body
            val boundVars = mutable.Set[Long](idx.id) // hold whatever variable that is bounded (related to the idx)
            findBoundVars(e)(boundVars, mutable.Map()) // collects bound vars and stores in boundVars
            val rest = findFree(e)(boundVars, hoisted) // find all free exprs and stores in hoisted
//            println(hoisted)
            if (!hoisted.isEmpty) modified = true
            hoisted.foldRight(DBuild(CLambda(idx, rest), n, ltt): Expr)((pairs, r) => CGenLet(pairs._1, r, pairs._2))
          case DIfold(CLambda(idx, CLambda(acc, e, _), _), init, d, n, _) =>
            val hoisted = mutable.ArrayBuffer[(ParamDef, Expr)]()
            val boundVars = mutable.Set[Long](idx.id, acc.id)
            findBoundVars(e)(boundVars, mutable.Map())
            val rest = findFree(e)(boundVars, hoisted)
            if (!hoisted.isEmpty) modified = true
            hoisted.foldRight(DIfold(CLambda(idx, CLambda(acc, rest)), init, d, n): Expr)((pairs, r) => CGenLet(pairs._1, r, pairs._2))
        }, isFixPoint = false)(cur))
      }
      cur
    }

    def findFree(e: Expr)(implicit boundVars: mutable.Set[Long], hoisted: mutable.ArrayBuffer[(ParamDef, Expr)]): Expr = e match {
      case CGenLet(p, b, a, _) if isFree(a) => hoisted.append((p, a)); findFree(b)
      case CGenLet(p, b, a, _) => a match {
        case lp@Loop(f, m, hc, _) if hc > 0 =>
          hoisted.append((p, Loop(f, m, hc - 1))); findFree(b)
        case _ => CGenLet(p, findFree(b), a)
      }
      case other => other // TODO: Handle the last one
    }

    def findBoundVars(node: ShirIR)(implicit boundVars: mutable.Set[Long], letParamArgMap: mutable.Map[Long, Expr]): Unit = {
      node match {
        case CGenLet(p, _, a, _) if !isFree(a)(boundVars) =>
          boundVars += p.id
          letParamArgMap += ((p.id, a))
          defineAt(a)
        case _ =>
      }
      node.children.foreach(findBoundVars(_))
    }

    /** Check is e is free from the given boundvars */
    def isFree(e: Expr)(implicit boundVars: mutable.Set[Long]): Boolean = {
      e.visit({
        case pd: ParamDef if boundVars.contains(pd.id) => return false
        case _ =>
      })
      return true
    }

    def defineAt(node: ShirIR)(implicit boundVars: mutable.Set[Long], letParamArgMap: mutable.Map[Long, Expr]): Unit = {
      node.visit({
        case CGenLet(p, _, a, _) => {
          boundVars += p.id
          letParamArgMap += ((p.id, a))
        }
        case Assign(_, ParamUse(pu), _) => letParamArgMap.get(pu.id) match {
          case Some(IdxDest(d: ParamUse, _, _)) => boundVars += pu.id += d.p.id
          case Some(FstDest(d: ParamUse, _)) => boundVars += pu.id += d.p.id
          case Some(SndDest(d: ParamUse, _)) => boundVars += pu.id += d.p.id
          case Some(Alloc(_, _, _)) => boundVars += pu.id
          case None => boundVars += pu.id
        }
        case DExternFunctionCall(_, ins, _, _) => boundVars += ins.head.asInstanceOf[ParamUse].p.id
        case _ =>
      })
    }
  }

  /**
   * rules for hoisting LoopInvariantExpr out of build and ifold
   */
  object Hoist2 {
    def HoistLoopInvariantPass(): RebuildPass = RebuildPass("HoistLoopInvariantPass", {
      case CGenLet(p, CGenLet(p1, b1, li: LoopInvariantExpr, _), a, _) if !a.isInstanceOf[LoopInvariantExpr] =>
        CGenLet(p1, CGenLet(p, b1, a), li)
      case DBuild(CLambda(p, CGenLet(p0, b, li: LoopInvariantExpr, _), _), n, vt, _) =>
        CGenLet(p0, DBuild(CLambda(p, b), n, vt), li)
      case DIfold(CLambda(idx, CLambda(acc, CGenLet(p, b, li: LoopInvariantExpr, _), _), _), init, d, n, _) =>
        CGenLet(p, DIfold(CLambda(idx, CLambda(acc, b)), init, d, n), li)
    })

    def RemoveLoopInvariantPass(): RebuildPass = RebuildPass("RemoveLoopInvariantPass", { case LoopInvariant(in, _) => in })

    def apply(expr: Expr): Expr = {
      val hoisted = OrchestratePass.apply2(expr, e => HoistLoopInvariantPass()(e), ANF.apply)
      ANF(RemoveLoopInvariantPass()(hoisted))
    }
  }
}
