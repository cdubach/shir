package cGen.pass

import cGen.{CFC, CFCExpr, CGenExpr, CGenLet, CLambda, EagerType, SrcViewType, VoidTypeT}
import core.util.IRDotGraph
import core.{Expr, LambdaT, ParamDef, ParamUse}

import scala.collection.mutable

object CommonPass {
  def BetaReduction(f: LambdaT, arg: Expr): Expr = RenameParamDefToExprPass(f.param, arg, isMuted = true)(f.body)
  def BetaReduction(cfc: CFCExpr): Expr = cfc match {
    case CFC(f, arg, _) => BetaReduction(f.asInstanceOf[LambdaT], arg)
  }

  def BetaReductionPass(): RebuildPass = RebuildPass("BetaReductionPass", {
    case c@CFC(f: LambdaT, arg, _) if !c.isLet => RenameParamDefToExprPass(f.param, arg, "BetaReductionRename")(f.body)
  })

  def RenameParamDefToExprPass(pd: ParamDef, target: Expr, name: String = "RenamePdRebuilder", isMuted: Boolean = true): RebuildPass = RebuildPass(name, {
    case ParamUse(cpd) if cpd.id == pd.id => target
  }, isFixPoint = false, isMuted=isMuted)

  def RenameCGenExprToParamUsePass(from: CGenExpr, to: ParamUse, name: String = "RenameExprToParamUsePass", isMuted: Boolean = true): RebuildPass = RebuildPass(name, {
    case ce: CGenExpr if ce.id == from.id => to
  }, isFixPoint = false, isMuted=isMuted)

  def hasUseParamDef(pd: ParamDef, inside: Expr): Boolean = {
    inside.visit({
      case ParamUse(pdd) if pdd.id == pd.id => return true
      case _ =>
    })
    return false
  }

  // let a = b in c, but a is never used in a
  def DeadLetRemovalPass(): RebuildPass = {
    RebuildPass("DeadLetRemovalPass", {
      case CGenLet(p@ParamDef(t, _), b, a, _) if !hasUseParamDef(p, b) && !t.isInstanceOf[VoidTypeT] => b // when p is voidT, it means arg may have side-effect
    })
  }

  def hasEager(expr: Expr): Boolean = {
    expr.visit({
      case CGenExpr(_, EagerType()) => return true
      case _ =>
    })
    return false
  }

  def hasSrcView(expr: Expr): Boolean = {
    expr.visit({
      case CGenExpr(_, SrcViewType()) => return true
      case _ =>
    })
    return false
  }

  def countEager(expr: Expr): Int = {
    var count = 0
    expr.visit({
      case CGenExpr(_, EagerType()) => count += 1
      case _ =>
    })
    return count
  }

  def exists(in: Expr, m: PartialFunction[Expr, Boolean]): Boolean = {
    in.visit({
      case e: Expr => if (m.isDefinedAt(e)) return true
      case _ =>
    })
    return false
  }

  def replace(in: Expr, m: PartialFunction[Expr, Expr]): Expr = {
    RebuildPass("replacePass", m)(in)
  }

  def count(in: Expr, m: PartialFunction[Expr, Boolean]): Int = {
    var counter = 0
    in.visit({
      case e: Expr => if (m.isDefinedAt(e)) counter += 1
      case _ =>
    })
    return counter
  }
}
