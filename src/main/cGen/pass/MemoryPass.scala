package cGen.pass

import cGen.AllocKind.{Heap, Stack}
import cGen.DPSPass.isScalar
import cGen.Lib.{Program, quickTypeCheck, tt}
import cGen._
import cGen.idioms.TypeArith
import cGen.pass.CommonPass.BetaReduction
import cGen.pass.MemoryPass.DPS
import cGen.primitive._
import core._

object MemoryPass {
  private val ACCUMULATE_SCALAR_IN_VAR = false

  /**
   * @param expr
   * @return lowered program, map, zip, split => build, reduce => ifold
   */
  def LowerSrcViewAndEagerPass(expr: Expr): Expr = RebuildPass("LowerSrcViewAndEagerPass", {
    case Reduce(f, init, in, _) =>
      val u = ParamDef(init.t)
      val idx = ParamDef(IntType(32))
      val body = BetaReduction(CFC(BetaReduction(CFC(f, ParamUse(u))), IdxS(in, ParamUse(idx))))
      val idxf = CLambda(idx, CLambda(u, body))
      Ifold(idxf, init, TypeChecker.check(in).t.asInstanceOf[ArrayTypeT].len)
    case map@Map(f, in, _, _) if map.isEager || map.isSrcView =>
      val idx = ParamDef(IntType(32))
      val idxf = CLambda(idx, BetaReduction(CFC(f, IdxS(in, ParamUse(idx)))))
      Build(idxf, TypeChecker.check(map).t.asInstanceOf[ArrayTypeT].len, map.rightmostViewType)
    case zip@Zip(in, _) if zip.isSrcView =>
      val idx = ParamDef(IntType(32))
      val _in = TypeChecker.check(in)
      val idxf = CLambda(idx, Tuple(IdxS(SelectS(_in, 0), ParamUse(idx)), IdxS(SelectS(_in, 1), ParamUse(idx)), SrcViewType()))
      val n = TypeChecker.check(zip).t.asInstanceOf[ArrayTypeT].len
      Build(idxf, n, SrcViewType())
    case permute@Permute(f, in, _) if permute.isSrcView =>
      val idx = ParamDef(IntType(32))
      val idxf = CLambda(idx, IdxS(in, BetaReduction(CFC(f, ParamUse(idx)))))
      Build(idxf, TypeChecker.check(in).t.asInstanceOf[ArrayTypeT].len, SrcViewType())
    case slice@Slice(in, start, m, _) if slice.isSrcView =>
      val idx = ParamDef(IntType(32))
      val idxf = CLambda(idx, IdxS(in, BinaryOp(ParamUse(idx), start, AlgoOp.Add)))
      Build(idxf, m, SrcViewType())
    case slide@Slide(in, windowWidth, outt) if slide.isSrcView =>
      val i = ParamDef(IntType(32))
      val j = ParamDef(IntType(32))
      Build(CLambda(i,
        Build(CLambda(j, IdxS(in, BinaryOp(ParamUse(j), ParamUse(i), AlgoOp.Add)))
          , windowWidth, SrcViewType())
      ), outt.asInstanceOf[ArrayTypeT].len, SrcViewType())
    case Range(start, step, n, _) =>
      val idx = ParamDef(IntType(32))
      val idxf = CLambda(idx, BinaryOp(start, BinaryOp(ParamUse(idx), step, AlgoOp.Mul), AlgoOp.Add))
      Build(idxf, n, SrcViewType())
    case con@Concat(in, _, _) if con.isSrcView =>
      val idx = ParamDef(IntType(32))
      val _in = TypeChecker.check(in)
      val len0 = _in.t.asInstanceOf[NamedTupleStructType].fst.asInstanceOf[ArrayTypeT].len
      val len1 = _in.t.asInstanceOf[NamedTupleStructType].snd.asInstanceOf[ArrayTypeT].len
      Build(CLambda(idx,
        If(BinaryOp(ParamUse(idx), Constant(len1.ae.evalInt, IntType(32)), AlgoOp.Lt, isBool = true),
          IdxS(Fst(_in, SrcViewType()), ParamUse(idx)),
          IdxS(Snd(_in, SrcViewType()), ParamUse(idx)),
          SrcViewType())
      ), len0.ae + len1.ae, SrcViewType())
    case join@Join(in, _, _) if join.isSrcView =>
      val idx = ParamDef(IntType(32))
      val m = TypeChecker.check(in).t.asInstanceOf[ArrayTypeT].len
      val n = TypeChecker.check(in).t.asInstanceOf[ArrayTypeT].et.asInstanceOf[ArrayTypeT].len
      val mm = Constant(m.ae.evalInt, IntType(32))
      val nn = Constant(n.ae.evalInt, IntType(32))
      val idxf = CLambda(idx, IdxS(IdxS(in, BinaryOp(ParamUse(idx), mm, AlgoOp.Div)), BinaryOp(ParamUse(idx), nn, AlgoOp.Mod)))
      Build(idxf, m.ae.evalInt * n.ae.evalInt, SrcViewType())
    case split@Split(in, m, _, _) if split.isSrcView =>
      val i = ParamDef(IntType(32))
      val j = ParamDef(IntType(32))
      val n = TypeChecker.check(in).t.asInstanceOf[ArrayTypeT].len
      val cs = Constant(m.ae.evalInt, IntType(32))
      Build(CLambda(i,
        Build(CLambda(j, IdxS(in, BinaryOp(BinaryOp(ParamUse(i), cs, AlgoOp.Mul), ParamUse(j), AlgoOp.Add)), None), m, SrcViewType()))
        , n.ae.evalInt / m.ae.evalInt, SrcViewType())
    case trans@Transpose(in, _) if trans.isSrcView =>
      val i = ParamDef(IntType(32))
      val j = ParamDef(IntType(32))
      val m = TypeChecker.check(in).t.asInstanceOf[ArrayTypeT].len
      val n = TypeChecker.check(in).t.asInstanceOf[ArrayTypeT].et.asInstanceOf[ArrayTypeT].len
      Build(CLambda(i,
        Build(CLambda(j, IdxS(IdxS(in, ParamUse(j)), ParamUse(i)), None), m, SrcViewType())),
        n, SrcViewType())
    case repeat@Repeat(m, in, _) if repeat.isSrcView => {
      val idx = ParamDef(IntType(32))
      val idxf = CLambda(idx, in)
      Build(idxf, m, SrcViewType())
    }
  })(expr)

  /**
   * DPS transformation rewrites high-level primitives by index primitives(Build, Ifold) and
   * provides memory management in a functional way through the usage of DPS and
   * abstract memory primitives.  Also, it invokes D transformation when
   * destination views are inplace.
   *
   * DPS takes two arguments.  The first one is the expression that it is
   * traversing, and the second one is a destination.  It aims to write the result
   * from the first argument to the destination.
   */
  def DPS(src: Expr, dest: Option[Expr]): Expr = {
    (src, dest) match {
      case (CGenLet(p, b, a, _), _) => CGenLet(p, DPS(b, dest), DPS(a, None))
      case (CLambda(p, b: CLambda, _), _) => CLambda(p, DPS(b, dest))
      case (CLambda(p, b: CGenExpr, t), None) if t.viewType.isInstanceOf[EagerTypeT] || t.viewType.isInstanceOf[DestViewTypeT] =>
        val d = ParamDef(AddressType(b.t))
        CLambda(p, CLambda(d, DPS(b, Some(ParamUse(d)))))
      //      case (CLambda(p, b: CGenExpr, t), None) if t.viewType.isInstanceOf[SrcViewTypeT] => CLambda(p, DPS(b, dest))
      case (src: CGenExpr, None) if src.isEager || src.isDestView => // WHERE ALLOCS HAPPEN
        val d = ParamDef(AddressType(src.t))
        CGenLet(d, Alloc(src.t, Heap)) apply DPS(src, Some(ParamUse(d))).asInstanceOf[CGenExpr] >> ValueAt(ParamUse(d))
      case (ifold@Ifold(CLambda(idx, CLambda(u, body, _), _), init, n, t), Some(d)) if ACCUMULATE_SCALAR_IN_VAR && ifold.isEager && isScalar(t) =>
        // When we encounter an ifold with a scalar accumulator, it sometimes helps to allocate a separate memory
        // location for that accumulator. Doing so eliminates spurious memory round-trips through the destination
        // location. Had such round-trips not been preempted, they could have complicated C compiler's optimizer's job,
        // possibly resulting in poorer compiled executable performance.
        // Conversely, a separate accumulator introduces loop-carried dependencies which might inhibit, e.g.,
        // loop interchange.
        val acc = ParamDef(AddressType(t))
        val idxf = CLambda(idx, CLambda(u, DPS(body, Some(ParamUse(acc)))))
        val loweredFold = DIfold(idxf, DPS(LowerSrcViewAndEagerPass(Materialize(init)), Some(ParamUse(acc))), ParamUse(acc), n)
        CGenLet(acc, Alloc(t, Heap)) apply loweredFold >> Assign(ValueAt(ParamUse(acc)), d)
      case (ifold@Ifold(CLambda(idx, CLambda(u, body, _), _), init, n, _), Some(d)) if ifold.isEager =>
        val idxf = CLambda(idx, CLambda(u, DPS(body, Some(d))))
        DIfold(idxf, DPS(LowerSrcViewAndEagerPass(Materialize(init)), Some(d)), d, n)
      case (build@Build(CLambda(idx, b, _), n, _, _), Some(d)) if build.isEager =>
        DBuild(CLambda(idx, DPS(b, Some(IdxDest(d, ParamUse(idx))))), n, EagerType())
      case (build@Build(CLambda(idx, b, _), n, _, _), None) if build.isSrcView =>
        Build(CLambda(idx, DPS(b, None)), n, SrcViewType())
      case (tup@Tuple(in1, in2, _), None) if tup.isSrcView => Tuple(DPS(in1, None), DPS(in2, None), SrcViewType())
      case (IdxS(in, idx, _), None) => IdxS(DPS(in, None), DPS(idx, None))
      case (SelectS(in, which, _), None) => SelectS(DPS(in, None), which)
      case (con@If(cond, b0, b1, _), None) if con.isSrcView =>
        val condD = ParamDef(AddressType(cond.t))
        SIf(
          //          CGenLet(condD, DPS(cond, Some(ParamUse(condD))), Alloc(cond.t, Heap)),
          cond,
          DPS(b0, None),
          DPS(b1, None)
        )
      //      case (ce@CGenExpr(args, SrcViewType()), None) => ce.build(args.map(DPS(_, None)))
      case (rs@RollingSlide(in, w, SrcViewType(), outt), None) =>
        val prog = new Program()
        val i = ParamDef(IntType(32))
//        val j = ParamDef(IntType(32))
//        val slide = Build(CLambda(j, IdxS(in, BinaryOp(ParamUse(j), ParamUse(i), AlgoOp.Add))), w, SrcViewType())
        val buf = prog << LoopInvariant(Alloc(tt(rs).asInstanceOf[ArrayTypeT].et, Stack))
        prog << LoopInvariant(DPS(Materialize.lowered(IdxS(in, Constant(0, IntType(32)))), Some(IdxDest(buf, Constant(1, IntType(32)))))) // assuming that the windows size is 3
        prog << LoopInvariant(DPS(Materialize.lowered(IdxS(in, Constant(1, IntType(32)))), Some(IdxDest(buf, Constant(2, IntType(32)))))) // init the windows
        prog build Build(CLambda(i, {
          val prog1 = new Program()
          prog1 << DPS(Materialize.lowered(IdxS(ValueAt(buf), Constant(1, IntType(32)))), Some(IdxDest(buf, Constant(0, IntType(32)))))
          prog1 << DPS(Materialize.lowered(IdxS(ValueAt(buf), Constant(2, IntType(32)))), Some(IdxDest(buf, Constant(1, IntType(32)))))
          prog1 << DPS(Materialize.lowered(IdxS(in, BinaryOp(ParamUse(i), Constant(2, IntType(32)), AlgoOp.Add))), Some(IdxDest(buf, Constant(2, IntType(32)))))
          prog1 build ValueAt(buf)
          //          CGenLet(ParamDef(VoidType()), DPS(slide, Some(buf))) apply ValueAt(buf)
        }), outt.asInstanceOf[ArrayTypeT].len, SrcViewType())
      case (va@ValueAt(_, _), None) => va

      // ---- DEST VIEW
      case (con@If(cond, b0, b1, _), Some(d)) if con.isDestView =>
        val condD = ParamDef(AddressType(cond.t))
        DIf(
          CGenLet(condD, DPS(cond, Some(ParamUse(condD))), Alloc(cond.t, Heap)),
          ValueAt(ParamUse(condD)),
          DPS(b0, Some(d)),
          DPS(b1, Some(d)),
          DestViewType()
        )
      case (tuple@Tuple(in1, in2, _), Some(d)) if tuple.isDestView =>
        CGenLet(ParamDef(VoidType()), DPS(UnFst(in1, tt(in2).asInstanceOf[CGenDataTypeT]), Some(d))) apply
          DPS(UnSnd(in2, tt(in1).asInstanceOf[CGenDataTypeT]), Some(d))
      case (Idx(arr, i, DestViewType(), _), Some(d)) =>
        val prog = new Program()
        val ipu = prog << DPS(i, None)
        prog build DPS(arr, Some(getInverse(Idx(arr, ipu, DestViewType())).get(d)))
      case (pd: CGenExpr, Some(d)) if pd.isDestView && pd.args.length == 1 => DPS(pd.args.head, Some(getInverse(pd).get(d)))

      // ---- EAGER
      case (BinaryOp(in1, in2, op, isBool, _), Some(d)) => Assign(BinaryOp(DPS(in1, None), DPS(in2, None), op, isBool), d)
      case (Id(in, _), Some(d)) => Assign(Id(DPS(in, None)), d)
      case (UnaryOp(in, op, _), Some(d)) => Assign(UnaryOp(DPS(in, None), op), d)
      case (ExternFunctionCall(name, ins, intvs, _), Some(d)) =>
        val _d = TypeChecker.check(d)
        DExternFunctionCall(name, ins.map(in => DPS(in, None)).+:(d), intvs.+:(AddressTypeVar(_d.t.asInstanceOf[AddressType].ptrType)))

      // ---- Terminations
      case (ParamUse(_), None) => src
      case (Constant(_, _), None) => src
      case (constant@Constant(_, _), Some(d)) => Assign(Id(constant), d)
      case (TypeArith(_, _), None) => src
      case (constant@TypeArith(_, _), Some(d)) => Assign(Id(constant), d)
      case (TypeConv(in, toT), _) => TypeConv(DPS(in, dest), toT)

      // ---- OpenCL
      case (mapglb@MapGlb(f, in, _), Some(d)) =>
        val idx = ParamDef(IntType(32))
        val idxf = CLambda(idx, DPS(BetaReduction(CFC(f, IdxS(in, ParamUse(idx)))), Some(IdxDest(d, ParamUse(idx)))))
        BuildGlb(idxf, TypeChecker.check(mapglb).t.asInstanceOf[ArrayTypeT].len)

      case _ =>
        throw new MalformedExprException(
          s"""DPS does not handle d: $dest, s: ${src.toString}""" + s"${
            if (src.isInstanceOf[CGenExpr]) src.asInstanceOf[CGenExpr].rightmostViewType else ""
          }")
    }
  }

  def getInverse(e: Expr): Option[Expr => Expr] = getOneInverse(e)

  /** if an inverse function is found, return a function: d => InverseFunction(d)
   */
  def getOneInverse(e: Expr): Option[Expr => Expr] = e match {
    case Map(CLambda(p, b, _), in, _, _) => {
      getAllInverse(b)(Set(p.id)) match {
        case Some(inverse) =>
          val i = ParamDef(IntType(32))
          val n = TypeChecker.check(in).t.asInstanceOf[ArrayTypeT].len
          Some(d => DestBuild(CLambda(i, inverse(IdxDest(d, ParamUse(i)))), n))
        case None => None
      }
    }
    case Join(in, _, _) => Some(d => {
      val i = ParamDef(IntType(32))
      val j = ParamDef(IntType(32))
      val n = TypeChecker.check(in).t.asInstanceOf[ArrayTypeT].len // 2
      val m = TypeChecker.check(in).t.asInstanceOf[ArrayTypeT].et.asInstanceOf[ArrayTypeT].len // 4
      DestBuild(CLambda(i, DestBuild(CLambda(j,
        IdxDest(
          d,
          DPS(BinaryOp(
            BinaryOp(ParamUse(i), Constant(n.ae.evalInt, IntType(32)), AlgoOp.Mul),
            ParamUse(j), AlgoOp.Add), None))), m)), n)
    })
    case Transpose(_, _) => Some(d => {
      val i = ParamDef(IntType(32))
      val j = ParamDef(IntType(32))
      val m = TypeChecker.check(d).t.asInstanceOf[AddressType].ptrType.asInstanceOf[ArrayTypeT].len
      val n = TypeChecker.check(d).t.asInstanceOf[AddressType].ptrType.asInstanceOf[ArrayTypeT].et.asInstanceOf[ArrayTypeT].len
      DestBuild(CLambda(i, DestBuild(CLambda(j,
        IdxDest(IdxDest(d, ParamUse(j)), ParamUse(i))), m)), n)
    })
    case UnFst(_, _, _) => Some(d => FstDest(d))
    case UnSnd(_, _, _) => Some(d => SndDest(d))
    case Fst(in, _, _) => Some(d => UnFstDest(d, tt(in).asInstanceOf[NamedTupleStructType].fst))
    case Snd(in, _, _) => Some(d => UnSndDest(d, tt(in).asInstanceOf[CGenDataTypeT]))
    case Zip(in, _) => Some(d => { // d is an array of tuple
      val i = ParamDef(IntType(32))
      val j = ParamDef(IntType(32))
      val n = TypeChecker.check(in).t.asInstanceOf[NamedTupleStructType].fst.asInstanceOf[ArrayTypeT].len
      DestTuple(
        DestBuild(CLambda(i, FstDest(IdxDest(d, ParamUse(i)))), n),
        DestBuild(CLambda(j, SndDest(IdxDest(d, ParamUse(j)))), n)
      )
    })
    case Concat(in, _, _) => Some(d => {
      val i = ParamDef(IntType(32))
      val j = ParamDef(IntType(32))
      val n = TypeChecker.check(in).t.asInstanceOf[NamedTupleStructType].fst.asInstanceOf[ArrayTypeT].len
      val m = TypeChecker.check(in).t.asInstanceOf[NamedTupleStructType].snd.asInstanceOf[ArrayTypeT].len
      DestTuple(
        DestBuild(CLambda(i, IdxDest(d, ParamUse(i))), n),
        DestBuild(CLambda(j, IdxDest(d, DPS(BinaryOp(ParamUse(j), Constant(n.ae.evalInt, IntType(32)), AlgoOp.Add), None))), m)
      )
    })
    case Idx(arr, idx, _, _) => Some(d => UnIdxDest(d, idx, tt(arr).asInstanceOf[ArrayTypeT].len))
    case Repeat(_, in, _) => Some(d =>
      Unrepeat(d))
    case other => throw MalformedExprException(s"cannot inverse ${e.toString}")
  }

  /** get all inverse function for a composition of view primitives, used by MapD usually
   */
  def getAllInverse(e: Expr)(implicit terminators: Set[Long]): Option[Expr => Expr] = {
    /** helper function continue to dive in the $in$ expr, until reach terminators
     * apply the return function in a stack like manner
     */
    def helper(in: Expr)(implicit terminators: Set[Long]): Option[Expr => Expr] = {
      getAllInverse(in) match {
        case Some(k) => Some(d => k(getOneInverse(e).get(d)))
        case None => None
      }
    }

    e match {
      case ParamUse(pd) =>
        if (terminators.contains(pd.id)) Some(d => d)
        else None
      case Join(in, _, _) => helper(in)
      case Transpose(in, _) => helper(in)
      case Map(CLambda(pd, b, _), in, _, _) => helper(in)(terminators ++ Set(pd.id))
    }
  }

}
