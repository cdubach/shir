package cGen

import cGen.Debug.SHIRC_PATH
import cGen.Util.FileWriter
import core.util.Log
import core.{ArithType, Expr, ParamDef, Type}
import lift.arithmetic.Cst

import scala.collection.mutable
import scala.sys.process._


/** The old codegen has the potential problem of stackoverflow, since the input program is in ANF and the tree
 * will became linear (like a list) and very deep. Stackvoerflow may happen even when increasing the thread stack size to 1g.
 * Tail-recursion is difficult to be implemented since it is not in CPS. A linear version codegen seems to be a better option.
 */
object CCodeGen {
  def apply(program: Expr, funcName: String = "func0", platform: Platform = Linux64, moreIncludes: String = ""): CCode = {
    implicit val env: Env = new Env(None)
    val funDecls = CASTPass(program, env, platform)
    new CCode(includes(platform) + moreIncludes + cgen(CProgram(funDecls, env.structDecls))(platform, funcName), funcName)
  }

  def tryCCodeGen(program: Expr, funcName: String = "func0", platform: Platform = Linux64, moreIncludes: String = ""): Option[CCode] = {
    implicit val env: Env = new Env(None)
    try {
      val funDecls = CASTPass(program, env, platform)
      Some(new CCode(includes(platform) + moreIncludes + cgen(CProgram(funDecls, env.structDecls))(platform, funcName), funcName))
    } catch {
      case e: Throwable =>
        Log.error(this, e.toString)
        None
    }
  }

  type FtdLet = (Option[Expr], Expr) // one line of decomposed let

  private def includes(platform: Platform) = platform match {
    case Linux64 | FlatLinux64 => s"#include <stdlib.h>\n#include<stdio.h>\n#include <math.h>\n"
    case OpenCL64 => ""
  }


  def cgen(node: CAST)(implicit platform: Platform, funcName: String): String = node match {
    case CProgram(fds, sds) =>
      val sb = new StringBuilder()
      sds.foreach(x => sb.append(cgen(x)))
      fds.foreach(x => sb.append(cgen(x)))
      sb.toString()
    case CFunDecl(params, block, t) =>
      val sb = new StringBuilder()
      platform match {
        case Linux64 | FlatLinux64 =>
          sb.append(s"${cgen(t)} ${funcName} (")
          sb.append(params.map(vd => s"${cgen(vd)}").mkString(", "))
          sb.append(")\n")
          sb.append(cgen(block))
        case OpenCL64 =>
          sb.append(s"__kernel ${cgen(t)} ${funcName} (")
          sb.append(params.map(vd => s"__global ${cgen(vd)}").mkString(", "))
          sb.append(")\n")
          sb.append(cgen(block))
      }
      sb.toString()
    case CStructDecl(tst) =>
      val sb = new StringBuilder()
      sb.append(s"typedef struct DEF_${tst.name.toUpperCase()} {\n")
      tst.namedTypes.foreach(p => sb.append(s"${cgen(p._2)} ${p._1};\n"))
      sb.append(s"} ${tst.name};\n\n")
      sb.toString()
    case CBlock(vds, stmts) => {
      // TODO: gen the code for vd in cblock
      val sb = new StringBuilder()
      sb.append("{\n")
      ///vds.foreach { vd => sb.append(s"${cgen(vd.t)} ${cgen(vd)};\n") }
      vds.foreach { vd => sb.append(cgen(vd) + ";\n") }
      stmts.foreach {
        case CExprStmt(e) =>
          sb.append(s"${cgen(e)};\n")
        case other =>
          sb.append(cgen(other))
      }
      sb.append("}\n")
      sb.toString()
    }
    case CForloop(begin, end, idx, block) => {
      s"for (int ${cgen(CVarExpr(idx))} = ${cgen(begin)}; ${cgen(CVarExpr(idx))} < ${cgen(end)}; ${cgen(CVarExpr(idx))}++)\n" + cgen(block)
    }
    case CVarDecl(pd, false) => s"${cgen(pd.t)} var${pd.id}"
    case CVarDecl(pd@ParamDef(ArrayType(et, len), _), true) if platform.IS_FLATTEN => s"${cgen(et)} var${pd.id}[${len.ae.evalInt}]" // declare an array on stack and is_flatten
    case CVarExpr(vd) => s"var${vd.pd.id}"
    case CConstant(i, t) => t match {
      case BoolType() => i.toInt.toString
      case IntType(_, _) => i.toInt.toString
      case DoubleType() => "%.5f".format(i)
      case FloatType() => "%.5f".format(i)
    }
    case CAssign(lhs, rhs) =>
      s"${cgen(lhs)} = ${cgen(rhs)};\n"
    case CValueAt(n) => n.t match {
      case AddressType(pt) if pt.isInstanceOf[ArrayType] => s"${cgen(n)}"
      case _ => s"*(${cgen(n)})"
    }
    case CAddressOf(n) => s"&(${cgen(n)})"
    case CDestArrayAccess(dest, idx) => node.asInstanceOf[CExpr].t match {
      case AddressType(pt) if pt.isInstanceOf[ArrayType] => s"${cgen(dest)}[${cgen(idx)}]"
      case _ => s"&(${cgen(dest)}[${cgen(idx)}])"
    }
    case CBinOp(op, in1, in2) => op match {
      case AlgoOp.Div => s"(${cgen(in1)} / ${cgen(in2)})"
      case AlgoOp.Mod => s"(${cgen(in1)} % ${cgen(in2)})"
      case AlgoOp.Add => s"(${cgen(in1)} + ${cgen(in2)})"
      case AlgoOp.Sub => s"(${cgen(in1)} - ${cgen(in2)})"
      case AlgoOp.Mul => s"(${cgen(in1)} * ${cgen(in2)})"
      case AlgoOp.Pow => s"pow(${cgen(in1)}, ${cgen(in2)})"
      case AlgoOp.Gt => s"(${cgen(in1)} > ${cgen(in2)})"
      case AlgoOp.Lt => s"(${cgen(in1)} < ${cgen(in2)})"
      case AlgoOp.Ge => s"(${cgen(in1)} >= ${cgen(in2)})"
      case AlgoOp.Le => s"(${cgen(in1)} <= ${cgen(in2)})"
      case AlgoOp.Eq => s"(${cgen(in1)} == ${cgen(in2)})"
      case AlgoOp.Andand => s"(${cgen(in1)} && ${cgen(in2)})"
      case AlgoOp.RightShift => s"(${cgen(in1)} >> ${cgen(in2)})"
      case AlgoOp.LeftShift => s"(${cgen(in1)} << ${cgen(in2)})"
    }
    case CSingleOp(op, in) => op match {
      case AlgoOp.Minus => s"-(${cgen(in)})"
      case AlgoOp.Sin => s"sin(${cgen(in)})"
      case AlgoOp.Cos => s"cos(${cgen(in)})"
      case AlgoOp.Sqrt => s"sqrt(${cgen(in)})"
      case AlgoOp.Exp => s"exp(${cgen(in)})"
      case AlgoOp.Log => s"log(${cgen(in)})"
    }
    case CTypeConv(in, t) => s"(${cgen(t)}) (${cgen(in)})"
    case CArrayAccess(array, idx) => s"${cgen(array)}[${cgen(idx)}]"
    case CManyStmts(stmts) => {
      val sb = new StringBuilder()
      stmts.foreach(stmt => {
        if (stmt.isExpr) sb.append(s"${cgen(stmt)};\n")
        else sb.append(cgen(stmt))
      })
      sb.toString()
    }
    case CMalloc(tt) => s"malloc(${sizeof(tt)})"
    case CStaticAlloc(ArrayType(et, len)) => s""
    case CDummy() => ""
    case CExprStmt(e) => {
      s"${cgen(e)};\n"
    }
    case CIf(computeCond, cond, branch0, branch1) =>
      val sb = new StringBuilder()
      sb.append(cgen(computeCond))
      sb.append(s"if (${cgen(cond)}) {\n")
      sb.append(cgen(branch0))
      sb.append(s"}\nelse {\n")
      sb.append(cgen(branch1))
      sb.append(s"}\n")
      sb.toString()
    case CIfExpr(cond, branch0, branch1) =>
      s"(${cgen(cond)} ? ${cgen(branch0)} : ${cgen(branch1)})"
    case CFunctionCall(name, ins) =>
      s"$name(${ins.map(cgen).reverse.mkString(",")})"
    case CFree(in) =>
      s"free(${cgen(in)});\n"
    case s@CSelect0(in) => s"(${cgen(in)}).${s.name}"
    case s@CSelect1(in) => s"(${cgen(in)}).${s.name}"
    case CStructExpr(csd) => csd.tst.name
    // ----- OpenCL -----
    case CGetGlbId(dim) => s"get_global_id($dim)"
  }

  def cgen(t: Type)(implicit platform: Platform): String = t match {
    case v: VoidType => "void"
    case ArrayType(dt@ArrayType(_, _), _) => {
      if (platform.IS_FLATTEN) s"${cgen(dt)}" // avoid adding another pointer sign "*"
      else s"${cgen(dt)}*"
    }
    case ArrayType(dt, _) => s"${cgen(dt)}*"
    case IntType(ArithType(Cst(32)), ArithType(Cst(0))) => "int"
    case IntType(ArithType(Cst(32)), ArithType(Cst(1))) => "unsigned int"
    case IntType(ArithType(Cst(width)), ArithType(Cst(0))) => platform.nameIntType(width.toInt)
    case IntType(ArithType(Cst(width)), ArithType(Cst(1))) => platform.nameUIntType(width.toInt)
    case DoubleType() => "double"
    case FloatType() => "float"
    case CharType() => "char"
    case AddressType(pt) => pt match {
      // in C the pointer type to an array(dt) should the type of *dt, thus no * in the front of it
      case at: ArrayType => s"${cgen(at)}"
      case _ => s"${cgen(pt)}*"
    }
    case BoolType() => "int"
    case tst@NamedTupleStructType(_, _) => tst.name
    case _ =>
      throw new Exception(s"Unknown Type ${t}")
  }

  def sizeof(t: Type)(implicit platform: Platform): String = t match {
    case ArrayType(dt, len) if platform.IS_FLATTEN => s"${sizeof(dt)} * ${len.ae.evalInt}"
    case ArrayType(dt, len) if !platform.IS_FLATTEN => s"sizeof(${cgen(dt)}) * ${len.ae.evalInt}"
    case IntType(ArithType(Cst(32)), ArithType(Cst(0))) => "sizeof(int)"
    case IntType(ArithType(Cst(32)), ArithType(Cst(1))) => "sizeof(unsigned int)"
    case IntType(ArithType(Cst(width)), ArithType(Cst(0))) => s"sizeof(${platform.nameIntType(width.toInt)})"
    case IntType(ArithType(Cst(width)), ArithType(Cst(1))) => s"sizeof(${platform.nameUIntType(width.toInt)})"
    case DoubleType() => "sizeof(double)"
    case BoolType() => "sizeof(int)"
    case ntst@NamedTupleStructType(_, _) => s"sizeof(${ntst.name})"
    case _ => throw new Exception(s"Unknown Type ${t}")
  }
}

/** A function defines a block
 * blocks can be nested
 * each block defines a Env
 */
class Env(parent: Option[Env]) {
  private var cvds: Seq[CVarDecl] = Seq() // use to generate variable definition in block
  var cvdIds: mutable.Set[Long] = mutable.Set()
  var cvdFuncIds: mutable.Set[Long] = mutable.Set() // parameters of a function
  val pdvdMap: mutable.Map[Long, CVarDecl] = parent match {
    case Some(e) => e.pdvdMap
    case None => mutable.Map() // map from paramDef to varDecl
  }
  val pdCExprMap: mutable.Map[Long, CExpr] = parent match { // map from paramDef/Use id to a AST CExpr
    case Some(e) => e.pdCExprMap // only one pdCExprMap exists, if there is a parent
    case None => mutable.Map()
  }
  val structDecls: mutable.Set[CStructDecl] = parent match {
    case Some(e) => e.structDecls // only one pdCExprMap exists, if there is a parent
    case None => mutable.Set()
  }

  def isPresent(cvd: CVarDecl): Boolean =
    if (cvdIds.contains(cvd.pd.id) || cvdFuncIds.contains(cvd.pd.id)) true
    else {
      parent match {
        case Some(e) => e.isPresent(cvd)
        case None => false
      }
    }

  def addVarDecl(cvd: CVarDecl): Unit = if (!isPresent(cvd)) {
    cvds = cvds :+ cvd
    cvdIds.add(cvd.pd.id)
  }

  /** When a cvd is added to the cvdFuncIds, it means that it is a parameter of a function.
   * And there is no need to declare it in the function body
   *
   * @param cvd
   */
  def addFuncVarDecl(cvd: CVarDecl): Unit = if (!isPresent(cvd)) {
    cvdFuncIds.add(cvd.pd.id)
  }

  def varDecls: Seq[CVarDecl] = cvds.toSeq

  def addCStructDecls(csd: CStructDecl): Unit = structDecls.add(csd)
}

object CCode {
  def genCodes(codes: Seq[CCode]): Unit = {
    codes.zipWithIndex.foreach({
      case (code, idx) => code.genCode(s"${code.funcName}_$idx")
    })
  }

}

class CCode(code: String, val funcName: String) {
  override def toString: String = code

  def ccode(): String = toString // for backward compatibility

  def genTest(fileName: String = funcName): CCode = {
    ToFile.genTest(code, funcName, fileName)
    this
  }

  def genCode(fileName: String = funcName): CCode = {
    ToFile.genCode(code, funcName, fileName)
    this
  }

  private object ToFile {
    def genTest(code: String, funcName: String, fileName: String): Unit = {
      val t = templete(funcName)
      if (!new java.io.File(s"C/$fileName.c").exists) {
        FileWriter(s"$SHIRC_PATH/$fileName.c", t)
        format(s"$SHIRC_PATH/$fileName.c")
      }
      genCode(code, funcName, fileName)
    }

    def genCode(code: String, funcName: String, fileName: String): Unit = {
      FileWriter(s"${SHIRC_PATH}/$fileName.h", code)
      format(s"${SHIRC_PATH}/$fileName.h")
    }

    def templete(funcName: String = "func_0"): String = {
      s"""#include "vector.h"
         |#include "test.h"
         |#include <stdio.h>
         |#include <stdlib.h>
         |#include <time.h>
         |#include <math.h>
         |
         |#ifdef KERNEL
         |#define HEADER STRINGIFY( KERNEL )
         |#else
         |#define HEADER "$funcName.h"
         |#endif
         |
         |#include HEADER
         |
         |int main() {
         |
         |}
         |""".stripMargin
    }

    def format(path: String): Unit = s"clang-format -i $path" !
  }

}
