package cGen

import cGen.primitive.{IdxDest, IdxS}
import core.{Expr, ParamDef, ParamUse, TypeChecker}
import sun.reflect.generics.reflectiveObjects.NotImplementedException

/**A smart assign. Assign primitives should only work on CScalarType.
 * However, we need to assign an array or even a matrix to a destination sometime.
 * SmartAssign help to create the nested loop for such assignment.
 * It only works on the View Removing Level
 */
object SmartAssign {
  def apply(src: Expr, dest: Expr): CGenExpr = {
    TypeChecker.check(src).t match {
      case ArrayType(_, len) => {
        val idx = ParamDef(IntType(32))
        Loop(CLambda(idx,
          SmartAssign(IdxS(src, ParamUse(idx)), IdxDest(dest, ParamUse(idx)))
        ), Constant(len.ae.evalInt, IntType(32)))
      }
      case _: CScalarTypeT => Assign(src, dest)
      case other =>
        throw new Exception(other.toShortString + "is not supported")
    }
  }
}
