package cGen

import core.{BuiltinExpr, CoreExpr, Expr, ShirIR, ParamDef, Type, TypeChecker, TypeVarT}

import scala.collection.mutable
import PatternMatchingRewriter.{ParamReifier, RewriteOrder, RewriteRule, TypeReifier}
import cGen.idioms.{IntrinsicCall, IntrinsicCallExpr}
import cGen.pass.RebuildPass

import scala.annotation.tailrec

object PatternMatchingRewriter {
  /**
    * A mapping that reifies parameters. That is, a mapping that maps the unbound parameters of a pattern to the
    * expressions to which they are bound in a reified expression. The mapping also renames bound parameters.
    */
  type ParamReifier = PartialFunction[ParamDef, Expr]

  /**
    * A mapping that reifies types. That is, a mapping that maps the unbound type variables of a pattern to the types
    * to which they are bound in a reified expression.
    */
  type TypeReifier = PartialFunction[TypeVarT, Type]

  /**
    * A rewrite rule is a named (source pattern, destination pattern) pair.
    *
    * Source patterns use unbound parameters to describe syntactic variation points that may be reified into concrete
    * expressions. Bound parameters may be renamed. Unbound type variables may be reified into concrete types.
    *
    * Destination patterns are responsible for appropriately reifying parameters and types as part of their
    * implementation. To do so, they have access to a reification map for parameters as well as one for type variables.
    */
  type RewriteRule = (String, Expr, (ParamReifier, TypeReifier) => Expr)

  /**
    * Describes the order in which rewrite rules are applied.
    */
  object RewriteOrder extends Enumeration {
    /**
      * Indicates that rewrite rules are applied in a bottom-up fashion: Rewrite rules are applied to the smallest
      * matching subtrees of an expression first.
      */
    val BottomUp: Value = Value

    /**
      * Indicates that rewrite rules are applied in a top-down fashion: Rewrite rules are applied to the largest
      * matching subtrees of an expression first.
      */
    val TopDown: Value = Value
  }
}

/**
  * An expression rewriter that is rooted in pattern matching. Input patterns are matched to expression (sub)trees.
  * Matching subtrees are rewritten as output patterns.
  * @param rewriteRules The set of rewrite rules to use. Each rewrite rule is a named
  *                     (source pattern, destination pattern) pair.
  *                     Source patterns use unbound parameters to describe syntactic variation points that may be
  *                     reified into concrete expressions. Bound parameters may be renamed. Unbound type variables may
  *                     be reified into concrete types.
  *                     Destination patterns are responsible for appropriately reifying parameters and types as part of
  *                     their implementation. To do so, they have access to a reification map for parameters as well as
  *                     one for type variables.
  * @param order The order in which expressions are rewritten. This determines whether minimal subtrees or maximal
  *              subtrees are rewritten first.
  * @param preprocessRules Maximally rewrites the rules (using the rules) before matching patterns. Combining this with
  *                        bottom-up rewriting can produce a "best of both worlds" rewriter that allows for meaningful
  *                        rule interaction without smaller rules sabotaging larger rules. The main caveat is that
  *                        maximally rewriting the rules with respect to each other may not terminate, depending on the
  *                        rules.
  */
class PatternMatchingRewriter(rewriteRules: Seq[RewriteRule],
                              order: RewriteOrder.Value = RewriteOrder.TopDown,
                              preprocessRules: Boolean = false) {

  private val processedRules: Seq[RewriteRule] = if (preprocessRules) {
    // Rewrite each rule using the other rules, but not using itself.
    rewriteRules.zipWithIndex.map(pair => {
      val (rule, index) = pair
      val (name, input, output) = rule
      val innerRewriter = new PatternMatchingRewriter(
        rewriteRules.zipWithIndex.filter(p => p._2 != index).map(p => p._1),
        order,
        false)
      (name, innerRewriter(input), output)
    })
  } else {
    rewriteRules
  }

  /**
    * Rewrites subtrees in `expr` in accordance with the rewrite rules passed to this pattern-matching rewriter.
    * @param expr An expression to rewrite.
    * @return An modified version of `expr` that has been maximally rewritten by this rewriter. That is, a rewritten
    *         version of `expr` that contains no further opportunities for rewriting by the set of rules passed to this
    *         rewriter.
    */
  def apply(expr: Expr): Expr = {
    order match {
      case RewriteOrder.BottomUp =>
        // Build a pass that rewrites expressions. Compute and memoize results using a `MemoizingPartialFunction`.
        // Since the partial function does not outlive this function call, the memory used to cache results can be
        // reclaimed as soon as this call exits.
        val rewritePass = RebuildPass("RewriteExpressions", new MemoizingPartialFunction(tryRewrite))
        rewritePass(expr)
      case RewriteOrder.TopDown =>
        // FIXME: modify this for efficiency's sake. The only problem is that `RebuildPass` does bottom-up (postorder)
        //  rewriting. We want top-down (preorder) rewriting.
        @tailrec
        def rewrite(ir: ShirIR): ShirIR = ir match {
          case expr: Expr => tryRewrite(expr) match {
            case Some(result) => rewrite(result)
            case None => expr
          }
          case other => other
        }
        expr.visitAndRebuild(rewrite, ir => ir).asInstanceOf[Expr]
    }
  }

  /**
    * A partial function implementation that computes results only once for each input.
    * @param impl The partial function's implementation, as a function that returns an `Option`.
    * @tparam A The partial function's parameter type.
    * @tparam B The partial function's return type.
    */
  private class MemoizingPartialFunction[A, B](impl: A => Option[B]) extends PartialFunction[A, B] {
    private val cache: mutable.Map[A, Option[B]] = mutable.Map()

    private def get(x: A): Option[B] = {
      if (cache contains x) {
        cache(x)
      } else {
        val result = impl(x)
        cache(x) = result
        result
      }
    }

    override def isDefinedAt(x: A): Boolean = get(x).isDefined

    override def apply(x: A): B = get(x).get
  }

  /**
    * Tries to rewrite an expression using one of the rules in `rewriteRules`. The expression is rewritten using the
    * first matching expression.
    * @param expr The expression to rewrite.
    * @return A rewritten expression if the expression can be rewritten; otherwise, `None`.
    */
  private def tryRewrite(expr: Expr): Option[Expr] = {
    // Traverse all rewrite rules in sequence.
    for ((_, from, to) <- processedRules) {
      // Check if `expr` is a reification of `from`.
      tryUnwrapReification(from, expr) match {
        // If `expr` is a reification of `from`, then we ascertain how `from` was reified into `expr`. We apply these
        // findings to reify `to`.
        case Some((reifyParam, reifyType)) =>
          val result = to(reifyParam, reifyType)
          return Some(result)

        case None =>
      }
    }
    None
  }

  /**
    * Tries to unwrap a pattern reification. If `reification` is indeed a reification of `pattern`, the reification
    * mappings to turn `pattern` into `reification` are reproduced.
    * @param pattern A pattern whose unbound parameters may be reified into expression and whose unbound type variables
    *                may be reified into types.
    * @param reification An expression that may be a reification of `pattern`.
    * @return A pair of reification mappings if `reification` is indeed a reification of `pattern`; otherwise, `None`.
    */
  private def tryUnwrapReification(pattern: Expr, reification: Expr): Option[(ParamReifier, TypeReifier)] = {
    // Figure out which parameters we can reify.
    val reifiable = mutable.Set[Long]()
    pattern.visit({
      case param: ParamDef => reifiable.add(param.id)
      case _ =>
    })

    // Try to reify parameters until `pattern` and `reification` are equivalent.
    val checker = new ExprEqualityCheckerModuloParams(reifiable.toSet)
    if (checker.check(pattern, reification)) {
      // The expressions are syntactically equivalent. Now let's check if `pattern`'s types can be reified such that it
      // becomes `reification`.
      TypeChecker.solveConstraints(checker.typeConstraints) match {
        // If we can verify the reification, return a pair of reification mappings.
        case Some(solution) =>
          Some ({
            case p if checker.paramReification contains p.id => checker.paramReification(p.id)
          }, {
            case tv if solution contains tv => solution(tv)
          })

        case None => None
      }
    } else {
      None
    }
  }

  /**
    * Checks expression equality modulo parameters. That is, it checks if two expressions are equal if we allow
    * for parameters to be reified. Parameter reification is restricted to a well-defined set of parameters.
    */
  private class ExprEqualityCheckerModuloParams(reifiableParams: Set[Long]) {
    val paramReification: mutable.Map[Long, Expr] = mutable.Map()
    val typeConstraints: mutable.Buffer[(Type, Type)] = mutable.Buffer()

    def check(pattern: ShirIR, reification: ShirIR): Boolean = (pattern, reification) match {
      // This is one of the more interesting cases: If a parameter has already been reified, then we check that
      // the reification is consistent. Otherwise, we reify the parameter.
      case (pattern: ParamDef, reification: Expr) if reifiableParams contains pattern.id =>
        val constraint = (pattern.t, reification.t)
        typeConstraints += constraint
        if (paramReification contains pattern.id) {
          // To ensure that our reification was okay, we will recurse on the reified left parameter. Note that
          // for sensible definitions of `reifiable`, this recursive call will not reify any component of `pattern`
          // again.
          check(paramReification(pattern.id), reification)
        } else {
          paramReification(pattern.id) = reification
          true
        }
      case (pattern: ParamDef, reification: ParamDef) => pattern.id == reification.id
      case (_: ParamDef, _) | (_, _: ParamDef) => false

      // Some expressions require a little bit of extra love.
      case (pattern: BinaryOpExpr, reification: BinaryOpExpr) =>
        pattern.op == reification.op &&
          pattern.isBool == reification.isBool &&
          check(pattern.innerIR, reification.innerIR)
      case (_: BinaryOpExpr, _) | (_, _: BinaryOpExpr) => false

      case (Constant(val1, t1), Constant(val2, t2)) => val1 == val2 && check(t1, t2)
      case (Constant(_, _), _) | (_, Constant(_, _)) => false

      case (IntrinsicCall(name1, args1, _, _), IntrinsicCall(name2, args2, _, _))
        if name1 == name2 && args1.length == args2.length =>

        args1.zip(args2).forall(pair => check(pair._1, pair._2))

      case (_: IntrinsicCallExpr, _) | (_, _: IntrinsicCallExpr) => false

      // Builtin expressions are fairly formulaic: We simply recurse on the args and types if we find two nodes that
      // have the same class.
      case (pattern: BuiltinExpr, reification: BuiltinExpr)
        if pattern.getClass().equals(reification.getClass())
        && pattern.args.length == reification.args.length
        && pattern.types.length == reification.types.length =>

        typeConstraints ++= pattern.types.zip(reification.types)
        pattern.args.zip(reification.args).forall(pair => check(pair._1, pair._2))

      // We have two builtin expressions that should be equivalent if the trees are to match, but the expressions are of
      // different classes altogether. There's no way we can match those two together.
      case (_: BuiltinExpr, _) | (_, _: BuiltinExpr) => false

      // Some classes of expressions have inheritance hierarchies that do not involve BuiltinExpr. In those cases, we'll
      // recurse on the children instead. The children can be any IR nodes including expressions and types.
      case (pattern: CoreExpr, reification: CoreExpr)
        if pattern.getClass().equals(reification.getClass())
        && pattern.children.length == reification.children.length =>

        pattern.children.zip(reification.children).forall(pair => check(pair._1, pair._2))

      case (_: CoreExpr, _) | (_, _: CoreExpr) => false

      // Check types by appending types to the set of type constraints.
      case (pattern: Type, reification: Type) =>
        val constraint = (pattern, reification)
        typeConstraints += constraint
        true
      case (_: Type, _) | (_, _: Type) => false

      case (_, _) => {
        throw new UnsupportedOperationException(
          s"Reification unwrapping encountered unexpected IR nodes: '$pattern' and '$reification'")
      }
    }
  }
}
