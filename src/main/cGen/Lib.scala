package cGen

import cGen.AlgoOp.AlgoOp
import cGen.Lib.{GetPosedRelatives, RadialDistort}
import cGen.TupleStructName.COMPLEX
import cGen.idioms.{IntrinsicCall, IntrinsicCallExpr, TypeArith, TypeArithExpr}
import cGen.primitive._
import core._
import lift.arithmetic.ArithExpr

import scala.language.postfixOps
import scala.annotation.tailrec

object Lib {
  def addTup(implicit t: () => Type): CLambda = {
    val tup = ParamDef(TupleType(t().asInstanceOf[CGenDataTypeT], t().asInstanceOf[CGenDataTypeT]))
    CLambda(tup, Add(ParamUse(tup)))
    //    CLambda(tup, {
    //      val p = ParamDef(tup.t)
    //      CGenLet(p, Materialize(ParamUse(tup))) apply
    //        BinaryOp(Select(ParamUse(p), 0), Select(ParamUse(p), 1), AlgoOp.Add)
    //    })
  }

  def subTup(implicit t: () => Type): CLambda = {
    val tup = ParamDef(TupleType(t().asInstanceOf[CGenDataTypeT], t().asInstanceOf[CGenDataTypeT]))
    CLambda(tup, BinaryOp(SelectS(ParamUse(tup), 0), SelectS(ParamUse(tup), 1), AlgoOp.Sub))
  }

  def add2(implicit t: () => Type): CLambda = {
    val p1 = ParamDef(t())
    val p2 = ParamDef(t())
    CLambda(Seq(p1, p2), Add(Tuple(ParamUse(p1), ParamUse(p2))))
  }

  def mulTup(implicit t: () => Type): CLambda = {
    val tup = ParamDef(TupleType(t().asInstanceOf[CGenDataTypeT], t().asInstanceOf[CGenDataTypeT]))
    CLambda(tup, BinaryOp(SelectS(ParamUse(tup), 0), SelectS(ParamUse(tup), 1), AlgoOp.Mul))
  }

  def mul2: CLambda = {
    val p1 = ParamDef(IntType(32))
    val p2 = ParamDef(IntType(32))
    CLambda(Seq(p1, p2), Mul(Tuple(ParamUse(p1), ParamUse(p2))))
  }

  def zip(len: Int): CLambda = {
    val p1 = ParamDef(ArrayType(IntType(32), len))
    val p2 = ParamDef(ArrayType(IntType(32), len))
    CLambda(Seq(p1, p2), Zip(Tuple(ParamUse(p1), ParamUse(p2))))
  }

  def vectorAdd(len: Int)(implicit t: () => Type): CLambda = {
    val a = ParamDef(ArrayType(IntType(32), len))
    val b = ParamDef(ArrayType(IntType(32), len))
    val map = Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))))
    CLambda(Seq(a, b), map)
  }

  def VectorAdd(v0: Expr, v1: Expr, viewType: ViewTypeT = EagerType())(implicit t: () => Type): Expr =
    Map(addTup, Zip(Tuple(v0, v1)), Some(viewType))

  def VectorSub(v0: Expr, v1: Expr, viewType: ViewTypeT = EagerType())(implicit t: () => Type): Expr =
    Map(subTup, Zip(Tuple(v0, v1)), Some(viewType))

  def VectorMul(v0: Expr, v1: Expr, viewType: ViewTypeT = EagerType())(implicit t: () => Type): Expr =
    Map(mulTup, Zip(Tuple(v0, v1)), Some(viewType))

  def VectorSubWorseCase(v0: Expr, v1: Expr)(implicit t: () => Type): Expr = {
    VectorOpWorseCase(v0, v1, subTup)
  }

  def VectorAddWorseCase(v0: Expr, v1: Expr)(implicit t: () => Type): Expr = {
    VectorOpWorseCase(v0, v1, addTup)
  }

  def VectorMulWorseCase(v0: Expr, v1: Expr)(implicit t: () => Type): Expr = {
    VectorOpWorseCase(v0, v1, mulTup)
  }

  def VectorOpWorseCase(v0: Expr, v1: Expr, f: Expr)(implicit t: () => Type): Expr = {
    val prog = new Program()
    val tup = prog << Materialize(Tuple(v0, v1))
    val zip = prog << Materialize(Zip(tup))
    prog.build(Map(f, zip))
  }

  def zero(implicit t: () => Type): Expr = num(0)

  def one(implicit t: () => Type): Expr = num(1)

  def two(implicit t: () => Type): Expr = num(2)

  def three(implicit t: () => Type): Expr = num(3)

  def four(implicit t: () => Type): Expr = num(4)

  def five(implicit t: () => Type): Expr = num(5)

  def six(implicit t: () => Type): Expr = num(6)

  def seven(implicit t: () => Type): Expr = num(7)

  def nine(implicit t: () => Type): Expr = num(9)

  def num(value: Double)(implicit t: () => Type) = Constant(value, t())

  def add(s1: Expr, s2: Expr): Expr = {
    BinaryOp(s1, s2, AlgoOp.Add)
    //Add(Tuple(s1, s2))
  }

  def sub(s1: Expr, s2: Expr): Expr = {
    BinaryOp(s1, s2, AlgoOp.Sub)
    // Sub(Tuple(s1, s2))
  }

  def mul(s1: Expr, s2: Expr): Expr = {
    BinaryOp(s1, s2, AlgoOp.Mul)
    // Mul(Tuple(s1, s2))
  }

  def div(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Div)

  def mod(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Mod)

  def pow(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Pow)

  def rightShift(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.RightShift)

  def leftShift(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.LeftShift)

  def cos(s: Expr): Expr = UnaryOp(s, AlgoOp.Cos)

  def sin(s: Expr): Expr = UnaryOp(s, AlgoOp.Cos)

  def minus(s: Expr): Expr = UnaryOp(s, AlgoOp.Minus)

  def exp(s: Expr): Expr = UnaryOp(s, AlgoOp.Exp)

  def log(s: Expr): Expr = UnaryOp(s, AlgoOp.Log)

  def sqrt(s: Expr): Expr = UnaryOp(s, AlgoOp.Sqrt)

  def ge(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Ge, isBool = true)

  def gt(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Gt, isBool = true)

  def lt(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Lt, isBool = true)

  def le(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Le, isBool = true)

  def eq(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Eq, isBool = true)

  def at(e0: Expr, e1: Expr): Expr = IdxS(e0, e1)

  def andand(s1: Expr, s2: Expr): Expr = BinaryOp(s1, s2, AlgoOp.Andand, isBool = true)

  def VectorSum(v: Expr)(implicit t: () => Type): Expr = Reduce(add2, zero, v)

  def VectorSumWorseCase(v: Expr)(implicit t: () => Type): Expr = {
    val prog = new Program()
    val vv = prog << v
    Reduce(add2, zero, vv)
  }

  def VectorAdd(v1: Expr, v2: Expr)(implicit t: () => Type): Expr = Map(addTup, Zip(Tuple(v1, v2)))

  def VectorMulElemwise(v1: Expr, v2: Expr)(implicit t: () => Type): Expr = Map(mulTup, Zip(Tuple(v1, v2)))

  def MatrixSum(m: Expr)(implicit t: () => Type): Expr = {
    val v = ParamDef(tt(m).asInstanceOf[ArrayTypeT].et)
    VectorSum(Map(CLambda(v, VectorSum(ParamUse(v))), m))
  }

  def MatrixMulElemwise(m1: Expr, m2: Expr)(implicit t: () => Type): Expr = {
    val v1 = ParamDef(tt(m1).asInstanceOf[ArrayTypeT].et)
    val v2 = ParamDef(tt(m2).asInstanceOf[ArrayTypeT].et)
    val m = MatrixMap2(CLambda(v1, CLambda(v2, VectorMulElemwise(ParamUse(v1), ParamUse(v2)))), m1, m2)
    TypeChecker.check(m)
    m
  }


  def VectorSubScalar(v: Expr, scalar: Expr, vt: ViewTypeT = EagerType())(implicit t: () => Type): Expr = {
    val elm = ParamDef(t())
    Map(CLambda(elm, sub(ParamUse(elm), scalar)), v, Some(vt))
  }

  def VectorAddScalar(v: Expr, scalar: Expr)(implicit t: () => Type): Expr = {
    val elm = ParamDef(t())
    Map(CLambda(elm, add(ParamUse(elm), scalar)), v)
  }

  def VectorMulScalar(v: Expr, scalar: Expr)(implicit t: () => Type): Expr = {
    val elm = ParamDef(t())
    Map(CLambda(elm, mul(ParamUse(elm), scalar)), v)
  }

  def VectorSingleOp(v: Expr, op: AlgoOp, vt: ViewTypeT = EagerType())(implicit t: () => Type): Expr = {
    val elm = ParamDef(t())
    Map(CLambda(elm, UnaryOp(ParamUse(elm), op)), v, Some(vt))
  }

  def VectorMax(v: Expr)(implicit t: () => Type): Expr = {
    val acc = ParamDef(t())
    val cur = ParamDef(t())
    val f = CLambda(acc, CLambda(cur, If(gt(ParamUse(acc), ParamUse(cur)), Id(ParamUse(acc)), Id(ParamUse(cur)))))
    Reduce(f, Constant(-1000, t()), v)
  }

  def SqNorm(v: Expr)(implicit t: () => Type): Expr = VectorSum {
    val elm = ParamDef(TypeChecker.check(v).t.asInstanceOf[ArrayTypeT].et)
    Map(CLambda(elm, BinaryOp(ParamUse(elm), two, AlgoOp.Pow)), v, EagerType())
  }
  //    Build({
  //      val idx = ParamDef(IntType(32))
  //      CLambda(idx, BinaryOp(ArrayAccess(v, ParamUse(idx)), Constant(2, DoubleType()), AlgoOp.Pow))
  //    }, length(v))

  def SqNormForGMMAllView(v: Expr)(implicit t: () => Type): Expr = {
    val prog = new Program()
    val vv = prog << v
    prog.build(VectorSum(
      Build({
        val idx = ParamDef(IntType(32))
        CLambda(idx, BinaryOp(IdxS(vv, ParamUse(idx)), Constant(2, DoubleType()), AlgoOp.Pow))
      }, length(v))
    ))
  }

  def SqNormWorseCase(v: Expr)(implicit t: () => Type): Expr = {
    val prog = new Program()
    val b = prog << Build({
      val idx = ParamDef(IntType(32))
      CLambda(idx, BinaryOp(IdxS(v, ParamUse(idx)), Constant(2, DoubleType()), AlgoOp.Pow))
    }, length(v))
    prog.build(VectorSum(b))
  }

  def DotProd(v1: Expr, v2: Expr)(implicit t: () => Type): Expr =
    VectorSum(MapS(mulTup, Zip(Tuple(v1, v2))))

  def DotProdWorseCase(v1: Expr, v2: Expr)(implicit t: () => Type): Expr = {
    val prog = new Program()
    val tup = prog << Materialize(Tuple(v1, v2))
    val zip = prog << Materialize(Zip(tup))
    val map = prog << Materialize(Map(mulTup, zip))
    prog.build(VectorSum(map))
  }

  def MulByScalar(scalar: Expr, v: Expr)(implicit t: () => Type) = Map({
    val p = ParamDef(t())
    CLambda(p, mul(ParamUse(p), scalar))
  }, v)

  def complexMul(c0: Expr, c1: Expr): Expr = {
    Tuple(COMPLEX,
      sub(mul(SelectS(c0, 0), SelectS(c1, 0)), mul(SelectS(c0, 1), SelectS(c1, 1))),
      add(mul(SelectS(c0, 0), SelectS(c1, 1)), mul(SelectS(c0, 1), SelectS(c1, 0)))
    )
  }

  def complexVecMul(v0: Expr, v1: Expr)(implicit t: () => Type): Expr = {
    val z = Zip(Tuple(v0, v1))
    val p = ParamDef(TypeChecker.check(z).t.asInstanceOf[ArrayTypeT].et)
    val tupT = p.t.asInstanceOf[NamedTupleStructTypeT].namedTypes.head._2
    val et = tupT.asInstanceOf[NamedTupleStructTypeT].namedTypes.head._2 // complex has two same element type
    val acc = ParamDef(tupT)
    val cur = ParamDef(tupT)
    val map = TypeChecker.check(Map(CLambda(p, complexMul(SelectS(ParamUse(p), 0), SelectS(ParamUse(p), 1))), z))
    val init = ComplexTuple(Id(Constant(0, et)), Id(Constant(0, et)), Some(DestViewType()))
    Reduce(CLambda(acc, CLambda(cur, complexAdd(ParamUse(acc), ParamUse(cur)))), init, map)
  }

  def complexMatVecMul(mat: Expr, v: Expr)(implicit t: () => Type): Expr = {
    val p = ParamDef(TypeChecker.check(mat).t.asInstanceOf[ArrayTypeT].et)
    Map(CLambda(p, complexVecMul(ParamUse(p), v)), mat, SrcViewType())
  }

  def complexMulWorseCase(c0: Expr, c1: Expr): Expr = {
    val prog = new Program()
    val sc00 = prog << Materialize(SelectS(c0, 0))
    val sc10 = prog << Materialize(SelectS(c1, 0))
    val sc01 = prog << Materialize(SelectS(c0, 1))
    val sc11 = prog << Materialize(SelectS(c1, 1))
    prog.build(Tuple(COMPLEX,
      sub(mul(sc00, sc10), mul(sc01, sc11)),
      add(mul(sc00, sc11), mul(sc01, sc10))
    ))
  }

  def complexAdd(c0: Expr, c1: Expr): Expr = {
    Tuple(COMPLEX, add(SelectS(c0, 0), SelectS(c1, 0)), add(SelectS(c0, 1), SelectS(c1, 1)))
  }

  def complexSub(c0: Expr, c1: Expr): Expr = {
    Tuple(COMPLEX, sub(SelectS(c0, 0), SelectS(c1, 0)), sub(SelectS(c0, 1), SelectS(c1, 1)))
  }

  def slideS2D(_input: Expr, size: ArithTypeT): Expr = {
    val input = TypeChecker.check(_input)
    val tmp = TypeChecker.check(Slide(Map({
      val p = ParamDef(input.t.asInstanceOf[ArrayTypeT].et)
      CLambda(p, Slide(ParamUse(p), size))
    }, input, Some(SrcViewType())), size))
    Map({
      val p = ParamDef(tmp.t.asInstanceOf[ArrayTypeT].et)
      CLambda(p, transposeS(ParamUse(p)))
    }, tmp, Some(SrcViewType()))
  }

  // radParams is a view
  def RadialDistort(radParams: Expr, _proj: Expr) = {
    implicit val t: () => Type = () => DoubleType()
    val prog = new Program()
    val proj = prog.add(_proj)
    val rsq = prog.add(SqNorm(proj))
    val l0 = mul(IdxS(radParams, zero(() => IntType(32))), rsq)
    val l1 = mul(mul(IdxS(radParams, one(() => IntType(32))), rsq), rsq)
    val l = add(add(one, l0), l1)
    prog.build(MulByScalar(l, proj))
  }

  def RadialDistortWorseCase(radParams: Expr, _proj: Expr) = {
    implicit val t: () => Type = () => DoubleType()
    val prog = new Program()
    val proj = prog.add(_proj)
    val rsq = prog.add(SqNormWorseCase(proj))
    val l0 = mul(IdxS(radParams, zero(() => IntType(32))), rsq)
    val l1 = mul(mul(IdxS(radParams, one(() => IntType(32))), rsq), rsq)
    val l = add(add(one, l0), l1)
    prog.build(MulByScalar(l, proj))
  }

  def Cross(v1: Expr, v2: Expr): Expr = {
    val c00 = Mul(Tuple(IdxS(v1, Constant(1, IntType(32))), IdxS(v2, Constant(2, IntType(32)))))
    val c01 = Mul(Tuple(IdxS(v1, Constant(2, IntType(32))), IdxS(v2, Constant(1, IntType(32)))))
    val c10 = Mul(Tuple(IdxS(v1, Constant(2, IntType(32))), IdxS(v2, Constant(0, IntType(32)))))
    val c11 = Mul(Tuple(IdxS(v1, Constant(0, IntType(32))), IdxS(v2, Constant(2, IntType(32)))))
    val c20 = Mul(Tuple(IdxS(v1, Constant(0, IntType(32))), IdxS(v2, Constant(1, IntType(32)))))
    val c21 = Mul(Tuple(IdxS(v1, Constant(1, IntType(32))), IdxS(v2, Constant(0, IntType(32)))))
    val c0 = Sub(Tuple(c00, c01))
    val c1 = Sub(Tuple(c10, c11))
    val c2 = Sub(Tuple(c20, c21))
    ConcatFactory(ConcatFactory(RepeatOnce(c0), RepeatOnce(c1), DestViewType()), RepeatOnce(c2), DestViewType())
  }

  def LiftStyleCross(v1: Expr, v2: Expr): Expr = {
    val pf0 = {
      val i = ParamDef(IntType(32))
      implicit val t = () => IntType(32)
      CLambda(i, mod(add(ParamUse(i), one), three))
    }

    val pf1 = {
      val i = ParamDef(IntType(32))
      implicit val t = () => IntType(32)
      CLambda(i, mod(add(ParamUse(i), two), three))
    }

    implicit val t = () => DoubleType()
    VectorSub(
      VectorMul(Permute(pf0, v1), Permute(pf1, v2), SrcViewType()),
      VectorMul(Permute(pf1, v1), Permute(pf0, v2), SrcViewType())
      , EagerType())
  }

  // rot is a forward view
  def RodriguesRotatePoint(rot: Expr, _x: Expr): Expr = {
    implicit val t: () => DoubleType = () => DoubleType()
    val prog = new Program()
    val x = prog.add(_x)
    val sqtheta = prog.add(SqNorm(rot))
    prog.build(
      If(BinaryOp(sqtheta, zero(() => DoubleType()), AlgoOp.Eq, isBool = true),
        {
          val prog = new Program()
          val theta = prog.add(UnaryOp(sqtheta, AlgoOp.Sqrt))
          val costheta = prog.add(UnaryOp(theta, AlgoOp.Cos))
          val sintheta = prog.add(UnaryOp(theta, AlgoOp.Sin))
          val thetaInv = prog.add(BinaryOp(one, theta, AlgoOp.Div))
          val w = prog.add(MulByScalar(thetaInv, rot))
          val wCrossX = LiftStyleCross(w, x)
          val tmp = mul(DotProd(w, x), sub(one, costheta))
          prog.build(VectorAdd(
            VectorAdd(MulByScalar(costheta, x), MulByScalar(sintheta, wCrossX)),
            MulByScalar(tmp, w)
          ))
        },
        {
          VectorAdd(x, LiftStyleCross(rot, x))
        }
      )
    )
  }

  def RodriguesRotatePointWorseCase(rot: Expr, _x: Expr): Expr = {
    implicit val t: () => DoubleType = () => DoubleType()
    val prog = new Program()
    val x = prog.add(_x)
    val sqtheta = prog.add(SqNorm(rot))
    prog.build(
      If(BinaryOp(sqtheta, zero(() => DoubleType()), AlgoOp.Eq, isBool = true),
        {
          val prog = new Program()
          val theta = prog.add(UnaryOp(sqtheta, AlgoOp.Sqrt))
          val costheta = prog.add(UnaryOp(theta, AlgoOp.Cos))
          val sintheta = prog.add(UnaryOp(theta, AlgoOp.Sin))
          val thetaInv = prog.add(BinaryOp(one, theta, AlgoOp.Div))
          val w = prog.add(MulByScalar(thetaInv, rot))
          val wCrossX = prog << Cross(w, x)
          val tmp = prog << mul(DotProdWorseCase(w, x), sub(one, costheta))
          val mbs0 = prog << MulByScalar(sintheta, wCrossX)
          val mbs1 = prog << MulByScalar(costheta, x)
          val mbs2 = prog << MulByScalar(tmp, w)
          val va0 = prog << VectorAdd(mbs0, mbs1)
          prog.build(VectorAdd(va0, mbs2))
        },
        {
          VectorAdd(x, Cross(rot, x))
        }
      )
    )
  }

  def Project(cam: Expr, X: Expr): Expr = {
    implicit val t: () => DoubleType = () => DoubleType()
    val prog = new Program()
    val Xcam = prog.add(RodriguesRotatePoint(
      Slice(cam, zero(() => IntType(32)), ArithType(3)),
      VectorSub(X, Slice(cam, three(() => IntType(32)), ArithType(3)), EagerType())))
    val distorted = RadialDistort(
      Slice(cam, nine(() => IntType(32)), ArithType(2)),
      MulByScalar(BinaryOp(one, IdxS(Xcam, two(() => IntType(32))), AlgoOp.Div), Slice(Xcam, zero(() => IntType(32)), ArithType(2)))
    )
    val proj = VectorAdd(Slice(cam, seven(() => IntType(32)), ArithType(2)), MulByScalar(IdxS(cam, six(() => IntType(32))), distorted))
    prog.build(proj)
  }

  def ProjectWorseCase(cam: Expr, X: Expr): Expr = {
    implicit val t: () => DoubleType = () => DoubleType()
    val prog = new Program()
    val s0 = Materialize(Slice(cam, zero(() => IntType(32)), ArithType(3)))
    val Xcam = prog << RodriguesRotatePointWorseCase(
      s0,
      VectorSub(X, Slice(cam, three(() => IntType(32)), ArithType(3))))
    val s1 = prog << Materialize(Slice(cam, nine(() => IntType(32)), ArithType(2)))
    val s2 = prog << Materialize(Slice(Xcam, zero(() => IntType(32)), ArithType(2)))
    val distorted = prog << RadialDistortWorseCase(
      s1,
      MulByScalar(BinaryOp(one, IdxS(Xcam, two(() => IntType(32))), AlgoOp.Div), s2)
    )
    val s3 = prog << Materialize(Slice(cam, seven(() => IntType(32)), ArithType(2)))
    val mbs = prog << MulByScalar(IdxS(cam, six(() => IntType(32))), distorted)
    val proj = VectorAdd(s3, mbs)
    prog.build(proj)
  }

  def ProjectAndSqNorm(cam: Expr, X: Expr): Expr = {
    implicit val t: () => DoubleType = () => DoubleType()
    val prog = new Program()
    val Xcam = prog.add(RodriguesRotatePoint(
      Slice(cam, zero(() => IntType(32)), ArithType(3)),
      VectorSub(X, Slice(cam, three(() => IntType(32)), ArithType(3)))))
    val distorted = RadialDistort(
      Slice(cam, nine(() => IntType(32)), ArithType(2)),
      MulByScalar(BinaryOp(one, IdxS(Xcam, two(() => IntType(32))), AlgoOp.Div), Slice(Xcam, zero(() => IntType(32)), ArithType(2)))
    )
    val proj = VectorAdd(Slice(cam, seven(() => IntType(32)), ArithType(2)), MulByScalar(IdxS(cam, six(() => IntType(32))), distorted))
    prog.build(SqNorm(proj))
  }

  def BundleAdjustment(a: Expr, b: Expr, cam: Expr): Expr = {
    val prog = new Program()
    prog.build({
      val j = prog.add(RadialDistort(a, b))
      Project(cam, j)
    })
  }

  class Program {
    var pairs = Seq[(ParamDef, Expr)]()

    def add(expr: Expr): ParamUse = {
      expr match {
        case use: ParamUse => use
        case _ =>
          val p = ParamDef(TypeChecker.check(expr).t)
          add(p, expr)
          ParamUse(p)
      }
    }

    def add(pd: ParamDef, a: Expr): Unit = pairs = pairs :+ (pd, a)

    def <<(expr: Expr): ParamUse = add(expr)

    def build(expr: Expr): Expr = pairs.foldRight(expr)((pe, in) => CGenLet(pe._1, in, pe._2))
  }

  def vectorAdd3(len: Int): CLambda = {
    implicit val t: () => DoubleType = () => DoubleType()
    val a = ParamDef(ArrayType(t(), len))
    val b = ParamDef(ArrayType(t(), len))
    val c = ParamDef(ArrayType(t(), len))
    val map1 = Map(Lib.addTup, Zip(Tuple(ParamUse(a), ParamUse(b))), Some(SrcViewType()))
    val map2 = Materialize(Map(Lib.addTup, Zip(Tuple(ParamUse(c), map1)), Some(SrcViewType())))
    CLambda(Seq(a, b, c), map2)
  }

  def vectorAdd3Opt(len: Int): CLambda = {
    val a = ParamDef(ArrayType(IntType(32), len))
    val b = ParamDef(ArrayType(IntType(32), len))
    val c = ParamDef(ArrayType(IntType(32), len))
    val tup = ParamDef(TupleType(IntType(32), IntType(32), IntType(32)))
    val add3 = CLambda(tup, Add(Tuple(SelectS(ParamUse(tup), 2, 3), (Add(Tuple(SelectS(ParamUse(tup), 0, 3), SelectS(ParamUse(tup), 1, 3)))))))
    val map = Map(add3, Zip3(Tuple3(ParamUse(a), ParamUse(b), ParamUse(c))))
    CLambda(Seq(a, b, c), map)
  }

  def LogSumExp(x: Expr)(implicit t: () => Type = () => DoubleType()): Expr = {
    val prog = new Program()
    val mx = prog.add(VectorMax(x))
    val xMinusMx = VectorSubScalar(x, mx, SrcViewType())
    val emx = VectorSingleOp(xMinusMx, AlgoOp.Exp, SrcViewType())
    prog.build(add(UnaryOp(VectorSum(emx), AlgoOp.Log), mx))
  }

  def LogSumExpWorseCase(x: Expr)(implicit t: () => Type = () => DoubleType()): Expr = {
    val prog = new Program()
    val mx = prog << VectorMax(x)
    val xMinusMx = prog << VectorSubScalar(x, mx)
    val emx = prog << VectorSingleOp(xMinusMx, AlgoOp.Exp)
    prog.build(add(UnaryOp(VectorSum(emx), AlgoOp.Log), mx))
  }

  def tri(n: Expr): Expr = {
    implicit val t = () => IntType(32)
    div(mul(n, add(n, one)), two)
  }

  def Qtimesv(q: Expr, _l: Expr, _v: Expr): Expr = {
    implicit val t = () => DoubleType()
    val l = TypeChecker.check(_l)
    val v = TypeChecker.check(_v)
    val i = ParamDef(IntType(32))
    val buildIdxf = CLambda(i, {
      val idx = ParamDef(IntType(32))
      val acc = ParamDef(t())
      val p0 = new Program()
      val tis = p0 << tri(sub(ParamUse(i), one(() => IntType(32))))
      val ifoldIdxf = CLambda(idx, CLambda(acc, {
        val p1 = new Program()
        val j = p1 << sub(ParamUse(idx), tis)
        val isRange = andand(ge(j, zero(() => IntType(32))), lt(j, ParamUse(i)))
        p1.build(If(isRange, add(ParamUse(acc), mul(IdxS(l, ParamUse(idx)), IdxS(v, j))), Id(ParamUse(acc))))
      }))
      val sum = Ifold(ifoldIdxf, zero, length(l))
      p0.build(add(sum, mul(exp(IdxS(q, ParamUse(i))), IdxS(v, ParamUse(i)))))
    })
    Build(buildIdxf, length(v), EagerType())
  }

  def MatrixMul2(m1: Expr, m2: Expr, vt: ViewTypeT)(implicit isTransposeLazy: Boolean = true, t: () => Type): Expr = {
    val len = cols(TypeChecker.check(m1))
    val row1 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    val row2 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    if (isTransposeLazy) {
      val m2T = TypeChecker.check(transpose(m2))
      Map(CLambda(row1,
        Map(CLambda(row2,
          DotProd(ParamUse(row1), ParamUse(row2))
        ), m2T, Some(vt))), m1, Some(vt))
    } else {
      val prog = new Program()
      val m2T = prog.add(transpose(m2, EagerType()))
      prog.build(
        Map(CLambda(row1,
          Map(CLambda(row2,
            DotProd(ParamUse(row1), ParamUse(row2))
          ), m2T, Some(vt))), m1, Some(vt))
      )
    }
  }

  def matrixMulS(m1: Expr, m2: Expr)(implicit t: () => Type): Expr = {
    val len = cols(TypeChecker.check(m1))
    val row1 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    val row2 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    MapS(CLambda(row1,
      MapS(CLambda(row2,
        DotProd(ParamUse(row1), ParamUse(row2))
      ), transposeS(m2))), m1)
  }

  def MatrixMulWorseCase(m1: Expr, m2: Expr)(implicit t: () => Type): Expr = {
    val len = cols(TypeChecker.check(m1))
    val row1 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    val row2 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    val prog = new Program()
    val m2T = prog << TypeChecker.check(transpose(m2, EagerType()))
    prog.build(Map(CLambda(row1,
      Map(CLambda(row2,
        DotProdWorseCase(ParamUse(row1), ParamUse(row2))
      ), m2T)), m1))
  }

  def MatrixMul(m1: Expr, m2: Expr)(implicit isTransposeLazy: Boolean = true, t: () => Type): Expr = {
    val m2T = if (isTransposeLazy) TypeChecker.check(transpose(m2)) else transpose(m2, EagerType())
    MatrixMulPretransposed(m1, m2T)
  }

  def MatrixMulPretransposed(m1: Expr, m2T: Expr)(implicit isTransposeLazy: Boolean = true, t: () => Type): Expr = {
    val len = cols(TypeChecker.check(m1))
    val row1 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    val row2 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    if (isTransposeLazy) {
      Map(CLambda(row1,
        Map(CLambda(row2,
          DotProd(ParamUse(row1), ParamUse(row2))
        ), m2T)), m1)
    } else {
      val prog = new Program()
      val m2TParam = prog.add(m2T)
      prog.build(Map(CLambda(row1,
        Map(CLambda(row2,
          DotProd(ParamUse(row1), ParamUse(row2))
        ), m2TParam, Some(EagerType()))), m1, Some(EagerType()))
      )
    }
  }

  def MatrixAdd(m1: Expr, m2: Expr): Expr = {
    implicit val t = () => DoubleType()
    val p1 = ParamDef(tt(m1).asInstanceOf[ArrayTypeT].et)
    val p2 = ParamDef(tt(m2).asInstanceOf[ArrayTypeT].et)
    MatrixMap2(CLambda(p1, CLambda(p2, VectorAdd(ParamUse(p1), ParamUse(p2)))), m1, m2)
  }

  def AutoTuneMatrixMul(m1: Expr, m2: Expr)(implicit t: () => Type): Expr = {
    val len = cols(TypeChecker.check(m1))
    val row1 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    val row2 = ParamDef(ArrayType(t().asInstanceOf[CGenDataTypeT], len))
    Must(MapS(CLambda(row1,
      MapS(CLambda(row2,
        DotProd(ParamUse(row1), ParamUse(row2))
      ), Mtrl(TypeChecker.check(transpose(m2))))), m1))
  }

  def AutoTuneVectorMulScalar(v: Expr, scalar: Expr)(implicit t: () => Type): Expr = {
    val elm = ParamDef(t())
    MapS(CLambda(elm, mul(ParamUse(elm), scalar)), v)
  }

  def AutoTuneMatrixMulScalar(m: Expr, s: Expr)(implicit t: () => CGenDataTypeT): Expr = {
    val in = ParamDef(TypeChecker.check(m).t.asInstanceOf[ArrayTypeT].et)
    Mtrl(MapS(CLambda(in, VectorMulScalar(ParamUse(in), s)), m))
  }


  def MatrixAddScalar(m: Expr, s: Expr)(implicit t: () => CGenDataTypeT): Expr = {
    val in = ParamDef(TypeChecker.check(m).t.asInstanceOf[ArrayTypeT].et)
    Map(CLambda(in, VectorAddScalar(ParamUse(in), s)), m)
  }

  def MatrixMulScalar(m: Expr, s: Expr)(implicit t: () => CGenDataTypeT): Expr = {
    val in = ParamDef(TypeChecker.check(m).t.asInstanceOf[ArrayTypeT].et)
    Map(CLambda(in, VectorMulScalar(ParamUse(in), s)), m)
  }

  def MatrixMulScalarS(scalar: Expr, m: Expr)(implicit t: () => Type): Expr = {
    val v = ParamDef(t())
    map2DS(CLambda(v, mul(scalar, ParamUse(v))), m)
  }

  def MatrixMulScalarE(scalar: Expr, m: Expr)(implicit t: () => Type): Expr = {
    val v = ParamDef(t())
    map2DE(CLambda(v, mul(scalar, ParamUse(v))), m)
  }

  def MatrixMap2(f: Expr, m1: Expr, m2: Expr): Expr = {
    val in = ParamDef(TupleType(tt(m1).asInstanceOf[ArrayTypeT].et.asInstanceOf[CGenDataTypeT], tt(m2).asInstanceOf[ArrayTypeT].et.asInstanceOf[CGenDataTypeT]))
    Map(CLambda(in, CFC(CFC(f, SelectS(ParamUse(in), 0)), SelectS(ParamUse(in), 1))), Zip(Tuple(m1, m2)))
  }

  def mapWithFS(f: Expr => Expr, in: Expr): Expr = {
    val et = tt(in).asInstanceOf[ArrayTypeT].et
    val p = ParamDef(et)
    MapS(CLambda(p, f(ParamUse(p))), in)
  }

  def MapEId(in: Expr): Expr = {
    val et = tt(in).asInstanceOf[ArrayTypeT].et
    val p = ParamDef(et)
    MapE(CLambda(p, Id(ParamUse(p))), in)
  }

  def Map2EId(in: Expr): Expr = {
    val et = tt(in).asInstanceOf[ArrayTypeT].et.asInstanceOf[ArrayTypeT].et
    val p = ParamDef(et)
    val f = CLambda(p, Id(ParamUse(p)))
    map2DE(f, in)
  }

  def Map2SId(in: Expr): Expr = {
    val et = tt(in).asInstanceOf[ArrayTypeT].et.asInstanceOf[ArrayTypeT].et
    val p = ParamDef(et)
    val f = CLambda(p, Id(ParamUse(p)))
    map2DS(f, in)
  }

  def map2DD(f: Expr, m: Expr): Expr = map2D(f, m, DestViewType())

  def map2DS(f: Expr, m: Expr): Expr = map2D(f, m, SrcViewType())

  def map2DE(f: Expr, m: Expr): Expr = map2D(f, m, EagerType())

  def map2D(f: Expr, m: Expr, vt: ViewTypeT): Expr = {
    val et0 = tt(m).asInstanceOf[ArrayTypeT].et
    val et1 = tt(m).asInstanceOf[ArrayTypeT].et.asInstanceOf[ArrayTypeT].et
    val p0 = ParamDef(et0)
    val p1 = ParamDef(et1)
    val f1 = CLambda(p1, CFC(f, ParamUse(p1)))
    vt match {
      case EagerType() => MapE(CLambda(p0, MapE(f1, ParamUse(p0))), m)
      case SrcViewType() => MapS(CLambda(p0, MapS(f1, ParamUse(p0))), m)
      case DestViewType() => MapD(CLambda(p0, MapS(f1, ParamUse(p0))), m)
    }
  }

  def quickTypeCheck(e: Expr): Expr = e.t match {
    case UnknownType => TypeChecker.check(e)
    case _ =>
      if (!e.t.hasUnknownType) TypeChecker.check(e)
      else e
  }

  def tt(e: Expr): Type = TypeChecker.check(e).t

  def GMMAutoTune(x: Expr, alphas: Expr, means: Expr, qs: Expr, ls: Expr, wishartGamma: Expr, wishartM: Expr): Expr = {
    implicit val t = () => DoubleType()
    val n = 1 << 18;
    val d = 3;
    val K = 5;
    val td = ((d) * ((d) + (1))) / (2)
    val i = ParamDef(IntType(32))
    val k = ParamDef(IntType(32))
    val part1 = Must(VectorSum(
      Build(CLambda(i,
        LogSumExp(Build(CLambda(k,
          sub(add(IdxS(alphas, ParamUse(k)), VectorSum(IdxS(qs, ParamUse(k)))),
            div(SqNorm(Extern.qtimesv(IdxS(qs, ParamUse(k)), IdxS(ls, ParamUse(k)), VectorSub(IdxS(x, ParamUse(i)), IdxS(means, ParamUse(k))))), two))
        ), K, SrcViewType()))
      ), n, SrcViewType())))
    val part2 =
      div(VectorSum(Map({
        val x = ParamDef(TupleType(ArrayType(t(), d), ArrayType(t(), td)))
        val y = ParamDef(t())
        CLambda(x, add(SqNorm(Map(CLambda(y, exp(ParamUse(y))), SelectS(ParamUse(x), 0), Some(SrcViewType()))), SqNorm(SelectS(ParamUse(x), 1))))
      }, Zip(Tuple(qs, ls)), Some(SrcViewType()))), two)
    add(sub(part1, mul(Constant(n, t()), LogSumExp(alphas))), part2)
  }

  def GMM(n: Int, d: Int, K: Int, x: Expr, alphas: Expr, means: Expr, qs: Expr, ls: Expr, wishartGamma: Expr, wishartM: Expr): Expr = {
    implicit val t = () => DoubleType()
    val td = ((d) * ((d) + (1))) / (2)
    val i = ParamDef(IntType(32))
    val k = ParamDef(IntType(32))
    val part1 = VectorSum(
      Build(CLambda(i,
        LogSumExp(Build(CLambda(k,
          sub(add(IdxS(alphas, ParamUse(k)), VectorSum(IdxS(qs, ParamUse(k)))),
            div(SqNorm(Extern.qtimesv(IdxS(qs, ParamUse(k)), IdxS(ls, ParamUse(k)), VectorSub(IdxS(x, ParamUse(i)), IdxS(means, ParamUse(k))))), two))
        ), K, SrcViewType()))
      ), n, SrcViewType()))
    val part2 =
      div(VectorSum(Map({
        val x = ParamDef(TupleType(ArrayType(t(), d), ArrayType(t(), td)))
        val y = ParamDef(t())
        CLambda(x, add(SqNorm(Map(CLambda(y, exp(ParamUse(y))), SelectS(ParamUse(x), 0), Some(SrcViewType()))), SqNorm(SelectS(ParamUse(x), 1))))
      }, Zip(Tuple(qs, ls)), Some(SrcViewType()))), two)
    add(sub(part1, mul(Constant(n, t()), LogSumExp(alphas))), part2)
  }

  def GMMAllView2(n: Int, d: Int, K: Int, x: Expr, alphas: Expr, means: Expr, qs: Expr, ls: Expr, wishartGamma: Expr, wishartM: Expr): Expr = {
    implicit val t = () => DoubleType()
    val td = ((d) * ((d) + (1))) / (2)
    val i = ParamDef(IntType(32))
    val k = ParamDef(IntType(32))
    val part1 = VectorSum(
      Build(CLambda(i,
        LogSumExp(Build(CLambda(k, {
          val prog = new Program()
          val qtimesv = prog << SqNorm(Qtimesv(IdxS(qs, ParamUse(k)), IdxS(ls, ParamUse(k)), VectorSub(IdxS(x, ParamUse(i)), IdxS(means, ParamUse(k)))))
          prog.build(sub(add(IdxS(alphas, ParamUse(k)), VectorSum(IdxS(qs, ParamUse(k)))),
            div(qtimesv, two)))
        }
        ), K, SrcViewType()))
      ), n, SrcViewType()))
    val part2 =
      div(VectorSum(Map({
        val x = ParamDef(TupleType(ArrayType(t(), d), ArrayType(t(), td)))
        val y = ParamDef(t())
        CLambda(x, add(SqNorm(Map(CLambda(y, exp(ParamUse(y))), SelectS(ParamUse(x), 0), Some(SrcViewType()))), SqNorm(SelectS(ParamUse(x), 1))))
      }, Zip(Tuple(qs, ls)), Some(SrcViewType()))), two)
    add(sub(part1, mul(Constant(n, t()), LogSumExp(alphas))), part2)
  }

  def GMMAllView(x: Expr, alphas: Expr, means: Expr, qs: Expr, ls: Expr, wishartGamma: Expr, wishartM: Expr): Expr = {
    implicit val t = () => DoubleType()
    val n = 1 << 18;
    val d = 3;
    val K = 5;
    val td = ((d) * ((d) + (1))) / (2)
    val i = ParamDef(IntType(32))
    val k = ParamDef(IntType(32))
    val part1 = VectorSum(
      Build(CLambda(i,
        LogSumExp(Build(CLambda(k, {
          val prog = new Program()
          val sq = prog << SqNormForGMMAllView(Qtimesv(IdxS(qs, ParamUse(k)), IdxS(ls, ParamUse(k)), VectorSub(IdxS(x, ParamUse(i)), IdxS(means, ParamUse(k)))))
          prog.build(sub(add(IdxS(alphas, ParamUse(k)), VectorSum(IdxS(qs, ParamUse(k)))),
            div(sq, two)))
        }
        ), K, EagerType()))
      ), n, EagerType()))
    val part2 =
      div(VectorSum(Map({
        val x = ParamDef(TupleType(ArrayType(t(), d), ArrayType(t(), td)))
        val y = ParamDef(t())
        CLambda(x, add(SqNorm(Map(CLambda(y, exp(ParamUse(y))), SelectS(ParamUse(x), 0))), SqNorm(SelectS(ParamUse(x), 1))))
      }, Zip(Tuple(qs, ls)))), two)
    add(sub(part1, mul(Constant(n, t()), LogSumExp(alphas))), part2)
  }

  def GMMWorseCase(x: Expr, alphas: Expr, means: Expr, qs: Expr, ls: Expr, wishartGamma: Expr, wishartM: Expr): Expr = {
    implicit val t = () => DoubleType()
    val n = 1 << 18;
    val d = 3;
    val K = 5;
    val td = ((d) * ((d) + (1))) / (2)
    val i = ParamDef(IntType(32))
    val k = ParamDef(IntType(32))
    val prog = new Program()
    val b0 = prog << Build(CLambda(i, {
      val prog = new Program()
      val b = prog << Build(CLambda(k,
        sub(add(IdxS(alphas, ParamUse(k)), VectorSum(IdxS(qs, ParamUse(k)))),
          div(SqNormWorseCase(Extern.qtimesv(IdxS(qs, ParamUse(k)), IdxS(ls, ParamUse(k)), VectorSub(IdxS(x, ParamUse(i)), IdxS(means, ParamUse(k))))), two))
      ), K, EagerType())
      prog.build(LogSumExpWorseCase(b))
    }), n, EagerType())
    val part1 = prog << VectorSum(b0)
    val map1 = prog << Map({
      val prog = new Program()
      val x = ParamDef(TupleType(ArrayType(t(), d), ArrayType(t(), td)))
      val y = ParamDef(t())
      val map0 = prog << Map(CLambda(y, exp(ParamUse(y))), SelectS(ParamUse(x), 0))
      CLambda(x, prog.build(add(SqNorm(map0), SqNormWorseCase(SelectS(ParamUse(x), 1)))))
    }, Zip(Tuple(qs, ls)))
    val part2 = div(VectorSum(map1), two)
    prog.build(add(sub(part1, mul(Constant(n, t()), LogSumExpWorseCase(alphas))), part2))
  }

  def inferType(expr: Expr): Type = {
    // HACK: intuitively, `TypeChecker.check(expr).t` should work in all cases, but when type vars are involved,
    // `TypeChecker.check(expr).t` and an expr's output type may not be the same
    expr match {
      case ExternFunctionCall(_, _, _, outt) => outt
      case _ => TypeChecker.check(expr).t
    }
  }

  def length(v: Expr): ArithTypeT = length(inferType(v))

  def length(t: Type): ArithTypeT = t.asInstanceOf[ArrayTypeT].len

  def rows(v: Expr): ArithTypeT = length(v)

  def rows(t: Type): ArithTypeT = length(t)

  def cols(v: Expr): ArithTypeT = cols(inferType(v))

  def cols(t: Type): ArithTypeT = t.asInstanceOf[ArrayTypeT].et.asInstanceOf[ArrayTypeT].len

  def dims(v: Expr): Seq[ArithTypeT] = dims(inferType(v))

  def dims(t: Type): Seq[ArithTypeT] = t match {
    case t: ArrayTypeT => t.len +: dims(t.et)
    case _ => Seq()
  }

  def flatLength(t: Type): ArithExpr = {
    val init: ArithExpr = 1
    dims(t).foldLeft(init)((acc, value) => acc * value.ae)
  }

  def flatLength(v: Expr): ArithExpr = flatLength(inferType(v))

  /**
    * Gets a type's innermost element type.
    *
    * @param v The type to examine.
    * @return The type if the expression is a scalar.
    *         If the type is an array, then the innermost
    *         element type of the array type.
    */
  @tailrec
  def elementType(t: Type): CGenDataTypeT = t match {
    case ArrayType(et, _) => elementType(et)
    case t: CGenDataTypeT => t
    case other =>
      throw new Exception(s"Unexpected element type '$other'.")
  }

  /**
   * Gets an expression's innermost element type.
   * @param v The expression to examine.
   * @return The expression's type if the expression produces a scalar.
   *         If the expression produces an array, then the innermost
   *         element type of the array-producing expression.
   */
  def elementType(v: Expr): CGenDataTypeT = elementType(inferType(v))

  def AngleAxisToRotationMatrix(angleAxis: Expr): Expr = {
    implicit val t = () => DoubleType()
    val prog = new Program()
    val n = prog.add(sqrt(SqNorm(angleAxis)))
    prog.build(
      If(lt(n, div(Constant(1, DoubleType()), Constant(1000, DoubleType()))),
        {
          val v0 = ConcatFactory(ConcatFactory(RepeatOnce(Id(one)), RepeatOnce(Id(zero)), DestViewType()), RepeatOnce(Id(zero)), DestViewType())
          val v1 = ConcatFactory(ConcatFactory(RepeatOnce(Id(zero)), RepeatOnce(Id(one)), DestViewType()), RepeatOnce(Id(zero)), DestViewType())
          val v2 = ConcatFactory(ConcatFactory(RepeatOnce(Id(zero)), RepeatOnce(Id(zero)), DestViewType()), RepeatOnce(Id(one)), DestViewType())
          ConcatFactory(ConcatFactory(RepeatOnce(v0), RepeatOnce(v1), vt = DestViewType()), RepeatOnce(v2), vt = DestViewType())
        },
        {
          val prog = new Program()
          val x = prog.add(div(IdxS(angleAxis, zero(() => IntType(32))), n))
          val y = prog.add(div(IdxS(angleAxis, one(() => IntType(32))), n))
          val z = prog.add(div(IdxS(angleAxis, two(() => IntType(32))), n))
          val s = prog.add(sin(n))
          val c = prog.add(cos(n))

          val v0 = ConcatFactory(ConcatFactory(RepeatOnce(add(mul(x, x), mul(sub(one, mul(x, x)), c))), RepeatOnce(sub(mul(mul(x, y), sub(one, c)), mul(z, s))), DestViewType()), RepeatOnce(add(mul(mul(x, z), sub(one, c)), mul(y, s))), DestViewType())
          val v1 = ConcatFactory(ConcatFactory(RepeatOnce(add(mul(mul(x, y), sub(one, c)), mul(z, s))), RepeatOnce(add(mul(y, y), mul(sub(one, mul(y, y)), c))), DestViewType()), RepeatOnce(sub(mul(mul(y, z), sub(one, c)), mul(x, s))), DestViewType())
          val v2 = ConcatFactory(ConcatFactory(RepeatOnce(sub(mul(mul(x, z), sub(one, c)), mul(y, s))), RepeatOnce(add(mul(mul(z, y), sub(one, c)), mul(x, s))), DestViewType()), RepeatOnce(add(mul(z, z), mul(sub(one, mul(z, z)), c))), DestViewType())
          prog.build(ConcatFactory(ConcatFactory(RepeatOnce(v0), RepeatOnce(v1), vt = DestViewType()), RepeatOnce(v2), vt = DestViewType()))
        }
      )
    )
  }

  def AngleAxisToRotationMatrixWorseCase(angleAxis: Expr): Expr = {
    implicit val t = () => DoubleType()
    val prog = new Program()
    val n = prog.add(sqrt(SqNorm(angleAxis)))
    prog.build(
      If(lt(n, div(Constant(1, DoubleType()), Constant(1000, DoubleType()))),
        {
          val v0 = ConcatFactory(ConcatFactory(RepeatOnce(Id(one)), RepeatOnce(Id(zero))), RepeatOnce(Id(zero)))
          val v1 = ConcatFactory(ConcatFactory(RepeatOnce(Id(zero)), RepeatOnce(Id(one))), RepeatOnce(Id(zero)))
          val v2 = ConcatFactory(ConcatFactory(RepeatOnce(Id(zero)), RepeatOnce(Id(zero))), RepeatOnce(Id(one)))
          ConcatFactory(ConcatFactory(RepeatOnce(v0), RepeatOnce(v1)), RepeatOnce(v2))
        },
        {
          val prog = new Program()
          val x = prog.add(div(IdxS(angleAxis, zero(() => IntType(32))), n))
          val y = prog.add(div(IdxS(angleAxis, one(() => IntType(32))), n))
          val z = prog.add(div(IdxS(angleAxis, two(() => IntType(32))), n))
          val s = prog.add(sin(n))
          val c = prog.add(cos(n))

          val v0 = prog << ConcatFactory(ConcatFactory(RepeatOnce(add(mul(x, x), mul(sub(one, mul(x, x)), c))), RepeatOnce(sub(mul(mul(x, y), sub(one, c)), mul(z, s)))), RepeatOnce(add(mul(mul(x, z), sub(one, c)), mul(y, s))))
          val v1 = prog << ConcatFactory(ConcatFactory(RepeatOnce(add(mul(mul(x, y), sub(one, c)), mul(z, s))), RepeatOnce(add(mul(y, y), mul(sub(one, mul(y, y)), c)))), RepeatOnce(sub(mul(mul(y, z), sub(one, c)), mul(x, s))))
          val v2 = prog << ConcatFactory(ConcatFactory(RepeatOnce(sub(mul(mul(x, z), sub(one, c)), mul(y, s))), RepeatOnce(add(mul(mul(z, y), sub(one, c)), mul(x, s)))), RepeatOnce(add(mul(z, z), mul(sub(one, mul(z, z)), c))))
          prog.build(ConcatFactory(ConcatFactory(RepeatOnce(Materialize(v0)), RepeatOnce(Materialize(v1))), RepeatOnce(Materialize(v2))))
        }
      )
    )
  }

  def EulerAnglesToRotationMatrix(xzy: Expr): Expr = {
    implicit val t = () => DoubleType()
    val tx = IdxS(xzy, zero(() => IntType(32)))
    val ty = IdxS(xzy, two(() => IntType(32)))
    val tz = IdxS(xzy, one(() => IntType(32)))
    val rx = ConcatFactory(ConcatFactory(
      RepeatOnce(ConcatFactory(ConcatFactory(RepeatOnce(Id(one)), RepeatOnce(Id(zero))), RepeatOnce(Id(zero)))),
      RepeatOnce(ConcatFactory(ConcatFactory(RepeatOnce(Id(zero)), RepeatOnce(cos(tx))), RepeatOnce(minus(sin(tx)))))),
      RepeatOnce(ConcatFactory(ConcatFactory(RepeatOnce(Id(zero)), RepeatOnce(sin(ty))), RepeatOnce(minus(cos(ty))))))
    val ry = ConcatFactory(ConcatFactory(
      RepeatOnce(ConcatFactory(ConcatFactory(RepeatOnce(cos(ty)), RepeatOnce(Id(zero))), RepeatOnce(sin(ty)))),
      RepeatOnce(ConcatFactory(ConcatFactory(RepeatOnce(Id(zero)), RepeatOnce(Id(one))), RepeatOnce(Id(zero))))),
      RepeatOnce(ConcatFactory(ConcatFactory(RepeatOnce(minus(sin(ty))), RepeatOnce(Id(zero))), RepeatOnce(cos(ty)))))
    val rz = ConcatFactory(ConcatFactory(
      RepeatOnce(ConcatFactory(ConcatFactory(RepeatOnce(cos(tz)), RepeatOnce(minus(sin(tz)))), RepeatOnce(Id(zero)))),
      RepeatOnce(ConcatFactory(ConcatFactory(RepeatOnce(sin(tz)), RepeatOnce(cos(tz))), RepeatOnce(Id(zero))))),
      RepeatOnce(ConcatFactory(ConcatFactory(RepeatOnce(Id(zero)), RepeatOnce(Id(zero))), RepeatOnce(Id(one)))))
    MatrixMul(rx, MatrixMul(ry, rz))
  }

  def MakeRelative(possParams: Expr, baseRelative: Expr): Expr = {
    implicit val t = () => DoubleType()
    val R = EulerAnglesToRotationMatrix(possParams)
    val r1 = ConcatFactory(ConcatFactory(RepeatOnce(RepeatOnce(Id(zero))), RepeatOnce(RepeatOnce(Id(zero)))), RepeatOnce(RepeatOnce(Id(zero))))
    val r2 = TypeChecker.check(RepeatOnce(ConcatFactory(ConcatFactory(ConcatFactory(RepeatOnce(Id(zero)), RepeatOnce(Id(zero))), RepeatOnce(Id(zero))), RepeatOnce(Id(one)))))
    val T = TypeChecker.check(ConcatFactory(MatrixConcatCol(R, r1), r2))
    MatrixMul(baseRelative, T)
  }

  def transposeS(m: Expr) = transpose(m, SrcViewType())

  def transpose(_m: Expr, vt: ViewTypeT = SrcViewType()) = {
    val m = TypeChecker.check(_m)
    val r = rows(m)
    val c = cols(m)
    Build({
      val i = ParamDef(IntType(32))
      CLambda(i, Build({
        val j = ParamDef(IntType(32))
        vt match {
          case _: EagerTypeT => CLambda(j, Id(IdxS(IdxS(m, ParamUse(j)), ParamUse(i))))
          case _: SrcViewTypeT => CLambda(j, IdxS(IdxS(m, ParamUse(j)), ParamUse(i)))
          case _ => throw new Exception("should not reach")
        }
      }, r, vt
      ))
    }, c, vt)
  }

  def MatrixConcatCol(_m1: Expr, _m2: Expr): Expr = {
    val m1 = TypeChecker.check(_m1)
    val m2 = TypeChecker.check(_m2)
    val _m1t = transpose(m1, EagerType())
    val _m2t = transpose(m2, EagerType())
    val m1t = TypeChecker.check((_m1t))
    val m2t = TypeChecker.check((_m2t))
    transpose(ConcatFactory(m1t, m2t), EagerType())
  }

  def ApplyGlobalTransform(poseParams: Expr, positions: Expr): Expr = {
    implicit val t = () => DoubleType()
    val R = AngleAxisToRotationMatrix(IdxS(poseParams, zero(() => IntType(32))))
    val scale = IdxS(poseParams, one(() => IntType(32)))
    //    val scale = Slide(ArrayAccess(poseParams, one(() => IntType(32))), zero(() => IntType(32)), 3)
    val k = TypeChecker.check(R)
    val d = TypeChecker.check(scale)
    print(k, d)
    val R1 = Map({
      val row = ParamDef(TypeChecker.check(scale).t)
      CLambda(row, VectorMulElemwise(ParamUse(row), scale))
    }, R)
    TypeChecker.check(TypeChecker.check(R1))
    val T = MatrixConcatCol(R1, transpose(RepeatOnce(Id(IdxS(poseParams, two(() => IntType(32)))))))
    //    val T = MatrixConcatCol(R1, MatrixTranspose(Push(Id(Slide(ArrayAccess(poseParams, two(() => IntType(32))), zero(() => IntType(32)), 3)))))
    val ones = Build({
      val idx = ParamDef(IntType(32))
      CLambda(idx, Id(one))
    }, cols(positions), EagerType())
    val positionsHomog = ConcatFactory(positions, RepeatOnce(ones))
    MatrixMul(T, positionsHomog)
  }

  // n -> mat -> mat -> mat
  def GetPosedRelatives(nBones: ArithTypeT, poseParams: Expr, baseRelative: Expr): Expr = {
    val offset = ArithType(2) // should be 3
    Build({
      val i = ParamDef(IntType(32))
      CLambda(i, MakeRelative(IdxS(poseParams, IdxOffsetOp(ParamUse(i), offset, AlgoOp.Add)), IdxS(baseRelative, ParamUse(i))))
    }, nBones, EagerType()) // should be just nBone
  }

  def GetPosedRelativesFunction(nBones: ArithTypeT): Expr = {
    val poseParams = ParamDef(ArrayType(ArrayType(DoubleType(), 4), 6))
    val baseRelatives = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
    CLambda(poseParams, CLambda(baseRelatives, GetPosedRelatives(nBones, ParamUse(poseParams), ParamUse(baseRelatives))))
  }

  def Matrix3DUpdate(_m: Expr, s: Expr, e: Expr, _nm: Expr): Expr = {
    val i = ParamDef(IntType(32))
    val m = TypeChecker.check(_m)
    val nm = TypeChecker.check(_nm)
    Build(CLambda(i, {
      val isInRange = BinaryOp(
        BinaryOp(ParamUse(i), s, AlgoOp.Gt, isBool = true),
        BinaryOp(ParamUse(i), e, AlgoOp.Lt, isBool = true), AlgoOp.Andand, isBool = true)
      If(isInRange, Id(IdxS(nm, ParamUse(i))), Id(IdxS(m, ParamUse(i))))
    }), length(m))
  }

  // relatives_to_absolutes (relatives: Matrix[]) (parents: Vector)
  def RelativeToAbsolute(relatives: Expr, parents: Expr): Expr = {
    implicit val t = () => DoubleType()
    val init = relatives
    Ifold({
      val idx = ParamDef(IntType(32))
      val acc = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
      CLambda(idx, CLambda(acc,
        If(BinaryOp(IdxS(parents, ParamUse(idx)), minus(Constant(1, DoubleType())), AlgoOp.Eq, isBool = true),
          {
            val newMatrix = RepeatOnce(IdxS(relatives, ParamUse(idx)))
            Matrix3DUpdate(ParamUse(acc), ParamUse(idx), add(ParamUse(idx), Constant(1, IntType(32))), newMatrix)
          },
          {
            val newMatrix = RepeatOnce(MatrixMul(IdxS(ParamUse(acc), TypeConv(IdxS(parents, ParamUse(idx)), IntType(32))), IdxS(relatives, ParamUse(idx))))
            Matrix3DUpdate(ParamUse(acc), ParamUse(idx), add(ParamUse(idx), Constant(1, IntType(32))), newMatrix)
          }
        )))
    }, init, length(relatives))
  }

  // let get_skinned_vertex_positions (is_mirrored: Index) (n_bones: Cardinality) (pose_params: Matrix) (base_relatives: Matrix[]) (parents: Vector)
  // (inverse_base_absolutes: Matrix[]) (base_positions: Matrix) (weights: Matrix)
  def GetSkinnedVertexPositions(isMirrored: Expr, nBones: ArithTypeT, poseParams: Expr, baseRelatives: Expr, parents: Expr,
                                inverseBaseAbsolutes: Expr, basePosition: Expr, basePositionHomg: Expr, weights: Expr): Expr = {
    implicit val t = () => DoubleType()
    val relatives = Extern.getPosedRelatives(nBones, poseParams, baseRelatives, gen = false)
    val absolutes = RelativeToAbsolute(relatives, parents)

    val transforms = Map({
      val m = ParamDef(TupleType(ArrayType(ArrayType(DoubleType(), 4), 4), ArrayType(ArrayType(DoubleType(), 4), 4)))
      CLambda(m, MatrixMul(SelectS(ParamUse(m), 0), SelectS(ParamUse(m), 1)))
    }, Zip(Tuple(absolutes, inverseBaseAbsolutes)))

    val nVerts = length(IdxS(basePosition, zero(() => IntType(32))))
    val initPositions = Build({
      val i = ParamDef(IntType(32))
      CLambda(i,
        Build({
          val j = ParamDef(IntType(32))
          CLambda(j, Id(zero))
        }, 3, EagerType())
      )
    }, 3, EagerType())
    val positions = Ifold({
      val iTransform = ParamDef(IntType(32))
      val acc = ParamDef(ArrayType(ArrayType(t(), 3), 3))
      CLambda(iTransform, CLambda(acc, {
        val currPositions = MatrixMul(Slice(IdxS(transforms, ParamUse(iTransform)), zero(() => IntType(32)), 3), basePositionHomg)
        val w = Build({
          val i = ParamDef(IntType(32))
          CLambda(i, IdxS(weights, ParamUse(iTransform)))
        }, length(basePosition), EagerType())
        MatrixAdd(ParamUse(acc), MatrixMulElemwise(currPositions, w))
      }))
    }, initPositions, length(transforms).ae - 1)
    ApplyGlobalTransform(poseParams, positions)
    //    positions
  }

  def newValue(rows: Int, cols: Int) = {
    implicit val t: () => IntType = () => IntType(32)
    val row = ParamDef(IntType(32))
    val col = ParamDef(IntType(32))
    val image = ParamDef(ArrayType(ArrayType(DoubleType(), cols), rows))
    CLambda(image, CLambda(row, CLambda(col, {
      val prog = new Program()
      val a = IdxS(IdxS(ParamUse(image), sub(ParamUse(row), one)), sub(ParamUse(col), one))
      val b = IdxS(IdxS(ParamUse(image), sub(ParamUse(row), one)), ParamUse(col))
      val c = IdxS(IdxS(ParamUse(image), sub(ParamUse(row), one)), add(ParamUse(col), one))
      val d = IdxS(IdxS(ParamUse(image), ParamUse(row)), sub(ParamUse(col), one))
      val e = IdxS(IdxS(ParamUse(image), ParamUse(row)), ParamUse(col))
      val f = IdxS(IdxS(ParamUse(image), ParamUse(row)), add(ParamUse(col), one))
      val g = IdxS(IdxS(ParamUse(image), add(ParamUse(row), one)), sub(ParamUse(col), one))
      val h = IdxS(IdxS(ParamUse(image), add(ParamUse(row), one)), ParamUse(col))
      val i = IdxS(IdxS(ParamUse(image), add(ParamUse(row), one)), add(ParamUse(col), one))
      val sum = Seq(a, b, c, d, e, f, g, h, i).reduce((x: Expr, y: Expr) => prog << add(x, y))
      prog.build(div(sum, nine(() => DoubleType())))
    }
    )))
  }

  def blurForward(rows: Int, cols: Int) = {
    implicit val t: () => IntType = () => IntType(32)

    val image = ParamDef(ArrayType(ArrayType(DoubleType(), cols), rows))
    val row = ParamDef(IntType(32))
    val col = ParamDef(IntType(32))
    CLambda(image,
      Build(CLambda(row, Build(CLambda(col,
        If({
          Seq(gt(row, zero), lt(row, num(rows - 1)), gt(col, zero), lt(col, num(cols - 1))) reduce ((a, b) => andand(a, b))
        }, CFC(newValue(rows, cols), Seq(ParamUse(col), ParamUse(row), ParamUse(image))),
          Id(IdxS(IdxS(ParamUse(image), ParamUse(row)), ParamUse(col)))
        )
      ), cols, EagerType())), rows, EagerType())
    )
  }

  def blurBackward(rows: Int, cols: Int) = {
    implicit val t: () => IntType = () => IntType(32)

    val image = ParamDef(ArrayType(ArrayType(DoubleType(), cols), rows))
    val row = ParamDef(IntType(32))
    val col = ParamDef(IntType(32))
    CLambda(image,
      ConcatFactory(ConcatFactory(RepeatOnce(Build(CLambda(col, Id(IdxS(IdxS(ParamUse(image), zero), ParamUse(col)))), cols, EagerType())),
        Build(CLambda(row,
          ConcatFactory(ConcatFactory(RepeatOnce(Id(IdxS(IdxS(ParamUse(image), add(ParamUse(row), one)), zero))),
            Build(CLambda(col,
              CFC(newValue(rows, cols), Seq(add(ParamUse(col), one), add(ParamUse(row), one), ParamUse(image))))
              , cols - 2, EagerType())), RepeatOnce(Id(IdxS(IdxS(ParamUse(image), ParamUse(row)), num(cols - 1)))))),
          rows - 2, EagerType())), RepeatOnce(Build(CLambda(col, Id(IdxS(IdxS(ParamUse(image), num(rows - 1)), ParamUse(col)))), cols, EagerType())))
    )
  }

  def fftShortVersion(N: Int): CLambda = {
    val LEN: Int = ((N / 2) * (math.log(N) / math.log(2))).toInt
    val primal = ParamDef(ArrayType(TupleType(COMPLEX, DoubleType(), DoubleType()), N))
    val offset0 = ParamDef(ArrayType(IntType(32), LEN))
    val offset1 = ParamDef(ArrayType(IntType(32), LEN))
    val omegas = ParamDef(ArrayType(TupleType(COMPLEX, DoubleType(), DoubleType()), LEN))

    val FFT = CLambda(primal, CLambda(offset0, CLambda(offset1, CLambda(omegas, {
      val prog = new Program()

      val initDual = Permute({
        implicit val t = () => IntType(32)
        val idx = ParamDef(IntType(32))
        CLambda(idx, Extern.reverseBits(TypeConv(ParamUse(idx), IntType(32, unsigned = 1)), Constant(math.log(N) / math.log(2), IntType(32))))
      },
        Map({
          val idx = ParamDef(IntType(32))
          CLambda(idx, {
            Tuple(COMPLEX, Id(SelectS(IdxS(ParamUse(primal), ParamUse(idx)), 0)), Id(SelectS(IdxS(ParamUse(primal), ParamUse(idx)), 1)))
          })
        }, Range(zero(() => IntType(32)), one(() => IntType(32)), N), Some(EagerType()))
        , DestViewType())

      val InitDual = prog.add(initDual)

      val j = ParamDef(IntType(32))
      val acc = ParamDef(ArrayType(TupleType(COMPLEX, DoubleType(), DoubleType()), N))
      val reduceBody = {
        val prog = new Program()

        val us = {
          val k = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Map(CLambda(k, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), ParamUse(k)))
            prog.build(Id(IdxS(ParamUse(acc), IdxS(ParamUse(offset0), i))))
          }), Range(zero, one, N / 2), Some(EagerType()))
        }

        val ms = {
          val k = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Map(CLambda(k, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), ParamUse(k)))
            prog.build(complexMul(
              IdxS(ParamUse(omegas), i),
              IdxS(ParamUse(acc), IdxS(ParamUse(offset1), i))))
          }), Range(zero, one, N / 2), Some(EagerType()))
        }

        val Us = prog.add(us)
        val Ms = prog.add(ms)

        val dual0 = {
          val tup = ParamDef(TupleType(
            TupleType(COMPLEX, DoubleType(), DoubleType()),
            TupleType(COMPLEX, DoubleType(), DoubleType()))
          )
          implicit val t = () => IntType(32)
          Map(CLambda(tup, {
            complexAdd(SelectS(ParamUse(tup), 0), SelectS(ParamUse(tup), 1))
          }), Zip(Tuple(Us, Ms)), Some(EagerType()))
        }

        val dual1 = {
          val tup = ParamDef(TupleType(
            TupleType(COMPLEX, DoubleType(), DoubleType()),
            TupleType(COMPLEX, DoubleType(), DoubleType()))
          )
          implicit val t = () => IntType(32)
          Map(CLambda(tup, {
            complexSub(SelectS(ParamUse(tup), 0), SelectS(ParamUse(tup), 1))
          }), Zip(Tuple(Us, Ms)), Some(EagerType()))
        }

        val dualConcat = TypeChecker.check(ConcatFactory(dual0, dual1))

        val dualPermute = TypeChecker.check({
          val idx = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Permute(CLambda(idx, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), mod(ParamUse(idx), num(N / 2))))
            prog.build(If(lt(ParamUse(idx), num(N / 2)),
              Id(IdxS(ParamUse(offset0), i)),
              Id(IdxS(ParamUse(offset1), i)) // equivalent to concat(slide)
            ))
          }), dualConcat, DestViewType())
        })
        prog.build(dualPermute)
      }
      implicit val t = () => IntType(32)
      val r = Reduce(CLambda(acc, CLambda(j, reduceBody)), Materialize(InitDual), Range(zero, one, math.floor(math.log(N) / math.log(2)).toInt))
      prog.build(r)
    }))))
    FFT
  }

  def fftShortVersionWorseCase(N: Int): CLambda = {
    val LEN: Int = ((N / 2) * (math.log(N) / math.log(2))).toInt
    val primal = ParamDef(ArrayType(TupleType(COMPLEX, DoubleType(), DoubleType()), N))
    val offset0 = ParamDef(ArrayType(IntType(32), LEN))
    val offset1 = ParamDef(ArrayType(IntType(32), LEN))
    val omegas = ParamDef(ArrayType(TupleType(COMPLEX, DoubleType(), DoubleType()), LEN))

    val FFT = CLambda(primal, CLambda(offset0, CLambda(offset1, CLambda(omegas, {
      val prog = new Program()

      val map0 = prog << Map({
        val idx = ParamDef(IntType(32))
        CLambda(idx, {
          Tuple(COMPLEX, Id(SelectS(IdxS(ParamUse(primal), ParamUse(idx)), 0)), Id(SelectS(IdxS(ParamUse(primal), ParamUse(idx)), 1)))
        })
      }, Range(zero(() => IntType(32)), one(() => IntType(32)), N))

      val initDual = Materialize(Permute({ // permute is a source view
        implicit val t = () => IntType(32)
        val idx = ParamDef(IntType(32))
        CLambda(idx, Extern.reverseBits(TypeConv(ParamUse(idx), IntType(32, unsigned = 1)), Constant(math.log(N) / math.log(2), IntType(32))))
      }, map0))

      val InitDual = prog << initDual

      val j = ParamDef(IntType(32))
      val acc = ParamDef(ArrayType(TupleType(COMPLEX, DoubleType(), DoubleType()), N))
      val reduceBody = {
        val prog = new Program()

        val us = {
          val k = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Map(CLambda(k, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), ParamUse(k)))
            prog.build(Id(IdxS(ParamUse(acc), IdxS(ParamUse(offset0), i))))
          }), Range(zero, one, N / 2))
        }

        val ms = {
          val k = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Map(CLambda(k, {
            val prog = new Program()
            val i = prog << add(mul(ParamUse(j), num(N / 2)), ParamUse(k))
            val oi = prog << Materialize(IdxS(ParamUse(omegas), i))
            val accoi = prog << Materialize(IdxS(ParamUse(acc), IdxS(ParamUse(offset1), i)))
            prog.build(complexMulWorseCase(oi, accoi))
          }), Range(zero, one, N / 2))
        }

        val Us = prog.add(us)
        val Ms = prog.add(ms)

        val dual0 = prog << {
          val tup = ParamDef(TupleType(
            TupleType(COMPLEX, DoubleType(), DoubleType()),
            TupleType(COMPLEX, DoubleType(), DoubleType()))
          )
          implicit val t = () => IntType(32)
          Map(CLambda(tup, {
            complexAdd(SelectS(ParamUse(tup), 0), SelectS(ParamUse(tup), 1))
          }), Zip(Tuple(Us, Ms)))
        }

        val dual1 = prog << {
          val tup = ParamDef(TupleType(
            TupleType(COMPLEX, DoubleType(), DoubleType()),
            TupleType(COMPLEX, DoubleType(), DoubleType()))
          )
          implicit val t = () => IntType(32)
          Map(CLambda(tup, {
            complexSub(SelectS(ParamUse(tup), 0), SelectS(ParamUse(tup), 1))
          }), Zip(Tuple(Us, Ms)))
        }

        val dualConcat = prog << ConcatFactory(Materialize(dual0), Materialize(dual1))

        val dualPermute = Materialize(TypeChecker.check({
          val idx = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Permute(CLambda(idx, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), mod(ParamUse(idx), num(N / 2))))
            prog.build(If(lt(ParamUse(idx), num(N / 2)),
              Id(IdxS(ParamUse(offset0), i)),
              Id(IdxS(ParamUse(offset1), i)) // equivalent to concat(slide)
            ))
          }), dualConcat)
        }))
        prog.build(dualPermute)
      }
      implicit val t = () => IntType(32)
      val r = Reduce(CLambda(acc, CLambda(j, reduceBody)), InitDual, Range(zero, one, math.floor(math.log(N) / math.log(2)).toInt))
      prog.build(r)
    }))))
    FFT
  }

  def fft(N: Int): CLambda = {
    val LEN: Int = ((N / 2) * (math.log(N) / math.log(2))).toInt
    val primal = ParamDef(ArrayType(TupleType(COMPLEX, DoubleType(), DoubleType()), N))
    //    val p = ParamDef(ArrayType(IntType(32), LEN))
    //    val offset = ParamDef(ArrayType(IntType(32), LEN))
    //    val offset1 = ParamDef(ArrayType(IntType(32), LEN))
    val FFT = CLambda(primal, {
      val prog = new Program()

      val p = {
        implicit val t = () => IntType(32)
        Map({
          val idx = ParamDef(IntType(32))
          CLambda(idx, add(one, div(ParamUse(idx), Constant(N / 2, IntType(32)))))
        }, Range(zero, one, LEN))
      }

      val offset = {
        implicit val t = () => IntType(32)
        Map({
          val idx = ParamDef(IntType(32))
          CLambda(idx, {
            val prog = new Program()
            val j = prog.add(div(ParamUse(idx), Constant(N / 2, IntType(32))))
            val k = prog.add(mod(ParamUse(idx), Constant(N / 2, IntType(32))))
            val part0 = mul(div(k, pow(two, j)), pow(two, add(j, one)))
            val part1 = mod(k, pow(two, j))
            prog.build(add(part0, part1))
          })
        }, Range(zero, one, LEN))
      }

      val P = prog.add(p)
      val Offset0 = prog.add(offset)

      val offset1 = TypeChecker.check({
        implicit val t = () => IntType(32)
        Map({
          val idx = ParamDef(IntType(32))
          CLambda(idx, {
            add(IdxS(Offset0, ParamUse(idx)), div(leftShift(one, IdxS(P, ParamUse(idx))), two))
          })
        }, Range(zero, one, LEN))
      })

      val Offset1 = prog.add(offset1)

      val unityRoots = Map({
        val pi = ParamDef(IntType(32))
        CLambda(pi, {
          val unityStep = BinaryOp(one(() => IntType(32)), ParamUse(pi), AlgoOp.LeftShift)
          val prog = new Program()
          val theta = prog.add(BinaryOp(num(2 * math.Pi)(() => DoubleType()), TypeConv(unityStep, DoubleType()), AlgoOp.Div))
          prog.build(Tuple(COMPLEX, UnaryOp(theta, AlgoOp.Cos), UnaryOp(theta, AlgoOp.Sin)))
        })
      }, P)

      val init = {
        implicit val t = () => IntType(32)
        Map(CLambda(ParamDef(IntType(32)), Tuple(COMPLEX, Id(zero(() => DoubleType())), Id(zero(() => DoubleType())))),
          Range(zero, one, LEN))
      }
      val omegas = AccMap({
        val acc = ParamDef(ArrayType(TupleType(COMPLEX, DoubleType(), DoubleType()), LEN))
        val idx = ParamDef(IntType(32))
        CLambda(acc, CLambda(idx, {
          implicit val t = () => IntType(32)
          val x = div(ParamUse(idx), num(N / 2))
          val cond = eq(mod(ParamUse(idx), pow(two, x)), zero)
          If(cond, Tuple(COMPLEX, Id(one(() => DoubleType())), Id(zero(() => DoubleType()))), {
            val j = sub(ParamUse(idx), one)
            complexMul(IdxS(ParamUse(acc), j), IdxS(unityRoots, j))
          })
        }))
      }, init, {
        implicit val t = () => IntType(32)
        Range(zero, one, LEN)
      })

      val initDual = Permute({
        implicit val t = () => IntType(32)
        val idx = ParamDef(IntType(32))
        CLambda(idx, Extern.reverseBits(TypeConv(ParamUse(idx), IntType(32, unsigned = 1)), Constant(math.log(N) / math.log(2), DoubleType())))
      },
        Map({
          val idx = ParamDef(IntType(32))
          CLambda(idx, {
            Tuple(COMPLEX, Id(SelectS(IdxS(ParamUse(primal), ParamUse(idx)), 0)), Id(SelectS(IdxS(ParamUse(primal), ParamUse(idx)), 1)))
          })
        }, Range(zero(() => IntType(32)), one(() => IntType(32)), N))
      )

      val InitDual = prog.add(initDual)
      val Omegas = prog.add(omegas)

      val j = ParamDef(IntType(32))
      val acc = ParamDef(ArrayType(TupleType(COMPLEX, DoubleType(), DoubleType()), N))
      val reduceBody = {
        val prog = new Program()

        val us = {
          val idx = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Map(CLambda(idx, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), ParamUse(idx)))
            prog.build(Id(IdxS(ParamUse(acc), IdxS(Offset0, i))))
          }), Range(zero, one, N / 2))
        }

        val ms = {
          val idx = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Map(CLambda(idx, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), ParamUse(idx)))
            prog.build(complexMul(
              IdxS(Omegas, i),
              IdxS(ParamUse(acc), IdxS(Offset1, i))))
          }), Range(zero, one, N / 2))
        }

        val Us = prog.add(us)
        val Ms = prog.add(ms)

        val dual0 = {
          val tup = ParamDef(TupleType(
            TupleType(COMPLEX, DoubleType(), DoubleType()),
            TupleType(COMPLEX, DoubleType(), DoubleType()))
          )
          implicit val t = () => IntType(32)
          Map(CLambda(tup, {
            complexAdd(SelectS(ParamUse(tup), 0), SelectS(ParamUse(tup), 1))
          }), Zip(Tuple(Us, Ms)))
        }
        //
        val dual1 = {
          val tup = ParamDef(TupleType(
            TupleType(COMPLEX, DoubleType(), DoubleType()),
            TupleType(COMPLEX, DoubleType(), DoubleType()))
          )
          implicit val t = () => IntType(32)
          Map(CLambda(tup, {
            complexSub(SelectS(ParamUse(tup), 0), SelectS(ParamUse(tup), 1))
          }), Zip(Tuple(Us, Ms)))
        }

        val dualConcat = ConcatFactory(dual0, dual1)

        val dualPermute = TypeChecker.check({
          val idx = ParamDef(IntType(32))
          implicit val t = () => IntType(32)
          Permute(CLambda(idx, {
            val prog = new Program()
            val i = prog.add(add(mul(ParamUse(j), num(N / 2)), mod(ParamUse(idx), num(N / 2))))
            prog.build(If(lt(ParamUse(idx), num(N / 2)),
              Id(IdxS(Offset0, i)),
              Id(IdxS(Offset1, i)) // equivalent to concat(slide)
            ))
          }), dualConcat)
        })
        prog.build(dualPermute)
      }
      implicit val t = () => IntType(32)
      val r = Reduce(CLambda(acc, CLambda(j, reduceBody)), InitDual, Range(zero, one, math.floor(math.log(N) / math.log(2)).toInt))
      prog.build(r)
    })
    FFT
  }

  def oneMM(P: Int, Q: Int, R: Int)(implicit isTransposeLazy: Boolean = true) = {
    implicit val t = () => DoubleType()
    val A = ParamDef(ArrayType(ArrayType(t(), Q), P))
    val B = ParamDef(ArrayType(ArrayType(t(), R), Q))
    CLambda(Seq(A, B).reverse, MatrixMul(ParamUse(A), ParamUse(B)))
  }

  /** 2mm
   * alphas, beta: scalars
   * A: P * Q
   * B: Q * R
   * C: R * S
   * D: P * S
   * returns: E = alpha * A * B * C + beta * D
   */
  def twoMM(P: Int, Q: Int, R: Int, S: Int)(implicit isTransposeLazy: Boolean = true) = {
    implicit val t = () => DoubleType()
    val A = ParamDef(ArrayType(ArrayType(t(), Q), P))
    val B = ParamDef(ArrayType(ArrayType(t(), R), Q))
    val C = ParamDef(ArrayType(ArrayType(t(), S), R))
    val D = ParamDef(ArrayType(ArrayType(t(), S), P))
    val alpha = ParamDef(t())
    val beta = ParamDef(t())

    CLambda(Seq(alpha, beta, A, B, C, D).reverse, {
      MatrixAdd(
        MatrixMulScalar(MatrixMul(MatrixMul(ParamUse(A), ParamUse(B)), ParamUse(C)), ParamUse(alpha)),
        MatrixMulScalar(ParamUse(D), ParamUse(beta))
        //        ParamUse(A), ParamUse(B)
      )
      //        MatrixAdd(
      //          MatrixMulScalar(MatrixMul(MatrixMul(ParamUse(A), ParamUse(B)), ParamUse(C)), ParamUse(alpha)),
      //          MatrixMulScalar(ParamUse(D), ParamUse(beta)))
      //      val prog = new Program()
      //      val tmp = prog.add(MatrixMulScalar(MatrixMul2(ParamUse(A), ParamUse(B), SrcViewType()), ParamUse(alpha)))
      //      prog.build(MatrixAdd(MatrixMul2(tmp, ParamUse(C), SrcViewType()), MatrixMulScalar(ParamUse(D), ParamUse(beta))))
    })
  }

  def threeMM(P: Int, Q: Int, R: Int, S: Int, T: Int)(implicit isTransposeLazy: Boolean = true) = {
    implicit val t = () => DoubleType()
    val A = ParamDef(ArrayType(ArrayType(t(), Q), P))
    val B = ParamDef(ArrayType(ArrayType(t(), R), Q))
    val C = ParamDef(ArrayType(ArrayType(t(), S), R))
    val D = ParamDef(ArrayType(ArrayType(t(), S), T))

    CLambda(Seq(A, B, C, D).reverse, {
      val prog = new Program()
      val E = prog.add(MatrixMul(ParamUse(A), ParamUse(B)))
      val F = prog.add(MatrixMul(ParamUse(C), ParamUse(D)))
      prog.build(MatrixMul(E, F))
    })
  }

  def threeMMAllView(P: Int, Q: Int, R: Int, S: Int, T: Int)(implicit isTransposeLazy: Boolean = true) = {
    implicit val t = () => DoubleType()
    val A = ParamDef(ArrayType(ArrayType(t(), Q), P))
    val B = ParamDef(ArrayType(ArrayType(t(), R), Q))
    val C = ParamDef(ArrayType(ArrayType(t(), S), R))
    val D = ParamDef(ArrayType(ArrayType(t(), S), T))

    CLambda(Seq(A, B, C, D).reverse, {
      val prog = new Program()
      val E = prog.add(MatrixMul(ParamUse(A), ParamUse(B)))
      val F = prog.add(MatrixMul(ParamUse(C), ParamUse(D)))
      prog.build(MatrixMul(E, F))
    })
  }

  def constArray(value: Expr, length: ArithTypeT): Expr = {
    Build({
      val i = ParamDef(IntType(32))
      CLambda(i, value)
    }, length)
  }

  def stencil1D(vector: Expr, stencil: Expr): Expr = {
    implicit val t: () => Type = () => vector.t.asInstanceOf[ArrayTypeT].et
    val _stencil = TypeChecker.check(stencil)
    val len = _stencil.t.asInstanceOf[ArrayTypeT].len
    Map({
      val window = ParamDef(_stencil.t)
      CLambda(window, Lib.DotProd(ParamUse(window), _stencil))
    }, Slide(vector, len))
  }

  def blur1D(len: Int): CLambda = {
    implicit val t = () => DoubleType()
    val in = ParamDef(ArrayType(t(), len))
    CLambda(in, ConcatFactory(
      ConcatFactory(RepeatOnce(IdxS(ParamUse(in), zero(() => IntType(32)))),
        Map({
          val window = ParamDef(ArrayType(t(), 3))
          CLambda(window, div(VectorSum(ParamUse(window)), three))
        }, Slide(ParamUse(in), 3))),
      RepeatOnce(IdxS(ParamUse(in), num(len - 1)(() => IntType(32))))
    ))
  }

  def jacobi1D(len: Int, step: Int): CLambda = {
    implicit val t = () => DoubleType()
    val A = ParamDef(ArrayType(t(), len))
    CLambda(A, {
      Iterate(
        ParamUse(A), {
          val in = ParamDef(ArrayType(t(), len))
          CLambda(in, ConcatFactory(
            ConcatFactory(RepeatOnce(IdxS(ParamUse(in), zero(() => IntType(32)))),
              Map({
                val window = ParamDef(ArrayType(t(), 3))
                CLambda(window, div(VectorSum(ParamUse(window)), three))
              }, Slide(ParamUse(in), 3))),
            RepeatOnce(IdxS(ParamUse(in), num(len - 1)(() => IntType(32))))
          ))
        },
        Constant(step, IntType(32))
      )
    })
  }

  def jacobi1DNonIter(len: Int, step: Int): CLambda = {
    implicit val t = () => DoubleType()
    val in = ParamDef(ArrayType(t(), len))
    CLambda(in,
      ConcatFactory(
        ConcatFactory(RepeatOnce(Id(IdxS(ParamUse(in), zero(() => IntType(32))))),
          Materialize(Map({
            val window = ParamDef(ArrayType(t(), 3))
            CLambda(window, div(VectorSum(ParamUse(window)), three))
          }, Slide(ParamUse(in), 3), Some(SrcViewType())))),
        RepeatOnce(Id(IdxS(ParamUse(in), num(len - 1)(() => IntType(32)))))
      )
    )
  }

  def jacobi1DWorseCase(len: Int, step: Int): CLambda = {
    implicit val t = () => DoubleType()
    val in = ParamDef(ArrayType(t(), len))
    CLambda(in, {
      val prog = new Program()
      val slide = prog << Materialize(Slide(ParamUse(in), 3))
      prog.build(ConcatFactory(
        ConcatFactory(RepeatOnce(IdxS(ParamUse(in), zero(() => IntType(32)))),
          Map({
            val window = ParamDef(ArrayType(t(), 3))
            CLambda(window, div(VectorSum(ParamUse(window)), three))
          }, slide)),
        RepeatOnce(IdxS(ParamUse(in), num(len - 1)(() => IntType(32))))
      ))
    })
  }

  def jacobi2D(len: Int, step: Int): CLambda = {

    def stencilJacobi2D(rows: Int, cols: Int) = {
      implicit val t: () => IntType = () => IntType(32)

      def newValue(rows: Int, cols: Int) = {
        implicit val t: () => IntType = () => IntType(32)
        val row = ParamDef(IntType(32))
        val col = ParamDef(IntType(32))
        val image = ParamDef(ArrayType(ArrayType(DoubleType(), cols), rows))
        CLambda(image, CLambda(row, CLambda(col, {
          val prog = new Program()
          val sum = Seq(
            IdxS(IdxS(ParamUse(image), ParamUse(row)), ParamUse(col)),
            IdxS(IdxS(ParamUse(image), sub(ParamUse(row), one)), ParamUse(col)),
            IdxS(IdxS(ParamUse(image), add(ParamUse(row), one)), ParamUse(col)),
            IdxS(IdxS(ParamUse(image), ParamUse(row)), sub(ParamUse(col), one)),
            IdxS(IdxS(ParamUse(image), ParamUse(row)), add(ParamUse(col), one)),
          ) map (e => prog.add(e)) reduce ((x: Expr, y: Expr) => add(x, y))
          prog.build(div(sum, five(() => DoubleType())))
        }
        )))
      }

      val image = ParamDef(ArrayType(ArrayType(DoubleType(), cols), rows))
      val row = ParamDef(IntType(32))
      val col = ParamDef(IntType(32))
      CLambda(image,
        ConcatFactory(ConcatFactory(RepeatOnce(Build(CLambda(col, Id(IdxS(IdxS(ParamUse(image), zero), ParamUse(col)))), cols, EagerType())),
          Build(CLambda(row,
            ConcatFactory(ConcatFactory(RepeatOnce(Id(IdxS(IdxS(ParamUse(image), add(ParamUse(row), one)), zero))),
              Build(CLambda(col,
                CFC(newValue(rows, cols), Seq(add(ParamUse(col), one), add(ParamUse(row), one), ParamUse(image))))
                , cols - 2, EagerType())), RepeatOnce(Id(IdxS(IdxS(ParamUse(image), ParamUse(row)), num(cols - 1)))))),
            rows - 2, EagerType())), RepeatOnce(Build(CLambda(col, Id(IdxS(IdxS(ParamUse(image), num(rows - 1)), ParamUse(col)))), cols, EagerType())))
      )
    }

    implicit val t = () => DoubleType()
    val A = ParamDef(ArrayType(ArrayType(t(), len), len))
    CLambda(A,
      Iterate(
        ParamUse(A), {
          val in = ParamDef(ArrayType(ArrayType(t(), len), len))
          CLambda(in, CFC(stencilJacobi2D(len, len), ParamUse(in)))
        },
        Constant(step, IntType(32))
      )
    )
  }


  def conv1(implicit datat: () => Type): CLambda = {
    val wd = ParamDef(ArrayType(datat().asInstanceOf[CGenDataTypeT], 3))
    implicit val t = () => IntType(32)
    CLambda(wd, {
      val prog = new Program()
      val sum = Seq(
        IdxS(ParamUse(wd), one),
        IdxS(ParamUse(wd), zero),
        IdxS(ParamUse(wd), two),
      ) map (e => prog.add(e)) reduce ((x: Expr, y: Expr) => add(x, y))
      prog.build(div(sum, three(() => datat())))
    })
  }

  def conv2(implicit datat: () => Type): CLambda = {
    val wd = ParamDef(ArrayType(ArrayType(datat().asInstanceOf[CGenDataTypeT], 3), 3))
    implicit val t = () => IntType(32)
    CLambda(wd, {
      val prog = new Program()
      val sum = Seq(
        IdxS(IdxS(ParamUse(wd), zero), one),
        IdxS(IdxS(ParamUse(wd), one), zero),
        IdxS(IdxS(ParamUse(wd), one), one),
        IdxS(IdxS(ParamUse(wd), one), two),
        IdxS(IdxS(ParamUse(wd), two), one),
      ) map (e => prog.add(e)) reduce ((x: Expr, y: Expr) => add(x, y))
      prog.build(div(sum, five(() => datat())))
    })
  }

  def padS(row: Expr) = {
    val len = tt(row).asInstanceOf[ArrayTypeT].len
    ConcatFactory(RepeatS(1, IdxS(row, Constant(0, IntType(32)))),
      ConcatFactory(row, RepeatS(1, IdxS(row, Constant(len.ae.evalInt - 1, IntType(32)))), SrcViewType()),
      SrcViewType()
    )
  }

  def padS2D(mat: Expr) = transposeS(padS(transposeS(padS(mat))))

  def stencilWithSlide2(len: Int, conv: CLambda): CLambda = {
    implicit val t = () => DoubleType()

    val A = ParamDef(ArrayType(ArrayType(t(), len), len))
    CLambda(A, {
      val map = Map({
        val wds = ParamDef(ArrayType(ArrayType(ArrayType(t(), 3), 3), len - 2))
        CLambda(wds,
          Map({
            val wd = ParamDef(ArrayType(ArrayType(t(), 3), 3))
            CLambda(wd, CFC(conv, ParamUse(wd)))
          }, ParamUse(wds), Some(SrcViewType())))
      }, slideS2D(ParamUse(A), 3), Some(SrcViewType()))
      val pad2d = {
        implicit val t = () => IntType(32)
        ConcatFactory(ConcatFactory(
          RepeatOnce(Materialize(IdxS(ParamUse(A), zero))),
          Map({
            val row = ParamDef(ArrayType(DoubleType(), len - 2))
            CLambda(row, pad1D(ParamUse(row)))
          }, map, Some(EagerType()))),
          RepeatOnce(Materialize(IdxS(ParamUse(A), num(len - 1)))),
        )
      }
      pad2d
    })
  }

  def jacobi2DWithSlide2(len: Int, step: Int): CLambda = {
    val conv2: CLambda = {
      val wd = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
      implicit val t = () => IntType(32)
      CLambda(wd, {
        val prog = new Program()
        val sum = Seq(
          IdxS(IdxS(ParamUse(wd), zero), one),
          IdxS(IdxS(ParamUse(wd), one), zero),
          IdxS(IdxS(ParamUse(wd), one), one),
          IdxS(IdxS(ParamUse(wd), one), two),
          IdxS(IdxS(ParamUse(wd), two), one),
        ) map (e => prog.add(e)) reduce ((x: Expr, y: Expr) => add(x, y))
        prog.build(div(sum, five(() => DoubleType())))
      })
    }

    stencilWithSlide2(len, conv2)
  }

  def pad1D(e: Expr) = {
    val len = TypeChecker.check(e).t.asInstanceOf[ArrayTypeT].len
    implicit val t = () => IntType(32)
    ConcatFactory(ConcatFactory(
      RepeatOnce(Materialize(IdxS(e, zero))),
      Materialize(e)),
      RepeatOnce(Materialize(IdxS(e, num(len.ae.evalInt - 1)))))
  }

  def jacobi2DWorseCase(len: Int, step: Int): CLambda = {
    implicit val t = () => DoubleType()

    val conv2: CLambda = {
      val wd = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
      implicit val t = () => IntType(32)
      CLambda(wd, {
        val prog = new Program()
        val sum = Seq(
          IdxS(IdxS(ParamUse(wd), zero), one),
          IdxS(IdxS(ParamUse(wd), one), zero),
          IdxS(IdxS(ParamUse(wd), one), one),
          IdxS(IdxS(ParamUse(wd), one), two),
          IdxS(IdxS(ParamUse(wd), two), one),
        ) map (e => prog.add(e)) reduce ((x: Expr, y: Expr) => add(x, y))
        prog.build(div(sum, five(() => DoubleType())))
      })
    }

    val A = ParamDef(ArrayType(ArrayType(t(), len), len))
    CLambda(A, {
      // pad2d
      val prog = new Program()
      val s2 = prog << Materialize(slideS2D(ParamUse(A), 3))
      val map = Map({
        val wds = ParamDef(ArrayType(ArrayType(ArrayType(t(), 3), 3), len - 2))
        CLambda(wds,
          Map({
            val wd = ParamDef(ArrayType(ArrayType(t(), 3), 3))
            CLambda(wd, CFC(conv2, ParamUse(wd)))
          }, ParamUse(wds)))
      }, s2)

      val pad2d = {
        implicit val t = () => IntType(32)
        ConcatFactory(ConcatFactory(
          RepeatOnce(IdxS(ParamUse(A), zero)),
          Map({
            val row = ParamDef(ArrayType(DoubleType(), len - 2))
            CLambda(row, pad1D(ParamUse(row)))
          }, map)),
          RepeatOnce(IdxS(ParamUse(A), num(len - 1))),
        )
      }
      prog.build(pad2d)
    })
  }

  def seidel2D(len: Int, step: Int): CLambda = {
      val conv2: CLambda = {
        val wd = ParamDef(ArrayType(ArrayType(DoubleType(), 3), 3))
        implicit val t = () => IntType(32)
        CLambda(wd, {
          val prog = new Program()
          val sum = Seq(
            IdxS(IdxS(ParamUse(wd), zero), zero),
            IdxS(IdxS(ParamUse(wd), zero), one),
            IdxS(IdxS(ParamUse(wd), zero), two),
            IdxS(IdxS(ParamUse(wd), one), zero),
            IdxS(IdxS(ParamUse(wd), one), one),
            IdxS(IdxS(ParamUse(wd), one), two),
            IdxS(IdxS(ParamUse(wd), two), zero),
            IdxS(IdxS(ParamUse(wd), two), one),
            IdxS(IdxS(ParamUse(wd), two), two),
          ) map (e => prog.add(e)) reduce ((x: Expr, y: Expr) => add(x, y))
          prog.build(div(sum, nine(() => DoubleType())))
        })
      }

      stencilWithSlide2(len, conv2)
  }
}

object TupleStructName {
  val COMPLEX = "complex"
}

object ComplexTuple {
  def apply(v0: Expr, v1: Expr, vt: Option[ViewTypeT] = None) = vt match {
    case Some(vt) => Tuple(COMPLEX, v0, v1, vt)
    case None => Tuple(COMPLEX, v0, v1)
  }
}

object Intrinsics {
  /**
   * Produces an intrinsic call that computes a dot product.
   * @param left The left-hand operand vector.
   * @param right The right-hand operand vector.
   * @return An intrinsic call that computes the dot product of `left` and `right`.
   */
  def DotProd(left: Expr, right: Expr): IntrinsicCallExpr = {
    val _left = TypeChecker.check(left)
    val _right = TypeChecker.check(right)
    val n = Lib.length(_left)
    val scalar_t = Lib.elementType(_left)
    // TODO: assert that `right`'s length is equal to `left`'s, ditto for their element types.
    IntrinsicCall(
      "vector.dot_product",
      Seq(left, right).reverse,
      Seq(ArrayTypeVar(scalar_t, n), ArrayTypeVar(scalar_t, n)).reverse,
      scalar_t)
  }

  /**
   * Produces an intrinsic call that represents a matrix transpose.
   * @param matrix The matrix to transpose.
   * @return An intrinsic call that transposes `matrix`.
   */
  def MatrixTranspose(matrix: Expr): IntrinsicCallExpr = {
    val mat = TypeChecker.check(matrix)
    val n = Lib.rows(mat)
    val m = Lib.cols(mat)
    val t = Lib.elementType(mat)
    IntrinsicCall(
      "matrix.transpose",
      Seq(matrix),
      Seq(ArrayTypeVar(ArrayType(t, m), n)),
      ArrayType(ArrayType(t, n), m))
  }

  /**
   * Produces an intrinsic call that represents a matrix multiplication.
   * @param left The left-hand operand matrix.
   * @param right The right-hand operand matrix.
   * @param leftTransposed States if `left` is to be transposed prior to the multiplication.
   * @param rightTransposed States if `right` is to be transposed prior to the multiplication.
   * @return An intrinsic call that multiplies `left` and `right`.
   */
  def MatrixMul(left: Expr,
                right: Expr,
                leftTransposed: Boolean = false,
                rightTransposed: Boolean = false): IntrinsicCallExpr = {

    val lhs = TypeChecker.check(left)
    val rhs = TypeChecker.check(right)
    val elemType = Lib.elementType(lhs)
    // TODO: assert that `left` and `right` have compatible element types and dimensions.
    val n = Lib.rows(lhs)
    val m = Lib.cols(lhs)
    val k = Lib.cols(rhs)
    val l = Lib.rows(rhs)
    val dims = (leftTransposed, rightTransposed) match {
      case (false, false) => (n, m, k)
      case (false, true) => (n, m, l)
      case (true, false) => (m, n, k)
      case (true, true) => (m, n, l)
    }
    implicit val t: () => Type = () => IntType(32)
    def bool2int(b: Boolean): Int = if (b) 1 else 0

    IntrinsicCall(
      "matrix.multiply",
      Seq(
        Lib.num(bool2int(leftTransposed)),
        Lib.num(bool2int(rightTransposed)),
        left,
        right).reverse,
      Seq(
        IntTypeVar(32),
        IntTypeVar(32),
        ArrayTypeVar(ArrayType(elemType, m), n),
        ArrayTypeVar(ArrayType(elemType, k), l)).reverse,
      ArrayType(ArrayType(elemType, dims._3), dims._1))
  }
}

object Extern {
  def qtimesv(q: Expr, l: Expr, v: Expr): ExternFunctionCallExpr = {
    val d = 3;
    val td = ((d) * ((d) + (1))) / (2)
    ExternFunctionCall("qtimesv",
      Seq(q, l, v).reverse,
      Seq(ArrayTypeVar(DoubleType(), d), ArrayTypeVar(DoubleType(), td), ArrayTypeVar(DoubleType(), d)).reverse,
      ArrayType(DoubleType(), d))
  }

  def getPosedRelatives(nBones: ArithTypeT, poseParams: Expr, baseRelative: Expr, gen: Boolean): ExternFunctionCallExpr = {
    val poseParamsDef = ParamDef(ArrayType(ArrayType(DoubleType(), 4), 6))
    val baseRelativesDef = ParamDef(ArrayType(ArrayType(ArrayType(DoubleType(), 4), 4), 4))
    val body = GetPosedRelatives(nBones, poseParams, baseRelative)
    val tree = CLambda(poseParamsDef, CLambda(baseRelativesDef, body))
    if (gen) {
      CCodeGen(DPSPass(tree)).genCode("get_posed_relatives")
    }
    ExternFunctionCall("get_posed_relatives",
      Seq(poseParams, baseRelative).reverse,
      Seq(ArrayTypeVar(ArrayType(DoubleType(), 4), 6), ArrayTypeVar(ArrayType(ArrayType(DoubleType(), 4), 4), 4)).reverse,
      TypeChecker.check(body).t
    )
  }

  def reverseBits(x: Expr, P: Expr): ExternFunctionCallExpr = {
    ExternFunctionCall("reverse_bits",
      Seq(x, P).reverse,
      Seq(IntTypeVar(32, unsigned = 1), IntTypeVar(32)).reverse,
      IntType(32)
    )
  }

  def MatrixTranspose(matrix: Expr, func_name: String = "idiom_transpose"): ExternFunctionCallExpr = {
    val n = Lib.rows(matrix)
    val m = Lib.cols(matrix)
    val elemType = Lib.elementType(matrix)
    val int_t = IntType(32)
    ExternFunctionCall(
      func_name,
      Seq(TypeArith(n, int_t), TypeArith(m, int_t), matrix).reverse,
      Seq(
        IntTypeVar(32),
        IntTypeVar(32),
        ArrayTypeVar(ArrayType(elemType, m), n),
      ).reverse,
      ArrayType(ArrayType(elemType, n), m))
  }

  def MatrixMul(left: Expr,
                right: Expr,
                leftTransposed: Boolean = false,
                rightTransposed: Boolean = false): ExternFunctionCallExpr = {
    val n = Lib.rows(left)
    val m = Lib.cols(left)
    val k = Lib.cols(right)
    val l = Lib.rows(right)
    val dims = (leftTransposed, rightTransposed) match {
      case (false, false) => (n, m, k)
      case (false, true) => (n, m, l)
      case (true, false) => (m, n, k)
      case (true, true) => (m, n, l)
    }
    implicit val t: () => Type = () => IntType(32)
    def bool2int(b: Boolean): Int = if (b) 1 else 0

    ExternFunctionCall(
      "matrix_mul",
      Seq(
        TypeArith(bool2int(leftTransposed), t()),
        TypeArith(bool2int(rightTransposed), t()),
        left,
        right,
        TypeArith(dims._1, t()),
        TypeArith(dims._2, t()),
        TypeArith(dims._3, t())).reverse,
      Seq(
        IntTypeVar(32),
        IntTypeVar(32),
        ArrayTypeVar(ArrayType(DoubleType(), m), n),
        ArrayTypeVar(ArrayType(DoubleType(), k), l),
        IntTypeVar(32),
        IntTypeVar(32),
        IntTypeVar(32)).reverse,
      ArrayType(ArrayType(DoubleType(), dims._3), dims._1))
  }

  def bool2int(b: Boolean): Int = if (b) 1 else 0

  def GEMM(alpha: Expr,
           A: Expr,
           B: Expr,
           beta: Expr,
           C: Expr,
           ATransposed: Boolean = false,
           BTransposed: Boolean = false): ExternFunctionCallExpr = {
    val n = Lib.rows(A)
    val m = Lib.cols(A)
    val k = Lib.rows(B)
    val l = Lib.cols(B)
    val dims = (ATransposed, BTransposed) match {
      case (false, false) => (n, m, k)
      case (false, true) => (n, m, l)
      case (true, false) => (m, n, k)
      case (true, true) => (m, n, l)
    }
    implicit val t: () => Type = () => IntType(32)

    ExternFunctionCall(
      "idiom_dgemm",
      Seq(
        TypeArith(bool2int(ATransposed), t()),
        TypeArith(bool2int(BTransposed), t()),
        TypeArith(dims._1, t()),
        TypeArith(dims._2, t()),
        TypeArith(dims._3, t()),
        alpha,
        A,
        B,
        beta,
        C).reverse,
      Seq(
        IntTypeVar(32),
        IntTypeVar(32),
        IntTypeVar(32),
        IntTypeVar(32),
        IntTypeVar(32),
        DoubleTypeVar(),
        ArrayTypeVar(ArrayType(DoubleType(), m), n),
        ArrayTypeVar(ArrayType(DoubleType(), l), k),
        DoubleTypeVar(),
        ArrayTypeVar(ArrayType(DoubleType(), dims._3), dims._1)).reverse,
      ArrayType(ArrayType(DoubleType(), dims._3), dims._1))
  }

  def GEMV(alpha: Expr,
           A: Expr,
           B: Expr,
           beta: Expr,
           C: Expr,
           ATransposed: Boolean = false): ExternFunctionCallExpr = {
    val n = Lib.length(C)
    val m = Lib.length(B)
    implicit val t: () => Type = () => IntType(32)
    val dims = if (ATransposed) (n, m) else (m, n)

    ExternFunctionCall(
      "idiom_dgemv",
      Seq(
        TypeArith(bool2int(ATransposed), t()),
        TypeArith(n, t()),
        TypeArith(m, t()),
        alpha,
        A,
        B,
        beta,
        C).reverse,
      Seq(
        IntTypeVar(32),
        IntTypeVar(32),
        IntTypeVar(32),
        DoubleTypeVar(),
        ArrayTypeVar(ArrayType(DoubleType(), dims._1), dims._2),
        ArrayTypeVar(DoubleType(), m),
        DoubleTypeVar(),
        ArrayTypeVar(DoubleType(), n)).reverse,
      ArrayType(DoubleType(), n))
  }

  def DoubleDotProd(left: Expr, right: Expr): ExternFunctionCallExpr =
    DotProd(left, right, "idiom_ddot")(() => DoubleType())

  def DotProd(left: Expr, right: Expr, func_name: String)(implicit t: () => CScalarTypeT): ExternFunctionCallExpr = {
    val n = Lib.length(left)
    val scalar_t = t()
    val int_t = () => IntType(32)
    val int_var_t = IntTypeVar(32)
    ExternFunctionCall(func_name,
      Seq(TypeArith(n, int_t()), left, Lib.one(int_t), right, Lib.one(int_t)).reverse,
      Seq(
        int_var_t,
        ArrayTypeVar(scalar_t, n),
        int_var_t,
        ArrayTypeVar(scalar_t, n),
        int_var_t).reverse,
      scalar_t)
  }

  def AXPY(alpha: Expr, x: Expr, y: Expr): ExternFunctionCallExpr = {
    implicit val t = DoubleType
    val n = Lib.length(x)
    val scalar_t = t()
    val int_t = () => IntType(32)
    val int_var_t = IntTypeVar(32)
    ExternFunctionCall("idiom_axpy",
      Seq(TypeArith(n, int_t()), alpha, x, Lib.one(int_t), y, Lib.one(int_t)).reverse,
      Seq(
        int_var_t,
        DoubleTypeVar(),
        ArrayTypeVar(scalar_t, n),
        int_var_t,
        ArrayTypeVar(scalar_t, n),
        int_var_t).reverse,
      x.t)
  }

  def memset(constant: Byte, arrayType: ArrayTypeT): ExternFunctionCallExpr = {
    implicit val int_t: Type = IntType(32)
    ExternFunctionCall("idiom_memset",
      Seq(Constant(constant, int_t), BinaryOp(sizeof(arrayType.et), TypeArith(arrayType.len, int_t), AlgoOp.Mul)).reverse,
      Seq(
        IntTypeVar(32),
        IntTypeVar(32)).reverse,
      arrayType)
  }

  def sizeof(t: Type)(implicit int_t: Type): Expr = {
    t match {
      case DoubleType() => Constant(8, int_t)
      case IntType(ArithType(ae), _) if ae.isEvaluable =>
        Constant(math.ceil(math.log(ae.evalDouble) / math.log(2)), int_t)
      case _ => throw new Exception(s"Cannot measure size of unsupported type $t")
    }
  }
}
