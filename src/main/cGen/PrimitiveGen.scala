package cGen

import core.util.ToFile
import org.junit.Test

import java.lang.Character.isLetter
import scala.collection.immutable
import scala.collection.immutable.ListSet
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/** According to the provided signatures, automatically generates the corresponding
 * CGenExpr scala code.
 */
object PrimitiveGenApp extends App {
  val sigs = Seq(
    "Idx :: a^DataT |-> b^IntT |-> n^NatT |-> v^ViewT |-> [a]n -> b -v> a",
    "IdxDest :: a^DataT |-> b^IntT |-> n^NatT |-> Addr[[a]n] -> b -S> Addr[a]",
    "UnIdxDest :: a^DataT |-> b^IntT |-> n^NatT |-> Addr[a] -> b -S> Addr[[a]n]", // Build(i -> if(i==j, d, None)
    "Fst   :: a^DataT |-> b^DataT |-> v^ViewT |-> (a, b) -v> a",
    "Snd   :: a^DataT |-> b^DataT |-> v^ViewT |-> (a, b) -v> b",
    "FstDest   :: a^DataT |-> b^DataT |-> Addr[(a, b)] -S> Addr[a]",
    "SndDest   :: a^DataT |-> b^DataT |-> Addr[(a, b)] -S> Addr[b]",
    "UnFst :: a^DataT |-> b^DataT |-> a -D> (a, b)",
    "UnSnd :: a^DataT |-> b^DataT |-> b -D> (a, b)",
    "UnFstDest :: a^DataT |-> b^DataT |-> Addr[a] -S> Addr[(a, b)]",
    "UnSndDest :: a^DataT |-> b^DataT |-> Addr[b] -S> Addr[(a, b)]",
    "Add   :: a^DataT |-> (a, a) -E> a",
    "Id    :: a^DataT |-> a -E> a",
    "Nowhere :: a^DataT |-> a", // a dest represents no where
    "Dummy :: VoidT", // do nothing
    "M :: a^DataT |-> a -E> a",
    "Concat :: a^DataT |-> n^NatT |-> m^NatT |-> v^ViewT |-> ([a]n, [a]m) -v> [a]n+m",
    "RollingSlide :: a^DataT |-> n^NatT |-> w^NatT |-> v^ViewT |-> [a]n -v> [[a]w]n-w+1",
    "LoopInvariant:: a^DataT |-> a -E> a",
    "Unrepeat :: a^DataT |-> n^NatT |-> Addr[[a]n] -S> Addr[a]",
  )
  sigs.map(SignatureParser.apply).foreach(p => {
    val name = p._1
    val code = p._2
    new ToFile {
      override def toStringIterator: Iterator[String] = Seq(code).toIterator
    }.toFile(s"$name.scala", "./src/main/cGen/primitive")
  })
}

object SignatureParser {
  trait Type {
    val toName = this.getClass.getSimpleName
  }

  private case class UnknownType() extends Type

  private case class CGenDataType() extends Type {
  }

  private case class TupleType(t0: Type, t1: Type) extends Type

  private case class AddressType(t: Type) extends Type

  private case class VoidType() extends Type

  private case class IntType() extends Type

  private case class ArithType() extends Type

  private case class AddArithType(t0: Type, t1: Type) extends Type

  private case class SubArithType(t0: Type, t1: Type) extends Type

  private case class ArrayType(et: Type, len: Type) extends Type

  private case class TypeVar(name: String, t: Type) extends Type

  private case class Constant(lit: Char) extends Type


  trait ViewTypeT extends Type

  private case class ViewType() extends ViewTypeT

  private case class EagerType() extends ViewTypeT

  private case class DestViewType() extends ViewTypeT

  private case class SrcViewType() extends ViewTypeT

  private case class NoEffectType() extends ViewTypeT


  private case class TypeFunType(tv: TypeVar, b: Type) extends Type

  private case class FunType(t: Type, b: Type, view: Type) extends Type

  def visit(expr: Type, pre: Type => Unit): Unit = {
    pre(expr)
    expr match {
      case TypeFunType(tv, b) => visit(tv, pre); visit(b, pre)
      case FunType(t, b, view) => visit(t, pre); visit(b, pre); visit(view, pre)
      case TupleType(t0, t1) => visit(t0, pre); visit(t1, pre)
      case ArrayType(t0, t1) => visit(t0, pre); visit(t1, pre)
      case AddressType(t) => visit(t, pre)
      case AddArithType(t0, t1) => visit(t0, pre); visit(t1, pre)
      case SubArithType(t0, t1) => visit(t0, pre); visit(t1, pre)
      case _ =>
    }
  }

  def rebuild(t: Type, pre: Type => Type, post: Type => Type = t => t): Type = {
    val newT = pre(t)
    val res = newT match {
      case TypeFunType(tv, b) => TypeFunType(rebuild(tv, pre, post).asInstanceOf[TypeVar], rebuild(b, pre, post))
      case FunType(t, b, view) => FunType(rebuild(t, pre, post), rebuild(b, pre, post), rebuild(view, pre, post))
      case TupleType(t0, t1) => TupleType(rebuild(t0, pre, post), rebuild(t1, pre, post))
      case ArrayType(t0, t1) => ArrayType(rebuild(t0, pre, post), rebuild(t1, pre, post))
      case AddressType(t) => AddressType(rebuild(t, pre, post))
      case AddArithType(t0, t1) => AddArithType(rebuild(t0, pre, post), rebuild(t1, pre, post))
      case SubArithType(t0, t1) => SubArithType(rebuild(t0, pre, post), rebuild(t1, pre, post))
      case other => other
    }
    post(res)
  }

  class MyIterator[T](private val _arr: Seq[T]) {
    private var it = -1
    private val arr: ArrayBuffer[T] = ArrayBuffer[T]() ++ _arr

    def next(): T = {
      it += 1
      arr(it)
    }

    def hasNext(): Boolean = it + 1 < arr.length

    def hasNext(x: Int): Boolean = it + x < arr.length

    def head: T = arr(it + 1)

    def head(x: Int): T = arr(it + x)
  }

  /**
   * @param s , a signature such as "UnFst :: y^DataT |-> x^DataT |-> x -D> (x, y)"
   * @return the code that you may want to generate as a CGenExpr
   */
  def apply(s: String): (String, String) = {
    val ss = s.filter(!_.isWhitespace)
    implicit val it: MyIterator[Char] = new MyIterator[Char](ss.toSeq)
    val name = parseName().mkString("")
    var t = parseBody()
    t = solveUnknownTypes(t)
    println(t)

    (name, genCode(name, t, s))
  }

  /** Given a TypeFunType, say x^T |-> x -> x, the second and the third x will have a unknown type
   * since, they don't have supertype annotation "^T". But we can still infer their type based on
   * the first x. This function make sure their type are being determined.
   */
  def solveUnknownTypes(t: Type): Type = {
    val binding = mutable.HashMap[String, Type]()
    rebuild(t, {
      case tft@TypeFunType(tv, _) => binding += (tv.name -> tv.t); tft
      case TypeVar(name, UnknownType()) => TypeVar(name, binding(name))
      case other => other
    })
  }


  def genCode(name: String, e: Type, origin: String): String = {
    val params = gatherLamParams(e)
    val paramsTypeVars = {
      var res = ListSet[TypeVar]() // use set to remove duplicates
      params.foreach(p => visit(p, {
        case tv@TypeVar(_, _) => res = res + tv
        case _ =>
      }))
      res
    }
    val typeParamsTypeVars = gatherTypeLamParams(e).asInstanceOf[Seq[TypeVar]]

    // Check the if the input is valid
    params.foreach(p => visit(p, {
      case tv@TypeVar(name, t) if !typeParamsTypeVars.contains(tv) =>
        throw new RuntimeException(s"lambda should only used param from typelambda, Params: $params, TypeParams: $typeParamsTypeVars")
      case _ =>
    }))


    val typeParamsButNotParams: Seq[TypeVar] = typeParamsTypeVars.filter(tv => !paramsTypeVars.contains(tv))
    println("params: " + params)
    println("paramsTypeVars: " + paramsTypeVars)
    println("typeParamsTypeVars: " + typeParamsTypeVars)
    println("typeParamsButNotParams: " + typeParamsButNotParams)

    implicit val bindParams: Map[Type, String] =
      params.zipWithIndex.map(p => (p._1, s"in${p._2}tv")).toMap ++ // bind params to names in the generated code
        typeParamsButNotParams.map(p => (p, s"${p.name}")).toMap ++ // bind type params
        paramsTypeVars.map(tv => (tv, s"${tv.name}tv"))
    println(bindParams)

    // equations used inside the "object" code
    val eqs = paramsTypeVars.
      map(tv => (s"${getBinding(tv)}", s"${toScalaTypeVar(tv.t)}")) ++ params.map(tv => (s"${getBinding(tv)}", s"${toScalaTypeVar(tv)}")).
      filter(p => p._1 != p._2) // removed the redundant eqs where a = a;

    val importCode =
      s"""|// DON'T MODIFY. THIS CODE IS GENERATED AUTOMATICALLY
          |package cGen.primitive
          |
          |import cGen._
          |import core._
          |""".stripMargin

    val caseCode =
      s"""
         |case class ${name}Expr(innerIR: Expr, types: Seq[Type]) extends CGenExpr {
         |  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): ${name}Expr = ${name}Expr(newInnerIR, newTypes)
         |  override def argsBuild(args: Seq[Expr]): CGenExpr =
         |    ${name}(${(Seq.range(0, params.length).map(idx => s"args($idx)") ++ typeParamsButNotParams.zipWithIndex.map(p => s"types(${p._2}).asInstanceOf[${p._1.t.toName}T]")).mkString(", ")})
         |}
         |""".stripMargin

    val innerIR = {
      if (params.isEmpty) {
        s"Value(outt)"
      } else {
        s"FunctionCall(TypeFunctionCall(BuiltinTypeFunction(TypeFunType(Seq(${params.map(getBinding).mkString(", ")}), CFunType(Seq(${params.map(getBinding).mkString(", ")}), outt, ${toScalaViewTypeConstruct(getRightMostViewType(e))}))), Seq(${params.zipWithIndex.map(p => s"in${p._2}.t").mkString(", ")})), Seq(${params.zipWithIndex.map(p => s"in${p._2}").mkString(", ")}))"
      }
    }

    val unapply = {
      if (params.isEmpty && typeParamsButNotParams.isEmpty) {
        s"def unapply(expr: ${name}Expr): Some[(Type)] = Some((expr.t))"
      } else {
        s"""
           |  def unapply(expr: ${name}Expr): Some[(${(params.map(_ => "Expr") ++ typeParamsButNotParams.map(p => s"${p.t.toName}T")).mkString(", ")}, Type)] =
           |    Some((${(params.zipWithIndex.map(p => s"expr.args(${p._2})") ++ typeParamsButNotParams.zipWithIndex.map(p => s"expr.types(${p._2}).asInstanceOf[${p._1.t.toName}T]")).mkString(", ")}, expr.t))
        """.stripMargin
      }
    }

    val objectCode =
      s"""
         |// $origin
         |object $name {
         |  def apply(${(params.zipWithIndex.map(p => s"in${p._2}: Expr") ++ typeParamsButNotParams.map(p => s"${getBinding(p)}: ${p.t.toName}T")).mkString(", ")}): ${name}Expr = ${name}Expr({
         |    ${eqs.map(p => s"val ${p._1} = ${p._2}").mkString("\n    ")}
         |    val outt = ${toScalaType(getOutType(e))}
         |    ${innerIR}
         |  }, Seq(${typeParamsButNotParams.map(_.name).mkString(", ")}))
         |  $unapply
         |}
         |""".stripMargin

    importCode + caseCode + objectCode
  }

  def toScalaTypeVar(t: Type)(implicit binding: Map[Type, String]): String = t match {
    case TupleType(t0, t1) => s"TupleTypeVar(${toScalaTypeVar(t0)}, ${toScalaTypeVar(t1)})"
    case AddressType(t) => s"AddressTypeVar(${toScalaTypeVar(t)})"
    case ArrayType(et, len) => s"ArrayTypeVar(${toScalaTypeVar(et)}, ${toScalaTypeVar(len)})"
    case IntType() => s"IntTypeVar(32)"
    case TypeVar(_, _) => s"${getBinding(t)}"
    case AddArithType(t0, t1) => s"${toScalaTypeVar(t0)} + ${toScalaTypeVar(t1)}"
    case SubArithType(t0, t1) => s"${toScalaTypeVar(t0)} - ${toScalaTypeVar(t1)}"
    case ArithType() => "ArithTypeVar()"
    case CGenDataType() => "CGenDataTypeVar()"
    case VoidType() => "VoidTypeVar()"
  }

  def toScalaType(t: Type)(implicit binding: Map[Type, String]): String = t match {
    case TupleType(t0, t1) => s"TupleType(${toScalaType(t0)}, ${toScalaType(t1)})"
    case AddressType(t) => s"AddressType(${toScalaType(t)})"
    case ArrayType(et, len) => s"ArrayType(${toScalaType(et)}, ${toScalaType(len)})"
    case IntType() => s"IntType(32)"
    case TypeVar(_, _) => getBinding(t)
    case AddArithType(t0, t1) => s"${toScalaType(t0)}${if (t0.isInstanceOf[TypeVar]) ".ae" else ""} + ${toScalaType(t1)}${if (t0.isInstanceOf[TypeVar]) ".ae" else ""}"
    case SubArithType(t0, t1) => s"${toScalaType(t0)}${if (t0.isInstanceOf[TypeVar]) ".ae" else ""} - ${toScalaType(t1)}${if (t0.isInstanceOf[TypeVar]) ".ae" else ""}"
    case ArithType() => "ArithType()"
    case CGenDataType() => "CGenDataType()"
    case Constant(lit) => lit.toString
    case VoidType() => "VoidType()"
  }

  def toScalaViewTypeConstruct(t: Type)(implicit binding: Map[Type, String]): String = t match {
    case tv@TypeVar(_, _) => s"${getBinding(tv)}"
    case _: ViewTypeT => s"${t.toName}()"
  }

  def getRightMostViewType(e: Type)(implicit preVt: Option[Type] = None): Type = e match {
    case TypeFunType(_, b) => getRightMostViewType(b)
    case FunType(_, b, vt) => getRightMostViewType(b)(Some(vt))
    case _ => preVt.get
  }

  def getBinding(expr: Type)(implicit binding: Map[Type, String]) = {
    binding.get(expr) match {
      case Some(s) => s
      case None =>
        throw new RuntimeException(s"fail to get string binding for $expr")
    }
  }

  def getOutType(e: Type): Type = e match {
    case FunType(_, b, _) => getOutType(b)
    case TypeFunType(_, b) => getOutType(b)
    case other => other
  }

  def gatherLamParams(expr: Type): Seq[Type] = {
    var res = Seq[Type]()
    visit(expr, {
      case FunType(t, _, _) => res = res :+ t
      case _ =>
    })
    res
  }

  def gatherTypeLamParams(expr: Type): Seq[Type] = {
    var res = Seq[Type]()
    visit(expr, {
      case TypeFunType(t, _) => res = res :+ t
      case _ =>
    })
    res
  }


  /** get the name of the primitive
   */
  def parseName()(implicit it: MyIterator[Char]): Seq[Char] = {
    if (it.hasNext) {
      val c = it.next()
      if (c.isLetter) Seq(c) ++ parseName()
      else if (c == ':' && it.hasNext && it.next() == ':') Seq()
      else throw new RuntimeException()
    } else throw new RuntimeException()
  }

  /** parse the current type, for example: from a -> b, a TypeVar(a) will be return
   */
  def parseType()(implicit it: MyIterator[Char]): Type = {
    if (it.hasNext) {
      if (it.head == '(') { // TupleType
        assert(it.next() == '(')
        val x = parseType()
        assert(it.next() == ',')
        val y = parseType()
        assert(it.next() == ')')
        TupleType(x, y)
      }
      else if (it.head == 'A') { //AddrType
        assert(it.next() == 'A'); assert(it.next() == 'd'); assert(it.next() == 'd'); assert(it.next() == 'r');
        assert(it.next() == '[')
        val res = AddressType(parseType())
        assert(it.next() == ']')
        res
      }
      else if (it.head == 'V') { //VoidType
        assert(it.next == 'V'); assert(it.next == 'o'); assert(it.next == 'i'); assert(it.next == 'd'); assert(it.next == 'T');
        return VoidType()
      }
      else if (it.head == '[') { // ArrayType
        assert(it.next == '[');
        val elemT = parseType()
        assert(it.next == ']');
        val lenT = parseArithType()
        return ArrayType(elemT, lenT)
      }
      else { // TypeVar
        val c = it.next()
        assert(c.isLetter)
        var superType = ""
        if (it.hasNext && it.head == '^') {
          assert(it.next() == '^')
          while (it.hasNext && it.head.isLetter) {
            superType += it.next()
          }
        } else superType = "UnknownT" // when tv is not annotated, it becomes a UnknownT, will be solved later
        TypeVar(c.toString, {
          superType match {
            case "DataT" => CGenDataType()
            case "ViewT" => ViewType()
            case "IntT" => IntType()
            case "NatT" => ArithType()
            case "UnknownT" => UnknownType()
            case _ => throw new RuntimeException(s"not support $superType as sueprType")
          }
        })
      }
    } else throw new RuntimeException()
  }

  def parseArithType()(implicit it: MyIterator[Char]): Type = { // left associative
    val c = it.next()
    assert(c.isLetter)
    val opSet = Set('+', '-')
    var curType: Type = TypeVar(c.toString, UnknownType())
    while(it.hasNext && opSet.contains(it.head)) {
      it.head match {
        case '-' if it.hasNext(3) && it.head(3) == '>' => return curType // special case for [a]n-v> [b]n, where n-v should not be interpreted as sub(n, v)
        case '-' if it.hasNext(2) && it.head(2) == '>' => return curType // special case for [a]n-> [b]n, where n-> should not be interpreted as sub(n, >)
        case '+' =>
          it.next()
          val right = if (it.head.isDigit) Constant(it.next()) else TypeVar(it.next().toString, UnknownType()) // assume it is less than 10
          curType = AddArithType(curType, right)
        case '-' =>
          it.next()
          val right = if (it.head.isDigit) Constant(it.next()) else TypeVar(it.next().toString, UnknownType())// assume it is less than 10
          curType = SubArithType(curType, right)
        case _ => return curType
      }
    }
    curType
  }

  /** parse the signature's body
   * say: fst :: x |-> y |-> (x, y) -> x, this function parse anything behind "::"
   */
  def parseBody()(implicit it: MyIterator[Char]): Type = {
    if (it.hasNext) {
      val t = parseType()
      if (!it.hasNext) return t

      // the next Lambda
      if (it.head == '|') { // TypeFunType
        it.next() // '|'
        assert(it.next() == '-')
        assert(it.next() == '>')
        TypeFunType(t.asInstanceOf[TypeVar], parseBody())
      } else if (it.head == '-') { // FunType NoEffect
        it.next() // '-'
        if (it.head == '>') {
          it.next()
          FunType(t, parseBody(), NoEffectType())
        }
        else if (it.head.isLetter) { // FunType WithEffect
          val vt = it.head match {
            case 'S' => it.next; SrcViewType()
            case 'D' => it.next; DestViewType()
            case 'E' => it.next; EagerType()
            case other if isLetter(other) => parseType()
            case _ => throw new RuntimeException()
          }
          assert(it.next() == '>') // '>'
          FunType(t, parseBody(), vt)
        }
        else throw new RuntimeException()
      } else throw new RuntimeException()
    } else throw new RuntimeException()
  }
}


class PrimitiveGenTest {
  @Test
  def test() = {
    val sigs = Seq(
//      "RollingSlide :: a^DataT |-> n^NatT |-> w^NatT |-> v^ViewT |-> [a]n -v> [[a]w]n-w+1"
//    "LoopInvariant:: a^DataT |-> a -E> a"
    //        "Concat :: a^DataT |-> n^NatT |-> m^NatT |-> v^ViewT |-> ([a]n, [a]m) -v> [a]n+m",
      "Unrepeat :: a^DataT |-> n^NatT |-> Addr[[a]n] -> Addr[a] "
    )
    sigs.map(SignatureParser.apply).foreach(p => {
      println(p)
    })

  }
}