package cGen

import cGen.NamedTupleStructTypeKind.{TUPLE_NAME_PREFIX, TUPLE_PREFIX, getName}
import core._

import java.util.concurrent.atomic.AtomicInteger
import scala.::
import scala.annotation.tailrec


// Make initialBody public, for the unapply in BuildExpr
trait CGenLambdaT extends LambdaT {
  val body: Expr
}

/**
 * This type is the root of all types in the cGen package / in the CGenIR
 */

case object CGenDataTypeKind extends Kind

sealed trait CGenDataTypeT extends BuiltinDataTypeT with CGenIR {
  override def kind: Kind = CGenDataTypeKind

  override def superType: Type = BuiltinDataType()
}

final case class CGenDataType() extends CGenDataTypeT {
  override def build(newChildren: Seq[ShirIR]): CGenDataTypeT = this
}

final case class CGenDataTypeVar(tvFixedId: Option[Long] = None) extends CGenDataTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = CGenDataTypeVar(tvFixedId)

  override def upperBound: CGenDataTypeT = CGenDataType()
}

/**
 * abstract super type for all collection like types
 */

case object AbstractCollectionTypeKind extends Kind

sealed trait AbstractCollectionTypeT extends CGenDataTypeT {
  override def kind: Kind = AbstractCollectionTypeKind

  override def superType: Type = CGenDataType()

  override def children: Seq[Type] = Seq()
}

final case class AbstractCollectionType() extends AbstractCollectionTypeT {
  override def build(newChildren: Seq[ShirIR]): AbstractCollectionTypeT = AbstractCollectionType()
}

final case class AbstractCollectionTypeVar(tvFixedId: Option[Long] = None) extends AbstractCollectionTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = AbstractCollectionTypeVar(tvFixedId)

  override def upperBound: CGenDataTypeT = AbstractCollectionType()
}

case object NamedTupleStructTypeKind extends Kind {
  final val TUPLE_PREFIX: String = "t"
  final val TUPLE_NAME_PREFIX: String = "tuple_"

  def getName(namedTypes: Seq[(String, CGenDataTypeT)]): String =
    TUPLE_NAME_PREFIX + namedTypes.map(_._2).map(getName(_)).mkString("_")


  def getName(t: Type): String = t match {
    case ArrayType(et, len) => "ARRAY_" + getName(et)
    case ntst: NamedTupleStructType => ntst.name
    case other => other.toShortString
  }
}

sealed trait NamedTupleStructTypeT extends AbstractCollectionTypeT {
  protected val _name: String
  val namedTypes: Seq[(String, CGenDataTypeT)]

  def name: String

  assert(namedTypes.nonEmpty)

  override def kind: Kind = NamedTupleStructTypeKind

  override def superType: Type = AbstractCollectionType()

  override def children: Seq[Type] = namedTypes.map(_._2)
}

object NamedTupleStructType {
  def unapply(ntst: NamedTupleStructType): Some[(String, Seq[(String, CGenDataTypeT)])] =
    Some((ntst.name, ntst.namedTypes))
}

// nameType represent the name of the struct
// should not use the name, should use the getName method instead
final case class NamedTupleStructType(_name: String, namedTypes: Seq[(String, CGenDataTypeT)]) extends NamedTupleStructTypeT {
  override def build(newChildren: Seq[ShirIR]): NamedTupleStructTypeT = {
    NamedTupleStructType(_name, namedTypes.map(_._1).zip(newChildren.map(_.asInstanceOf[CGenDataTypeT])))
  }

  def name = {
    if (_name == "") getName(namedTypes)
    else _name
  }

  def fst = this.namedTypes.head._2
  def snd = this.namedTypes(1)._2

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter =>
        this match {
          case NamedTupleStructType(name, namedTypes) =>
            val fieldNames = namedTypes.map(_._1)
            val expectedFieldNames = TupleType(namedTypes.map(_._2):_*).namedTypes.map(_._1)
            if (fieldNames == expectedFieldNames) {
              val fields = namedTypes.map(p => formatter.formatAtomic(p._2))
              Some(s"(${fields.mkString(", ")})")
            } else {
              val fields = namedTypes.map(p => s"${p._1}: ${formatter.formatAtomic(p._2)}")
              Some(s"struct $name { ${fields.mkString(", ")} }")
            }
        }
      case _ => None
    }
  }
}

final case class NamedTupleStructTypeVar(namedTypes: Seq[(String, CGenDataTypeT)], tvFixedId: Option[Long] = None) extends NamedTupleStructTypeT with TypeVarGenT {
  val _name = ""
  def name = _name

  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarGenT =
    NamedTupleStructTypeVar(namedTypes.map(_._1).zip(newChildren.map(_.asInstanceOf[CGenDataTypeT])), tvFixedId)

  override def upperBound: CGenDataTypeT = NamedTupleStructType(_name, namedTypes)
}

object TupleType {
  def apply(types: CGenDataTypeT*): NamedTupleStructType = {
    val namedTypes = types.zipWithIndex.map(p => (TUPLE_PREFIX + p._2, p._1))
    NamedTupleStructType("", namedTypes)
  }

  def apply(name: String, types: CGenDataTypeT*): NamedTupleStructType = {
    val namedTypes = types.zipWithIndex.map(p => (TUPLE_PREFIX + p._2, p._1))
    NamedTupleStructType(name, namedTypes)
  }
}

object TupleTypeVar {
  def apply(types: CGenDataTypeT*): NamedTupleStructTypeVar = {
    val namedTypes = types.zipWithIndex.map(p => (TUPLE_PREFIX + p._2, p._1))
    NamedTupleStructTypeVar(namedTypes)
  }
}

/**
 * This type represents arrays.
 */

case object ArrayTypeKind extends Kind

sealed trait ArrayTypeT extends AbstractCollectionTypeT {
  val et: CGenDataTypeT
  val len: ArithTypeT

  override def kind: Kind = ArrayTypeKind

  override def superType: Type = AbstractCollectionType()

  override def children: Seq[Type] = Seq(et, len)
}

final case class ArrayType(et: CGenDataTypeT, len: ArithTypeT) extends ArrayTypeT {
  override def build(newChildren: Seq[ShirIR]): ArrayTypeT = ArrayType(newChildren.head.asInstanceOf[CGenDataTypeT], newChildren(1).asInstanceOf[ArithTypeT])

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case formatter: TextFormatter => Some(s"[${formatter.formatAtomic(et)} x ${formatter.formatAtomic(len)}]")
      case _ => None
    }
  }
}

final case class ArrayTypeVar(et: CGenDataTypeT = CGenDataTypeVar(), len: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends ArrayTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = ArrayTypeVar(newChildren.head.asInstanceOf[CGenDataTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = ArrayType(et, len)
}

case object VoidTypeKind extends Kind

sealed trait VoidTypeT extends CGenDataTypeT with CGenIR {
  override def kind: Kind = VoidTypeKind

  override def superType: Type = CGenDataType()
}

final case class VoidType() extends VoidTypeT {
  override def build(newChildren: Seq[ShirIR]): VoidTypeT = this
}

final case class VoidTypeVar(tvFixedId: Option[Long] = None) extends VoidTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = VoidTypeVar(tvFixedId)

  override def upperBound: VoidTypeT = VoidType()
}

case object AddressTypeKind extends Kind

sealed trait AddressTypeT extends CGenDataTypeT with CGenIR {
  val ptrType: Type

  override def kind: Kind = AddressTypeKind

  override def superType: Type = CGenDataType()

  override def children: Seq[Type] = Seq(ptrType)
}

final case class AddressType(ptrType: Type) extends AddressTypeT {
  override def build(newChildren: Seq[ShirIR]): AddressTypeT = AddressType(newChildren.head.asInstanceOf[Type])
}

final case class AddressTypeVar(ptrType: Type = CGenDataTypeVar(), tvFixedId: Option[Long] = None) extends AddressTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = AddressTypeVar(newChildren.head.asInstanceOf[Type], tvFixedId)

  override def upperBound: Type = AddressType(ptrType)
}

object Allocation extends Enumeration {
  type Allocation = Value
  val ALLOCATED, UNALLOCATED = Value
}

case object CScalarTypeKind extends Kind

sealed trait CScalarTypeT extends CGenDataTypeT {
  override def kind: Kind = CScalarTypeKind

  override def superType: Type = CGenDataType()

  override def children: Seq[Type] = Seq()
}

final case class CScalarType() extends CScalarTypeT {
  override def build(newChildren: Seq[ShirIR]): CScalarTypeT = CScalarType()
}

final case class CScalarTypeVar(tvFixedId: Option[Long] = None) extends CScalarTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = CScalarTypeVar(tvFixedId)

  override def upperBound: CGenDataTypeT = CScalarType()
}

case object IntTypeKind extends Kind

sealed trait IntTypeT extends CScalarTypeT {
  val width: ArithTypeT
  val unsigned: ArithTypeT // 0: signed int, 1: unsigned int

  override def kind: Kind = IntTypeKind

  override def superType: Type = CScalarType()

  override def children: Seq[Type] = Seq(width, unsigned)
}

final case class IntType(width: ArithTypeT, unsigned: ArithTypeT = 0) extends IntTypeT {
  override def build(newChildren: Seq[ShirIR]): IntTypeT = IntType(newChildren.head.asInstanceOf[ArithTypeT], newChildren(1).asInstanceOf[ArithTypeT])

  override def tryFormat(formatter: Formatter): Option[String] = {
    (formatter, unsigned.evalBool) match {
      case (formatter: TextFormatter, false) => Some(s"i${formatter.formatAtomic(width)}")
      case (formatter: TextFormatter, true) => Some(s"u${formatter.formatAtomic(width)}")
      case _ => None
    }
  }
}

final case class IntTypeVar(width: ArithTypeT = ArithTypeVar(), unsigned: ArithTypeT = ArithTypeVar(), tvFixedId: Option[Long] = None) extends IntTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = IntTypeVar(newChildren.head.asInstanceOf[ArithTypeT], newChildren(1).asInstanceOf[ArithTypeT], tvFixedId)

  override def upperBound: Type = IntType(width, unsigned)
}

case object DoubleTypeKind extends Kind

sealed trait DoubleTypeT extends CScalarTypeT {
  override def kind: Kind = DoubleTypeKind

  override def superType: Type = CScalarType()

  override def children: Seq[Type] = Seq()
}

final case class DoubleType() extends DoubleTypeT {
  override def build(newChildren: Seq[ShirIR]): DoubleTypeT = DoubleType()

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case _: TextFormatter => Some("double")
      case _ => None
    }
  }
}

final case class DoubleTypeVar(tvFixedId: Option[Long] = None) extends DoubleTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = DoubleTypeVar(tvFixedId)

  override def upperBound: Type = DoubleType()
}

case object FloatTypeKind extends Kind

sealed trait FloatTypeT extends CScalarTypeT {
  override def kind: Kind = FloatTypeKind

  override def superType: Type = CScalarType()

  override def children: Seq[Type] = Seq()
}

final case class FloatType() extends FloatTypeT {
  override def build(newChildren: Seq[ShirIR]): FloatTypeT = FloatType()

  override def tryFormat(formatter: Formatter): Option[String] = {
    formatter match {
      case _: TextFormatter => Some("float")
      case _ => None
    }
  }
}

final case class FloatTypeVar(tvFixedId: Option[Long] = None) extends FloatTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = FloatTypeVar(tvFixedId)

  override def upperBound: Type = FloatType()
}

case object BoolTypeKind extends Kind

sealed trait BoolTypeT extends CScalarTypeT {
  override def kind: Kind = BoolTypeKind

  override def superType: Type = CScalarType()

  override def children: Seq[Type] = Seq()
}

final case class BoolType() extends BoolTypeT {
  override def build(newChildren: Seq[ShirIR]): BoolTypeT = BoolType()
}

final case class BoolTypeVar(tvFixedId: Option[Long] = None) extends BoolTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = BoolTypeVar(tvFixedId)

  override def upperBound: Type = BoolType()
}

case object CharTypeKind extends Kind

sealed trait CharTypeT extends CScalarTypeT {
  override def kind: Kind = CharTypeKind

  override def superType: Type = CScalarType()

  override def children: Seq[Type] = Seq()
}

final case class CharType() extends CharTypeT {
  override def build(newChildren: Seq[ShirIR]): CharTypeT = CharType()
}

final case class CharTypeVar(tvFixedId: Option[Long] = None) extends CharTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = CharTypeVar(tvFixedId)

  override def upperBound: Type = CharType()
}

case object ViewTypeKind extends Kind

sealed trait ViewTypeT extends MetaTypeT {
  override def kind: Kind = ViewTypeKind

  override def superType: Type = AnyType()

  override def children: Seq[Type] = Seq()
}

final case class ViewType() extends ViewTypeT {
  override def build(newChildren: Seq[ShirIR]): ViewTypeT = ViewType()
}

final case class ViewTypeVar(tvFixedId: Option[Long] = None) extends ViewTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = ViewTypeVar(tvFixedId)

  override def upperBound: Type = ViewType()
}

case object NoEffectTypeKind extends Kind

sealed trait NoEffectTypeT extends LazyTypeT {
  override def kind: Kind = ViewTypeKind

  override def superType: Type = LazyType()

  override def children: Seq[Type] = Seq()
}

final case class NoEffectType() extends NoEffectTypeT {
  override def build(newChildren: Seq[ShirIR]): ViewTypeT = NoEffectType()
}

final case class DontCareTypeVar(tvFixedId: Option[Long] = None) extends NoEffectTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = DontCareTypeVar(tvFixedId)

  override def upperBound: Type = NoEffectType()
}

case object LazyTypeKind extends Kind

sealed trait LazyTypeT extends ViewTypeT {
  override def kind: Kind = LazyTypeKind

  override def superType: Type = ViewType()

  override def children: Seq[Type] = Seq()
}

final case class LazyType() extends LazyTypeT {
  override def build(newChildren: Seq[ShirIR]): ViewTypeT = LazyType()
}

final case class LazyTypeVar(tvFixedId: Option[Long] = None) extends LazyTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = LazyTypeVar(tvFixedId)

  override def upperBound: Type = LazyType()
}

case object SrcViewTypeKind extends Kind

sealed trait SrcViewTypeT extends LazyTypeT {
  override def kind: Kind = ViewTypeKind

  override def superType: Type = LazyType()

  override def children: Seq[Type] = Seq()
}

final case class SrcViewType() extends SrcViewTypeT {
  override def build(newChildren: Seq[ShirIR]): ViewTypeT = SrcViewType()
}

final case class SrcViewTypeVar(tvFixedId: Option[Long] = None) extends SrcViewTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = SrcViewTypeVar(tvFixedId)

  override def upperBound: Type = SrcViewType()
}

case object DestViewTypeKind extends Kind

sealed trait DestViewTypeT extends LazyTypeT {
  override def kind: Kind = ViewTypeKind

  override def superType: Type = LazyType()

  override def children: Seq[Type] = Seq()
}

final case class DestViewType() extends DestViewTypeT {
  override def build(newChildren: Seq[ShirIR]): ViewTypeT = DestViewType()
}

final case class DestViewTypeVar(tvFixedId: Option[Long] = None) extends DestViewTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = DestViewTypeVar(tvFixedId)

  override def upperBound: Type = DestViewType()
}

case object EagerTypeKind extends Kind

sealed trait EagerTypeT extends ViewTypeT {
  override def kind: Kind = ViewTypeKind

  override def superType: Type = ViewType()

  override def children: Seq[Type] = Seq()
}

final case class EagerType() extends EagerTypeT {
  override def build(newChildren: Seq[ShirIR]): ViewTypeT = EagerType()
}

final case class EagerTypeVar(tvFixedId: Option[Long] = None) extends EagerTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): TypeVarT = EagerTypeVar(tvFixedId)

  override def upperBound: Type = EagerType()
}

case object CFunTypeKind extends Kind

trait CFunTypeT extends FunTypeT {
  val viewType: ViewTypeT

  override def kind: Kind = CFunTypeKind

  override def superType: Type = superFunType

  override def children: Seq[Type] = Seq(inType, outType, viewType)

  /**
   * @return If this CFunTypeT is nested, return the innermost view type
   */
  @tailrec
  final def rightmostViewType: ViewTypeT = outType match {
    case cft: CFunTypeT => cft.rightmostViewType
    case other => viewType
  }
}

final case class CFunType(inType: Type, outType: Type, viewType: ViewTypeT) extends CFunTypeT {
  override def build(newChildren: Seq[ShirIR]): FunTypeT = CFunType(newChildren.head.asInstanceOf[Type], newChildren(1).asInstanceOf[Type], newChildren(2).asInstanceOf[ViewTypeT])
}

object CFunType {
  @tailrec
  def apply(inTypes: Seq[Type], outType: Type, viewType: ViewTypeT = NoEffectType()): CFunType = {
    if (inTypes.isEmpty) {
      throw MalformedTypeException("Function Type must have at least one input type")
    } else if (inTypes.size == 1) {
      CFunType(inTypes.head, outType, viewType)
    } else {
      CFunType(inTypes.tail, CFunType(inTypes.head, outType, viewType), NoEffectType())
    }
  }

  def apply(inType: Type, outType: Type): CFunType = {
    CFunType(inType, outType, NoEffectType())
  }
  //
  //  def apply(inType: Type, outType: Type, viewType: ViewTypeT): CFunType = {
  //    CFunType(inType, outType, viewType)
  //  }
}

final case class CFunTypeVar(inType: Type = AnyTypeVar(), outType: Type = AnyTypeVar(), viewType: ViewTypeT = ViewTypeVar(), tvFixedId: Option[Long] = None) extends CFunTypeT with TypeVarGenT {
  override def buildTV(newChildren: Seq[ShirIR], tvFixedId: Option[Long]): CFunTypeVar = CFunTypeVar(newChildren.head.asInstanceOf[Type], newChildren(1).asInstanceOf[Type], newChildren(2).asInstanceOf[ViewTypeT], tvFixedId)

  override def upperBound: Type = CFunType(inType, outType, viewType)
}

object CFunTypeVar {
  @tailrec
  def apply(inTypes: Seq[Type], outType: Type): CFunTypeVar = {
    if (inTypes.isEmpty) {
      throw MalformedTypeException("Function Type must have at least one input type")
    } else if (inTypes.size == 1) {
      CFunTypeVar(inTypes.head, outType)
    } else {
      CFunTypeVar(inTypes.tail, CFunTypeVar(inTypes.head, outType))
    }
  }
}

case object IdTypeKind extends Kind

sealed trait IdTypeT extends MetaTypeT {
  override def kind: Kind = IdTypeKind

  override def superType: Type = AnyType()

  override def children: Seq[Type] = Seq()
}

final case class ZeroIdType() extends IdTypeT {
  override def build(newChildren: Seq[ShirIR]): IdTypeT = ZeroIdType()
}

final case class SuccIdType(t: IdTypeT) extends IdTypeT {
  override def children: Seq[Type] = Seq(t)

  override def build(newChildren: Seq[ShirIR]): IdTypeT = SuccIdType(newChildren.head.asInstanceOf[IdTypeT])
}

