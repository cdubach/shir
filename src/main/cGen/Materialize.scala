package cGen

import cGen.primitive.{Id, SelectS}
import cGen.pass.MemoryPass.LowerSrcViewAndEagerPass
import core.{Expr, ParamDef, ParamUse, Type, TypeChecker}

/** Id only works on ScalarT. Materialize is just like Id, but works on any type.
 * It pattern matches the input and generates call to Map or Tuple based on the input type.
 */
object Materialize {
  def apply(src: Expr): CGenExpr = {
    val checkedType = if (src.t.hasUnknownType) TypeChecker.check(src).t else src.t
    checkedType match {
      case arrT: ArrayTypeT =>
        val in = ParamDef(arrT.et)
        Map(CLambda(in, Materialize(ParamUse(in))), src, EagerType())
      case ntst: NamedTupleStructTypeT =>
        Tuple(ntst.name, Materialize(SelectS(src, 0)), Materialize(SelectS(src, 1)), DestViewType())
      case _: CScalarTypeT => Id(src)
      case other => throw new Exception(other.toShortString + "is not supported")
    }
  }

  def lowered(src: Expr): CGenExpr = LowerSrcViewAndEagerPass(Materialize(src)).asInstanceOf[CGenExpr]
}

case class MaterializableExpr(innerIR: Expr) extends DecorExpr {
  override def cgenBuild(newInnerIR: Expr, newTypes: Seq[Type]): MaterializableExpr = MaterializableExpr(newInnerIR)

  override def argsBuild(args: Seq[Expr]): CGenExpr = this

  override def types: Seq[Type] = Seq()
}

// Short for Materializable, A tem expr to indicate a materialize can be inserted in the very position
object Mtrl {
  def apply(in: Expr): MaterializableExpr = MaterializableExpr(in)

  def unapply(in: MaterializableExpr): Some[(Expr)] = Some(in.innerIR)
}

object Must {
  def apply(in: Expr): Expr = {
    in match {
      case MaterializableExpr(e) => Must(e)
      case ce: CGenExpr if ce.rightmostViewType.isInstanceOf[EagerTypeT] => in
      case _ => Materialize(in)
    }
  }
}

object AutoTune {
  def apply(in: Expr): Seq[Expr] = {
    var mCounter = 0
    in.visit({ // count the occurrences of M
      case Mtrl(_) => mCounter += 1
      case _ =>
    })

    /**f function used to generate a binary sequence.
     * when n == 1, the seq will be (0, 1)
     * when n == 2, the seq will be (00, 01, 10, 11)
     */
    def f(mss: Seq[Seq[Int]], n: Int): Seq[Seq[Int]] = {
      if (n == 0) mss
      else f((mss map (_ :+ 0)) ++ (mss map (_ :+ 1)), n - 1)
    }

    val mss = f(Seq(Seq()), mCounter)
    println(s"autotune counter : $mCounter, total: ${mss.length}")
    mss.map(ms => {
      var counter = -1
      in.visitAndRebuild({
        case Mtrl(in) => {
          counter += 1
          if (ms(counter) == 1) Materialize(in)
          else in
        }
        case other => other
      }).asInstanceOf[Expr]
    })
//
//    trees.foreach(_.visit({
//      case MaterializableExpr(_) => throw new Exception("no more materializableExpr should exist")
//      case _ =>
//    }))
//
//    trees
  }

  def tuneOne(in: Expr): Expr = {
    in.visitAndRebuild({
      case Mtrl(in) => Materialize(in)
      case other => other
    }).asInstanceOf[Expr]
  }
}
