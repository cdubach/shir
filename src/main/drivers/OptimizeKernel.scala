package drivers

import eqsat.Extractor.DefaultSmallest
import cGen.{CCodeGen, DPSPass, FlatLinux64}
import eqsat.{ConcatSaturationStrategy, DeBruijnLambda, EClassT, EGraph, EGraphObserver, EGraphStepObserver, ENodeT, ExprToText, Extractor, HashConsEClass, HashConsENode, HierarchicalStopwatch, IsariaStrategy, IterationLimitedSaturationStrategy, NonRebasingStrategy, PerRuleRandomSaturationStrategy, RebasingRewriteEngine, RebasingStrategy, Rewrite, SaturationStrategy, SimpleSaturationStrategy, SympyStrategy, TimeLimitedRebasingStrategy, TimeLimitedSaturationStrategy}
import cGen.idioms.{BlasIdiomRules, ExprToPython, ExternCallInResultObserver, ExtractionBasedSemanticRules, PrepareForC, PrepareForPytorch, PrepareForTensorflow, PytorchIdiomRules, TensorflowIdiomRules, TimeComplexityExtractor}
import cGen.kernels.Kernel
import core.util.Prune
import core.{Expr, LambdaT, Type}

import java.time.{ZoneId, ZonedDateTime}
import java.util.concurrent.TimeUnit
import java.util.{Timer, TimerTask}
import scala.concurrent.duration.Duration
import scala.language.postfixOps
import sys.process._

object OptimizeKernel {
  val kernels = Seq(
    cGen.kernels.VectorSumKernel(1000000),
    cGen.kernels.AxpyKernel(1000000),
    cGen.kernels.GemvKernel(4000),
    cGen.kernels.GesummvKernel(2800),
    cGen.kernels.GemverKernel(4000),
    cGen.kernels.GEMMKernel(2000, 2000, 2000),
    cGen.kernels.OneMMKernel(2000),
    cGen.kernels.OuterMMKernel(2000),
    cGen.kernels.DoitgenKernel(256, 256, 256),
    cGen.kernels.MvtKernel(4000),
    cGen.kernels.MemsetZeroKernel(100000),
    cGen.kernels.BicgKernel(1800, 2200),
    cGen.kernels.AtaxKernel(2200, 2200),
    cGen.kernels.Blur1DKernel(100000),
    cGen.kernels.Jacobi1DKernel(4000, 1000),
    cGen.kernels.Stencil2DKernel(1024),
    cGen.kernels.SlimTwoMMKernel(256),
    cGen.kernels.TwoMMKernel(256)
  )

  sealed trait Target
  object BlasIdiomsTarget extends Target {
    override def toString: String = "blas"
  }
  object TensorflowIdiomsTarget extends Target {
    override def toString: String = "tensorflow"
  }

  object PytorchIdiomsTarget extends Target {
    override def toString: String = "pytorch"
  }
  object NoIdiomsTarget extends Target {
    override def toString: String = "none"
  }

  final case class Ruleset(simplificationRules: Seq[Rewrite], expansionRules: Seq[Rewrite], idiomRules: Seq[Rewrite]) {
    def all: Seq[Rewrite] = simplificationRules ++ expansionRules ++ idiomRules
  }

  sealed trait Strategy {
    def apply[ENode <: ENodeT, EClass <: EClassT](rules: Ruleset, maxSteps: Int, timeBudget: Duration): RebasingStrategy[ENode, EClass]
  }

  object SimpleStrategy extends Strategy {
    override def toString: String = "simple"

    override def apply[ENode <: ENodeT, EClass <: EClassT](rules: Ruleset, maxSteps: Int, timeBudget: Duration): RebasingStrategy[ENode, EClass] =
      NonRebasingStrategy(
        TimeLimitedSaturationStrategy(
          IterationLimitedSaturationStrategy(
            SimpleSaturationStrategy(rules.all),
            maxSteps),
          timeBudget))
  }

  final case class PerRuleRandomStrategy(limitPerRule: Int) extends Strategy {
    override def toString: String = "per-rule-random"

    override def apply[ENode <: ENodeT, EClass <: EClassT](rules: Ruleset, maxSteps: Int, timeBudget: Duration): RebasingStrategy[ENode, EClass] = {
      val limits = rules.all.map(_ => limitPerRule)
      NonRebasingStrategy(
        TimeLimitedSaturationStrategy(
          IterationLimitedSaturationStrategy(
            PerRuleRandomSaturationStrategy(rules.all.zip(limits)),
            maxSteps),
          timeBudget))
    }
  }

  final case class IsariaLiarStrategy(timeoutPerPhase: Duration, rebase: Boolean = true) extends Strategy {
    override def toString: String = if (rebase) "isaria" else "isaria-no-rebase"

    override def apply[ENode <: ENodeT, EClass <: EClassT](rules: Ruleset, maxSteps: Int, timeBudget: Duration): RebasingStrategy[ENode, EClass] = {
      val recurrentPhase = ConcatSaturationStrategy(
          TimeLimitedSaturationStrategy(
            SimpleSaturationStrategy(rules.expansionRules),
            timeoutPerPhase),
          TimeLimitedSaturationStrategy(
            SimpleSaturationStrategy(rules.simplificationRules),
            timeoutPerPhase))
      val finalPhase = TimeLimitedSaturationStrategy(SimpleSaturationStrategy(rules.idiomRules), timeoutPerPhase)
      IsariaStrategy(recurrentPhase, finalPhase, recurrentPhaseTimeout = Some(timeBudget), rebase = rebase)
    }
  }

  final case class SympyLiarStrategy(cycles: Int = 2, rebase: Boolean = true) extends Strategy {
      override def toString: String = if (rebase) "sympy" else "sympy-no-rebase"

      override def apply[ENode <: ENodeT, EClass <: EClassT](rules: Ruleset, maxSteps: Int, timeBudget: Duration): RebasingStrategy[ENode, EClass] = {
        val timePerCycle = timeBudget / cycles
        SympyStrategy(
          rules.all,
          cycles = cycles,
          limitCycle = TimeLimitedSaturationStrategy(_, timePerCycle),
          rebase = rebase)
      }
    }

  val BLAS_FLAG = "--idioms=blas"
  val TENSORFLOW_FLAG = "--idioms=tensorflow"
  val PYTORCH_FLAG = "--idioms=pytorch"
  val NO_IDIOMS_FLAG = "--idioms=none"

  val SIMPLE_STRATEGY_FLAG = "--strategy=simple"
  val PER_RULE_RANDOM_STRATEGY_FLAG = "--strategy=per-rule-random"
  val ISARIA_STRATEGY_FLAG = "--strategy=isaria"
  val ISARIA_NO_REBASE_STRATEGY_FLAG = "--strategy=isaria-no-rebase"
  val SYMPY_STRATEGY_FLAG = "--strategy=sympy"
  val SYMPY_NO_REBASE_STRATEGY_FLAG = "--strategy=sympy-no-rebase"

  def main(args: Array[String]): Unit = {
    if (args.length != 5
      || !Seq(BLAS_FLAG, TENSORFLOW_FLAG, PYTORCH_FLAG, NO_IDIOMS_FLAG).contains(args(3))
      || !Seq(SIMPLE_STRATEGY_FLAG, PER_RULE_RANDOM_STRATEGY_FLAG, ISARIA_STRATEGY_FLAG, ISARIA_NO_REBASE_STRATEGY_FLAG, SYMPY_STRATEGY_FLAG, SYMPY_NO_REBASE_STRATEGY_FLAG).contains(args(4))) {

      println("usage: RunKernel kernel-name timeout max-steps --idioms=blas|tensorflow|pytorch|none --strategy=simple|per-rule-random|isaria|isaria-no-rebase|sympy|sympy-no-rebase")
      sys.exit(1)
    }

    val kernelName = args.head
    val timeout = args(1).toLong
    kernels.find(k => k.name == kernelName || k.qualifiedName == kernelName) match {
      case None =>
        println(s"unknown kernel $kernelName")
        val kernelNames = kernels.map(_.qualifiedName).mkString(", ")
        println(s"known kernels: $kernelNames")

      case Some(kernel) =>
        optimizeKernel(
          kernel,
          timeout,
          args(2).toInt,
          args(3) match {
            case BLAS_FLAG => BlasIdiomsTarget
            case TENSORFLOW_FLAG => TensorflowIdiomsTarget
            case PYTORCH_FLAG => PytorchIdiomsTarget
            case NO_IDIOMS_FLAG => NoIdiomsTarget
          }, args(4) match {
            case SIMPLE_STRATEGY_FLAG => SimpleStrategy
            case PER_RULE_RANDOM_STRATEGY_FLAG => PerRuleRandomStrategy(100)
            case ISARIA_STRATEGY_FLAG => IsariaLiarStrategy(Duration(60, TimeUnit.SECONDS))
            case ISARIA_NO_REBASE_STRATEGY_FLAG => IsariaLiarStrategy(Duration(60, TimeUnit.SECONDS), rebase = false)
            case SYMPY_STRATEGY_FLAG => SympyLiarStrategy()
            case SYMPY_NO_REBASE_STRATEGY_FLAG => SympyLiarStrategy(rebase = false)
          })
    }
  }

  /**
   * Runs a specific kernel, logging its status throughout the running process.
   * @param kernel The kernel to run.
   * @param timeout The amount of time the kernel has to run, in milliseconds.
   */
  def optimizeKernel(kernel: Kernel, timeout: Long, maxSteps: Int, target: Target, strategy: Strategy): Unit = {
    println(s"running kernel: ${kernel.qualifiedName}")

    val time = ZonedDateTime.now(ZoneId.of("UTC"))
    println(s"date/time: $time")
    println(s"commit id: ${"git rev-parse HEAD" !!}")
    println(s"machine name: $machineName")
    println(s"optimizer scala version: ${util.Properties.versionString}")
    println(s"timeout: $timeout")
    println(s"idiom recognition enabled: ${target != NoIdiomsTarget}")
    println(s"target: $target")
    println(s"strategy: $strategy")

    val loweredKernel = kernel.transformedInput
    println(s"original kernel: ${ExprToText(loweredKernel)}")

    val (paramTypes, returnType) = paramTypesAndReturnType(loweredKernel)
    println(s"parameter types: <<<")
    paramTypes.map(ExprToText(_)).foreach(println)
    println(">>>")
    println(s"return type: ${ExprToText(returnType)}")

    Prune.findUnsolvedTypeVar(loweredKernel).foreach(smallest =>
      println(s"warning: found unsolved type var in ${ExprToText(smallest)}"))

    val extractor = TimeComplexityExtractor
    val idiomExtractor = DefaultSmallest
    val semanticRules = ExtractionBasedSemanticRules(extractor)
    val simplificationRules = semanticRules.simplificationRules
    val expansionRules = semanticRules.inliningRule +: semanticRules.expansionRules
    val rules = expansionRules ++ simplificationRules
    val idioms = target match {
      case BlasIdiomsTarget => BlasIdiomRules(idiomExtractor).rules
      case NoIdiomsTarget => Seq()
      case TensorflowIdiomsTarget => TensorflowIdiomRules(idiomExtractor).rules
      case PytorchIdiomsTarget => PytorchIdiomRules(idiomExtractor).rules
    }
    val instance = kernel.instantiate(extractor.analysis, rules, idioms)

    val observer = new ProgressPrinter[HashConsENode, HashConsEClass](None, kernel, target)
    val engine = instance.createRebasingEngine(observer)
    observer.engine = Some(engine)
    observer.printStatistics(engine.graph, fullIteration = true, appliedMatchCount = 0)

    val timer = new Timer(true)
    timer.schedule(new TimerTask {
      override def run(): Unit = {
        System.exit(0)
      }
    }, timeout)

    val duration = Duration.fromNanos(timeout * 1000000)
    strategy(Ruleset(simplificationRules, expansionRules, idioms), maxSteps, duration)(engine)
  }

  private def paramTypesAndReturnType(expr: Expr): (Seq[Type], Type) = {
    expr match {
      case lam: LambdaT =>
        val (innerParams, innerReturn) = paramTypesAndReturnType(lam.body)
        (lam.param.t +: innerParams, innerReturn)
      case _ => (Seq.empty, expr.t)
    }
  }

  private def machineName: String = {
    val env = System.getenv
    if (env.containsKey("COMPUTERNAME")) env.get("COMPUTERNAME")
    else if (env.containsKey("HOSTNAME")) env.get("HOSTNAME")
    else "hostname" !!
  }

  private final class ProgressPrinter[EngineENode <: ENodeT, EngineEClass <: EClassT](var engine: Option[RebasingRewriteEngine[EngineENode, EngineEClass]],
                                                                                      kernel: Kernel,
                                                                                      target: Target) extends EGraphObserver {
    private val stopwatch = HierarchicalStopwatch.start("")

    def printStatistics[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass],
                                                            fullIteration: Boolean,
                                                            appliedMatchCount: Int): Unit = {
      // HACK: recompute extraction analysis
      graph.recomputeAnalysis(kernel.extractionAnalysis)

      println(s"full iteration: $fullIteration")
      println(s"applied matches: $appliedMatchCount")

      val bestSoFar = engine.get.expr
      val named = DeBruijnLambda.toNamedRec(bestSoFar).asInstanceOf[Expr]
      val externCalls = ExternCallInResultObserver.countExternCalls(bestSoFar)
        .map(t => s"${t._2} x ${t._1}")
        .mkString(", ")
      println(s"node count: ${graph.classes.map(_.nodes.length).sum}")
      println(s"class count: ${graph.classes.length}")
      println(s"elapsed nanoseconds: ${stopwatch.elapsedNanoseconds}")
      println(s"extern calls: $externCalls")
      println(s"solution: ${ExprToText(bestSoFar)}")

      target match {
        case TensorflowIdiomsTarget | PytorchIdiomsTarget =>
          generatePython(named)

        case BlasIdiomsTarget =>
          generateC(named)

        case NoIdiomsTarget =>
          generateC(named)
          generatePython(named)
      }
    }

    private def generateC(expr: Expr): Unit = {
      val prepared = PrepareForC(expr)
      println(s"C codegen input: ${ExprToText(prepared)}")
      println("C solution: <<<")
      println(CCodeGen(DPSPass(prepared), platform = FlatLinux64).ccode())
      println(">>>")
    }

    private def generatePython(expr: Expr): Unit = {
      val prepared = target match {
        case TensorflowIdiomsTarget => PrepareForTensorflow(expr)
        case PytorchIdiomsTarget => PrepareForPytorch(expr)
        case NoIdiomsTarget => expr
        case _ => ???
      }
      println(s"python codegen input: ${ExprToText(prepared)}")
      println("python solution: <<<")
      println(target match {
        case TensorflowIdiomsTarget => ExprToPython.toTensorflowProgram(prepared)
        case PytorchIdiomsTarget => ExprToPython.toPytorchProgram(prepared)
        case NoIdiomsTarget => ExprToPython.toPythonProgram(prepared)
        case _ => ???
      })
      println(">>>")
    }

    override def afterSaturationStep[ENode <: ENodeT, EClass <: EClassT](graph: EGraph[ENode, EClass],
                                                                         fullIteration: Boolean,
                                                                         appliedMatchCount: Int): Unit = {
      println("===   step   ===")
      printStatistics(graph, fullIteration, appliedMatchCount)
      println("=== step end ===")
    }
  }
}
