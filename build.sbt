name := "shir"

version := "1.0"

scalaVersion := "2.12.19"

// Check Java version
initialize := {
  val _ = initialize.value // run the previous initialization
  val minVersion = VersionNumber("11")
  val current  = VersionNumber(sys.props("java.specification.version"))
  val sufficient = current.numbers.zip(minVersion.numbers).foldRight(minVersion.numbers.size<=current.numbers.size)((a,b) => (a._1 > a._2) || (a._1==a._2 && b))
  assert(sufficient, s"Unsupported JDK: java.specification.version $current. Require at least JDK version $minVersion.")
}

compile := ((compile in Compile) dependsOn (updateSubmodules)).value

lazy val updateSubmodules = taskKey[Unit]("Update the submodules")

updateSubmodules := {
  import scala.language.postfixOps
  import scala.sys.process._
  //noinspection PostfixMethodCall
  "echo y" #| "./updateSubmodules.sh" !
}

//scalacOptions ++= Seq("-Xmax-classfile-name", "100", "-unchecked", "-deprecation", "-feature")

// Main sources
scalaSource in Compile := baseDirectory(_ / "src/main").value
javaSource in Compile := baseDirectory(_ / "src/main").value

// Test sources
scalaSource in Test := baseDirectory(_ / "src/test").value
javaSource in Test := baseDirectory(_ / "src/test").value

// Scala libraries
//libraryDependencies += "org.scala-lang" % "scala-reflect" % "2.11.8"
//libraryDependencies += "org.scala-lang" % "scala-compiler" % "2.11.8"
//libraryDependencies += "org.scala-lang" % "scala-library" % "2.11.8"

//libraryDependencies += "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.4"

//libraryDependencies += "jline" % "jline" % "2.12.1"

// Jsoniter
libraryDependencies += "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-core" % "2.19.0"
libraryDependencies += "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-macros" % "2.19.0"

// JUnit
libraryDependencies += "junit" % "junit" % "4.11"
libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test"

// ScalaCheck
//libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.0" % "test"

// Time utilities
//libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "2.16.0"

//lazy val profiler = RootProject(file("lib/Profiler"))

//lazy val root = (project in file(".")).aggregate(profiler).dependsOn(profiler)

scalacOptions in (Compile,doc) := Seq("-implicits", "-diagrams")
javaOptions in Test += "-Xss16m" // Increase this number if StackOverflowError occurs during sbt test. IDEA users should set this flag in Run -> Edit Configurations.

// Build ArithExpr
unmanagedSourceDirectories in Compile += baseDirectory.value / "lib/arithexpr/src/main/"

testOptions += Tests.Argument(TestFrameworks.JUnit, "-q", "-v", "-a")

fork := true
