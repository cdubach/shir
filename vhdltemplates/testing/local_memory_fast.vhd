library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity local_memory is
    generic(
        BUFFER_SIZE: natural := 64;
        LOCAL_MEM_ADDR_WIDTH: natural := 10;
        LOCAL_MEM_DATA_WIDTH: natural := 512;
        DELAY_CONSTANT: natural := 4
    );
    port(
        clk: in std_logic;
        
        bank_waitrequest: out std_logic;
        bank_readdata: out std_logic_vector(LOCAL_MEM_DATA_WIDTH-1 downto 0);
        bank_readdatavalid: out std_logic;

        bank_burstcount: in std_logic_vector(7-1 downto 0);     -- Note that this is not used by model
        bank_writedata: in std_logic_vector(LOCAL_MEM_DATA_WIDTH-1 downto 0);
        bank_address: in std_logic_vector(LOCAL_MEM_ADDR_WIDTH-1 downto 0);
        bank_write: in std_logic;
        bank_read: in std_logic;
        bank_byteenable: in std_logic_vector(64-1 downto 0)     -- Also not used by model
    );
end local_memory;

architecture behavioral of local_memory is

    type buffer_entry is record
        address: std_logic_vector(LOCAL_MEM_ADDR_WIDTH-1 downto 0);
        data: std_logic_vector(LOCAL_MEM_DATA_WIDTH-1 downto 0);
        wr_or_rd: std_logic;    -- 1 for write, 0 for read
    end record buffer_entry;

    type buffer_type is array(0 to BUFFER_SIZE-1) of buffer_entry;
    signal data_buffer: buffer_type := (others => (  address => (others => '0'),
                                                data => (others => '0'),
                                                wr_or_rd => '0'));

    type memory is array(0 to 2**LOCAL_MEM_ADDR_WIDTH-1) of std_logic_vector(LOCAL_MEM_DATA_WIDTH-1 downto 0);
    signal local_mem: memory := (others => (others => '0'));

    signal waitrequest: std_logic := '0';
    signal push_ctr: natural range 0 to (BUFFER_SIZE-1) := 0;
    signal pop_ctr: natural range 0 to (BUFFER_SIZE-1) := 0;
    signal buffer_num_elems: natural range 0 to BUFFER_SIZE := 0;
    signal pop_ctr_delay: natural := 0;

begin

    update_buffer_push: process(clk)
    begin
        if rising_edge(clk) then
            if bank_read = '1' and waitrequest = '0' then
                data_buffer(push_ctr).address <= bank_address;
                data_buffer(push_ctr).data <= (others => '0');
                data_buffer(push_ctr).wr_or_rd <= '0';
            elsif bank_write = '1' and waitrequest = '0' then
                data_buffer(push_ctr).address <= bank_address;
                data_buffer(push_ctr).data <= bank_writedata;
                data_buffer(push_ctr).wr_or_rd <= '1';
            end if;
        end if;
    end process;

    push_ctr_update: process(clk)
    begin
        if rising_edge(clk) then
            if (bank_read = '1' or bank_write = '1') and waitrequest = '0' then
                if push_ctr = (BUFFER_SIZE-1) then
                    push_ctr <= 0;
                else
                    push_ctr <= push_ctr + 1;
                end if;
            else
                push_ctr <= push_ctr;
            end if;
        end if;
    end process;

    pop_ctr_delay <= 0;

    pop_ctr_update: process(clk)
    begin
        if rising_edge(clk) then
            if pop_ctr_delay = 0 and buffer_num_elems > 0 then
                if pop_ctr = (BUFFER_SIZE-1) then
                    pop_ctr <= 0;
                else
                    pop_ctr <= pop_ctr + 1;
                end if;
            end if;
        end if;
    end process;

    buffer_num_elems_update: process(clk)
    begin
        if rising_edge(clk) then
            if ((bank_read = '1' or bank_write = '1') and waitrequest = '0') and (pop_ctr_delay = 0 and buffer_num_elems > 0) then
                buffer_num_elems <= buffer_num_elems;
            elsif ((bank_read = '1' or bank_write = '1') and waitrequest = '0') and (not (pop_ctr_delay = 0 and buffer_num_elems > 0)) then
                buffer_num_elems <= buffer_num_elems + 1;
            elsif (not ((bank_read = '1' or bank_write = '1') and waitrequest = '0')) and (pop_ctr_delay = 0 and buffer_num_elems > 0) then
                buffer_num_elems <= buffer_num_elems - 1;
            else
                buffer_num_elems <= buffer_num_elems;
            end if;
        end if;
    end process;

    update_buffer_pop: process(clk)
    begin
        if rising_edge(clk) then
            if pop_ctr_delay = 0 and buffer_num_elems > 0 then
                if data_buffer(pop_ctr).wr_or_rd = '1' then
                    local_mem(to_integer(unsigned(data_buffer(pop_ctr).address))) <= data_buffer(pop_ctr).data;
                    bank_readdatavalid <= '0';
                elsif data_buffer(pop_ctr).wr_or_rd = '0' then
                    bank_readdata <= local_mem(to_integer(unsigned(data_buffer(pop_ctr).address)));
                    bank_readdatavalid <= '1';
                end if;
            else
                bank_readdatavalid <= '0';
            end if;
        end if;
    end process;

    update_waitrequest: process(clk)
    begin
        if rising_edge(clk) then
            if buffer_num_elems + 8 > BUFFER_SIZE then
                waitrequest <= '1';
            else
                waitrequest <= '0';
            end if;
        end if;
    end process;

    bank_waitrequest <= waitrequest;

end behavioral;