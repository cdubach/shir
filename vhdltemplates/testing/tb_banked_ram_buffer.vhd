library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_banked_ram_buffer is
end tb_banked_ram_buffer;

architecture behavioral of tb_banked_ram_buffer is
    component banked_ram_buffer
        generic(
            entries: natural := 8
        );
        port(
            clk: in std_logic;
            reset: in std_logic;
            port_in_data: in memory_input_vector_type;
            port_in_last: in last_0_type;
            port_in_valid: in std_logic;
            port_in_ready: out ready_0_type;
            port_out_data: out data_word_vector_type;
            port_out_last: out last_0_type;
            port_out_valid: out std_logic;
            port_out_ready: in ready_0_type
        );
    end component;
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    constant wait_time: time := 2 * clk_period;
    signal reset: std_logic;

    signal tb_in_data: memory_input_vector_type := (data => (others => (others => '0')), addr => (others => (others => '0')), we => (others => '0'));
    signal tb_in_last: last_0_type;
    signal tb_in_valid: std_logic := '0';
    signal tb_in_ready: ready_0_type := "0";
    signal tb_out_data: data_word_vector_type := (others => (others => '0'));
    signal tb_out_last: last_0_type;
    signal tb_out_valid: std_logic := '0';
    signal tb_out_ready: ready_0_type := "0";

    signal switch: std_logic := '0';
begin

    uut: banked_ram_buffer port map(
        clk => clk,
        reset => reset,
        port_in_data => tb_in_data,
        port_in_last => tb_in_last,
        port_in_valid => tb_in_valid,
        port_in_ready => tb_in_ready,
        port_out_data => tb_out_data,
        port_out_last => tb_out_last,
        port_out_valid => tb_out_valid,
        port_out_ready => tb_out_ready
    );

    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    reset <= '1', '0' after 4*clk_period;

    tb_out_ready <= "1";

    process
    begin
        tb_in_data.data <= (others => (others => '1'));
        tb_in_data.addr <= (others => "000000000000000000000000000000000000000001");
        tb_in_data.we <= (others => '1');

        tb_in_valid <= '0';
        wait for 10*clk_period;
        wait until rising_edge(clk);
        tb_in_valid <= '1';
        wait until rising_edge(clk);
        tb_in_valid <= '0';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        tb_in_data.we <= (others => '0');
        tb_in_valid <= '1';
        wait until rising_edge(clk);
        tb_in_valid <= '0';
        wait until rising_edge(clk);
        tb_in_valid <= '1';
        wait until rising_edge(clk);
        wait;
    end process;

end behavioral;
