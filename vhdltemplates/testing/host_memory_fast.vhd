library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.common.all;

entity host_memory is
    generic(
        req_buffer_size: natural := 64
    );
    port(
        clk: in std_logic;
        -- read request
        read_req_address: in type_IntTypeArithType42;
        read_req_mdata: in type_VectorTypeLogicTypeArithType16;
        read_req_valid: in std_logic;
        read_req_almFull: out std_logic;
        -- read response
        read_rsp_data: out type_VectorTypeLogicTypeArithType512;
        read_rsp_mdata: out type_VectorTypeLogicTypeArithType16;
        read_rsp_valid: out std_logic;
        -- write request
        write_req_address: in type_IntTypeArithType42;
        write_req_mdata: in type_VectorTypeLogicTypeArithType16;
        write_req_data: in type_VectorTypeLogicTypeArithType512;
        write_req_valid: in std_logic;
        write_req_almFull: out std_logic;
        -- write response
        write_rsp_mdata: out type_VectorTypeLogicTypeArithType16;
        write_rsp_valid: out std_logic;
        -- final mem image correct
        mem_correct: out std_logic
    );
end host_memory;

architecture behavioral of host_memory is

    impure function count_lines (constant filename: in string) return natural is
        file f: text;
        variable lines: natural := 0;
        variable linebuffer: line;
    begin
        lines := 0;
        file_open(f, filename, read_mode);
        while not endfile(f) loop
            readline(f, linebuffer);
            lines := lines + 1;
        end loop;
        file_close(f);
        return lines;
    end count_lines;

    type mem_image_type is array (0 to count_lines("mem_final.dat") - 1) of std_logic_vector(511 downto 0);

    impure function load_mem_image (constant filename: in string) return mem_image_type is
        file mem_file: text;
        variable index: natural := 0;
        variable linebuffer: line;
        variable cacheline: std_logic_vector(511 downto 0);
        variable result: mem_image_type;
    begin
        index := 0;
        file_open(mem_file, filename, read_mode);
        while not endfile(mem_file) loop
            readline(mem_file, linebuffer);
            read(linebuffer, cacheline);
            result(index) := cacheline;
            index := index + 1;
        end loop;
        file_close(mem_file);
        return result;
    end load_mem_image;

    signal ram: mem_image_type := load_mem_image("mem_initial.dat");

    signal final_ram: mem_image_type := load_mem_image("mem_final.dat");

begin

    mem_correct <= '1' when ram = final_ram else '0';

    -- set flag, when there are only 8 entries left in the buffer
    read_req_almFull <= '0';
    write_req_almFull <= '0';

    -- read
    read_logic: process(clk)
    begin
        if rising_edge(clk) then
            read_rsp_data <= (others => '0');
            read_rsp_mdata <= (others => '0');
            read_rsp_valid <= '0';
            if read_req_valid = '1' then
                read_rsp_data <= ram(to_integer(unsigned(read_req_address)));
                read_rsp_mdata <= read_req_mdata;
                read_rsp_valid <= '1';
            end if;
        end if;
    end process;

    -- write
    write_logic: process(clk)
    begin
        if rising_edge(clk) then
            write_rsp_mdata <= (others => '0');
            write_rsp_valid <= '0';
            if write_req_valid = '1' then
                ram(to_integer(unsigned(write_req_address))) <= write_req_data;
                if final_ram(to_integer(unsigned(write_req_address))) = write_req_data then
                    report "cacheline " & natural'image(to_integer(unsigned(write_req_address))) & " correctly written!";
                else
                    report "cacheline " & natural'image(to_integer(unsigned(write_req_address))) & " written! But incorrect value!";
                end if;
                write_rsp_mdata <= write_req_mdata;
                write_rsp_valid <= '1';
            end if;
        end if;
    end process;

end behavioral;
