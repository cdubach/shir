library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_memory_reader is
end tb_memory_reader;

architecture behavioral of tb_memory_reader is
    component host_memory
        port(
            clk: in std_logic;
            -- read request
            read_req_address: in mem_address_type;
            read_req_mdata: in req_id_type;
            read_req_valid: in std_logic;
            read_req_almFull: out std_logic;
            -- read response
            read_rsp_data: out cache_line_type;
            read_rsp_mdata: out req_id_type;
            read_rsp_valid: out std_logic;
            -- write request
            write_req_address: in mem_address_type;
            write_req_mdata: in req_id_type;
            write_req_data: in cache_line_type;
            write_req_valid: in std_logic;
            write_req_almFull: out std_logic;
            -- write response
            write_rsp_mdata: out req_id_type;
            write_rsp_valid: out std_logic
        );
    end component;
    component memory_reader is
        port(
            clk: in std_logic;
            reset: in std_logic;
            port_mem_out_data: out request_mem_address_type;
            port_mem_out_valid: out std_logic;
            port_mem_out_ready: in std_logic;
            port_mem_in_data: in request_cache_line_type;
            port_mem_in_valid: in std_logic;
            port_in_valid: in std_logic;
            port_in_ready: out ready_0_type;
            port_out_data: out cache_line_unordered_stream_type;
            port_out_valid: out std_logic;
            port_out_ready: in ready_1_type
        );
    end component;

    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    constant wait_time: time := 2 * clk_period;
    signal reset: std_logic;

    signal tb_hostmem_read_req_address: mem_address_type;
    signal tb_hostmem_read_req_mdata: req_id_type;
    signal tb_hostmem_read_req_valid: std_logic;
    signal tb_hostmem_read_req_almFull: std_logic;
    signal tb_hostmem_read_rsp_data: cache_line_type;
    signal tb_hostmem_read_rsp_mdata: req_id_type;
    signal tb_hostmem_read_rsp_valid: std_logic;
    
    signal tb_hostmem_write_req_address: mem_address_type;
    signal tb_hostmem_write_req_mdata: req_id_type;
    signal tb_hostmem_write_req_data: cache_line_type;
    signal tb_hostmem_write_req_valid: std_logic;
    signal tb_hostmem_write_req_almFull: std_logic;
    signal tb_hostmem_write_rsp_mdata: req_id_type;
    signal tb_hostmem_write_rsp_valid: std_logic;

    signal tb_client_in_valid: std_logic;
    signal tb_client_in_ready: ready_0_type;
    signal tb_client_result_data: cache_line_unordered_stream_type;
    signal tb_client_result_valid: std_logic;
    signal tb_client_result_ready: ready_1_type;

begin
    clk_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    reset <= '1', '0' after 4*clk_period;

    hostmem: host_memory port map(
                     clk => clk,
        read_req_address => tb_hostmem_read_req_address,
          read_req_mdata => tb_hostmem_read_req_mdata,
          read_req_valid => tb_hostmem_read_req_valid,
        read_req_almFull => tb_hostmem_read_req_almFull,
           read_rsp_data => tb_hostmem_read_rsp_data,
          read_rsp_mdata => tb_hostmem_read_rsp_mdata,
          read_rsp_valid => tb_hostmem_read_rsp_valid,
       write_req_address => tb_hostmem_write_req_address,
         write_req_mdata => tb_hostmem_write_req_mdata,
          write_req_data => tb_hostmem_write_req_data,
         write_req_valid => tb_hostmem_write_req_valid,
       write_req_almFull => tb_hostmem_write_req_almFull,
         write_rsp_mdata => tb_hostmem_write_rsp_mdata,
         write_rsp_valid => tb_hostmem_write_rsp_valid
    );
    client: memory_reader port map(
                           clk => clk,
                         reset => reset,
        port_mem_out_data.data => tb_hostmem_read_req_address,
      port_mem_out_data.req_id => tb_hostmem_read_req_mdata,
            port_mem_out_valid => tb_hostmem_read_req_valid,
            port_mem_out_ready => '1',
         port_mem_in_data.data => tb_hostmem_read_rsp_data,
       port_mem_in_data.req_id => tb_hostmem_read_rsp_mdata,
             port_mem_in_valid => tb_hostmem_read_rsp_valid,
                 port_in_valid => tb_client_in_valid,
                 port_in_ready => tb_client_in_ready,
                 port_out_data => tb_client_result_data,
                port_out_valid => tb_client_result_valid,
                port_out_ready => tb_client_result_ready
    );

    tb_client_result_ready <= "01";

    stim_proc: process
    begin
        tb_client_in_valid <= '1';
        wait for 2*clk_period;
        tb_client_in_valid <= '0';
        wait for 100*clk_period;
    end process;
end behavioral;
