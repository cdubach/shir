library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity mul_int is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_tuple_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_mul_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end mul_int;

architecture behavioral of mul_int is
    signal mul_in1_reg1: std_logic_vector(port_in_data.t0'range) := (others => '0');
    signal mul_in2_reg1: std_logic_vector(port_in_data.t1'range) := (others => '0');
    signal mul_in1_reg2: std_logic_vector(port_in_data.t0'range) := (others => '0');
    signal mul_in2_reg2: std_logic_vector(port_in_data.t1'range) := (others => '0');
    signal mul_in_valid1: std_logic := '0';
    signal mul_in_valid2: std_logic := '0';
    signal mul_out_reg1: std_logic_vector(port_out_data'range) := (others => '0');
    signal mul_out_reg2: std_logic_vector(port_out_data'range) := (others => '0');
    signal mul_out_valid1: std_logic := '0';
    signal mul_out_valid2: std_logic := '0';

    signal stall_regs: std_logic := '0';

    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');

begin

    in_ready <= "1" when stall_regs = '0' else "0";
    port_in_ready <= in_ready;

    port_out_data <= mul_out_reg2;
    port_out_last <= (others => '0');
    port_out_valid <= mul_out_valid2;

    stall_regs <= '1' when mul_out_valid2 = '1' and port_out_ready = "0" else '0';

    mul_logic: process(clk)
    begin
        -- XXX:
        -- Here we omit the DSP reset since resetting the handshaking signal is sufficient.
        -- IF they were used, they MUST be asynchronous!
        if rising_edge(clk) then
            if stall_regs = '0' then
                mul_in1_reg1 <= port_in_data.t0;
                mul_in2_reg1 <= port_in_data.t1;
                mul_in1_reg2 <= mul_in1_reg1;
                mul_in2_reg2 <= mul_in2_reg1;
                mul_out_reg1 <= std_logic_vector(signed(mul_in1_reg2) * signed(mul_in2_reg2));
                mul_out_reg2 <= mul_out_reg1;
            end if;
        end if;
    end process;

    pipeline_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                mul_in_valid1 <= '0';
                mul_in_valid2 <= '0';
                mul_out_valid1 <= '0';
                mul_out_valid2 <= '0';
            else
                if stall_regs = '0' then
                    mul_in_valid1 <= '0';
                    if port_in_valid = '1' and in_ready = "1" then
                        mul_in_valid1 <= '1';
                    end if;

                    mul_in_valid2 <= mul_in_valid1;
                    mul_out_valid1 <= mul_in_valid2;
                    mul_out_valid2 <= mul_out_valid1;
                end if;
            end if;
        end if;
    end process;

end behavioral;
