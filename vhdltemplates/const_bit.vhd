library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity const_bit is
    generic(
        value: std_logic := '0'
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_out_data: out std_logic;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end const_bit;

architecture behavioral of const_bit is

begin

    port_out_data <= value;
    port_out_last <= (others => '0');
    port_out_valid <= '1';

end behavioral;
