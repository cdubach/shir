library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity rotate_vector is
    generic(
        rotate_left: boolean := '0'
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_vector_tuple_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_vector_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end rotate_vector;

architecture behavioral of rotate_vector is
    type word_vector_type is array (port_out_data'length * 2 - 1 downto 0) of data_word_type;
    signal expand_line: word_vector_type;
begin

    DOUBLE_CONNECT: process(port_in_data.t0)
    begin
        for i in 0 to port_out_data'length - 1 loop
            expand_line(i) <= port_in_data.t0(i);
            expand_line(i + port_out_data'length) <= port_in_data.t0(i);
        end loop;
    end process;

    LEFT_GEN : if rotate_left = '1' generate
        LEFT_CONNECT: process(expand_line, port_in_data.t1)
        begin
            for i in 0 to port_out_data'length - 1 loop
                port_out_data(i) <= expand_line(port_out_data'length - to_integer(unsigned(port_in_data.t1)) + i);
            end loop;
        end process;
    end generate LEFT_GEN;

    RIGHT_GEN : if rotate_left = '0' generate
        RIGHT_CONNECT: process(expand_line, port_in_data.t1)
        begin
            for i in 0 to port_out_data'length - 1 loop
                port_out_data(i) <= expand_line(to_integer(unsigned(port_in_data.t1)) + i);
            end loop;
        end process;
    end generate RIGHT_GEN;

    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

end behavioral;