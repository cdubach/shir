library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity ram_buffer is
    generic(
        data_width: natural := 8
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in memory_input_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end ram_buffer;

--
-- this is a special case for a single-entry "ram" buffer. for this case, the
-- original ram_buffer.vhd would cost AT LEAST 4*data_width registers for data
-- alone because Quartus will use registers to implement the depth-1 ram_sp,
-- combined with the double (triple?) buffered read queue.
--
-- with some assumptions (see below), we can do the same with 1*data_width
-- data registers + a few more for the FSM states.
--
-- regarding FSM states and pipelining:
-- *  we ASSUME the presence of a arbiter-sync-function, meaning a client (a
--    reader or writer but not both) fully finishes before switching.
--    this is also why there are NO transitions between read and write states.
-- *  we do NOT pipeline writes: assuming the typical workload where we write
--    everything and then read everything (multiple times if for caches), we
--    only write once. after all, there is only one entry stored here.
-- *  we DO pipeline reads up to two levels, which is enough for our cases.
--
architecture behavioral of ram_buffer is
    signal reg: std_logic_vector(port_in_data.data'range) := (others => '0');

    type state_type is (idle, write, read1, read2);
    signal state: state_type := idle;
begin

    port_out_data <= reg;
    port_out_last <= (others => '0');

    with state select port_out_valid <= '0' when idle, '1' when others;
    with state select port_in_ready <= "1" when idle | read1, "0" when others;

    process(clk)
    begin
        if rising_edge(clk) then
            if port_in_valid = '1' and port_in_data.we = '1' then
                reg <= port_in_data.data;
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= idle;
            else
                case state is
                    when idle =>
                        if port_in_valid = '1' then
                            if port_in_data.we = '1' then
                                state <= write;
                            else
                                state <= read1;
                            end if;
                        end if;
                    when write =>
                        if port_out_ready(port_out_ready'low) = '1' then
                            state <= idle;
                        end if;
                    when read1 =>
                        if port_in_valid = '1' and port_in_data.we = '0' then
                            if port_out_ready(port_out_ready'low) = '0' then
                                state <= read2;
                            end if;
                        elsif port_out_ready(port_out_ready'low) = '1' then
                            state <= idle;
                        end if;
                    when read2 =>
                        if port_out_ready(port_out_ready'low) = '1' then
                            state <= read1;
                        end if;
                end case;
            end if;
        end if;
    end process;

end behavioral;

