library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity clip_bankers_round_shift is
    port(
        clk: id std_logic;
        reset: in std_logic;
        port_in_data: in data_word_tuple_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end clip_bankers_round_shift;

architecture behavioral of clip_bankers_round_shift is
    signal out_value: std_logic_vector(port_out_data'high downto port_out_data'low) := (others => '0');
begin
    port_out_data <= out_value;
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

    process(port_in_data)
        constant max_value: std_logic_vector(port_out_data'high downto port_out_data'low) := (port_out_data'high => '0', others => '1');
        constant min_value: std_logic_vector(port_out_data'high downto port_out_data'low) := (port_out_data'high => '1', others => '0');

        variable shifted_value: std_logic_vector(port_out_data'high downto port_out_data'low) := (others => '0');
    begin
        if signed(port_in_data.t1) >= port_out_data'length then
            -- oversized left shift: saturate based on sign
            if signed(port_in_data.t0) > 0 then
                out_value <= max_value;
            elsif signed(port_in_data.t0) < 0 then
                out_value <= min_value;
            else
                out_value <= (others => '0');
            end if;

        elsif signed(port_in_data.t1) <= -port_out_data'length then
            -- oversized right shift: -0.5 <= (sext(X) >> -s) <= 0.5
            -- bankers rounding is always 0
            out_value <= (others => '0');

        elsif signed(port_in_data.t1) > 0 then
            shifted_value := std_logic_vector(shift_left(signed(port_in_data.t0), to_integer(signed(port_in_data.t1))));

            if shift_right(signed(shifted_value), to_integer(signed(port_in_data.t1))) /= signed(port_in_data.t0) then
                -- left shift overflowed: saturate based on sign
                if signed(port_in_data.t0) > 0 then
                    shifted_value := max_value;
                else
                    shifted_value := min_value;
                end if;
            end if;
            out_value <= shifted_value;

        elsif signed(port_in_data.t1) < 0 then
            shifted_value := std_logic_vector(shift_right(signed(port_in_data.t0), -to_integer(signed(port_in_data.t1))));

            if port_in_data.t0(-to_integer(signed(port_in_data.t1)) - 1) = '1' and (
                shifted_value(0) = '1' or
                shift_left(signed(port_in_data.t0), to_integer(signed(port_in_data.t1)) + port_out_data'length + 1) /= 0
            ) then
                -- we have odd with at least .5 OR even with more than .5:
                -- right shift should be rounded
                shifted_value := std_logic_vector(signed(shifted_value) + 1);
            end if;
            out_value <= shifted_value;

        else
            -- shifting by zero: identity
            out_value <= port_in_data.t0;

        end if;
    end process;
end behavioral;

