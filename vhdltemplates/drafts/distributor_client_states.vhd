library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity distributor is
    generic(
        num_clients: natural := 2
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_stream_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;

        port_out_clients_data: out data_word_stream_vector_generic_type(num_clients - 1 downto 0);
        port_out_clients_last: out last_1_vector_generic_type(num_clients - 1 downto 0);
        port_out_clients_valid: out std_logic_vector(num_clients - 1 downto 0);
        port_out_clients_ready: in ready_1_vector_generic_type(num_clients - 1 downto 0)
    );
end distributor;

architecture behavioral of distributor is

    --signal clients_in_sync: std_logic := '1';

    --signal clients_finished: std_logic_vector(num_clients - 1 downto 0) := (others => '0');
    --signal clients_ready: std_logic_vector(port_in_ready'range) := (others => '1');

begin

    -- TODO this is not very generic!
    port_out_clients_data <= (others => port_in_data);
    port_out_clients_last <= (others => port_in_last);
    port_out_clients_valid <= (others => port_in_valid);

    ready_signal: process(port_out_clients_ready)
        variable ready: std_logic_vector(port_in_ready'range) := (others => '1');
    begin
    --    if clients_in_sync = '1' then
            ready := port_out_clients_ready(0);
            for i in 1 to num_clients - 1 loop
                ready := ready and port_out_clients_ready(i);
            end loop;
    --    else
    --        ready := clients_ready;
    --    end if;
        port_in_ready <= ready;
    end process;

    --sync_logic: process(clk)

    --finished_states: process(clk)
    --begin
    --    if rising_edge(clk) then
    --        if reset = '1' then
    --            clients_finished <= (others => 0);
    --        else
    --            if port_in_valid = '1' then
    --                for i in dimensions'low to dimensions'high loop
    --                    if port_out_ready(i) = '1' then
    --                        if counter_dimensions(i) < dimensions(i) - 1 then
    --                            counter_dimensions(i) <= counter_dimensions(i) + 1;
    --                        else
    --                            counter_dimensions(i) <= 0;
    --                        end if;
    --                    end if;
    --                end loop;
    --            end if;
    --        end if;
    --    end if;
    --end process;


end behavioral;
