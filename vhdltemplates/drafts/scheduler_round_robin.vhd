library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity scheduler_round_robin is
    generic(
        num_clients: natural := 2
    );
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- vector of valid signal for each client
        port_in_data: in std_logic_vector(num_clients - 1 downto 0);
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;

        -- index of next selected client
        port_out_data: out scheduler_output_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end scheduler_round_robin;

architecture behavioral of scheduler_round_robin is

    signal selected_client: natural range 0 to num_clients - 1 := 0;

begin

    port_in_ready <= port_out_ready;

    port_out_data.cli_id <= std_logic_vector(to_unsigned(selected_index), port_out_data'length));
    port_out_data.valid <= port_in_data(selected_client);
    port_out_valid <= port_in_valid;

    -- simple round robin WITH possible wait times! (if a client is not trying to get the ressource (valid = '0'))
    select_client_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                selected_client <= 0;
            else
                if port_in_valid = '1' and port_out_ready = "1" then
                    if selected_client = num_clients - 1 then
                        selected_client <= 0;
                    else
                        selected_client <= selected_client + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
