library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

-- ReduceStm template with the consideration of pipeline length of function f.
entity reduce_ordered_stream is
    generic(
        pipeline_length: natural := 2
    );
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- stream of data to reduce
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        -- initial value for accumulator
        port_in_init_data: in data_word_type;
        port_in_init_last: in last_0_type;
        port_in_init_valid: in std_logic;
        port_in_init_ready: out ready_0_type;
        -- accumulator output
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end reduce_ordered_stream;

architecture behavioral of reduce_ordered_stream is

    component add is -- %TESTING_ONLY
        port( -- %TESTING_ONLY
            clk: in std_logic; -- %TESTING_ONLY
            reset: in std_logic; -- %TESTING_ONLY
            port_in_1_data: in data_word_type; -- %TESTING_ONLY
            port_in_1_last: in last_0_type; -- %TESTING_ONLY
            port_in_1_valid: in std_logic; -- %TESTING_ONLY
            port_in_1_ready: out ready_0_type; -- %TESTING_ONLY
            port_in_2_data: in data_word_type; -- %TESTING_ONLY
            port_in_2_last: in last_0_type; -- %TESTING_ONLY
            port_in_2_valid: in std_logic; -- %TESTING_ONLY
            port_in_2_ready: out ready_0_type; -- %TESTING_ONLY
            port_out_data: out data_word_type; -- %TESTING_ONLY
            port_out_last: out last_0_type; -- %TESTING_ONLY
            port_out_valid: out std_logic; -- %TESTING_ONLY
            port_out_ready: in ready_0_type -- %TESTING_ONLY
        ); -- %TESTING_ONLY
    end component; -- %TESTING_ONLY

    type state_type is (init_acc, reducing);
    signal state: state_type := init_acc;

    -- accumulates the values in this stream
    signal buf: std_logic_vector(port_out_data'range) := (others => '0');
    signal buf_valid: std_logic := '1'; -- buffer contains a value that is ready to be processes for the next iteration
    signal buf_last: std_logic := '0'; -- if the buffer contains the reduction up to the last value of the stream => output is ready!
    signal f_last: std_logic := '0'; -- if the function is currently working of the last value of the stream

    signal port_f_out_acc_data: data_word_type; -- %TESTING_ONLY
    signal port_f_out_acc_last: last_0_type; -- %TESTING_ONLY
    signal port_f_out_acc_valid: std_logic; -- %TESTING_ONLY
    signal port_f_out_acc_ready: ready_0_type; -- %TESTING_ONLY
    signal port_f_out_data_data: data_word_type; -- %TESTING_ONLY
    signal port_f_out_data_last: last_0_type; -- %TESTING_ONLY
    signal port_f_out_data_valid: std_logic; -- %TESTING_ONLY
    signal port_f_out_data_ready: ready_0_type; -- %TESTING_ONLY
    signal port_f_in_data: data_word_type; -- %TESTING_ONLY
    signal port_f_in_last: last_0_type; -- %TESTING_ONLY
    signal port_f_in_valid: std_logic; -- %TESTING_ONLY
    signal port_f_in_ready: ready_0_type; -- %TESTING_ONLY

    signal pipeline_count: natural range 0 to pipeline_length := 0;
    signal first_arrival_reg: std_logic := '0';
    signal first_arrival: std_logic := '0';
    signal last_emission: std_logic := '0';
    signal last_emission_reg: std_logic := '0';
    constant port_in_last_all_one: std_logic_vector(port_in_last'range) := (others => '1');
begin
    
    f: add port map( -- %TESTING_ONLY
        clk => clk, -- %TESTING_ONLY
        reset => reset, -- %TESTING_ONLY
        port_in_1_data => port_f_out_acc_data, -- %TESTING_ONLY
        port_in_1_last => port_f_out_acc_last, -- %TESTING_ONLY
        port_in_1_valid => port_f_out_acc_valid, -- %TESTING_ONLY
        port_in_1_ready => port_f_out_acc_ready, -- %TESTING_ONLY
        port_in_2_data => port_f_out_data_data, -- %TESTING_ONLY
        port_in_2_last => port_f_out_data_last, -- %TESTING_ONLY
        port_in_2_valid => port_f_out_data_valid, -- %TESTING_ONLY
        port_in_2_ready => port_f_out_data_ready, -- %TESTING_ONLY
        port_out_data => port_f_in_data, -- %TESTING_ONLY
        port_out_last => port_f_in_last, -- %TESTING_ONLY
        port_out_valid => port_f_in_valid, -- %TESTING_ONLY
        port_out_ready => port_f_in_ready -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    port_out_data <= buf;
    port_out_valid <= '1' when state = reducing and buf_valid = '1' and buf_last = '1' else '0';

    port_f_out_acc_data <= buf;
    port_f_out_acc_last <= (others => '0');
    -- After sending last item, valid signal must be de-asserted.
    port_f_out_acc_valid <= '1' when state = reducing and buf_valid = '1' and buf_last = '0' and last_emission_reg = '0' else '0';

    port_f_out_data_data <= port_in_data;
    port_f_out_data_last <= port_in_last(port_f_out_data_last'high downto port_f_out_data_last'low);
    -- After sending last item, valid signal must be de-asserted.
    port_f_out_data_valid <= '1' when state = reducing and port_in_valid = '1' and buf_last = '0' and last_emission_reg = '0' else '0'; 

    port_in_ready <= (port_in_last(port_in_last'high) and port_f_out_data_ready(port_f_out_data_ready'high)) & port_f_out_data_ready when state = reducing and last_emission_reg = '0' else (others => '0');    
    -- there will never be the request to repeat the stream, because the result of the reduction is just an element, that can be "reread"
    port_in_init_ready <= "1" when state = init_acc else "0";
    port_f_in_ready <= "1" when state = reducing and buf_last = '0' else "0";

    state_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= init_acc;
            else
                case state is
                    when init_acc =>
                        if port_in_init_valid = '1' then
                            state <= reducing;
                        end if;
                    when reducing =>
                        -- reset buffer to initial_value, when the final value has been transmitted
                        if buf_valid = '1' and buf_last = '1' and port_out_ready(port_out_ready'high) = '1' then
                            state <= init_acc;
                        end if;
                end case;
            end if;
        end if;
    end process;

    buffer_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                buf <= (others => '0');
                buf_valid <= '0';
                buf_last <= '0';
                f_last <= '0';
            else
                case state is
                    when init_acc =>
                        if port_in_init_valid = '1' then
                            buf <= (others => '0');
                            buf(port_in_init_data'high downto port_in_init_data'low) <= port_in_init_data;
                            buf_valid <= '1';
                            buf_last <= '0';
                        end if;
                        -- f_last implies function's last signal. Need to be reset.
                        f_last <= '0';
                    when reducing =>
                        -- new data just entered function f, buffer contains 'old' results and is not valid anymore
                        -- After sending last item, buf_valid should not be de-asserted. (There must be a valid item in buffer.)
                        if port_f_out_acc_valid = '1' and port_f_out_data_valid = '1' and port_f_out_acc_ready(port_f_out_acc_ready'high) = '1' and port_f_out_data_ready(port_f_out_data_ready'high) = '1' and last_emission_reg = '0' then
                            buf_valid <= '0';
                            f_last <= port_in_last(port_in_last'high);
                        end if;
                        -- if function f is done, store result in buffer and mark as valid.
                        -- Note that pipeline_count will only be valid after first item arrrives.
                        if (port_f_in_valid = '1' and port_f_in_ready(port_f_in_ready'high) = '1') or (pipeline_count < pipeline_length and (first_arrival = '1' or first_arrival_reg = '1')) then
                            buf <= port_f_in_data(buf'high downto buf'low);
                            buf_valid <= '1';
                            -- when pipeline_count is larger than zero, there are still some items processed by funtion f. Must wait until all items are finished.
                            if pipeline_count > 0 then
                                buf_last <= '0';
                            -- seems like this function has 0 cycles of delay => directly forward last signal
                            elsif (port_f_out_acc_valid = '1' and port_f_out_data_valid = '1' and port_f_out_data_ready(port_f_out_data_ready'high) = '1') and last_emission_reg = '0' then
                                buf_last <= port_in_last(port_in_last'high);
                            else
                                buf_last <= f_last;
                            end if;
                        end if;
                end case;
            end if;
        end if;
    end process;

    -- Counting the items that are sent to function f but not returned.
    queue_logic: process(clk)
        variable pipeline_count_v: natural range 0 to pipeline_length := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                pipeline_count <= 0;
            else
                pipeline_count_v := pipeline_count;
                if port_f_out_acc_valid = '1' and port_f_out_data_valid = '1' and port_f_out_acc_ready(port_f_out_acc_ready'high) = '1' and port_f_out_data_ready(port_f_out_data_ready'high) = '1' and pipeline_count < pipeline_length and last_emission_reg = '0' then
                    pipeline_count_v := pipeline_count_v + 1;
                end if;

                if port_f_in_valid = '1' and port_f_in_ready(port_f_in_ready'high) = '1' and pipeline_count > 0 then
                    pipeline_count_v := pipeline_count_v - 1;
                end if;

                pipeline_count <= pipeline_count_v;
            end if;
        end if;
    end process;

    -- first_arrival indicates that first valid data arrives.
    first_arrival <= '1' when state = reducing and port_in_valid = '1' and port_in_init_valid = '1' else '0';

    first_arrival_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                first_arrival_reg <= '0';
            else
                if state = init_acc then
                    first_arrival_reg <= '0';
                elsif first_arrival_reg = '0' then
                    first_arrival_reg <= first_arrival;
                end if;
            end if;
        end if;
    end process;

    -- last_emission indicates that last valid data is sent to function f.
    last_emission <= '1' when state = reducing and port_in_valid = '1' and port_in_last = port_in_last_all_one and port_f_out_data_ready(port_f_out_data_ready'low) = '1' else '0';

    last_emission_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                last_emission_reg <= '0';
            else
                if state = init_acc then
                    last_emission_reg <= '0';
                elsif last_emission_reg = '0' then
                    last_emission_reg <= last_emission;
                end if;
            end if;
        end if;
    end process;

end behavioral;
