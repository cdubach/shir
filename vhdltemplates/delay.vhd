library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity delay is
    generic(
        delay_cycles: natural := 100
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end delay;

architecture behavioral of delay is
    signal counter: natural range 0 to delay_cycles := delay_cycles;
begin

    port_out_data <= port_in_data;
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid when counter = 0 else '0';
    port_in_ready <= port_out_ready when counter = 0 else (others => '0');

    counter_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter <= delay_cycles;
            else
                if counter > 0 then
                    counter <= counter - 1;
                else
                    if port_in_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                        counter <= delay_cycles;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
