library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity counter is
    generic(
        start: natural := 0;
        increments: natural_vector_type := (2, 5, 3);
        dimensions: natural_vector_type := (2, 5, 3); -- read from right to left (innermost dimension to outermost)
        repetitions: std_logic_vector := "110"; -- read from left to right (innermost to outermost repeat)
        loop_all: std_logic := '0'
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_out_data: out data_word_type;
        port_out_last: out last_3_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_3_type
    );
end counter;

architecture behavioral of counter is

    -- function to precompute constants (during compilation)
    function precomp_increments return natural_vector_type is
        variable increment_per_dimension: natural_vector_type := (others => 0);
    begin
        if repetitions(0) = '1' then
            increment_per_dimension(0) := increments(0);
        else
            increment_per_dimension(0) := dimensions(0) * increments(0);
        end if;

        -- multiplication accumulate
        for i in dimensions'low + 1 to dimensions'high loop
            if repetitions(i) = '1' then
                increment_per_dimension(i) := increment_per_dimension(i - 1);
            else
                increment_per_dimension(i) := increments(i) * dimensions(i);
            end if;
        end loop;

        -- Assume that increment of repeated dimension is always one.
        for i in dimensions'low to dimensions'high loop
            if repetitions(i) = '1' then
                increment_per_dimension(i) := dimensions(i);
            end if;
        end loop;

        return increment_per_dimension;
    end function;

    function get_max return natural is
        variable accumulation: natural := 0;
    begin
        for i in dimensions'low to dimensions'high loop
            if repetitions(i) = '0' then
                accumulation := accumulation + increments(i) * (dimensions(i) - 1);
            end if;
        end loop;
        return accumulation;
    end function;

    signal counter_value: natural := 0;
    signal counter_values: natural_vector_type := (others => 0);
    signal counter_dimensions: natural_vector_type := (others => 0);
    constant increment_per_dimension: natural_vector_type := precomp_increments;
    constant max_val: natural := get_max;

    signal out_last: std_logic_vector(port_out_last'range) := (others => '0');
    signal out_valid: std_logic := '1';

begin

    port_out_data <= std_logic_vector(to_unsigned(counter_value + start, port_out_data'length));
    port_out_last <= out_last;
    port_out_valid <= out_valid;

    last_signals: process(counter_dimensions)
    begin
        out_last <= (others => '0');
        for i in dimensions'low to dimensions'high loop
            if counter_dimensions(i) = dimensions(i) - 1 then
                out_last(i) <= '1';
            end if;
        end loop;
    end process;

    counter_dimensions_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_dimensions <= (others => 0);
            else
                if out_valid = '1' then
                    for i in dimensions'low to dimensions'high loop
                        if port_out_ready(i) = '1' then
                            if counter_dimensions(i) < dimensions(i) - 1 then
                                counter_dimensions(i) <= counter_dimensions(i) + 1;
                            else
                                counter_dimensions(i) <= 0;
                            end if;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;

    sum_values: process(counter_values)
        variable counter_value_next: natural := 0;
    begin
        counter_value_next := 0;
        for i in dimensions'low to dimensions'high loop
            if repetitions(i) = '0' then
                counter_value_next := counter_value_next + counter_values(i);
            end if;
        end loop;
        counter_value <= counter_value_next;
    end process;

    counter_value_logic: process(clk)
        variable counter_values_next: natural_vector_type := (others => 0);
        variable propogate_status: std_logic := '1';
        variable soft_reset_flag: std_logic := '1';
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_values <= (others => 0);
                out_valid <= '1';
            else
                if out_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                    for i in dimensions'low to dimensions'high loop
                        counter_values_next(i) := counter_values(i);
                    end loop;
                    -- increase value
                    propogate_status := '1';
                    if counter_values(0) <= increment_per_dimension(0) - increments(0) - 1 then
                        counter_values_next(0) := counter_values(0) + increments(0);
                        propogate_status := propogate_status and '0';
                    else
                        counter_values_next(0) := 0;
                        propogate_status := propogate_status and '1';
                    end if;
                    for i in dimensions'low + 1 to dimensions'high loop
                        if propogate_status = '1' then
                            if counter_values(i) <= increment_per_dimension(i) - increments(i) - 1 then
                                counter_values_next(i) := counter_values(i) + increments(i);
                                propogate_status := propogate_status and '0';
                            else
                                counter_values_next(i) := 0;
                                propogate_status := propogate_status and '1';
                            end if;
                        end if;
                    end loop;
                    if counter_value >= max_val then
                        if port_out_ready(port_out_ready'high) = '1' then
                            -- finished counting, do not repeat, if loop_all is not set
                            out_valid <= loop_all;
                        end if;
                    end if;

                    soft_reset_flag := '0';
                    for i in dimensions'high downto dimensions'low loop
                        if (out_last(i) = '1' and port_out_ready(i) = '1' and port_out_ready(i + 1) = '0') or soft_reset_flag = '1' then
                            --assert(counter_values(i) + increments(i) - increment_per_dimension(i) >= 0) report "error in incoming ready signals";
                            counter_values_next(i) := counter_values(i) + increments(i) - increment_per_dimension(i);
                            soft_reset_flag := '1';
                        end if;
                    end loop;


                    for i in dimensions'low to dimensions'high loop
                        counter_values(i) <= counter_values_next(i);
                    end loop;
                end if;
            end if;
        end if;
    end process;

end behavioral;