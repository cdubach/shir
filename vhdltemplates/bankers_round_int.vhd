library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity bankers_round_int is
    generic(
        low_elements: natural := 2;
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end bankers_round_int;

architecture behavioral of bankers_round_int is
    signal round_value: unsigned(port_in_data'high + 1 - low_elements downto port_in_data'low) := (others => '0');
    signal round_hi: unsigned(round_value'range) := (others => '0');
    signal round_lo: std_logic_vector(low_elements + 2 downto port_in_data'low) := (others => '0');
    constant lo_zeros: std_logic_vector(round_lo'high - 2 downto round_lo'low) := (others => '0');
begin
    round_hi <= resize(unsigned(port_in_data(port_in_data'high downto low_elements)), round_hi'length);
    round_lo <= port_in_data(low_elements downto port_in_data'low) & "00";

    -- round if at least 0.5 and (odd or more than 0.5)
    round_value <= round_hi + 1 when (
        round_lo(round_lo'high - 1) = '1' and
        (round_lo(round_lo'high) = '1' or round_lo(lo_zeros'range) /= lo_zeros)
    ) else round_hi;

    port_out_data <= std_logic_vector(resize(round_value, port_out_data'length));

    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;
end behavioral;
