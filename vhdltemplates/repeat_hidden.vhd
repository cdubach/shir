library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity repeat_hidden is
    generic(
        stream_length: natural := 10
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end repeat_hidden;

architecture behavioral of repeat_hidden is

    component repeat_hidden -- %TESTING_ONLY
        port( -- %TESTING_ONLY
            clk: in std_logic; -- %TESTING_ONLY
            reset: in std_logic; -- %TESTING_ONLY
            port_out_data: out data_word_type; -- %TESTING_ONLY
            port_out_last: out last_1_type; -- %TESTING_ONLY
            port_out_valid: out std_logic; -- %TESTING_ONLY
            port_out_ready: in ready_1_type -- %TESTING_ONLY
        ); -- %TESTING_ONLY
    end component; -- %TESTING_ONLY

    signal elem_counter: natural range 0 to stream_length - 1 := 0;

    signal port_f_in_data: data_word_type; -- %TESTING_ONLY
    signal port_f_in_last: last_1_type; -- %TESTING_ONLY
    signal port_f_in_valid: std_logic; -- %TESTING_ONLY
    signal port_f_in_ready: ready_1_type; -- %TESTING_ONLY

begin

    f: repeat_hidden port map( -- %TESTING_ONLY
        clk => clk, -- %TESTING_ONLY
        reset => reset, -- %TESTING_ONLY
        port_out_data => port_f_in_data, -- %TESTING_ONLY
        port_out_last => port_f_in_last, -- %TESTING_ONLY
        port_out_valid => port_f_in_valid, -- %TESTING_ONLY
        port_out_ready => port_f_in_ready -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    port_out_data <= port_f_in_data;
    port_out_last <= port_f_in_last;
    port_out_valid <= port_f_in_valid;

    ready_signal: process(port_out_ready, elem_counter)
    begin
        port_f_in_ready <= port_out_ready;
        if elem_counter < stream_length - 1 then
            port_f_in_ready(port_f_in_ready'high) <= '0';
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                elem_counter <= 0;
            else
                if port_f_in_valid = '1' and port_out_ready(port_out_ready'high) = '1' then
                    if elem_counter < stream_length - 1 then
                        elem_counter <= elem_counter + 1;
                    else
                        elem_counter <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
