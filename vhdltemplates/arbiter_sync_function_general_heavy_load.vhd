library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity arbiter_sync_function is
    generic(
        num_clients: natural := 2
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        -- to master
        port_out_1_data: out function_in_type; -- %TESTING_ONLY
        port_out_1_last: out last_0_type; -- %TESTING_ONLY
        port_out_1_valid: out std_logic; -- %TESTING_ONLY
        port_out_1_ready: in ready_0_type; -- %TESTING_ONLY
        port_out_2_data: out function_in_type; -- %TESTING_ONLY
        port_out_2_last: out last_0_type; -- %TESTING_ONLY
        port_out_2_valid: out std_logic; -- %TESTING_ONLY
        port_out_2_ready: in ready_0_type; -- %TESTING_ONLY
        -- from master
        port_in_data: in function_out_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;

        -- from clients
        port_in_clients_1_data: in function_in_vector_type(num_clients-1 downto 0); -- %TESTING_ONLY
        port_in_clients_1_last: in function_last_vector_type(num_clients-1 downto 0); -- %TESTING_ONLY
        port_in_clients_1_valid: in std_logic_vector(num_clients-1 downto 0); -- %TESTING_ONLY
        port_in_clients_1_ready: out function_ready_vector_type(num_clients-1 downto 0); -- %TESTING_ONLY
        port_in_clients_2_data: in function_in_vector_type(num_clients-1 downto 0); -- %TESTING_ONLY
        port_in_clients_2_last: in function_last_vector_type(num_clients-1 downto 0); -- %TESTING_ONLY
        port_in_clients_2_valid: in std_logic_vector(num_clients-1 downto 0); -- %TESTING_ONLY
        port_in_clients_2_ready: out function_ready_vector_type(num_clients-1 downto 0); -- %TESTING_ONLY
        -- to clients
        port_out_clients_data: out function_out_vector_type(num_clients-1 downto 0);
        port_out_clients_last: out function_last_vector_type(num_clients-1 downto 0);
        port_out_clients_valid: out std_logic_vector(num_clients-1 downto 0);
        port_out_clients_ready: in function_ready_vector_type(num_clients-1 downto 0)
    );
end arbiter_sync_function;

architecture behavioral of arbiter_sync_function is

    subtype client_id_type is natural range 0 to num_clients-1;

    signal selected_client: client_id_type := 0;

    signal port_in_clients_all_valid: std_logic_vector(num_clients-1 downto 0);
    signal one_remaining_item: std_logic := '0';

    signal start_arrival: std_logic := '0';
    signal start_count_1: natural := 0; -- %TESTING_ONLY
    signal start_count_1_in: natural := 0; -- %TESTING_ONLY
begin

    port_in_clients_all_valid(selected_client) <= '1' when port_in_clients_1_valid(selected_client) = '1' and port_in_clients_2_valid(selected_client) = '1' else '0'; -- %TESTING_ONLY
    one_remaining_item <= '1' when start_count_1_in = 1 and start_count_2_in = 1 else '0'; -- %TESTING_ONLY

    -- from master
    port_out_clients_data <= (others => port_in_data);
    port_out_clients_last <= (others => port_in_last);
    clients_valid_signal: process(selected_client, port_in_valid)
    begin
        port_out_clients_valid <= (others => '0');
        port_out_clients_valid(selected_client) <= port_in_valid;
    end process;
    port_in_ready <= port_out_clients_ready(selected_client);

    -- simple round robin WITH possible wait times! (if a client is not ready to make a request)
    select_client_logic: process(clk)
        variable next_client: client_id_type := 0;
        variable found_next: std_logic := '0';
    begin
        if rising_edge(clk) then
            if reset = '1' then
                selected_client <= 0;
            else
                --if selected_client = client_id_type'high then
                --    next_client := client_id_type'low;
                --else
                --    next_client := selected_client + 1;
                --end if;
                next_client := selected_client;
                if next_client = client_id_type'high then
                    next_client := client_id_type'low;
                else
                    next_client := next_client + 1;
                end if;

                -- remove second condition here to go back to true round robin without possible starvation of clients
                if not (port_in_clients_all_valid(selected_client) = '1' and port_out_clients_ready(selected_client)(port_in_ready'low) = '1') and start_arrival = '0' then
                    selected_client <= next_client;
                end if;
            end if;
        end if;
    end process;

    start_logic: process(clk)
        constant last_all_one: std_logic_vector(port_out_clients_last(0)'range) := (others => '1');
    begin
        if rising_edge(clk) then
            if reset = '1' then
                start_arrival <= '0';
            else
                -- start_count1_in and start_count2_in indicate the number of elements to be processed.
                -- If output is ready and there is only one element, start_arrival can be deasserted (which means the function is free).
                if port_in_valid = '1' and port_in_last = last_all_one and port_out_clients_ready(selected_client)(port_in_ready'low) = '1' and one_remaining_item = '1' then
                    start_arrival <= '0';
                elsif port_in_clients_all_valid(selected_client) = '1' and port_out_clients_ready(selected_client)(port_in_ready'low) = '1' then
                    start_arrival <= '1';
                end if;
            end if;
        end if;
    end process;

end behavioral;