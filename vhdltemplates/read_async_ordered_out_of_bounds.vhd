library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity read_async is
    generic(
        data_length: natural :=  8;
        padded_value: std_logic_vector(511 downto 0) := (others => '0');
        physical_addr_limit: natural := 1048575
    );
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- to controller
        port_mem_out_data: out request_mem_address_type;
        port_mem_out_valid: out std_logic;
        port_mem_out_ready: in std_logic;
        -- from controller
        port_mem_in_data: in request_cache_line_type;
        port_mem_in_valid: in std_logic;
        -- stream of addresses
        port_in_data: in data_word_stream_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        -- base address (added to address)
        port_in_baseaddr_data: in base_address_type;
        port_in_baseaddr_last: in last_0_type;
        port_in_baseaddr_valid: in std_logic;
        port_in_baseaddr_ready: out ready_0_type;

        -- outgoing ordered stream of tuple of data and address
        port_out_data: out data_word_stream_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end read_async;

architecture behavioral of read_async is

    component ram_dp
        generic (
            data_width: natural := 32;
            addr_width: natural := 4;
            entries: natural := 8
        );
        port(
            clk: in std_logic;
            data_in: in std_logic_vector(data_width - 1 downto 0);
            write_addr: in std_logic_vector(addr_width - 1 downto 0);
            read_addr: in std_logic_vector(addr_width - 1 downto 0);
            we: in std_logic;
            data_out: out std_logic_vector(data_width - 1 downto 0)
        );
    end component;

    constant addr_width: natural := integer(ceil(log2(real(data_length))));

    signal write_addr: std_logic_vector(addr_width - 1 downto 0) := (others => '0');
    signal read_addr: std_logic_vector(addr_width - 1 downto 0) := (others => '0');
    signal write_enable: std_logic := '0';
    signal ram_data_out: std_logic_vector(port_out_data'range) := (others => '0');

    signal req_id: natural range 0 to data_length - 1 := 0;

    type state_type is (init, requesting, receiving);
    signal state: state_type := init;

    signal data_buffer_valid: std_logic_vector(0 to data_length - 1) := (others => '0');

    signal sending_entry: natural range 0 to data_length - 1 := 0;


    signal data_buffer_out_last: std_logic_vector(port_out_last'range) := "0";
    signal data_buffer_out_valid: std_logic := '0';


    signal pipeline_count: natural range 0 to 1 := 0;

    type output_buffer_type is array (0 to 2) of std_logic_vector(port_out_data'range);
    signal output_buffer: output_buffer_type := (others => (others => '0'));
    type output_buffer_last_type is array (0 to 2) of std_logic_vector(port_out_last'range);
    signal output_last_buffer: output_buffer_last_type := (others => (others => '0'));
    signal output_buffer_count: natural range 0 to (output_buffer_type'high + 1) := 0;

    signal output_buffer_ready: std_logic := '1';

    subtype output_buffer_index_type is natural range output_buffer_type'range;
    signal next_free_output_buffer_entry: output_buffer_index_type := 0;
    signal first_used_output_buffer_entry: output_buffer_index_type := 0;

    signal out_last: std_logic_vector(port_out_last'range) := "1";
    signal out_valid: std_logic := '1';

    signal addr_limit_lock: std_logic := '0';
    signal converted_addr: data_word_stream_type;
    signal invalid_addr: std_logic := '0';
    signal invalid_addr_valid: std_logic := '0';
    signal write_data: data_word_type;
    signal stored_req_id: natural range 0 to data_length - 1 := 0;
    signal addr_valid: std_logic := '0';
    signal req_flag: std_logic := '0';
begin

    inferred_ram: ram_dp
    generic map(
        data_width => port_out_data'length,
        addr_width => write_addr'length,
        entries => data_length
    )
    port map(
        clk => clk,
        data_in => write_data,
        write_addr => write_addr,
        read_addr => read_addr,
        we => write_enable,
        data_out => ram_data_out
    );


    write_addr <= port_mem_in_data.req_id(write_addr'high downto write_addr'low) when port_mem_in_valid = '1' else
        std_logic_vector(to_unsigned(stored_req_id, write_addr'length)) when addr_limit_lock = '1' else
        std_logic_vector(to_unsigned(req_id, write_addr'length));
    read_addr <= std_logic_vector(to_unsigned(sending_entry, read_addr'length));
    write_enable <= port_mem_in_valid or addr_limit_lock or (invalid_addr_valid and addr_valid and req_flag);
    write_data <= port_mem_in_data.data when port_mem_in_valid = '1' else padded_value;

    -- requests
    port_mem_out_data.data <= std_logic_vector(to_unsigned(
                              to_integer(unsigned(port_in_baseaddr_data)) +
                              to_integer(unsigned(converted_addr)),
                              port_mem_out_data.data'length)) when state = requesting else (others => '0');
    port_mem_out_data.req_id <= std_logic_vector(to_unsigned(req_id, port_mem_out_data.req_id'length));
    port_mem_out_valid <= '1' when state = requesting and port_in_valid = '1' and port_in_baseaddr_valid = '1' and invalid_addr = '0' else '0';
    port_in_ready <= port_in_last(port_in_last'high) & '1' when state = requesting and (port_mem_out_ready = '1' or invalid_addr_valid = '1') and addr_valid = '1' else (others => '0');

    port_in_baseaddr_ready <= "1" when port_in_last(port_in_last'high) = '1' and state = requesting and (port_mem_out_ready = '1' or invalid_addr_valid = '1') and addr_valid = '1' else (others => '0');




    output_buffer_ready <= '1' when output_buffer_count + pipeline_count <= output_buffer_type'high else '0';

    port_out_data <= output_buffer(first_used_output_buffer_entry);
    out_last <= output_last_buffer(first_used_output_buffer_entry);
    port_out_last <= out_last;
    out_valid <= '1' when output_buffer_count > 0 and state /= init else '0';
    port_out_valid <= out_valid;

    -- new logic for invalid addr
    addr_valid <= port_in_valid and port_in_baseaddr_valid;
    converted_addr <= port_in_data; -- %TESTING_ONLY
    invalid_addr <= '1' when to_integer(unsigned(converted_addr)) >= physical_addr_limit else '0';
    invalid_addr_valid <= '1' when invalid_addr = '1' and addr_limit_lock = '0' else '0';
    req_flag <= '1' when state = requesting else '0';

    lock_logic: process(clk)
        variable addr_limit_lock_v: std_logic := '0';
    begin
        if rising_edge(clk) then
            if reset = '1' or state = init then
                addr_limit_lock <= '0';
                stored_req_id <= 0;
            else
                if state = requesting and invalid_addr_valid = '1' and port_in_valid = '1' and port_in_baseaddr_valid = '1' then
                    addr_limit_lock_v := '1';
                    stored_req_id <= req_id;
                end if;

                if port_mem_in_valid = '0' then
                    addr_limit_lock_v := '0';
                    stored_req_id <= 0;
                end if;

                addr_limit_lock <= addr_limit_lock_v;
            end if;
        end if;
    end process;

    -- end of new logic

    next_state_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= init;
            else
                case state is
                    when init =>
                        if port_in_valid = '1' and port_in_baseaddr_valid = '1' then
                            state <= requesting;
                        end if;
                    when requesting =>
                        if (port_mem_out_ready = '1' or invalid_addr_valid = '1') and port_in_valid = '1' and port_in_baseaddr_valid = '1' then
                            if req_id >= data_length - 1 then
                                state <= receiving;
                            end if;
                        end if;
                    when receiving =>
                        if out_last = "1" and out_valid = '1' and port_out_ready(port_out_ready'high) = '1' then
                            state <= init;
                        end if;
                end case;
            end if;
        end if;
    end process;

    request_id_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                req_id <= 0;
            else
                if state = init then
                    req_id <= 0;
                elsif state = requesting then
                    if (port_mem_out_ready = '1' or invalid_addr_valid = '1') and port_in_valid = '1' and port_in_baseaddr_valid = '1' then
                        if req_id < data_length - 1 then
                            req_id <= req_id + 1;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

    valid_buffer_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                data_buffer_valid <= (others => '0');
            else
                if write_enable = '1' then
                    data_buffer_valid(to_integer(unsigned(write_addr))) <= '1';
                end if;

                if state = receiving and out_last = "1" and out_valid = '1' and port_out_ready(port_out_ready'high) = '1' then
                    data_buffer_valid <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    send_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' or state = init then
                sending_entry <= 0;
            else
                if data_buffer_valid(sending_entry) = '1' and output_buffer_ready = '1' then
                    if sending_entry < data_length - 1 then
                        sending_entry <= sending_entry + 1;
                    else
                        sending_entry <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

    pipeline_counter: process(clk)
        variable pipeline_count_v: natural range 0 to (1 + 1) := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' or state = init then
                pipeline_count <= 0;
            else
                pipeline_count_v := pipeline_count;
                if data_buffer_valid(sending_entry) = '1' and output_buffer_ready = '1' then
                    pipeline_count_v := pipeline_count_v + 1;
                end if;
                if data_buffer_out_valid = '1' then
                    pipeline_count_v := pipeline_count_v - 1;
                end if;
                pipeline_count <= pipeline_count_v;
            end if;
        end if;
    end process;

    output_buffer_logic: process(clk)
        variable output_buffer_count_v: natural range 0 to (output_buffer_type'high + 2) := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' or state = init then
                output_buffer <= (others => (others => '0')); -- enfore the use of registers instead of ram blocks
                output_last_buffer <= (others => (others => '0')); -- enfore registers
                output_buffer_count <= 0;
                next_free_output_buffer_entry <= 0;
                first_used_output_buffer_entry <= 0;
            else
                output_buffer_count_v := output_buffer_count;
                if data_buffer_out_valid = '1' then
                    output_buffer(next_free_output_buffer_entry) <= ram_data_out;
                    output_last_buffer(next_free_output_buffer_entry) <= data_buffer_out_last;
                    output_buffer_count_v := output_buffer_count_v + 1;
                    if next_free_output_buffer_entry = output_buffer_index_type'high then
                        next_free_output_buffer_entry <= output_buffer_index_type'low;
                    else
                        next_free_output_buffer_entry <= next_free_output_buffer_entry + 1;
                    end if;
                end if;
                if out_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                    output_buffer_count_v := output_buffer_count_v - 1;
                    if first_used_output_buffer_entry = output_buffer_index_type'high then
                        first_used_output_buffer_entry <= output_buffer_index_type'low;
                    else
                        first_used_output_buffer_entry <= first_used_output_buffer_entry + 1;
                    end if;
                end if;
                output_buffer_count <= output_buffer_count_v;
            end if;
        end if;
    end process;

    pipeline_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' or state = init then
                data_buffer_out_last <= "0";
                data_buffer_out_valid <= '0';
            else
                data_buffer_out_valid <= '0';
                if data_buffer_valid(sending_entry) = '1' and output_buffer_ready = '1' then
                    data_buffer_out_valid <= '1';
                end if;

                data_buffer_out_last <= "0";
                if sending_entry = data_length - 1 then
                    data_buffer_out_last <= "1";
                end if;
            end if;
        end if;
    end process;

end behavioral;
