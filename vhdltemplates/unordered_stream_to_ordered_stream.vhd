library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity unordered_stream_to_ordered_stream is
    generic(
        stream_length: natural := 8
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_unordered_stream_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end unordered_stream_to_ordered_stream;

architecture behavioral of unordered_stream_to_ordered_stream is

    component ram_dp
        generic (
            data_width: natural := 32;
            addr_width: natural := 4;
            entries: natural := 8
        );
        port(
            clk: in std_logic;
            data_in: in std_logic_vector(data_width - 1 downto 0);
            write_addr: in std_logic_vector(addr_width - 1 downto 0);
            read_addr: in std_logic_vector(addr_width - 1 downto 0);
            we: in std_logic;
            data_out: out std_logic_vector(data_width - 1 downto 0)
        );
    end component;

    signal write_addr: std_logic_vector(port_in_data.idx'range) := (others => '0');
    signal read_addr: std_logic_vector(port_in_data.idx'range) := (others => '0');
    signal write_enable: std_logic := '0';

    signal data_valid: std_logic_vector(0 to stream_length - 1) := (others => '0');

    signal receiving_entry: natural range 0 to stream_length := 0;
    signal sending_entry: natural range 0 to stream_length - 1 := 0;
    
    signal out_valid: std_logic := '0';

begin

    inferred_ram: ram_dp
    generic map(
        data_width => port_out_data'length,
        addr_width => write_addr'length,
        entries => stream_length
    )
    port map(
        clk => clk,
        data_in => port_in_data.data,
        write_addr => write_addr,
        read_addr => read_addr,
        we => write_enable,
        data_out => port_out_data
    );

    write_addr <= port_in_data.idx;
    read_addr <= std_logic_vector(to_unsigned(sending_entry, read_addr'length));
    write_enable <= '1' when port_in_valid = '1' and receiving_entry < stream_length else '0';

    port_out_valid <= out_valid;

    port_in_ready(port_in_ready'low) <= '1' when receiving_entry < stream_length else '0';
    port_in_ready(port_in_ready'high) <= '1' when port_in_last = "1" and receiving_entry < stream_length else '0';

    port_out_last <= "1" when sending_entry = stream_length - 1 else "0";

    receive_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                receiving_entry <= 0;
                data_valid <= (others => '0');
            else
                -- receive data
                if port_in_valid = '1' and receiving_entry < stream_length then
                    receiving_entry <= receiving_entry + 1;
                    data_valid(to_integer(unsigned(port_in_data.idx))) <= '1';
                end if;
                -- all data has been send and must not be repeated
                if out_valid = '1' and port_out_ready(port_out_ready'high) = '1' then
                    if sending_entry >= stream_length - 1 then
                        receiving_entry <= 0;
                        data_valid <= (others => '0'); -- reset valid signals
                    end if;
                end if;
            end if;
        end if;
    end process;

    send_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                sending_entry <= 0;
                out_valid <= '0';
            else
                out_valid <= data_valid(sending_entry);
                if out_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                    out_valid <= '0';
                    if sending_entry < stream_length - 1 then
                        sending_entry <= sending_entry + 1;
                    else -- sending_entry >= stream_length - 1
                        sending_entry <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
