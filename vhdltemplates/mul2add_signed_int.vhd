library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity mul2add_int is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_tuple_tuple_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_mul2add_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end mul2add_int;

architecture behavioral of mul2add_int is

    -- keep the input registers the same as input size.
    -- avoids cases like signed 18x19 bit dot product inferring incorrectly as 27x27.
    signal mul_in1_1_reg1: std_logic_vector(port_in_data.t0.t0'range) := (others => '0');
    signal mul_in1_2_reg1: std_logic_vector(port_in_data.t0.t1'range) := (others => '0');
    signal mul_in2_1_reg1: std_logic_vector(port_in_data.t1.t0'range) := (others => '0');
    signal mul_in2_2_reg1: std_logic_vector(port_in_data.t1.t1'range) := (others => '0');
    signal mul_in_valid1: std_logic := '0';

    signal mul_in1_1_reg2: std_logic_vector(mul_in1_1_reg1'range) := (others => '0');
    signal mul_in1_2_reg2: std_logic_vector(mul_in1_2_reg1'range) := (others => '0');
    signal mul_in2_1_reg2: std_logic_vector(mul_in2_1_reg1'range) := (others => '0');
    signal mul_in2_2_reg2: std_logic_vector(mul_in2_2_reg1'range) := (others => '0');
    signal mul_in_valid2: std_logic := '0';

    signal mul_out1_reg: std_logic_vector(mul_in1_1_reg2'high + mul_in1_2_reg2'high + 1 downto 0) := (others => '0');
    signal mul_out2_reg: std_logic_vector(mul_in2_1_reg2'high + mul_in2_2_reg2'high + 1 downto 0) := (others => '0');
    signal mul_out_valid: std_logic := '0';

    signal acc_out_reg: std_logic_vector(port_out_data'range) := (others => '0');
    signal acc_out_valid: std_logic := '0';

    signal stall_regs: std_logic := '0';

    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');

begin

    in_ready <= "1" when stall_regs = '0' else "0";
    port_in_ready <= in_ready;

    port_out_data <= acc_out_reg;
    port_out_last <= (others => '0');
    port_out_valid <= acc_out_valid;

    stall_regs <= '1' when acc_out_valid = '1' and port_out_ready = "0" else '0';

    mul_logic: process(clk)
    begin
        -- XXX:
        -- Here we omit the DSP reset since resetting the handshaking signal is sufficient.
        -- IF they were used, they MUST be asynchronous!
        if rising_edge(clk) then
            if stall_regs = '0' then
                mul_in1_1_reg1 <= port_in_data.t0.t0;
                mul_in1_2_reg1 <= port_in_data.t0.t1;
                mul_in2_1_reg1 <= port_in_data.t1.t0;
                mul_in2_2_reg1 <= port_in_data.t1.t1;

                mul_in1_1_reg2 <= mul_in1_1_reg1;
                mul_in1_2_reg2 <= mul_in1_2_reg1;
                mul_in2_1_reg2 <= mul_in2_1_reg1;
                mul_in2_2_reg2 <= mul_in2_2_reg1;

                mul_out1_reg <= std_logic_vector(signed(mul_in1_1_reg2) * signed(mul_in1_2_reg2));
                mul_out2_reg <= std_logic_vector(signed(mul_in2_1_reg2) * signed(mul_in2_2_reg2));

                acc_out_reg <= std_logic_vector(resize(signed(mul_out1_reg), acc_out_reg'length) + resize(signed(mul_out2_reg), acc_out_reg'length));
            end if;
        end if;
    end process;

    pipeline_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                mul_in_valid1 <= '0';
                mul_in_valid2 <= '0';
                mul_out_valid <= '0';
                acc_out_valid <= '0';
            else
                if stall_regs = '0' then
                    mul_in_valid1 <= '0';
                    if port_in_valid = '1' and in_ready = "1" then
                        mul_in_valid1 <= '1';
                    end if;

                    mul_in_valid2 <= mul_in_valid1;
                    mul_out_valid <= mul_in_valid2;
                    acc_out_valid <= mul_out_valid;
                end if;
            end if;
        end if;
    end process;

end behavioral;
