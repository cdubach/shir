library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity ordered_stream_to_unordered_stream is
    generic(
        stream_length: natural := 8
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_unordered_stream_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end ordered_stream_to_unordered_stream;

architecture behavioral of ordered_stream_to_unordered_stream is
    signal counter: natural range 0 to stream_length - 1 := 0;
begin

    port_out_data.data <= port_in_data;
    port_out_data.idx <= std_logic_vector(to_unsigned(counter, port_out_data.idx'length));
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

    counter_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter <= 0;
            else
                if port_in_valid = '1' and port_out_ready(port_out_ready'high - 1) = '1' then
                    if counter < stream_length - 1 then
                        counter <= counter + 1;
                    else
                        counter <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
