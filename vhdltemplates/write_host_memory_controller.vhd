library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity write_host_memory_controller is
    generic(
        req_buffer_size: natural := 64
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        -- this controller must stall until the entire memory is ready to be accessed
        port_in_mem_ready: in std_logic;
        -- request to mem controller
        port_out_req_address: out mem_address_type;
        port_out_req_mdata: out req_id_type;
        port_out_req_data: out cache_line_type;
        port_out_req_valid: out std_logic;

        port_out_req_total: out std_logic_vector(63 downto 0);
        port_out_req_pending: out std_logic_vector(63 downto 0);
        -- request full
        port_in_req_almFull: in std_logic;

        -- response from mem controller
        port_in_rsp_mdata: in req_id_type;
        port_in_rsp_valid: in std_logic;

        -- client requests
        port_in_data: in request_cache_line_with_address_type;
        port_in_valid: in std_logic;
        port_in_ready: out std_logic;
        -- client response
        port_out_data: out request_cache_line_type;
        port_out_valid: out std_logic
    );
end write_host_memory_controller;

architecture behavioral of write_host_memory_controller is

    signal req_pending: natural range 0 to req_buffer_size := 0;
    signal req_total: std_logic_vector(63 downto 0);

    signal mem_ctrl_ready: std_logic := '0';

    constant alm_full_valid_cycles: natural := 7;
    signal alm_full_valid_count: natural range 0 to 7 := 0;
    signal req_valid: std_logic := '0';

begin

    port_out_req_address <= port_in_data.data.addr;
    port_out_req_mdata <= port_in_data.req_id;
    port_out_req_data <= port_in_data.data.data;
    req_valid <= port_in_valid when mem_ctrl_ready = '1' else '0'; -- do not send requests during reset / when memory is not ready
    port_out_req_valid <= req_valid;
    port_out_req_total <= req_total;
    port_out_req_pending <= std_logic_vector(to_unsigned(req_pending, port_out_req_pending'length));

    mem_ctrl_ready <= port_in_mem_ready when reset = '0' and req_pending < req_buffer_size and (port_in_req_almFull = '0' or alm_full_valid_count < alm_full_valid_cycles) else '0';
    port_in_ready <= mem_ctrl_ready;
    
    port_out_data.req_id <= port_in_rsp_mdata;
    port_out_valid <= port_in_rsp_valid;

    req_counter: process(clk)
        variable req_pending_v: natural range 0 to req_buffer_size := 0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                req_total <= (others => '0');
                req_pending <= 0;
            else
                req_pending_v := req_pending;
                if port_in_valid = '1' and mem_ctrl_ready = '1' then
                    req_pending_v := req_pending_v + 1;
                    req_total <= std_logic_vector(to_unsigned(to_integer(unsigned(req_total)) + 1, req_total'length));
                end if;
                if port_in_rsp_valid = '1' then
                    req_pending_v := req_pending_v - 1;
                end if;
                req_pending <= req_pending_v;
            end if;
        end if;
    end process;

    alm_full_counter: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                alm_full_valid_count <= 0;
            else
                if port_in_req_almFull = '0' then
                    alm_full_valid_count <= 0;
                else
                    if alm_full_valid_count < alm_full_valid_cycles and req_valid = '1' then 
                        alm_full_valid_count <= alm_full_valid_count + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
