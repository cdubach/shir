use work.common.all;

entity slide_vector is
    generic(
        step_size: natural := 1
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_vector_type;
        port_in_valid: in std_logic;
        port_in_ready: out std_logic;
        port_out_data: out data_word_vector_slice_type;
        port_out_valid: out std_logic;
        port_out_ready: in std_logic
    );
end slide_vector;

architecture behavioral of drop_vector is
begin

    data_signal: process(port_in_data)
    begin
        for i in port_out_data'low to port_out_data'high loop
            for j in port_out_data(0)'low to port_out_data(0)'high loop
                port_out_data(i)(j) <= port_in_data(i * step_size + j);
            end loop;
        end loop;
    end process;

    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

end behavioral;