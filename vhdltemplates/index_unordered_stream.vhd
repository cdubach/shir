library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity index_unordered_stream is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_unordered_stream_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_tuple_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end index_unordered_stream;

architecture behavioral of index_unordered_stream is

begin

    port_out_data.t0 <= port_in_data.data;
    port_out_data.t1 <= port_in_data.idx;
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

end behavioral;
