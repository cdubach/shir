library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity concat_ordered_stream is
    generic(
        concat_dimension: natural := 0
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_1_data: in data_word_type;
        port_in_1_last: in last_1_type;
        port_in_1_valid: in std_logic;
        port_in_1_ready: out ready_1_type;
        port_in_2_data: in data_word_type;
        port_in_2_last: in last_1_type;
        port_in_2_valid: in std_logic;
        port_in_2_ready: out ready_1_type;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end concat_ordered_stream;

architecture behavioral of concat_ordered_stream is
    signal stream_select: std_logic := '0';
    signal out_valid: std_logic := '0';
    --constant last1_all_ones: std_logic_vector(port_in_1_last'range) := (others => '1');
begin

    port_out_data <= port_in_1_data when stream_select = '0' else port_in_2_data;
    port_out_valid <= out_valid;
    out_valid <= port_in_1_valid when stream_select = '0' else port_in_2_valid;

    ready1_signal: process(stream_select, port_in_1_last, port_in_1_valid, port_out_ready)
        constant last1_all_ones: std_logic_vector(port_in_1_last'range) := (others => '1');
    begin 
        if stream_select = '0' then
            port_in_1_ready((port_in_1_ready'high - concat_dimension - 1) downto port_in_1_ready'low) <= port_out_ready((port_in_1_ready'high - concat_dimension - 1) downto port_in_1_ready'low);
        else
            port_in_1_ready((port_in_1_ready'high - concat_dimension - 1) downto port_in_1_ready'low) <= (others => '0');
        end if;
    
        for i in 0 to concat_dimension loop
            if stream_select = '0' and 
            port_in_1_last(port_in_1_last'high - i downto port_in_1_last'low) = last1_all_ones(port_in_1_last'high - i downto port_in_1_last'low) and 
            port_in_1_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                port_in_1_ready(port_in_1_ready'high - i) <= '1';
            else
                port_in_1_ready(port_in_1_ready'high - i) <= '0';
            end if;
        end loop;
    end process;            

    port_in_2_ready <= port_out_ready when stream_select = '1' else (others => '0');

    last_signal: process(stream_select, port_in_1_last, port_in_2_last)
    begin 
        if stream_select = '0' then
            port_out_last <= port_in_1_last;
            for i in port_out_last'high - concat_dimension to port_out_last'high loop
                port_out_last(i) <= '0'; -- output is never last, when we are still forwarding the first stream
            end loop;
        else
            port_out_last <= port_in_2_last;
        end if;
    end process;

    select_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                stream_select <= '0';
            else
                if out_valid = '1' and port_out_ready(port_out_ready'high - concat_dimension - 1) = '1' then
                    if stream_select = '0' and port_in_1_last(port_in_1_last'high - concat_dimension) = '1' then
                        stream_select <= '1';
                    elsif stream_select = '1' and port_in_2_last(port_in_2_last'high - concat_dimension) = '1' then
                        stream_select <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
