library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity on_board_ram_buffer is
	port(
		clk: in std_logic;
		reset: in std_logic;

		bank_waitrequest: in std_logic;
		bank_read_data: in std_logic_vector(LOCAL_MEM_DATA_WIDTH-1 downto 0);
		bank_read_valid: in std_logic;

		bank_burstcount: out std_logic_vector(LOCAL_MEM_BURST_CNT_WIDTH-1 downto 0);
		bank_write_data: out std_logic_vector(LOCAL_MEM_DATA_WIDTH-1 downto 0);
		bank_address: out std_logic_vector(LOCAL_MEM_ADDR_WIDTH-1 downto 0);
		bank_write_enable: out std_logic;
		bank_read_enable: out std_logic;
		bank_byteenable: out std_logic_vector(LOCAL_MEM_DATA_N_BYTES-1 downto 0);

		port_in_data: in mixed_cache_line_type;
		port_in_valid: in std_logic;        
		port_in_ready: out std_logic;

		port_out_data: out request_cache_line_type;
		port_out_valid: out std_logic;

		-- Just for DEBUG
		port_out_write_req_count: out std_logic_vector(63 downto 0);
		port_out_write_rsp_count: out std_logic_vector(63 downto 0);
		port_out_read_req_count: out std_logic_vector(63 downto 0);
		port_out_read_rsp_count: out std_logic_vector(63 downto 0);
		port_out_obr_write_req_count: out std_logic_vector(63 downto 0);
		port_out_obr_read_req_count: out std_logic_vector(63 downto 0);
		port_out_obr_read_rsp_count: out std_logic_vector(63 downto 0);
		port_out_write_req_id_req_sum: out std_logic_vector(63 downto 0);
		port_out_write_req_id_rsp_sum: out std_logic_vector(63 downto 0);
		port_out_read_req_id_req_sum: out std_logic_vector(63 downto 0);
		port_out_read_req_id_rsp_sum: out std_logic_vector(63 downto 0);
		port_out_client_read_req_count: out std_logic_vector(63 downto 0);
		port_out_idx_buffer_count: out std_logic_vector(63 downto 0);
		port_out_information: out std_logic_vector(4 downto 0)
	);
end on_board_ram_buffer;

architecture behavioral of on_board_ram_buffer is

	constant LOCAL_MEM_DATA_WIDTH: natural := 512;
	constant LOCAL_MEM_ADDR_WIDTH: natural := 27;
	constant LOCAL_MEM_BURST_CNT_WIDTH: natural := 7;
	constant LOCAL_MEM_DATA_N_BYTES: natural := (LOCAL_MEM_DATA_WIDTH + 8 - 1)/8;
	constant FIFO_MAX_DEPTH: natural := 64;
	constant FIFO_ADDR_WIDTH: natural := positive(ceil(log2(real(FIFO_MAX_DEPTH))));
	constant read_req_id_buffer_allowance: natural := 8;

	type ram_control_signal_type is record
		write_addr : std_logic_vector(FIFO_ADDR_WIDTH-1 downto 0);
		write_ptr : natural;
		read_addr : std_logic_vector(FIFO_ADDR_WIDTH-1 downto 0);
		read_ptr  : natural;
		we : std_logic;
		num_elems : natural;
	end record ram_control_signal_type;

	type ram_idx_entry_type is record
		input_data : std_logic_vector(port_in_data.req_id'range);
		output_data : std_logic_vector(port_out_data.req_id'range);
		ctrl_sigs : ram_control_signal_type;		
	end record ram_idx_entry_type;

	type ram_data_entry_type is record
		input_data : std_logic_vector(bank_read_data'range);
		output_data : std_logic_vector(port_out_data.data'range);
		ctrl_sigs : ram_control_signal_type;
	end record ram_data_entry_type;

	component ram_dp
	generic (
		data_width: natural := 512;
		addr_width: natural := 6;
		entries: natural := 64
	);
	port(
		clk: in std_logic;
		data_in: in std_logic_vector(data_width - 1 downto 0);
		write_addr: in std_logic_vector(addr_width - 1 downto 0);
		read_addr: in std_logic_vector(addr_width - 1 downto 0);
		we: in std_logic;
		data_out: out std_logic_vector(data_width - 1 downto 0)
	);
	end component;

	signal is_bank_ready: std_logic;
	signal ram_data_entry: ram_data_entry_type;
	signal ram_idx_entry: ram_idx_entry_type;
	signal output_data_pop: std_logic;
	signal is_read_rsp: std_logic;
	signal is_write_rsp: std_logic;
	signal is_buffer_ready_reqs: std_logic;
	signal is_buffer_ready_rsps: std_logic;

	-- Temporary signal so that I can read back the value of bank_write_enable
	signal temp_out_write_enable: std_logic;

	-- Psuedo Signal
	signal bank_req_id: std_logic_vector(port_in_data.req_id'range);
	signal write_rsp_req_id: std_logic_vector(port_in_data.req_id'range);

	-- Temp Signals for debug
	signal temp_in_ready: std_logic;
	signal temp_out_read_enable: std_logic;
	signal temp_out_req_id: std_logic_vector(port_out_data.req_id'range);

	signal write_req_count: natural;
	signal write_rsp_count: natural;
	signal read_req_count: natural;
	signal read_rsp_count: natural;
	signal obr_write_req_count: natural;
	signal obr_read_req_count: natural;
	signal obr_read_rsp_count: natural;
	signal write_req_id_req_sum: natural;
	signal write_req_id_rsp_sum: natural;
	signal read_req_id_req_sum: natural;
	signal read_req_id_rsp_sum: natural;
	signal client_read_req_count: natural;

begin

	-- DEBUG Processes
	
	port_out_write_req_count <= std_logic_vector(to_unsigned(write_req_count, 64));
	port_out_write_rsp_count <= std_logic_vector(to_unsigned(write_rsp_count, 64));
	port_out_read_req_count <= std_logic_vector(to_unsigned(read_req_count, 64));
	port_out_read_rsp_count <= std_logic_vector(to_unsigned(read_rsp_count, 64));
	port_out_obr_write_req_count <= std_logic_vector(to_unsigned(obr_write_req_count, 64));
	port_out_obr_read_req_count <= std_logic_vector(to_unsigned(obr_read_req_count, 64));
	port_out_obr_read_rsp_count <= std_logic_vector(to_unsigned(obr_read_rsp_count, 64));
	port_out_write_req_id_req_sum <= std_logic_vector(to_unsigned(write_req_id_req_sum, 64));
	port_out_write_req_id_rsp_sum <= std_logic_vector(to_unsigned(write_req_id_rsp_sum, 64));
	port_out_read_req_id_req_sum <= std_logic_vector(to_unsigned(read_req_id_req_sum, 64));
	port_out_read_req_id_rsp_sum <= std_logic_vector(to_unsigned(read_req_id_rsp_sum, 64));
	port_out_client_read_req_count <= std_logic_vector(to_unsigned(client_read_req_count, 64));
	port_out_idx_buffer_count <= std_logic_vector(to_unsigned(ram_idx_entry.ctrl_sigs.num_elems, 64));

	information_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				port_out_information <= (others => '0');
			else
				port_out_information <= (	0 => port_in_valid,
											1 => port_in_data.we,
											2 => temp_in_ready,
											3 => is_bank_ready,
											4 => is_buffer_ready_reqs);
			end if;
		end if;
	end process;

	write_req_count_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				write_req_count <= 0;
				write_req_id_req_sum <= 0;
			else
				if port_in_valid = '1' and port_in_data.we = '1' and temp_in_ready = '1' then
					write_req_count <= write_req_count + 1;
					write_req_id_req_sum <= write_req_id_req_sum + to_integer(unsigned(port_in_data.req_id));
				end if;
			end if;
		end if;
	end process;

	write_rsp_count_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				write_rsp_count <= 0;
				write_req_id_rsp_sum <= 0;
			else
				if is_write_rsp = '1' then
					write_rsp_count <= write_rsp_count + 1;
					write_req_id_rsp_sum <= write_req_id_rsp_sum + to_integer(unsigned(temp_out_req_id));
				end if;
			end if;
		end if;
	end process;

	read_req_count_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				read_req_count <= 0;
				read_req_id_req_sum <= 0;
			else
				if port_in_valid = '1' and port_in_data.we = '0' and temp_in_ready = '1' then
					read_req_count <= read_req_count + 1;
					read_req_id_req_sum <= read_req_id_req_sum + to_integer(unsigned(port_in_data.req_id));
				end if;
			end if;
		end if;
	end process;

	client_read_req_count_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				client_read_req_count <= 0;
			else
				if port_in_valid = '1' and port_in_data.we = '0' then
					client_read_req_count <= client_read_req_count + 1;
				end if;
			end if;
		end if;
	end process;

	read_rsp_count_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				read_rsp_count <= 0;
				read_req_id_rsp_sum <= 0;
			else
				if is_read_rsp = '1' then
					read_rsp_count <= read_rsp_count + 1;
					read_req_id_rsp_sum <= read_req_id_rsp_sum + to_integer(unsigned(temp_out_req_id));
				end if;
			end if;
		end if;
	end process;

	obr_write_req_count_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				obr_write_req_count <= 0;
			else
				if temp_out_write_enable = '1' and is_bank_ready = '1' then
					obr_write_req_count <= obr_write_req_count + 1;
				end if;
			end if;
		end if;
	end process;

	obr_read_req_count_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				obr_read_req_count <= 0;
			else
				if temp_out_read_enable = '1' and is_bank_ready = '1' then
					obr_read_req_count <= obr_read_req_count + 1;
				end if;
			end if;
		end if;
	end process;

	obr_read_rsp_count_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				obr_read_rsp_count <= 0;
			else
				if bank_read_valid = '1' then
					obr_read_rsp_count <= obr_read_rsp_count + 1;
				end if;
			end if;
		end if;
	end process;

	-- We need a burstcount of 1 and all bytes in data path must be enabled
	bank_burstcount <= std_logic_vector(to_unsigned(1, LOCAL_MEM_BURST_CNT_WIDTH));
	bank_byteenable <= (others => '1');

	-- Send temporary write signal to actual write signal
	bank_write_enable <= temp_out_write_enable;
	bank_read_enable <= temp_out_read_enable;		-- Temp TODO to remove

	-- Req ID Buffer is limiting for requests since, N(Req ID Buffer) >= N(Resp Data Buffer) always
	-- Data Buffer is limiting for responses
	is_bank_ready <= not bank_waitrequest when reset = '0' else '0';
	is_buffer_ready_reqs <= '1' when (ram_idx_entry.ctrl_sigs.num_elems + read_req_id_buffer_allowance <= FIFO_MAX_DEPTH) and reset = '0' else '0';
	is_buffer_ready_rsps <= '1' when (ram_data_entry.ctrl_sigs.num_elems > 0) else '0';

	-- This module is ready to accept requests when both bank and internal buffer are ready to accept new requests
	temp_in_ready <= is_bank_ready and is_buffer_ready_reqs;
	port_in_ready <= temp_in_ready;					-- Temp TODO to remove

	-- FIFO Block for incoming data from On Board RAM
	inferred_data_ram: ram_dp
	generic map(
		data_width => port_out_data.data'length,
		addr_width => FIFO_ADDR_WIDTH,
		entries => FIFO_MAX_DEPTH
	)
	port map(
		clk => clk,
		data_in => ram_data_entry.input_data,
		write_addr => ram_data_entry.ctrl_sigs.write_addr,
		read_addr => ram_data_entry.ctrl_sigs.read_addr,
		we => ram_data_entry.ctrl_sigs.we,
		data_out => ram_data_entry.output_data
	);

	-- Store data in buffer as soon as data comes from OBR
	ram_data_entry.input_data <= bank_read_data;
	ram_data_entry.ctrl_sigs.write_addr <= std_logic_vector(to_unsigned(ram_data_entry.ctrl_sigs.write_ptr, FIFO_ADDR_WIDTH));
	ram_data_entry.ctrl_sigs.read_addr <= std_logic_vector(to_unsigned(ram_data_entry.ctrl_sigs.read_ptr, FIFO_ADDR_WIDTH));
	ram_data_entry.ctrl_sigs.we <= bank_read_valid;

	-- Logic for updating Write Pointer, update if a valid write is taking place
	data_ram_write_ptr_update: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				ram_data_entry.ctrl_sigs.write_ptr <= 0;
			else
				if ram_data_entry.ctrl_sigs.we = '1' then
					if ram_data_entry.ctrl_sigs.write_ptr < (FIFO_MAX_DEPTH-1) then
						ram_data_entry.ctrl_sigs.write_ptr <= ram_data_entry.ctrl_sigs.write_ptr + 1;
					else
						ram_data_entry.ctrl_sigs.write_ptr <= 0;
					end if;
				end if;
			end if;
		end if;
	end process;

	-- Logic for updating Read Pointer
	-- output_data_pop keeps track whether pop will take place or not
	data_ram_read_ptr_update: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				ram_data_entry.ctrl_sigs.read_ptr <= 0;
			else
				if output_data_pop = '1' then
					if ram_data_entry.ctrl_sigs.read_ptr < (FIFO_MAX_DEPTH-1) then
						ram_data_entry.ctrl_sigs.read_ptr <= ram_data_entry.ctrl_sigs.read_ptr + 1;
					else
						ram_data_entry.ctrl_sigs.read_ptr <= 0;
					end if;
				end if;
			end if;
		end if;
	end process;

	-- Logic for updating the number of elements
	data_ram_num_elems_update: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				ram_data_entry.ctrl_sigs.num_elems <= 0;
			else
				if ram_data_entry.ctrl_sigs.we = '1' and output_data_pop = '0' then
					ram_data_entry.ctrl_sigs.num_elems <= ram_data_entry.ctrl_sigs.num_elems + 1;
				elsif ram_data_entry.ctrl_sigs.we = '0' and output_data_pop = '1' then
					ram_data_entry.ctrl_sigs.num_elems <= ram_data_entry.ctrl_sigs.num_elems - 1;
				end if;
			end if;
		end if;
	end process;

	-- FIFO Block for storing req IDs from incoming read requests
	inferred_idx_ram: ram_dp
	generic map(
		data_width => port_out_data.req_id'length,
		addr_width => FIFO_ADDR_WIDTH,
		entries => FIFO_MAX_DEPTH
	)
	port map(
		clk => clk,
		data_in => ram_idx_entry.input_data,
		write_addr => ram_idx_entry.ctrl_sigs.write_addr,
		read_addr => ram_idx_entry.ctrl_sigs.read_addr,
		we => ram_idx_entry.ctrl_sigs.we,
		data_out => ram_idx_entry.output_data
	);

	-- Store ID in buffer when valid request AND buffer is ready to accept requests
	ram_idx_entry.input_data <= port_in_data.req_id;
	ram_idx_entry.ctrl_sigs.write_addr <= std_logic_vector(to_unsigned(ram_idx_entry.ctrl_sigs.write_ptr, FIFO_ADDR_WIDTH));
	ram_idx_entry.ctrl_sigs.read_addr <= std_logic_vector(to_unsigned(ram_idx_entry.ctrl_sigs.read_ptr, FIFO_ADDR_WIDTH));
	ram_idx_entry.ctrl_sigs.we <= '1' when port_in_valid = '1' and port_in_data.we = '0' and is_bank_ready = '1' and is_buffer_ready_reqs = '1' else '0';

	-- Logic for updating Write Pointer, update if a valid write is taking place
	idx_ram_write_ptr_update: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				ram_idx_entry.ctrl_sigs.write_ptr <= 0;
			else
				if ram_idx_entry.ctrl_sigs.we = '1' then
					if ram_idx_entry.ctrl_sigs.write_ptr < (FIFO_MAX_DEPTH-1) then
						ram_idx_entry.ctrl_sigs.write_ptr <= ram_idx_entry.ctrl_sigs.write_ptr + 1;
					else
						ram_idx_entry.ctrl_sigs.write_ptr <= 0;
					end if;
				end if;
			end if;
		end if;
	end process;

	-- Logic for updating Read Pointer
	-- output_data_pop keeps track whether pop will take place or not
	idx_ram_read_ptr_update: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				ram_idx_entry.ctrl_sigs.read_ptr <= 0;
			else
				if output_data_pop = '1' then
					if ram_idx_entry.ctrl_sigs.read_ptr < (FIFO_MAX_DEPTH-1) then
						ram_idx_entry.ctrl_sigs.read_ptr <= ram_idx_entry.ctrl_sigs.read_ptr + 1;
					else
						ram_idx_entry.ctrl_sigs.read_ptr <= 0;
					end if;
				end if;
			end if;
		end if;
	end process;

	-- Logic for updating the number of elements
	idx_ram_num_elems_update: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				ram_idx_entry.ctrl_sigs.num_elems <= 0;
			else
				if ram_idx_entry.ctrl_sigs.we = '1' and output_data_pop = '0' then
					ram_idx_entry.ctrl_sigs.num_elems <= ram_idx_entry.ctrl_sigs.num_elems + 1;
				elsif ram_idx_entry.ctrl_sigs.we = '0' and output_data_pop = '1' then
					ram_idx_entry.ctrl_sigs.num_elems <= ram_idx_entry.ctrl_sigs.num_elems - 1;
				end if;
			end if;
		end if;
	end process;

	-- Main Logic for generating the output_data_pop signal
	-- 1) Both Data and IDX Buffers are non-empty
	-- 2) Write response does not need to be sent out in the next clock cycle

	output_data_pop <= '1' when is_buffer_ready_rsps = '1' and not(temp_out_write_enable = '1' and is_bank_ready = '1') else '0';

	-- Control Signals going to On Board RAM (OBR)

	-- Change when valid write request can be accepted by OBR
	bank_write_data_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				bank_write_data <= (others => '0');
			else
				if port_in_valid = '1' and port_in_data.we = '1' and is_bank_ready = '1' then
					bank_write_data <= port_in_data.data;
				end if;
			end if;
		end if;
	end process;

	-- Process to update the pseudo signal going to OBR
	bank_req_id_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				bank_req_id <= (others => '0');
			else
				if port_in_valid = '1' and port_in_data.we = '1' and is_bank_ready = '1' then
					bank_req_id <= port_in_data.req_id;
				end if;
			end if;
		end if;
	end process;

	-- Change when valid read/write request can be accepted by OBR
	bank_address_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				bank_address <= (others => '0');
			else
				if port_in_valid = '1' and is_bank_ready = '1' then
					bank_address <= port_in_data.addr;
				end if;
			end if;
		end if;
	end process;

	-- Set to 1 when valid write request can be accepted by OBR
	-- Set to 0 if internal buffers are full, so writes will keep on being sent although output ready is low
	-- DO NOT change value if bank is not ready (takes priority over everything else!!!)
	bank_write_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				temp_out_write_enable <= '0';
			else
				if is_buffer_ready_reqs = '0' and is_bank_ready = '1' then
					temp_out_write_enable <= '0';
				elsif port_in_valid = '1' and port_in_data.we = '1' and is_bank_ready = '1' then
					temp_out_write_enable <= '1';
				elsif not(port_in_valid = '1' and port_in_data.we = '1') and is_bank_ready = '1' then
					temp_out_write_enable <= '0';
				end if;
			end if;
		end if;
	end process;

	-- Set to 1 when valid read request can be accepted by OBR
	-- Set to 0 if internal buffers are full, so reads will keep on being sent although output ready is low
	-- DO NOT change value if bank is not ready
	bank_read_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				temp_out_read_enable <= '0';
			else
				if is_buffer_ready_reqs = '0' and is_bank_ready = '1' then
					temp_out_read_enable <= '0';
				elsif port_in_valid = '1' and port_in_data.we = '0' and is_bank_ready = '1' then
					temp_out_read_enable <= '1';
				elsif not(port_in_valid = '1' and port_in_data.we = '0') and is_bank_ready = '1' then
					temp_out_read_enable <= '0';
				end if;
			end if;
		end if;
	end process;

	port_out_data.data <= ram_data_entry.output_data;

	temp_out_req_id <= ram_idx_entry.output_data when is_read_rsp = '1' else
							write_rsp_req_id when is_write_rsp = '1' else
							(others => '0');

	port_out_data.req_id <= temp_out_req_id;		-- temp TODO To remove

	-- Earlier logic will ensure that write responses and read responses will be never be sent out together
	out_valid_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				port_out_valid <= '0';
			else
				if temp_out_write_enable = '1' and is_bank_ready = '1' then
					port_out_valid <= '1';
				elsif output_data_pop = '1' then
					port_out_valid <= '1';
				else
					port_out_valid <= '0';
				end if;
			end if;
		end if;
	end process;

	-- Logic to track if read_rsp or write_rsp is being sent out
	is_read_rsp_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				is_read_rsp <= '0';
			else
				if output_data_pop = '1' then
					is_read_rsp <= '1';
				else
					is_read_rsp <= '0';
				end if;
			end if;
		end if;
	end process;

	is_write_rsp_proc: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				is_write_rsp <= '0';
			else
				if temp_out_write_enable = '1' and is_bank_ready = '1' then
					is_write_rsp <= '1';
				else
					is_write_rsp <= '0';
				end if;
			end if;
		end if;
	end process;

	-- The pseudo bank_req_id must be buffered by one cycle before being sent back
	extra_buf: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				write_rsp_req_id <= (others => '0');
			else
				write_rsp_req_id <= bank_req_id;
			end if;
		end if;
	end process;

end behavioral;