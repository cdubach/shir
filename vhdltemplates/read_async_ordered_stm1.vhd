library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity read_async is
    generic(
        data_length: natural := 8
    );
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- to controller
        port_mem_out_data: out request_mem_address_type;
        port_mem_out_valid: out std_logic;
        port_mem_out_ready: in std_logic;
        -- from controller
        port_mem_in_data: in request_cache_line_type;
        port_mem_in_valid: in std_logic;
        -- stream of addresses
        port_in_data: in data_word_stream_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        -- base address (added to address)
        port_in_baseaddr_data: in base_address_type;
        port_in_baseaddr_last: in last_0_type;
        port_in_baseaddr_valid: in std_logic;
        port_in_baseaddr_ready: out ready_0_type;

        -- outgoing ordered stream of tuple of data and address
        port_out_data: out data_word_stream_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end read_async;

--
-- this is a special case for reading length-1 streams, which may happen if
-- the data is small. also has to do with how Quartus implements the depth-1
-- ram_dp and the extra buffered read queue.
--
-- The implementation is more-or-less the same as the original (more general)
-- read_async_ordered.vhd except:
-- *  we do NOT have a read queue (seems excessive for a length-1 stream)
-- *  the FSM directly encodes the waiting of data transmission
--
architecture behavioral of read_async is
    signal reg: std_logic_vector(port_out_data'range) := (others => '0');

    type state_type is (init, requesting, receiving, waiting);
    signal state: state_type := init;
begin

    port_mem_out_data.data <= std_logic_vector(to_unsigned(
                              to_integer(unsigned(port_in_baseaddr_data)) +
                              to_integer(unsigned(port_in_data)),
                              port_mem_out_data.data'length)) when state = requesting else (others => '0');
    port_mem_out_data.req_id <= (others => '0');
    port_mem_out_valid <= '1' when state = requesting and port_in_valid = '1' and port_in_baseaddr_valid = '1' else '0';
    port_in_ready <= port_in_last(port_in_last'high) & '1' when state = requesting and port_mem_out_ready = '1' else (others => '0');

    port_in_baseaddr_ready <= "1" when port_in_last(port_in_last'high) = '1' and state = requesting and port_mem_out_ready = '1' else (others => '0');

    port_out_data <= reg;
    port_out_last <= (others => '1');
    port_out_valid <= '1' when state = waiting else '0';

    process(clk)
    begin
        if rising_edge(clk) then
            if port_mem_in_valid = '1' then
                reg <= port_mem_in_data.data;
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= init;
            else
                case state is
                    when init =>
                        if port_in_valid = '1' and port_in_baseaddr_valid = '1' then
                            state <= requesting;
                        end if;
                    when requesting =>
                        if port_mem_out_ready = '1' and port_in_valid = '1' and port_in_baseaddr_valid = '1' then
                            state <= receiving;
                        end if;
                    when receiving =>
                        if port_mem_in_valid = '1' then
                            state <= waiting;
                        end if;
                    when waiting =>
                        if port_out_ready(port_out_ready'low) = '1' then
                            state <= init;
                        end if;
                end case;
            end if;
        end if;
    end process;

end behavioral;

