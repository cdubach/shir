library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity ifelse is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_tuple_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end ifelse;

architecture behavioral of ifelse is
    component id is -- %TESTING_ONLY
        port( -- %TESTING_ONLY
            clk: in std_logic; -- %TESTING_ONLY
            reset: in std_logic; -- %TESTING_ONLY
            port_in_data: in data_word_type; -- %TESTING_ONLY
            port_in_last: in last_1_type; -- %TESTING_ONLY
            port_in_valid: in std_logic; -- %TESTING_ONLY
            port_in_ready: out ready_1_type; -- %TESTING_ONLY
            port_out_data: out data_word_type; -- %TESTING_ONLY
            port_out_last: out last_1_type; -- %TESTING_ONLY
            port_out_valid: out std_logic; -- %TESTING_ONLY
            port_out_ready: in ready_1_type -- %TESTING_ONLY
        ); -- %TESTING_ONLY
    end component; -- %TESTING_ONLY

    signal port_to_f1_data: data_word_type; -- %TESTING_ONLY
    signal port_to_f1_last: std_logic_vector(port_in_last'range) := (others => '0'); -- %TESTING_ONLY
    signal port_to_f1_valid: std_logic := '0'; -- %TESTING_ONLY
    signal port_to_f1_ready: std_logic_vector(port_in_ready'range) := (others => '0'); -- %TESTING_ONLY

    signal port_from_f1_data: data_word_type; -- %TESTING_ONLY
    signal port_from_f1_last: std_logic_vector(port_out_last'range) := (others => '0'); -- %TESTING_ONLY
    signal port_from_f1_valid: std_logic := '0'; -- %TESTING_ONLY
    signal port_from_f1_ready: std_logic_vector(port_out_ready'range) := (others => '0'); -- %TESTING_ONLY

    signal port_to_f2_data: data_word_type; -- %TESTING_ONLY
    signal port_to_f2_last: std_logic_vector(port_in_last'range) := (others => '0'); -- %TESTING_ONLY
    signal port_to_f2_valid: std_logic := '0'; -- %TESTING_ONLY
    signal port_to_f2_ready: std_logic_vector(port_in_ready'range) := (others => '0'); -- %TESTING_ONLY

    signal port_from_f2_data: data_word_type; -- %TESTING_ONLY
    signal port_from_f2_last: std_logic_vector(port_out_last'range) := (others => '0'); -- %TESTING_ONLY
    signal port_from_f2_valid: std_logic := '0'; -- %TESTING_ONLY
    signal port_from_f2_ready: std_logic_vector(port_out_ready'range) := (others => '0'); -- %TESTING_ONLY

    type state_type is (select_idle, select_f1, select_f2);
    signal state: state_type := select_idle;

begin

    f1: id port map( -- %TESTING_ONLY
        clk => clk,  -- %TESTING_ONLY
        reset => reset,  -- %TESTING_ONLY
        port_in_data =>   port_to_f1_data,  -- %TESTING_ONLY
        port_in_last =>   port_to_f1_last,  -- %TESTING_ONLY
        port_in_valid =>  port_to_f1_valid,  -- %TESTING_ONLY
        port_in_ready =>  port_to_f1_ready,  -- %TESTING_ONLY
        port_out_data =>  port_from_f1_data,  -- %TESTING_ONLY
        port_out_last =>  port_from_f1_last,  -- %TESTING_ONLY
        port_out_valid => port_from_f1_valid,  -- %TESTING_ONLY
        port_out_ready => port_from_f1_ready -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    f2: id port map( -- %TESTING_ONLY
        clk => clk,  -- %TESTING_ONLY
        reset => reset,  -- %TESTING_ONLY
        port_in_data =>   port_to_f2_data,  -- %TESTING_ONLY
        port_in_last =>   port_to_f2_last,  -- %TESTING_ONLY
        port_in_valid =>  port_to_f2_valid,  -- %TESTING_ONLY
        port_in_ready =>  port_to_f2_ready,  -- %TESTING_ONLY
        port_out_data =>  port_from_f2_data,  -- %TESTING_ONLY
        port_out_last =>  port_from_f2_last,  -- %TESTING_ONLY
        port_out_valid => port_from_f2_valid,  -- %TESTING_ONLY
        port_out_ready => port_from_f2_ready -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    port_to_f1_data <= port_in_data.t0;
    port_to_f1_last <= port_in_last;
    port_to_f1_valid <= port_in_valid when state = select_f1 else '0';
    port_to_f2_data <= port_in_data.t0;
    port_to_f2_last <= port_in_last;
    port_to_f2_valid <= port_in_valid when state = select_f2 else '0';

    port_in_ready <= port_to_f1_ready when state = select_f1 else port_to_f2_ready when state = select_f2 else (others => '0');

    state_logic: process(clk)
        constant out_last_all_one: std_logic_vector(port_out_last'range) := (others => '1');
        constant out_ready_all_one: std_logic_vector(port_out_ready'range) := (others => '1');
        constant in_last_all_one: std_logic_vector(port_in_last'range) := (others => '1');
        constant in_ready_all_one: std_logic_vector(port_in_ready'range) := (others => '1');
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= select_idle;
            else
                if state = select_idle then
                    if port_in_valid = '1' then
                        if port_in_data.t1(0) = '0' then
                            state <= select_f1;
                        else
                            state <= select_f2;
                        end if;
                    end if;
                else
                    if port_out_ready = out_ready_all_one then
                        if state = select_f1 then
                            if port_from_f1_last = out_last_all_one and port_from_f1_valid = '1' then
                                state <= select_idle;
                            end if;
                        elsif state = select_f2 then
                            if port_from_f2_last = out_last_all_one and port_from_f2_valid = '1' then
                                state <= select_idle;
                            end if;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

    port_out_data <= port_from_f1_data when state = select_f1 else port_from_f2_data;
    port_out_last <= port_from_f1_last when state = select_f1 else port_from_f2_last;
    port_out_valid <= port_from_f1_valid when state = select_f1 else port_from_f2_valid;

    port_from_f1_ready <= port_out_ready when state = select_f1 else (others => '0');
    port_from_f2_ready <= port_out_ready when state = select_f2 else (others => '0');

end behavioral;