library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity vector_fork is
    generic(
        num_clients: natural := 4
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_vector_generic_type(num_clients - 1 downto 0);
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;

        port_out_clients_data: out data_word_vector_generic_type(num_clients - 1 downto 0);
        port_out_clients_last: out last_0_vector_generic_type(num_clients - 1 downto 0);
        port_out_clients_valid: out std_logic_vector(num_clients - 1 downto 0);
        port_out_clients_ready: in ready_0_vector_generic_type(num_clients - 1 downto 0)
    );
end vector_fork;

architecture behavioral of vector_fork is
begin

    port_out_clients_data <= port_in_data;
    port_out_clients_last <= (others => port_in_last);
    port_out_clients_valid <= (others => port_in_valid);
    -- TODO this is not very generic! check that all clients have been ready, then send ready signal
    port_in_ready <= port_out_clients_ready(0);

end behavioral;
