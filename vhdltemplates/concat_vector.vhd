library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity concat_vector is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_vector_tuple_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_vector_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end concat_vector;

architecture behavioral of concat_vector is
    -- all these types and signals are required to explain to VHDL that the types match
    type generic_vector_type is array (natural range <>) of data_word_type;
    subtype full_vector_type      is generic_vector_type(data_word_vector_type'high downto data_word_vector_type'low);
    subtype low_vector_part_type  is generic_vector_type(data_word_vector_slice_low_type'high downto data_word_vector_slice_low_type'low);
    subtype high_vector_part_type is generic_vector_type(data_word_vector_slice_high_type'high downto data_word_vector_slice_high_type'low);

    signal result: full_vector_type;
begin

    result <= high_vector_part_type(port_in_data.t1) & low_vector_part_type(port_in_data.t0);
    port_out_data <= data_word_vector_type(result);
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= "1" when port_out_ready = "1" else "0";

end behavioral;
