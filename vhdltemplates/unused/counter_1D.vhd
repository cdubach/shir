library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity counter is
    generic(
        start: natural := 4;
        limit: natural := 8;
        increment: natural := 2
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_out_data: out data_word_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end counter;

architecture behavioral of counter is
    signal counter_cnt: natural range start to limit := start;
begin

    port_out_data <= std_logic_vector(to_unsigned(counter_cnt, port_out_data'length));
    port_out_valid <= '1';

    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_cnt <= start;
            else
                if port_out_ready(port_out_ready'low) = '1' then
                    if counter_cnt <= limit - increment then
                        counter_cnt <= counter_cnt + increment;
                    else
                        counter_cnt <= start;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
