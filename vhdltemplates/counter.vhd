library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity counter is
    generic(
        start: natural := 0;
        increment: natural := 2;
        dimensions: natural_vector_type := (2, 5, 3); -- read from right to left (innermost dimension to outermost)
        repetitions: std_logic_vector := "110"; -- read from left to right (innermost to outermost repeat)
        loop_all: std_logic := '0'
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_out_data: out data_word_type;
        port_out_last: out last_3_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_3_type
    );
end counter;

architecture behavioral of counter is

    -- function to precompute constants (during compilation)
    function precomp_increments return natural_vector_type is
        variable increment_per_dimension: natural_vector_type := (others => increment);
    begin
        if repetitions(0) = '1' then
            increment_per_dimension(0) := increment;
        else
            increment_per_dimension(0) := dimensions(0) * increment;
        end if;

        -- multiplication accumulate
        for i in dimensions'low + 1 to dimensions'high loop
            if repetitions(i) = '1' then
                increment_per_dimension(i) := increment_per_dimension(i - 1);
            else
                increment_per_dimension(i) := increment_per_dimension(i - 1) * dimensions(i);
            end if;
        end loop;
        return increment_per_dimension;
    end function;

    signal counter_value: natural := 0;
    signal counter_dimensions: natural_vector_type := (others => 0);
    constant increment_per_dimension: natural_vector_type := precomp_increments;

    signal out_last: std_logic_vector(port_out_last'range) := (others => '0');
    signal out_valid: std_logic := '1';

begin

    port_out_data <= std_logic_vector(to_unsigned(counter_value + start, port_out_data'length));
    port_out_last <= out_last;
    port_out_valid <= out_valid;

    last_signals: process(counter_dimensions)
    begin
        out_last <= (others => '0');
        for i in dimensions'low to dimensions'high loop
            if counter_dimensions(i) = dimensions(i) - 1 then
                out_last(i) <= '1';
            end if;
        end loop;
    end process;

    counter_dimensions_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_dimensions <= (others => 0);
            else
                if out_valid = '1' then
                    for i in dimensions'low to dimensions'high loop
                        if port_out_ready(i) = '1' then
                            if counter_dimensions(i) < dimensions(i) - 1 then
                                counter_dimensions(i) <= counter_dimensions(i) + 1;
                            else
                                counter_dimensions(i) <= 0;
                            end if;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;

    counter_value_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_value <= 0;
                out_valid <= '1';
            else
                if out_valid = '1' and port_out_ready(port_out_ready'low) = '1' then
                    -- increase value
                    if repetitions(repetitions'low) = '0' or out_last(out_last'low) = '1' then
                        if counter_value <= increment_per_dimension(dimensions'high) - increment - 1 then
                            counter_value <= counter_value + increment;
                        else
                            counter_value <= 0;
                            if port_out_ready(port_out_ready'high) = '1' then
                                -- finished counting, do not repeat, if loop_all is not set
                                out_valid <= loop_all;
                            end if;
                        end if;
                    end if;

                    -- repeat
                    for i in dimensions'low to dimensions'high - 1 loop
                        if out_last(i) = '1' and port_out_ready(i) = '1' and (port_out_ready(i + 1) = '0' or (repetitions(i + 1) = '1' and out_last(i + 1) = '0')) then
                            assert(counter_value + increment - increment_per_dimension(i) >= 0) report "error in incoming ready signals";
                            counter_value <= counter_value + increment - increment_per_dimension(i);
                            exit;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;

end behavioral;
