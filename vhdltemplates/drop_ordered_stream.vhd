library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity drop_ordered_stream is
    generic(
        stream_length: natural := 8;
        first_elements: natural := 2;
        last_elements: natural := 1
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_1_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_1_type;
        port_out_data: out data_word_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end drop_ordered_stream;

architecture behavioral of drop_ordered_stream is

    constant last_all_one: std_logic_vector(port_in_last'range) := (others => '1');

    signal last_data_buffer: data_word_type;

    signal index_valid: std_logic := '0';

    signal out_last: std_logic_vector(port_out_last'range) := (others => '0');
    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');

    signal counter: natural range 0 to stream_length - 1 := 0;

begin

    -- if last_elements is > 0 send last_data_buffer as last element instead of port_out_data
    port_out_data <= port_in_data when last_elements = 0 or out_last /= last_all_one else last_data_buffer;

    port_out_last <= out_last;
    last_signal: process(port_in_last, counter)
    begin
        out_last <= port_in_last;
        if counter >= stream_length - 1 - last_elements then
            out_last(out_last'high) <= '1';
        else
            out_last(out_last'high) <= '0';
        end if;
    end process;

    port_out_valid <= port_in_valid and index_valid;

    index_valid <= '1' when (counter >= first_elements and counter <= stream_length - 1 - last_elements and out_last /= last_all_one) or port_in_last = last_all_one else '0';

    port_in_ready <= in_ready;
    ready_signal: process(port_in_last, port_out_ready, in_ready, index_valid)
    begin
        if index_valid = '1' then
            in_ready <= port_out_ready;
        else
            -- always ready!
            in_ready(0) <= '1';
            for i in in_ready'low + 1 to in_ready'high loop
                if port_in_last(i - 1) = '1' and in_ready(i - 1) = '1' then
                    in_ready(i) <= '1';
                else
                    in_ready(i) <= '0';
                end if;
            end loop;
        end if;
    end process;

    last_data_buffer_logic: process(clk)
    begin
        if rising_edge(clk) then
            if counter = stream_length - 1 - last_elements and port_in_valid = '1' then
                last_data_buffer <= port_in_data;
            end if;
        end if;
    end process;

    counter_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter <= 0;
            else
                if port_in_valid = '1' and in_ready(in_ready'high - 1) = '1' then
                    if counter < stream_length - 1 then
                        counter <= counter + 1;
                    else
                        counter <= 0;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;
