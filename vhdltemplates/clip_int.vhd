library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity clip_int is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end clip_int;

architecture behavioral of clip_int is
    constant max_val: std_logic_vector(port_out_data'range) := (others => '1');
begin
    port_out_data <= max_val when unsigned(port_in_data) > unsigned(max_val) else
                     port_in_data(port_out_data'range);
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;
end behavioral;
