library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity join_vector is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_vector_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_joined_vector_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end join_vector;

architecture behavioral of join_vector is
begin

    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;
    
    process(port_in_data)
        constant chunksize: natural := port_in_data(0)'length;
    begin
        for i in 0 to port_in_data'length-1 loop
            for j in 0 to chunksize-1 loop
                port_out_data(i*chunksize + j) <= port_in_data(i)(j);
            end loop;
        end loop;
        --for i in 0 to port_in_data'length-1 loop
        --    port_out_data((i+1)*chunksize-1 downto i*chunksize) <= port_in_data(i);
        --end loop;
    end process;

end behavioral;
