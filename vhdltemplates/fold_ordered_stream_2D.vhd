library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common.all;

entity fold_ordered_stream is
    generic(
        inner_length: natural := 8
    );
    port(
        clk: in std_logic;
        reset: in std_logic;

        -- stream of data to fold
        port_in_data: in data_word_stream_of_stream_type;
        port_in_last: in last_2_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_2_type;
        -- accumulator output
        port_out_data: out data_word_stream_type;
        port_out_last: out last_1_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_1_type
    );
end fold_ordered_stream;

architecture behavioral of fold_ordered_stream is

    component add is -- %TESTING_ONLY
        port( -- %TESTING_ONLY
            clk: in std_logic; -- %TESTING_ONLY
            reset: in std_logic; -- %TESTING_ONLY
            port_in_1_data: in data_word_type; -- %TESTING_ONLY
            port_in_1_last: in last_1_type; -- %TESTING_ONLY
            port_in_1_valid: in std_logic; -- %TESTING_ONLY
            port_in_1_ready: out ready_1_type; -- %TESTING_ONLY
            port_in_2_data: in data_word_type; -- %TESTING_ONLY
            port_in_2_last: in last_1_type; -- %TESTING_ONLY
            port_in_2_valid: in std_logic; -- %TESTING_ONLY
            port_in_2_ready: out ready_1_type; -- %TESTING_ONLY
            port_out_data: out data_word_type; -- %TESTING_ONLY
            port_out_last: out last_1_type; -- %TESTING_ONLY
            port_out_valid: out std_logic; -- %TESTING_ONLY
            port_out_ready: in ready_1_type -- %TESTING_ONLY
        ); -- %TESTING_ONLY
    end component; -- %TESTING_ONLY

    component fifo_ram_buffer
        generic(
            data_width: natural := 32;
            entries: natural := 8
        );
        port(
            clk: in std_logic;
            reset: in std_logic;
            data_in: in std_logic_vector(data_width - 1 downto 0);
            valid_in: in std_logic;
            ready_in: out std_logic_vector(0 downto 0);
            data_out: out std_logic_vector(data_width - 1 downto 0);
            valid_out: out std_logic;
            ready_out: in std_logic_vector(0 downto 0);
            element_count: out natural range 0 to entries
        );
    end component;

    signal fifo_in_data: std_logic_vector(port_out_data'range) := (others => '0');
    signal fifo_in_valid: std_logic;
    signal fifo_in_ready: std_logic_vector(0 downto 0) := "0";
    signal fifo_out_data: std_logic_vector(port_out_data'range) := (others => '0');
    signal fifo_out_valid: std_logic;
    signal fifo_out_ready: std_logic_vector(0 downto 0) := "0";

    signal first_inner_stream: std_logic := '1';
    signal first_inner_element: std_logic := '1';

    signal in_ready: std_logic_vector(port_in_ready'range) := (others => '0');

    signal port_f_out_acc_data: data_word_type; -- %TESTING_ONLY
    signal port_f_out_acc_last: last_1_type; -- %TESTING_ONLY
    signal port_f_out_acc_valid: std_logic; -- %TESTING_ONLY
    signal port_f_out_acc_ready: ready_1_type; -- %TESTING_ONLY
    signal port_f_out_data_data: data_word_type; -- %TESTING_ONLY
    signal port_f_out_data_last: last_1_type; -- %TESTING_ONLY
    signal port_f_out_data_valid: std_logic; -- %TESTING_ONLY
    signal port_f_out_data_ready: ready_1_type; -- %TESTING_ONLY
    signal port_f_in_data: data_word_type; -- %TESTING_ONLY
    signal port_f_in_last: last_1_type; -- %TESTING_ONLY
    signal port_f_in_valid: std_logic; -- %TESTING_ONLY
    signal port_f_in_ready: ready_1_type; -- %TESTING_ONLY

begin

    f: add port map( -- %TESTING_ONLY
        clk => clk, -- %TESTING_ONLY
        reset => reset, -- %TESTING_ONLY
        port_in_1_data => port_f_out_acc_data, -- %TESTING_ONLY
        port_in_1_last => port_f_out_acc_last, -- %TESTING_ONLY
        port_in_1_valid => port_f_out_acc_valid, -- %TESTING_ONLY
        port_in_1_ready => port_f_out_acc_ready, -- %TESTING_ONLY
        port_in_2_data => port_f_out_data_data, -- %TESTING_ONLY
        port_in_2_last => port_f_out_data_last, -- %TESTING_ONLY
        port_in_2_valid => port_f_out_data_valid, -- %TESTING_ONLY
        port_in_2_ready => port_f_out_data_ready, -- %TESTING_ONLY
        port_out_data => port_f_in_data, -- %TESTING_ONLY
        port_out_last => port_f_in_last, -- %TESTING_ONLY
        port_out_valid => port_f_in_valid, -- %TESTING_ONLY
        port_out_ready => port_f_in_ready -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    fifo: fifo_ram_buffer
    generic map(
        data_width => port_out_data'length,
        entries => inner_length + 3 -- must have two extra slots to push and pop at the same time; the 3rd additional slot gives some extra performance (one wait cycle is avoided)
    )
    port map(
        clk => clk,
        reset => reset,
        data_in => fifo_in_data,
        valid_in => fifo_in_valid,
        ready_in => fifo_in_ready,
        data_out => fifo_out_data,
        valid_out => fifo_out_valid,
        ready_out => fifo_out_ready,
        element_count => open
    );

    port_out_data <= port_f_in_data;
    port_out_last <= port_f_in_last;
    port_out_valid <= '1' when port_in_last(port_in_last'high) = '1' and port_f_in_valid = '1' else '0';

    fifo_in_data <= port_f_in_data;
    fifo_in_valid <= '1' when port_in_last(port_in_last'high) = '0' and port_f_in_valid = '1' else '0';

    port_f_in_ready <= (fifo_in_ready(fifo_in_ready'high) and port_f_in_last(port_f_in_last'high)) & fifo_in_ready when port_in_last(port_in_last'high) = '0' else port_out_ready;

    port_f_out_acc_data <= (others => '0') when first_inner_stream = '1' else fifo_out_data;
    port_f_out_acc_last <= port_in_last(port_f_out_data_last'high downto port_f_out_data_last'low);
    port_f_out_acc_valid <= port_in_valid when first_inner_stream = '1' else fifo_out_valid;
    fifo_out_ready <= "0" when first_inner_stream = '1' else port_f_out_acc_ready(port_f_out_acc_ready'low downto port_f_out_acc_ready'low);

    port_f_out_data_data <= port_in_data;
    port_f_out_data_last <= port_in_last(port_f_out_data_last'high downto port_f_out_data_last'low);
    port_f_out_data_valid <= port_in_valid;
    in_ready(port_f_out_data_ready'high downto port_f_out_data_ready'low) <= port_f_out_data_ready(port_f_out_data_ready'high downto port_f_out_data_ready'low + 1) &'1' when first_inner_element = '1' else port_f_out_data_ready;
    in_ready(in_ready'high) <= port_in_last(port_in_last'high) and in_ready(in_ready'high - 1);
    port_in_ready <= in_ready;

    state_logic: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                first_inner_stream <= '1';
                first_inner_element <= '1';
            else
                if port_f_in_valid = '1' and port_f_in_ready(in_ready'low) = '1' then
                    first_inner_element <= '0';
                    if port_f_in_last(port_f_in_last'high) = '1' then
                        if port_in_last(port_in_last'high) = '1' then
                            first_inner_stream <= '1';
                            first_inner_element <= '1';
                        elsif first_inner_stream = '1' then
                            first_inner_stream <= '0';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

end behavioral;