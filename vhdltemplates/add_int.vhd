library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity add_int is
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_tuple_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end add_int;

architecture behavioral of add_int is
    signal a1: std_logic_vector(port_out_data'range) := (others => '0');
    signal a2: std_logic_vector(port_out_data'range) := (others => '0');
begin

    fill_zero: process(port_in_data)
    begin
        a1 <= (others => '0');
        a1(port_in_data.t0'high downto port_in_data.t0'low) <= port_in_data.t0;
        a2 <= (others => '0');
        a2(port_in_data.t1'high downto port_in_data.t1'low) <= port_in_data.t1;
    end process;

    port_out_data <= std_logic_vector(unsigned(a1) + unsigned(a2));
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

end behavioral;
