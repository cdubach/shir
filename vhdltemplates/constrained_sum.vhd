library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity constrained_sum is
    generic(
        w_lower: natural := 1;
        w_upper: natural := 7;
        h_lower: natural := 1;
        h_upper: natural := 7;
        offset: natural := 6
    );
    port(
        clk: in std_logic;
        reset: in std_logic;
        port_in_data: in data_word_tuple_type;
        port_in_last: in last_0_type;
        port_in_valid: in std_logic;
        port_in_ready: out ready_0_type;
        port_out_data: out data_word_type;
        port_out_last: out last_0_type;
        port_out_valid: out std_logic;
        port_out_ready: in ready_0_type
    );
end constrained_sum;

architecture behavioral of constrained_sum is

begin

    port_out_data <= std_logic_vector(resize(unsigned(port_in_data.t0) - w_lower + (unsigned(port_in_data.t1) - h_lower) * offset, port_out_data'length)) when (unsigned(port_in_data.t0) >= w_lower and unsigned(port_in_data.t0) < w_upper) and (unsigned(port_in_data.t1) >= h_lower and unsigned(port_in_data.t1) < h_upper) else (others => '1');
    port_out_last <= port_in_last;
    port_out_valid <= port_in_valid;
    port_in_ready <= port_out_ready;

end behavioral;