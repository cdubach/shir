library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.common.all;

entity tb_template is
end tb_template;

architecture behavioral of tb_template is
    component test_uut  -- %TESTING_ONLY
        port( -- %TESTING_ONLY
            clk: in std_logic; -- %TESTING_ONLY
            reset: in std_logic -- %TESTING_ONLY
        ); -- %TESTING_ONLY
    end component; -- %TESTING_ONLY
    
    signal clk: std_logic;
    constant clk_period: time := 100 ns;
    signal reset: std_logic;

begin
    uut: test_uut port map( -- %TESTING_ONLY
        clk => clk, -- %TESTING_ONLY
        reset => reset -- %TESTING_ONLY
    ); -- %TESTING_ONLY

    clock_process: process
    begin
        clk <= '1';
        wait for clk_period /2;
        clk <= '0';
        wait for clk_period /2;
    end process;

    reset <= '1', '0' after 3.9*clk_period;

    stim_proc: process
    begin
        -- write test code here
        wait for 10*clk_period;



        wait;
    end process;
end behavioral;
