#!/bin/bash

# cleanup
rm -rf build_synth

# check if current user is in the `fpga` group
if id -nG "$USER" | grep -qw "fpga"; then
    echo $USER belongs to fpga group
else
    echo $USER does not belong to fpga group!
    exit 1
fi

# add required paths
source /opt/inteldevstack/init_env.sh

# create sources.txt, containing the relative file paths to all the vhd files and the json config file
cd hw
echo "shir_afu.json" > sources.txt
echo "rtl/generated/common.vhd" >> sources.txt
find rtl/generated/*.vhd ! -name "common.vhd" >> sources.txt
echo "rtl/shir_acc.vhd" >> sources.txt
echo "rtl/afu.sv" >> sources.txt
echo "rtl/ccip_interface_reg.sv" >> sources.txt
echo "rtl/ccip_std_afu.sv" >> sources.txt
cd ..

# initiate synthesis
afu_synth_setup --source hw/sources.txt build_synth
cd build_synth

# run actual synthesis to generate the bitstream `shir_afu.gbs`
${OPAE_PLATFORM_ROOT}/bin/run.sh |& tee ../real_synth.log

if [ -f "shir_afu.gbs" ]; then
    # remove signature from the generated bitstream
    PACSign PR -t UPDATE -H openssl_manager -i shir_afu.gbs -o shir_afu_unsigned_ssl.gbs -y
else
    # no bitstream found!
    echo "Error during Synthesis! No bitstream generated!"
fi
