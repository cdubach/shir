# SHIR wrapper README

- this wrapper and the provided scripts are required to synthesise the generated HDL files for the Intel Arria 10 FPGA
- create a copy of this wrapper folder on the FPGA server, place the generated HDL files in `hw/rtl/generated/` and run some of the following commands:
    - `real.sh` - run the synthesis to generate a bitstream for the _real_ FPGA.
    - `real_start.sh` - install the generated bitstream on the FPGA.
    - `real_sw` - run the software part, which communicates with the _real_ FPGA.
    - `sim.sh` - synthesise for simulation only (much faster).
    - `sim_start.sh` - start simulation.
    - `sim_sw.sh` - run the software part for the simulated FPGA.
- `hw` is based on the hello\_afu sample from the opae-sdk, take required dependencies from there.
- `sw` is based on the hello\_mpf\_afu sample (because it has a handy cpp class for all the FPGA functions) from the opae-sdk, take required dependencies from there.

